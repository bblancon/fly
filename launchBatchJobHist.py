#!/usr/bin/env python3
import os
import sys
import datetime
import ROOT
from os import listdir
from os.path import isfile, join

def dir_checker(directory):
    if not os.path.exists(directory):
        os.mkdir(directory)

date = sys.argv[1]
Data = sys.argv[2]
Combine = sys.argv[3]
JEC = sys.argv[4]
pwd = 'analyzed/' + date + '/Merged/'
Samples = [samples for samples in listdir(pwd) if '.root' in samples]

files = (
    (Samples, pwd)
)

cpus_per_task = '1'

for sample in files[0]:

    if len(files[0]) == 0: break
    file = ROOT.TFile.Open(pwd + sample, "READ")
    tree = file.Get("outputTree")
    if not tree or tree.GetEntries() == 0:
        print("Deleting empty file:" + sample)
        os.remove(pwd + sample)
        file.Close()
        continue
    file.Close()

    process = 'none'
    if('Signal600' in sample):
        process = 'signal600'
    elif('Signal625' in sample):
        process = 'signal625'
    elif('Signal650' in sample):
        process = 'signal650'
    elif('Signal675' in sample):
        process = 'signal675'
    elif('Signal700' in sample):
        process = 'signal700'
    elif('Signal800' in sample):
        process = 'signal800'
    elif('Signal900' in sample):
        process = 'signal900'
    elif('Signal1000' in sample):
        process = 'signal1000'
    elif('Signal1100' in sample):
        process = 'signal1100'
    elif('Signal1200' in sample):
        process = 'signal1200'
    elif('DY' in sample):
        process = 'dy'
    elif('TT_2L' in sample):
        process = 'tt2l'
    elif('TT_SL' in sample):
        process = 'tt1l'
    elif('TT_Had' in sample):
        process = 'tt0l'
    elif('tW_top' in sample or 'tW_antitop' in sample or 'tHq' in sample or 'tHW' in sample or 'tZq' in sample):
        process = 'singletop'
    elif('ttW' in sample or 'ttH' in sample or 'ttZ' in sample or 'ttg' in sample):
        process = 'ttx'
    elif('WW' in sample):
        if('WW_DS' in sample):
            process = 'ww_ds'
        elif('WWW' in sample or 'WWZ' in sample):
            process = 'multibosons'
        else:
            process = 'ww'
    elif('WH' in sample or 'WZ' in sample or 'ZH' in sample or 'ZZ' in sample):
        process = 'multibosons'
    elif('WJets' in sample):
        process = 'wjets'
    elif('Data' in sample):
        process = 'data_obs'

    year = '2018'
    if('2016preVFP' in sample):
        year = '2016preVFP'
    elif('2016postVFP' in sample):
        year = '2016postVFP'
    elif('2017' in sample):
        year = '2017'

    analysisOutput = 'analyzed/' + str(datetime.datetime.now().strftime('%Y_%m_%d_%H')) + '/'
    dir_checker(analysisOutput)
    os.system('cp src/TprimeAnalyser.cpp ' + analysisOutput + 'TprimeAnalyser_COPY.cpp')
    analysisOutput = 'analyzed/' + str(datetime.datetime.now().strftime('%Y_%m_%d_%H')) + '/UL' + year + '/'
    dir_checker(analysisOutput)
    dir_checker(analysisOutput + 'slurmOutputs')
    analysisOutput_merged = 'analyzed/' + str(datetime.datetime.now().strftime('%Y_%m_%d_%H')) + '/Merged/'
    dir_checker(analysisOutput_merged)

    position = sample.find(year)
    cut_index = sample[position:].replace(year+'_','').replace('.root','')

    slurmConfigs = [

        ' --cpus-per-task=' + cpus_per_task,
        ' --ntasks=' + str(1),
        ' --job-name=' + sample + year.replace('20',''),
        ' --output=' + analysisOutput + 'slurmOutputs/output.out',
        ' --error=' + analysisOutput + 'slurmOutputs/errors.err'
    ]

    args = [

        ' ' + files[1] + sample, #inputFile
        ' ' + analysisOutput, #outputFiles
        ' ' + year,
        ' ' + process, #which process is studied
        ' ' + Combine, #true to save histograms only with weights
        ' ' + JEC, #true to run only for JEC weights
        ' ' + cut_index, #run the correct region
        ' ' + Data,
    ]

    if(Combine == 'False') or (Combine == 'True' and JEC == 'False' and cut_index[4] == '0' and ((Data == 'False') or (Data == 'True' and cut_index[8] == '1'))) or (Combine == 'True' and JEC == 'True' and cut_index[2] != '0' and 'tW_' in sample):# and year == '2018' and process == 'signal700'):
        os.system('sbatch ' + ''.join(slurmConfigs) + ' submitBatchJobHist.sh' + ''.join(args))