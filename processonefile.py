#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 14 11:01:46 2018

@author: Suyong Choi (Department of Physics, Korea University suyong@korea.ac.kr)

This script applies nanoaod processing to one file
"""

import sys
import cppyy
import ROOT
import re
import os
from importlib import import_module
from argparse import ArgumentParser
from btagging_efficiency_binning import btageff_dataset_dict

if __name__=='__main__':
    parser = ArgumentParser(usage="%prog inputfile outputfile jobconfmod year era process BTagEff Hist Combine JEC Data")
    parser.add_argument("inputfile")
    parser.add_argument("outputfile")
    parser.add_argument("jobconfmod")
    parser.add_argument("year")
    parser.add_argument("era")
    parser.add_argument("process")
    parser.add_argument("BTagEff")
    parser.add_argument("Hist")
    parser.add_argument("Combine")
    parser.add_argument("JEC")
    parser.add_argument("Data")
    args = parser.parse_args()
    inputfile = args.inputfile
    outputfile = args.outputfile
    jobconfmod = args.jobconfmod
    year = args.year
    era = args.era
    process = args.process
    BTagEff = args.BTagEff
    Hist = args.Hist
    Combine = args.Combine
    JEC = args.JEC
    Data = args.Data
    if(BTagEff == 'False'):
        BTagEff = bool(0)
    if(BTagEff == 'True'):
        BTagEff = bool(1)
    if(Hist == 'False'):
        Hist = bool(0)
    if(Hist == 'True'):
        Hist = bool(1)
    if(Combine == 'False'):
        Combine = bool(0)
    if(Combine == 'True'):
        Combine = bool(1)
    if(JEC == 'False'):
        JEC = bool(0)
    if(JEC == 'True'):
        JEC = bool(1)
    if(Data == 'False'):
        Data = bool(0)
    if(Data == 'True'):
        Data = bool(1)

    # load job configuration python module and get bjects
    mod = import_module(jobconfmod)
    config = getattr(mod, 'config')
    procflags = getattr(mod, 'procflags')
    print(config)

    intreename = config['intreename']
    outtreename = config['outtreename']
    saveallbranches = procflags['saveallbranches']

    # load compiled C++ library into ROOT/python
    cppyy.load_reflection_info("libcorrectionlib.so")
    cppyy.load_reflection_info("libMathMore.so")
    cppyy.load_reflection_info("libnanoadrdframe.so")
    t = ROOT.TChain(intreename)
    t.Add(inputfile)
    print("Inside process one file..!!")

    aproc = ROOT.TprimeAnalyser(t, outputfile)
    sys.stdout.flush()

    aproc.setParams(year, config['runtype'],config['datatype'], Data)
    sys.stdout.flush()
    if("data_obs" in process):
        aproc.setupCorrections(config['goodjson'], config['pileupfname'], config['pileuptag'], config['btvfname'], config['btvtype'], config['fname_btagEff'], config['hname_Loose_btagEff_bcflav'], config['hname_Loose_btagEff_lflav'], config['hname_Medium_btagEff_bcflav'], config['hname_Medium_btagEff_lflav'], config['hname_Tight_btagEff_bcflav'], config['hname_Tight_btagEff_lflav'], config['pileupjetidfname'], config['fname_pileupjetidEff'], config['hname_Loose_pileupjetidEff'], config['hname_Medium_pileupjetidEff'], config['hname_Tight_pileupjetidEff'], config['muon_roch_fname'], config['muon_fname'], config['muon_loosett1l_ISO_type'], config['muon_HLT_type'], config['muon_RECO_type'], config['muon_ID_type'], config['muon_ISO_type'], config['electron_fname'], config['electron_loosett1l_id_type'], config['electron_reco_type'], config['electron_id_type'], config['fname_electriggerEff'], config['hname_electriggerEff_eff'], config['hname_electriggerEff_statdata'], config['hname_electriggerEff_statmc'], config['hname_electriggerEff_systmc'], config['jercfname'], config['jerctag_'+era], config['jercunctag'], config['jertag'], config['fname_metphimod'])
    else:
        aproc.setupCorrections(config['goodjson'], config['pileupfname'], config['pileuptag'], config['btvfname'], config['btvtype'], config['fname_btagEff'], config['hname_Loose_btagEff_bcflav'], config['hname_Loose_btagEff_lflav'], config['hname_Medium_btagEff_bcflav'], config['hname_Medium_btagEff_lflav'], config['hname_Tight_btagEff_bcflav'], config['hname_Tight_btagEff_lflav'], config['pileupjetidfname'], config['fname_pileupjetidEff'], config['hname_Loose_pileupjetidEff'], config['hname_Medium_pileupjetidEff'], config['hname_Tight_pileupjetidEff'], config['muon_roch_fname'], config['muon_fname'], config['muon_loosett1l_ISO_type'], config['muon_HLT_type'], config['muon_RECO_type'], config['muon_ID_type'], config['muon_ISO_type'], config['electron_fname'], config['electron_loosett1l_id_type'], config['electron_reco_type'], config['electron_id_type'], config['fname_electriggerEff'], config['hname_electriggerEff_eff'], config['hname_electriggerEff_statdata'], config['hname_electriggerEff_statmc'], config['hname_electriggerEff_systmc'], config['jercfname'], config['jerctag_MC'], config['jercunctag'], config['jertag'], config['fname_metphimod'])
    sys.stdout.flush()

    aproc.setupObjects(year, process, config ['weights'])
    sys.stdout.flush()
    aproc.setupAnalysis(year, process, BTagEff, Hist, Combine, JEC, config['variables_MC'], config['variables'], config ['weights'], config ['Cuts'], config['Masses'], config['getQuantiles_ptleptons'], config['getQuantiles_massjets'], config['getQuantiles_ptleptons_loosett1l'], config['getQuantiles_massjets_loosett1l'], '0')
    sys.stdout.flush()
    aproc.run(saveallbranches, outtreename, process, Hist, JEC)
