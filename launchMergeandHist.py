#!/usr/bin/env python3
import ROOT
import sys
import os

date = '2025_02_25_19'
date2 = '2025_02_23_21'

# # Case 1/Case 2: MC (not Combine and Combine but not JEC)
# os.system('./mergeFiles.sh '+ date + ' ' + date2)

# Case 1: MC (not Combine)
# os.system('python3 removeEmptyFiles.py analyzed/'+ date + ' MC')
# os.system('./mergeFiles.sh '+ date + ' ' + date2)
# os.system('python3 launchBatchJobHist.py ' + date + ' False False False')
# os.system('./mergeFiles.sh '+ date + ' ' + date2)

# Case 2: MC (Combine but not JEC)
# os.system('python3 removeEmptyFiles.py analyzed/'+ date + ' MC')
os.system('./mergeFiles.sh '+ date + ' ' + date2)
# os.system('python3 launchBatchJobHist.py ' + date + ' False True False')
# os.system('./mergeFiles.sh '+ date + ' ' + date2)

# Case 3: MC (Combine and JEC)
# os.system('python3 removeEmptyFiles.py analyzed/'+ date + ' MC')
# os.system('./mergeFiles.sh '+ date + ' ' + date2)
# os.system('python3 launchBatchJobHist.py ' + date + ' False True True')
# os.system('./mergeFiles.sh '+ date + ' ' + date2)

# Case 4: Data (not Combine)
# os.system('python3 removeEmptyFiles.py analyzed/'+ date + ' DATA')
# os.system('./mergeFiles.sh '+ date + ' ' + date2)
# os.system('python3 removeDoubleCounting.py analyzed/'+ date +'/Merged')
# os.system('python3 launchBatchJobHist.py ' + date + ' True False False')
# os.system('./mergeFiles.sh '+ date + ' ' + date2)

# Case 5: Data (Combine)
# os.system('python3 removeEmptyFiles.py analyzed/'+ date + ' DATA')
# os.system('./mergeFiles.sh '+ date + ' ' + date2)
# os.system('python3 removeDoubleCounting.py analyzed/'+ date +'/Merged')
# os.system('python3 launchBatchJobHist.py ' + date + ' True True False')
# os.system('./mergeFiles.sh '+ date + ' ' + date2)