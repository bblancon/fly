import os
import datetime

def dir_checker(directory):
    if not os.path.exists(directory):
        os.mkdir(directory)

# year = '2016preVFP'
# year = '2016postVFP'
year = '2017'
# year = '2018'
Data = 'False'
BTagEff = 'False'
Hist = 'True'
Combine = 'True'
JEC = 'False'
pwd = 'list_datasets/UL' + year + '/'

if(Data == 'False'):
    Samples = [

    # 'List_Signal600',
    # 'List_Signal625',
    # 'List_Signal650',
    # 'List_Signal675',
    # 'List_Signal700',
    # 'List_Signal800',
    # 'List_Signal900',
    # 'List_Signal1000',
    # 'List_Signal1100',
    # 'List_Signal1200',
    'List_DY_10_50',
    # 'List_DY_4_50_70',
    # 'List_DY_4_50_100',
    # 'List_DY_4_50_200',
    # 'List_DY_4_50_400',
    # 'List_DY_4_50_600',
    # 'List_TT_2L',
    # 'List_TT_SL',
    # 'List_TT_Had',
    # 'List_ttW',
    # 'List_ttH',
    # 'List_ttZ',
    # 'List_tW_antitop',
    # 'List_tW_top',
    # 'List_tHq',
    # 'List_tHW',
    # 'List_tZq',
    # 'List_WW',
    # 'List_WW_DS',
    # 'List_WJets',
    # 'List_WH_plus',
    # 'List_WH_minus',
    # 'List_WZ',
    # 'List_ZH',
    # 'List_ZZ_2L2Nu',
    # 'List_ZZ_2L2Q',
    # 'List_ZZ_4L',
    # 'List_WWW',
    # 'List_WWZ',
    # 'List_WZZ',
    # 'List_ZZZ',
    ]

elif(Data == 'True'):
    if(year == "2016preVFP"):
        Samples = [

        # # 'List_Data_DoubleMuon_UL2016preVFPB1',
        # 'List_Data_DoubleMuon_UL2016preVFPB2',
        # 'List_Data_DoubleMuon_UL2016preVFPC',
        # 'List_Data_DoubleMuon_UL2016preVFPD',
        # 'List_Data_DoubleMuon_UL2016preVFPE',
        # 'List_Data_DoubleMuon_UL2016preVFPF',
        # # 'List_Data_SingleMuon_UL2016preVFPB1',
        # 'List_Data_SingleMuon_UL2016preVFPB2',
        # 'List_Data_SingleMuon_UL2016preVFPC',
        # 'List_Data_SingleMuon_UL2016preVFPD',
        # 'List_Data_SingleMuon_UL2016preVFPE',
        # 'List_Data_SingleMuon_UL2016preVFPF',
        # # 'List_Data_SingleElectron_UL2016preVFPB1',
        # 'List_Data_SingleElectron_UL2016preVFPB2',
        # 'List_Data_SingleElectron_UL2016preVFPC',
        # 'List_Data_SingleElectron_UL2016preVFPD',
        # 'List_Data_SingleElectron_UL2016preVFPE',
        # 'List_Data_SingleElectron_UL2016preVFPF',
        # # 'List_Data_MuonEG_UL2016preVFPB1',
        # 'List_Data_MuonEG_UL2016preVFPB2',
        # 'List_Data_MuonEG_UL2016preVFPC',
        # 'List_Data_MuonEG_UL2016preVFPD',
        # 'List_Data_MuonEG_UL2016preVFPE',
        # 'List_Data_MuonEG_UL2016preVFPF',
        # # 'List_Data_DoubleEG_UL2016preVFPB1',
        # 'List_Data_DoubleEG_UL2016preVFPB2',
        # 'List_Data_DoubleEG_UL2016preVFPC',
        'List_Data_DoubleEG_UL2016preVFPD',
        # 'List_Data_DoubleEG_UL2016preVFPE',
        # 'List_Data_DoubleEG_UL2016preVFPF',
        # # 'List_Data_MET_UL2016preVFPB1',
        # 'List_Data_MET_UL2016preVFPB2',
        # 'List_Data_MET_UL2016preVFPC',
        # 'List_Data_MET_UL2016preVFPD',
        # 'List_Data_MET_UL2016preVFPE',
        # 'List_Data_MET_UL2016preVFPF',
        ]

    if(year == "2016postVFP"):
        Samples = [

        'List_Data_DoubleMuon_UL2016postVFPF',
        'List_Data_DoubleMuon_UL2016postVFPG',
        'List_Data_DoubleMuon_UL2016postVFPH',
        'List_Data_SingleMuon_UL2016postVFPF',
        'List_Data_SingleMuon_UL2016postVFPG',
        'List_Data_SingleMuon_UL2016postVFPH',
        'List_Data_SingleElectron_UL2016postVFPF',
        'List_Data_SingleElectron_UL2016postVFPG',
        'List_Data_SingleElectron_UL2016postVFPH',
        'List_Data_MuonEG_UL2016postVFPF',
        'List_Data_MuonEG_UL2016postVFPG',
        'List_Data_MuonEG_UL2016postVFPH',
        'List_Data_DoubleEG_UL2016postVFPF',
        'List_Data_DoubleEG_UL2016postVFPG',
        'List_Data_DoubleEG_UL2016postVFPH',
        # 'List_Data_MET_UL2016postVFPF',
        # 'List_Data_MET_UL2016postVFPG',
        # 'List_Data_MET_UL2016postVFPH',
        ]

    if(year == "2017"):
        Samples = [

        'List_Data_DoubleMuon_UL2017B',
        'List_Data_DoubleMuon_UL2017C',
        'List_Data_DoubleMuon_UL2017D',
        'List_Data_DoubleMuon_UL2017E',
        'List_Data_DoubleMuon_UL2017F',
        'List_Data_SingleMuon_UL2017B',
        'List_Data_SingleMuon_UL2017C',
        'List_Data_SingleMuon_UL2017D',
        'List_Data_SingleMuon_UL2017E',
        'List_Data_SingleMuon_UL2017F',
        'List_Data_SingleElectron_UL2017B',
        'List_Data_SingleElectron_UL2017C',
        'List_Data_SingleElectron_UL2017D',
        'List_Data_SingleElectron_UL2017E',
        'List_Data_SingleElectron_UL2017F',
        'List_Data_MuonEG_UL2017B',
        'List_Data_MuonEG_UL2017C',
        'List_Data_MuonEG_UL2017D',
        'List_Data_MuonEG_UL2017E',
        'List_Data_MuonEG_UL2017F',
        'List_Data_DoubleEG_UL2017B',
        'List_Data_DoubleEG_UL2017C',
        'List_Data_DoubleEG_UL2017D',
        'List_Data_DoubleEG_UL2017E',
        'List_Data_DoubleEG_UL2017F',
        # 'List_Data_MET_UL2017B',
        # 'List_Data_MET_UL2017C',
        # 'List_Data_MET_UL2017D',
        # 'List_Data_MET_UL2017E',
        # 'List_Data_MET_UL2017F',
        ]

    if(year == "2018"):
        Samples = [

        'List_Data_DoubleMuon_UL2018A',
        'List_Data_DoubleMuon_UL2018B',
        'List_Data_DoubleMuon_UL2018C',
        'List_Data_DoubleMuon_UL2018D',
        'List_Data_SingleMuon_UL2018A',
        'List_Data_SingleMuon_UL2018B',
        'List_Data_SingleMuon_UL2018C',
        'List_Data_SingleMuon_UL2018D',
        'List_Data_MuonEG_UL2018A',
        'List_Data_MuonEG_UL2018B',
        'List_Data_MuonEG_UL2018C',
        'List_Data_MuonEG_UL2018D',
        'List_Data_EGamma_UL2018A',
        'List_Data_EGamma_UL2018B',
        'List_Data_EGamma_UL2018C',
        'List_Data_EGamma_UL2018D',
        # 'List_Data_MET_UL2018A',
        # 'List_Data_MET_UL2018B',
        # 'List_Data_MET_UL2018C',
        # 'List_Data_MET_UL2018D',
        ]

files = (
    (Samples, pwd)
)

tasks = 'All'
cpus_per_task = '1'

for sample in files[0]:

    if len(files[0]) == 0: break

    process = 'none'
    if('Signal600' in sample):
        process = 'signal600'
    elif('Signal625' in sample):
        process = 'signal625'
    elif('Signal650' in sample):
        process = 'signal650'
    elif('Signal675' in sample):
        process = 'signal675'
    elif('Signal700' in sample):
        process = 'signal700'
    elif('Signal800' in sample):
        process = 'signal800'
    elif('Signal900' in sample):
        process = 'signal900'
    elif('Signal1000' in sample):
        process = 'signal1000'
    elif('Signal1100' in sample):
        process = 'signal1100'
    elif('Signal1200' in sample):
        process = 'signal1200'
    elif('DY' in sample):
        process = 'dy'
    elif('TT_2L' in sample):
        process = 'tt2l'
    elif('TT_SL' in sample):
        process = 'tt1l'
    elif('TT_Had' in sample):
        process = 'tt0l'
    elif('tW_top' in sample or 'tW_antitop' in sample or 'tHq' in sample or 'tHW' in sample or 'tZq' in sample):
        process = 'singletop'
    elif('ttW' in sample or 'ttH' in sample or 'ttZ' in sample or 'ttg' in sample):
        process = 'ttx'
    elif('WW' in sample):
        if('WW_DS' in sample):
            process = 'ww_ds'
        elif('WWW' in sample or 'WWZ' in sample):
            process = 'multibosons'
        else:
            process = 'ww'
    elif('WH' in sample or 'WZ' in sample or 'ZH' in sample or 'ZZ' in sample):
        process = 'multibosons'
    elif('WJets' in sample):
        process = 'wjets'
    elif('Data' in sample):
        process = 'data_obs'
    sample += '.txt'

    era = 'MC'
    if('Data' in sample):
        era = sample.split('_')[3].replace('.txt','')

    analysisOutput = 'analyzed/' + str(datetime.datetime.now().strftime('%Y_%m_%d_%H')) + '/'
    dir_checker(analysisOutput)
    os.system('cp src/TprimeAnalyser.cpp ' + analysisOutput + 'TprimeAnalyser_COPY.cpp')
    analysisOutput = 'analyzed/' + str(datetime.datetime.now().strftime('%Y_%m_%d_%H')) + '/UL' + year + '/'
    dir_checker(analysisOutput)
    analysisOutput = 'analyzed/' + str(datetime.datetime.now().strftime('%Y_%m_%d_%H')) + '/UL' + year + '/' + sample.replace('List_','').replace('.txt','') + '/'
    dir_checker(analysisOutput)
    dir_checker(analysisOutput + 'slurmOutputs')
    analysisOutput_merged = 'analyzed/' + str(datetime.datetime.now().strftime('%Y_%m_%d_%H')) + '/Merged/'
    dir_checker(analysisOutput_merged)

    if tasks == 'All':
        with open(files[1] + sample, 'r') as fToRead:
            ntasks = str(len(fToRead.readlines()))
    else: ntasks = tasks

    if('Data' in sample):
        slurmConfigs = [
            ' --array=1-' + ntasks,
            ' --cpus-per-task=' + cpus_per_task,
            ' --partition=normal',
            ' --mem=16G',
            ' --job-name=' + sample.replace('List_Data_','').replace('.txt',''),
            ' --output=' + analysisOutput + 'slurmOutputs/output.out',
            ' --error=' + analysisOutput + 'slurmOutputs/errors.err'
        ]

    else:
        slurmConfigs = [
            ' --array=1-' + ntasks,
            ' --cpus-per-task=' + cpus_per_task,
            ' --partition=normal',
            ' --mem=16G',
            ' --job-name=' + sample.replace('List_','').replace('.txt','') + year[3:],
            ' --output=' + analysisOutput + 'slurmOutputs/output.out',
            ' --error=' + analysisOutput + 'slurmOutputs/errors.err'
        ]

    args = [

            ' ' + files[1] + sample, #inputFile
            ' ' + analysisOutput, #outputFiles
            ' ' + year,
            ' ' + era, #which era of Data is selected
            ' ' + process, #which process is studied
            ' ' + BTagEff, #run to compute the Btag efficiency or not
            ' ' + Hist, #false if we want to store the trees, true if we want to store the histograms
            ' ' + Combine, #true to save histograms only with weights
            ' ' + JEC, #true to run only for JEC weights
            ' ' + Data,
    ]

    os.system('sbatch' + ''.join(slurmConfigs) + ' submitBatchJob.sh' + ''.join(args))