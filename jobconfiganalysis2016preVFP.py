"""
File contains job options 
"""

# options for Nanoaodrdframe
config = {

        # tree name of input file(s)
        'intreename': "Events",

        # tree name of output file(s) it cannot be the same as the input tree name or it'll crash
        'outtreename': "outputTree",

        #data year (2016,2017,2018)
        'year': 2016,

        # is ReReco or Ultra Legacy
        'runtype': 'UL',

        'datatype': -1, # 0=MC ; 1=DATA ; -1=Auto
        'era_list': ["Run2016preVFPB1", "Run2016preVFPB2", "Run2016preVFPC", "Run2016preVFPD", "Run2016preVFPE", "Run2016preVFPF"],

        #for correction
        # good json file
        # 'goodjson': 'data/Cert_271036-284044_13TeV_23Sep2016ReReco_Collisions16_JSON.txt',
        'goodjson' : 'data/Legacy_RunII/Cert_271036-284044_13TeV_Legacy2016_Collisions16_JSON.txt',

        # pileup weight for MC
        'pileupfname': 'data/LUM/2016preVFP_UL/puWeights.json',

        'pileuptag': 'Collisions16_UltraLegacy_goldenJSON',

        # json filename for BTV correction
        'btvfname': 'data/BTV/2016preVFP_UL/btagging.json',
        'fname_btagEff': 'data/BTV/2016preVFP_UL/BtaggingEfficiency.root',
        'hname_Loose_btagEff_bcflav': 'h_btagEff_bcflav',
        'hname_Loose_btagEff_lflav': 'h_btagEff_lflav',
        'hname_Medium_btagEff_bcflav': 'h_btagEff_bcflav',
        'hname_Medium_btagEff_lflav': 'h_btagEff_lflav',
        'hname_Tight_btagEff_bcflav': 'h_btagEff_bcflav',
        'hname_Tight_btagEff_lflav': 'h_btagEff_lflav',

        # BTV correction type
        # 'btvtype': 'deepJet_shape',
        'btvtype': 'deepJet_mujets',

        # json filenmae for PileupJetID correction
        'pileupjetidfname': 'data/JME/2016preVFP_UL/jmar.json.gz',
        'fname_pileupjetidEff': 'data/JME/2016preVFP_UL/PUID_106XTraining_ULRun2_EffSFandUncties_v1.root',
        'hname_Loose_pileupjetidEff': 'h2_eff_sfUL2016APV_L',
        'hname_Medium_pileupjetidEff': 'h2_eff_sfUL2016APV_M',
        'hname_Tight_pileupjetidEff': 'h2_eff_sfUL2016APV_T',

        # Muon Correction
        'muon_roch_fname': 'data/MUO/2016preVFP_UL/RoccoR2016aUL.txt',
        'muon_fname': 'data/MUO/2016preVFP_UL/muon_Z.json.gz',
        'muon_loosett1l_ISO_type': 'NUM_LooseRelIso_DEN_MediumID',#'ISO UL scale factor',
        'muon_HLT_type': 'NUM_IsoMu24_or_IsoTkMu24_DEN_CutBasedIdTight_and_PFIsoTight',#'HLT UL scale factor',
        'muon_RECO_type': 'NUM_TrackerMuons_DEN_genTracks',#'RECO UL scale factor',
        'muon_ID_type': 'NUM_MediumID_DEN_TrackerMuons',#'ID UL scale factor',
        'muon_ISO_type': 'NUM_TightRelIso_DEN_MediumID',#'ISO UL scale factor',

        # Electron Correction
        'electron_fname': 'data/EGM/2016preVFP_UL/electron.json.gz',
        'electron_loosett1l_id_type': 'Loose',
        'electron_reco_type': 'RecoAbove20',
        'electron_id_type': 'Tight',#'Tight ID UL scale factor',
        'fname_electriggerEff': 'data/EGM/2016preVFP_UL/egammaTrigEffi_wp90noiso_preVFP_EGM2D_2016.root',
        'hname_electriggerEff_eff': 'EGamma_SF2D',
        'hname_electriggerEff_statdata': 'statData',
        'hname_electriggerEff_statmc': 'statMC',
        'hname_electriggerEff_systmc': 'altTagSelection',

        # json file name for JERC
        'jercfname': 'data/JME/2016preVFP_UL/jet_jerc.json.gz',

        # combined correction type for jets
        'jerctag_MC': 'Summer19UL16APV_V7_MC',
        'jerctag_UL2016preVFPB1': 'Summer19UL16APV_RunBCD_V7_DATA',
        'jerctag_UL2016preVFPB2': 'Summer19UL16APV_RunBCD_V7_DATA',
        'jerctag_UL2016preVFPC': 'Summer19UL16APV_RunBCD_V7_DATA',
        'jerctag_UL2016preVFPD': 'Summer19UL16APV_RunBCD_V7_DATA',
        'jerctag_UL2016preVFPE': 'Summer19UL16APV_RunEF_V7_DATA',
        'jerctag_UL2016preVFPF': 'Summer19UL16APV_RunEF_V7_DATA',
        'jertag': 'Summer20UL16APV_JRV3_MC',

        # jet uncertainty
        # 'jercunctag' : [ ],
        'jercunctag': [
                "Regrouped_Absolute_2016",
                "Regrouped_Absolute",
                "Regrouped_BBEC1_2016",
                "Regrouped_BBEC1",
                "Regrouped_EC2_2016",
                "Regrouped_EC2",
                "Regrouped_FlavorQCD",
                "Regrouped_HF_2016",
                "Regrouped_HF",
                "Regrouped_RelativeSample_2016",
                "Regrouped_RelativeBal",
        ],
        
        # json file name for MET phi modulation correction
        'fname_metphimod': 'data/JME/2016preVFP_UL/met.json.gz',

        'variables_MC' : [
                # ("genweight","Genweight of Events; Genweight of Events;Events;", 40, -400.0, 400.0, "genWeight"),
                # ("puweight","PU; PU;Events;", 40, -400.0, 400.0, "puWeight"),
                # ("pugenweight","PU * Genweight of Events; PU * Genweight of Events;Events;", 40, -400.0, 400.0, "pugenWeight"),
                # ("MotherFromLepton1_pdgId","Nature of mother of first lepton; Nature of the mother of the first lepton;Events;", 50, 0, 50, "MotherfromLepton1_loosett1l_pdgId"),
                # ("MotherFromLepton2_pdgId","Nature of mother of second lepton; Nature of the mother of the second lepton;Events;", 50, 0, 50, "MotherfromLepton2_loosett1l_pdgId"),
                # ("MotherFromLepton12_pdgId","Sum of nature of mothers of leptons; Sum of the nature of the mothers of the two leptons;Events;", 50, 0, 50, "MotherfromLepton12_loosett1l_pdgId"),
                # ("muon_sf_central","Muon SF central; Muon SF central;Events;", 40, 0.5, 1.5, "muon_loosett1l_SF_central"),
                # ("ele_sf_central","Electron SF central; Electron SF central;Events;", 40, 0.5, 1.5, "ele_loosett1l_SF_central"),
                # ("btag_sf_bcflav_central","Btag SF central (bc flavour); Btag SF central (bc flavour);Events;", 40, 0.5, 1.5, "btag_loosett1l_SF_bcflav_central"),
                # ("btag_sf_lflav_central","Btag SF central (light flavour); Btag SF central (light flavour);Events;", 40, 0.5, 1.5, "btag_loosett1l_SF_lflav_central"),
                # ("btag_total","Btag SF central (bc + light flavour); Btag SF central;Events;", 40, 0.5, 1.5, "totbtagSF_loosett1l"),
                # ("evWeight_wobtagsf","Event weight without Btag; Event weight without Btag;Events;", 40, -400.0, 400.0, "evWeight_wobtagSF_loosett1l"),
                # ("evWeight","Event weight; Event weight;Events;", 40, -400.0, 400.0, "evWeight_loosett1l"),
                # ("MotherFromLepton1_pdgId","Nature of mother of first lepton; Nature of the mother of the first lepton;Events;", 50, 0, 50, "MotherfromLepton1_pdgId"),
                # ("MotherFromLepton2_pdgId","Nature of mother of second lepton; Nature of the mother of the second lepton;Events;", 50, 0, 50, "MotherfromLepton2_pdgId"),
                # ("MotherFromLepton12_pdgId","Sum of nature of mothers of leptons; Sum of the nature of the mothers of the two leptons;Events;", 50, 0, 50, "MotherfromLepton12_pdgId"),
                # ("muon_sf_central","Muon SF central; Muon SF central;Events;", 40, 0.5, 1.5, "muon_SF_central"),
                # ("ele_sf_central","Electron SF central; Electron SF central;Events;", 40, 0.5, 1.5, "ele_SF_central"),
                # ("btag_sf_bcflav_central","Btag SF central (bc flavour); Btag SF central (bc flavour);Events;", 40, 0.5, 1.5, "btag_SF_bcflav_central"),
                # ("btag_sf_lflav_central","Btag SF central (light flavour); Btag SF central (light flavour);Events;", 40, 0.5, 1.5, "btag_SF_lflav_central"),
                # ("btag_total","Btag SF central (bc + light flavour); Btag SF central;Events;", 40, 0.5, 1.5, "totbtagSF"),
                # ("evWeight_wobtagsf","Event weight without Btag; Event weight without Btag;Events;", 40, -400.0, 400.0, "evWeight_wobtagSF"),
                # ("evWeight","Event weight; Event weight;Events;", 40, -400.0, 400.0, "evWeight")
        ],

        'variables' : [
                # ("Number_Electrons","Number(electrons); Number(electrons);Events;", 5, 0.0, 5.0, "Selected_electron_loosett1l_number"),
                ("Pt_Electrons","p_{T}(electrons); p_{T}(electrons) (GeV);Events;", 20, 0.0, 400.0, "Selected_electron_loosett1l_pt"),
                # ("Leading_Pt_Electrons","p_{T}(e_{1}); p_{T}(e_{1}) (GeV);Events;", 20, 0.0, 400.0, "Selected_electron_loosett1l_leading_pt"),
                # ("Subleading_Pt_Electrons","p_{T}(e_{2}); p_{T}(e_{2}) (GeV);Events;", 10, 0.0, 200.0, "Selected_electron_loosett1l_subleading_pt"),
                ("Eta_Electrons","Eta(electrons); Eta(electrons);Events;", 25, -2.5, 2.5, "Selected_electron_loosett1l_eta"),
                # ("Leading_Eta_Electrons","Eta(e_{1}); Eta(e_{1});Events;", 25, -2.5, 2.5, "Selected_electron_loosett1l_leading_eta"),
                # ("Subleading_Eta_Electrons","Eta(e_{2}); Eta(e_{2});Events;", 25, -2.5, 2.5, "Selected_electron_loosett1l_subleading_eta"),
                ("Phi_Electrons","Phi(electrons); Phi(electrons);Events;", 35, -3.5, 3.5, "Selected_electron_loosett1l_phi"),
                # ("Charge_Electrons","Charge(electrons); Charge(electrons);Events;", 5, -2.0, 3.0, "Selected_electron_loosett1l_charge"),
                # ("miniPFRelIso_all_Electrons","Mini relative isolation(electrons); Mini relative isolation(electrons);Events;", 20, 0.0, 0.2, "Selected_electron_loosett1l_miniPFRelIso_all"),
                # ("Sip_Electrons","SIP_{3D}(electrons); SIP_{3D}(electrons);Events;", 10, 0.0, 2.0, "Selected_electron_loosett1l_sip3d"),
                # ("Number_Muons","Number(muons); Number(muons);Events;", 5, 0.0, 5.0, "Selected_muon_loosett1l_number"),
                ("Pt_Muons","p_{T}(muons); p_{T}(muons) (GeV);Events;", 20, 0.0, 400.0, "Selected_muon_loosett1l_pt"),
                # ("Leading_Pt_Muons","p_{T}(\\mu_{1}); p_{T}(\\mu_{1}) (GeV);Events;", 20, 0.0, 400.0, "Selected_muon_loosett1l_leading_pt"),
                # ("Subleading_Pt_Muons","p_{T}(\\mu_{2}); p_{T}(\\mu_{2}) (GeV);Events;", 10, 0.0, 200.0, "Selected_muon_loosett1l_subleading_pt"),
                ("Eta_Muons","Eta(muons); Eta(muons);Events;", 25, -2.5, 2.5, "Selected_muon_loosett1l_eta"),
                # ("Leading_Eta_Muons","Eta(\\mu_{1}); Eta(\\mu_{1});Events;", 25, -2.5, 2.5, "Selected_muon_loosett1l_leading_eta"),
                # ("Subleading_Eta_Muons","Eta(\\mu_{2}); Eta(\\mu_{2});Events;", 25, -2.5, 2.5, "Selected_muon_loosett1l_subleading_eta"),
                ("Phi_Muons","Phi(muons); Phi(muons);Events;", 35, -3.5, 3.5, "Selected_muon_loosett1l_phi"),
                # ("Charge_Muons","Charge(muons); Charge(muons);Events;", 5, -2.0, 3.0, "Selected_muon_loosett1l_charge"),
                # ("miniPFRelIso_all_Muons","Mini relative isolation(muons); Mini relative isolation(muons);Events;", 20, 0.0, 0.2, "Selected_muon_loosett1l_miniPFRelIso_all"),
                # ("Sip_Muons","SIP_{3D}(muons); SIP_{3D}(muons);Events;", 15, 0.0, 3.0, "Selected_muon_loosett1l_sip3d"),
                # ("Number_Leptons","Number(leptons); Number(leptons);Events;", 5, 0.0, 5.0, "Selected_lepton_loosett1l_number"),
                # ("Pt_Leptons","p_{T}(leptons); p_{T}(leptons) (GeV);Events;", 20, 0.0, 400.0, "Selected_lepton_loosett1l_pt"),
                # ("Leading_Pt_Leptons","p_{T}(l_{1}); p_{T}(l_{1}) (GeV);Events;", 20, 0.0, 400.0, "Selected_lepton_loosett1l_leading_pt"),
                # ("Subleading_Pt_Leptons","p_{T}(l_{2}); p_{T}(l_{2}) (GeV);Events;", 10, 0.0, 200.0, "Selected_lepton_loosett1l_subleading_pt"),
                ("Sum_Pt_Two_Leptons","p_{T}(l_{1}) + p_{T}(l_{2}); p_{T}(l_{1}) + p_{T}(l_{2}) (GeV);Events;", 30, 0.0, 600.0, "Selected_lepton_loosett1l_sum_two_leptons_pt"),
                # ("Eta_Leptons","Eta(leptons); Eta(leptons);Events;", 25, -2.5, 2.5, "Selected_lepton_loosett1l_eta"),
                # ("Leading_Eta_Leptons","Eta(l_{1}); Eta(l_{1});Events;", 25, -2.5, 2.5, "Selected_lepton_loosett1l_leading_eta"),
                # ("Subleading_Eta_Leptons","Eta(l_{2}); Eta(l_{2});Events;", 25, -2.5, 2.5, "Selected_lepton_loosett1l_subleading_eta"),
                # ("Phi_Leptons","Phi(leptons); Phi(leptons);Events;", 35, -3.5, 3.5, "Selected_lepton_loosett1l_phi"),
                # ("Charge_Leptons","Charge(leptons); Charge(leptons);Events;", 5, -2.0, 3.0, "Selected_lepton_loosett1l_charge"),
                # ("Deltaeta_Leptons","\\Delta \\eta (l_{1},l_{2}); \\Delta \\eta (l_{1},l_{2});Events;", 20, 0.0, 4.0, "Selected_lepton_loosett1l_deltaeta"),
                # ("Deltaphi_Leptons","\\Delta \\phi (l_{1},l_{2}); \\Delta \\phi (l_{1},l_{2});Events;", 20, 0.0, 4.0, "Selected_lepton_loosett1l_deltaphi"),
                ("DeltaR_Leptons","\\Delta R (l_{1},l_{2}); \\Delta R (l_{1},l_{2});Events;", 30, 0.0, 6.0, "Selected_lepton_loosett1l_deltaR"),
                # ("miniPFRelIso_all_Leptons","Mini relative isolation(leptons); Mini relative isolation(leptons);Events;", 20, 0.0, 0.2, "Selected_lepton_loosett1l_miniPFRelIso_all"),
                # ("Sip_Leptons","SIP_{3D}(leptons); SIP_{3D}(leptons);Events;", 15, 0.0, 3.0, "Selected_lepton_loosett1l_sip3d"),
                # ("Pt_Sum_Two_Leptons","p_{T}(l_{1}+l_{2}); p_{T}(l_{1}+l_{2}) (GeV);Events;", 30, 0.0, 600.0, "Vectorial_sum_two_leptons_loosett1l_pt"),
                # ("Mass_Sum_Two_Leptons","M(l_{1}+l_{2}); M(l_{1}+l_{2}) (GeV);Events;", 30, 0.0, 600.0, "Vectorial_sum_two_leptons_loosett1l_mass"),
                ("Number_Jets","Number(jets); Number(jets);Events;", 10, 0.0, 10.0, "Selected_clean_jet_loosett1l_number"),
                ("Pt_Jets","p_{T}(jets); p_{T}(jets) (GeV);Events;", 25, 0.0, 500.0, "Selected_clean_jet_loosett1l_pt"),
                # ("Leading_Pt_Jets","p_{T}(j_{1}); p_{T}(j_{1}) (GeV);Events;", 25, 0.0, 500.0, "Selected_clean_jet_loosett1l_leading_pt"),
                # ("Subleading_Pt_Jets","p_{T}(j_{2}); p_{T}(j_{2}) (GeV);Events;", 20, 0.0, 400.0, "Selected_clean_jet_loosett1l_subleading_pt"),
                # ("Subsubleading_Pt_Jets","p_{T}(j_{3}); p_{T}(j_{3}) (GeV);Events;", 15, 0.0, 300.0, "Selected_clean_jet_loosett1l_subsubleading_pt"),
                # ("Ht_Jets","Ht; Ht (GeV);Events;", 30, 0.0, 1200.0, "Selected_clean_jet_loosett1l_Ht"),
                ("Eta_Jets","Eta(jets); Eta(jets);Events;", 25, -2.5, 2.5, "Selected_clean_jet_loosett1l_eta"),
                ("Phi_Jets","Phi(jets); Phi(jets);Events;", 35, -3.5, 3.5, "Selected_clean_jet_loosett1l_phi"),
                # ("Mass_Jets","Mass(jets); Mass(jets) (GeV);Events;", 15, 0.0, 75.0, "Selected_clean_jet_loosett1l_mass"),
                # ("DeepB_Jets","DeepCSV(jets); DeepCSV(jets);Events;", 20, 0.0, 1.0, "Selected_clean_jet_loosett1l_btagDeepB"),
                # ("DeepFlavB_Jets","DeepJet(jets); DeepJet(jets);Events;", 20, 0.0, 1.0, "Selected_clean_jet_loosett1l_btagDeepFlavB"),
                ("Number_bJets","Number(bjets); Number(bjets);Events;", 5, 0.0, 5.0, "Selected_clean_bjet_loosett1l_number"),
                ("Pt_bJets","p_{T}(bjets); p_{T}(bjets) (GeV);Events;", 25, 0.0, 500.0, "Selected_clean_bjet_loosett1l_pt"),
                # ("Leading_Pt_bJets","p_{T}(b_{1}); p_{T}(b_{1}) (GeV);Events;", 25, 0.0, 500.0, "Selected_clean_bjet_loosett1l_leading_pt"),
                # ("Subleading_Pt_bJets","p_{T}(b_{2}); p_{T}(b_{2}) (GeV);Events;", 20, 0.0, 400.0, "Selected_clean_bjet_loosett1l_subleading_pt"),
                # ("Subsubleading_Pt_bJets","p_{T}(b_{3}); p_{T}(b_{3}) (GeV);Events;", 15, 0.0, 300.0, "Selected_clean_bjet_loosett1l_subsubleading_pt"),
                ("Eta_bJets","Eta(bjets); Eta(bjets);Events;", 25, -2.5, 2.5, "Selected_clean_bjet_loosett1l_eta"),
                ("Phi_bJets","Phi(bjets); Phi(bjets);Events;", 35, -3.5, 3.5, "Selected_clean_bjet_loosett1l_phi"),
                # ("Mass_bJets","Mass(bjets); Mass(bjets) (GeV);Events;", 15, 0.0, 75.0, "Selected_clean_bjet_loosett1l_mass"),
                # ("DeepB_bJets","DeepCSV(bjets); DeepCSV(bjets);Events;", 20, 0.0, 1.0, "Selected_clean_bjet_loosett1l_btagDeepB"),
                # ("DeepFlavB_bJets","DeepJet(bjets); DeepJet(bjets);Events;", 20, 0.0, 1.0, "Selected_clean_bjet_loosett1l_btagDeepFlavB"),
                # ("Mass_Sum_Three_Jets","M(3j); M(3j) (GeV);Events;", 15, 0.0, 3000.0, "Vectorial_sum_three_clean_jets_loosett1l_mass"),
                # ("Mass_Sum_Three_Jets_offset","M(3j) - m_{top}; M(3j) - m_{top} (GeV);Events;", 15, -200.0, 2800.0, "Vectorial_sum_three_clean_jets_loosett1l_mass_offset"),
                ("Mass_Sum_Three_Jets_min","Min(|M(3j) - m_{top}|); Min(|M(3j) - m_{top}|) (GeV);Events;", 30, 0.0, 500.0, "Vectorial_sum_three_clean_jets_loosett1l_mass_min"),
                # ("St","St_{lep}; St_{lep} (GeV);Events;", 16, 0.0, 2000.0, "St_loosett1l"),
                # ("St_with_MET","St; St (GeV);Events;", 16, 0.0, 2000.0, "St_with_MET_loosett1l"),
                # ("Mass_LeptonFromtop_bJetFromtop","M_{lb}; M_{lb} (GeV);Events;", 10, 0.0, 300.0, "Lepton2Fromtop_bJet1Fromtop_loosett1l_mass"),
                # ("Number_Electrons","Number(electrons); Number(electrons);Events;", 5, 0.0, 5.0, "Selected_electron_number"),
                # ("Pt_Electrons","p_{T}(electrons); p_{T}(electrons) (GeV);Events;", 20, 0.0, 400.0, "Electron_pt"),
                ("Pt_Electrons","p_{T}(electrons); p_{T}(electrons) (GeV);Events;", 20, 0.0, 400.0, "Selected_electron_pt"),
                # ("Leading_Pt_Electrons","p_{T}(e_{1}); p_{T}(e_{1}) (GeV);Events;", 20, 0.0, 400.0, "Selected_electron_leading_pt"),
                # ("Subleading_Pt_Electrons","p_{T}(e_{2}); p_{T}(e_{2}) (GeV);Events;", 10, 0.0, 200.0, "Selected_electron_subleading_pt"),
                ("Eta_Electrons","Eta(electrons); Eta(electrons);Events;", 25, -2.5, 2.5, "Selected_electron_eta"),
                # ("Leading_Eta_Electrons","Eta(e_{1}); Eta(e_{1});Events;", 25, -2.5, 2.5, "Selected_electron_leading_eta"),
                # ("Subleading_Eta_Electrons","Eta(e_{2}); Eta(e_{2});Events;", 25, -2.5, 2.5, "Selected_electron_subleading_eta"),
                ("Phi_Electrons","Phi(electrons); Phi(electrons);Events;", 35, -3.5, 3.5, "Selected_electron_phi"),
                # ("Charge_Electrons","Charge(electrons); Charge(electrons);Events;", 5, -2.0, 3.0, "Selected_electron_charge"),
                # ("miniPFRelIso_all_Electrons","Mini relative isolation(electrons); Mini relative isolation(electrons);Events;", 20, 0.0, 0.2, "Selected_electron_miniPFRelIso_all"),
                # ("Sip_Electrons","SIP_{3D}(electrons); SIP_{3D}(electrons);Events;", 10, 0.0, 2.0, "Selected_electron_sip3d"),
                # ("Number_Muons","Number(muons); Number(muons);Events;", 5, 0.0, 5.0, "Selected_muon_number"),
                # ("Pt_Muons","p_{T}(muons); p_{T}(muons) (GeV);Events;", 20, 0.0, 400.0, "Muon_pt"),
                ("Pt_Muons","p_{T}(muons); p_{T}(muons) (GeV);Events;", 20, 0.0, 400.0, "Selected_muon_pt"),
                # ("Leading_Pt_Muons","p_{T}(\\mu_{1}); p_{T}(\\mu_{1}) (GeV);Events;", 20, 0.0, 400.0, "Selected_muon_leading_pt"),
                # ("Subleading_Pt_Muons","p_{T}(\\mu_{2}); p_{T}(\\mu_{2}) (GeV);Events;", 10, 0.0, 200.0, "Selected_muon_subleading_pt"),
                ("Eta_Muons","Eta(muons); Eta(muons);Events;", 25, -2.5, 2.5, "Selected_muon_eta"),
                # ("Leading_Eta_Muons","Eta(\\mu_{1}); Eta(\\mu_{1});Events;", 25, -2.5, 2.5, "Selected_muon_leading_eta"),
                # ("Subleading_Eta_Muons","Eta(\\mu_{2}); Eta(\\mu_{2});Events;", 25, -2.5, 2.5, "Selected_muon_subleading_eta"),
                ("Phi_Muons","Phi(muons); Phi(muons);Events;", 35, -3.5, 3.5, "Selected_muon_phi"),
                # ("Charge_Muons","Charge(muons); Charge(muons);Events;", 5, -2.0, 3.0, "Selected_muon_charge"),
                # ("miniPFRelIso_all_Muons","Mini relative isolation(muons); Mini relative isolation(muons);Events;", 20, 0.0, 0.2, "Selected_muon_miniPFRelIso_all"),
                # ("Sip_Muons","SIP_{3D}(muons); SIP_{3D}(muons);Events;", 15, 0.0, 3.0, "Selected_muon_sip3d"),
                # ("Number_Leptons","Number(leptons); Number(leptons);Events;", 5, 0.0, 5.0, "Selected_lepton_number"),
                # ("Pt_Leptons","p_{T}(leptons); p_{T}(leptons) (GeV);Events;", 20, 0.0, 400.0, "Selected_lepton_pt"),
                # ("Leading_Pt_Leptons","p_{T}(l_{1}); p_{T}(l_{1}) (GeV);Events;", 20, 0.0, 400.0, "Selected_lepton_leading_pt"),
                # ("Subleading_Pt_Leptons","p_{T}(l_{2}); p_{T}(l_{2}) (GeV);Events;", 10, 0.0, 200.0, "Selected_lepton_subleading_pt"),
                ("Sum_Pt_Two_Leptons","p_{T}(l_{1}) + p_{T}(l_{2}); p_{T}(l_{1}) + p_{T}(l_{2}) (GeV);Events;", 30, 0.0, 600.0, "Selected_lepton_sum_two_leptons_pt"),
                # ("Eta_Leptons","Eta(leptons); Eta(leptons);Events;", 25, -2.5, 2.5, "Selected_lepton_eta"),
                # ("Leading_Eta_Leptons","Eta(l_{1}); Eta(l_{1});Events;", 25, -2.5, 2.5, "Selected_lepton_leading_eta"),
                # ("Subleading_Eta_Leptons","Eta(l_{2}); Eta(l_{2});Events;", 25, -2.5, 2.5, "Selected_lepton_subleading_eta"),
                # ("Phi_Leptons","Phi(leptons); Phi(leptons);Events;", 35, -3.5, 3.5, "Selected_lepton_phi"),
                # ("Charge_Leptons","Charge(leptons); Charge(leptons);Events;", 5, -2.0, 3.0, "Selected_lepton_charge"),
                # ("Deltaeta_Leptons","\\Delta \\eta (l_{1},l_{2}); \\Delta \\eta (l_{1},l_{2});Events;", 20, 0.0, 4.0, "Selected_lepton_deltaeta"),
                # ("Deltaphi_Leptons","\\Delta \\phi (l_{1},l_{2}); \\Delta \\phi (l_{1},l_{2});Events;", 20, 0.0, 4.0, "Selected_lepton_deltaphi"),
                ("DeltaR_Leptons","\\Delta R (l_{1},l_{2}); \\Delta R (l_{1},l_{2});Events;", 30, 0.0, 6.0, "Selected_lepton_deltaR"),
                # ("miniPFRelIso_all_Leptons","Mini relative isolation(leptons); Mini relative isolation(leptons);Events;", 20, 0.0, 0.2, "Selected_lepton_miniPFRelIso_all"),
                # ("Sip_Leptons","SIP_{3D}(leptons); SIP_{3D}(leptons);Events;", 15, 0.0, 3.0, "Selected_lepton_sip3d"),
                # ("Pt_Sum_Two_Leptons","p_{T}(l_{1}+l_{2}); p_{T}(l_{1}+l_{2}) (GeV);Events;", 30, 0.0, 600.0, "Vectorial_sum_two_leptons_pt"),
                # ("Mass_Sum_Two_Leptons","M(l_{1}+l_{2}); M(l_{1}+l_{2}) (GeV);Events;", 30, 0.0, 600.0, "Vectorial_sum_two_leptons_mass"),
                ("Number_Jets","Number(jets); Number(jets);Events;", 10, 0.0, 10.0, "Selected_clean_jet_number"),
                ("Pt_Jets","p_{T}(jets); p_{T}(jets) (GeV);Events;", 25, 0.0, 500.0, "Selected_clean_jet_pt"),
                # ("Leading_Pt_Jets","p_{T}(j_{1}); p_{T}(j_{1}) (GeV);Events;", 25, 0.0, 500.0, "Selected_clean_jet_leading_pt"),
                # ("Subleading_Pt_Jets","p_{T}(j_{2}); p_{T}(j_{2}) (GeV);Events;", 20, 0.0, 400.0, "Selected_clean_jet_subleading_pt"),
                # ("Subsubleading_Pt_Jets","p_{T}(j_{3}); p_{T}(j_{3}) (GeV);Events;", 15, 0.0, 300.0, "Selected_clean_jet_subsubleading_pt"),
                # ("Ht_Jets","Ht; Ht (GeV);Events;", 30, 0.0, 1200.0, "Selected_clean_jet_Ht"),
                ("Eta_Jets","Eta(jets); Eta(jets);Events;", 25, -2.5, 2.5, "Selected_clean_jet_eta"),
                ("Phi_Jets","Phi(jets); Phi(jets);Events;", 35, -3.5, 3.5, "Selected_clean_jet_phi"),
                # ("Mass_Jets","Mass(jets); Mass(jets) (GeV);Events;", 15, 0.0, 75.0, "Selected_clean_jet_mass"),
                # ("DeepB_Jets","DeepCSV(jets); DeepCSV(jets);Events;", 20, 0.0, 1.0, "Selected_clean_jet_btagDeepB"),
                # ("DeepFlavB_Jets","DeepJet(jets); DeepJet(jets);Events;", 20, 0.0, 1.0, "Selected_clean_jet_btagDeepFlavB"),
                ("Number_bJets","Number(bjets); Number(bjets);Events;", 5, 0.0, 5.0, "Selected_clean_bjet_number"),
                ("Pt_bJets","p_{T}(bjets); p_{T}(bjets) (GeV);Events;", 25, 0.0, 500.0, "Selected_clean_bjet_pt"),
                # ("Leading_Pt_bJets","p_{T}(b_{1}); p_{T}(b_{1}) (GeV);Events;", 25, 0.0, 500.0, "Selected_clean_bjet_leading_pt"),
                # ("Subleading_Pt_bJets","p_{T}(b_{2}); p_{T}(b_{2}) (GeV);Events;", 20, 0.0, 400.0, "Selected_clean_bjet_subleading_pt"),
                # ("Subsubleading_Pt_bJets","p_{T}(b_{3}); p_{T}(b_{3}) (GeV);Events;", 15, 0.0, 300.0, "Selected_clean_bjet_subsubleading_pt"),
                ("Eta_bJets","Eta(bjets); Eta(bjets);Events;", 25, -2.5, 2.5, "Selected_clean_bjet_eta"),
                ("Phi_bJets","Phi(bjets); Phi(bjets);Events;", 35, -3.5, 3.5, "Selected_clean_bjet_phi"),
                # ("Mass_bJets","Mass(bjets); Mass(bjets) (GeV);Events;", 15, 0.0, 75.0, "Selected_clean_bjet_mass"),
                # ("DeepB_bJets","DeepCSV(bjets); DeepCSV(bjets);Events;", 20, 0.0, 1.0, "Selected_clean_bjet_btagDeepB"),
                # ("DeepFlavB_bJets","DeepJet(bjets); DeepJet(bjets);Events;", 20, 0.0, 1.0, "Selected_clean_bjet_btagDeepFlavB"),
                # ("Mass_Sum_Three_Jets","M(3j); M(3j) (GeV);Events;", 15, 0.0, 1000.0, "Vectorial_sum_three_clean_jets_mass"),
                # ("Mass_Sum_Three_Jets_offset","M(3j) - m_{top}; M(3j) - m_{top} (GeV);Events;", 15, -200.0, 800.0, "Vectorial_sum_three_clean_jets_mass_offset"),
                ("Mass_Sum_Three_Jets_min","Min(|M(3j) - m_{top}|); Min(|M(3j) - m_{top}|) (GeV);Events;", 30, 0.0, 500.0, "Vectorial_sum_three_clean_jets_mass_min"),
                ("Pt_MET","p_{T}(MET); p_{T}(MET) (GeV);Events;", 20, 0.0, 400.0, "MET_pt_corr"),
                ("Phi_MET","Phi(MET); Phi(MET) (GeV);Events;", 35, -3.5, 3.5, "MET_phi_corr"),
                # ("St","St_{lep}; St_{lep} (GeV);Events;", 16, 0.0, 2000.0, "St"),
                # ("St_with_MET","St; St (GeV);Events;", 16, 0.0, 2000.0, "St_with_MET"),
                # ("Mass_LeptonFromtop_bJetFromtop","M_{lb}; M_{lb} (GeV);Events;", 10, 0.0, 300.0, "Lepton2Fromtop_bJet1Fromtop_mass")
        ],

        'weights': [],

        'Cuts': [
                'mumu_SR',
                'mumu_MR_tt2l',
                'mumu_MR_tt1l_SR',
                'mumu_MR_tt1l_CR_ttX',
                'mumu_CR_ttX',
                'muel_SR',
                'muel_MR_tt2l',
                'muel_MR_tt1l_SR',
                'muel_MR_tt1l_CR_ttX',
                'muel_CR_ttX',
                'elel_SR',
                'elel_MR_tt2l',
                'elel_MR_tt1l_SR',
                'elel_MR_tt1l_CR_ttX',
                'elel_CR_ttX',
                'Total_SR',
                'Total_MR_tt2l',
                'Total_MR_tt1l_SR',
                'Total_MR_tt1l_CR_ttX',
                'Total_CR_ttX',
        ],

        'Masses': [
                '600',
                '625',
                '650',
                '675',
                '700',
                '800',
                '900',
                '1000',
                '1100',
                '1200',
        ],

        'getQuantiles_ptleptons': [
                '7.481+0.3147*Tprime_transverse_mass_first_600-0.0001024*Tprime_transverse_mass_first_600*Tprime_transverse_mass_first_600',
                '6.352+0.3144*Tprime_transverse_mass_first_625-0.0001015*Tprime_transverse_mass_first_625*Tprime_transverse_mass_first_625',
                '7.024+0.3087*Tprime_transverse_mass_first_650-0.00009726*Tprime_transverse_mass_first_650*Tprime_transverse_mass_first_650',
                '7.571+0.2943*Tprime_transverse_mass_first_675-0.00009941*Tprime_transverse_mass_first_675*Tprime_transverse_mass_first_675',
                '6.266+0.2944*Tprime_transverse_mass_first_700-0.00009876*Tprime_transverse_mass_first_700*Tprime_transverse_mass_first_700',
                '4.364+0.2879*Tprime_transverse_mass_first_800-0.00009266*Tprime_transverse_mass_first_800*Tprime_transverse_mass_first_800',
                '12+0.2394*Tprime_transverse_mass_first_900-0.00006813*Tprime_transverse_mass_first_900*Tprime_transverse_mass_first_900',
                '9.308+0.2175*Tprime_transverse_mass_first_1000-0.00005941*Tprime_transverse_mass_first_1000*Tprime_transverse_mass_first_1000',
                '6.41+0.2155*Tprime_transverse_mass_first_1100-0.00005653*Tprime_transverse_mass_first_1100*Tprime_transverse_mass_first_1100',
                '15.3+0.1754*Tprime_transverse_mass_first_1200-0.00004224*Tprime_transverse_mass_first_1200*Tprime_transverse_mass_first_1200',
        ],

        'getQuantiles_massjets': [
                '14.76+0.04216*Tprime_transverse_mass_first_600-0.00001469*Tprime_transverse_mass_first_600*Tprime_transverse_mass_first_600',
                '14.98+0.04069*Tprime_transverse_mass_first_625-0.00001359*Tprime_transverse_mass_first_625*Tprime_transverse_mass_first_625',
                '14.18+0.04237*Tprime_transverse_mass_first_650-0.0000146*Tprime_transverse_mass_first_650*Tprime_transverse_mass_first_650',
                '19.14+0.03011*Tprime_transverse_mass_first_675-0.000007073*Tprime_transverse_mass_first_675*Tprime_transverse_mass_first_675',
                '18.59+0.03115*Tprime_transverse_mass_first_700-0.000007559*Tprime_transverse_mass_first_700*Tprime_transverse_mass_first_700',
                '-3.666+0.07098*Tprime_transverse_mass_first_800-0.00003138*Tprime_transverse_mass_first_800*Tprime_transverse_mass_first_800',
                '-3.582+0.07166*Tprime_transverse_mass_first_900-0.0000324*Tprime_transverse_mass_first_900*Tprime_transverse_mass_first_900',
                '-3.354+0.07061*Tprime_transverse_mass_first_1000-0.00003184*Tprime_transverse_mass_first_1000*Tprime_transverse_mass_first_1000',
                '-5.681+0.07559*Tprime_transverse_mass_first_1100-0.00003425*Tprime_transverse_mass_first_1100*Tprime_transverse_mass_first_1100',
                '0.4488+0.05818*Tprime_transverse_mass_first_1200-0.0000234*Tprime_transverse_mass_first_1200*Tprime_transverse_mass_first_1200',
        ],

        'getQuantiles_ptleptons_loosett1l': [
                '7.481+0.3147*Tprime_transverse_mass_first_loosett1l_600-0.0001024*Tprime_transverse_mass_first_loosett1l_600*Tprime_transverse_mass_first_loosett1l_600',
                '6.352+0.3144*Tprime_transverse_mass_first_loosett1l_625-0.0001015*Tprime_transverse_mass_first_loosett1l_625*Tprime_transverse_mass_first_loosett1l_625',
                '7.024+0.3087*Tprime_transverse_mass_first_loosett1l_650-0.00009726*Tprime_transverse_mass_first_loosett1l_650*Tprime_transverse_mass_first_loosett1l_650',
                '7.571+0.2943*Tprime_transverse_mass_first_loosett1l_675-0.00009941*Tprime_transverse_mass_first_loosett1l_675*Tprime_transverse_mass_first_loosett1l_675',
                '6.266+0.2944*Tprime_transverse_mass_first_loosett1l_700-0.00009876*Tprime_transverse_mass_first_loosett1l_700*Tprime_transverse_mass_first_loosett1l_700',
                '4.364+0.2879*Tprime_transverse_mass_first_loosett1l_800-0.00009266*Tprime_transverse_mass_first_loosett1l_800*Tprime_transverse_mass_first_loosett1l_800',
                '12+0.2394*Tprime_transverse_mass_first_loosett1l_900-0.00006813*Tprime_transverse_mass_first_loosett1l_900*Tprime_transverse_mass_first_loosett1l_900',
                '9.308+0.2175*Tprime_transverse_mass_first_loosett1l_1000-0.00005941*Tprime_transverse_mass_first_loosett1l_1000*Tprime_transverse_mass_first_loosett1l_1000',
                '6.41+0.2155*Tprime_transverse_mass_first_loosett1l_1100-0.00005653*Tprime_transverse_mass_first_loosett1l_1100*Tprime_transverse_mass_first_loosett1l_1100',
                '15.3+0.1754*Tprime_transverse_mass_first_loosett1l_1200-0.00004224*Tprime_transverse_mass_first_loosett1l_1200*Tprime_transverse_mass_first_loosett1l_1200',
        ],

        'getQuantiles_massjets_loosett1l': [
                '14.76+0.04216*Tprime_transverse_mass_first_loosett1l_600-0.00001469*Tprime_transverse_mass_first_loosett1l_600*Tprime_transverse_mass_first_loosett1l_600',
                '14.98+0.04069*Tprime_transverse_mass_first_loosett1l_625-0.00001359*Tprime_transverse_mass_first_loosett1l_625*Tprime_transverse_mass_first_loosett1l_625',
                '14.18+0.04237*Tprime_transverse_mass_first_loosett1l_650-0.0000146*Tprime_transverse_mass_first_loosett1l_650*Tprime_transverse_mass_first_loosett1l_650',
                '19.14+0.03011*Tprime_transverse_mass_first_loosett1l_675-0.000007073*Tprime_transverse_mass_first_loosett1l_675*Tprime_transverse_mass_first_loosett1l_675',
                '18.59+0.03115*Tprime_transverse_mass_first_loosett1l_700-0.000007559*Tprime_transverse_mass_first_loosett1l_700*Tprime_transverse_mass_first_loosett1l_700',
                '-3.666+0.07098*Tprime_transverse_mass_first_loosett1l_800-0.00003138*Tprime_transverse_mass_first_loosett1l_800*Tprime_transverse_mass_first_loosett1l_800',
                '-3.582+0.07166*Tprime_transverse_mass_first_loosett1l_900-0.0000324*Tprime_transverse_mass_first_loosett1l_900*Tprime_transverse_mass_first_loosett1l_900',
                '-3.354+0.07061*Tprime_transverse_mass_first_loosett1l_1000-0.00003184*Tprime_transverse_mass_first_loosett1l_1000*Tprime_transverse_mass_first_loosett1l_1000',
                '-5.681+0.07559*Tprime_transverse_mass_first_loosett1l_1100-0.00003425*Tprime_transverse_mass_first_loosett1l_1100*Tprime_transverse_mass_first_loosett1l_1100',
                '0.4488+0.05818*Tprime_transverse_mass_first_loosett1l_1200-0.0000234*Tprime_transverse_mass_first_loosett1l_1200*Tprime_transverse_mass_first_loosett1l_1200',
        ]
}

# Define the initial systematic variations and other weights
initial_weights = [
        "syst_elec_hltUp", "syst_elec_hltDown", "stat_elec_hlt_2016Up", "stat_elec_hlt_2016Down", 
        "syst_elec_recoUp", "syst_elec_recoDown", "syst_elec_idUp", "syst_elec_idDown", 
        "syst_muon_hltUp", "syst_muon_hltDown", "stat_muon_hlt_2016Up", "stat_muon_hlt_2016Down", 
        "syst_muon_recoUp", "syst_muon_recoDown", "stat_muon_reco_2016Up", "stat_muon_reco_2016Down", 
        "syst_muon_idUp", "syst_muon_idDown", "stat_muon_id_2016Up", "stat_muon_id_2016Down", 
        "syst_muon_isoUp", "syst_muon_isoDown", "stat_muon_iso_2016Up", "stat_muon_iso_2016Down", 
        "syst_puUp", "syst_puDown",
        "syst_b_correlatedUp", "syst_b_correlatedDown", "syst_b_uncorrelated_2016preVFPUp", "syst_b_uncorrelated_2016preVFPDown",
        "syst_l_correlatedUp",  "syst_l_correlatedDown", "syst_l_uncorrelated_2016preVFPUp", "syst_l_uncorrelated_2016preVFPDown", 
        "syst_prefiringUp", "syst_prefiringDown", "syst_pileupjetidUp", "syst_pileupjetidDown",
        "syst_pt_topUp", "syst_pt_topDown"
]

# Add the scale and PS weights
scale_ps_weights = [
        "syst_mescaleUp", "syst_mescaleDown",
        "syst_renscaleUp", "syst_renscaleDown",
        "syst_facscaleUp", "syst_facscaleDown",
        "syst_isrUp", "syst_isrDown",
        "syst_fsrUp",  "syst_fsrDown",
]

# Combine the initial weights and scale/PS weights
config['weights'] = initial_weights + scale_ps_weights

# Add PDF weight variations
pdf_weights = ["syst_pdf{}".format(i) for i in range(1, 103)]
config['weights'].extend(pdf_weights)

# Add specific PDF alpha_s variations
config['weights'].extend(["syst_pdfalphasUp", "syst_pdfalphasDown"])

# processing options
procflags = {
        # how many jobs?
        'split': 'Max',
        # if False, one output file per input file, if True then one output file for everything
        'allinone': False,
        # if True then skip existing analyzed files
        'skipold': True,
        # travel through the subdirectories and their subdirecties when processing.
        # becareful not to mix MC and real DATA in them.
        'recursive': True,
        # if False then only selected branches which is done in the .cpp file will be saved
        'saveallbranches': False,
        #How many input files?
        'nrootfiles': 10000,
        }