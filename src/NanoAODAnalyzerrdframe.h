/*
 * NanoAODAnalyzerrdframe.h
 *
 *  Created on: Sep 30, 2018
 *      Author: suyong
 */

#ifndef NANOAODANALYZERRDFRAME_H_
#define NANOAODANALYZERRDFRAME_H_

#include "TTree.h"
#include "TFile.h"

#include "ROOT/RDataFrame.hxx"
#include "ROOT/RVec.hxx"

//#include "Math/Vector4D.h"

#include "correction.h"

#include <string>
#include <vector>
#include <map>
//#include "rapidjson/document.h"
#include "nlohmann/json.hpp"

#include "utility.h" // floats, etc are defined here
#include "RNodeTree.h"
#include "TCut.h"
#include "TRandom.h"
#include "TRandom3.h"
#include "RoccoR.h"

using namespace ROOT::RDF;
using namespace std;

class TH1D;

using json = nlohmann::json;

class NanoAODAnalyzerrdframe {
	using RDF1DHist = RResultPtr<TH1D>;
	using RDF2DHist = RResultPtr<TH2D>;
public:
	NanoAODAnalyzerrdframe(string infilename, string intreename, string outfilename);
	NanoAODAnalyzerrdframe(TTree *t, string outfilename);
	virtual ~NanoAODAnalyzerrdframe();


	virtual void setupAnalysis();
	virtual void setTree(TTree *t, string outfilename);

	// object selectors
	// RNode is in namespace ROOT::RDF
	bool readgoodjson(string goodjsonfname); // get ready for applying golden JSON
	void selectFatJets();
	void removeHEMEvents();
	void setupCorrections(string goodjsonfname, string pufname, string putag, string btvfname, string btvtype, string fname_btagEff, string hname_Loose_btagEff_bcflav, string hname_Loose_btagEff_lflav, string hname_Medium_btagEff_bcflav, string hname_Medium_btagEff_lflav, string hname_Tight_btagEff_bcflav, string hname_Tight_btagEff_lflav, string pileupjetidfname, string fname_pileupjetidEff, string hname_Loose_pileupjetidEff, string hname_Medium_pileupjetidEff, string hname_Tight_pileupjetidEff, string muon_roch_fname, string muon_fname, string muonisotype_loosett1l, string muonhlttype, string muonrecotype, string muonidtype, string muonisotype, string electron_fname, string electron_id_type_loosett1l, string electron_reco_type, string electron_id_type, string fname_electriggerEff, string hname_electriggerEff_eff, string hname_electriggerEff_statdata, string hname_electriggerEff_statmc, string hname_electriggerEff_systmc, string jercfname, string jerctag, std::vector<std::string> jercunctag, string jertag, string fname_metphimod);
	//void setupJetMETCorrection(string fname, string jettag, const std::vector<std::string> _jercunctag, string jertag, string fname_metphimod);
	void setupJetMETCorrection(const string& fname, const string& jettag, const vector<string>& jercUncTags, const string& jertag, const string& fname_metphimod);
	void applyJetMETCorrections();
	void defineJESUncRegion(const string& pt_cut = "");
	void applyMETPhiCorrections();
	//virtual void applyJetMETCorrections();
	void applyMuPtCorrection();
	void addVar(varinfo v);

	// define variables
	template <typename T, typename std::enable_if<!std::is_convertible<T, string>::value, int>::type = 0>
	void defineVar(string varname, T function,  const RDFDetail::ColumnNames_t &columns = {})
	{
		_rlm = _rlm.Define(varname, function, columns);
	};

	void addVartoStore(string varname);
	void addCuts(string cut, string idx);
	void add1DHist(TH1DModel histdef, string variable, string weight, string mincutstep="");
	void add2DHist(TH2DModel histdef, string variable1, string variable2, string weight, string mincutstep="");
	void add2DHist_vbin(TH2DModel histdef, string variable1, string variable2, string weight, string mincutstep="");
	double getBTaggingEff(double hadflav, double eta, double pt, std::string _BTaggingWP);
	double getPileupJetIDEff(double eta, double pt, std::string _PileupJetIDWP);
	double getEleTriggerEff(double eta, double pt, std::string variation);
	ROOT::RDF::RNode calculateBTagSF(RNode _rlm, std::vector<std::string> Jets_vars_names, const int&_case, const double btag_cut, std::string _BTaggingWP = "M", std::string output_var = "btag_SF_", const int& redefine = 0);
	ROOT::RDF::RNode calculatePileupJetIDSF(RNode _rlm, std::vector<std::string> Jets_vars_names, std::string year, std::string _PileupJetIDWP = "T", std::string output_var = "pileupjetid_SF_", const int&_redefine = 0);
	ROOT::RDF::RNode calculateMuSF(RNode _rlm, std::vector<std::string> Muon_vars, std::string output_var = "muon_SF_");
	ROOT::RDF::RNode calculateMuSF_loosett1l(RNode _rlm, std::vector<std::string> Muon_vars, std::string output_var = "muon_loosett1l_SF_");
	ROOT::RDF::RNode calculateEleSF(RNode _rlm, std::vector<std::string> Ele_vars, std::string output_var = "ele_SF_");
	ROOT::RDF::RNode calculateEleSF_loosett1l(RNode _rlm, std::vector<std::string> Ele_vars, std::string output_var = "ele_loosett1l_SF_");
	ROOT::RDF::RNode applyPrefiringWeight(RNode _rlm, std::string output_var="prefiring_SF_");

	void topPtReweight(std::string process);
	string jes_unc_level_start_idx;
	void setupCuts_and_Hists(std::string jes_unc_level_start_idx);
	void drawHists(RNode t);
	void run(bool saveAll=true, string outtreename="outputTree", std::string process = "none", bool Hist="False", bool JEC="False");
	void setupTree();

	//setting parameters for nanoaod
	void setParams(string year, string runtype, int datatype, bool Data);
	string _year;
	string _runtype;
	int _datatype;
	bool _isUL =false;
	bool _isReReco = false;
	bool _isData;
	bool Data;
	TTree* _atree;
	bool debug = true;
	//floats PDFWeights;

	//initialize HLT names
	std::string ctrlBranchName(string str_Branch);
	std::string setHLT_muons(string str_HLT = "" );
	std::string setHLT_elec(string str_HLT = "" );
	std::vector< std::string > HLTGlobalNames;
	std::vector< std::string > HLT2016Names_muons;
	std::vector< std::string > HLT2017Names_muons;
	std::vector< std::string > HLT2018Names_muons;
	std::vector< std::string > HLT2016Names_elec;
	std::vector< std::string > HLT2017Names_elec;
	std::vector< std::string > HLT2018Names_elec;

	//initialize object IDs
	std::string ElectronID(int cutbasedID);
	std::string MuonID(int cutbasedID);
	std::string JetID(int cutbasedID);
	
	//private:
	ROOT::RDataFrame _rd;
	
	//bool _isData;
	bool _jsonOK;
	string _outfilename;
	string _jsonfname;
	string _jerctag;
	std::vector<std::string> _jercunctag;
	string _putag;
	string _btvtype;
	string _muon_loosett1l_iso_type;
	string _muon_hlt_type;
	string _muon_reco_type;
	string _muon_id_type;
	string _muon_iso_type;
	string _electron_loosett1l_id_type;
	string _electron_reco_type;
	string _electron_id_type;
	TFile *_outrootfile;
	vector<string> _outrootfilenames;
	RNode _rlm;
	
	map<string, RDF1DHist> _th1dhistos;
	bool helper_1DHistCreator(string hname, string title, const int nbins, const double xlow, const double xhi, string rdfvar, string evWeight, RNode *anode);
	vector<hist1dinfo> _hist1dinfovector;
	
	//for 2D histograms
	map<string, RDF2DHist> _th2dhistos;
	bool helper_2DHistCreator(string hname, string title, const int nbinsx, const double xlow, const double xhi, const int nbinsy, const double ylow, const double yhi, string rdfvarx, string rdfvary, string evWeight, RNode *anode);
	vector<hist2dinfo> _hist2dinfovector;

	//for 2D histograms with variable binning
	map<string, RDF2DHist> _th2dvbinhistos;
	bool helper_2DHistCreator_vbin(std::string hname, std::string title, const int nbinsx, const double *xbins, const int nbinsy, const double *ybins,std::string rdfvarx,std::string rdfvary, std::string evWeight, RNode *anode);
	vector<hist2dinfo> _hist2dvbininfovector;

	vector<string> _originalvars;
	vector<string> _selections;
	
	vector<varinfo> _varinfovector;
	vector<cutinfo> _cutinfovector;

	vector<string> _varstostore;
	map<string, vector<std::string>> _varstostorepertree;

	json jsonroot;
	// pile up weights
	std::unique_ptr<correction::CorrectionSet> _correction_pu;

	//muon correction
	std::unique_ptr<correction::CorrectionSet> _correction_muon ;
	RoccoR _Roch_corr;

	//electron correction
	std::unique_ptr<correction::CorrectionSet> _correction_electron ;

	// JERC scale factors
	std::unique_ptr<correction::CorrectionSet> _correction_jerc; // json containing all forms of corrections and uncertainties
	std::shared_ptr<const correction::CompoundCorrection> _jetCorrector; // just the combined L1L2L3 correction
	vector<std::shared_ptr<const correction::Correction>> _jetCorrectionUnc; // for uncertainty corresponding to the jet corrector

	std::shared_ptr<const correction::Correction> _jetResCorrector; 
	std::shared_ptr<const correction::Correction> _jetResCorrector_SF; 

	std::unique_ptr<correction::CorrectionSet> _metphiCorrector; // json containing met phi mod correction
	// btag correction
	std::unique_ptr<correction::CorrectionSet> _correction_btag1;
	std::unique_ptr<correction::CorrectionSet> _correction_pileupjetid;
	TFile *f_electriggerEff;
	TH2F *hist_electriggerEff_eff;
	TH2F *hist_electriggerEff_statdata;
	TH2F *hist_electriggerEff_statmc;
	TH2F *hist_electriggerEff_systmc;
	TFile *f_btagEff;
	TH2D *hist_Loose_btagEff_bcflav;
	TH2D *hist_Loose_btagEff_lflav;
	TH2D *hist_Medium_btagEff_bcflav;
	TH2D *hist_Medium_btagEff_lflav;
	TH2D *hist_Tight_btagEff_bcflav;
	TH2D *hist_Tight_btagEff_lflav;
	TFile *f_pileupjetidEff;
	TH2D *hist_Loose_pileupjetidEff;
	TH2D *hist_Medium_pileupjetidEff;
	TH2D *hist_Tight_pileupjetidEff;

	RNodeTree _rnt;

	bool isDefined(string v);
	floats PDFWeights;
	floats PSWeights;
	floats ScaleWeights;
	ROOT::RDF::RNode addTheorySystematics(RNode _rlm);
};

#endif /* NANOAODANALYZERRDFRAME_H_ */
