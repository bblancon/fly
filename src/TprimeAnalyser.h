/*
 * TprimeAnalyser.h
 *
 *  Created on: May 6, 2022
 *      Author: suyong
 *		Developper: cdozen
 */

#ifndef BASEANALYSER_H_
#define BASEANALYSER_H_

#include "NanoAODAnalyzerrdframe.h"

class TprimeAnalyser: public NanoAODAnalyzerrdframe
{

  	public:
  	TprimeAnalyser(TTree *t, std::string outfilename);
  	void defineCuts(std::string year, std::string process, bool BTagEff, bool Hist, bool Combine, bool JEC, const std::vector<std::string>& getQuantiles_ptleptons, const std::vector<std::string>& getQuantiles_massjets, const std::vector<std::string>& getQuantiles_ptleptons_loosett1l, const std::vector<std::string>& getQuantiles_massjets_loosett1l); //define a series of cuts from defined variables only. you must implement this in your subclassed analysis
	void selectElectrons(std::string year);
  	void selectMuons(std::string year);
  	void selectLeptons();
	void selectReconstructedLeptons();
	void selectJets(std::string year);
	void selectReconstructedJets();
	void selectMET();
	void calculateEvWeight(std::string year, std::string process, const std::vector<std::string>& weights);
	void defineMoreVars(); //define higher-level variables from basic ones, you must implement this in your subclassed analysis code
  	void storingVariables(std::string year, bool JEC);
	ROOT::RDF::RNode redefineVars(RNode _rlm, std::string year, std::string process);
	void bookHists(std::string year, std::string process, bool BTagEff, bool Combine, bool JEC, const std::vector<std::tuple<std::string, std::string, int, double, double, std::string>>& variables_MC, const std::vector<std::tuple<std::string, std::string, int, double, double, std::string>>& variables, const std::vector<std::string>& weights); //book histograms, you must implement this in your subclassed analysis code
  	void setupCuts_and_Hists(std::string year, std::string process, bool BTagEff, bool Hist, bool JEC, const std::vector<std::string>& Cuts, const std::vector<std::string>& Masses, std::string second_cut);
	void setupObjects(std::string year, std::string process, const std::vector<std::string>& weights);
  	void setupAnalysis(std::string year, std::string process, bool BTagEff, bool Hist, bool Combine, bool JEC, const std::vector<std::tuple<std::string, std::string, int, double, double, std::string>>& variables_MC, const std::vector<std::tuple<std::string, std::string, int, double, double, std::string>>& variables, const std::vector<std::string>& weights, const std::vector<std::string>& Cuts, const std::vector<std::string>& Masses, const std::vector<std::string>& getQuantiles_ptleptons, const std::vector<std::string>& getQuantiles_massjets, const std::vector<std::string>& getQuantiles_ptleptons_loosett1l, const std::vector<std::string>& getQuantiles_massjets_loosett1l, std::string second_cut);

 	bool debug = true;
	bool _jsonOK;
	string _outfilename;
	string _putag;
	TFile *_outrootfile;
	vector<string> _outrootfilenames;
};

#endif /* BASEANALYSER_H_ */
