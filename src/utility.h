/*
 * utility.h
 *
 *  Created on: Dec 4, 2018
 *      Author: suyong
 */

#ifndef UTILITY_H_
#define UTILITY_H_

#include "ROOT/RDataFrame.hxx"
#include "ROOT/RVec.hxx"
#include "Math/Vector4D.h"
#include "correction.h"
#include "TTree.h"
#include "TFile.h"
#include "TMath.h"
#include <iostream>
#include <string>
using namespace ROOT;
using namespace ROOT::VecOps;
using namespace std;
#include <TLorentzVector.h>

using floats =  ROOT::VecOps::RVec<float>;
using doubles =  ROOT::VecOps::RVec<double>;
using ints =  ROOT::VecOps::RVec<int>;
using bools = ROOT::VecOps::RVec<bool>;
using uchars = ROOT::VecOps::RVec<unsigned char>;

using FourVector = ROOT::Math::PtEtaPhiMVector;
using FourVectorVec = std::vector<FourVector>;
using FourVectorRVec = ROOT::VecOps::RVec<FourVector>;

struct hist1dinfo
{
  ROOT::RDF::TH1DModel hmodel;
  std::string varname;
  std::string weightname;
  std::string mincutstep;
} ;

//for 2D histograms
struct hist2dinfo
{
  ROOT::RDF::TH2DModel hmodel;
  std::string varname1;
  std::string varname2;
  std::string weightname;
  std::string mincutstep;
} ;

struct varinfo
{
  std::string varname;
  std::string vardefinition;
  std::string mincutstep;
};

struct cutinfo
{
  std::string cutdefinition;
  std::string idx;
};

float pucorrection(std::unique_ptr<correction::CorrectionSet> &cset, std::string name, std::string syst, float ntruepileup);

floats weightv(floats &x, float evWeight);
floats sphericity(FourVectorVec &p);
double foxwolframmoment(int l, FourVectorVec &p, int minj=0, int maxj=-1);
floats chi2(float smtop_mass, float smw_mass, float lfvtop_mass);
floats top_reconstruction_whad(FourVectorVec &jets, FourVectorVec &bjets, FourVectorVec &muons);
floats w_reconstruction (FourVectorVec &jets);
floats compute_DR (FourVectorVec &muons, ints goodMuons_charge);
floats H_reconstruction (FourVectorVec &jets, FourVectorVec &bjets);
floats dimuon (FourVectorVec &muons, ints Muon_charge);
floats sort_discriminant( floats &discr, floats &obj );
FourVector select_leadingvec( FourVectorVec &v );
void PrintVector(floats &myvector);
ints pTcounter(floats vec);

floats ratio(floats &numerator, floats &denominator);
floats ptrel_numerator(floats &lpx, floats &lpy, floats &lpz, floats &jpx, floats &jpy, floats &jpz);
floats ptrel_denominator(floats &lpx, floats &lpy, floats &lpz, floats &jpx, floats &jpy, floats &jpz);
bools electron_isolated_2016(floats &I1, floats &I2, floats &I3, floats &lpt, floats &ltemppt, floats &leta, floats &ltempeta);
bools electron_isolated_2017_2018(floats &I1, floats &I2, floats &I3, floats &lpt, floats &ltemppt, floats &leta, floats &ltempeta);
bools muon_isolated_2016(floats &I1, floats &I2, floats &I3, floats &lpt, floats &ltemppt, floats &leta, floats &ltempeta);
bools muon_isolated_2017_2018(floats &I1, floats &I2, floats &I3, floats &lpt, floats &ltemppt, floats &leta, floats &ltempeta);
bool false_mass_dilepton (FourVectorVec &leptons);
FourVectorVec generate_4vec(floats &pt, floats &eta, floats &phi, floats &mass);
FourVectorVec generate_4vec_2(float pt, float eta, float phi, float mass);
FourVectorVec generate_4vec_3(floats &pt, floats &eta, floats &phi);
FourVectorVec generate_4vec_4(float pt, float eta, float phi);
FourVectorVec generate_4vec_5(floats pt, double eta, floats phi);
FourVectorRVec generate_4Rvec(floats &pt, floats &eta, floats &phi, floats &mass);
FourVectorRVec generate_4Rvec_2(double pt, double eta, double phi, double mass);
FourVectorRVec generate_4Rvec_4(float pt, float eta, float phi);
float FourVecPt(FourVector fv);
float FourVecEta(FourVector fv);
float FourVecPhi(FourVector fv);
float FourVecMass(FourVector fv);
float FourVecEnergy(FourVector fv);
float FourVecPx(FourVector fv);
float FourVecPy(FourVector fv);
float FourVecPz(FourVector fv);
FourVectorVec genmet4vec(float met_pt, float met_phi);
float calculate_deltaEta( FourVector &p1, FourVector &p2);
float calculate_deltaPhi( FourVector &p1, FourVector &p2);
float calculate_deltaR( FourVector &p1, FourVector &p2);
float calculate_invMass( FourVector &p1, FourVector &p2);
bools CheckOverlaps(const FourVectorVec &jets, const FourVectorVec &leps);
RVec< RVec<int> > distinct_comb_3(int n);
// FourVectorRVec Threejets (FourVectorRVec jet1, FourVectorRVec jet2, FourVectorRVec jet3, int jet_number, int bjet_number);
FourVectorRVec Threejets(FourVectorVec &jet4vecs, floats jeteta, floats jetphi, int jet_number, int bjet_number, double bjeteta, double bjetphi);
// FourVectorRVec Twojets (FourVectorRVec jet1, FourVectorRVec jet2, FourVectorRVec jet3, floats &jet1_btag, floats &jet2_btag, floats &jet3_btag, int jet_number, int bjet_number);

RVec< RVec<int> > distinct_comb_1(int nn);
RVec< RVec<int> > distinct_comb_1_even(int nn);
RVec< RVec<int> > distinct_comb_1_odd(int nn);
RVec< RVec<int> > distinct_comb_2(int nl, int nj);
RVec< RVec<int> > distinct_comb_2_bis(int nj);
RVec< RVec<int> > distinct_comb_2_even_odd(int nn);
floats DR_1 (floats &eta1, floats &eta2, floats &phi1, floats &phi2);
floats DR_2 (double &eta1, floats &eta2, double &phi1, floats &phi2);
int maxDR (double &eta1, floats &eta2, double &phi1, floats &phi2);
ints minDR_1 (floats &eta1, floats &eta2, floats &phi1, floats &phi2);
ints minDR_2 (floats &eta1, floats &eta2, floats &phi1, floats &phi2);
int minDR_3 (double &eta1, floats &eta2, double &phi1, floats &phi2);
ints minDR_4 (floats &eta1, floats &eta2, floats &phi1, floats &phi2, floats &jet_pt, double &bjet_pt);
float minDR_5 (floats &eta1, floats &eta2, floats &phi1, floats &phi2);
float minDR_6 (double &eta1, floats &eta2, double &phi1, floats &phi2);
ints minDR_7 (floats &eta1, floats &eta2, floats &phi1, floats &phi2);
floats ratio_1 (floats &lepton_pt, floats &jet_pt);
floats ratio_2 (double &lepton_pt, floats &jet_pt);
float minratio_1 (floats &lepton_pt, floats &jet_pt);
float minratio_2 (double &lepton_pt, floats &jet_pt);
FourVectorRVec sum_two_4Rvecs (FourVectorRVec lepton, FourVectorRVec bjet);
FourVectorRVec removebjet(FourVectorRVec jet4vecs, floats jetpt, double bjetpt);
floats deltaeta(double &leta, floats &neta);
floats deltaphi(double &lphi, floats &nphi);
floats deltaR(double &leta, floats &neta, double &lphi, floats &nphi);
floats transverse_mass(double &lmass, floats &te, double &lpt, floats &npt, floats &dphi);
FourVectorRVec Leptonneutrinobjet (FourVectorRVec bjet, FourVectorRVec leptonneutrino, int nn);
floats transverse_mass_bjet(double &bmass, floats &lnmass, floats &te, double &jpt, floats &lnpt, floats &dphi);

bools isLfromtopHiggs (ints &pdgId, ints &genpdgId, ints &lgenidx, ints &midx);//, floats &leptonpt);
FourVectorVec isLfromtoporHiggs (ints &pdgId, ints &genpdgId, ints &lgenidx, ints &midx, FourVectorVec &genleptons4vecs);
bools isLfromW (ints &pdgId, ints &genpdgId, ints &lgenidx, ints &midx);
bool isLdifferentcharge (ints &pdgId, ints &genpdgId, ints &lgenidx);
bool isLnonprompt(uchars &lgenflav);
ints MotherfromL (ints &pdgId, ints &genpdgId, ints &lgenidx, ints &midx);
bools isWfromHiggs (ints &genpdgId, ints &wgenidx, ints &midx, floats &genmass);
bools isWfromtop (ints &genpdgId, ints &wgenidx, ints &midx, floats &genmass);
bools isLfromtopantitop (ints &pdgId, ints &genpdgId, ints &lgenidx, ints &midx);//, floats &leptonpt);
FourVectorVec isLfromtoporantitop (ints &pdgId, ints &genpdgId, ints &lgenidx, ints &midx, FourVectorVec &genleptons4vecs);
bools isWfromtoponly (ints &genpdgId, ints &wgenidx, ints &midx, floats &genmass);
bools isWfromantitop (ints &genpdgId, ints &wgenidx, ints &midx, floats &genmass);
bools isPfromtopHiggs (ints &genpdgId, ints &midx, ints &lgenidx);
bools isPfromHiggs (ints &genpdgId, ints &midx, ints &lgenidx);
bools isPfromtop (ints &genpdgId, ints &midx, ints &lgenidx);
bools isPfromtoponly (ints &genpdgId, ints &midx, ints &lgenidx);
bools isPfromantitop (ints &genpdgId, ints &midx, ints &lgenidx);
bools isJetFromgJet (floats &jet_pt, floats &gjet_pt, floats &gjetTp_pt, ints &jet_gidx);//, floats &jet_eta, floats &jet_phi, floats &jet_mass, ints &jet_jetid, floats &gjet_eta, floats &gjet_phi, floats &gjet_mass)
bools isJetFromgJet_2 (double &jet_pt, floats &gjet_pt, floats &gjetTp_pt, int &jet_gidx);
ints good_idx(ints good);
floats concatenate( double first, double second);
bools isdR04 (floats &eta1, floats &eta2, floats &phi1, floats &phi2);

#endif /* UTILITY_H_ */