/*
 * utility.cpp
 *
 *  Created on: Dec 4, 2018
 *      Author: suyong
 */
#include "utility.h"
#include "TMatrixDSym.h"
#include "TVectorT.h"
#include "Math/SpecFuncMathMore.h"
#include "Math/GenVector/VectorUtil.h"
#include "Math/GenVector/Rotation3D.h"
#include "Math/Math.h"
#include "ROOT/RVec.hxx"
#include "Math/Vector4D.h"
#include "TTree.h"
#include "TFile.h"
#include "TMath.h"
#include "correction.h"
// #include "Mt2/Basic_Mt2_332_Calculator.h"
// #include "Mt2/Mt2Units.h"
// #include "Minuit2/MnUserParameterState.h"
// #include "Minuit2/MnMigrad.h"
// #include "Minuit2/MnSimplex.h"
// #include "Minuit2/MnScan.h"
// #include "Minuit2/FunctionMinimum.h"
#include <iostream>
#include <algorithm>
using namespace std;
using namespace ROOT;
using namespace ROOT::VecOps;

using floats =  ROOT::VecOps::RVec<float>;
using ints =  ROOT::VecOps::RVec<int>;
using FourVector = ROOT::Math::PtEtaPhiMVector;
using FourVectorRVec = ROOT::VecOps::RVec<FourVector>;
using FourVectorVec = vector<FourVector>;
// using ROOT::Minuit2::MnUserParameters;
// using ROOT::Minuit2::MnMigrad;
// using ROOT::Minuit2::MnSimplex;
// using ROOT::Minuit2::MnScan;
// using ROOT::Minuit2::FunctionMinimum;
#include<cmath>

using namespace std;

float pucorrection(std::unique_ptr<correction::CorrectionSet> &cset, std::string name, std::string syst, float ntruepileup)
{
	return float(cset->at(name)->evaluate({ntruepileup, syst.c_str()}));
}

floats weightv(floats &x, float evWeight)
{
  const int nsize = x.size();
  floats weightvector(nsize, evWeight);
  return weightvector;
}

floats sphericity(FourVectorVec &p)
{
	TMatrixDSym NormMomTensor(3);

	NormMomTensor = 0.0;
	double p2sum = 0.0;
	for(auto x: p)
	{
		p2sum += x.P2();
		double mom[3] = {x.Px(), x.Py(), x.Pz()};
		for(int irow=0; irow<3; irow++)
		{
			for(int icol=irow; icol<3; icol++)
			{
				NormMomTensor(irow, icol) += mom[irow] * mom[icol];
			}
		}
	}
	NormMomTensor *= (1.0/p2sum);
	TVectorT<double> Qrev;
	NormMomTensor.EigenVectors(Qrev);
	floats Q(3);
	for(auto i=0; i<3; i++) Q[i] = Qrev[2-i];

	return Q;
}

double foxwolframmoment(int l, FourVectorVec &p, int minj, int maxj)
{   // PRD 87, 073014 (2013)
  double answer = 0.0;
  
  double ptsum=0.0;
  
  if(maxj==-1) // process everything
  {
    maxj = p.size();
  }
  //for(auto x: p)
  for(auto i=minj; i<maxj; i++)
  {
    auto x = p[i];
    ptsum += x.Pt();
    //for(auto y: p)
    for(auto j=minj; j<maxj; j++)
    {
      auto y = p[j];
      double wij = x.Pt() * y.Pt();
      double cosdOmega = x.Vect().Dot(y.Vect()) / (x.P() * y.P());
      if(cosdOmega>1.0) cosdOmega=1.0;
      if(cosdOmega<-1.0) cosdOmega=-1.0;
      answer += wij * ROOT::Math::legendre(l, cosdOmega);
    }
  }
  answer /= ptsum*ptsum;
  if(fabs(answer)>1.0) cout << "FW>1 " << answer << endl;
  return answer;
}

floats chi2(float smtop_mass, float smw_mass, float lfvtop_mass)
{
  floats out;
  // Theory values
  //        const float MT_LFV = 172.5;
  //        const float MT_SM = 172.5;
  //        const float MW = 80.4;
  //        const float WT_LFV = 1.41;
  //        const float WT_SM = 1.41;
  //        const float WW = 2.085;
  
  // Resolution applied values
  const float MT_LFV = 150.5;
  const float MT_SM = 165.2;
  const float MW = 80.8;
  const float WT_LFV = 17.8;
  const float WT_SM = 21.3;
  const float WW = 11.71;    
  
  float chi2_SMTop = pow((MT_SM-smtop_mass)/WT_SM, 2);
  float chi2_SMW = pow((MW-smw_mass)/WW, 2);
  float chi2_LFVTop = pow((MT_LFV-lfvtop_mass)/WT_LFV, 2);
  float chi2 = chi2_SMTop + chi2_SMW + chi2_LFVTop;
  
  out.emplace_back(chi2);
  out.emplace_back(chi2_SMTop);
  out.emplace_back(chi2_SMW);
  out.emplace_back(chi2_LFVTop);
  
  return out;
}

//floats top_reconstruction_whad(FourVectorVec &jets, FourVectorVec &bjets, FourVectorVec &muons, FourVectorVec &taus){
floats top_reconstruction_whad(FourVectorVec &jets, FourVectorVec &bjets, FourVectorVec &muons)
{
  floats out;
  
  float LFVtop_mass, SMW_mass, SMtop_mass;
  float X_LFVtop, X_SMW, X_SMtop;
  float X_min=9999999999, X_min_LFVtop_mass=-1, X_min_SMW_mass=-1, X_min_SMtop_mass=-1;
  float X_min_LFVtop=999999999, X_min_SMW=999999999, X_min_SMtop=999999999;
  float c_idx=-1, wj1_idx=-1, wj2_idx=-1;
  
  // Mass and Width of top and w boson
  const float MT_LFV = 150.5;
  const float MT_SM = 165.2;
  const float MW = 80.8;
  const float WT_LFV = 17.8;
  const float WT_SM = 21.3;
  const float WW = 11.71;    
  
  // U or C jet
  for(unsigned int j1 = 0; j1<jets.size(); j1++)
  {
    //LFVtop_mass = (jets[j1]+taus[0]+muons[0]).M();
    LFVtop_mass = (jets[j1]+muons[0]).M();
    X_LFVtop = pow((MT_LFV-LFVtop_mass)/WT_LFV,2);
    // Jet1 from W
    for(unsigned int j2 = 0; j2<jets.size(); j2++)
    {
      if(jets[j2].Pt() == bjets[0].Pt() || jets[j2].Pt() == jets[j1].Pt()) continue;
      // Jet2 from W
      for(unsigned int j3 = 0; j3<jets.size(); j3++)
      {
	      if(jets[j3].Pt() == jets[j2].Pt() || jets[j3].Pt() == bjets[0].Pt() || jets[j3].Pt() == jets[j1].Pt()) continue;
	      SMW_mass = (jets[j2]+jets[j3]).M();
	      X_SMW = pow((MW-SMW_mass)/WW,2);
	      SMtop_mass = (bjets[0]+jets[j2]+jets[j3]).M();
	      X_SMtop = pow((MT_SM-SMtop_mass)/WT_SM,2);
	      //min error
	      if(X_LFVtop + X_SMW + X_SMtop < X_min)
	      {
	        X_min = X_LFVtop + X_SMW + X_SMtop;
      	  X_min_LFVtop = X_LFVtop;
      	  X_min_SMW = X_SMW;
      	  X_min_SMtop = X_SMtop;
      	  X_min_LFVtop_mass = LFVtop_mass;
      	  X_min_SMW_mass = SMW_mass;
      	  X_min_SMtop_mass = SMtop_mass;
      	  c_idx = float(j1);
      	  wj1_idx = float(j2);
      	  wj2_idx = float(j3);
	      }
      }
    }
  }
  out.push_back(X_min);               // 0
  out.push_back(X_min_LFVtop_mass);   // 1
  out.push_back(X_min_SMW_mass);      // 2
  out.push_back(X_min_SMtop_mass);    // 3
  out.push_back(c_idx);               // 4
  out.push_back(wj1_idx);             // 5
  out.push_back(wj2_idx);             // 6
  out.push_back(X_min_LFVtop);        // 7
  out.push_back(X_min_SMW);           // 8
  out.push_back(X_min_SMtop);         // 9

  return out;
}

floats w_reconstruction (FourVectorVec &jets)
{
  floats out;
  float dijetMass;
  float dijetMass_out;
  float dijetDR;
  const float Mass_W = 80.9; //W mass 
  const float Width_W = 10.8; //W width
  float X_Min = 99999;
  float X_Recwidth_W; 
  
  //Loop on all selected jets
  for(unsigned int j1 = 0; j1<jets.size()-1; j1++)
  {
    for(unsigned int j2 = j1+1; j2<jets.size(); j2++)
    {
      //select 2 jets
      dijetMass = (jets[j1]+jets[j2]).M();
      //select a best W candidate in min width in a event
      X_Recwidth_W = pow((Mass_W-dijetMass)/Width_W,2);
      if(X_Recwidth_W<X_Min)
      {
	      dijetDR = ROOT::Math::VectorUtil::DeltaR(jets[j1],jets[j2]);
	      X_Min = X_Recwidth_W;
	      dijetMass_out = dijetMass;
      }    
    }
  }
  out.push_back(dijetMass_out); //0:w_mass
  out.push_back(dijetDR);  
  return out;
}

floats compute_DR (FourVectorVec &muons, ints &goodMuons_charge)
{
  floats out;
  float mu_ss_DR;
  float mu_os_DR;
  //cout<<"Muonsize: " << muons.size()<<endl;
  if(muons.size()>0)
    //Loop on all selected muons
    for(unsigned int mu1 = 0; mu1<muons.size()-1; mu1++)
    {
      for(unsigned int mu2 = mu1+1; mu2<muons.size(); mu2++)
      {
	      //select 2 muons with same sign
	      if(goodMuons_charge[mu1]!=goodMuons_charge[mu2]) continue; //check charge of muons
	      mu_ss_DR = ROOT::Math::VectorUtil::DeltaR(muons[mu1],muons[mu2]);
	      //select 2 muons with same sign
	      if(goodMuons_charge[mu1]==goodMuons_charge[mu2]) continue;
	      mu_os_DR = ROOT::Math::VectorUtil::DeltaR(muons[mu1],muons[mu2]);
      }
    }
  out.push_back(mu_ss_DR);		//0: same sign dimuon DR
  out.push_back(mu_os_DR);        //1: opposite sign dimuon dR
  return out;
}

floats H_reconstruction (FourVectorVec &jets, FourVectorVec &bjets)
{
  floats out;
  unsigned int hj1_idx=-1, hj2_idx=-1, wj1_idx=-1, wj2_idx=-1, tjb_idx=-1, oj_idx=-1;
  
  float H_mass, W_mass, Top_mass;
  float Chi2_H, Chi2_W, Chi2_Top;
  float Chi2_min=FLT_MAX;
  float Chi2_min_H=FLT_MAX, Chi2_min_W=FLT_MAX, Chi2_min_Top=FLT_MAX;
  // Mass and Width - 2018UL
  //const float trueSenario_mass;
  const float trueH_mass = 120.2;
  const float trueZ_mass = 90.9;
  const float trueTop_mass = 175.9;
  const float trueW_mass = 83.9;
  const float widthZ = 11.3;
  const float widthTop = 17.2;
  const float widthW = 10.8;
  
  const float widthH = 14.3;
  float H_mass_out;
        
  //Loop on all selected jets
  for(unsigned int bj1 = 0; bj1<bjets.size()-1; bj1++)
  {
    for(unsigned int bj2 = bj1+1; bj2<bjets.size(); bj2++)
    {
      //select 2 jets
      H_mass = (bjets[bj1]+bjets[bj2]).M();
      //X_Recwidth_W = pow((Mass_W-dijetMass)/Width_W,2);
      Chi2_H = pow((trueH_mass-H_mass)/widthH,2);
      if(Chi2_H < Chi2_min_H)
      {
	Chi2_min_H = Chi2_H;
	H_mass_out = H_mass;
	hj1_idx = bj1;
	hj2_idx = bj2;
      }
    }
  }
  out.push_back(H_mass_out);   //0:H_mass
  out.push_back(hj1_idx);
  out.push_back(hj2_idx);
  
  return out;
}

floats dimuon (FourVectorVec &muons, ints Muon_charge)
{
  //cout<<"AAAA"<<endl;
  floats out;
  //         float dimuonMass;
  //         float dimuonMass_out;
  float dimuonDphi;
  float dimuonDR;
  
  //Loop on all selected muons
  //	if(muons.size()>0){
  for(unsigned int j1 = 0; j1<muons.size(); j1++)
  {
    for(unsigned int j2 = j1+1; j2<muons.size(); j2++)
    {
      //dimuonMass = (muon[j1]+muon[j2]).M();
      //X_Min = X_Recwidth_W;
      //dimuonMass_out = dimuonMass;
      //select 2 muons of same charge
      //if(Muon_charge[j1]==Muon_charge[j2]){
      dimuonDphi = ROOT::Math::VectorUtil::DeltaPhi(muons[j1],muons[j2]);
      dimuonDR = ROOT::Math::VectorUtil::DeltaR(muons[j1],muons[j2]);
      //}
    }
  }
  //	}
  //         out.push_back(dijetMass_out);   //0:dilepton_mass
  out.push_back(dimuonDphi);         //1:dimuon_dphi (0 if the mass is commented)
  out.push_back(dimuonDR);         //2:dimuon_dr (1 if the mass is commented)
  
  return out;
}

floats sort_discriminant( floats &discr, floats &obj )
{
  auto sorted_discr = Reverse(Argsort(discr));
  floats out;
  for(auto idx : sorted_discr)
  {
    out.emplace_back(obj[idx]);
  }
  return out;
}

FourVector select_leadingvec( FourVectorVec &v )
{
  FourVector vout;
  if(v.size() > 0) return v[0];
  else return vout;
}

void PrintVector(floats &myvector)
{
  for(size_t i = 0; i < myvector.size(); i++)
  {
    cout<<myvector[i]<<"\n";
  }
}
ints pTcounter(floats &vec)
{
	ints output;
	int counter = 0;
	for(auto pt: vec)
	{
		if(pt>10.0) counter++;
	}
	output.emplace_back(counter);
	return output;
}

floats ratio(floats &numerator, floats &denominator)
{
  // cout<<"I1I1I1"<<endl;
  floats ratio;
  float temp;
  const int nsize = numerator.size();
  const int nnsize = denominator.size();
  if(nsize == 0 || nnsize == 0)
  {
    return ratio;
  }
  
  for(auto i=0; i<nsize; i++)
  {
    temp = numerator[i] / denominator[i];
    ratio.emplace_back(temp);
  }
  // cout<<"ratio = "<<ratio<<endl;
  return ratio;
}

floats ptrel_numerator(floats &lpx, floats &lpy, floats &lpz, floats &jpx, floats &jpy, floats &jpz)
{
  // cout<<"I2I2I2"<<endl;
  floats ratio;
  float temp;
  const int nsize = lpx.size();
  const int nnsize = jpx.size();
  if(nsize == 0 || nnsize == 0)
  {
    return ratio;
  }
  
  for(auto i=0; i<nsize; i++)
  {
    temp = sqrt((jpy[i]*lpz[i]-jpz[i]*lpy[i])*(jpy[i]*lpz[i]-jpz[i]*lpy[i])+(jpz[i]*lpx[i]-jpx[i]*lpz[i])*(jpz[i]*lpx[i]-jpx[i]*lpz[i])+(jpx[i]*lpy[i]-jpy[i]*lpx[i])*(jpx[i]*lpy[i]-jpy[i]*lpx[i]));
    ratio.emplace_back(temp);
  }
  // cout<<"ratio = "<<ratio<<endl;
  return ratio;
}

floats ptrel_denominator(floats &lpx, floats &lpy, floats &lpz, floats &jpx, floats &jpy, floats &jpz)
{
  // cout<<"I3I3I3"<<endl;
  floats ratio;
  float temp;
  const int nsize = lpx.size();
  const int nnsize = jpx.size();
  if(nsize == 0 || nnsize == 0)
  {
    return ratio;
  }
  
  for(auto i=0; i<nsize; i++)
  {
    temp = sqrt((jpx[i]-lpx[i])*(jpx[i]-lpx[i])+(jpy[i]-lpy[i])*(jpy[i]-lpy[i])+(jpz[i]-lpz[i])*(jpz[i]-lpz[i]));
    ratio.emplace_back(temp);
  }
  // cout<<"ratio = "<<ratio<<endl;
  return ratio;
}

bools electron_isolated_2016(floats &I1, floats &I2, floats &I3, floats &lpt, floats &ltemppt, floats &leta, floats &ltempeta)
{
  // cout<<"I4I4I4"<<endl;
  bools out;
  bool temp;
  const int nsize = I1.size();
  const int ntotsize = lpt.size();
  if(nsize == 0 && ntotsize == 0)
  {
    return out;
  }
  
  for(auto i=0; i<ntotsize; i++)
  {
    bool done = false;
    for(auto j=0; j < nsize; j++)
    {
      if(lpt[i] == ltemppt[j] && leta[i] == ltempeta[j])
      {
        temp = I1[j] < 0.12 && (I2[j] > 0.80 || I3[j] > 7.2);
        out.emplace_back(temp);
        done = true;
      }
    }
    if(done == false)
    {
      out.emplace_back(0);
    }
  }

  // cout<<"out = "<<out<<endl;
  return out;
}

bools electron_isolated_2017_2018(floats &I1, floats &I2, floats &I3, floats &lpt, floats &ltemppt, floats &leta, floats &ltempeta)
{
  // cout<<"I5I5I5"<<endl;
  bools out;
  bool temp;
  const int nsize = I1.size();
  const int ntotsize = lpt.size();
  if(nsize == 0 && ntotsize == 0)
  {
    return out;
  }
  
  for(auto i=0; i<ntotsize; i++)
  {
    bool done = false;
    for(auto j=0; j < nsize; j++)
    {
      if(lpt[i] == ltemppt[j] && leta[i] == ltempeta[j])
      {
        // cout<<"I1 = "<<I1[j]<<", I2 = "<<I2[j]<<", I3 = "<<I3[j];
        temp = I1[j] < 0.07 && (I2[j] > 0.78 || I3[j] > 8.0);
        // cout<<", temp = "<<temp<<endl;
        out.emplace_back(temp);
        done = true;
      }
    }
    if(done == false)
    {
      out.emplace_back(done);
    }
  }

  // cout<<"out = "<<out<<endl;
  return out;
}

bools muon_isolated_2016(floats &I1, floats &I2, floats &I3, floats &lpt, floats &ltemppt, floats &leta, floats &ltempeta)
{
  // cout<<"I6I6I6"<<endl;
  bools out;
  bool temp;
  const int nsize = I1.size();
  const int ntotsize = lpt.size();
  if(nsize == 0 && ntotsize == 0)
  {
    return out;
  }
  
  for(auto i=0; i<ntotsize; i++)
  {
    bool done = false;
    for(auto j=0; j < nsize; j++)
    {
      if(lpt[i] == ltemppt[j] && leta[i] == ltempeta[j])
      {
        temp = I1[j] < 0.16 && (I2[j] > 0.76 || I3[j] > 7.2);
        out.emplace_back(temp);
        done = true;
      }
    }
    if(done == false)
    {
      out.emplace_back(0);
    }
  }

  // cout<<"out = "<<out<<endl;
  return out;
}

bools muon_isolated_2017_2018(floats &I1, floats &I2, floats &I3, floats &lpt, floats &ltemppt, floats &leta, floats &ltempeta)
{
  // cout<<"I7I7I7"<<endl;
  bools out;
  int temp;
  const int nsize = I1.size();
  const int ntotsize = lpt.size();
  if(nsize == 0 && ntotsize == 0)
  {
    return out;
  }
  
  for(auto i=0; i<ntotsize; i++)
  {
    bool done = false;
    for(auto j=0; j < nsize; j++)
    {
      if(lpt[i] == ltemppt[j] && leta[i] == ltempeta[j])
      {
        temp = I1[j] < 0.11 && (I2[j] > 0.74 || I3[j] > 6.8);
        out.emplace_back(temp);
        done = true;
      }
    }
    if(done == false)
    {
      out.emplace_back(0);
    }
  }

  // cout<<"out = "<<out<<endl;
  return out;
}

bool false_mass_dilepton (FourVectorVec &leptons)
{
  // cout<<"H1H1H1"<<endl;
  bool out = false;
  float dilepton_mass = 0;

  if(leptons.size() < 2)
  {
    return out;
  }

  for( unsigned int i = 0; i < leptons.size(); i++)
  {
    // cout<<"i = "<<i<<endl;
    for( unsigned int j = i+1; j < leptons.size(); j++)
    {
      // cout<<"j = "<<j<<endl;
      auto dilepton = leptons[i] + leptons[j];
      dilepton_mass = dilepton.M();
      // cout<<"dilepton_mass = "<<dilepton_mass<<endl;
      if(dilepton_mass > 80 && dilepton_mass < 100) out = true;
      break;
    }
  }
  // cout<<"out = "<<out<<endl;
  return out;
}

FourVectorVec generate_4vec(floats &pt, floats &eta, floats &phi, floats &mass)
{
  const int nsize = pt.size();
  FourVectorVec fourvecs;
  fourvecs.reserve(nsize);
  for(auto i=0; i<nsize; i++)
  {
    // cout<<"pt = "<<pt<<", eta = "<<eta<<", phi = "<<phi<<", mass = "<<mass<<endl;
    fourvecs.emplace_back(pt[i], eta[i], phi[i], fabs(mass[i]));
  }
  
  return fourvecs;
}

FourVectorVec generate_4vec_2(float pt, float eta, float phi, float mass)
{
  FourVectorVec fourvecs;
  fourvecs.reserve(1);
  // cout<<"pt = "<<pt<<", eta = "<<eta<<", phi = "<<phi<<", mass = "<<mass<<endl;
  fourvecs.emplace_back(pt, eta, phi, fabs(mass));
  return fourvecs;
}

FourVectorVec generate_4vec_3(floats &pt, floats &eta, floats &phi)
{
  const int nsize = pt.size();
  FourVectorVec fourvecs;
  fourvecs.reserve(nsize);
  for(auto i=0; i<nsize; i++)
  {
    // cout<<"pt = "<<pt<<", eta = "<<eta<<", phi = "<<phi<<endl;
    fourvecs.emplace_back(pt[i], eta[i], phi[i], phi[i]-phi[i]);
  }
  return fourvecs;
}

FourVectorVec generate_4vec_4(float pt, float eta, float phi)
{
  FourVectorVec fourvecs;
  fourvecs.reserve(1);
  // cout<<"pt = "<<pt<<", eta = "<<eta<<", phi = "<<phi<<", mass = "<<mass<<endl;
  fourvecs.emplace_back(pt, eta, phi, phi-phi);
  return fourvecs;
}

FourVectorVec generate_4vec_5(floats pt, double eta, floats phi)
{
  FourVectorVec fourvecs;
  fourvecs.reserve(1);
  // cout<<"pt = "<<pt<<", eta = "<<eta<<", phi = "<<phi<<", mass = "<<mass<<endl;
  fourvecs.emplace_back(pt[0], (float) eta, phi[0], phi[0]-phi[0]);
  return fourvecs;
}

FourVectorRVec generate_4Rvec(floats &pt, floats &eta, floats &phi, floats &mass)
{
    const int nsize = pt.size();
    FourVectorRVec fourvecs;
    fourvecs.reserve(nsize);
    for(auto i=0; i<nsize; i++)
    {
        // cout<<"pt = "<<pt<<", eta = "<<eta<<", phi = "<<phi<<", mass = "<<mass<<endl;
        fourvecs.emplace_back(pt[i], eta[i], phi[i], fabs(mass[i]));
    }

    return fourvecs;
}

FourVectorRVec generate_4Rvec_2(double pt, double eta, double phi, double mass)
{
    FourVectorRVec fourvecs;
    fourvecs.reserve(1);
    // cout<<"pt = "<<pt<<", eta = "<<eta<<", phi = "<<phi<<", mass = "<<mass<<endl;
    fourvecs.emplace_back(pt, eta, phi, fabs(mass));
    return fourvecs;
}

FourVectorRVec generate_4Rvec_4(double pt, double eta, double phi)
{
  FourVectorRVec fourvecs;
  fourvecs.reserve(1);
  // cout<<"pt = "<<pt<<", eta = "<<eta<<", phi = "<<phi<<", mass = "<<mass<<endl;
  fourvecs.emplace_back(pt, eta, phi, phi-phi);
  return fourvecs;
}

float FourVecPt(FourVector fv)
{
  return fv.Pt();
}

float FourVecEta(FourVector fv)
{
  return fv.Eta();
}

float FourVecPhi(FourVector fv)
{
  return fv.Phi();
}

float FourVecMass(FourVector fv)
{
  return fv.M();
}

float FourVecEnergy(FourVector fv)
{
  return fv.E();
}

float FourVecPx(FourVector fv)
{
  return fv.Px();
}

float FourVecPy(FourVector fv)
{
  return fv.Py();
}

float FourVecPz(FourVector fv)
{
  return fv.Pz();
}

FourVectorVec genmet4vec(float met_pt, float met_phi)
{
        FourVectorVec vecs;
        float met_px = met_pt*cos(met_phi);
        float met_py = met_pt*sin(met_phi);
        for(int i = -500; i <= 500; i+=50)
        {
          FourVector metfourvec;
          metfourvec.SetPxPyPzE(met_px, met_py, i, met_pt);
          vecs.emplace_back(metfourvec);
        }
        return vecs;
}

float calculate_deltaEta( FourVector &p1, FourVector &p2)
{
  return p1.Eta() - p2.Eta();
}

float calculate_deltaPhi( FourVector &p1, FourVector &p2)
{
  return ROOT::Math::VectorUtil::DeltaPhi(p1, p2);
}

float calculate_deltaR( FourVector &p1, FourVector &p2)
{
  return ROOT::Math::VectorUtil::DeltaR(p1, p2);
}

float calculate_invMass( FourVector &p1, FourVector &p2)
{
  return ROOT::Math::VectorUtil::InvariantMass(p1, p2);
}

bools CheckOverlaps(const FourVectorVec &jets, const FourVectorVec &leps)
{       
  bools out;
  doubles mindrlepton;
  bool flag = false;
  for(auto ajet: jets)
  {
    flag = false;
    auto mindr = 6.0;
    for(auto alepton: leps)
    {
      auto dr = ROOT::Math::VectorUtil::DeltaR(ajet, alepton);
      if(dr < mindr)
      { 
        mindr = dr;
      }
    }
    if(mindr > 0.4)
    {
      flag = true;
    }
    else
    {
      flag = false;
    }
    out.emplace_back(flag);
  }
  return out;
}

RVec< RVec<int> > distinct_comb_3(int n)
{
  // cout<<"C1C1C1"<<endl;
  // cout <<"n = "<<n<< endl;
  RVec<int> idx1;
  RVec<int> idx2;
  RVec<int> idx3;
  RVec< RVec<int> > res;
  idx1.reserve(n*(n-1)/2);
  idx2.reserve(n*(n-1)/2);
  idx3.reserve(n*(n-1)/2);

  if(n < 3)
  {
    idx1.emplace_back(0);
    idx2.emplace_back(0);
    idx3.emplace_back(0);
  }  

  for(int i = 0; i < n-2; i++)
  {
    for(int j = i+1; j < n-1; j++)
    {
      for(unsigned int k = j+1; k < n; k++)
      {
	      idx1.emplace_back(i);
	      idx2.emplace_back(j);
	      idx3.emplace_back(k);
      }
    }
  }      
  // cout<<"idx1 = "<<idx1<<", idx2 = "<<idx2<<"idx3 = "<<idx3<<endl;
  res.emplace_back(idx1);
  res.emplace_back(idx2);
  res.emplace_back(idx3);
  return res;      
}

// FourVectorRVec Threejets (FourVectorRVec jet1, FourVectorRVec jet2, FourVectorRVec jet3, int jet_number, int bjet_number)
// {
//   // cout<<"C2C2C2"<<endl;
//   FourVectorRVec threejets;
//   if(jet_number>=3 && bjet_number>=1)
//   {
//     threejets = jet1 + jet2 + jet3;
//     //cout<<"njet = "<<jet_number<<endl; 
//   }
//   else
//   {
//     threejets = jet1 - jet1;
//     //cout<<"njet = "<<jet_number<<endl; 
//   }
//   // cout<<"threejets = "<<threejets<<endl;
//   return threejets;
// }

FourVectorRVec Threejets(FourVectorVec &jet4vecs, floats jeteta, floats jetphi, int jet_number, int bjet_number, double bjeteta, double bjetphi)
{
  // cout<<"C2C2C2"<<endl;
  FourVectorVec out;
  FourVectorVec threejets;
  float deltaR = FLT_MAX;
  int index_first = 0;
  int index_second = 0;
  int index_third = 0;
  if(jet_number>=3 && bjet_number>=1)
  {
    // threejets = jet1 + jet2 + jet3;
    // cout<<"njet = "<<jet_number<<endl;
    for(unsigned int i = 0; i < jet_number; i++)
    {
      // cout<<"i = "<<i<<endl;
      // cout<<"jeteta[i] = "<<jeteta[i]<<", bjeteta = "<<bjeteta<<", jetphi[i] = "<<jetphi[i]<<", bjetphi = "<<bjetphi<<endl;
      if(jeteta[i] == bjeteta && jetphi[i] == bjetphi)
      {
        index_first = i;
        for(unsigned int j = 0; j < jet_number-1; j++)
        {
          for(unsigned int k = j+1; k < jet_number; k++)
          {
            float temp_eta = 0;
            float temp_phi = 0;
            if(j != i && k != i)
            {
              temp_eta = (jet4vecs[j] + jet4vecs[k]).Eta();
              temp_phi = (jet4vecs[j] + jet4vecs[k]).Phi();
              float temp = ROOT::VecOps::DeltaR(jeteta[i],temp_eta,jetphi[i],temp_phi);
              // cout<<"temp = "<<temp<<endl;
              if(temp < deltaR)
              {
                // cout<<"yeaaaaah"<<endl;
                deltaR = temp;
                index_second = j;
                index_third = k;
              }
            }
          }
        }
      }
    }
    threejets.emplace_back(jet4vecs[index_first] + jet4vecs[index_second] + jet4vecs[index_third]);
  }
  else
  {
    threejets.emplace_back(jet4vecs[0] - jet4vecs[0]);
  }
  out.emplace_back(threejets[0].Pt(),threejets[0].Eta(),threejets[0].Phi(),threejets[0].M());
  // cout<<"out = "<<out<<endl;
  return out;
}

// FourVectorRVec Twojets (FourVectorRVec jet1, FourVectorRVec jet2, FourVectorRVec jet3, floats &jet1_btag, floats &jet2_btag, floats &jet3_btag, int jet_number, int bjet_number)
// {
//   // cout<<"C3C3C3"<<endl;
//   FourVectorRVec twojets;
//   int ncombi = jet1_btag.size();
//   // cout<<"jet1_btag = "<<jet1_btag<<", jet2_btag = "<<jet2_btag<<", jet3_btag = "<<jet3_btag<<endl;
//   if(jet_number>=3 && bjet_number>=1)
//   {
//     // cout<<"pouet"<<endl;
//     for(unsigned int i = 0; i < ncombi; i++)
//     {
//       if(jet3_btag[i] < jet1_btag[i] && jet3_btag[i] < jet2_btag[i])
//       {
//         // cout<<"jet 1 and jet 2 are chosen for W."<<endl;
//         twojets = jet1 + jet2;
//       }
//       if(jet2_btag[i] < jet1_btag[i] && jet2_btag[i] < jet3_btag[i])
//       {
//         // cout<<"jet 1 and jet 3 are chosen for W."<<endl;
//         twojets = jet1 + jet3;
//       }
//       if(jet1_btag[i] < jet2_btag[i] && jet1_btag[i] < jet3_btag[i])
//       {
//         // cout<<"jet 2 and jet 3 are chosen for W."<<endl;
//         twojets = jet2 + jet3;
//       }
//       //cout<<"njet = "<<jet_number<<endl; 
//     }
//   }
//   else
//   {
//     twojets = jet1 - jet1;
//     //cout<<"njet = "<<jet_number<<endl; 
//   }
//   return twojets;
// }

RVec< RVec<int> > distinct_comb_1(int nn)
{
  // cout<<"L5L5L5"<<endl;
  // cout<<"nn = "<<nn<<endl;
  RVec<int> idx1;
  RVec< RVec<int> > res;
  idx1.reserve(nn*(nn-1)/2);

  if(nn < 1)
  {
    idx1.emplace_back(0);
    return res;
  }  

  for(unsigned int i = 0; i < nn; i = i+1)
  {
	    idx1.emplace_back(i);
  }      
  // cout<<"idx1 = "<<idx1<<endl;
  res.emplace_back(idx1);
  return res;      
}

RVec< RVec<int> > distinct_comb_1_even(int nn)
{
  // cout<<"L2L2L2"<<endl;
  // cout<<"nn = "<<nn<<endl;
  RVec<int> idx1;
  RVec< RVec<int> > res;
  idx1.reserve(nn*(nn-1)/4);

  if(nn < 2)
  {
    idx1.emplace_back(0);
    return res;
  }  

  for(unsigned int i = 0; i < nn; i = i+2)
  {
	    idx1.emplace_back(i);
  }      
  // cout<<"idx1 = "<<idx1<<endl;
  res.emplace_back(idx1);
  return res;      
}

RVec< RVec<int> > distinct_comb_1_odd(int nn)
{
  // cout<<"L3L3L3"<<endl;
  // cout<<"nn = "<<nn<<endl;
  RVec<int> idx1;
  RVec< RVec<int> > res;
  idx1.reserve(nn*(nn-1)/4);

  if(nn < 2)
  {
    idx1.emplace_back(0);
    return res;
  }  

  for(unsigned int i = 1; i < nn; i = i+2)
  {
	    idx1.emplace_back(i);
  }      
  // cout<<"idx1 = "<<idx1<<endl;
  res.emplace_back(idx1);
  return res;      
}

RVec< RVec<int> > distinct_comb_2(int nl, int nj)
{
  // cout<<"J3J3J3"<<endl;
  // cout<<"nl = "<<nl<<endl;
  // cout<<"nj = "<<nj<<endl;
  RVec<int> idx1;
  RVec<int> idx2;
  RVec< RVec<int> > res;
  idx1.reserve(nl*(nl-1)/2);
  idx2.reserve(nj*(nj-1)/2);

  if(nl < 2 || nj < 1)
  {
    idx1.emplace_back(0);
    idx2.emplace_back(0);
  }  

  for(unsigned int i = 0; i < nl; i++)
  {
    for(unsigned int j = 0; j < nj; j++)
    {
	    idx1.emplace_back(i);
	    idx2.emplace_back(j);
    }
  }      
  // cout<<"idx1 = "<<idx1<<", idx2 = "<<idx2<<endl;
  res.emplace_back(idx1);
  res.emplace_back(idx2);
  return res;      
}

RVec< RVec<int> > distinct_comb_2_bis(int nj)
{
  // cout<<"J5J5J5"<<endl;
  // cout<<"nj = "<<nj<<endl;
  RVec<int> idx1;
  RVec<int> idx2;
  RVec< RVec<int> > res;
  idx1.reserve(nj*(nj-1)/2);
  idx2.reserve(nj*(nj-1)/2);

  if(nj < 2)
  {
    idx1.emplace_back(0);
    idx2.emplace_back(0);
  }  

  for(unsigned int i = 0; i < nj; i++)
  {
    for(unsigned int j = i+1; j < nj; j++)
    {
	    idx1.emplace_back(i);
	    idx2.emplace_back(j);
    }
  }      
  // cout<<"idx1 = "<<idx1<<", idx2 = "<<idx2<<endl;
  res.emplace_back(idx1);
  res.emplace_back(idx2);
  return res;      
}

RVec< RVec<int> > distinct_comb_2_even_odd(int nn)
{
  // cout<<"L4L4L4"<<endl;
  // cout<<"nn = "<<nn<<endl;
  RVec<int> idx1;
  RVec<int> idx2;
  RVec< RVec<int> > res;
  idx1.reserve(nn*(nn-1)/4);
  idx2.reserve(nn*(nn-1)/4);

  if(nn < 2)
  {
    idx1.emplace_back(0);
    idx2.emplace_back(0);
    return res;
  }  

  for(unsigned int i = 0; i < nn; i = i+2)
  {
    for(unsigned int j = 1; j < nn; j = j+2)
    {
	    idx1.emplace_back(i);
	    idx2.emplace_back(j);
    }
  }      
  // cout<<"idx1 = "<<idx1<<", idx2 = "<<idx2<<endl;
  res.emplace_back(idx1);
  res.emplace_back(idx2);
  return res;      
}

floats DR_1 (floats &eta1, floats &eta2, floats &phi1, floats &phi2)
{
  // cout<<"G1G1G1"<<endl;
  floats out;
  float deltaR = FLT_MAX;
  for( unsigned int i = 0; i < eta1.size(); i++ )
  {
    // cout<<"i = "<<i<<endl;
    for( unsigned int j = 0; j < eta2.size(); j++ )
    {
      // cout<<"j = "<<j<<endl;
      deltaR = FLT_MAX;
      auto deltaR = ROOT::VecOps::DeltaR(eta1[i],eta2[j],phi1[i],phi2[j]);
	    // cout<<"deltaR = "<<deltaR<<endl;
      out.emplace_back(deltaR);
    }
  }
  // cout<<"out = "<<out<<endl;
  return out;
}

floats DR_2 (double &eta1, floats &eta2, double &phi1, floats &phi2)
{
  // cout<<"G2G2G2"<<endl;
  floats out;
  float deltaR = FLT_MAX;
  for( unsigned int i = 0; i < eta2.size(); i++ )
  {
    // cout<<"i = "<<i<<endl;
    deltaR = FLT_MAX;
    auto deltaR = ROOT::VecOps::DeltaR(float(eta1),eta2[i],float(phi1),phi2[i]);
    // cout<<"deltaR = "<<deltaR<<endl;
    out.emplace_back(deltaR);
  }
  // cout<<"out = "<<out<<endl;
  return out;
}

int maxDR (double &eta1, floats &eta2, double &phi1, floats &phi2)
{
  //cout<<"G3G3G3"<<endl;
  int out = -1;
  float maxdr = -FLT_MAX;
  for( unsigned int i = 0; i < eta2.size(); i++ )
  {
    //cout<<"i = "<<i<<endl; 
    auto dr = ROOT::VecOps::DeltaR(float(eta1),eta2[i],float(phi1),phi2[i]);
    if( dr > maxdr )
    {
      out = i;
      maxdr = dr;
      //cout<<"maxdr = "<<maxdr<<endl;
    }
  }
  //cout<<"out = "<<out<<endl;
  return out;
}

ints minDR_1 (floats &eta1, floats &eta2, floats &phi1, floats &phi2)
{
  // cout<<"G4G4G4"<<endl;
  ints out = {-1, -1}; 
  ints mout = {-1, -1};
  float mindr = FLT_MAX;
  float mmindr = FLT_MAX;
  // cout<<"nj = "<<eta2.size()<<endl;
  if(eta2.size() < 3)
  {
    for( unsigned int i = 0; i < eta1.size(); i++ )
    {
      // cout<<"i = "<<i<<endl;
      for( unsigned int j = 0; j < eta2.size(); j++ )
      {
        // cout<<"j = "<<j<<endl;
        auto dr = ROOT::VecOps::DeltaR(eta1[i],eta2[j],phi1[i],phi2[j]);
        if( dr < mindr )
        {
	        out = {i, j};
	        mindr = dr;
	        // cout<<"mindr = "<<mindr<<endl;
        }
      }
    }
  }
  else if(eta2.size() >= 3)
  {
    for( unsigned int i = 0; i < eta1.size(); i++ )
    {
      // cout<<"i = "<<i<<endl;
      for( unsigned int j = 0; j < eta2.size(); j++ )
      {
        // cout<<"j = "<<j<<endl;
        auto dr = ROOT::VecOps::DeltaR(eta1[i],eta2[j],phi1[i],phi2[j]);
        if( dr < mindr )
        {
          if( dr < mmindr )
          {
            out = mout;
            mout = {i, j};
            mindr = mmindr;
            mmindr = dr;
            // cout<<"mindr = "<<mindr<<endl;
            // cout<<"mmindr = "<<mmindr<<endl;
          }
          else
          {
            out = {i, j};
            mindr = dr;
            // cout<<"mindr = "<<mindr<<endl;
            // cout<<"mmindr = "<<mmindr<<endl;
          }
        }
      }
    }
  }
  // cout<<"out = "<<out<<endl;
  return out;
}

ints minDR_2 (floats &eta1, floats &eta2, floats &phi1, floats &phi2)
{
  // cout<<"G5G5G5"<<endl;
  ints out = {-1, -1}; 
  float mindr = FLT_MAX;
  for( unsigned int i = 0; i < eta1.size(); i++ )
  {
    // cout<<"i = "<<i<<endl;	 
    for( unsigned int j = i+1; j < eta2.size(); j++ )
    {
      // cout<<"j = "<<j<<endl;
      auto dr = ROOT::VecOps::DeltaR(eta1[i],eta2[j],phi1[i],phi2[j]);
      if( dr < mindr )
      {
	      out = {i, j};
	      mindr = dr;
	      // cout<<"mindr = "<<mindr<<endl;
      }
    }
  }
  // cout<<"out = "<<out<<endl;
  return out;
}

int minDR_3 (double &eta1, floats &eta2, double &phi1, floats &phi2)
{
  //cout<<"G6G6G6"<<endl;
  int out = -1;
  float mindr = FLT_MAX;
  for( unsigned int i = 0; i < eta2.size(); i++ )
  {
    //cout<<"i = "<<i<<endl; 
    auto dr = ROOT::VecOps::DeltaR(float(eta1),eta2[i],float(phi1),phi2[i]);
    if( dr < mindr )
    {
      out = i;
      mindr = dr;
      //cout<<"mindr = "<<mindr<<endl;
    }
  }
  // cout<<"out = "<<out<<endl;
  return out;
}

ints minDR_4 (floats &eta1, floats &eta2, floats &phi1, floats &phi2, floats &jet_pt, double &bjet_pt)
{
  //cout<<"G7G7G7"<<endl;
  ints out = {-1, -1}; 
  float mindr = FLT_MAX;
  for( unsigned int i = 0; i < eta1.size(); i++ )
  {
    //cout<<"i = "<<i<<endl;	 
    for( unsigned int j = i+1; j < eta2.size(); j++ )
    {
      //cout<<"j = "<<j<<endl;
      if(jet_pt[i] != bjet_pt && jet_pt[j] != bjet_pt)
      {
	      auto dr = ROOT::VecOps::DeltaR(eta1[i],eta2[j],phi1[i],phi2[j]);
	      if( dr < mindr )
	      {
	        out = {i, j};
	        mindr = dr;
	        //cout<<"mindr = "<<mindr<<endl;
	      }
      }
    }
  }
  //cout<<"out = "<<out<<endl;
  return out;
}

float minDR_5 (floats &eta1, floats &eta2, floats &phi1, floats &phi2)
{
  // cout<<"G8G8G8"<<endl;
  float out = 100;
  float mindr = FLT_MAX;
  for( unsigned int i = 0; i < eta1.size(); i++ )
  {
    // cout<<"i = "<<i<<endl;
    for( unsigned int j = 0; j < eta2.size(); j++ )
    {
      // cout<<"j = "<<j<<endl;
      auto dr = ROOT::VecOps::DeltaR(eta1[i],eta2[j],phi1[i],phi2[j]);
      if( dr < mindr )
      {
	      mindr = dr;
        out = mindr;
	      // cout<<"mindr = "<<mindr<<endl;
      }
    }
  }
  // cout<<"out = "<<out<<endl;
  return out;
}

float minDR_6 (double &eta1, floats &eta2, double &phi1, floats &phi2)
{
  // cout<<"G9G9G9"<<endl;
  float out = 100;
  float mindr = FLT_MAX;
  for( unsigned int i = 0; i < eta2.size(); i++ )
  {
    // cout<<"i = "<<i<<endl;
    auto dr = ROOT::VecOps::DeltaR(float(eta1),eta2[i],float(phi1),phi2[i]);
    if( dr < mindr )
    {
	    mindr = dr;
      out = mindr;
	    // cout<<"mindr = "<<mindr<<endl;
    }
  }
  // cout<<"out = "<<out<<endl;
  return out;
}

ints minDR_7 (floats &eta1, floats &eta2, floats &phi1, floats &phi2)
{
  // cout<<"G6G6G6"<<endl;
  ints out;
  int temp = 0;
  float mindr = FLT_MAX;
  for( unsigned int i = 0; i < eta1.size(); i++ )
  {
    temp = 0;
    mindr = FLT_MAX;
    // cout<<"i = "<<i<<endl;	 
    for( unsigned int j = 0; j < eta2.size(); j++ )
    {
      // cout<<"j = "<<j<<endl;
	    auto dr = ROOT::VecOps::DeltaR(eta1[i],eta2[j],phi1[i],phi2[j]);
	    if( dr < mindr )
	    {
	      temp = j;
	      mindr = dr;
        // cout<<"mindr = "<<mindr<<endl;
	    }
    }
    out.emplace_back(temp);
  }
  // cout<<"out = "<<out<<endl;
  return out;
}

floats ratio_1 (floats &lepton_pt, floats &jet_pt)
{
  // cout<<"I1I1I1"<<endl;
  floats out;
  float ratio_pt = 0;

  for( unsigned int i = 0; i < lepton_pt.size(); i++)
  {
    // cout<<"i = "<<i<<endl;
    for( unsigned int j = 0; j < jet_pt.size(); j++)
    {
      // cout<<"j = "<<j<<endl;
      float ratio_pt = 0;
      ratio_pt = lepton_pt[i]/jet_pt[j];
      // cout<<"ratio_pt = "<<ratio_pt<<endl;
      out.emplace_back(ratio_pt);
    }
  }
  // cout<<"out = "<<out<<endl;
  return out;
}

floats ratio_2 (double &lepton_pt, floats &jet_pt)
{
  // cout<<"I2I2I2"<<endl;
  floats out;
  float ratio_pt = 0;

  for( unsigned int i = 0; i < jet_pt.size(); i++)
  {
    // cout<<"i = "<<i<<endl;
    float ratio_pt = 0;
    ratio_pt = float(lepton_pt)/jet_pt[i];
    // cout<<"ratio_pt = "<<ratio_pt<<endl;
    out.emplace_back(ratio_pt);
  }
  // cout<<"out = "<<out<<endl;
  return out;
}

float minratio_1 (floats &lepton_pt, floats &jet_pt)
{
  // cout<<"I3I3I3"<<endl;
  float out = 100;
  float minratio_pt = FLT_MAX;

  for( unsigned int i = 0; i < lepton_pt.size(); i++)
  {
    // cout<<"i = "<<i<<endl;
    for( unsigned int j = 0; j < jet_pt.size(); j++)
    {
      // cout<<"j = "<<j<<endl;
      auto ratio_pt = lepton_pt[i]/jet_pt[j];
      if( ratio_pt < minratio_pt )
      {
        minratio_pt = ratio_pt;
        out = minratio_pt;
        // cout<<"minratio_pt = "<<minratio_pt<<endl;
      }
    }
  }
  // cout<<"out = "<<out<<endl;
  return out;
}

float minratio_2 (double &lepton_pt, floats &jet_pt)
{
  // cout<<"I4I4I4"<<endl;
  float out = 100;
  float minratio_pt = FLT_MAX;

  for( unsigned int i = 0; i < jet_pt.size(); i++)
  {
    // cout<<"i = "<<i<<endl;
    auto ratio_pt = float(lepton_pt)/jet_pt[i];
    if( ratio_pt < minratio_pt )
    {
      minratio_pt = ratio_pt;
      out = minratio_pt;
      // cout<<"minratio_pt = "<<minratio_pt<<endl;
    }
  }
  // cout<<"out = "<<out<<endl;
  return out;
}

FourVectorRVec sum_two_4Rvecs (FourVectorRVec vec1, FourVectorRVec vec2)
{
  // cout<<"J4J4J4"<<endl;
  FourVectorRVec vec;
  vec = vec1 + vec2;
  // cout<<"vec = "<<vec<<endl;
  return vec;
}

FourVectorRVec removebjet(FourVectorRVec jet4Rvecs, floats jetpt, double bjetpt)
{
  // cout<<"J1J1J1"<<endl;
  unsigned int nj = jetpt.size();
  // cout<<"nj = "<<nj<<endl;
  unsigned int index = 0;
  bool bjet = false;
  FourVectorRVec temp;
  temp.reserve(1);
  if(nj < 1)
  {
    return jet4Rvecs;
  }
  for( unsigned int i = 0; i < nj; i++)
  {
    // cout<<"i = "<<i<<endl;
    if(jetpt[i] == (float) bjetpt)
    {
      // cout<<"The bjet coming from the top has the index "<<i<<"."<<endl;
      index = i;
      bjet = true;
    }
    if(bjet == true && index != i)
    {
      temp[0] = jet4Rvecs[i];
      jet4Rvecs[i] = jet4Rvecs[index];
      jet4Rvecs[index] = temp[0];
      index = i;
    }
  }
  // temp = jet4Rvecs[nj];
  // jet4Rvecs[nj] = jet4Rvecs[index];
  // jet4Rvecs[index] = temp;
  jet4Rvecs.pop_back();
  // cout<<"jet4Rvecs = "<<jet4Rvecs<<endl;

  return jet4Rvecs;
}

floats deltaeta(double &leta, floats &neta)
{
  // cout<<"H2H2H2"<<endl;
  floats deta;
  unsigned int nn = neta.size();
  if(neta[0] == 0)
  {
    deta.push_back(0);
    return deta;
  }
  for(unsigned int i = 0; i < nn; i++)
  {
    double delta = abs(float(leta) - neta[i]);
    deta.push_back(delta);
  }
  // cout<<"deta = "<<deta<<endl;
  return deta;
}

floats deltaphi(double &lphi, floats &nphi)
{
  // cout<<"H3H3H3"<<endl;
  floats dphi;
  unsigned int nn = nphi.size();
  if(nphi[0] == 0)
  {
    dphi.push_back(0);
    return dphi;
  }
  for(unsigned int i = 0; i < nn; i++)
  {
    double delta = ROOT::VecOps::DeltaPhi(float(lphi), nphi[i]);
    dphi.push_back(delta);
  }
  // cout<<"dphi = "<<dphi<<endl;
  return dphi;
}

floats deltaR(double &leta, floats &neta, double &lphi, floats &nphi)
{
  // cout<<"H4H4H4"<<endl;
  floats dR;
  unsigned int nn = neta.size();
  if(neta[0] == 0)
  {
    dR.push_back(0);
    return dR;
  }
  for(unsigned int i = 0; i < nn; i++)
  {
    double delta = ROOT::VecOps::DeltaR(float(leta), neta[i], float(lphi), nphi[i]);
    dR.push_back(delta);
  }
  // cout<<"dR = "<<dR<<endl;
  return dR;
}

floats transverse_mass(double &lmass, floats &te, double &lpt, floats &npt, floats &dphi)
{
  // cout<<"H5H5H5"<<endl;
  floats tm;
  unsigned int nn = npt.size();
  if(npt[0] == 0)
  {
    tm.push_back(0);
    return tm;
  }
  for(unsigned int i = 0; i < nn; i++)
  {
    double transmass = sqrt(float(lmass)*float(lmass) + 2*te[i]*te[i] - 2*float(lpt)*npt[i]*cos(dphi[i]));
    tm.push_back(transmass);
  }
  // cout<<"tm = "<<tm<<endl;
  return tm;
}

FourVectorRVec Leptonneutrinobjet (FourVectorRVec bjet, FourVectorRVec leptonneutrino, int nn)
{
  // cout<<"H6H6H6"<<endl;
  FourVectorRVec vecs;
  FourVectorRVec temp;
  vecs.reserve(nn/2);
  // cout<<"bjet = "<<bjet<<endl;

  for(unsigned int i = 0; i < nn/2; i++)
  {
    // cout<<"leptonneutrino["<<i<<"] = "<<leptonneutrino[i]<<endl;
    if(leptonneutrino[i].Pt() == 0)
    {
      vecs.emplace_back(0, 0, 0, 0);
      return vecs;
    }
    temp = bjet + leptonneutrino[i];
    // cout<<"temp = "<<temp<<", Pt = "<<temp[0].Pt()<<", Eta = "<<temp[0].Eta()<<", Phi = "<<temp[0].Phi()<<", Mass = "<<temp[0].M()<<endl;
    vecs.emplace_back(temp[0].Pt(),temp[0].Eta(),temp[0].Phi(),temp[0].M());
  }
  // cout<<"vecs = "<<vecs<<endl;
  return vecs;
}

floats transverse_mass_bjet(double &bmass, floats &lnmass, floats &te, double &jpt, floats &lnpt, floats &dphi)
{
  // cout<<"H7H7H7"<<endl;
  floats tm;
  unsigned int nn = lnpt.size();
  if(lnpt[0] == 0)
  {
    tm.push_back(0);
    return tm;
  }
  for(unsigned int i = 0; i < nn; i++)
  {
    double transmass = sqrt(float(bmass)*float(bmass) + lnmass[i]*lnmass[i] + 2*te[i]*te[i] - 2*float(jpt)*lnpt[i]*cos(dphi[i]));
    tm.push_back(transmass);
  }
  // cout<<"tm = "<<tm<<endl;
  return tm;
}

bools isLfromtopHiggs (ints &pdgId, ints &genpdgId, ints &lgenidx, ints &midx)//, floats &leptonpt)
{
  //cout<<"B1B1B1"<<endl;
  bools out;
  unsigned int nl = pdgId.size();
  //cout<<"nl = "<<nl<<endl;
  bool flag = false;
  bool wrong_decay = false;
  bool decay_W = false;
  bool decay_top = false;
  bool decay_Higgs = false;

  // GenParticle loop 
  for(unsigned int i = 0; i < nl; i++)
  {
    flag = false;
    wrong_decay = false;
    decay_W = false;
    //cout<<"i = "<<i<<endl;
    
    // matching the lepton to the same lepton in the Generator tree
    int genlidx = lgenidx[i];
    //cout<<"Nature of the particle = "<<pdgId[i]<<" "<<endl;
    //cout<<"Lepton_pt = "<<leptonpt[i]<<" "<<endl;
    //cout<<"genlidx = "<<genlidx<<" "<<endl;
    //cout<<"Nature of the Gen particle = "<<genpdgId[genlidx]<<" "<<endl;
    //if(pdgId[i] == -genpdgId[genlidx])cout<<"The particle and the Gen particle don't have the same charge."<<endl;
    
    // tracking the mother
    int moid = midx[genlidx];
    //just to be sure if it is not out of the range... (otherwise it arises segmentation fault)
    //cout<<"moid = "<<moid<<" nature = "<<genpdgId[moid]<<endl;
    if( moid > genpdgId.size() || moid < -1 ) break;
    //cout<<"moidx = "<<moid<<" "<<endl;
    //cout<<"Nature of its mother = "<<genpdgId[moid]<<" "<<endl;
    
    // check if the lepton comes from a W
    while( abs( genpdgId[moid] ) != 24 && (abs( genpdgId[moid] ) == 13 || abs( genpdgId[moid] ) == 11))
    {
      moid = midx[moid];
      if( moid > genpdgId.size() || moid < -1 ) break;
      //cout<<"moidx = "<<moid<<" "<<endl;
      //cout<<"Nature of its mother = "<<genpdgId[moid]<<" "<<endl;
      if( abs( genpdgId[moid] ) == 5 )
      {
	      //cout<<"The lepton comes from a quark b."<<endl;
	      wrong_decay = true;
	      break;
      }
    }
    
    if( abs( genpdgId[moid] ) == 24 && wrong_decay == false )
    {
      decay_W = true;
      //cout<<"The lepton comes from a W."<<endl;
      int gmoid = midx[moid];
      //just to be sure if it is not out of the range... (otherwise it arises segmentation fault)
      if( gmoid > genpdgId.size() || gmoid < -1 ) break;
      //cout<<"Nature of its grandmother = "<<genpdgId[gmoid]<<" "<<endl;
      
      // make sure the lepton isn't coming from a quark b
      if( abs( genpdgId[gmoid] ) == 5 )
      {
	      //cout<<"The lepton comes from a quark b."<<endl;
	      wrong_decay = true;
	      break;
      }
      if( abs( genpdgId[gmoid] ) == 6 )
      {
	      //cout<<"The lepton comes from a quark t."<<endl;
	      decay_top = true;
      }
      if( abs( genpdgId[gmoid] ) == 25 )
      {
	      //cout<<"The lepton comes from a Higgs boson."<<endl;
	      decay_Higgs = true;
      }
      
      while( abs( genpdgId[gmoid] ) != 5)
      {
	      gmoid = midx[gmoid];
	      if( gmoid > genpdgId.size() || gmoid < -1 ) break;
	      //cout<<"gmoidx = "<<gmoid<<" "<<endl;
	      //cout<<"Nature of its grandmother = "<<genpdgId[gmoid]<<" "<<endl;
	      if( abs( genpdgId[gmoid] ) == 5 )
	      {
	        //cout<<"The lepton comes from a quark b."<<endl;
	        wrong_decay = true;
	        break;
	      }
	      if( abs( genpdgId[gmoid] ) == 6 )
	      {	
	        //cout<<"The lepton comes from a quark t."<<endl;
	        decay_top = true;
	      }
	      if( abs( genpdgId[gmoid] ) == 25 )
	      {
	        //cout<<"The lepton comes from a Higgs boson."<<endl;
	        decay_Higgs = true;
	      }	     
	      if( abs( genpdgId[gmoid] ) == 8000001 )
	      {
	        //cout<<"The lepton comes from the T'."<<endl;
	      }
      }
    }

    if( abs( genpdgId[moid] ) == 24 || abs( genpdgId[moid] ) == 15)
    {
      flag = true;
      //cout<<"yeaaaah"<<endl;
    }

    if( i == 0 && decay_W == true && wrong_decay == false && (decay_top == true || decay_Higgs == true))
    {
      flag = true;
      //cout<<"yeaaaah"<<endl;
    }
    if( i == 1 && decay_W == true && wrong_decay == false && decay_top == true && decay_Higgs == true)
    {
      flag = true;
      //cout<<"yeaaaah"<<endl;
    }
    out.emplace_back(flag);
    //cout<<"out = "<<out<<endl;
  }          
  return out;
}

FourVectorVec isLfromtoporHiggs (ints &pdgId, ints &genpdgId, ints &lgenidx, ints &midx, FourVectorVec &genleptons4vecs)
{
  //cout<<"B2B2B2"<<endl;
  unsigned int nl = pdgId.size();
  //cout<<"nl = "<<nl<<endl;
  bool flag = false;
  bool wrong_decay = false;
  bool decay_W = false;
  bool decay_top = false;
  bool decay_Higgs = false;
  bool top_leading = false;
  bool Higgs_leading = false;
  
  // GenParticle loop 
  for(unsigned int i = 0; i < nl; i++)
  {
    flag = false;
    wrong_decay = false;
    decay_W = false;
    //cout<<"i = "<<i<<endl;
    
    // matching the lepton to the same lepton in the Generator tree
    int genlidx = lgenidx[i];
    //cout<<"Nature of the particle = "<<pdgId[i]<<" "<<endl;
    //cout<<"genlidx = "<<genlidx<<" "<<endl;
    //cout<<"Nature of the Gen particle = "<<genpdgId[genlidx]<<" "<<endl;
    
    // tracking the mother
    int moid = midx[genlidx];
    //just to be sure if it is not out of the range... (otherwise it arises segmentation fault)
    if( moid > genpdgId.size() || moid < -1 ) break;
    //cout<<"moidx = "<<moid<<" "<<endl;
    //cout<<"Nature of its mother = "<<genpdgId[moid]<<" "<<endl;
    
    // check if the lepton comes from a W
    while( abs( genpdgId[moid] ) != 24 && (abs( genpdgId[moid] ) == 13 || abs( genpdgId[moid] ) == 11))
    {
      moid = midx[moid];
      if( moid > genpdgId.size() || moid < -1 ) break;
      //cout<<"moidx = "<<moid<<" "<<endl;
      //cout<<"Nature of its mother = "<<genpdgId[moid]<<" "<<endl;
      if( abs( genpdgId[moid] ) == 5 )
      {
	      //cout<<"The lepton comes from a quark b."<<endl;
	      wrong_decay = true;
	      break;
      }
    }
    
    if( abs( genpdgId[moid] ) == 24 && wrong_decay == false )
    {
      decay_W = true;
      //cout<<"The lepton comes from a W."<<endl;
      int gmoid = midx[moid];
      //just to be sure if it is not out of the range... (otherwise it arises segmentation fault)
      if( gmoid > genpdgId.size() || gmoid < -1 ) break;
      //cout<<"Nature of its grandmother = "<<genpdgId[gmoid]<<" "<<endl;
      
      // make sure the lepton isn't coming from a quark b
      if( abs( genpdgId[gmoid] ) == 5 )
      {
	      //cout<<"The lepton comes from a quark b."<<endl;
	      wrong_decay = true;
	      break;
      }
      if( abs( genpdgId[gmoid] ) == 6 )
      {
	      //cout<<"The lepton comes from a quark t."<<endl;
	      decay_top = true;
      }
      if( abs( genpdgId[gmoid] ) == 25 )
      {
	      //cout<<"The lepton comes from a Higgs boson."<<endl;
	      decay_Higgs = true;
      }
      
      while( abs( genpdgId[gmoid] ) != 5)
      {
	      gmoid = midx[gmoid];
	      if( gmoid > genpdgId.size() || gmoid < -1 ) break;
	      //cout<<"gmoidx = "<<gmoid<<" "<<endl;
	
	      //cout<<"Nature of its grandmother = "<<genpdgId[gmoid]<<" "<<endl;
	      if( abs( genpdgId[gmoid] ) == 5 )
	      {
	        //cout<<"The lepton comes from a quark b."<<endl;
	        wrong_decay = true;
	        break;
	      }
	      if( abs( genpdgId[gmoid] ) == 6 )
	      {
	        //cout<<"The lepton comes from a quark t."<<endl;
	        decay_top = true;
	      }
	      if( abs( genpdgId[gmoid] ) == 25 )
	      {
	        //cout<<"The lepton comes from a Higgs boson."<<endl;
	        decay_Higgs = true;
	      }	     
	      if( abs( genpdgId[gmoid] ) == 8000001 )
	      {
	        //cout<<"The lepton comes from the T'."<<endl;
	      }
      }
    }
    
    if( i == 0 && decay_W == true && wrong_decay == false && decay_top == true)
    {
      top_leading = true;
      //cout<<"yeaaaah"<<endl;
    }
    if( i == 1 && decay_W == true && wrong_decay == false && decay_top == true && decay_Higgs == true && top_leading == true)
    {
      //cout<<"yeaaaah"<<endl;
    }
    if( i == 0 && decay_W == true && wrong_decay == false && decay_Higgs == true)
    {
      Higgs_leading = true;
      //cout<<"yeaaaah"<<endl;
    }
    if( i == 1 && decay_W == true && wrong_decay == false && decay_top == true && decay_Higgs == true && Higgs_leading == true)
    {
      swap(genleptons4vecs[0],genleptons4vecs[1]);
      //cout<<"yeaaaah"<<endl;
      //cout<<"Swap!"<<endl;
    }
  }          
  return genleptons4vecs;
}

bools isLfromW (ints &pdgId, ints &genpdgId, ints &lgenidx, ints &midx)
{
  // cout<<"B3B3B3"<<endl;
  bools out;
  unsigned int nl = pdgId.size();
  // cout<<"nl = "<<nl<<endl;
  bool flag = false;
  
  // GenParticle loop 
  for(unsigned int i = 0; i < nl; i++)
  {
    flag = false;
    // cout<<"i = "<<i<<endl;
    
    // matching the lepton to the same lepton in the Generator tree
    int genlidx = lgenidx[i];
    // cout<<"Nature of the particle = "<<pdgId[i]<<" "<<endl;
    // cout<<"genlidx = "<<genlidx<<" "<<endl;
    // cout<<"Nature of the Gen particle = "<<genpdgId[genlidx]<<" "<<endl;
    
    // tracking the mother
    int moid = midx[genlidx];
    //just to be sure if it is not out of the range... (otherwise it arises segmentation fault)
    //cout<<"moid = "<<moid<<" nature = "<<genpdgId[moid]<<endl;
    //if( moid > genpdgId.size() || moid < -1 ) break;
    // cout<<"moidx = "<<moid<<" "<<endl;
    // cout<<"Nature of its mother = "<<genpdgId[moid]<<" "<<endl;
    
    // check if the lepton comes from a W
    while( abs( genpdgId[moid] ) != 24 && (abs( genpdgId[moid] ) == 13 || abs( genpdgId[moid] ) == 11))
    {
      moid = midx[moid];
      //if( moid > genpdgId.size() || moid < -1 ) break;
      // cout<<"moidx = "<<moid<<" "<<endl;
      // cout<<"Nature of its mother = "<<genpdgId[moid]<<" "<<endl;
    }
    
    if( abs( genpdgId[moid] ) == 24)
    {
      flag = true;
      // cout<<"yeaaaah"<<endl;
    }

//     // check if the lepton comes from a tau or a Z boson
//     while( abs( genpdgId[moid] ) != 15) // && abs( genpdgId[moid] ) != 23 )
//     {
//       moid = midx[moid];
//       if( moid > genpdgId.size() || moid < -1 ) break;
//       //cout<<"moidx = "<<moid<<" "<<endl;
//       //cout<<"Nature of its mother = "<<genpdgId[moid]<<" "<<endl;
//     }
//     //cout<<"Nature of its mother = "<<genpdgId[moid]<<" "<<endl;

//     if( abs( genpdgId[moid] ) == 15 || abs( genpdgId[moid] ) == 23 )
//     {
//       flag = true;
//       //cout<<"The lepton comes from a tau or a Z boson."<<endl;
//     }

//     if( abs( genpdgId[moid] ) == 24)
//     {
//       while( abs( genpdgId[moid] ) != 8000001)
//       {
// 	   moid = midx[moid];
// 	   if( moid > genpdgId.size() || moid < -1 ) break;
// 	   cout<<"moidx = "<<moid<<" "<<endl;
// 	   cout<<"Nature of its mother = "<<genpdgId[moid]<<" "<<endl;

// 	   if( abs( genpdgId[moid] ) == 15 || abs( genpdgId[moid] ) == 23 )
// 	   {
// 	     flag = false;
// 	     cout<<"The lepton comes from a tau or a Z boson."<<endl;
// 	     break;
// 	   }
//       }

//       if( abs( genpdgId[moid] ) == 8000001)
//       {
// 	   flag = true;
// 	   cout<<"yeaaaah"<<endl;
//       }
//     }

    out.emplace_back(flag);
    // cout<<"out = "<<out<<endl;
  }          
  return out;
}

bool isLdifferentcharge(ints &pdgId, ints &genpdgId, ints &lgenidx)
{
  // cout<<"B4B4B4"<<endl;
  bool out = false;
  unsigned int nl = pdgId.size();
  // cout<<"nl = "<<nl<<endl;

  // GenParticle loop 
  for(unsigned int i = 0; i < nl; i++)
  {
    // matching the lepton to the same lepton in the Generator tree
    int genlidx = lgenidx[i];
    // cout<<"Nature of the particle = "<<pdgId[i]<<" "<<endl;
    // cout<<"genlidx = "<<genlidx<<" "<<endl;
    // cout<<"Nature of the Gen particle = "<<genpdgId[genlidx]<<" "<<endl;
    if(pdgId[i] == -genpdgId[genlidx])
    {
      // cout<<"The particle and the Gen particle don't have the same charge."<<endl;
      out = true;
      break;
    }
  }

  // cout<<"out = "<<out<<endl;
  return out;
}

bool isLnonprompt(uchars &lgenflav)
{
  // cout<<"B5B5B5"<<endl;
	bool out = false;
  unsigned int nl = lgenflav.size();

	for(unsigned int i = 0; i < nl; i++)
  {
		int genpartflav = (int) lgenflav[i];
    // cout<<"Flavor of the particle = "<<genpartflav<<" "<<endl;
		if(genpartflav != 1)
    {
      // cout<<"The lepton is non prompt."<<endl;
      out = true;
      break;
    }
	}

  // cout<<"out = "<<out<<endl;
	return out;
}


ints MotherfromL (ints &pdgId, ints &genpdgId, ints &lgenidx, ints &midx)
{
  //cout<<"B6B6B6"<<endl;
  ints out = {-1, -1};
  unsigned int nl = pdgId.size();
  // cout<<"nl = "<<nl<<endl;
  bool flag = false;
  
  // GenParticle loop 
  for(unsigned int i = 0; i < nl; i++)
  {
    flag = false;
    // cout<<"i = "<<i<<endl;
    
    // matching the lepton to the same lepton in the Generator tree
    int genlidx = lgenidx[i];
    // cout<<"Nature of the particle = "<<pdgId[i]<<" "<<endl;
    // cout<<"genlidx = "<<genlidx<<" "<<endl;
    // cout<<"Nature of the Gen particle = "<<genpdgId[genlidx]<<" "<<endl;
    
    // tracking the mother
    int moid = midx[genlidx];
    //just to be sure if it is not out of the range... (otherwise it arises segmentation fault)
    // cout<<"moid = "<<moid<<" nature = "<<genpdgId[moid]<<endl;
    if( moid > genpdgId.size() || moid < -1 ) break;
    // cout<<"moidx = "<<moid<<" "<<endl;
    // cout<<"Nature of its mother = "<<genpdgId[moid]<<" "<<endl;
    
    // check if the lepton comes from a W
    while(abs( genpdgId[moid] ) == 13 || abs( genpdgId[moid] ) == 11)
    {
      moid = midx[moid];
      if( moid > genpdgId.size() || moid < -1 ) break;
      // cout<<"moidx = "<<moid<<" "<<endl;
      // cout<<"Nature of its mother = "<<genpdgId[moid]<<" "<<endl;
    }
    
    // cout<<"Nature of its mmother = "<<genpdgId[moid]<<" "<<endl;
    out[i] = genpdgId[moid];
    // cout<<"out = "<<out<<endl;
  }          
  return out;
}

bools isWfromHiggs (ints &genpdgId, ints &wgenidx, ints &midx, floats &genmass)
{
  // cout<<"B7B7B7"<<endl;
  bools out;
  unsigned int nl = wgenidx.size();
  // cout<<"nl = "<<nl<<endl;
  bool flag = false;
  // bool onshell = false;
  
  // GenParticle loop 
  for(unsigned int i = 0; i < nl; i++)
  {
    flag = false;
    // onshell = false;
    // cout<<"i = "<<i<<endl;
    
    int genlidx = wgenidx[i];
    // cout<<"genlidx = "<<genlidx<<" "<<endl;
    // cout<<"Nature of the Gen particle = "<<genpdgId[genlidx]<<", Mass of the Gen particle = "<<genmass[genlidx]<<" "<<endl;
    // if(genmass[genlidx] > 70 && genmass[genlidx] < 90)
    // {
    //   onshell = true;
    // }
    
    // tracking the mother
    int moid = midx[genlidx];
    //just to be sure if it is not out of the range... (otherwise it arises segmentation fault)
    // cout<<"moid = "<<moid<<" nature = "<<genpdgId[moid]<<endl;
    if( moid > genpdgId.size() || moid < -1 ) break;
    // cout<<"moidx = "<<moid<<" "<<endl;
    // cout<<"Nature of its mother = "<<genpdgId[moid]<<", Mass of the mother = "<<genmass[moid]<<" "<<endl;
    
    // check if the lepton comes from a W
    while( abs( genpdgId[moid] ) == 24)
    {
      moid = midx[moid];
      if( moid > genpdgId.size() || moid < -1 ) break;
      // cout<<"moidx = "<<moid<<" "<<endl;
      // cout<<"Nature of its mother = "<<genpdgId[moid]<<", Mass of the mother = "<<genmass[moid]<<" "<<endl;
    }
    
    if( abs( genpdgId[moid] ) == 25)// && onshell == true)
    {
      flag = true;
      // cout<<"yeaaaah"<<endl;
    }

    out.emplace_back(flag);
    // cout<<"out = "<<out<<endl;
  }          
  return out;
}

bools isWfromtop (ints &genpdgId, ints &wgenidx, ints &midx, floats &genmass)
{
  // cout<<"B8B8B8"<<endl;
  bools out;
  unsigned int nl = wgenidx.size();
  // cout<<"nl = "<<nl<<endl;
  bool flag = false;
  
  // GenParticle loop 
  for(unsigned int i = 0; i < nl; i++)
  {
    flag = false;
    //cout<<"i = "<<i<<endl;
    
    // matching the lepton to the same lepton in the Generator tree
    int genlidx = wgenidx[i];
    // cout<<"genlidx = "<<genlidx<<" "<<endl;
    // cout<<"Nature of the Gen particle = "<<genpdgId[genlidx]<<", Mass of the Gen particle = "<<genmass[genlidx]<<" "<<endl;
    
    // tracking the mother
    int moid = midx[genlidx];
    //just to be sure if it is not out of the range... (otherwise it arises segmentation fault)
    // cout<<"moid = "<<moid<<" nature = "<<genpdgId[moid]<<endl;
    if( moid > genpdgId.size() || moid < -1 ) break;
    // cout<<"moidx = "<<moid<<" "<<endl;
    // cout<<"Nature of its mother = "<<genpdgId[moid]<<", Mass of the mother = "<<genmass[moid]<<" "<<endl;
    
    // check if the lepton comes from a W
    while( abs( genpdgId[moid] ) == 24)
    {
      moid = midx[moid];
      if( moid > genpdgId.size() || moid < -1 ) break;
      // cout<<"moidx = "<<moid<<" "<<endl;
      // cout<<"Nature of its mother = "<<genpdgId[moid]<<", Mass of the mother = "<<genmass[moid]<<" "<<endl;
    }
    
    if( abs( genpdgId[moid] ) == 6)
    {
      flag = true;
      // cout<<"yeaaaah"<<endl;
    }

    out.emplace_back(flag);
    // cout<<"out = "<<out<<endl;
  }          
  return out;
}

bools isLfromtopantitop (ints &pdgId, ints &genpdgId, ints &lgenidx, ints &midx)//, floats &leptonpt)
{
  // cout<<"B9B9B9"<<endl;
  bools out;
  unsigned int nl = pdgId.size();
  // cout<<"nl = "<<nl<<endl;
  bool flag = false;
  bool wrong_decay = false;
  bool decay_W = false;
  bool decay_top = false;
  bool decay_antitop = false;

  // GenParticle loop 
  for(unsigned int i = 0; i < nl; i++)
  {
    flag = false;
    wrong_decay = false;
    decay_W = false;
    // cout<<"i = "<<i<<endl;
    
    // matching the lepton to the same lepton in the Generator tree
    int genlidx = lgenidx[i];
    // cout<<"Nature of the particle = "<<pdgId[i]<<" "<<endl;
    //cout<<"Lepton_pt = "<<leptonpt[i]<<" "<<endl;
    // cout<<"genlidx = "<<genlidx<<" "<<endl;
    // cout<<"Nature of the Gen particle = "<<genpdgId[genlidx]<<" "<<endl;
    //if(pdgId[i] == -genpdgId[genlidx])cout<<"The particle and the Gen particle don't have the same charge."<<endl;
    
    // tracking the mother
    int moid = midx[genlidx];
    //just to be sure if it is not out of the range... (otherwise it arises segmentation fault)
    // cout<<"moid = "<<moid<<" nature = "<<genpdgId[moid]<<endl;
    // if( moid > genpdgId.size() || moid < -1 ) break;
    // cout<<"moidx = "<<moid<<" "<<endl;
    // cout<<"Nature of its mother = "<<genpdgId[moid]<<" "<<endl;
    
    // check if the lepton comes from a W
    while( abs( genpdgId[moid] ) != 24 && (abs( genpdgId[moid] ) == 13 || abs( genpdgId[moid] ) == 11))
    {
      moid = midx[moid];
      // if( moid > genpdgId.size() || moid < -1 ) break;
      // cout<<"moidx = "<<moid<<" "<<endl;
      // cout<<"Nature of its mother = "<<genpdgId[moid]<<" "<<endl;
      if( abs( genpdgId[moid] ) == 5 )
      {
	      // cout<<"The lepton comes from a quark b."<<endl;
	      wrong_decay = true;
	      break;
      }
    }
    
    if( abs( genpdgId[moid] ) == 24 && wrong_decay == false )
    {
      decay_W = true;
      // cout<<"The lepton comes from a W."<<endl;
      int gmoid = midx[moid];
      //just to be sure if it is not out of the range... (otherwise it arises segmentation fault)
      if( gmoid > genpdgId.size() || gmoid < -1 ) break;
      // cout<<"Nature of its grandmother = "<<genpdgId[gmoid]<<" "<<endl;
      
      // make sure the lepton isn't coming from a quark b
      if( abs( genpdgId[gmoid] ) == 5 )
      {
	      // cout<<"The lepton comes from a quark b."<<endl;
	      wrong_decay = true;
	      break;
      }
      if( genpdgId[gmoid] == 6 )
      {
	      // cout<<"The lepton comes from a quark t."<<endl;
	      decay_top = true;
      }
      if( genpdgId[gmoid] == -6 )
      {
	      // cout<<"The lepton comes from an antiquark t."<<endl;
	      decay_antitop = true;
      }
      
      while( abs( genpdgId[gmoid] ) != 5)
      {
	      gmoid = midx[gmoid];
	      if( gmoid > genpdgId.size() || gmoid < -1 ) break;
	      // cout<<"gmoidx = "<<gmoid<<" "<<endl;
	      // cout<<"Nature of its grandmother = "<<genpdgId[gmoid]<<" "<<endl;
	      if( abs( genpdgId[gmoid] ) == 5 )
	      {
	        //cout<<"The lepton comes from a quark b."<<endl;
	        wrong_decay = true;
	        break;
	      }
	      if( genpdgId[gmoid] == 6 )
	      {	
	        // cout<<"The lepton comes from a quark t."<<endl;
	        decay_top = true;
	      }
	      if( genpdgId[gmoid] == -6 )
	      {
	        // cout<<"The lepton comes from an antiquark t."<<endl;
	        decay_antitop = true;
	      }	     
	      if( abs( genpdgId[gmoid] ) == 8000001 )
	      {
	        // cout<<"The lepton comes from the T'."<<endl;
	      }
      }
    }

    if( i == 0 && decay_W == true && wrong_decay == false && (decay_top == true || decay_antitop == true))
    {
      flag = true;
      // cout<<"yeaaaah"<<endl;
    }
    if( i == 1 && decay_W == true && wrong_decay == false && decay_top == true && decay_antitop == true)
    {
      flag = true;
      // cout<<"yeaaaah"<<endl;
    }
    out.emplace_back(flag);
    // cout<<"out = "<<out<<endl;
  }          
  return out;
}

FourVectorVec isLfromtoporantitop (ints &pdgId, ints &genpdgId, ints &lgenidx, ints &midx, FourVectorVec &genleptons4vecs)
{
  // cout<<"B10B10B10"<<endl;
  unsigned int nl = pdgId.size();
  // cout<<"nl = "<<nl<<endl;
  bool flag = false;
  bool wrong_decay = false;
  bool decay_W = false;
  bool decay_top = false;
  bool decay_antitop = false;
  bool top_leading = false;
  bool antitop_leading = false;
  
  // GenParticle loop 
  for(unsigned int i = 0; i < nl; i++)
  {
    flag = false;
    wrong_decay = false;
    decay_W = false;
    // cout<<"i = "<<i<<endl;
    
    // matching the lepton to the same lepton in the Generator tree
    int genlidx = lgenidx[i];
    // cout<<"Nature of the particle = "<<pdgId[i]<<" "<<endl;
    // cout<<"genlidx = "<<genlidx<<" "<<endl;
    // cout<<"Nature of the Gen particle = "<<genpdgId[genlidx]<<" "<<endl;
    
    // tracking the mother
    int moid = midx[genlidx];
    //just to be sure if it is not out of the range... (otherwise it arises segmentation fault)
    // if( moid > genpdgId.size() || moid < -1 ) break;
    // cout<<"moidx = "<<moid<<" "<<endl;
    // cout<<"Nature of its mother = "<<genpdgId[moid]<<" "<<endl;
    
    // check if the lepton comes from a W
    while( abs( genpdgId[moid] ) != 24 && (abs( genpdgId[moid] ) == 13 || abs( genpdgId[moid] ) == 11))
    {
      moid = midx[moid];
      // if( moid > genpdgId.size() || moid < -1 ) break;
      // cout<<"moidx = "<<moid<<" "<<endl;
      // cout<<"Nature of its mother = "<<genpdgId[moid]<<" "<<endl;
      if( abs( genpdgId[moid] ) == 5 )
      {
	      //cout<<"The lepton comes from a quark b."<<endl;
	      wrong_decay = true;
	      break;
      }
    }
    
    if( abs( genpdgId[moid] ) == 24 && wrong_decay == false )
    {
      decay_W = true;
      // cout<<"The lepton comes from a W."<<endl;
      int gmoid = midx[moid];
      //just to be sure if it is not out of the range... (otherwise it arises segmentation fault)
      if( gmoid > genpdgId.size() || gmoid < -1 ) break;
      // cout<<"Nature of its grandmother = "<<genpdgId[gmoid]<<" "<<endl;
      
      // make sure the lepton isn't coming from a quark b
      if( abs( genpdgId[gmoid] ) == 5 )
      {
	      //cout<<"The lepton comes from a quark b."<<endl;
	      wrong_decay = true;
	      break;
      }
      if( genpdgId[gmoid] == 6 )
      {
	      // cout<<"The lepton comes from a quark t."<<endl;
	      decay_top = true;
      }
      if( genpdgId[gmoid] == -6 )
      {
	      // cout<<"The lepton comes from an antiquark t."<<endl;
	      decay_antitop = true;
      }
      
      while( abs( genpdgId[gmoid] ) != 5)
      {
	      gmoid = midx[gmoid];
	      if( gmoid > genpdgId.size() || gmoid < -1 ) break;
	      // cout<<"gmoidx = "<<gmoid<<" "<<endl;
	
	      // cout<<"Nature of its grandmother = "<<genpdgId[gmoid]<<" "<<endl;
	      if( abs( genpdgId[gmoid] ) == 5 )
	      {
	        //cout<<"The lepton comes from a quark b."<<endl;
	        wrong_decay = true;
	        break;
	      }
	      if( genpdgId[gmoid] == 6 )
	      {
	        // cout<<"The lepton comes from a quark t."<<endl;
	        decay_top = true;
	      }
	      if( genpdgId[gmoid] == -6 )
	      {
	        // cout<<"The lepton comes from an antiquark t."<<endl;
	        decay_antitop = true;
	      }	     
	      if( abs( genpdgId[gmoid] ) == 8000001 )
	      {
	        //cout<<"The lepton comes from the T'."<<endl;
	      }
      }
    }
    
    if( i == 0 && decay_W == true && wrong_decay == false && decay_top == true)
    {
      top_leading = true;
      // cout<<"yeaaaah"<<endl;
    }
    if( i == 1 && decay_W == true && wrong_decay == false && decay_top == true && decay_antitop == true && top_leading == true)
    {
      // cout<<"yeaaaah"<<endl;
    }
    if( i == 0 && decay_W == true && wrong_decay == false && decay_antitop == true)
    {
      antitop_leading = true;
      // cout<<"yeaaaah"<<endl;
    }
    if( i == 1 && decay_W == true && wrong_decay == false && decay_top == true && decay_antitop == true && antitop_leading == true)
    {
      swap(genleptons4vecs[0],genleptons4vecs[1]);
      // cout<<"yeaaaah"<<endl;
      // cout<<"Swap!"<<endl;
    }
  }          
  return genleptons4vecs;
}

bools isWfromtoponly (ints &genpdgId, ints &wgenidx, ints &midx, floats &genmass)
{
  // cout<<"B11B11B11"<<endl;
  bools out;
  unsigned int nl = wgenidx.size();
  // cout<<"nl = "<<nl<<endl;
  bool flag = false;
  
  // GenParticle loop 
  for(unsigned int i = 0; i < nl; i++)
  {
    flag = false;
    // cout<<"i = "<<i<<endl;
    
    // matching the lepton to the same lepton in the Generator tree
    int genlidx = wgenidx[i];
    // cout<<"genlidx = "<<genlidx<<" "<<endl;
    // cout<<"Nature of the Gen particle = "<<genpdgId[genlidx]<<", Mass of the Gen particle = "<<genmass[genlidx]<<" "<<endl;
    
    // tracking the mother
    int moid = midx[genlidx];
    //just to be sure if it is not out of the range... (otherwise it arises segmentation fault)
    // cout<<"moid = "<<moid<<" nature = "<<genpdgId[moid]<<endl;
    if( moid > genpdgId.size() || moid < -1 ) break;
    // cout<<"moidx = "<<moid<<" "<<endl;
    // cout<<"Nature of its mother = "<<genpdgId[moid]<<", Mass of the mother = "<<genmass[moid]<<" "<<endl;
    
    // check if the lepton comes from a W
    while( abs( genpdgId[moid] ) == 24)
    {
      moid = midx[moid];
      if( moid > genpdgId.size() || moid < -1 ) break;
      // cout<<"moidx = "<<moid<<" "<<endl;
      // cout<<"Nature of its mother = "<<genpdgId[moid]<<", Mass of the mother = "<<genmass[moid]<<" "<<endl;
    }
    
    if( genpdgId[moid] == 6)
    {
      flag = true;
      // cout<<"yeaaaah"<<endl;
    }

    out.emplace_back(flag);
    // cout<<"out = "<<out<<endl;
  }          
  return out;
}

bools isWfromantitop (ints &genpdgId, ints &wgenidx, ints &midx, floats &genmass)
{
  // cout<<"B12B12B12"<<endl;
  bools out;
  unsigned int nl = wgenidx.size();
  // cout<<"nl = "<<nl<<endl;
  bool flag = false;
  
  // GenParticle loop 
  for(unsigned int i = 0; i < nl; i++)
  {
    flag = false;
    // cout<<"i = "<<i<<endl;
    
    // matching the lepton to the same lepton in the Generator tree
    int genlidx = wgenidx[i];
    // cout<<"genlidx = "<<genlidx<<" "<<endl;
    // cout<<"Nature of the Gen particle = "<<genpdgId[genlidx]<<", Mass of the Gen particle = "<<genmass[genlidx]<<" "<<endl;
    
    // tracking the mother
    int moid = midx[genlidx];
    //just to be sure if it is not out of the range... (otherwise it arises segmentation fault)
    // cout<<"moid = "<<moid<<" nature = "<<genpdgId[moid]<<endl;
    if( moid > genpdgId.size() || moid < -1 ) break;
    // cout<<"moidx = "<<moid<<" "<<endl;
    // cout<<"Nature of its mother = "<<genpdgId[moid]<<", Mass of the mother = "<<genmass[moid]<<" "<<endl;
    
    // check if the lepton comes from a W
    while( abs( genpdgId[moid] ) == 24)
    {
      moid = midx[moid];
      if( moid > genpdgId.size() || moid < -1 ) break;
      // cout<<"moidx = "<<moid<<" "<<endl;
      // cout<<"Nature of its mother = "<<genpdgId[moid]<<", Mass of the mother = "<<genmass[moid]<<" "<<endl;
    }
    
    if( genpdgId[moid] == -6)
    {
      flag = true;
      // cout<<"yeaaaah"<<endl;
    }

    out.emplace_back(flag);
    // cout<<"out = "<<out<<endl;
  }          
  return out;
}

bools isPfromtopHiggs (ints &genpdgId, ints &midx, ints &lgenidx)
{
  // cout<<"D1D1D1"<<endl;
  bools out;
  unsigned int np = genpdgId.size();
  // cout<<"\n np = "<<np<<endl;
  bool flag = false;
  bool lepton = false;
  bool decay_top = false;
  bool decay_Higgs = false;
  
  // GenParticle loop 
  for(unsigned int i = 0; i < np; i++)
  {
    flag = false;
    decay_top = false;
    decay_Higgs = false;
    lepton = false;
    // cout<<"i = "<<i<<endl;
    // cout<<"Nature of the particle = "<<genpdgId[i]<<" "<<endl;
    if(lgenidx.size()==2)
    {
      if(i == lgenidx[0] || i == lgenidx[1])
      {
        // cout<<"The particle is a selected electron or a muon."<<endl;
        lepton = true;
      }
    }
    
    // tracking the mother
    int moid = midx[i];
    // cout<<"moidx = "<<moid<<" "<<endl;
    // cout<<"Nature of its mother = "<<genpdgId[moid]<<" "<<endl;
    
    // check if the particle comes from a top or a Higgs
    while(abs(genpdgId[moid]) != 6 && abs(genpdgId[moid]) != 25 && lepton == false)
    {
      moid = midx[moid];
      if( moid > genpdgId.size() || moid < -1 ) break;
      // cout<<"moidx = "<<moid<<" "<<endl;
      // cout<<"Nature of its mother = "<<genpdgId[moid]<<" "<<endl;
    }
    
    if(abs(genpdgId[moid]) == 6)
    {
      // cout<<"The particle comes from a quark t."<<endl;
      decay_top = true;
    }
    
    if(abs(genpdgId[moid]) == 25)
    {
      // cout<<"The particle comes from a Higgs boson."<<endl;
      decay_Higgs = true;
    }
    
    if((decay_top == true || decay_Higgs == true) && lepton == false)
    {
      // int gmoid = midx[gmoid];
      //cout<<"gmoidx = "<<gmoid<<" "<<endl;
      //cout<<"Nature of its grandmother = "<<genpdgId[gmoid]<<" "<<endl;	     
      // if(abs(genpdgId[gmoid]) == 8000001)
      // {
	      //cout<<"The particle comes from the T'."<<endl;
      // }
      flag = true;
      //cout<<"yeaaaah"<<endl;
    }
    out.emplace_back(flag);
    // cout<<"out = "<<out<<endl;
  }          
  return out;
}

bools isPfromHiggs (ints &genpdgId, ints &midx, ints &lgenidx)
{
  // cout<<"D2D2D2"<<endl;
  bools out;
  unsigned int np = genpdgId.size();
  // cout<<"np = "<<np<<endl;
  bool flag = false;
  bool lepton = false;
  bool decay_Higgs = false;
  
  // GenParticle loop 
  for(unsigned int i = 0; i < np; i++)
  {
    flag = false;
    decay_Higgs = false;
    lepton = false;
    // cout<<"i = "<<i<<endl;
    // cout<<"Nature of the particle = "<<genpdgId[i]<<" "<<endl;
    if(lgenidx.size()==2)
    {
      if(i == lgenidx[0] || i == lgenidx[1])
      {
        // cout<<"The particle is a selected electron or a muon."<<endl;
        lepton = true;
      }
    }
    
    // tracking the mother
    int moid = midx[i];
    // cout<<"moidx = "<<moid<<" "<<endl;
    // cout<<"Nature of its mother = "<<genpdgId[moid]<<" "<<endl;
    
    // check if the particle comes from a top or a Higgs
    while(abs(genpdgId[moid]) != 25 && lepton == false)
    {
      moid = midx[moid];
      if( moid > genpdgId.size() || moid < -1 ) break;
      // cout<<"moidx = "<<moid<<" "<<endl;
      // cout<<"Nature of its mother = "<<genpdgId[moid]<<" "<<endl;
    }
      
    if(abs(genpdgId[moid]) == 25)
    {
      // cout<<"The particle comes from a Higgs boson."<<endl;
      decay_Higgs = true;
    }
    
    if(decay_Higgs == true && lepton == false)
    { 
      int gmoid = midx[moid];
      // cout<<"gmoidx = "<<gmoid<<" "<<endl;
      // cout<<"Nature of its grandmother = "<<genpdgId[gmoid]<<" "<<endl;	     
      if(abs(genpdgId[gmoid]) == 8000001)
      {
      	// cout<<"The particle comes from the T'."<<endl;
      }
      flag = true;
      // cout<<"yeaaaah"<<endl;
    }
    out.emplace_back(flag);
    // cout<<"out = "<<out<<endl;
  }          
  return out;
}

bools isPfromtop (ints &genpdgId, ints &midx, ints &lgenidx)
{
  // cout<<"D3D3D3"<<endl;
  bools out;
  unsigned int np = genpdgId.size();
  // cout<<"np = "<<np<<endl;
  bool flag = false;
  bool lepton = false;
  bool decay_top = false;
  
  // GenParticle loop 
  for(unsigned int i = 0; i < np; i++)
  {
    flag = false;
    decay_top = false;
    lepton = false;
    // cout<<"i = "<<i<<endl;
    // cout<<"Nature of the particle = "<<genpdgId[i]<<" "<<endl;
    if(lgenidx.size()==2)
    {
      if(i == lgenidx[0] || i == lgenidx[1])
      {
        // cout<<"The particle is a selected electron or a muon."<<endl;
        lepton = true;
      }
    }

    // tracking the mother
    int moid = midx[i];
    // cout<<"moidx = "<<moid<<" "<<endl;
    // cout<<"Nature of its mother = "<<genpdgId[moid]<<" "<<endl;
    
    // check if the particle comes from a top or a Higgs
    while(abs(genpdgId[moid]) != 6 && lepton == false)
    {
      moid = midx[moid];
      if( moid > genpdgId.size() || moid < -1 ) break;
      // cout<<"moidx = "<<moid<<" "<<endl;
      // cout<<"Nature of its mother = "<<genpdgId[moid]<<" "<<endl;
    }
    
    if(abs(genpdgId[moid]) == 6)
    {
      //cout<<"The particle comes from a quark t."<<endl;
      decay_top = true;
    }
    
    if(decay_top == true && lepton == false)
    {
      // int gmoid = midx[gmoid];
      // cout<<"gmoidx = "<<gmoid<<" "<<endl;
      // cout<<"Nature of its grandmother = "<<genpdgId[gmoid]<<" "<<endl;	     
      // if(abs(genpdgId[gmoid]) == 8000001)
      // {
	      //cout<<"The particle comes from the T'."<<endl;
      // }
      flag = true;
      //cout<<"yeaaaah"<<endl;
    }
    out.emplace_back(flag);
    //cout<<"out = "<<out<<endl;
  }          
  return out;
}

bools isPfromtoponly (ints &genpdgId, ints &midx, ints &lgenidx)
{
  // cout<<"D4D4D4"<<endl;
  bools out;
  unsigned int np = genpdgId.size();
  // cout<<"np = "<<np<<endl;
  bool flag = false;
  bool lepton = false;
  bool decay_top = false;
  
  // GenParticle loop 
  for(unsigned int i = 0; i < np; i++)
  {
    flag = false;
    decay_top = false;
    lepton = false;
    // cout<<"i = "<<i<<endl;
    // cout<<"Nature of the particle = "<<genpdgId[i]<<" "<<endl;
    if(lgenidx.size()==2)
    {
      if(i == lgenidx[0] || i == lgenidx[1])
      {
        // cout<<"The particle is a selected electron or a muon."<<endl;
        lepton = true;
      }
    }

    // tracking the mother
    int moid = midx[i];
    // cout<<"moidx = "<<moid<<" "<<endl;
    // cout<<"Nature of its mother = "<<genpdgId[moid]<<" "<<endl;
    
    // check if the particle comes from a top or a Higgs
    while(genpdgId[moid] != 6 && lepton == false)
    {
      moid = midx[moid];
      if( moid > genpdgId.size() || moid < -1 ) break;
      // cout<<"moidx = "<<moid<<" "<<endl;
      // cout<<"Nature of its mother = "<<genpdgId[moid]<<" "<<endl;
    }
    
    if(genpdgId[moid] == 6)
    {
      // cout<<"The particle comes from a quark t."<<endl;
      decay_top = true;
    }
    
    if(decay_top == true && lepton == false)
    {
      // int gmoid = midx[gmoid];
      // cout<<"gmoidx = "<<gmoid<<" "<<endl;
      // cout<<"Nature of its grandmother = "<<genpdgId[gmoid]<<" "<<endl;	     
      // if(abs(genpdgId[gmoid]) == 8000001)
      // {
	      //cout<<"The particle comes from the T'."<<endl;
      // }
      flag = true;
      // cout<<"yeaaaah"<<endl;
    }
    out.emplace_back(flag);
    // cout<<"out = "<<out<<endl;
  }          
  return out;
}

bools isPfromantitop (ints &genpdgId, ints &midx, ints &lgenidx)
{
  // cout<<"D5D5D5"<<endl;
  bools out;
  unsigned int np = genpdgId.size();
  // cout<<"np = "<<np<<endl;
  bool flag = false;
  bool lepton = false;
  bool decay_antitop = false;
  
  // GenParticle loop 
  for(unsigned int i = 0; i < np; i++)
  {
    flag = false;
    decay_antitop = false;
    lepton = false;
    // cout<<"i = "<<i<<endl;
    // cout<<"Nature of the particle = "<<genpdgId[i]<<" "<<endl;
    if(lgenidx.size()==2)
    {
      if(i == lgenidx[0] || i == lgenidx[1])
      {
        // cout<<"The particle is a selected electron or a muon."<<endl;
        lepton = true;
      }
    }

    // tracking the mother
    int moid = midx[i];
    // cout<<"moidx = "<<moid<<" "<<endl;
    // cout<<"Nature of its mother = "<<genpdgId[moid]<<" "<<endl;
    
    // check if the particle comes from a top or a Higgs
    while(genpdgId[moid] != -6 && lepton == false)
    {
      moid = midx[moid];
      if( moid > genpdgId.size() || moid < -1 ) break;
      // cout<<"moidx = "<<moid<<" "<<endl;
      // cout<<"Nature of its mother = "<<genpdgId[moid]<<" "<<endl;
    }
    
    if(genpdgId[moid] == -6)
    {
      // cout<<"The particle comes from an antiquark t."<<endl;
      decay_antitop = true;
    }
    
    if(decay_antitop == true && lepton == false)
    {
      // int gmoid = midx[gmoid];
      // cout<<"gmoidx = "<<gmoid<<" "<<endl;
      // cout<<"Nature of its grandmother = "<<genpdgId[gmoid]<<" "<<endl;	     
      // if(abs(genpdgId[gmoid]) == 8000001)
      // {
	      //cout<<"The particle comes from the T'."<<endl;
      // }
      flag = true;
      // cout<<"yeaaaah"<<endl;
    }
    out.emplace_back(flag);
    // cout<<"out = "<<out<<endl;
  }          
  return out;
}

bools isJetFromgJet (floats &jet_pt, floats &gjet_pt, floats &gjetTp_pt, ints &jet_gidx)//, floats &jet_eta, floats &jet_phi, floats &jet_mass, ints &jet_jetid, floats &gjet_eta, floats &gjet_phi, floats &gjet_mass)
{
  // cout<<"F1F1F1"<<endl;
  bools out;
  for( unsigned int i = 0; i < jet_pt.size(); i++ )
  {
    // cout<<"i = "<<i<<endl;
    bool isFromTprime = false;
    //index of genJet matched with Jet i
    int gIdx = jet_gidx[i];
    // cout<<"jet_pt[i] = "<<jet_pt[i]<<", gIdx = "<<gIdx<<", gjet_pt[gIdx] = "<<gjet_pt[gIdx]<<endl;
//     cout<<"jet_eta[i] = "<<jet_eta[i]<<", gjet_eta[gIdx] = "<<gjet_eta[gIdx]<<endl;	   
//     cout<<"jet_phi[i] = "<<jet_phi[i]<<", gjet_phi[gIdx] = "<<gjet_phi[gIdx]<<endl;
//     cout<<"jet_mass[i] = "<<jet_mass[i]<<", gjet_mass[gIdx] = "<<gjet_mass[gIdx]<<endl;
//     //cout<<"jet_energy[i] = "<<jet_energy[i]<<", gjet_energy[gIdx] = "<<gjet_energy[gIdx]<<endl;
//     cout<<"jet_jetId[i] = "<<jet_jetid[i]<<endl;
    if( gIdx <= int(gjet_pt.size()) && gIdx > -1 )
    {
      // cout<<"F11F11F11"<<endl;
      for( unsigned int j = 0; j < gjetTp_pt.size(); j++ )
      {
	      // cout<<"j = "<<j<<endl;
	      // cout<<"gjetTp_pt[j] = "<<gjetTp_pt[j]<<endl;
	      if( abs((gjet_pt[gIdx] - gjetTp_pt[j])) < 0.01 )
	      {
	        isFromTprime = true;
	        // if(gjet_pt[gIdx] != gjetTp_pt[j]) cout<<"gjet_pt[gIdx] - gjetTp_pt[j] = "<<gjet_pt[gIdx] - gjetTp_pt[j]<<endl;
	        // cout<<"yeaaaah"<<endl;
	      }
      }
    }
    out.emplace_back(isFromTprime);
    // cout<<"out = "<<out<<endl;
  }
  return out;
}

bools isJetFromgJet_2 (double &jet_pt, floats &gjet_pt, floats &gjetTp_pt, int &jet_gidx)
{
  //cout<<"F2F2F2"<<endl;
  bools out;
  bool isFromTprime = false;
  int gIdx = jet_gidx;
  if( gIdx <= int(gjet_pt.size()) && gIdx > -1 )
  {
    for( unsigned int j = 0; j < gjetTp_pt.size(); j++ )
    {
      //cout<<"j = "<<j<<endl;
      //cout<<"jet_pt = "<<jet_pt<<", gjet_pt[gIdx] = "<<gjet_pt[gIdx]<<", gjetTp_pt[j] = "<<gjetTp_pt[j]<<endl;
      if( abs((gjet_pt[gIdx] - gjetTp_pt[j])) < 0.01 )
      {
	isFromTprime = true;
	//cout<<"yeaaaah"<<endl;
      }
    }
  }
  out.emplace_back(isFromTprime);
  //cout<<"out = "<<out<<endl;
  return out;
}

ints good_idx(ints good)
{
	ints out;
	for(int i = 0; i < int(good.size()); i++)
  {
		if( good[i] )
    {
			out.emplace_back(i);
		}
	}
	return out;
}

floats concatenate( double first, double second)
{
  // cout<<"A1A1A1"<<endl;
  floats out;
  out.emplace_back(first);
  out.emplace_back(second);
  // cout<<"out = "<<out<<endl;
  return out;
}

bools isdR04 (floats &eta1, floats &eta2, floats &phi1, floats &phi2)
{
  // cout<<"EEE"<<endl;
  bools out;
  for( unsigned int i = 0; i < eta1.size(); i++ )
  {
    // cout<<"i = "<<i<<endl;
    bool isless04 = false;	 
    float mindr = FLT_MAX;
    // cout<<"isless04 = "<<isless04<<", mindr = "<<mindr<<endl;
    for( unsigned int j = 0; j < eta2.size(); j++ )
    {
      // cout<<"j = "<<j<<endl;
      auto dr = ROOT::VecOps::DeltaR(eta1[i],eta2[j],phi1[i],phi2[j]);
      if( dr < mindr ) mindr = dr;
      // cout<<"mindr = "<<mindr<<endl;
    }
    if( mindr < 0.4 ) isless04 = true;
    // if(isless04 == true) cout<<" mindr is lower than 0.4."<<endl;	 
    out.emplace_back(isless04);
    // cout<<"out = "<<out<<endl;
  }
  return out;
}