/*
 * NanoAODAnalyzerrdframe.cpp
 *
 *  Created on: Sep 30, 2018
 *      Author: suyong
 *  Updated on: 10Oct, 2023
 *      Author: Arnab PUROHIT, IP2I, Lyon 
 */

#include "NanoAODAnalyzerrdframe.h"
#include <iostream>
#include <algorithm>
#include <typeinfo>
#include <random>

#include "TCanvas.h"
#include "Math/GenVector/VectorUtil.h"
#include <vector>
#include <fstream>
#include "utility.h"
#include <regex>
#include "ROOT/RDFHelpers.hxx"

using namespace std;

NanoAODAnalyzerrdframe::NanoAODAnalyzerrdframe(TTree *atree, std::string outfilename)
:_rd(*atree), _jsonOK(false),_outfilename(outfilename)
	, _outrootfile(0), _rlm(_rd)
	, _rnt(&_rlm) , PDFWeights(103, 0.0), PSWeights(4, 0.0), ScaleWeights(9, 0.0) //PDFWeights(103, 0.0)
{
	_atree=atree;
	//cout<<" run year====="<<_year <<endl;
	// if genWeight column exists, then it is not real data
	//

}

NanoAODAnalyzerrdframe::~NanoAODAnalyzerrdframe()
{
	// TODO Auto-generated destructor stub
}

bool NanoAODAnalyzerrdframe::isDefined(string v)
{
	auto result = std::find(_originalvars.begin(), _originalvars.end(), v);
	if(result != _originalvars.end()) return true;
	else return false;
}

void NanoAODAnalyzerrdframe::setTree(TTree *t, std::string outfilename)
{
	_rd = ROOT::RDataFrame(*t);
	_rlm = RNode(_rd);
	_outfilename = outfilename;
	_hist1dinfovector.clear();
	_th1dhistos.clear();
	_varstostore.clear();
	_hist2dinfovector.clear();
	_th2dhistos.clear();
	_hist2dvbininfovector.clear();
	_th2dvbinhistos.clear();
	_selections.clear();

	this->setupAnalysis();
}

void NanoAODAnalyzerrdframe::setupAnalysis()
{
	// Event weight for data it's always one. For MC, it depends on the sign
 	//cout<<"year===="<<_year<<"==runtype=== "<<_runtype <<endl;
	_rlm = _rlm.Define("one", "1.0");
	if(_isData && !isDefined("evWeight"))
	{
		_rlm = _rlm.Define("evWeight", [](){
				return 1.0;
			},{});
	}
	// Store PDF sum of weights
   /*if(!_isData){
            auto storeWeights = [this](floats weights)->floats {

                for(unsigned int i=0; i<weights.size(); i++)
                    PDFWeights[i] += weights[i];

                return PDFWeights;
            };
            try {
                _rlm.Foreach(storeWeights,{"LHEPdfWeight"});
            } catch (exception& e){
                cout<<e.what()<<endl;
                cout<<"No PDF weight in this root file!"<<endl;
            }
	}*/
	jes_unc_level_start_idx = "0_0";

	setupCuts_and_Hists(jes_unc_level_start_idx);
	setupTree();
}

bool NanoAODAnalyzerrdframe::readgoodjson(string goodjsonfname)
{
	
	if (goodjsonfname != "")
	{
		std::ifstream jsoninfile;
		jsoninfile.open(goodjsonfname);

		if(jsoninfile.good())
		{
			//using rapidjson
			//rapidjson::IStreamWrapper s(jsoninfile);
			//jsonroot.ParseStream(s);

			//using jsoncpp
			jsoninfile >> jsonroot;

			auto isgoodjsonevent = [jsonroot = this->jsonroot](unsigned int runnumber, unsigned int lumisection)
			{
			    auto key = std::to_string(runnumber).c_str();
			    bool goodeventflag = false;
			    
			    if (jsonroot.contains(key))
			    {
					for (auto &v: jsonroot[key])
				  	{
				    	if (v[0]<=lumisection && lumisection <=v[1])
						{
				    	  	goodeventflag = true;
				    	}
				  	}
			    }
			    return goodeventflag;
			};
			
			_rlm = _rlm.Define("goodjsonevent", isgoodjsonevent, {"run", "luminosityBlock"}).Filter("goodjsonevent");
			_jsonOK = true;
			return true;
		}
		else
		{
		    cout << "Problem reading json file " << goodjsonfname << endl;
		    return false;
	  	}
	}
	else
	{
	    cout << "no JSON file given" << endl;
	    return true;
	}
}

void NanoAODAnalyzerrdframe::selectFatJets()
{
	_rlm = _rlm.Define("fatjetcuts", "FatJet_pt>400.0 && abs(FatJet_eta)<2.4 && FatJet_tau1>0.0 && FatJet_tau2>0.0 && FatJet_tau3>0.0 && FatJet_tau3/FatJet_tau2<0.5")
				.Define("Sel_fatjetpt", "FatJet_pt[fatjetcuts]")
				.Define("Sel_fatjeteta", "FatJet_eta[fatjetcuts]")
				.Define("Sel_fatjetphi", "FatJet_phi[fatjetcuts]")
				.Define("Sel_fatjetmass", "FatJet_mass[fatjetcuts]")
				.Define("nfatjetspass", "int(Sel_fatjetpt.size())")
				.Define("Sel_fatjetweight", "std::vector<double>(nfatjetspass, evWeight)")
				.Define("Sel_fatjet4vecs", ::generate_4vec,{"Sel_fatjetpt", "Sel_fatjeteta", "Sel_fatjetphi", "Sel_fatjetmass"});
}

void NanoAODAnalyzerrdframe::removeHEMEvents()
{

  	auto func_hem_mask = [](floats jetetas, floats jetphis)->bool
    {
    	int not_HEM_Event = 1;
      	for(auto i =0; i<int(jetetas.size()); i++)
		{
	  		if(jetphis[i] > -1.57 && jetphis[i] < -0.87 && jetetas[i] > -2.5 && jetetas[i] < -1.3)
			{
	    		not_HEM_Event = 0;
	    		//std::cout<<"HEM affected event found!!!!"<<std::endl;
	    		//std::cout<<"The Jet's eta and phi are "<<jetetas[i]<<", "<<jetphis[i]<<std::endl;
	    		break;
	  		}
		}
      	return not_HEM_Event;
    };

    _rlm = _rlm.Define("not_HEM_Affected_Event", func_hem_mask,{"Selected_clean_jet_eta", "Selected_clean_jet_phi"}).Filter("not_HEM_Affected_Event");
}

//void NanoAODAnalyzerrdframe::setupJetMETCorrection(string fname, string jettag, const std::vector<std::string> _jercunctag, string jertag, string fname_metphimod) //data
void NanoAODAnalyzerrdframe::setupJetMETCorrection(const string& fname, const string& jettag, const vector<string>& jercUncTags, const string& jertag, const string& fname_metphimod) //data
{

    cout<<"SETUP JETMET correction"<<endl;
	// read from file 
	_correction_jerc = correction::CorrectionSet::from_file(fname);//jercfname=json
	//assert(_correction_jerc->validate()); //the assert functionality : check if the parameters passed to a function are valid =1:true
	if (!_correction_jerc->validate())
	{
		throw std::runtime_error("Failed to validate correction set from " + fname);
	}
	// correction type(jobconfiganalysis.py)
	cout<<"JERC JSON file : "<<fname<<endl;
	std::string level = "_L1L2L3Res";
	std::string algo = "_AK4PFchs";
	_jetCorrector = _correction_jerc->compound().at(jettag+level+algo);//jerctag#JSON (JEC,compound)compoundLevel="L1L2L3Res"
	cout<<"JET tag in JSON : "<<jettag<<endl;
	if(!_jercunctag.empty() && !_isData)
	{
		_jetCorrectionUnc.reserve(_jercunctag.size());
	  	//for(std::string jerc_unc_tag : _jercunctag){
	  	for(const std::string& jerc_unc_tag : _jercunctag)
		{
	    	if(jerc_unc_tag=="JER") continue;
	    	cout<<"JET uncertainity tag in JSON : "<<endl;
	    	cout<<jettag+"_"+jerc_unc_tag+algo<<endl;
	    	_jetCorrectionUnc.emplace_back(_correction_jerc->at(jettag+"_"+jerc_unc_tag+algo));
	  	}
	  	std::cout<<"================================//================================="<<std::endl;
	}
	std::string pt_label = "_PtResolution";
	_jetResCorrector = _correction_jerc->at(jertag+pt_label+algo);//jet energy resolution tag
	std::string sf_label = "_ScaleFactor";
	_jetResCorrector_SF = _correction_jerc->at(jertag+sf_label+algo);//jet energy resolution SF tag

	_metphiCorrector = correction::CorrectionSet::from_file(fname_metphimod);//jercfname=json
	assert(_metphiCorrector->validate()); //the assert functionality : check if the parameters passed to a function are valid =1:true
	// correction type(jobconfiganalysis.py)
	cout<<"MET Phi Modulation Correction JSON file : "<<fname_metphimod<<endl;
}

void NanoAODAnalyzerrdframe::applyJetMETCorrections() //data
{
    cout<<"apply JETMET correction"<<endl;

	auto appcorrlambdaf = [_jetCorrector = this->_jetCorrector](const floats& jetpts, const floats& jetetas, const floats& jetAreas, const floats& jetrawf, const floats& vars_to_corr, const float& rho)->floats
    {
    	floats corrfactors;
      	corrfactors.reserve(jetpts.size());
      	for(auto i =0; i<int(jetpts.size()); i++)
		{
	  		float rawjetpt = jetpts[i]*(1.0-jetrawf[i]);
	  		float rawjetvar = vars_to_corr[i]*(1.0-jetrawf[i]);
	  		// std::cout<<"jetpt===="<<jetpts[i] <<std::endl;
	  		// std::cout<<"rawjetpt===="<<rawjetpt <<std::endl;
	  		float corrfactor = 1;
	  		if(rawjetvar<6500)
			{
	    		corrfactor = _jetCorrector->evaluate({jetAreas[i], jetetas[i], rawjetpt, rho});
	  		}
	  		// std::cout<<"correction factor===="<<corrfactor <<std::endl;
	  		corrfactors.emplace_back(rawjetvar * corrfactor);
	  		// std::cout<<"rawjetpt* corrfactor ===="<<rawjetpt * corrfactor <<std::endl;
	  
		}
      	// std::cout<<"Facss===="<<corrfactors<<std::endl;
      	return corrfactors;
      
    };

	auto jecuncertaintylambdaf= [_jetCorrector = this->_jetCorrector, _jetCorrectionUnc = this->_jetCorrectionUnc](const floats& jetpts, const floats& jetetas, const floats& jetAreas, const floats& jetrawf, const floats& vartocorr, const float& rho, const int&jec_unc_idx, string unc_type)->floats
    {
    	floats uncertainties;
      	uncertainties.reserve(jetpts.size());
      	for(auto i =0; i<int(jetpts.size()); i++)
		{
	  		float rawjetpt = jetpts[i]*(1.0-jetrawf[i]);
	  		float corrfactor = 1;
	  		float corr_jet_pt = 0;
	  		float var_to_corr_withunc = 0.0;
	  		float corrfactor_unc = 1;
	  		if(rawjetpt<6500)
			{
	    		corrfactor = _jetCorrector->evaluate({jetAreas[i], jetetas[i], rawjetpt, rho});
	    		corr_jet_pt = corrfactor*rawjetpt;
	    
	    		if(unc_type=="up")
				{
	      			corrfactor_unc = corrfactor+_jetCorrectionUnc[jec_unc_idx]->evaluate({jetetas[i], rawjetpt});
	    		}
	    		else if(unc_type=="down")
				{
	      			corrfactor_unc = corrfactor-_jetCorrectionUnc[jec_unc_idx]->evaluate({jetetas[i], rawjetpt});
	    		}
	    		//std::cout<<" The value of Raw Jet var: "<<vartocorr[i]*(1.0-jetrawf[i])<<endl;
	    		// std::cout<<" The value of Jet var in NanoAOD: "<<vartocorr[i]<<endl;
				// std::cout<<" The value of Jet corr factor "<<corrfactor<<endl;
	    		// std::cout<<" The value of Corrected Jet var: "<<vartocorr[i]*(1.0-jetrawf[i])*corrfactor<<endl;
	    		// std::cout<<" The value of Uncertainty on Jet pt with unc "<< unc_type<<" of JEC Unc "<<jec_unc_idx<<" : "<<corrfactor_unc<<endl;
				// std::cout<<" The value of Corrected Jet pt: "<<corr_jet_pt<<endl;
	    		var_to_corr_withunc = vartocorr[i]*(1.0-jetrawf[i])*corrfactor_unc;
	    		// std::cout<<" The value of Corrected Jet var with unc NEW"<< unc_type<<" of JEC Unc "<<jec_unc_idx<<" : "<<var_to_corr_withunc<<endl;
	  		}
	  		uncertainties.emplace_back(var_to_corr_withunc);
		}

      	return uncertainties;
    };

	auto appresolutioncorrlambdaf = [_jetResCorrector_SF = this->_jetResCorrector_SF, _jetResCorrector = this->_jetResCorrector](const floats& jetpts, const floats& jetetas, const floats& jetphis, const float& rho, const floats& GenJetpts, const floats& GenJetetas, const floats& GenJetphis, string type_name)->floats
    {
    	floats correction_factors;
      	correction_factors.reserve(jetpts.size());

      	float matched_genJet_pt = -99.0;
      	float min_dR = 99.0;
      	for(int i =0; i<int(jetpts.size()); i++)
		{
	  		float corrfactor = 1;
	  		float sf =  _jetResCorrector_SF->evaluate({jetetas[i], type_name});
	  		float ptres =  _jetResCorrector->evaluate({jetetas[i],jetpts[i],rho});
	  		min_dR = 0.2;
	  		for(int j = 0; j < int(GenJetpts.size()); j++)
			{
	    		float dr = ROOT::VecOps::DeltaR<float>(jetetas[i], GenJetetas[j], jetphis[i], GenJetphis[j]);
	    		if(dr < min_dR && abs(jetpts[i]-GenJetpts[j])<3*jetpts[i]*ptres)
				{
					matched_genJet_pt = GenJetpts[j];
	    		}
	    		else
				{
	      			matched_genJet_pt = -1;
	    		}
	  		}
			if(matched_genJet_pt != -1)
			{
	    		corrfactor = 1 + (sf-1)*(jetpts[i]-matched_genJet_pt)/jetpts[i];
	  		}
	  		else
			{
	    		// Double_t randm = gRandom->Gaus(0, ptres);
				UInt_t seed = static_cast<UInt_t>(jetpts[i]*10 + GenJetpts[i]*10 + jetetas[i]*10 + GenJetetas[i]*10 + jetphis[i]*10 + GenJetphis[i]*10);
				TRandom3 randGen(seed);
				Double_t randm = randGen.Gaus(0, ptres);
				// cout<<"randm = "<<randm<<endl;
	    		corrfactor = 1 + randm*TMath::Sqrt(TMath::Max(sf*sf - 1.0, 0.0));
	  		}
	  		if(corrfactor<0) corrfactor=0;
			// cout<<"Final value = "<<jetpts[i]*corrfactor<<endl;
	  		correction_factors.emplace_back(corrfactor);
		}
      	// std::cout<<"Facsss===="<<correction_factors<<std::endl;
      	return correction_factors;
      
    };

	auto metcorrlambdaf = [](const float& met, const float& metphi, const floats& jetptsbefore, const floats& jetptsafter, const floats& jetphis)->float
    {
    	auto metx = met * cos(metphi);
    	auto mety = met * sin(metphi);
      	for(auto i=0; i<int(jetphis.size()); i++)
		{
	  		if(jetptsafter[i]>15.0)
	    	{
	      		metx += (jetptsafter[i] - jetptsbefore[i])*cos(jetphis[i]);
	      		mety += (jetptsafter[i] - jetptsbefore[i])*sin(jetphis[i]);
	    	}
		}
      	float met_corr = float(sqrt(metx*metx + mety*mety));
      	return met_corr;
    };

	auto metphicorrlambdaf = [](const float& met, const float& metphi, const floats& jetptsbefore, const floats& jetptsafter, const floats& jetphis)->float
    {
      	auto metx = met * cos(metphi);
      	auto mety = met * sin(metphi);
      	for(auto i=0; i<int(jetphis.size()); i++)
		{
	  		if(jetptsafter[i]>15.0)
	   		{
	      		metx += (jetptsafter[i] - jetptsbefore[i])*cos(jetphis[i]);
	      		mety += (jetptsafter[i] - jetptsbefore[i])*sin(jetphis[i]);
	    	}
		}
      	return float(atan2(mety, metx));
    };

	if(_jetCorrector != 0)
    {
		

      	//cout<<"jetcorrector==" <<_jetCorrector<<endl;
    	_rlm = _rlm.Define("Jet_pt_corr", appcorrlambdaf,{"Jet_pt", "Jet_eta", "Jet_area", "Jet_rawFactor", "Jet_pt", "fixedGridRhoFastjetAll"});
      	_rlm = _rlm.Define("Jet_mass_corr", appcorrlambdaf,{"Jet_pt", "Jet_eta", "Jet_area", "Jet_rawFactor", "Jet_mass", "fixedGridRhoFastjetAll"});
      	_rlm = _rlm.Define("MET_pt_corr", metcorrlambdaf,{"MET_pt_phimodcorr", "MET_phi_phimodcorr", "Jet_pt", "Jet_pt_corr", "Jet_phi"});
      	_rlm = _rlm.Define("MET_phi_corr", metphicorrlambdaf,{"MET_pt_phimodcorr", "MET_phi_phimodcorr", "Jet_pt", "Jet_pt_corr", "Jet_phi"});
      	if(!_isData)
		{
			_rlm = _rlm.Define("Jet_energy_res_corr_fact", [appresolutioncorrlambdaf](const floats& jetspt, const floats& jetseta, const floats& jetsphi, const float& rho, const floats& genjetspt, const floats& genjetseta, const floats& genjetsphi)
			{
	    		floats corr_fact = appresolutioncorrlambdaf(jetspt, jetseta, jetsphi, rho, genjetspt, genjetseta, genjetsphi, "nom");
	    		return corr_fact;
	  		},{"Jet_pt_corr", "Jet_eta", "Jet_phi", "fixedGridRhoFastjetAll", "GenJet_pt", "GenJet_eta", "GenJet_phi"});
			_rlm = _rlm.Define("Jet_pt_res_corr", "Jet_pt_corr * Jet_energy_res_corr_fact");
			_rlm = _rlm.Define("Jet_mass_res_corr", "Jet_mass_corr * Jet_energy_res_corr_fact");
			_rlm = _rlm.Define("MET_pt_res_corr", metcorrlambdaf,{"MET_pt_corr", "MET_phi_corr", "Jet_pt_corr", "Jet_pt_res_corr", "Jet_phi"});
			_rlm = _rlm.Define("MET_phi_res_corr", metphicorrlambdaf,{"MET_pt_corr", "MET_phi_corr", "Jet_pt_corr", "Jet_pt_res_corr", "Jet_phi"});
			_rlm = _rlm.Redefine("Jet_pt_corr", "Jet_pt_res_corr");
			_rlm = _rlm.Redefine("Jet_mass_corr", "Jet_mass_res_corr");
			_rlm = _rlm.Redefine("MET_pt_corr", "MET_pt_res_corr");
			_rlm = _rlm.Redefine("MET_phi_corr", "MET_phi_res_corr");
      	}

      	int unc_idx = 0;
      	if(_jercunctag.size()>0 && !_isData)
		{
			for(auto _unc_name : _jercunctag)
			{	    
	  			if(_unc_name=="JER") continue;
	  			string jet_pt_name = "Jet_pt_"+_unc_name;
	  			string jet_mass_name = "Jet_mass_"+_unc_name;
	  
	  			_rlm = _rlm.Define(jet_pt_name+"_up", [jecuncertaintylambdaf, unc_idx](const floats& jetspt, const floats& jetseta, const floats& jetsarea, const floats& jetsrawf, const floats& vartocorr, const float& rho)
				{
	      			floats jet_pt_up = jecuncertaintylambdaf(jetspt, jetseta, jetsarea, jetsrawf, vartocorr, rho, unc_idx, "up");
	      			return jet_pt_up;
	    		},{"Jet_pt", "Jet_eta", "Jet_area", "Jet_rawFactor", "Jet_pt", "fixedGridRhoFastjetAll"});
				_rlm = _rlm.Redefine(jet_pt_name+"_up", jet_pt_name+"_up * Jet_energy_res_corr_fact");

	  			_rlm = _rlm.Define(jet_pt_name+"_down", [jecuncertaintylambdaf, unc_idx](const floats& jetspt, const floats& jetseta, const floats& jetsarea, const floats& jetsrawf, const floats& vartocorr, const float& rho)
				{
	      			floats jet_pt_down = jecuncertaintylambdaf(jetspt, jetseta, jetsarea, jetsrawf, vartocorr, rho, unc_idx, "down");
	      			return jet_pt_down;
	    		},{"Jet_pt", "Jet_eta", "Jet_area", "Jet_rawFactor", "Jet_pt", "fixedGridRhoFastjetAll"});
				_rlm = _rlm.Redefine(jet_pt_name+"_down", jet_pt_name+"_down * Jet_energy_res_corr_fact");

	  			_rlm = _rlm.Define(jet_mass_name+"_up", [jecuncertaintylambdaf, unc_idx](const floats& jetspt, const floats& jetseta, const floats& jetsarea, const floats& jetsrawf, const floats& vartocorr, const float& rho)
				{
	      			floats jet_mass_up = jecuncertaintylambdaf(jetspt, jetseta, jetsarea, jetsrawf, vartocorr, rho, unc_idx, "up");
	      			return jet_mass_up;
	    		},{"Jet_pt", "Jet_eta", "Jet_area", "Jet_rawFactor", "Jet_mass", "fixedGridRhoFastjetAll"});
	  			_rlm = _rlm.Redefine(jet_mass_name+"_up", jet_mass_name+"_up * Jet_energy_res_corr_fact");

	  			_rlm = _rlm.Define(jet_mass_name+"_down", [jecuncertaintylambdaf, unc_idx](const floats& jetspt, const floats& jetseta, const floats& jetsarea, const floats& jetsrawf, const floats& vartocorr, const float& rho)
				{
	      			floats jet_mass_down = jecuncertaintylambdaf(jetspt, jetseta, jetsarea, jetsrawf, vartocorr, rho, unc_idx, "down");
	      			return jet_mass_down;
	    		},{"Jet_pt", "Jet_eta", "Jet_area", "Jet_rawFactor", "Jet_mass", "fixedGridRhoFastjetAll"}); 
				_rlm = _rlm.Redefine(jet_mass_name+"_down", jet_mass_name+"_down * Jet_energy_res_corr_fact");

	  			string met_pt_name = "MET_pt_corr_"+_unc_name;
	  			string met_phi_name = "MET_phi_corr_"+_unc_name;
	  			_rlm = _rlm.Define(met_pt_name+"_up", metcorrlambdaf,{"MET_pt_phimodcorr", "MET_phi_phimodcorr", "Jet_pt", jet_pt_name+"_up", "Jet_phi"});
	  			_rlm = _rlm.Define(met_phi_name+"_up", metphicorrlambdaf,{"MET_pt_phimodcorr", "MET_phi_phimodcorr", "Jet_pt", jet_pt_name+"_up", "Jet_phi"});
	  			_rlm = _rlm.Define(met_pt_name+"_down", metcorrlambdaf,{"MET_pt_phimodcorr", "MET_phi_phimodcorr", "Jet_pt", jet_pt_name+"_down", "Jet_phi"});
	  			_rlm = _rlm.Define(met_phi_name+"_down", metphicorrlambdaf,{"MET_pt_phimodcorr", "MET_phi_phimodcorr", "Jet_pt", jet_pt_name+"_down", "Jet_phi"});
	  			unc_idx +=1;
			}

			//For JER Uncertainties	
			_rlm = _rlm.Define("Jet_energy_res_corr_fact_up", [appresolutioncorrlambdaf](const floats& jetspt, const floats& jetseta, const floats& jetsphi, const float& rho, const floats& genjetspt, const floats& genjetseta, const floats& genjetsphi)
			{
	    		floats corr_fact = appresolutioncorrlambdaf(jetspt, jetseta, jetsphi, rho, genjetspt, genjetseta, genjetsphi, "up");
	    		return corr_fact;
	  		},{"Jet_pt_corr", "Jet_eta", "Jet_phi", "fixedGridRhoFastjetAll", "GenJet_pt", "GenJet_eta", "GenJet_phi"});
	
			_rlm = _rlm.Define("Jet_pt_JER_up", "Jet_pt_corr * Jet_energy_res_corr_fact_up");
			_rlm = _rlm.Define("Jet_mass_JER_up", "Jet_mass_corr * Jet_energy_res_corr_fact_up");	
			_rlm = _rlm.Define("MET_pt_corr_JER_up", metcorrlambdaf,{"MET_pt_corr", "MET_phi_corr", "Jet_pt_corr", "Jet_pt_JER_up", "Jet_phi"});
			_rlm = _rlm.Define("MET_phi_corr_JER_up", metphicorrlambdaf,{"MET_pt_corr", "MET_phi_corr", "Jet_pt_corr", "Jet_pt_JER_up", "Jet_phi"});

			_rlm = _rlm.Define("Jet_energy_res_corr_fact_down", [appresolutioncorrlambdaf](const floats& jetspt, const floats& jetseta, const floats& jetsphi, const float& rho, const floats& genjetspt, const floats& genjetseta, const floats& genjetsphi)
			{
	    		floats corr_fact = appresolutioncorrlambdaf(jetspt, jetseta, jetsphi, rho, genjetspt, genjetseta, genjetsphi, "down");
	    		return corr_fact;
	  		},{"Jet_pt_corr", "Jet_eta", "Jet_phi", "fixedGridRhoFastjetAll", "GenJet_pt", "GenJet_eta", "GenJet_phi"});
	
			_rlm = _rlm.Define("Jet_pt_JER_down", "Jet_pt_corr * Jet_energy_res_corr_fact_down");
			_rlm = _rlm.Define("Jet_mass_JER_down", "Jet_mass_corr * Jet_energy_res_corr_fact_down");
			_rlm = _rlm.Define("MET_pt_corr_JER_down", metcorrlambdaf,{"MET_pt_corr", "MET_phi_corr", "Jet_pt_corr", "Jet_pt_JER_down", "Jet_phi"});
			_rlm = _rlm.Define("MET_phi_corr_JER_down", metphicorrlambdaf,{"MET_pt_corr", "MET_phi_corr", "Jet_pt_corr", "Jet_pt_JER_down", "Jet_phi"});
      	}
    }
}

//=================================Define regions=================================================//
void NanoAODAnalyzerrdframe::defineJESUncRegion(const string&  pt_cut)
{

    if(debug)
	{
    	std::cout<<std::endl;
    	std::cout<<"================================//================================="<<std::endl;
    	std::cout<<"Line : "<<__LINE__<<" Function : "<<__FUNCTION__<<std::endl;
    	std::cout<<"================================//================================="<<std::endl;
    	std::cout<<"Define Region Based on Jets pt cut for all JES Unc variations"<<std::endl;
    }

	const float ptCut = std::stof(pt_cut);
    auto checkJetsAbovePtCut = [ptCut](const ROOT::RVec<float> &jetPts) -> int
	{
		int n_jets = 0;
		bool out = 0;
		for(int i = 0; i < jetPts.size(); i++)
		{
			if(jetPts[i] > ptCut) n_jets += 1;
		}
		if(n_jets >= 3)
		{
			out = 1;
		}
		return out;
    	// return std::any_of(jetPts.begin(), jetPts.end(), [ptCut](float pt){ return pt > ptCut; }) ? 1 : 0;
    };
    //_rlm.Range(10).Snapshot("debugTree", "debug.root",{"Jet_pt_corr"});

    //_rlm = _rlm.Define("region_Jet_pt_nom", "Jet_pt_corr>"+pt_cut);
    _rlm = _rlm.Define("region_Jet_pt_nom", checkJetsAbovePtCut, {"Jet_pt_corr"});
    
    //string reg_def = "region_Jet_pt_nom == 1 ? 0.0 : ";
    //string new_jet_pt_corr_def = "region_jes_unc == 0.0 ? Jet_pt_corr : ";
    
    if(!_jercunctag.empty() && !_isData)
	{
    	int jes_unc_idx = 1;
      	for(const auto& _unc_name : _jercunctag)
		{	    
			const string jet_pt_name_up = "Jet_pt_" + _unc_name + "_up";
			const string jet_pt_name_down = "Jet_pt_" + _unc_name + "_down";
			const string region_name_up = "region_jes_" + _unc_name + "_up";
			const string region_name_down = "region_jes_" + _unc_name + "_down";
			// _rlm = _rlm.Define(region_name_up, [checkJetsAbovePtCut, ptCut](const ROOT::RVec<float> &jetPts) { return checkJetsAbovePtCut(jetPts, ptCut); }, {jet_pt_name_up});
			// _rlm = _rlm.Define(region_name_down, [checkJetsAbovePtCut, ptCut](const ROOT::RVec<float> &jetPts) { return checkJetsAbovePtCut(jetPts, ptCut); }, {jet_pt_name_down});
			_rlm = _rlm.Define(region_name_up, checkJetsAbovePtCut, {jet_pt_name_up});
			_rlm = _rlm.Define(region_name_down, checkJetsAbovePtCut, {jet_pt_name_down});
	
			//reg_def = reg_def+ region_name_up + "==1 ? "+std::to_string(jes_unc_idx)+" : ";
			//reg_def = reg_def+ region_name_down + "==1 ? "+std::to_string(jes_unc_idx+1)+" : ";
			jes_unc_idx +=2;
      	}
    }
    //reg_def += "-1";
    //_rlm = _rlm.Define("region_jes_unc", reg_def);
    // _rlm = _rlm.Redefine("Jet_pt_corr", "region_jes_unc == 0.0 ? Jet_pt_corr : ");
}

void NanoAODAnalyzerrdframe::applyMETPhiCorrections() //dataAndMC
{

  	cout<<"apply MET phi correction"<<endl;
  
  	auto appmetcorrlambdaf = [this](const float& met, const float& metphi, const int&npvs, unsigned const int&run, string corr_name)->float
    {
    	float corrfactor = 1;
      	if(std::abs(metphi)>=3.15)
		{
			std::cout<<"MET :"<<met<<" and MET_phi :"<<metphi<<" and NPV :"<<float(npvs)<<" and run :"<<float(run)<<std::endl;
      	}
      	if(met<2000)
		{
			corrfactor = _metphiCorrector->at(corr_name)->evaluate({met, metphi, float(npvs), float(run)});
      	}
      	return corrfactor;
    };
  	std::vector<std::string> var = {"pt", "phi"};
  	if(_isData)
	{
    	for(const std::string& v : var)
		{
      		std::string var_name = "MET_" + v + "_phimodcorr";
      		std::string tag_name = v + "_metphicorr_pfmet_data";
      		_rlm = _rlm.Define(var_name, [appmetcorrlambdaf, tag_name](const float& met, const float& metphi, const int&npvs, unsigned const int&run){return appmetcorrlambdaf(met, metphi, npvs, run, tag_name);}, {"MET_pt", "MET_phi", "PV_npvs", "run"});
      		//_rlm = _rlm.Define("MET_phi_phimodcorr", appmetcorrlambdaf,{"MET_pt", "MET_phi", "PV_npvs", "run", "phi_metphicorr_pfmet_data"});
    	}
  	}
  	else
	{
    	for(const std::string& v : var)
		{
      		std::string var_name = "MET_" + v + "_phimodcorr";
      		std::string tag_name = v + "_metphicorr_pfmet_mc";
      		int dummy_run = 1;
      		_rlm = _rlm.Define(var_name, [appmetcorrlambdaf, tag_name, dummy_run](const float& met, const float& metphi, const int&npvs){return appmetcorrlambdaf(met, metphi, npvs, dummy_run, tag_name);}, {"MET_pt", "MET_phi", "PV_npvs"});
      		//_rlm = _rlm.Define("MET_phi_phimodcorr", appmetcorrlambdaf,{"MET_pt", "MET_phi", "PV_npvs", "run", "phi_metphicorr_pfmet_data"});
    	}
    	//_rlm = _rlm.Define("MET_pt_phimodcorr", appmetcorrlambdaf,{"MET_pt", "MET_phi", "PV_npvs", "one", "pt_metphicorr_pfmet_mc"});
    	//_rlm = _rlm.Define("MET_phi_phimodcorr", appmetcorrlambdaf,{"MET_pt", "MET_phi", "PV_npvs", "one", "phi_metphicorr_pfmet_mc"});
  	}
}

void NanoAODAnalyzerrdframe::applyMuPtCorrection() //data and MC
{
  cout<<"apply Muon Pt correction"<<endl;
  
	if(_isData)
  	{
    	auto lambdaf_data = [this](const ints mu_charges, const floats& mu_pts, const floats& mu_etas, const floats& mu_phis)->floats
    	{
			floats corrMuPts;
			corrMuPts.reserve(mu_pts.size());
			//std::cout<<"Number of muons: "<<mu_pts.size()<<std::endl;
			for(auto i =0; i<int(mu_pts.size()); i++)
	  		{
	    		float mu_pt_uncorr = mu_pts[i];
	    		//std::cout<<"The Muon Uncorrected pt"<<mu_pt_uncorr<<std::endl;	    
	    		float corrfactor = 1;
	    		if(mu_pt_uncorr<2000)
				{
	      			corrfactor = _Roch_corr.kScaleDT(mu_charges[i], mu_pt_uncorr, mu_etas[i], mu_phis[i], 0, 0);
	    		}
	    		//std::cout<<"Muon pt correction factor===="<<corrfactor <<std::endl;
	    		corrMuPts.emplace_back(mu_pt_uncorr * corrfactor);
	  		}    
			return corrMuPts;
      	};
    
    	_rlm = _rlm.Define("Muon_pt_corr", lambdaf_data,{"Muon_charge", "Muon_pt", "Muon_eta", "Muon_phi"});
	}
  	else
	{
  		auto lambdaf_mc = [this](const ints mu_charges, const floats& mu_pts, const floats& mu_etas, const floats& mu_phis, const ints muon_genIdx, const floats& gen_pts,  const ints nls)->floats
      	{
			floats corrMuPts;
			corrMuPts.reserve(mu_pts.size());
			//std::cout <<"Number of muons: "<<mu_pts.size()<<std::endl;
			for(int i=0; i<int(mu_pts.size()); i++)
	  		{
	    		float corrfactor = 1;
	    		float mu_pt_uncorr = mu_pts[i];
	    		//std::cout<<"The Muon Uncorrected pt"<<mu_pt_uncorr<<std::endl;
	    		if(muon_genIdx[i] != -1 && mu_pt_uncorr<2000)
				{
	      			corrfactor = _Roch_corr.kSpreadMC(mu_charges[i], mu_pt_uncorr, mu_etas[i], mu_phis[i], gen_pts[muon_genIdx[i]], 0, 0);
	    		}
	    		else if(muon_genIdx[i] == -1 && mu_pt_uncorr<2000)
				{
	      			float rand = gRandom->Rndm();
	      			corrfactor = _Roch_corr.kSmearMC(mu_charges[i], mu_pt_uncorr, mu_etas[i], mu_phis[i], nls[i], rand, 0, 0);
	    		}
	    		//std::cout<<"Muon corrected Pt ===="<<corrfactor * mu_pt_uncorr <<std::endl;
	    		corrMuPts.emplace_back(corrfactor * mu_pt_uncorr);
	  		}
			return corrMuPts;
      	};
    	_rlm = _rlm.Define("Muon_gen_pt", "GenPart_pt[Muon_genPartIdx]");
    	_rlm = _rlm.Define("Muon_pt_corr", lambdaf_mc,{"Muon_charge", "Muon_pt", "Muon_eta", "Muon_phi", "Muon_genPartIdx", "GenPart_pt", "Muon_nTrackerLayers"});
	}
}

void NanoAODAnalyzerrdframe::setupCorrections(string goodjsonfname, string pufname, string putag, string btvfname, string btvtype, string fname_btagEff, string hname_Loose_btagEff_bcflav, string hname_Loose_btagEff_lflav, string hname_Medium_btagEff_bcflav, string hname_Medium_btagEff_lflav, string hname_Tight_btagEff_bcflav, string hname_Tight_btagEff_lflav, string pileupjetidfname, string fname_pileupjetidEff, string hname_Loose_pileupjetidEff, string hname_Medium_pileupjetidEff, string hname_Tight_pileupjetidEff, string muon_roch_fname, string muon_fname, string muonisotype_loosett1l, string muonhlttype, string muonrecotype, string muonidtype, string muonisotype, string electron_fname, string electron_id_type_loosett1l, string electron_reco_type, string electron_id_type, string fname_electriggerEff, string hname_electriggerEff_eff, string hname_electriggerEff_statdata, string hname_electriggerEff_statmc, string hname_electriggerEff_systmc, string jercfname, string jerctag, std::vector<std::string> jercunctag, string jertag, string fname_metphimod)
//In this function the correction is evaluated for each jet, Muon, Electron and MET. The correction depends on the momentum, pseudorapidity, energy, and cone area of the jet, as well as the value of “rho” (the average momentum per area) and number of interactions in the event. The correction is used to scale the momentum of the jet.
{
	if(debug)
	{
	    std::cout<<std::endl;
    	std::cout<< "================================//=================================" << std::endl;
	    std::cout<< "Line : "<< __LINE__ << " Function : " << __FUNCTION__ << std::endl;
    	std::cout<< "================================//=================================" << std::endl;
	    cout << "set up Corrections!" << endl;
    	auto Nentry = _rlm.Count();
	    // This is how you can express a range of the first 100 entries
    	// _rlm = _rlm.Range(0, 1000000);
	    // auto Nentry_100 = _rlm.Count();
    	std::cout<< "-------------------------------------------------------------------" << std::endl;
	    cout << "In Set up Corrections!:\n"
		 << " - All entries: " << *Nentry << endl;
	    //<< " - Entries from 0 to 100: " << *Nentry_100 << endl;
    	std::cout<< "-------------------------------------------------------------------" << std::endl;
	}
	
	if(_isData) _jsonOK = readgoodjson(goodjsonfname); // read golden json file
	std::cout<<"Rochester correction files: "<<muon_roch_fname<<std::endl;
	_Roch_corr.init(muon_roch_fname);
	if(!_isData)
	{
	  	// using correctionlib
	  	// Muon corrections
	  	_correction_muon = correction::CorrectionSet::from_file(muon_fname);
	  	_muon_loosett1l_iso_type = muonisotype_loosett1l;
	  	_muon_hlt_type = muonhlttype;
	  	_muon_reco_type = muonrecotype;
	  	_muon_id_type = muonidtype;
	  	_muon_iso_type = muonisotype;
	  	std::cout<<"================================//================================="<<std::endl;
	  	cout<<"MUON JSON FILE : "<<muon_fname<<endl;
	  	cout<<"MUON loose tt1l ISO type in JSON : "<<_muon_loosett1l_iso_type<<endl;
	  	cout<<"MUON HLT type in JSON : "<<_muon_hlt_type<<endl;
	  	cout<<"MUON RECO type in JSON : "<<_muon_reco_type<<endl;
	  	cout<<"MUON ID type in JSON : "<<_muon_id_type<<endl;
	  	cout<<"MUON ISO type in JSON : "<<_muon_iso_type<<endl;
	  	assert(_correction_muon->validate());
	  
	  	// Electron corrections
	  	_correction_electron = correction::CorrectionSet::from_file(electron_fname);
	  	_electron_loosett1l_id_type = electron_id_type_loosett1l;
	 	_electron_reco_type = electron_reco_type;
	  	_electron_id_type = electron_id_type;
	  	std::cout<<"================================//================================="<<std::endl;
	  	cout<<"ELECTRON JSON FILE : "<<electron_fname<<endl;
	  	cout<<"ELECTRON loose tt1l ID type in JSON : "<<_electron_loosett1l_id_type<<endl;
	  	cout<<"ELECTRON RECO type in JSON : "<<_electron_reco_type<<endl;
	  	cout<<"ELECTRON ID type in JSON : "<<_electron_id_type<<endl;
	  	assert(_correction_electron->validate());

		// Electron trigger corrections
		// _correction_electron_trigger = correction::CorrectionSet::from_file(electron_hlt_fname);
		// _electron_hlt_type = electron_hlt_type;
		// assert(_correction_electron_trigger->validate());
		f_electriggerEff = new TFile(fname_electriggerEff.c_str(), "READ");
		hist_electriggerEff_eff = dynamic_cast<TH2F*>(f_electriggerEff->Get(hname_electriggerEff_eff.c_str()));
		hist_electriggerEff_statdata = dynamic_cast<TH2F*>(f_electriggerEff->Get(hname_electriggerEff_statdata.c_str()));
	  	hist_electriggerEff_statmc = dynamic_cast<TH2F*>(f_electriggerEff->Get(hname_electriggerEff_statmc.c_str()));
	  	hist_electriggerEff_systmc = dynamic_cast<TH2F*>(f_electriggerEff->Get(hname_electriggerEff_systmc.c_str()));
	  
	  	// btag corrections
	  	_correction_btag1 = correction::CorrectionSet::from_file(btvfname);
	  	_btvtype = btvtype;
	  	assert(_correction_btag1->validate());
	  	f_btagEff = new TFile(fname_btagEff.c_str(), "READ");
	  	hist_Loose_btagEff_bcflav = dynamic_cast<TH2D*>(f_btagEff->Get(hname_Loose_btagEff_bcflav.c_str()));
	  	hist_Loose_btagEff_lflav = dynamic_cast<TH2D*>(f_btagEff->Get(hname_Loose_btagEff_lflav.c_str()));
	  	hist_Medium_btagEff_bcflav = dynamic_cast<TH2D*>(f_btagEff->Get(hname_Medium_btagEff_bcflav.c_str()));
	  	hist_Medium_btagEff_lflav = dynamic_cast<TH2D*>(f_btagEff->Get(hname_Medium_btagEff_lflav.c_str()));
	  	hist_Tight_btagEff_bcflav = dynamic_cast<TH2D*>(f_btagEff->Get(hname_Tight_btagEff_bcflav.c_str()));
	  	hist_Tight_btagEff_lflav = dynamic_cast<TH2D*>(f_btagEff->Get(hname_Tight_btagEff_lflav.c_str()));

		// pileupjetID corrections
		_correction_pileupjetid = correction::CorrectionSet::from_file(pileupjetidfname);
	  	assert(_correction_pileupjetid->validate());
	  	f_pileupjetidEff = new TFile(fname_pileupjetidEff.c_str(), "READ");
	  	hist_Loose_pileupjetidEff = dynamic_cast<TH2D*>(f_pileupjetidEff->Get(hname_Loose_pileupjetidEff.c_str()));
	  	hist_Medium_pileupjetidEff = dynamic_cast<TH2D*>(f_pileupjetidEff->Get(hname_Medium_pileupjetidEff.c_str()));
	  	hist_Tight_pileupjetidEff = dynamic_cast<TH2D*>(f_pileupjetidEff->Get(hname_Tight_pileupjetidEff.c_str()));

	  	// pile up weights
	  	_correction_pu = correction::CorrectionSet::from_file(pufname);
	  	assert(_correction_pu->validate());
	  	_putag = putag;
	  	auto punominal = [this](const float& x) { return pucorrection(_correction_pu, _putag, "nominal", x); };
		auto puplus = [this](const float& x) { return pucorrection(_correction_pu, _putag, "up", x); };
		auto puminus = [this](const float& x) { return pucorrection(_correction_pu, _putag, "down", x); };

	  	if(!isDefined("puWeight")) _rlm = _rlm.Define("puWeight", punominal,{"Pileup_nTrueInt"});
	  	if(!isDefined("puWeight_plus")) _rlm = _rlm.Define("puWeight_plus", puplus,{"Pileup_nTrueInt"});
	  	if(!isDefined("puWeight_minus")) _rlm = _rlm.Define("puWeight_minus", puminus,{"Pileup_nTrueInt"});
	  	if(!isDefined("pugenWeight"))
	    {
	    	_rlm = _rlm.Define("pugenWeight", [this](float x, float y)
			{
				return x*y;
			},{"genWeight", "puWeight"});
	    }
	}
	_jerctag = jerctag;
	_jercunctag = jercunctag;
	
	setupJetMETCorrection(jercfname, _jerctag, _jercunctag, jertag, fname_metphimod);
	applyMETPhiCorrections();
	applyJetMETCorrections();
	applyMuPtCorrection();
}

double NanoAODAnalyzerrdframe::getBTaggingEff(double hadflav, double eta, double pt, std::string _BTaggingWP)
{
  	double efficiency = 1.0;
  	//int maxXBin = -1;
  	//int maxYBin = -1;
  	int binX = -1;
  	int binY = -1;
  	if(hadflav!=0)
	{
    	// Get the maximum bin number for x and y axes
    	//maxXBin = hist_btagEff_bcflav->GetXaxis()->GetNbins();
    	//maxYBin = hist_btagEff_bcflav->GetYaxis()->GetNbins();
    	if(_BTaggingWP=="L")
		{
      		// Get the bin number corresponding to the provided x and y values
      		binX = hist_Loose_btagEff_bcflav->GetXaxis()->FindBin(pt);
      		binY = hist_Loose_btagEff_bcflav->GetYaxis()->FindBin(std::abs(eta));
      		efficiency = hist_Loose_btagEff_bcflav->GetBinContent(binX, binY);
    	}
    	else if(_BTaggingWP=="M")
		{
      		// Get the bin number corresponding to the provided x and y values
      		binX = hist_Medium_btagEff_bcflav->GetXaxis()->FindBin(pt);
      		binY = hist_Medium_btagEff_bcflav->GetYaxis()->FindBin(std::abs(eta));
      		efficiency = hist_Medium_btagEff_bcflav->GetBinContent(binX, binY);
    	}
    	else if(_BTaggingWP=="T")
		{
      		// Get the bin number corresponding to the provided x and y values
      		binX = hist_Tight_btagEff_bcflav->GetXaxis()->FindBin(pt);
      		binY = hist_Tight_btagEff_bcflav->GetYaxis()->FindBin(std::abs(eta));
      		efficiency = hist_Tight_btagEff_bcflav->GetBinContent(binX, binY);
    	}
  	}
  	else
	{
    	// Get the maximum bin number for x and y axes
    	//maxXBin = hist_btagEff_lflav->GetXaxis()->GetNbins();
    	//maxYBin = hist_btagEff_lflav->GetYaxis()->GetNbins();
    	if(_BTaggingWP=="L")
		{
      		// Get the bin number corresponding to the provided x and y values
      		binX = hist_Loose_btagEff_lflav->GetXaxis()->FindBin(pt);
      		binY = hist_Loose_btagEff_lflav->GetYaxis()->FindBin(std::abs(eta));
      		efficiency = hist_Loose_btagEff_lflav->GetBinContent(binX, binY);
    	}
    	else if(_BTaggingWP=="M")
		{
      		// Get the bin number corresponding to the provided x and y values
      		binX = hist_Medium_btagEff_lflav->GetXaxis()->FindBin(pt);
      		binY = hist_Medium_btagEff_lflav->GetYaxis()->FindBin(std::abs(eta));
      		efficiency = hist_Medium_btagEff_lflav->GetBinContent(binX, binY);
    	}
    	else if(_BTaggingWP=="T")
		{
      		// Get the bin number corresponding to the provided x and y values
      		binX = hist_Tight_btagEff_lflav->GetXaxis()->FindBin(pt);
      		binY = hist_Tight_btagEff_lflav->GetYaxis()->FindBin(std::abs(eta));
      		efficiency = hist_Tight_btagEff_lflav->GetBinContent(binX, binY);
    	}
  	}
  	//std::cout<<"The BTagging efficiency for hadron Flavor ="<<hadflav<<" with eta = "<<eta<<" and pt = "<<pt<<" is "<<efficiency<<std::endl;
  	return efficiency;
}

ROOT::RDF::RNode NanoAODAnalyzerrdframe::calculateBTagSF(RNode _rlm, std::vector<std::string> Jets_vars_names, const int&_case, const double btag_cut, std::string _BTaggingWP, std::string output_var, const int&_redefine)
{

  	//case1 : fixedWP correction with mujets (here medium WP) # evaluate('systematic', 'working_point', 'flavor', 'abseta', 'pt')
  	//for case 1  use one of the btvtype = "deepJet_mujets " , deepJet_comb" for b/c , deepJet_incl" for lightjets 
  	if(_case==1)
	{
		//======================================================================================================================================
      	//>>>> function to calculate event weights for MC events, incorporating fixedWP correction with mujets (here medium WP)and systematics with
      	//all variations seperately (up/down/correlated/uncorrelated/)
      	//The weight for each variation is stored in separate columns (btag_SF_central,btag_SF_up, btag_SF_down, etc.). 
      	// btagWeight_case1_central  is used to recalculate the eventweight. Other variations are intended for systematics calculations.
      	//======================================================================================================================================
    	auto btagweightgenerator_bcflav_case1 = [this](const ROOT::VecOps::RVec<int>& hadflav, const ROOT::VecOps::RVec<float>& etas, const ROOT::VecOps::RVec<float>& pts, const ROOT::VecOps::RVec<float>& btag_scores, const double btag_cut, std::string _BTagWP, const std::string& variation) -> float
		{
      		double btagWeight_bcflav = 1.0;
      		for(std::size_t i = 0; i < pts.size(); i++)
			{
				// std::cout<<"The BTag flavor"<<hadflav[i]<<" BTagJet eta:"<<etas[i]<<" BTagJet pt"<<pts[i]<<std::endl;
				if(std::abs(etas[i])>2.4999 || pts[i]<30.000001 || hadflav[i]==0) continue;
				//double selection_cut = _correction_btag1->("deepJet_wp_values")->evaluate(_BTagWP);
				if(btag_scores[i]>=btag_cut)
				{
	    			double bcjets_weights = _correction_btag1->at("deepJet_mujets")->evaluate({variation, _BTagWP, hadflav[i], std::fabs(etas[i]), pts[i]});
	    			btagWeight_bcflav *= bcjets_weights;
				}
				else
				{
	    			double bcjets_weights = _correction_btag1->at("deepJet_mujets")->evaluate({variation, _BTagWP, hadflav[i], std::fabs(etas[i]), pts[i]});
					double eff = getBTaggingEff(hadflav[i], etas[i], pts[i], _BTagWP);
					btagWeight_bcflav *= (1 - bcjets_weights*eff)/(1-eff);
				}
      		}
      		return btagWeight_bcflav;
    	};

    	auto btagweightgenerator_lflav_case1 = [this](const ROOT::VecOps::RVec<int>& hadflav, const ROOT::VecOps::RVec<float>& etas, const ROOT::VecOps::RVec<float>& pts, const ROOT::VecOps::RVec<float>& btag_scores, const double btag_cut, std::string _BTagWP, const std::string& variation) -> float
		{
      		double btagWeight_lflav = 1.0;
      		for(std::size_t i = 0; i < pts.size(); i++)
			{
				//std::cout<<"The BTag flavor"<<hadflav[i]<<" BTagJet eta:"<<etas[i]<<" BTagJet pt"<<pts[i]<<std::endl;
				if(std::abs(etas[i])>2.4999 || pts[i]<30.000001 || hadflav[i]!=0) continue;
				//double selection_cut = _correction_btag1->("deepJet_wp_values")->evaluate(_BTagWP);
				if(btag_scores[i]>=btag_cut)
				{
	  				double lightjets_weights = _correction_btag1->at("deepJet_incl")->evaluate({variation, _BTagWP, hadflav[i], std::fabs(etas[i]), pts[i]});
	  				btagWeight_lflav *= lightjets_weights;
				}
				else
				{
	  				double lightjets_weights = _correction_btag1->at("deepJet_incl")->evaluate({variation, _BTagWP, hadflav[i], std::fabs(etas[i]), pts[i]});
	  				double eff = getBTaggingEff(hadflav[i], etas[i], pts[i], _BTagWP);
	  				btagWeight_lflav *= (1 - lightjets_weights*eff)/(1-eff);
				}
      		}
      		return btagWeight_lflav;
    	};
    	std::vector<std::string> variations = {};
    	std::string column_name_bcflav = "";
    	std::string column_name_lflav = "";
    	if(_redefine!=1)
		{
      		// btag weight for each variation individually
      		variations = {"central", "up", "down", "up_correlated", "down_correlated", "up_uncorrelated", "down_uncorrelated", "up_statistic", "down_statistic"}; 
      		for(const std::string& variation : variations)
			{
				column_name_bcflav = output_var + "bcflav_" +variation;
				_rlm = _rlm.Define(column_name_bcflav, [btagweightgenerator_bcflav_case1, variation, btag_cut, _BTaggingWP](const ROOT::VecOps::RVec<int>& hadflav, const ROOT::VecOps::RVec<float>& etas, const ROOT::VecOps::RVec<float>& pts, const ROOT::VecOps::RVec<float>& btag_scores)
				{
	    			float weight = btagweightgenerator_bcflav_case1(hadflav, etas, pts, btag_scores, btag_cut, _BTaggingWP, variation);// Get the weight for the corresponding variation
	    			return weight;
	  			}, Jets_vars_names); //after all cuts, remove overlapped
	
				column_name_lflav = output_var + "lflav_" +variation;
				_rlm = _rlm.Define(column_name_lflav, [btagweightgenerator_lflav_case1, variation, btag_cut, _BTaggingWP](const ROOT::VecOps::RVec<int>& hadflav, const ROOT::VecOps::RVec<float>& etas, const ROOT::VecOps::RVec<float>& pts, const ROOT::VecOps::RVec<float>& btag_scores)
				{
	    			float weight = btagweightgenerator_lflav_case1(hadflav, etas, pts, btag_scores, btag_cut, _BTaggingWP, variation);// Get the weight for the corresponding variation
	    			return weight;
	  			}, Jets_vars_names); //after all cuts, remove overlapped
				//std::cout<<"BJet SF column name: "<<column_name_bcflav<<" and "<<column_name_lflav <<std::endl;
				if(isDefined(column_name_bcflav))
				{
	  				std::cout<<"BJet SF column: "<<column_name_bcflav<<" is saved in the Node."<<std::endl;
				}
				if(isDefined(column_name_lflav))
				{
	  				std::cout<<"BJet SF column: "<<column_name_lflav<<" is saved in the Node."<<std::endl;
				}
      		}
    	}
    	else
		{
      		variations = {"central"}; 
      		for(const std::string& variation : variations)
			{
				column_name_bcflav = output_var + "bcflav_" +variation;
				_rlm = _rlm.Redefine(column_name_bcflav, [btagweightgenerator_bcflav_case1, variation, btag_cut, _BTaggingWP](const ROOT::VecOps::RVec<int>& hadflav, const ROOT::VecOps::RVec<float>& etas, const ROOT::VecOps::RVec<float>& pts, const ROOT::VecOps::RVec<float>& btag_scores)
				{
	    			float weight = btagweightgenerator_bcflav_case1(hadflav, etas, pts, btag_scores, btag_cut, _BTaggingWP, variation);// Get the weight for the corresponding variation
	    			return weight;
	  			}, Jets_vars_names); //after all cuts, remove overlapped
	
				column_name_lflav = output_var + "lflav_" +variation;
				_rlm = _rlm.Redefine(column_name_lflav, [btagweightgenerator_lflav_case1, variation, btag_cut, _BTaggingWP](const ROOT::VecOps::RVec<int>& hadflav, const ROOT::VecOps::RVec<float>& etas, const ROOT::VecOps::RVec<float>& pts, const ROOT::VecOps::RVec<float>& btag_scores)
				{
	    			float weight = btagweightgenerator_lflav_case1(hadflav, etas, pts, btag_scores, btag_cut, _BTaggingWP, variation);// Get the weight for the corresponding variation
	    			return weight;
	  			}, Jets_vars_names); //after all cuts, remove overlapped
				//std::cout<<"BJet SF column name: "<<column_name_bcflav<<" and "<<column_name_lflav <<std::endl;
				if(isDefined(column_name_bcflav))
				{
	  				std::cout<<"BJet SF column: "<<column_name_bcflav<<" is Changed in the Node."<<std::endl;
				}
				if(isDefined(column_name_lflav))
				{
	  				std::cout<<"BJet SF column: "<<column_name_lflav<<" is Changed in the Node."<<std::endl;
				}
      		}
    	}
  	}
  	else if(_case==3)
	{
    	//======================================================================================================================================
    	//case3 - Shape correction
    	//for case 3 : use btvtype': 'deepJet_shape' in jobconfiganalysis.py
    	cout<<"case 3 Shape correction B tagging SF for MC "<<endl;
    	//======================================================================================================================================
    	//>>>> function to calculate event weights for MC events,based on DeepJet algorithm, incorporating shape correction with central variation
    	//======================================================================================================================================
    	auto btagweightgenerator3= [this](ints &hadflav, floats &etas, floats &pts, floats &btags)->float
      	{
			double bweight=1.0;
			for(auto i=0; i<int(pts.size()); i++)
	  		{
	    		if(std::abs(etas[i])>2.5 || pts[i]<30.000001) continue;
	    		double w = _correction_btag1->at(_btvtype)->evaluate({"central", int(hadflav[i]), fabs(float(etas[i])), float(pts[i]), float(btags[i])});
	    		bweight *= w;
	  		}
			return bweight;
      	};
    
    	cout<<"Generate case3 b-tagging weight"<<endl;
    	std::string column_name = output_var + "case3";
    	_rlm = _rlm.Define(column_name, btagweightgenerator3, Jets_vars_names);
    	//Total event weight after shape correction
    	//_rlm = _rlm.Define("evWeight", "pugenWeight*btagWeight_case3");
    	std::cout<<"BJet SF column name: "<<column_name<<std::endl;
  	}
  	return _rlm;
}

double NanoAODAnalyzerrdframe::getPileupJetIDEff(double eta, double pt, std::string _PileupJetIDWP)
{
  	double efficiency = 1.0;
  	//int maxXBin = -1;
  	//int maxYBin = -1;
  	int binX = -1;
  	int binY = -1;
  	// Get the maximum bin number for x and y axes
    //maxXBin = hist_PileupJetID_bcflav->GetXaxis()->GetNbins();
	//maxYBin = hist_PileupJetID_bcflav->GetYaxis()->GetNbins();
    if(_PileupJetIDWP=="L")
	{
      	// Get the bin number corresponding to the provided x and y values
    	binX = hist_Loose_pileupjetidEff->GetXaxis()->FindBin(pt);
  		binY = hist_Loose_pileupjetidEff->GetYaxis()->FindBin(eta);
      	efficiency = hist_Loose_pileupjetidEff->GetBinContent(binX, binY);
    }
	else if(_PileupJetIDWP=="M")
	{
  		// Get the bin number corresponding to the provided x and y values
      	binX = hist_Medium_pileupjetidEff->GetXaxis()->FindBin(pt);
    	binY = hist_Medium_pileupjetidEff->GetYaxis()->FindBin(eta);
  		efficiency = hist_Medium_pileupjetidEff->GetBinContent(binX, binY);
    }
	else if(_PileupJetIDWP=="T")
	{
  		// Get the bin number corresponding to the provided x and y values
      	binX = hist_Tight_pileupjetidEff->GetXaxis()->FindBin(pt);
    	binY = hist_Tight_pileupjetidEff->GetYaxis()->FindBin(eta);
  		efficiency = hist_Tight_pileupjetidEff->GetBinContent(binX, binY);
  	}
  	//std::cout<<"The PileupJetID efficiency with eta = "<<eta<<" and pt = "<<pt<<" is "<<efficiency<<std::endl;
  	return efficiency;
}

ROOT::RDF::RNode NanoAODAnalyzerrdframe::calculatePileupJetIDSF(RNode _rlm, std::vector<std::string> Jets_vars_names, std::string year, std::string _PileupJetIDWP, std::string output_var, const int&_redefine)
{

  	auto pileupjetidweightgenerator = [this](const ROOT::VecOps::RVec<float>& etas, const ROOT::VecOps::RVec<float>& pts, const ROOT::VecOps::RVec<int>& pileupjetid_scores, const int pileupjetid_cut, std::string _PileupJetIDWP, const std::string& variation) -> float
	{
  		double pileupjetidWeight = 1.0;
      	for(std::size_t i = 0; i < pts.size(); i++)
		{
			// std::cout<<"The Jet eta:"<<etas[i]<<" Jet pt"<<pts[i]<<std::endl;
			if(std::abs(etas[i])>5.000001 || pts[i]>50.000001) continue;
			if(pileupjetid_scores[i]==pileupjetid_cut)
			{
	    		double pileupjetidjets_weights = _correction_pileupjetid->at("PUJetID_eff")->evaluate({etas[i], pts[i], variation, _PileupJetIDWP});
	    		pileupjetidWeight *= pileupjetidjets_weights;
			}
			else
			{
	    		double pileupjetidjets_weights = _correction_pileupjetid->at("PUJetID_eff")->evaluate({etas[i], pts[i], variation, _PileupJetIDWP});
				double eff = getPileupJetIDEff(etas[i], pts[i], _PileupJetIDWP);
				pileupjetidWeight *= (1 - pileupjetidjets_weights*eff)/(1-eff);
			}
  		}
      	return pileupjetidWeight;
    };

	int pileupjetid_cut = 0;
	if(_PileupJetIDWP == "L")
	{
		if(year == "2016")
		{
			pileupjetid_cut = 1;
		}
		else if(year == "2017" || year == "2018")
		{
			pileupjetid_cut = 4;
		}
	}
	else if(_PileupJetIDWP == "M")
	{
		if(year == "2016")
		{
			pileupjetid_cut = 3;
		}
		else if(year == "2017" || year == "2018")
		{
			pileupjetid_cut = 6;
		}
	}
	else if(_PileupJetIDWP == "T")
	{
		pileupjetid_cut = 7;
	}
    std::vector<std::string> variations = {};
	std::string column_name_pileupjetid = "";
	if(_redefine!=1)
	{
  		// pileupJetid weight for each variation individually
      	variations = {"nom", "up", "down"}; 
    	for(const std::string& variation : variations)
		{
			column_name_pileupjetid = output_var + variation;
			_rlm = _rlm.Define(column_name_pileupjetid, [pileupjetidweightgenerator, variation, pileupjetid_cut, _PileupJetIDWP](const ROOT::VecOps::RVec<float>& etas, const ROOT::VecOps::RVec<float>& pts, const ROOT::VecOps::RVec<int>& pileupjetid_scores)
			{
				float weight = pileupjetidweightgenerator(etas, pts, pileupjetid_scores, pileupjetid_cut, _PileupJetIDWP, variation);// Get the weight for the corresponding variation
				return weight;
  			}, Jets_vars_names); //after all cuts, remove overlapped
			//std::cout<<"PileupJetID SF column name: "<<column_name_pileupjetid<<std::endl;
			if(isDefined(column_name_pileupjetid))
			{
	  			std::cout<<"PileupJetID SF column: "<<column_name_pileupjetid<<" is saved in the Node."<<std::endl;
			}
      	}
    }
    else
	{
      	variations = {"nom"}; 
    	for(const std::string& variation : variations)
		{
			column_name_pileupjetid = output_var + variation;
			_rlm = _rlm.Redefine(column_name_pileupjetid, [pileupjetidweightgenerator, variation, pileupjetid_cut, _PileupJetIDWP](const ROOT::VecOps::RVec<float>& etas, const ROOT::VecOps::RVec<float>& pts, const ROOT::VecOps::RVec<int>& pileupjetid_scores)
			{
    			float weight = pileupjetidweightgenerator(etas, pts, pileupjetid_scores, pileupjetid_cut, _PileupJetIDWP, variation);// Get the weight for the corresponding variation
	    		return weight;
	  		}, Jets_vars_names); //after all cuts, remove overlapped
			//std::cout<<"PileupJetID SF column name: "<<column_name_pileupjetid<<std::endl;
			if(isDefined(column_name_pileupjetid))
			{
	  			std::cout<<"PileupJetID SF column: "<<column_name_pileupjetid<<" is Changed in the Node."<<std::endl;
			}
  		}
	}
  	return _rlm;
}

ROOT::RDF::RNode NanoAODAnalyzerrdframe::calculateMuSF(RNode _rlm, std::vector<std::string> Muon_vars, std::string output_var)
{

    //=====================================================Muon SF and eventweight============================================================// 
    //muontype= for thight: NUM_TightID_DEN_genTracks //for medium: NUM_MediumID_DEN_TrackerMuons
    //Muon MediumID ISO UL type: NUM_TightRelIso_DEN_MediumID && thightID:NUM_TightRelIso_DEN_TightIDandIPCut --> the type can be found in json file
    //--> As an example Medium wp is used 
    //===============================================================================================================================================//
    //cout<<"muon HLT SF for MC "<<endl;
  	auto muon_weightgenerator = [this](const std::string& muon_type, const ROOT::VecOps::RVec<float>& etas, const ROOT::VecOps::RVec<float>& pts, const std::string& variation) -> float
	{
    	double muon_w = 1.0;
		//std::cout<<"Initial Individual weight with variation ("<<variation<<"): "<<muon_w<<std::endl;
      	for(std::size_t i = 0; i < pts.size(); i++)
		{
			if(pts[i]<30.0001) continue;
			//std::cout<<"Muon abs_eta:"<<std::fabs(etas[i])<<" pt: "<<pts[i]<<std::endl;
			string mu_key = _year+"_"+_runtype;
			double w = _correction_muon->at(muon_type)->evaluate({mu_key, std::fabs(etas[i]), pts[i], variation});
			muon_w *= w;
			//std::cout<<"Individual weight (muon "<<i<<") with variation ("<<variation<<"): "<<w<<std::endl;
			//std::cout<<"Cumulative weight after muon "<<i<<": "<<muon_w<<std::endl;
      	}
      	return muon_w;
    };

	auto muon_trigger_weightgenerator = [this](const std::string& muon_type, const ROOT::VecOps::RVec<float>& etas, const ROOT::VecOps::RVec<float>& pts, const std::string& variation, const int pdgId, const float leading_pt) -> float
	{
    	double muonTrigger_w = 1.0;
		//std::cout<<"Initial Individual weight with variation ("<<variation<<"): "<<muonTrigger_w<<std::endl;
      	for(std::size_t i = 0; i < pts.size(); i++)
		{
			if(pts[i]<30.0001) continue;
			if(pts[i] == leading_pt && std::fabs(pdgId) == 13)
			{
				//std::cout<<"Muon abs_eta:"<<std::fabs(etas[i])<<" pt: "<<pts[i]<<std::endl;
				string mu_key = _year+"_"+_runtype;
				double w = _correction_muon->at(muon_type)->evaluate({mu_key, std::fabs(etas[i]), pts[i], variation});
				muonTrigger_w *= w;
				// std::cout<<"Individual weight (muon "<<i<<") with variation ("<<variation<<"): "<<w<<std::endl;
				// std::cout<<"Cumulative weight after muon "<<i<<": "<<muonTrigger_w<<std::endl;
			}
		}
      	return muonTrigger_w;
    };

	//'sf' is nominal, and 'systup' and 'systdown' are up/down variations with total stat+-syst uncertainties. Individual systs are also available (in these cases syst only, not sf +/- syst
	std::vector<std::string> variations = {"sf", "systup", "systdown", "syst", "stat"};

    //cout<<"Generate MUONHLT weight"<<endl;
	//muonHLT sf and systematics with up/down variations
 	//===========//===========//===========//===========//===========
	// define muon HLT weight sf/systs for each variation individually
  	for(const std::string& variation : variations)
	{
    	std::string column_name_hlt = output_var+"hlt_" + variation;
		//std::cout<<"Syst name "<<column_name_hlt<<std::endl;
		_rlm = _rlm.Define(column_name_hlt, [this, muon_trigger_weightgenerator, variation](const ROOT::VecOps::RVec<float>& etas, const ROOT::VecOps::RVec<float>& pts, const int pdgId, const float leading_pt)
		{
			float weight = muon_trigger_weightgenerator(_muon_hlt_type, etas, pts, variation, pdgId, leading_pt); // Get the weight for the corresponding variation
			// std::cout<<"Muon HLT weight ("<<variation<<"): "<<weight<<std::endl;
			return weight;
      	}, Muon_vars);

    	std::string column_name_reco = output_var+"reco_" + variation;
		//std::cout<<"Syst name "<<column_name_reco<<std::endl;
    	_rlm = _rlm.Define(column_name_reco, [this, muon_weightgenerator, variation](const ROOT::VecOps::RVec<float>& etas, const ROOT::VecOps::RVec<float>& pts, const int pdgId, const float leading_pt)
		{
			float weight = muon_weightgenerator(_muon_reco_type, etas, pts, variation); // Get the weight for the corresponding variation
			//std::cout<<"Muon RECO weight ("<<variation<<"): "<<weight<<std::endl;
			return weight;
    	}, Muon_vars);

    	std::string column_name_id = output_var+"id_" + variation;
    	//std::cout<<"Syst name "<<column_name_id<<std::endl;
		_rlm = _rlm.Define(column_name_id, [this, muon_weightgenerator, variation](const ROOT::VecOps::RVec<float>& etas, const ROOT::VecOps::RVec<float>& pts, const int pdgId, const float leading_pt)
		{
			float weight = muon_weightgenerator(_muon_id_type, etas, pts, variation); // Get the weight for the corresponding variation
			//std::cout<<"Muon ID weight ("<<variation<<"): "<<weight<<std::endl;
			return weight;
    	}, Muon_vars);

		std::string column_name_iso = output_var+"iso_" + variation;
		//std::cout<<"Syst name "<<column_name_iso<<std::endl;
    	_rlm = _rlm.Define(column_name_iso, [this, muon_weightgenerator, variation](const ROOT::VecOps::RVec<float>& etas, const ROOT::VecOps::RVec<float>& pts, const int pdgId, const float leading_pt)
		{
			float weight = muon_weightgenerator(_muon_iso_type, etas, pts, variation); // Get the weight for the corresponding variation
			//std::cout<<"Muon ISO weight ("<<variation<<"): "<<weight<<std::endl;
			return weight;
    	}, Muon_vars);
    
	    std::string column_name = output_var;
    	if(variation=="sf")
		{
      		column_name += "central";
    	}
    	else if(variation=="systup")
		{
      		column_name += "up";
    	}
    	else if(variation=="systdown")
		{
      		column_name += "down";
    	}
    	else if(variation=="syst")
		{
      		column_name += "syst";
    	}
    	else if(variation=="stat")
		{
      		column_name += "stat";
    	}

    	std::string sf_definition = column_name_hlt+" * "+column_name_reco+" * "+column_name_id+" * "+column_name_iso;
    	_rlm = _rlm.Define(column_name, sf_definition);
		//std::cout<<"Muon SF column name: "<<column_name<<std::endl;
	}

  	return _rlm;
}

ROOT::RDF::RNode NanoAODAnalyzerrdframe::calculateMuSF_loosett1l(RNode _rlm, std::vector<std::string> Muon_vars, std::string output_var)
{

    //=====================================================Muon SF and eventweight============================================================// 
    //muontype= for thight: NUM_TightID_DEN_genTracks //for medium: NUM_MediumID_DEN_TrackerMuons
    //Muon MediumID ISO UL type: NUM_TightRelIso_DEN_MediumID && thightID:NUM_TightRelIso_DEN_TightIDandIPCut --> the type can be found in json file
    //--> As an example Medium wp is used 
    //===============================================================================================================================================//
    //cout<<"muon HLT SF for MC "<<endl;
  	auto muon_weightgenerator = [this](const std::string& muon_type, const ROOT::VecOps::RVec<float>& etas, const ROOT::VecOps::RVec<float>& pts, const std::string& variation) -> float
	{
    	double muon_w = 1.0;
		//std::cout<<"Initial Individual weight with variation ("<<variation<<"): "<<muon_w<<std::endl;
      	for(std::size_t i = 0; i < pts.size(); i++)
		{
			if(pts[i]<30.0001) continue;
			//std::cout<<"Muon abs_eta:"<<std::fabs(etas[i])<<" pt: "<<pts[i]<<std::endl;
			string mu_key = _year+"_"+_runtype;
			double w = _correction_muon->at(muon_type)->evaluate({mu_key, std::fabs(etas[i]), pts[i], variation});
			muon_w *= w;
			//std::cout<<"Individual weight (muon "<<i<<") with variation ("<<variation<<"): "<<w<<std::endl;
			//std::cout<<"Cumulative weight after muon "<<i<<": "<<muon_w<<std::endl;
      	}
      	return muon_w;
    };

	auto muon_trigger_weightgenerator = [this](const std::string& muon_type, const ROOT::VecOps::RVec<float>& etas, const ROOT::VecOps::RVec<float>& pts, const std::string& variation, const int pdgId, const float leading_pt) -> float
	{
    	double muonTrigger_w = 1.0;
		//std::cout<<"Initial Individual weight with variation ("<<variation<<"): "<<muonTrigger_w<<std::endl;
      	for(std::size_t i = 0; i < pts.size(); i++)
		{
			if(pts[i]<30.0001) continue;
			if(pts[i] == leading_pt && std::fabs(pdgId) == 13)
			{
				//std::cout<<"Muon abs_eta:"<<std::fabs(etas[i])<<" pt: "<<pts[i]<<std::endl;
				string mu_key = _year+"_"+_runtype;
				double w = _correction_muon->at(muon_type)->evaluate({mu_key, std::fabs(etas[i]), pts[i], variation});
				muonTrigger_w *= w;
				//std::cout<<"Individual weight (muon "<<i<<") with variation ("<<variation<<"): "<<w<<std::endl;
				//std::cout<<"Cumulative weight after muon "<<i<<": "<<muonTrigger_w<<std::endl;
			}
		}
      	return muonTrigger_w;
    };

	//'sf' is nominal, and 'systup' and 'systdown' are up/down variations with total stat+-syst uncertainties. Individual systs are also available (in these cases syst only, not sf +/- syst
    std::vector<std::string> variations = {"sf", "systup", "systdown", "syst", "stat"};

	//cout<<"Generate MUONHLT weight"<<endl;
  	//muonHLT sf and systematics with up/down variations
	//===========//===========//===========//===========//===========
  	// define muon HLT weight sf/systs for each variation individually
	for(const std::string& variation : variations)
	{
		std::string column_name_hlt = output_var+"hlt_" + variation;
    	//std::cout<<"Syst name "<<column_name_hlt<<std::endl;
    	_rlm = _rlm.Define(column_name_hlt, [this, muon_trigger_weightgenerator, variation](const ROOT::VecOps::RVec<float>& etas, const ROOT::VecOps::RVec<float>& pts, const int pdgId, const float leading_pt)
		{
			float weight = muon_trigger_weightgenerator(_muon_hlt_type, etas, pts, variation, pdgId, leading_pt); // Get the weight for the corresponding variation
			//std::cout<<"Muon HLT weight ("<<variation<<"): "<<weight<<std::endl;
			return weight;
      	}, Muon_vars);

    	std::string column_name_reco = output_var+"reco_" + variation;
    	//std::cout<<"Syst name "<<column_name_reco<<std::endl;
		_rlm = _rlm.Define(column_name_reco, [this, muon_weightgenerator, variation](const ROOT::VecOps::RVec<float>& etas, const ROOT::VecOps::RVec<float>& pts, const int pdgId, const float leading_pt)
		{
			float weight = muon_weightgenerator(_muon_reco_type, etas, pts, variation); // Get the weight for the corresponding variation
			//std::cout<<"Muon RECO weight ("<<variation<<"): "<<weight<<std::endl;
			return weight;
      	}, Muon_vars);

    	std::string column_name_id = output_var+"id_" + variation;
		//std::cout<<"Syst name "<<column_name_id<<std::endl;
    	_rlm = _rlm.Define(column_name_id, [this, muon_weightgenerator, variation](const ROOT::VecOps::RVec<float>& etas, const ROOT::VecOps::RVec<float>& pts, const int pdgId, const float leading_pt)
		{
			float weight = muon_weightgenerator(_muon_id_type, etas, pts, variation); // Get the weight for the corresponding variation
			//std::cout<<"Muon ID weight ("<<variation<<"): "<<weight<<std::endl;
			return weight;
    	}, Muon_vars);

    	std::string column_name_iso = output_var+"iso_" + variation;
    	//std::cout<<"Syst name "<<column_name_iso<<std::endl;
		_rlm = _rlm.Define(column_name_iso, [this, muon_weightgenerator, variation](const ROOT::VecOps::RVec<float>& etas, const ROOT::VecOps::RVec<float>& pts, const int pdgId, const float leading_pt)
		{
			float weight = muon_weightgenerator(_muon_loosett1l_iso_type, etas, pts, variation); // Get the weight for the corresponding variation
			//std::cout<<"Muon ISO weight ("<<variation<<"): "<<weight<<std::endl;
			return weight;
    	}, Muon_vars);

    	std::string column_name = output_var;
		if(variation == "sf")
		{
    		column_name += "central";
		}
    	else if(variation == "systup")
		{
  			column_name += "up";
    	}
    	else if(variation == "systdown")
		{
      		column_name += "down";
    	}
    	else if(variation == "syst")
		{
      		column_name += "syst";
    	}
    	else if(variation == "stat")
		{
      		column_name += "stat";
    	}

    	std::string sf_definition = column_name_hlt+" * "+column_name_reco+" * "+column_name_id+" * "+column_name_iso;
		_rlm = _rlm.Define(column_name, sf_definition);
    	//std::cout<<"Muon SF column name: "<<column_name<<std::endl;
	}
	
  	return _rlm;
}

double NanoAODAnalyzerrdframe::getEleTriggerEff(double eta, double pt, std::string variation)
{
  	double efficiency = 1.0;
	double statdata = 1.0;
	double statmc = 1.0;
	double systmc = 1.0;
  	//int maxXBin = -1;
  	//int maxYBin = -1;
  	int binX = -1;
  	int binY = -1;
  	// Get the maximum bin number for x and y axes
    //maxXBin = hist_electriggerEff_eff->GetXaxis()->GetNbins();
    //maxYBin = hist_electriggerEff_eff->GetYaxis()->GetNbins();

    // Get the bin number corresponding to the provided x and y values
    binX = hist_electriggerEff_eff->GetXaxis()->FindBin(std::abs(eta));
    binY = hist_electriggerEff_eff->GetYaxis()->FindBin(pt);
    efficiency = hist_electriggerEff_eff->GetBinContent(binX, binY);
	binX = hist_electriggerEff_statdata->GetXaxis()->FindBin(std::abs(eta));
    binY = hist_electriggerEff_statdata->GetYaxis()->FindBin(pt);
    statdata = hist_electriggerEff_statdata->GetBinContent(binX, binY);
	binX = hist_electriggerEff_statmc->GetXaxis()->FindBin(std::abs(eta));
    binY = hist_electriggerEff_statmc->GetYaxis()->FindBin(pt);
    statmc = hist_electriggerEff_statmc->GetBinContent(binX, binY);
	binX = hist_electriggerEff_systmc->GetXaxis()->FindBin(std::abs(eta));
    binY = hist_electriggerEff_systmc->GetYaxis()->FindBin(pt);
    systmc = hist_electriggerEff_systmc->GetBinContent(binX, binY);

	// std::cout<<"The Electron Trigger efficiency with eta = "<<eta<<" and pt = "<<pt<<" is "<<efficiency<<std::endl;
	// std::cout<<"The Electron Trigger stat data with eta = "<<eta<<" and pt = "<<pt<<" is "<<statdata<<std::endl;
	// std::cout<<"The Electron Trigger stat mc with eta = "<<eta<<" and pt = "<<pt<<" is "<<statmc<<std::endl;
	// std::cout<<"The Electron Trigger syst mc with eta = "<<eta<<" and pt = "<<pt<<" is "<<systmc<<std::endl;
	if(variation == "sf")
	{
		return efficiency;
	}
	else if(variation == "systup")
	{
		return efficiency + sqrt(systmc*systmc + statmc*statmc);
	}
	else if(variation == "systdown")
	{
		return efficiency - sqrt(systmc*systmc + statmc*statmc);
	}
	else if(variation == "syst")
	{
		return sqrt(systmc*systmc + statmc*statmc);
	}
	else if(variation == "stat")
	{
		return statdata;
	}
}

ROOT::RDF::RNode NanoAODAnalyzerrdframe::calculateEleSF(RNode _rlm, std::vector<std::string> Ele_vars, std::string output_var)
{

	//auto cs = correction::CorrectionSet::from_file("electron.json.gz");
    //cout<<"Generate ELECTRONRECO weight"<<endl;
    //electronRECO sf and systematics with up/down variations
    //===========//===========//===========//===========//===========
  	auto electron_weightgenerator = [this](const std::string eletype, const ROOT::VecOps::RVec<float>& etas, const ROOT::VecOps::RVec<float>& pts, const std::string& variation) -> float
	{
      	double electron_w = 1.0;
      	for(std::size_t i = 0; i < pts.size(); i++)
		{
			string ele_key = _year;
			double w = _correction_electron->at("UL-Electron-ID-SF")->evaluate({ele_key, variation, eletype, std::fabs(etas[i]), pts[i]}); 
			electron_w *= w;
			//std::cout<<"Individual weight (electron "<<i<<"): "<<w<<std::endl;
			//std::cout<<"Cumulative weight after electron "<<i<<": "<<electronId_w<<std::endl;
      	}
      	return electron_w;
    };

	auto electron_trigger_weightgenerator = [this](const ROOT::VecOps::RVec<float>& etas, const ROOT::VecOps::RVec<float>& pts, const std::string& variation, const int pdgId, const float leading_pt) -> float
	{
      	double electronTrigger_w = 1.0;
      	for(std::size_t i = 0; i < pts.size(); i++)
		{
			if(pts[i] > 499.99999) continue;
			if(pts[i] == leading_pt && std::fabs(pdgId) == 11)
			{
				double w = getEleTriggerEff(etas[i], pts[i], variation); 
				electronTrigger_w *= w;
				// std::cout<<"Individual weight (electron "<<i<<"): "<<w<<std::endl;
				// std::cout<<"Cumulative weight after electron "<<i<<": "<<electronTrigger_w<<std::endl;
			}
		}
      	return electronTrigger_w;
    };

	//'sf' is nominal, and 'systup' and 'systdown' are up/down variations with total stat+-syst uncertainties. Individual systs are also available (in these cases syst only, not sf +/- syst
	std::vector<std::string> variations_elec = {"sf", "sfup", "sfdown"};

    for(const std::string& variation : variations_elec)
	{
      	// define electron RECO weight sf/systs for each variation individually
	  	std::string column_name_reco = output_var+ "reco_" + variation;
	  	_rlm = _rlm.Define(column_name_reco, [this, electron_weightgenerator, variation](const ROOT::VecOps::RVec<float>& etas, const ROOT::VecOps::RVec<float>& pts, const int pdgId, const float leading_pt)
		{
			float weight = electron_weightgenerator(_electron_reco_type, etas, pts, variation); // Get the weight for the corresponding variation
  			// std::cout<<"Electron RECO weight ("<<variation<<"): "<<weight<<std::endl;
	  		return weight;
		}, Ele_vars);

	    // define electron ID weight sf/systs for each variation individually
      	std::string column_name_id = output_var+ "id_" + variation;
  		_rlm = _rlm.Define(column_name_id, [this, electron_weightgenerator, variation](const ROOT::VecOps::RVec<float>& etas, const ROOT::VecOps::RVec<float>& pts, const int pdgId, const float leading_pt)
		{
			float weight = electron_weightgenerator(_electron_id_type, etas, pts, variation); // Get the weight for the corresponding variation
	  		// std::cout<<"Electron ID weight ("<<variation<<"): "<<weight<<std::endl;
		  	return weight;
		}, Ele_vars);

		// define electron HLT weight sf/systs for each variation individually
		std::vector<std::string> variations = {"sf", "systup", "systdown", "syst", "stat"};
		for(const std::string& variation2 : variations)
		{
		  	std::string column_name_hlt = output_var+ "hlt_" + variation2;
			if(variation == "sf")
			{
				_rlm = _rlm.Define(column_name_hlt, [this, electron_trigger_weightgenerator, variation2](const ROOT::VecOps::RVec<float>& etas, const ROOT::VecOps::RVec<float>& pts, const int pdgId, const float leading_pt)
				{
					float weight = electron_trigger_weightgenerator(etas, pts, variation2, pdgId, leading_pt); // Get the weight for the corresponding variation2
				  	// std::cout<<"Electron HLT weight ("<<variation2<<"): "<<weight<<std::endl;
					return weight;
				}, Ele_vars);
			}

			std::string column_name = output_var;
	      	if(variation == "sf" && variation2 == "sf")
			{
				column_name += "central";
    		}
      		else if(variation == "sfup" && variation2 == "systup")
			{
				column_name += "up";
    		}
  			else if (variation == "sfdown" && variation2 == "systdown")
			{
				column_name += "down";
    		}

    	  	std::string sf_definition = column_name_reco+" * "+column_name_id+" * "+column_name_hlt;
      		if((variation == "sf" && variation2 == "sf") || (variation == "sfup" && variation2 == "systup") || (variation == "sfdown" && variation2 == "systdown"))
			{
				_rlm = _rlm.Define(column_name, sf_definition);
			}
			//std::cout<<"Electron SF column name: "<<column_name<<std::endl;
		}
	}

    return _rlm;
}

ROOT::RDF::RNode NanoAODAnalyzerrdframe::calculateEleSF_loosett1l(RNode _rlm, std::vector<std::string> Ele_vars, std::string output_var)
{

	//auto cs = correction::CorrectionSet::from_file("electron.json.gz");
    //cout<<"Generate ELECTRONRECO weight"<<endl;
    //electronRECO sf and systematics with up/down variations
    //===========//===========//===========//===========//===========
  	auto electron_weightgenerator = [this](const std::string eletype, const ROOT::VecOps::RVec<float>& etas, const ROOT::VecOps::RVec<float>& pts, const std::string& variation) -> float
	{
      	double electron_w = 1.0;
      	for(std::size_t i = 0; i < pts.size(); i++)
		{
			string ele_key = _year;
			double w = _correction_electron->at("UL-Electron-ID-SF")->evaluate({ele_key, variation, eletype, std::fabs(etas[i]), pts[i]}); 
			electron_w *= w;
			//std::cout<<"Individual weight (electron "<<i<<"): "<<w<<std::endl;
			//std::cout<<"Cumulative weight after electron "<<i<<": "<<electronId_w<<std::endl;
      	}
      	return electron_w;
    };

	auto electron_trigger_weightgenerator = [this](const ROOT::VecOps::RVec<float>& etas, const ROOT::VecOps::RVec<float>& pts, const std::string& variation, const int pdgId, const float leading_pt) -> float
	{
      	double electronTrigger_w = 1.0;
      	for(std::size_t i = 0; i < pts.size(); i++)
		{
			if(pts[i] > 499.99999) continue;
			if(pts[i] == leading_pt && std::fabs(pdgId) == 11)
			{
				double w = getEleTriggerEff(etas[i], pts[i], variation); 
				electronTrigger_w *= w;
				// std::cout<<"Individual weight (electron "<<i<<"): "<<w<<std::endl;
				// std::cout<<"Cumulative weight after electron "<<i<<": "<<electronTrigger_w<<std::endl;
			}
		}
      	return electronTrigger_w;
    };

	//'sf' is nominal, and 'systup' and 'systdown' are up/down variations with total stat+-syst uncertainties. Individual systs are also available (in these cases syst only, not sf +/- syst
	std::vector<std::string> variations_elec = {"sf", "sfup", "sfdown"};

    for(const std::string& variation : variations_elec)
	{
      	// define electron RECO weight sf/systs for each variation individually
	  	std::string column_name_reco = output_var+ "reco_" + variation;
	  	_rlm = _rlm.Define(column_name_reco, [this, electron_weightgenerator, variation](const ROOT::VecOps::RVec<float>& etas, const ROOT::VecOps::RVec<float>& pts, const int pdgId, const float leading_pt)
		{
			float weight = electron_weightgenerator(_electron_reco_type, etas, pts, variation); // Get the weight for the corresponding variation
	  		// std::cout<<"Electron RECO weight ("<<variation<<"): "<<weight<<std::endl;
		  	return weight;
		}, Ele_vars);

    	// define electron ID weight sf/systs for each variation individually
  		std::string column_name_id = output_var+ "id_" + variation;
      	_rlm = _rlm.Define(column_name_id, [this, electron_weightgenerator, variation](const ROOT::VecOps::RVec<float>& etas, const ROOT::VecOps::RVec<float>& pts, const int pdgId, const float leading_pt)
		{
  			float weight = electron_weightgenerator(_electron_loosett1l_id_type, etas, pts, variation); // Get the weight for the corresponding variation
	  		//std::cout<<"Electron ID weight ("<<variation<<"): "<<weight<<std::endl;
	  		return weight;
		}, Ele_vars);

		// define electron HLT weight sf/systs for each variation individually
		std::vector<std::string> variations = {"sf", "systup", "systdown", "syst", "stat"};
		for(const std::string& variation2 : variations)
		{
		  	std::string column_name_hlt = output_var+ "hlt_" + variation2;
			if(variation == "sf")
			{
				_rlm = _rlm.Define(column_name_hlt, [this, electron_trigger_weightgenerator, variation2](const ROOT::VecOps::RVec<float>& etas, const ROOT::VecOps::RVec<float>& pts, const int pdgId, const float leading_pt)
				{
					float weight = electron_trigger_weightgenerator(etas, pts, variation2, pdgId, leading_pt); // Get the weight for the corresponding variation2
				  	// std::cout<<"Electron HLT weight ("<<variation2<<"): "<<weight<<std::endl;
					return weight;
				}, Ele_vars);
			}

			std::string column_name = output_var;
	      	if(variation == "sf" && variation2 == "sf")
			{
				column_name += "central";
    		}
      		else if(variation == "sfup" && variation2 == "systup")
			{
				column_name += "up";
    		}
  			else if (variation == "sfdown" && variation2 == "systdown")
			{
				column_name += "down";
    		}

    	  	std::string sf_definition = column_name_reco+" * "+column_name_id+" * "+column_name_hlt;
      		if((variation == "sf" && variation2 == "sf") || (variation == "sfup" && variation2 == "systup") || (variation == "sfdown" && variation2 == "systdown"))
			{
				_rlm = _rlm.Define(column_name, sf_definition);
			}
			//std::cout<<"Electron SF column name: "<<column_name<<std::endl;
		}
	}

    return _rlm;
}

ROOT::RDF::RNode NanoAODAnalyzerrdframe::applyPrefiringWeight(RNode _rlm, std::string output_var)
{
  
    std::vector<std::string> variations = {"Nom", "Up", "Dn"};
    std::vector<std::string> output_variations = {"central", "up", "down"};
    for(int i =0; i<int(variations.size()); i++)
	{
      	std::string input_column_name = "L1PreFiringWeight_" + variations[i];
      	std::string output_column_name = output_var + output_variations[i];
      	_rlm = _rlm.Define(output_column_name, input_column_name);
    }
    return _rlm;
}

ROOT::RDF::RNode NanoAODAnalyzerrdframe::addTheorySystematics(RNode _rlm)
{
  	// Store sum of weights
  	auto storePDFWeights = [this](floats weights, const float& gen)->floats
	{
		for(unsigned int i=0; i<weights.size(); i++)
		{
      		PDFWeights[i] += (gen / abs(gen)) * weights[i];
		}
    	return PDFWeights;
  	};

  	auto storePSWeights = [this](const floats& weights, const float& gen)->floats
	{
    	for(unsigned int i=0; i<weights.size(); i++)
		{
      		if(i > 3) continue; //JME Nano stores all PS
      		PSWeights[i] += (gen / abs(gen)) * weights[i];
    	}
    return PSWeights;
  	};

  	auto storeScaleWeights = [this](const floats& weights, const float& gen)->floats
	{
		for(unsigned int i=0; i<weights.size(); i++)
		{
      		ScaleWeights[i] += (gen / abs(gen)) * weights[i];
		}
    	return ScaleWeights;
  	};

  	try
	{
    	_rlm.Foreach(storePDFWeights, {"LHEPdfWeight", "genWeight"});
  	}
	catch (exception& e)
	{
    	cout<<e.what()<<endl;
    	cout<<"No PDF weight in this root file!"<<endl;
  	}
  	
	try
	{
    	_rlm.Foreach(storePSWeights, {"PSWeight", "genWeight"});
  	}
	catch (exception& e)
	{
    	cout<<e.what()<<endl;
    	cout<<"No PS weight in this root file!"<<endl;
  	}

  	try
	{
    	_rlm.Foreach(storeScaleWeights, {"LHEScaleWeight", "genWeight"});
  	}
	catch (exception& e)
	{
    	cout<<e.what()<<endl;
    	cout<<"No Scale weight in this root file!"<<endl;
  	}
  	return _rlm;
}

void NanoAODAnalyzerrdframe::topPtReweight(std::string process)
{

	_rlm = _rlm.Define("gentopcut", "abs(GenPart_pdgId) == 6 && GenPart_statusFlags & (1 << 13)")
			   .Define("GenPart_top_pt", "GenPart_pt[gentopcut]");

  	auto topPtNLOtoNNLO = [](const floats& toppt)->floats
	{
    	floats out;
    	out.reserve(3);
    	if(toppt.size() != 2)
		{
      		out.emplace_back(1.0); out.emplace_back(1.0); out.emplace_back(1.0);
    	}
		else
		{
      		float pt1 = toppt[0];
      		float pt2 = toppt[1];
      		float nom_w1 = (0.103 * std::exp(-0.0118 * pt1) - 0.000134 * pt1 + 0.973);
      		float nom_w2 = (0.103 * std::exp(-0.0118 * pt2) - 0.000134 * pt2 + 0.973);
      		float nom_weight = std::sqrt(nom_w1 * nom_w2);
      		out.emplace_back(nom_weight);

      		// blessed by TOP-22-003 team (ANv16) this goes to nom weight fct
      		std::vector<float> xbins = {0,100,200,300,400,500,600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2100,2200,2300,2400,2500};
      		std::vector<float> unc_up = {0, 0.2634291, 0.125821, 0.1204998, 0.1185625, 0.1225926, 0.1286782, 0.1353967, 0.1415252, 0.1471104, 0.1555594, 0.1580664, 0.1604668,
					0.163553, 0.1690409, 0.1725839, 0.1755679, 0.1779287, 0.1808263, 0.1841962, 0.1866275, 0.1877652, 0.1897599, 0.1920097, 0.20095 };
      		std::vector<float> unc_dn = {0, 0.4420274, 0.1457186, 0.1366773, 0.1278782, 0.1296307, 0.1372049, 0.1473315, 0.1561993, 0.1645779, 0.168799, 0.1779081, 0.1835472,
					0.1903936, 0.1969376, 0.2013376, 0.2056985, 0.2082445, 0.214616, 0.2186682, 0.2240442, 0.2240283, 0.2287682, 0.2321543, 0.249874 };

      		if(pt1 >= 2500) pt1 = 2499;
      		if(pt2 >= 2500) pt2 = 2499;
      		int xbin1 = (std::upper_bound(xbins.begin(), xbins.end(), pt1)-1) - xbins.begin();
      		int xbin2 = (std::upper_bound(xbins.begin(), xbins.end(), pt2)-1) - xbins.begin();
      		float up = nom_weight + std::sqrt(std::pow((nom_w2 * unc_up.at(xbin1)), 2) * std::pow((nom_w1 * unc_up.at(xbin2)), 2));
      		float dn = nom_weight - std::sqrt(std::pow((nom_w2 * unc_dn.at(xbin1)), 2) * std::pow((nom_w1 * unc_dn.at(xbin2)), 2));
      		out.emplace_back(up); out.emplace_back(dn);
    	}
    	return out;
  	};

	auto topPtDatatoPowheg = [](const floats& toppt)->floats
	{
	    floats out;
    	out.reserve(1);
	    if(toppt.size() != 2)
		{
    		out.emplace_back(1.0);
    	}
		else
		{
    		float pt1 = toppt[0];
    		float pt2 = toppt[1];
    		float nom_w1 = std::exp(0.0615-0.0005 * pt1);
			float nom_w2 = std::exp(0.0615-0.0005 * pt1);
    		float nom_weight = std::sqrt(nom_w1 * nom_w2);
    		out.emplace_back(nom_weight);
    	}
    return out;
  	};

  	// NLO ttbar: NLO to theory weight
  	// https://twiki.cern.ch/twiki/bin/view/CMS/TopPtReweighting#TOP_PAG_corrections_based_on_the
  	if(process.find("tt2l") != std::string::npos || process.find("tt1l") != std::string::npos || process.find("tt0l") != std::string::npos)
	{
    	// _rlm = _rlm.Define("TopPtWeight", topPtNLOtoNNLO, {"GenPart_top_pt"});
		_rlm = _rlm.Define("TopPtWeight", topPtDatatoPowheg, {"GenPart_top_pt"});
  	}
	else
	{
    	_rlm = _rlm.Define("TopPtWeight", "floats v{1.0, 1.0, 1.0}; return v;");
  	}
}

bool NanoAODAnalyzerrdframe::helper_1DHistCreator(std::string hname, std::string title, const int nbins, const double xlow, const double xhi, std::string rdfvar, std::string evWeight, RNode *anode)
{

	//cout<<"1DHistCreator "<<hname <<endl;
	RDF1DHist histojets = anode->Histo1D({hname.c_str(), title.c_str(), nbins, xlow, xhi}, rdfvar, evWeight); // Fill with weight given by evWeight
	_th1dhistos[hname] = histojets;
	//histojets.GetPtr()->Print("all");
	return true;
}

//for 2D histograms//
bool NanoAODAnalyzerrdframe::helper_2DHistCreator(std::string hname, std::string title, const int nbinsx, const double xlow, const double xhi, const int nbinsy, const double ylow, const double yhi, std::string rdfvarx, std::string rdfvary, std::string evWeight, RNode *anode)
{

	//cout<<"2DHistCreator "<<hname <<endl;
	RDF2DHist histojets = anode->Histo2D({hname.c_str(), title.c_str(), nbinsx, xlow, xhi,nbinsy, ylow, yhi}, rdfvarx,rdfvary, evWeight); // Fill with weight given by evWeight
	_th2dhistos[hname] = histojets;
	// histojets.GetPtr()->Print("all");
	return true;
}

//for 2D histograms with variable binning//
bool NanoAODAnalyzerrdframe::helper_2DHistCreator_vbin(std::string hname, std::string title, const int nbinsx, const double *xbins, const int nbinsy, const double *ybins, std::string rdfvarx, std::string rdfvary, std::string evWeight, RNode *anode)
{

	//cout<<"2DHistCreator "<<hname <<endl;
	RDF2DHist histojets = anode->Histo2D({hname.c_str(), title.c_str(), nbinsx, xbins,nbinsy, ybins}, rdfvarx,rdfvary, evWeight); // Fill with weight given by evWeight
	_th2dvbinhistos[hname] = histojets;
	// histojets.GetPtr()->Print("all");
	return true;
}

// Automatically loop to create
void NanoAODAnalyzerrdframe::setupCuts_and_Hists(std::string jes_unc_level_start_idx)
{

	ROOT::EnableImplicitMT();
	cout<<"setting up definitions, cuts, and histograms" <<endl;

	for(auto &c : _varinfovector)
	{
		if(c.mincutstep.length()==0) _rlm = _rlm.Define(c.varname, c.vardefinition);
	}

	for(auto &x : _hist1dinfovector)
	{
		std::string hpost = "_nocut";

		if(x.mincutstep.length()==0)
		{
			helper_1DHistCreator(std::string(x.hmodel.fName)+hpost,  std::string(x.hmodel.fTitle)+hpost, x.hmodel.fNbinsX, x.hmodel.fXLow, x.hmodel.fXUp, x.varname, x.weightname, &_rlm);
		}
	}

	//for 2D histograms
	for(auto &x : _hist2dinfovector)
	{
		std::string hpost = "_nocut";

		if(x.mincutstep.length()==0)
		{
			helper_2DHistCreator(std::string(x.hmodel.fName)+hpost,  std::string(x.hmodel.fTitle)+hpost, x.hmodel.fNbinsX, x.hmodel.fXLow, x.hmodel.fXUp, x.hmodel.fNbinsY, x.hmodel.fYLow, x.hmodel.fYUp, x.varname1, x.varname2, x.weightname, &_rlm);
		}
	}

	//for 2D histograms with variable binning
	for(auto &x : _hist2dvbininfovector)
	{
		std::string hpost = "_nocut";

		if(x.mincutstep.length()==0)
		{
			double Xbins[4] = {0., 0.6, 1.2, 2.5};
			double Ybins[9] = {0., 40., 60., 80., 100., 150., 200., 300., 1000.};
			helper_2DHistCreator_vbin(std::string(x.hmodel.fName)+hpost,  std::string(x.hmodel.fTitle)+hpost, x.hmodel.fNbinsX, Xbins, x.hmodel.fNbinsY, Ybins, x.varname1, x.varname2, x.weightname, &_rlm);
		}
	}

	_rnt.setRNode(&_rlm);

	for(auto acut : _cutinfovector)
	{
		//std::cout<<acut.idx<<" this is the cut we are analysing"<<std::endl;
	  	//std::cout<<jes_unc_level_start_idx<<" this is the jes_unc_level_start_idx we are analysing"<<std::endl;
		size_t lastNonZeroPos_nominal = jes_unc_level_start_idx.find_last_not_of('0');
	  	size_t lastNonZeroPos_jercunc = acut.idx.find_last_not_of('0');
	  	std::string parent_idx;
	  	string zeros_in_Nominal = std::string(jes_unc_level_start_idx.length() - lastNonZeroPos_nominal - 1, '0');
	  	string zeros_in_jercunc = std::string(acut.idx.length() - lastNonZeroPos_jercunc - 1, '0');
	  	//std::cout<<zeros_in_Nominal<<" this is the zeros_in_Nominal we are analysing"<<std::endl;
	  	//std::cout<<zeros_in_jercunc<<" this is the zeros_in_jercunc we are analysing"<<std::endl;
	  	
		/*if(!acut.idx.empty() && acut.idx.front() == '0')
		{
	    	parent_idx = acut.idx;
	  	}
	  	else if(!acut.idx.empty() && zeros_in_jercunc==zeros_in_Nominal)
		{
	    	parent_idx = std::string(jes_unc_level_start_idx.length() - lastNonZeroPos_nominal - 1, '0');
	  	}
	  	else
		{
	    	parent_idx = acut.idx;
	  	}*/

	  	parent_idx = acut.idx;
	  	//std::cout<<parent_idx.substr(0, parent_idx.length()-1) <<" this is the parent_idx we are analysing"<<std::endl;
		std::string cutname = "cut_"+ acut.idx;
	  	std::string hpost = "_"+cutname;
	  	//std::cout<<"Inside setup cut for "<<hpost<<std::endl;
	  	//RNode *r = _rnt.getParent(acut.idx)->getRNode();
	  	RNode *r = _rnt.getParent(parent_idx)->getRNode();
	  	auto rnext = new RNode(r->Define(cutname, acut.cutdefinition));
	  
	  	if(_jercunctag.size()>0 && !_isData)
		{
	    	int jes_unc_idx = 1;
	    	for(auto _unc_name : _jercunctag)
			{	    
	      		string jec_unc_reg_idx = jes_unc_level_start_idx.substr(0, jes_unc_level_start_idx.length()-1);
	      		string jec_unc_up_reg_idx = jec_unc_reg_idx+std::to_string(jes_unc_idx);
	      		string jec_unc_down_reg_idx = jec_unc_reg_idx+std::to_string(jes_unc_idx+1);
	      		//string jec_unc_up_reg_idx = _unc_name+"_Up_000";
	      		//string jec_unc_down_reg_idx = _unc_name+"_Down_000";
	      		string jet_pt_name;
	      		string jet_mass_name;
	      
	      		//if(acut.idx==jec_unc_up_reg_idx){
	      		if(acut.idx.find(jec_unc_up_reg_idx) == 0 && (acut.idx.length() == jec_unc_up_reg_idx.length() || (acut.idx.length() > jec_unc_up_reg_idx.length() && acut.idx[jec_unc_up_reg_idx.length()] == '_')))
				{
					jet_pt_name = "Jet_pt_"+_unc_name+"_up";
					jet_mass_name = "Jet_mass_"+_unc_name+"_up";
					*rnext = rnext->Redefine("Jet_pt_corr",jet_pt_name);
					*rnext = rnext->Redefine("Jet_mass_corr",jet_mass_name);
	      		}
				//else if(acut.idx==jec_unc_down_reg_idx){
	      		else if(acut.idx.find(jec_unc_down_reg_idx) == 0 && (acut.idx.length() == jec_unc_down_reg_idx.length() || (acut.idx.length() > jec_unc_down_reg_idx.length() && acut.idx[jec_unc_down_reg_idx.length()] == '_')))
				{
					jet_pt_name = "Jet_pt_"+_unc_name+"_down";
					jet_mass_name = "Jet_mass_"+_unc_name+"_down";
					*rnext = rnext->Redefine("Jet_pt_corr",jet_pt_name);
					*rnext = rnext->Redefine("Jet_mass_corr",jet_mass_name);
	      		}
	      		jes_unc_idx +=2;
	    	}
	  	}
		*rnext = rnext->Filter(cutname);

		for(auto &c : _varinfovector)
		{
			if(acut.idx.compare(c.mincutstep)==0) *rnext = rnext->Define(c.varname, c.vardefinition);
		}
		for(auto &x : _hist1dinfovector)
		{
			if(acut.idx.compare(0, x.mincutstep.length(), x.mincutstep)==0)
			{
				helper_1DHistCreator(std::string(x.hmodel.fName)+hpost,  std::string(x.hmodel.fTitle)+hpost, x.hmodel.fNbinsX, x.hmodel.fXLow, x.hmodel.fXUp, x.varname, x.weightname, rnext);
			}
		}

			//for 2DHistograms
		for(auto &x : _hist2dinfovector)
		{
			if(acut.idx.compare(0, x.mincutstep.length(), x.mincutstep)==0)
			{
				helper_2DHistCreator(std::string(x.hmodel.fName)+hpost,  std::string(x.hmodel.fTitle)+hpost, x.hmodel.fNbinsX, x.hmodel.fXLow, x.hmodel.fXUp, x.hmodel.fNbinsY, x.hmodel.fYLow, x.hmodel.fYUp, x.varname1, x.varname2, x.weightname, rnext);
			}
		}

		//for 2D histograms with variable binning
		for(auto &x : _hist2dvbininfovector)
		{
			if(acut.idx.compare(0, x.mincutstep.length(), x.mincutstep)==0)
			{
				double Xbins[4] = {0., 0.6, 1.2, 2.5};
				double Ybins[9] = {0., 40., 60., 80., 100., 150., 200., 300., 1000.};
				helper_2DHistCreator_vbin(std::string(x.hmodel.fName)+hpost,  std::string(x.hmodel.fTitle)+hpost, x.hmodel.fNbinsX, Xbins, x.hmodel.fNbinsY, Ybins, x.varname1, x.varname2, x.weightname, rnext);
			}
		}
		_rnt.addDaughter(rnext, acut.idx, parent_idx);
	}
}

void NanoAODAnalyzerrdframe::add1DHist(TH1DModel histdef, std::string variable, std::string weight, string mincutstep)
{
	_hist1dinfovector.push_back({histdef, variable, weight, mincutstep});
}
//for 2DHistograms
void NanoAODAnalyzerrdframe::add2DHist(TH2DModel histdef, std::string variable1, std::string variable2, std::string weight, string mincutstep)
{
	_hist2dinfovector.push_back({histdef, variable1,variable2, weight, mincutstep});
}
//for 2DHistograms with variable binning
void NanoAODAnalyzerrdframe::add2DHist_vbin(TH2DModel histdef, std::string variable1, std::string variable2, std::string weight, string mincutstep)
{
	_hist2dvbininfovector.push_back({histdef, variable1,variable2, weight, mincutstep});
}

void NanoAODAnalyzerrdframe::drawHists(RNode t)
{
	cout<<"processing" <<endl;
	t.Count();
}

void NanoAODAnalyzerrdframe::addVar(varinfo v)
{
	_varinfovector.push_back(v);
}

void NanoAODAnalyzerrdframe::addVartoStore(string varname)
{
	
	// varname is assumed to be a regular expression.
	// e.g. if varname is "Muon_eta" then "Muon_eta" will be stored
	// if varname=="Muon_.*", then any branch name that starts with "Muon_" string will
	// be saved
	_varstostore.push_back(varname);
}

void NanoAODAnalyzerrdframe::setupTree()
{

	std::cout<<"Inside Setup Tree"<<std::endl;
	vector<RNodeTree *> rntends;
	_rnt.getRNodeLeafs(rntends);
	for(auto arnt: rntends)
	{
		RNode *arnode = arnt->getRNode();
		string nodename = arnt->getIndex();
		vector<string> varforthistree;
		std::map<string, int> varused;

		if(nodename[0] == '3')
		{
			for(auto varname: _varstostore)
			{
				bool foundmatch = false;
				std::regex b(varname);
				for(auto a: arnode->GetColumnNames())
				{
					if(std::regex_match(a, b) && varused[a]==0)
					{
						varforthistree.push_back(a);
						varused[a]++;
						foundmatch = true;
					}
				}
				if(!foundmatch)
				{
					cout<<varname<<" not found at "<<nodename<<endl;
				}
			}
		}
		_varstostorepertree[nodename]  = varforthistree;
	}
}

void NanoAODAnalyzerrdframe::addCuts(string cut, string idx)
{
	_cutinfovector.push_back({cut, idx});
}

void NanoAODAnalyzerrdframe::run(bool saveAll, string outtreename, std::string process, bool Hist, bool JEC)
{

	vector<RNodeTree *> rntends;
	_rnt.getRNodeLeafs(rntends);
	// _rnt.Print();
    // cout<<rntends.size()<<endl;

	for(auto arnt: rntends)
	{
		string nodename = arnt->getIndex();
		// cout<<"nodename = "<<nodename<<endl;
		string outname = _outfilename;
		// cout<<"outname = "<<outname<<endl;
		int count = 0;
		bool good_idx = 0;
        for(int i = 0; i < nodename.length(); i++)
        {
            if(nodename[i] == '_') count++;
        }
		// cout<<"count = "<<count<<endl;
		if(process.find("signal") != std::string::npos)
		{
			if(Hist == 1)
			{
				if(count == 4) good_idx = 1;
			}
			else if(Hist == 0 && JEC == 0)
			{
				if(count == 4 && nodename[0] == '3') good_idx = 1;
				// if(nodename == "3_0_0_4_0") good_idx = 1;
			}
			else if(Hist == 0 && JEC == 1)
			{
				if(count == 2 && nodename[0] == '3' && nodename[2] != '0') good_idx = 1;
				// if(nodename == "3_0_0_4_0") good_idx = 1;
			}
		}
		else if(_isData)
		{
			if(Hist == 1)
			{
				if(count == 4) good_idx = 1;
			}
			else
			{
				if(count == 4 && nodename[0] == '3' && ((nodename[4] == '0' && nodename[8] == '1') || nodename[4] == '1' || nodename[4] == '2')) good_idx = 1;
				// if(nodename == "0_0_0_4_0" || nodename == "1_0_0_4_0" || nodename == "2_0_0_4_0") good_idx = 1;
				// if(nodename == "3_0_0_4_0" || nodename == "3_0_0_4_1" || nodename == "3_0_1_4_0" || nodename == "3_0_2_4_0" || nodename == "3_0_2_4_1") good_idx = 1;
			}
		}
		else
		{
			if(Hist == 1)
			{
				if(count == 5) good_idx = 1;
			}
			else if(Hist == 0 && JEC == 0)
			{
				if(count == 4 && nodename[0] == '3') good_idx = 1;
				// if(nodename == "3_0_0_4_0") good_idx = 1;
			}
			else if(Hist == 0 && JEC == 1)
			{
				if(count == 2 && nodename[0] == '3' && nodename[2] != '0') good_idx = 1;
				// if(nodename == "3_0_0_4_0") good_idx = 1;
			}
		}
		// cout<<"good_idx = "<<good_idx<<endl;
		// good_idx = 1;
		if(good_idx == 1)
		{
			if(Hist == 0)
			{
				if(rntends.size()>1) outname.replace(outname.find(".root"), 5, "_"+nodename+".root");
			}
			_outrootfilenames.push_back(outname);
			RNode *arnode = arnt->getRNode();
			std::cout<<"-------------------------------------------------------------------"<<std::endl;
            cout<<"cut : " ;
            cout<<arnt->getIndex();
			if(saveAll)
			{
				arnode->Snapshot(outtreename, outname);
			}
			else
			{
            	cout<<" --writing branches"<<endl;
				std::cout<<"-------------------------------------------------------------------"<<std::endl;
				for(auto bname: _varstostorepertree[nodename])
				{
					cout<<bname<<endl;
				}
				arnode->Snapshot(outtreename, outname, _varstostorepertree[nodename]);
			}
			std::cout<<"-------------------------------------------------------------------"<<std::endl;
			cout<<"Creating output root file :  "<<endl;
			cout<<outname<<" ";
			cout<<endl;
			std::cout<<"-------------------------------------------------------------------"<<std::endl;
			_outrootfile = new TFile(outname.c_str(), "UPDATE");
			cout<<"Writing histograms...   "<<endl;
			std::cout<<"-------------------------------------------------------------------"<<std::endl;
			for(auto &h : _th1dhistos)
			{
				if(h.second.GetPtr() != nullptr)
				{
					h.second.GetPtr()->Print();
					h.second.GetPtr()->Write();
				}
			}
			//for 2D histograms
			for(auto &h : _th2dhistos)
			{
				if(h.second.GetPtr() != nullptr)
				{
					h.second.GetPtr()->Print();
					h.second.GetPtr()->Write();
				}
			}
			//for 2D histograms with variable binning
			for(auto &h : _th2dvbinhistos)
			{
				if(h.second.GetPtr() != nullptr)
				{
					h.second.GetPtr()->Print();
					h.second.GetPtr()->Write();
				}
			}
			/*TH1F* hPDFWeights = new TH1F("LHEPdfWeightSum", "LHEPdfWeightSum", 103, 0, 1);
        	for(size_t i=0; i<PDFWeights.size(); i++)
			{
            	hPDFWeights->SetBinContent(i+1, PDFWeights[i]);
			}*/
			_outrootfile->Write(0, TObject::kOverwrite);
			_outrootfile->Close();
			if(_varstostorepertree[nodename].size()==0) break;
			if(Hist == 1)
			{
				break;
			}
		}
	}
    std::cout<<"-------------------------------------------------------------------"<<std::endl;
    std::cout<<"END...  :) "<<std::endl; 
}

void NanoAODAnalyzerrdframe::setParams(string year, string runtype, int datatype, bool Data)
{
    /*if(debug){
        std::cout<<"================================//================================="<<std::endl;
        std::cout<<"Line : "<<__LINE__<<" Function : "<<__FUNCTION__<<std::endl;
        std::cout<<"================================//================================="<<std::endl;
    }*/
	_year=year;
	_runtype=runtype;
	_datatype=datatype;
	

	if(_year=="2016preVFP" || _year=="2016postVFP")
	{
        cout<<"Analysing through Run 2016"<<endl;
    }
	else if(_year=="2017")
	{
        cout<<"Analysing through Run 2017"<<endl;
    }
	else if(_year=="2018")
	{
        cout<<"Analysing through Run 2018"<<endl;
    }

	if(_runtype.find("UL") != std::string::npos)
	{
        _isUL = true;
        cout<<"Ultra Legacy Selected "<<endl;
        std::cout<<"-------------------------------------------------------------------"<<std::endl;
    }
	else if(_runtype.find("ReReco") != std::string::npos)
	{
        _isReReco = true;
        cout<<" ReReco  Selected!"<<endl;
        std::cout<<"-------------------------------------------------------------------"<<std::endl;
    }
    if(!_isUL && !_isReReco)
	{
        std::cout<<"Default run version : UL or ReReco is not selected! "<<std::endl;
        std::cout<<"-------------------------------------------------------------------"<<std::endl;
    }

	if(_datatype==0)
	{
		_isData = false;
        std::cout<<" MC input files Selected!! "<<std::endl;
        std::cout<<"-------------------------------------------------------------------"<<std::endl;
    }
	else if(_datatype==1)
	{
        _isData = true;
        std::cout<<" DATA input files Selected!!" <<std::endl;
        std::cout<<"-------------------------------------------------------------------"<<std::endl;
    }
	if(_datatype==-1)
	{
		std::cout<<"Default root version :checking out gen branches! "<<std::endl;
        std::cout<<"-------------------------------------------------------------------"<<std::endl;

		if(_atree->GetBranch("genWeight") == nullptr && Data == 1)
		{
			_isData = true;
			cout<<"input file is DATA" <<endl;
		}
		else
		{
			_isData = false;
			cout<<"input file is MC" <<endl;
		}
	}
	TObjArray *allbranches = _atree->GetListOfBranches();
	for(int i =0; i<allbranches->GetSize(); i++)
	{
		TBranch *abranch = dynamic_cast<TBranch *>(allbranches->At(i));
		if(abranch!= nullptr){
			//cout<<abranch->GetName()<<endl;
			_originalvars.push_back(abranch->GetName());
		}
	}
}

//Checking HLTs in the input root file
std::string NanoAODAnalyzerrdframe::setHLT_muons(std::string str_HLT){
    if(debug)
	{
    	std::cout<<"================================//================================="<<std::endl;
    	std::cout<<"Line : "<<__LINE__<<" Function : "<<__FUNCTION__<<std::endl;
    	std::cout<<"================================//================================="<<std::endl;
    }

    if(str_HLT != "")
	{
        bool ctrl_HLT =isDefined(str_HLT);
        std::string output;
        if(ctrl_HLT)
		{
            output=str_HLT;
            std::cout<<"HLT : "<<str_HLT <<" : SUCCESSFULLY FOUND!!"<<std::endl;
        }
		else
		{
            std::cout<<"HLT : "<<str_HLT <<" : CAN NOT BE FOUND "<<std::endl;
            std::cout<<"Check HLT branches in the input root file!!"<<std::endl;
            std::cout<<"EXITING PROGRAM!!"<<std::endl;
            exit(1);
        }
        return output;
    }
	else
	{ // fill the HLT names in a vector according to each year
        std::vector<string> V_output;
        if(_year=="2016preVFP" || _year=="2016postVFP")
		{
            HLTGlobalNames=HLT2016Names_muons;
        }
		else if(_year=="2017")
		{
            HLTGlobalNames=HLT2017Names_muons;
        }
		else if(_year=="2018")
		{
            HLTGlobalNames=HLT2018Names_muons;
        }

        //loop on HLTs
        for(size_t i = 0; i < HLTGlobalNames.size(); i++)
        {
            /* code */
            bool ctrl_HLT = isDefined(HLTGlobalNames[i]);
            if(ctrl_HLT)
			{
                V_output.push_back(HLTGlobalNames[i]);
            }
        }
        std::string output_HLT;
        if(!V_output.empty())
		{
            for(size_t i = 0; i < V_output.size() ; i++)
            {
                if(i!=V_output.size()-1)
				{
					output_HLT += V_output[i] + "==1 || " ;
					// // for trigger efficiency
					// output_HLT += V_output[i] + "==1 && " ;
                }
				else
				{
                    output_HLT += V_output[i] + "==1 " ;
                }
            }
        }
		else
		{
            std::cout<<" Not matched with any HLT Triggers! Please check the HLT Names in the inputfile "<<std::endl;
            std::cout<<"EXITING PROGRAM!!"<<std::endl;
            exit(1);
        }
        std::cout<<" HLT names =  "<<output_HLT <<std::endl;
        return output_HLT;
    }
}

std::string NanoAODAnalyzerrdframe::setHLT_elec(std::string str_HLT){
    if(debug)
	{
    	std::cout<<"================================//================================="<<std::endl;
    	std::cout<<"Line : "<<__LINE__<<" Function : "<<__FUNCTION__<<std::endl;
    	std::cout<<"================================//================================="<<std::endl;
    }

    if(str_HLT != "")
	{
        bool ctrl_HLT =isDefined(str_HLT);
        std::string output;
        if(ctrl_HLT)
		{
            output=str_HLT;
            std::cout<<"HLT : "<<str_HLT <<" : SUCCESSFULLY FOUND!!"<<std::endl;
        }
		else
		{
            std::cout<<"HLT : "<<str_HLT <<" : CAN NOT BE FOUND "<<std::endl;
            std::cout<<"Check HLT branches in the input root file!!"<<std::endl;
            std::cout<<"EXITING PROGRAM!!"<<std::endl;
            exit(1);
        }
        return output;
    }
	else
	{ // fill the HLT names in a vector according to each year
        std::vector<string> V_output;
        if(_year=="2016preVFP" || _year=="2016postVFP")
		{
            HLTGlobalNames=HLT2016Names_elec;
        }
		else if(_year=="2017")
		{
            HLTGlobalNames=HLT2017Names_elec;
        }
		else if(_year=="2018")
		{
            HLTGlobalNames=HLT2018Names_elec;
        }

        //loop on HLTs
        for(size_t i = 0; i < HLTGlobalNames.size(); i++)
        {
            /* code */
            bool ctrl_HLT = isDefined(HLTGlobalNames[i]);
            if(ctrl_HLT)
			{
                V_output.push_back(HLTGlobalNames[i]);
            }
        }
        std::string output_HLT;
        if(!V_output.empty())
		{
            for(size_t i = 0; i < V_output.size() ; i++)
            {
                if(i!=V_output.size()-1)
				{
                	output_HLT += V_output[i] + "==1 || " ;
					// // for trigger efficiency
					// output_HLT += V_output[i] + "==1 && " ;
                }
				else
				{
                    output_HLT += V_output[i] + "==1 " ;
                }
            }
        }
		else
		{
            std::cout<<" Not matched with any HLT Triggers! Please check the HLT Names in the inputfile "<<std::endl;
            std::cout<<"EXITING PROGRAM!!"<<std::endl;
            exit(1);
        }
        std::cout<<" HLT names =  "<<output_HLT <<std::endl;
        return output_HLT;
    }
}

//control all branch names using in the addCuts function
std::string NanoAODAnalyzerrdframe::ctrlBranchName(std::string str_Branch)
{

    if(debug)
	{
        std::cout<<"================================//================================="<<std::endl;
        std::cout<<"Line : "<<__LINE__<<" Function : "<<__FUNCTION__<<std::endl;
        std::cout<<"================================//================================="<<std::endl;
    }

    bool ctrl_Branch =isDefined(str_Branch);
    std::string output;
    if(ctrl_Branch)
	{
        output=str_Branch;
    }
	else
	{
        std::cout<<"Branch : "<<str_Branch <<" : CAN NOT BE FOUND "<<std::endl;
        std::cout<<"Check your branches in the input root file!!"<<std::endl;
        std::cout<<"EXITING PROGRAM!!"<<std::endl;
        exit(1);
    }
    return output;
}

//cut-based ID Fall17 V2 (0:fail, 1:veto, 2:loose, 3:medium, 4:tight)
std::string NanoAODAnalyzerrdframe::ElectronID(int cutbasedID)
{

    if(debug)
	{
        std::cout<<"================================//================================="<<std::endl;
        std::cout<<"Line : "<<__LINE__<<" Function : "<<__FUNCTION__<<std::endl;
        std::cout<<"================================//================================="<<std::endl;
    }

    //double Electron_eta;
    //double Electron_pt;
    if(cutbasedID==1)std::cout<<" VETO Electron ID requested == "<<cutbasedID <<std::endl;
    if(cutbasedID==2)std::cout<<" LOOSE Electron ID requested == "<<cutbasedID <<std::endl;
    if(cutbasedID==3)std::cout<<" MEDIUM Electron ID requested == "<<cutbasedID <<std::endl;
    if(cutbasedID==4)std::cout<<" TIGHT Electron ID requested == "<<cutbasedID <<std::endl;
    std::cout<<"-------------------------------------------------------------------"<<std::endl;

    if(cutbasedID<0 || cutbasedID>4)
	{
        std::cout<<"ERROR!! Wrong Electron ID requested == "<<cutbasedID<<"!! Can't be applied" <<std::endl;
        std::cout<<"Please select ElectronID from 1 to 4 " <<std::endl;
        std::cout<<"-------------------------------------------------------------------"<<std::endl;
        std::cout<<"EXITING PROGRAM!!"<<std::endl;
        exit(1);
    }
	/*if(_year=="2018" && _isUL)
	{
    	if(cutbasedID==2)
		{
        	Electron_eta=2.5;
    		Electron_pt=10;
    	}
		else if(cutbasedID==3)
		{
        	Electron_eta=2.4;
        	Electron_pt=10;
		}
	}*/

	//Rdataframe look for the variables in the intput Ttree..
	std::string output = Form("Electron_cutBased == %d ",cutbasedID);
	//std::string output = Form("Electron_cutBased == %d &&  abs(Electron_eta)<%f && Electron_pt<%f",cutbasedID,  Electron_eta, Electron_pt);
	return output;
}

std::string NanoAODAnalyzerrdframe::MuonID(int cutbasedID)
{
    
    if(debug)
	{
        std::cout<<"================================//================================="<<std::endl;
        std::cout<<"Line : "<<__LINE__<<" Function : "<<__FUNCTION__<<std::endl;
        std::cout<<"================================//================================="<<std::endl;
    }
    
    //  cut-based ID Fall17 V2 (0:fail, 1:veto, 2:loose, 3:medium, 4:tight)
    if(cutbasedID==1)std::cout<<" Veto Muon ID requested == "<<cutbasedID <<std::endl;
    if(cutbasedID==2)std::cout<<" LOOSE Muon ID requested == "<<cutbasedID <<std::endl;
    if(cutbasedID==3)std::cout<<" MEDIUM Muon ID requested == "<<cutbasedID <<std::endl;
    if(cutbasedID==4)std::cout<<" TIGHT Muon ID requested == "<<cutbasedID <<std::endl;
    std::cout<<"-------------------------------------------------------------------"<<std::endl;

    if(cutbasedID<1 || cutbasedID>4)
	{
        std::cout<<"ERROR!! Wrong Muon ID requested == "<<cutbasedID<<"!! Can't be applied" <<std::endl;
        std::cout<<"Please select Muon ID from 2 to 4 " <<std::endl;
        std::cout<<"-------------------------------------------------------------------"<<std::endl;
        std::cout<<"EXITING PROGRAM!!"<<std::endl;
        exit(1);
    }

    string Muon_cutBased_ID;
    if(cutbasedID==1)
	{
        Muon_cutBased_ID = "Muon_looseId";
        std::cout<<" VETO Muon ID requested == "<<cutbasedID <<", but it doesn't exist in the nanoAOD branches. It is moved to loose MuonID. "<<cutbasedID <<std::endl;
    }
    if(cutbasedID == 2)
	{
        Muon_cutBased_ID = "Muon_looseId";
    
    }
	else if(cutbasedID == 3)
	{
        Muon_cutBased_ID = "Muon_mediumId";

    }
	else if(cutbasedID == 4)
	{
        Muon_cutBased_ID = "Muon_tightId";
    }
    string output;
    output = Form ("%s==true",Muon_cutBased_ID.c_str());
    return output;
}

std::string NanoAODAnalyzerrdframe::JetID(int cutbasedID)
{

    if(debug)
	{
        std::cout<<"================================//================================="<<std::endl;
        std::cout<<"Line : "<<__LINE__<<" Function : "<<__FUNCTION__<<std::endl;
        std::cout<<"================================//================================="<<std::endl;
    }

    if(cutbasedID<1 || cutbasedID>7)
	{
        std::cout<<"Error Wrong JET ID requested == "<<cutbasedID<<"!! Can't be applied" <<std::endl;
        std::cout<<"Please select number from 1 to 7 " <<std::endl;

    }else
	{
	    std::cout<<" JET ID requested == "<<cutbasedID <<std::endl;
    }

    string output;
    output = Form ("Jet_jetId==%d",cutbasedID);
    return output;
}
