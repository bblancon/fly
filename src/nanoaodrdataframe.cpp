//============================================================================
// Name        : nanoaodrdataframe.cpp
// Author      : Suyong Choi
// Version     :
// Copyright   : suyong@korea.ac.kr, Korea University, Department of Physics
// Description : Hello World in C, Ansi-style
//============================================================================

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include "NanoAODAnalyzerrdframe.h"
#include "TprimeAnalyser.h"
//#include "SkimEvents.h"
#include "TChain.h"

using namespace std;
using namespace ROOT;

int main(void) {

	TChain c1("Events");
	c1.Add("testinputdata/MC/2018/BG/TTbarBKGMC.root");
	TprimeAnalyser nanoaodrdf(&c1, "testout.root");

	string goodjsonfname = "data/Cert_294927-306462_13TeV_EOY2017ReReco_Collisions17_JSON.txt";
	string pileupfname = "data/LUM/2018_UL/puWeights.json";
	string pileuptag = "Collisions18_UltraLegacy_goldenJSON";
	string btvfname = "data/BTV/2018_UL/btagging.json";
	string btvtype = "deepJet_shape";
	string pileupjetidfname = "data/JME/2018_UL/jet_jerc.json.gz";
	string fname_btagEff = "data/BTV/2017_UL/BtaggingEfficiency.root";
	string hname_Loose_btagEff_bcflav = "h_btagEff_bcflav";
	string hname_Loose_btagEff_lflav = "h_btagEff_lflav";
	string hname_Medium_btagEff_bcflav = "h_btagEff_bcflav";
	string hname_Medium_btagEff_lflav = "h_btagEff_lflav";
	string hname_Tight_btagEff_bcflav = "h_btagEff_bcflav";
	string hname_Tight_btagEff_lflav = "h_btagEff_lflav";
	string fname_pileupjetidEff = "data/JERC/PUID_106XTraining_ULRun2_EffSFandUncties_v1.root";
	string hname_Loose_pileupjetidEff = "h2_eff_sfUL2018_L";
	string hname_Medium_pileupjetidEff = "h2_eff_sfUL2018_M";
	string hname_Tight_pileupjetidEff = "h2_eff_sfUL2018_T";
	string jercfname = "data/JERC/UL18_jerc.json";
	string jerctag = "Summer19UL18_V5_MC_L1L2L3Res_AK4PFchs";
	string jertag = "Summer19UL18_JRV2_MC";
	std::vector<std::string> jercunctag = {"Summer19UL18_V5_MC_Total_AK4PFchs"};
	string fname_metphimod = "data/JME/2018_UL/met.json.gz";
	string muon_roch_fname = "data/MUO/2017_UL/RoccoR2017UL.txt";
	string muon_fname = "data/MUO/2017_UL/muon_Z.json.gz";
	string muonloosett1lISOtype = "NUM_TightRelIso_DEN_MediumID";
	string muonHLTtype = "NUM_IsoMu27_DEN_CutBasedIdTight_and_PFIsoTight";
	string muonRECOtype = "NUM_TrackerMuons_DEN_genTracks";
	string muonIDtype = "NUM_MediumID_DEN_TrackerMuons";
	string muonISOtype = "NUM_TightRelIso_DEN_MediumID";
	string electron_fname = "data/EGM/2017_UL/electron.json.gz";
	string electron_loosett1l_id_type = "Tight";
	string electron_reco_type = "RecoAbove20";
	string electron_id_type = "Tight";
	string fname_electriggerEff = "data/EGM/2018_UL/egammaTrigEffi_wp90noiso_EGM2D_2018.root";
	string electron_hlt_eff = "EGamma_SF2D";
	string electron_hlt_stat_data = "statData";
	string electron_hlt_stat_mc = "statMC";
	string electron_hlt_syst_mc = "altTagSelection";
	string second_cut = "0";

	const std::vector<std::tuple<std::string, std::string, int, double, double, std::string>> variables_MC = {};
	const std::vector<std::tuple<std::string, std::string, int, double, double, std::string>> variables = {};
	const std::vector<std::string> weights = {};
	const std::vector<std::string> Cuts = {};
	const std::vector<std::string> Masses = {};
	const std::vector<std::string>& getQuantiles_ptleptons = {};
	const std::vector<std::string>& getQuantiles_massjets = {};
	const std::vector<std::string>& getQuantiles_ptleptons_loosett1l = {};
	const std::vector<std::string>& getQuantiles_massjets_loosett1l = {};

	nanoaodrdf.setupCorrections(goodjsonfname, pileupfname, pileuptag, btvfname, btvtype, fname_btagEff, hname_Loose_btagEff_bcflav, hname_Loose_btagEff_lflav, hname_Medium_btagEff_bcflav, hname_Medium_btagEff_lflav, hname_Tight_btagEff_bcflav, hname_Tight_btagEff_lflav, pileupjetidfname, fname_pileupjetidEff, hname_Loose_pileupjetidEff, hname_Medium_pileupjetidEff, hname_Tight_pileupjetidEff, muon_roch_fname, muon_fname, muonloosett1lISOtype, muonHLTtype, muonRECOtype, muonIDtype, muonISOtype, electron_fname, electron_loosett1l_id_type, electron_reco_type, electron_id_type, fname_electriggerEff, electron_hlt_eff, electron_hlt_stat_data, electron_hlt_stat_mc, electron_hlt_syst_mc, jercfname, jerctag, jercunctag, jertag, fname_metphimod);
	nanoaodrdf.setupObjects("2018", "none", weights);
	nanoaodrdf.setupAnalysis("2018", "none", "False", "False", "False", "False", variables_MC, variables, weights, Cuts, Masses, getQuantiles_ptleptons, getQuantiles_massjets, getQuantiles_ptleptons_loosett1l, getQuantiles_massjets_loosett1l, second_cut);
	nanoaodrdf.run(false, "outputTree", "none", "False","False");

	return EXIT_SUCCESS;
}