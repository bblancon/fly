/*
 * TprimeAnalyser.cpp
 *
 * Created on: May 6, 2022
 *      Author: suyong
 *      Developper: cdozen
 */

#include "TprimeAnalyser.h"
#include "utility.h"
#include "Math/GenVector/VectorUtil.h"
#include <ROOT/RDataFrame.hxx>
#include <ROOT/TProcessExecutor.hxx>
#include <TStopwatch.h>
#include "ROOT/RVec.hxx"
#include "Math/Vector4D.h"
#include "TTree.h"
#include "TFile.h"
#include "TMath.h"
#include <iostream>
#include "correction.h"
#include "TCanvas.h"
// #include "Mt2/Basic_Mt2_332_Calculator.h"
// #include "Mt2/Mt2Units.h"
// #include "Minuit2/MnUserParameterState.h"
// #include "Minuit2/MnMigrad.h"
// #include "Minuit2/MnSimplex.h"
// #include "Minuit2/MnScan.h"
// #include "Minuit2/FunctionMinimum.h"
#include <fstream>
using namespace std;
using namespace ROOT;
using namespace ROOT::VecOps;
using floats =  ROOT::VecOps::RVec<float>;
using ints =  ROOT::VecOps::RVec<int>;
using FourVector = ROOT::Math::PtEtaPhiMVector;
using FourVectorRVec = ROOT::VecOps::RVec<FourVector>;
using FourVectorVec = std::vector<FourVector>;
using correction::CorrectionSet;
// using ROOT::Minuit2::MnUserParameters;
// using ROOT::Minuit2::MnMigrad;
// using ROOT::Minuit2::MnSimplex;
// using ROOT::Minuit2::MnScan;
// using ROOT::Minuit2::FunctionMinimum;

TprimeAnalyser::TprimeAnalyser(TTree *t, std::string outfilename)
:NanoAODAnalyzerrdframe(t, outfilename)
{
    // initiliaze the HLT names in your analyzer class
    HLT2016Names_muons = {"HLT_IsoTkMu24"};
    HLT2016Names_elec = {"HLT_Ele27_WPTight_Gsf"};
    HLT2017Names_muons = {"HLT_IsoMu27"};
    HLT2017Names_elec = {"HLT_Ele32_WPTight_Gsf_L1DoubleEG"};
    HLT2018Names_muons = {"HLT_IsoMu24"};
    HLT2018Names_elec = {"HLT_Ele32_WPTight_Gsf"};

    // HLT2016Names_muons = {"HLT_PFMET120_PFMHT120_IDTight"};
    // HLT2016Names_elec = {"HLT_PFMET120_PFMHT120_IDTight"};
    // HLT2017Names_muons = {"HLT_PFMET140_PFMHT140_IDTight"};
    // HLT2017Names_elec = {"HLT_PFMET140_PFMHT140_IDTight"};
    // HLT2018Names_muons = {"HLT_PFMET120_PFMHT120_IDTight"};
    // HLT2018Names_elec = {"HLT_PFMET120_PFMHT120_IDTight"};

    // // HLT conditions needs to be changed in setHLT_muons and setHLT_elec in NanoAODAnalyzer
    // HLT2016Names_muons = {"HLT_PFMET120_PFMHT120_IDTight","HLT_IsoTkMu24"};
    // HLT2016Names_elec = {"HLT_PFMET120_PFMHT120_IDTight","HLT_Ele27_WPTight_Gsf"};
    // HLT2017Names_muons = {"HLT_PFMET140_PFMHT140_IDTight","HLT_IsoMu27"};
    // HLT2017Names_elec = {"HLT_PFMET140_PFMHT140_IDTight","HLT_Ele32_WPTight_Gsf_L1DoubleEG"};
    // HLT2018Names_muons = {"HLT_PFMET120_PFMHT120_IDTight","HLT_IsoMu24"};
    // HLT2018Names_elec = {"HLT_PFMET120_PFMHT120_IDTight","HLT_Ele32_WPTight_Gsf"};
}

// Define your cuts here
void TprimeAnalyser::defineCuts(std::string year, std::string process, bool BTagEff, bool Hist, bool Combine, bool JEC, const std::vector<std::string>& getQuantiles_ptleptons, const std::vector<std::string>& getQuantiles_massjets, const std::vector<std::string>& getQuantiles_ptleptons_loosett1l, const std::vector<std::string>& getQuantiles_massjets_loosett1l)
{
	if(debug)
	{
        cout<< "================================//================================="<<endl;
        cout<< "Line : "<< __LINE__<<" Function : "<<__FUNCTION__<<endl;
        cout<< "================================//================================="<<endl;
	}

	auto Nentry = _rlm.Count();
	//This is how you can express a range of the first 100 entries
	// _rlm = _rlm.Range(0, 10000);
	//auto Nentry_100 = _rlm.Count();
	// cout<< "-------------------------------------------------------------------"<<endl;
	// cout<<"Usage of ranges:\n"
	//     <<" - All entries: "<<*Nentry<<endl;
	// 	//<< " - Entries from 0 to 100: "<<*Nentry_100<<endl;
	// cout<< "-------------------------------------------------------------------"<<endl;

    string year2 = year;
    if(year == "2016preVFP" || year == "2016postVFP")
    {
        year2 = "2016";
    }

    string Filters;
    if(year2 == "2016")
    {
        if(!_isData)
        {
            Filters = "(genWeight < 400) && (genWeight > -400) && Flag_goodVertices == 1 && Flag_globalSuperTightHalo2016Filter == 1 && Flag_HBHENoiseFilter == 1 && Flag_HBHENoiseIsoFilter == 1 && Flag_EcalDeadCellTriggerPrimitiveFilter == 1 && Flag_BadPFMuonFilter == 1 && Flag_BadPFMuonDzFilter == 1 && Flag_hfNoisyHitsFilter == 1 && Flag_eeBadScFilter == 1";
        }
        if(_isData)
        {
            Filters = "Flag_goodVertices == 1 && Flag_globalSuperTightHalo2016Filter == 1 && Flag_HBHENoiseFilter == 1 && Flag_HBHENoiseIsoFilter == 1 && Flag_EcalDeadCellTriggerPrimitiveFilter == 1 && Flag_BadPFMuonFilter == 1 && Flag_BadPFMuonDzFilter == 1 && Flag_hfNoisyHitsFilter == 1 && Flag_eeBadScFilter == 1";
        }
    }
    else
    {
        if(!_isData)
        {
            Filters = "(genWeight < 400) && (genWeight > -400) && Flag_goodVertices == 1 && Flag_globalSuperTightHalo2016Filter == 1 && Flag_HBHENoiseFilter == 1 && Flag_HBHENoiseIsoFilter == 1 && Flag_EcalDeadCellTriggerPrimitiveFilter == 1 && Flag_BadPFMuonFilter == 1 && Flag_BadPFMuonDzFilter == 1 && Flag_hfNoisyHitsFilter == 1 && Flag_eeBadScFilter == 1 && Flag_ecalBadCalibFilter == 1";
        }
        if(_isData)
        {
            Filters = "Flag_goodVertices == 1 && Flag_globalSuperTightHalo2016Filter == 1 && Flag_HBHENoiseFilter == 1 && Flag_HBHENoiseIsoFilter == 1 && Flag_EcalDeadCellTriggerPrimitiveFilter == 1 && Flag_BadPFMuonFilter == 1 && Flag_BadPFMuonDzFilter == 1 && Flag_hfNoisyHitsFilter == 1 && Flag_eeBadScFilter == 1 && Flag_ecalBadCalibFilter == 1";
        }
    }

    if(BTagEff == 1)
    {
        addCuts(Form("((Selected_muon_loose_number==2 && Selected_muon_loosett1l_number==2) || (Selected_muon_loose_number==1 && Selected_muon_loosett1l_number==1) || (Selected_muon_loose_number==0 && Selected_muon_loosett1l_number==0)) && Selected_jet_loose_number >= 3 && %s && ((%s) || (%s))",Filters.c_str(),setHLT_muons().c_str(),setHLT_elec().c_str()), "0");
        addCuts("region_Jet_pt_nom == 1", "0_0");
        addCuts("((Selected_muon_loose_number==2 && Selected_muon_number==2 && Selected_electron_number==0) || (Selected_muon_loose_number==1 && Selected_muon_number==1 && Selected_electron_number==1) || (Selected_muon_loose_number==0 && Selected_muon_number==0 && Selected_electron_number==2)) && Selected_clean_jet_number >= 3 && Selected_lepton_deltaR > 1.8","0_0_0");
        addCuts("Selected_lepton_sum_two_leptons_pt > 160","0_0_0_0");
        addCuts("((Selected_muon_loose_number==2 && Selected_muon_loosett1l_number==2 && Selected_muon_number==1 && Selected_electron_loosett1l_number==0 && Selected_electron_number==0) || (Selected_muon_loose_number==1 && Selected_muon_loosett1l_number==1 && Selected_muon_number==1 && Selected_electron_loosett1l_number==1 && Selected_electron_number==0) || (Selected_muon_loose_number==1 && Selected_muon_loosett1l_number==1 && Selected_muon_number==0 && Selected_electron_loosett1l_number==0 && Selected_electron_number==1) || (Selected_muon_loose_number==0 && Selected_muon_loosett1l_number==0 && Selected_muon_number==0 && Selected_electron_loosett1l_number==1 && Selected_electron_number==1)) && (Sum(Selected_lepton_loosett1l_charge)==2 || Sum(Selected_lepton_loosett1l_charge)==-2) && Selected_clean_jet_loosett1l_number >= 3 && Selected_lepton_loosett1l_deltaR > 1.8","0_0_1");
        addCuts("Selected_lepton_loosett1l_sum_two_leptons_pt > 160","0_0_1_0");
    }
    if(BTagEff == 0)
    {
        // mumu channel
        // addCuts(Form("(%s)",setHLT_muons().c_str()), "0");
        addCuts(Form("Selected_muon_loose_number==2 && Selected_muon_loosett1l_number==2 && Selected_jet_loose_number >= 3 && %s",Filters.c_str()), "0");
        addCuts("region_Jet_pt_nom == 1", "0_0");
        // addCuts(Form("Selected_muon_loose_number==2 && Selected_muon_loosett1l_number==2 && Selected_jet_loose_number >= 3 && %s && %s",Filters.c_str(),setHLT_muons().c_str()), "0");
        if(Hist == 1)
        {
            // // separate preselection criteria
            // addCuts("(Selected_muon_loose_number==2 && Selected_muon_number==2 && Selected_electron_number==0) && (Sum(Selected_lepton_charge)==-2 || Sum(Selected_lepton_charge)==2)","0_0");
            // // addCuts("Selected_clean_jet_number >= 3","0_0_0");
            // // addCuts("Selected_clean_bjet_number >= 1","0_0_0_0");
            // // addCuts("Mass_dilepton_false == 0","0_0_0_0_0");
            // // addCuts(Form("%s",Filters.c_str()), "0_0_0_0_0_0");

            // // first cut-based selection
            // addCuts("(Selected_muon_loose_number==2 && Selected_muon_number==2 && Selected_electron_number==0) && (Sum(Selected_lepton_charge)==-2 || Sum(Selected_lepton_charge)==2) && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1","0_0_0");
            // addCuts("Selected_lepton_deltaR > 1.8","0_0_0_0");
            // addCuts("Selected_lepton_sum_two_leptons_pt > 160","0_0_0_0_0");
            // addCuts("Vectorial_sum_three_clean_jets_mass_min > 34","0_0_0_0_0_0");
            
            addCuts("(Selected_muon_loose_number==2 && Selected_muon_number==2 && Selected_electron_number==0) && (Sum(Selected_lepton_charge)==-2 || Sum(Selected_lepton_charge)==2) && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1 && Selected_lepton_deltaR > 1.8","0_0_0");
            for(int i = 0; i < 10; i++)
            {
                addCuts(Form("Selected_lepton_sum_two_leptons_pt > %s",getQuantiles_ptleptons[i].c_str()),"0_0_0_"+std::to_string(i));
                addCuts(Form("Vectorial_sum_three_clean_jets_mass_min > %s",getQuantiles_massjets[i].c_str()),"0_0_0_"+std::to_string(i)+"_0");
                addCuts(Form("Vectorial_sum_three_clean_jets_mass_min < %s",getQuantiles_massjets[i].c_str()),"0_0_0_"+std::to_string(i)+"_1");
            }
            if(Combine == 0)
            {
                addCuts("(Selected_muon_loose_number==2 && Selected_muon_number==2 && Selected_electron_number==0) && Sum(Selected_lepton_charge)==0 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1 && Selected_lepton_deltaR > 1.8","0_0_1");
                for(int i = 0; i < 10; i++)
                {
                    addCuts(Form("Selected_lepton_sum_two_leptons_pt > %s",getQuantiles_ptleptons[i].c_str()),"0_0_1_"+std::to_string(i));
                    addCuts(Form("Vectorial_sum_three_clean_jets_mass_min > %s",getQuantiles_massjets[i].c_str()),"0_0_1_"+std::to_string(i)+"_0");
                }
                addCuts("(Selected_muon_loose_number==2 && Selected_muon_loosett1l_number==2 && Selected_muon_number==1 && Selected_electron_loosett1l_number==0 && Selected_electron_number==0) && (Sum(Selected_lepton_loosett1l_charge)==2 || Sum(Selected_lepton_loosett1l_charge)==-2) && Selected_clean_jet_loosett1l_number >= 3 && Selected_clean_bjet_loosett1l_number >= 1 && Selected_lepton_loosett1l_deltaR > 1.8","0_0_2");
                for(int i = 0; i < 10; i++)
                {
                    addCuts(Form("Selected_lepton_loosett1l_sum_two_leptons_pt > %s",getQuantiles_ptleptons_loosett1l[i].c_str()),"0_0_2_"+std::to_string(i));
                    addCuts(Form("Vectorial_sum_three_clean_jets_loosett1l_mass_min > %s",getQuantiles_massjets_loosett1l[i].c_str()),"0_0_2_"+std::to_string(i)+"_0");
                    addCuts(Form("Vectorial_sum_three_clean_jets_loosett1l_mass_min < %s",getQuantiles_massjets_loosett1l[i].c_str()),"0_0_2_"+std::to_string(i)+"_1");
                }
            }
            if(!_isData && process.find("signal") == std::string::npos)
            {
                for(int i = 0; i < 10; i++)
                {
                    addCuts("lepton_different_charge == 0 && lepton_non_prompt == 0", "0_0_0_"+std::to_string(i)+"_0_0");
                    addCuts("lepton_different_charge == 1", "0_0_0_"+std::to_string(i)+"_0_1");
                    addCuts("lepton_different_charge == 0 && lepton_non_prompt == 1", "0_0_0_"+std::to_string(i)+"_0_2");
                    addCuts("lepton_different_charge == 0 && lepton_non_prompt == 0", "0_0_0_"+std::to_string(i)+"_1_0");
                    addCuts("lepton_different_charge == 1", "0_0_0_"+std::to_string(i)+"_1_1");
                    addCuts("lepton_different_charge == 0 && lepton_non_prompt == 1", "0_0_0_"+std::to_string(i)+"_1_2");
                    if(Combine == 0)
                    {
                        addCuts("lepton_different_charge == 0 && lepton_non_prompt == 0", "0_0_1_"+std::to_string(i)+"_0_0");
                        addCuts("lepton_different_charge == 1", "0_0_1_"+std::to_string(i)+"_0_1");
                        addCuts("lepton_different_charge == 0 && lepton_non_prompt == 1", "0_0_1_"+std::to_string(i)+"_0_2");
                        addCuts("lepton_different_charge_loosett1l == 0 && lepton_non_prompt_loosett1l == 0", "0_0_2_"+std::to_string(i)+"_0_0");
                        addCuts("lepton_different_charge_loosett1l == 1", "0_0_2_"+std::to_string(i)+"_0_1");
                        addCuts("lepton_different_charge_loosett1l == 0 && lepton_non_prompt_loosett1l == 1", "0_0_2_"+std::to_string(i)+"_0_2");
                        addCuts("lepton_different_charge_loosett1l == 0 && lepton_non_prompt_loosett1l == 0", "0_0_2_"+std::to_string(i)+"_1_0");
                        addCuts("lepton_different_charge_loosett1l == 1", "0_0_2_"+std::to_string(i)+"_1_1");
                        addCuts("lepton_different_charge_loosett1l == 0 && lepton_non_prompt_loosett1l == 1", "0_0_2_"+std::to_string(i)+"_1_2");
                    }
                }
            }
        }

        // muel channel
        // addCuts(Form("(%s) || (%s)",setHLT_muons().c_str(),setHLT_elec().c_str()), "1");
        addCuts(Form("Selected_muon_loose_number==1 && Selected_muon_loosett1l_number==1 && Selected_jet_loose_number >= 3 && %s",Filters.c_str()), "1");
        addCuts("region_Jet_pt_nom==1", "1_0");
        // addCuts(Form("Selected_muon_loose_number==1 && Selected_muon_loosett1l_number==1 && Selected_jet_loose_number >= 3 && %s && ((%s) || (%s))",Filters.c_str(),setHLT_muons().c_str(),setHLT_elec().c_str()), "1");
        if(Hist == 1)
        {
            // // separate preselection criteria
            // addCuts("(Selected_muon_loose_number==1 && Selected_muon_number==1 && Selected_electron_number==1) && (Sum(Selected_lepton_charge)==-2 || Sum(Selected_lepton_charge)==2)","1_0");
            // // addCuts("Selected_clean_jet_number >= 3","1_0_0");
            // // addCuts("Selected_clean_bjet_number >= 1","1_0_0_0");
            // // addCuts("Mass_dilepton_false == 0","1_0_0_0_0");
            // // addCuts(Form("%s",Filters.c_str()), "1_0_0_0_0_0");

            // // first cut-based selection
            // addCuts("(Selected_muon_loose_number==1 && Selected_muon_number==1 && Selected_electron_number==1) && (Sum(Selected_lepton_charge)==-2 || Sum(Selected_lepton_charge)==2) && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1","1_0_0");
            // addCuts("Selected_lepton_deltaR > 1.8","1_0_0_0");
            // addCuts("Selected_lepton_sum_two_leptons_pt > 160","1_0_0_0_0");
            // addCuts("Vectorial_sum_three_clean_jets_mass_min > 34","1_0_0_0_0_0");

            addCuts("(Selected_muon_loose_number==1 && Selected_muon_number==1 && Selected_electron_number==1) && (Sum(Selected_lepton_charge)==-2 || Sum(Selected_lepton_charge)==2) && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1 && Selected_lepton_deltaR > 1.8","1_0_0");
            for(int i = 0; i < 10; i++)
            {
                addCuts(Form("Selected_lepton_sum_two_leptons_pt > %s",getQuantiles_ptleptons[i].c_str()),"1_0_0_"+std::to_string(i));
                addCuts(Form("Vectorial_sum_three_clean_jets_mass_min > %s",getQuantiles_massjets[i].c_str()),"1_0_0_"+std::to_string(i)+"_0");
                addCuts(Form("Vectorial_sum_three_clean_jets_mass_min < %s",getQuantiles_massjets[i].c_str()),"1_0_0_"+std::to_string(i)+"_1");
            }
            if(Combine == 0)
            {
                addCuts("(Selected_muon_loose_number==1 && Selected_muon_number==1 && Selected_electron_number==1) && Sum(Selected_lepton_charge)==0 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1 && Selected_lepton_deltaR > 1.8","1_0_1");
                for(int i = 0; i < 10; i++)
                {
                    addCuts(Form("Selected_lepton_sum_two_leptons_pt > %s",getQuantiles_ptleptons[i].c_str()),"1_0_1_"+std::to_string(i));
                    addCuts(Form("Vectorial_sum_three_clean_jets_mass_min > %s",getQuantiles_massjets[i].c_str()),"1_0_1_"+std::to_string(i)+"_0");
                }
                addCuts("((Selected_muon_loose_number==1 && Selected_muon_loosett1l_number==1 && Selected_muon_number==1 && Selected_electron_loosett1l_number==1 && Selected_electron_number==0) || (Selected_muon_loose_number==1 && Selected_muon_loosett1l_number==1 && Selected_muon_number==0 && Selected_electron_loosett1l_number==0 && Selected_electron_number==1)) && (Sum(Selected_lepton_loosett1l_charge)==2 || Sum(Selected_lepton_loosett1l_charge)==-2) && Selected_clean_jet_loosett1l_number >= 3 && Selected_clean_bjet_loosett1l_number >= 1 && Selected_lepton_loosett1l_deltaR > 1.8","1_0_2");
                for(int i = 0; i < 10; i++)
                {
                    addCuts(Form("Selected_lepton_loosett1l_sum_two_leptons_pt > %s",getQuantiles_ptleptons_loosett1l[i].c_str()),"1_0_2_"+std::to_string(i));
                    addCuts(Form("Vectorial_sum_three_clean_jets_loosett1l_mass_min > %s",getQuantiles_massjets_loosett1l[i].c_str()),"1_0_2_"+std::to_string(i)+"_0");
                    addCuts(Form("Vectorial_sum_three_clean_jets_loosett1l_mass_min < %s",getQuantiles_massjets_loosett1l[i].c_str()),"1_0_2_"+std::to_string(i)+"_1");
                }
            }
            if(!_isData && process.find("signal") == std::string::npos)
            {
                for(int i = 0; i < 10; i++)
                {
                    addCuts("lepton_different_charge == 0 && lepton_non_prompt == 0", "1_0_0_"+std::to_string(i)+"_0_0");
                    addCuts("lepton_different_charge == 1", "1_0_0_"+std::to_string(i)+"_0_1");
                    addCuts("lepton_different_charge == 0 && lepton_non_prompt == 1", "1_0_0_"+std::to_string(i)+"_0_2");
                    addCuts("lepton_different_charge == 0 && lepton_non_prompt == 0", "1_0_0_"+std::to_string(i)+"_1_0");
                    addCuts("lepton_different_charge == 1", "1_0_0_"+std::to_string(i)+"_1_1");
                    addCuts("lepton_different_charge == 0 && lepton_non_prompt == 1", "1_0_0_"+std::to_string(i)+"_1_2");
                    if(Combine == 0)
                    {
                        addCuts("lepton_different_charge == 0 && lepton_non_prompt == 0", "1_0_1_"+std::to_string(i)+"_0_0");
                        addCuts("lepton_different_charge == 1", "1_0_1_"+std::to_string(i)+"_0_1");
                        addCuts("lepton_different_charge == 0 && lepton_non_prompt == 1", "1_0_1_"+std::to_string(i)+"_0_2");
                        addCuts("lepton_different_charge_loosett1l == 0 && lepton_non_prompt_loosett1l == 0", "1_0_2_"+std::to_string(i)+"_0_0");
                        addCuts("lepton_different_charge_loosett1l == 1", "1_0_2_"+std::to_string(i)+"_0_1");
                        addCuts("lepton_different_charge_loosett1l == 0 && lepton_non_prompt_loosett1l == 1", "1_0_2_"+std::to_string(i)+"_0_2");
                        addCuts("lepton_different_charge_loosett1l == 0 && lepton_non_prompt_loosett1l == 0", "1_0_2_"+std::to_string(i)+"_1_0");
                        addCuts("lepton_different_charge_loosett1l == 1", "1_0_2_"+std::to_string(i)+"_1_1");
                        addCuts("lepton_different_charge_loosett1l == 0 && lepton_non_prompt_loosett1l == 1", "1_0_2_"+std::to_string(i)+"_1_2");
                    }
                }
            }
        }

        // elel channel
        // addCuts(Form("(%s)",setHLT_elec().c_str()), "2");
        addCuts(Form("Selected_muon_loose_number==0 && Selected_muon_loosett1l_number==0 && Selected_jet_loose_number >= 3 && %s",Filters.c_str()), "2");
        addCuts("region_Jet_pt_nom==1", "2_0");
        // addCuts(Form("Selected_muon_loose_number==0 && Selected_muon_loosett1l_number==0 && Selected_jet_loose_number >= 3 && %s && %s",Filters.c_str(),setHLT_elec().c_str()), "2");
        if(Hist == 1)
        {
            // // separate preselection criteria
            // addCuts("(Selected_muon_loose_number==0 && Selected_muon_number==0 && Selected_electron_number==2) && (Sum(Selected_lepton_charge)==-2 || Sum(Selected_lepton_charge)==2)","2_0");
            // // addCuts("Selected_clean_jet_number >= 3","2_0_0");
            // // addCuts("Selected_clean_bjet_number >= 1","2_0_0_0");
            // // addCuts("Mass_dilepton_false == 0","2_0_0_0_0");
            // // addCuts(Form("%s",Filters.c_str()), "2_0_0_0_0_0");

            // // first cut-based selection
            // addCuts("(Selected_muon_loose_number==0 && Selected_muon_number==0 && Selected_electron_number==2) && (Sum(Selected_lepton_charge)==-2 || Sum(Selected_lepton_charge)==2) && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1","2_0_0");
            // addCuts("Selected_lepton_deltaR > 1.8","2_0_0_0");
            // addCuts("Selected_lepton_sum_two_leptons_pt > 160","2_0_0_0_0");
            // addCuts("Vectorial_sum_three_clean_jets_mass_min > 34","2_0_0_0_0_0");

            addCuts("(Selected_muon_loose_number==0 && Selected_muon_number==0 && Selected_electron_number==2) && (Sum(Selected_lepton_charge)==-2 || Sum(Selected_lepton_charge)==2) && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1 && Selected_lepton_deltaR > 1.8","2_0_0");
            for(int i = 0; i < 10; i++)
            {
                addCuts(Form("Selected_lepton_sum_two_leptons_pt > %s",getQuantiles_ptleptons[i].c_str()),"2_0_0_"+std::to_string(i));
                addCuts(Form("Vectorial_sum_three_clean_jets_mass_min > %s",getQuantiles_massjets[i].c_str()),"2_0_0_"+std::to_string(i)+"_0");
                addCuts(Form("Vectorial_sum_three_clean_jets_mass_min < %s",getQuantiles_massjets[i].c_str()),"2_0_0_"+std::to_string(i)+"_1");
            }
            if(Combine == 0)
            {
                addCuts("(Selected_muon_loose_number==0 && Selected_muon_number==0 && Selected_electron_number==2) && Sum(Selected_lepton_charge)==0 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1 && Selected_lepton_deltaR > 1.8","2_0_1");
                for(int i = 0; i < 10; i++)
                {
                    addCuts(Form("Selected_lepton_sum_two_leptons_pt > %s",getQuantiles_ptleptons[i].c_str()),"2_0_1_"+std::to_string(i));
                    addCuts(Form("Vectorial_sum_three_clean_jets_mass_min > %s",getQuantiles_massjets[i].c_str()),"2_0_1_"+std::to_string(i)+"_0");
                }
                addCuts("(Selected_muon_loose_number==0 && Selected_muon_loosett1l_number==0 && Selected_muon_number==0 && Selected_electron_loosett1l_number==1 && Selected_electron_number==1) && (Sum(Selected_lepton_loosett1l_charge)==2 || Sum(Selected_lepton_loosett1l_charge)==-2) && Selected_clean_jet_loosett1l_number >= 3 && Selected_clean_bjet_loosett1l_number >= 1 && Selected_lepton_loosett1l_deltaR > 1.8","2_0_2");
                for(int i = 0; i < 10; i++)
                {
                    addCuts(Form("Selected_lepton_loosett1l_sum_two_leptons_pt > %s",getQuantiles_ptleptons_loosett1l[i].c_str()),"2_0_2_"+std::to_string(i));
                    addCuts(Form("Vectorial_sum_three_clean_jets_loosett1l_mass_min > %s",getQuantiles_massjets_loosett1l[i].c_str()),"2_0_2_"+std::to_string(i)+"_0");
                    addCuts(Form("Vectorial_sum_three_clean_jets_loosett1l_mass_min < %s",getQuantiles_massjets_loosett1l[i].c_str()),"2_0_2_"+std::to_string(i)+"_1");
                }
            }
            if(!_isData && process.find("signal") == std::string::npos)
            {
                for(int i = 0; i < 10; i++)
                {
                    addCuts("lepton_different_charge == 0 && lepton_non_prompt == 0", "2_0_0_"+std::to_string(i)+"_0_0");
                    addCuts("lepton_different_charge == 1", "2_0_0_"+std::to_string(i)+"_0_1");
                    addCuts("lepton_different_charge == 0 && lepton_non_prompt == 1", "2_0_0_"+std::to_string(i)+"_0_2");
                    addCuts("lepton_different_charge == 0 && lepton_non_prompt == 0", "2_0_0_"+std::to_string(i)+"_1_0");
                    addCuts("lepton_different_charge == 1", "2_0_0_"+std::to_string(i)+"_1_1");
                    addCuts("lepton_different_charge == 0 && lepton_non_prompt == 1", "2_0_0_"+std::to_string(i)+"_1_2");
                    if(Combine == 0)
                    {
                        addCuts("lepton_different_charge == 0 && lepton_non_prompt == 0", "2_0_1_"+std::to_string(i)+"_0_0");
                        addCuts("lepton_different_charge == 1", "2_0_1_"+std::to_string(i)+"_0_1");
                        addCuts("lepton_different_charge == 0 && lepton_non_prompt == 1", "2_0_1_"+std::to_string(i)+"_0_2");
                        addCuts("lepton_different_charge_loosett1l == 0 && lepton_non_prompt_loosett1l == 0", "2_0_2_"+std::to_string(i)+"_0_0");
                        addCuts("lepton_different_charge_loosett1l == 1", "2_0_2_"+std::to_string(i)+"_0_1");
                        addCuts("lepton_different_charge_loosett1l == 0 && lepton_non_prompt_loosett1l == 1", "2_0_2_"+std::to_string(i)+"_0_2");
                        addCuts("lepton_different_charge_loosett1l == 0 && lepton_non_prompt_loosett1l == 0", "2_0_2_"+std::to_string(i)+"_1_0");
                        addCuts("lepton_different_charge_loosett1l == 1", "2_0_2_"+std::to_string(i)+"_1_1");
                        addCuts("lepton_different_charge_loosett1l == 0 && lepton_non_prompt_loosett1l == 1", "2_0_2_"+std::to_string(i)+"_1_2");
                    }
                }
            }
        }

        // Merged channels
        // addCuts("1 == 1", "3");
        // addCuts(Form("(%s) || (%s)",setHLT_muons().c_str(),setHLT_elec().c_str()), "3");
        addCuts(Form("((Selected_muon_loose_number==2 && Selected_muon_loosett1l_number==2) || (Selected_muon_loose_number==1 && Selected_muon_loosett1l_number==1) || (Selected_muon_loose_number==0 && Selected_muon_loosett1l_number==0)) && Selected_jet_loose_number >= 3 && %s && ((%s) || (%s))",Filters.c_str(),setHLT_muons().c_str(),setHLT_elec().c_str()), "3");
        addCuts("region_Jet_pt_nom==1", "3_0");
        // addCuts("((Selected_muon_loose_number==2 && Selected_muon_number==2 && Selected_electron_number==0) || (Selected_muon_loose_number==1 && Selected_muon_number==1 && Selected_electron_number==1) || (Selected_muon_loose_number==0 && Selected_muon_number==0 && Selected_electron_number==2)) && (Sum(Selected_lepton_charge)==-2 || Sum(Selected_lepton_charge)==2) && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1 && Selected_lepton_deltaR > 1.8","3_0_0");

        if(JEC == 0 || (JEC == 1 && Hist == 1))
        {
            // // separate preselection criteria
            // addCuts("((Selected_muon_loose_number==2 && Selected_muon_number==2 && Selected_electron_number==0) || (Selected_muon_loose_number==1 && Selected_muon_number==1 && Selected_electron_number==1) || (Selected_muon_loose_number==0 && Selected_muon_number==0 && Selected_electron_number==2)) && (Sum(Selected_lepton_charge)==-2 || Sum(Selected_lepton_charge)==2)","3_0_0");
            // addCuts("Selected_clean_jet_number >= 3","3_0_0_0");
            // addCuts("Selected_clean_bjet_number >= 1","3_0_0_0_0");
            // addCuts(Form("%s",Filters.c_str()), "3_0_0_0_0_0");

            // // first cut-based selection
            // addCuts("((Selected_muon_loose_number==2 && Selected_muon_number==2 && Selected_electron_number==0) || (Selected_muon_loose_number==1 && Selected_muon_number==1 && Selected_electron_number==1) || (Selected_muon_loose_number==0 && Selected_muon_number==0 && Selected_electron_number==2)) && (Sum(Selected_lepton_charge)==-2 || Sum(Selected_lepton_charge)==2) && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1","3_0_0");
            // addCuts("Selected_lepton_deltaR > 1.8","3_0_0_0");
            // addCuts("Selected_lepton_sum_two_leptons_pt > 160","3_0_0_0_0");
            // addCuts("Vectorial_sum_three_clean_jets_mass_min > 34","3_0_0_0_0_0");

            // // separate DeltaR cut from the preselection
            // addCuts("((Selected_muon_loose_number==2 && Selected_muon_number==2 && Selected_electron_number==0) || (Selected_muon_loose_number==1 && Selected_muon_number==1 && Selected_electron_number==1) || (Selected_muon_loose_number==0 && Selected_muon_number==0 && Selected_electron_number==2)) && (Sum(Selected_lepton_charge)==-2 || Sum(Selected_lepton_charge)==2) && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1","3_0_0");
            // for(int i = 0; i < 10; i++)
            // {
            //     addCuts("Selected_lepton_deltaR > 1.8","3_0_0_"+std::to_string(i));
            //     addCuts(Form("Selected_lepton_sum_two_leptons_pt > %s",getQuantiles_ptleptons[i].c_str()),"3_0_0_"+std::to_string(i)+"_0");
            //     addCuts(Form("Vectorial_sum_three_clean_jets_mass_min > %s",getQuantiles_massjets[i].c_str()),"3_0_0_"+std::to_string(i)+"_0_0");
            // }

            addCuts("((Selected_muon_loose_number==2 && Selected_muon_number==2 && Selected_electron_number==0) || (Selected_muon_loose_number==1 && Selected_muon_number==1 && Selected_electron_number==1) || (Selected_muon_loose_number==0 && Selected_muon_number==0 && Selected_electron_number==2)) && (Sum(Selected_lepton_charge)==-2 || Sum(Selected_lepton_charge)==2) && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1 && Selected_lepton_deltaR > 1.8","3_0_0");
            for(int i = 0; i < 10; i++)
            {
                addCuts(Form("Selected_lepton_sum_two_leptons_pt > %s",getQuantiles_ptleptons[i].c_str()),"3_0_0_"+std::to_string(i));
                addCuts(Form("Vectorial_sum_three_clean_jets_mass_min > %s",getQuantiles_massjets[i].c_str()),"3_0_0_"+std::to_string(i)+"_0");
                addCuts(Form("Vectorial_sum_three_clean_jets_mass_min < %s",getQuantiles_massjets[i].c_str()),"3_0_0_"+std::to_string(i)+"_1");
            }
            if(Combine == 0)
            {
                addCuts("((Selected_muon_loose_number==2 && Selected_muon_number==2 && Selected_electron_number==0) || (Selected_muon_loose_number==1 && Selected_muon_number==1 && Selected_electron_number==1) || (Selected_muon_loose_number==0 && Selected_muon_number==0 && Selected_electron_number==2)) && Sum(Selected_lepton_charge)==0 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1 && Selected_lepton_deltaR > 1.8","3_0_1");
                for(int i = 0; i < 10; i++)
                {
                    addCuts(Form("Selected_lepton_sum_two_leptons_pt > %s",getQuantiles_ptleptons[i].c_str()),"3_0_1_"+std::to_string(i));
                    addCuts(Form("Vectorial_sum_three_clean_jets_mass_min > %s",getQuantiles_massjets[i].c_str()),"3_0_1_"+std::to_string(i)+"_0");
                }
                addCuts("((Selected_muon_loose_number==2 && Selected_muon_loosett1l_number==2 && Selected_muon_number==1 && Selected_electron_loosett1l_number==0 && Selected_electron_number==0) || (Selected_muon_loose_number==1 && Selected_muon_loosett1l_number==1 && Selected_muon_number==1 && Selected_electron_loosett1l_number==1 && Selected_electron_number==0) || (Selected_muon_loose_number==1 && Selected_muon_loosett1l_number==1 && Selected_muon_number==0 && Selected_electron_loosett1l_number==0 && Selected_electron_number==1) || (Selected_muon_loose_number==0 && Selected_muon_loosett1l_number==0 && Selected_muon_number==0 && Selected_electron_loosett1l_number==1 && Selected_electron_number==1)) && (Sum(Selected_lepton_loosett1l_charge)==2 || Sum(Selected_lepton_loosett1l_charge)==-2) && Selected_clean_jet_loosett1l_number >= 3 && Selected_clean_bjet_loosett1l_number >= 1 && Selected_lepton_loosett1l_deltaR > 1.8","3_0_2");
                for(int i = 0; i < 10; i++)
                {
                    addCuts(Form("Selected_lepton_loosett1l_sum_two_leptons_pt > %s",getQuantiles_ptleptons_loosett1l[i].c_str()),"3_0_2_"+std::to_string(i));
                    addCuts(Form("Vectorial_sum_three_clean_jets_loosett1l_mass_min > %s",getQuantiles_massjets_loosett1l[i].c_str()),"3_0_2_"+std::to_string(i)+"_0");
                    addCuts(Form("Vectorial_sum_three_clean_jets_loosett1l_mass_min < %s",getQuantiles_massjets_loosett1l[i].c_str()),"3_0_2_"+std::to_string(i)+"_1");
                }
            }
            if(!_isData && process.find("signal") == std::string::npos && Hist == 1)
            {
                for(int i = 0; i < 10; i++)
                {
                    addCuts("lepton_different_charge == 0 && lepton_non_prompt == 0", "3_0_0_"+std::to_string(i)+"_0_0");
                    addCuts("lepton_different_charge == 1", "3_0_0_"+std::to_string(i)+"_0_1");
                    addCuts("lepton_different_charge == 0 && lepton_non_prompt == 1", "3_0_0_"+std::to_string(i)+"_0_2");
                    addCuts("lepton_different_charge == 0 && lepton_non_prompt == 0", "3_0_0_"+std::to_string(i)+"_1_0");
                    addCuts("lepton_different_charge == 1", "3_0_0_"+std::to_string(i)+"_1_1");
                    addCuts("lepton_different_charge == 0 && lepton_non_prompt == 1", "3_0_0_"+std::to_string(i)+"_1_2");
                    if(Combine == 0)
                    {
                        addCuts("lepton_different_charge == 0 && lepton_non_prompt == 0", "3_0_1_"+std::to_string(i)+"_0_0");
                        addCuts("lepton_different_charge == 1", "3_0_1_"+std::to_string(i)+"_0_1");
                        addCuts("lepton_different_charge == 0 && lepton_non_prompt == 1", "3_0_1_"+std::to_string(i)+"_0_2");
                        addCuts("lepton_different_charge_loosett1l == 0 && lepton_non_prompt_loosett1l == 0", "3_0_2_"+std::to_string(i)+"_0_0");
                        addCuts("lepton_different_charge_loosett1l == 1", "3_0_2_"+std::to_string(i)+"_0_1");
                        addCuts("lepton_different_charge_loosett1l == 0 && lepton_non_prompt_loosett1l == 1", "3_0_2_"+std::to_string(i)+"_0_2");
                        addCuts("lepton_different_charge_loosett1l == 0 && lepton_non_prompt_loosett1l == 0", "3_0_2_"+std::to_string(i)+"_1_0");
                        addCuts("lepton_different_charge_loosett1l == 1", "3_0_2_"+std::to_string(i)+"_1_1");
                        addCuts("lepton_different_charge_loosett1l == 0 && lepton_non_prompt_loosett1l == 1", "3_0_2_"+std::to_string(i)+"_1_2");
                    }
                }
            }
        }

        //Jet uncertainties
        int jes_unc_idx = 1;
	    if(!_jercunctag.empty() && !_isData && Hist == 0 && JEC == 1)
        {
	        for(const auto& _unc_name : _jercunctag)
            {
	            string region_name_up = "region_jes_"+_unc_name+"_up";
    	        string region_name_down = "region_jes_"+_unc_name+"_down";

                // Merged channels
	            string jec_unc_up_reg_idx = "3_"+std::to_string(jes_unc_idx);
	            addCuts(region_name_up+" == 1", jec_unc_up_reg_idx);
                addCuts("((Selected_muon_loose_number==2 && Selected_muon_number==2 && Selected_electron_number==0) || (Selected_muon_loose_number==1 && Selected_muon_number==1 && Selected_electron_number==1) || (Selected_muon_loose_number==0 && Selected_muon_number==0 && Selected_electron_number==2)) && (Sum(Selected_lepton_charge)==-2 || Sum(Selected_lepton_charge)==2) && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1 && Selected_lepton_deltaR > 1.8",jec_unc_up_reg_idx+"_0");
                
	            string jec_unc_down_reg_idx = "3_"+std::to_string(jes_unc_idx+1);
	            addCuts(region_name_down+" == 1", jec_unc_down_reg_idx);
                addCuts("((Selected_muon_loose_number==2 && Selected_muon_number==2 && Selected_electron_number==0) || (Selected_muon_loose_number==1 && Selected_muon_number==1 && Selected_electron_number==1) || (Selected_muon_loose_number==0 && Selected_muon_number==0 && Selected_electron_number==2)) && (Sum(Selected_lepton_charge)==-2 || Sum(Selected_lepton_charge)==2) && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1 && Selected_lepton_deltaR > 1.8",jec_unc_down_reg_idx+"_0");
                jes_unc_idx +=2;
	        }
	    }
    }
}

//===============================Find Good Electrons===========================================//
//: Define Good Electrons in rdata frame
//=============================================================================================//

void TprimeAnalyser::selectElectrons(std::string year)
{
    cout<<"select good electrons"<<endl;
    if(debug)
    {
    cout<< "================================//================================="<<endl;
    cout<< "Line : "<< __LINE__<<" Function : "<<__FUNCTION__<<endl;
    cout<< "================================//================================="<<endl;
    }

    string year2 = year;
    if(year == "2016preVFP" || year == "2016postVFP")
    {
        year2 = "2016";
    }

    _rlm = _rlm.Define("goodElecIDloosett1l", ElectronID(2));
    _rlm = _rlm.Define("goodElectronsloosett1l","goodElecIDloosett1l && Electron_pt > 40 && (abs(Electron_eta) < 1.442 || (abs(Electron_eta) > 1.566 && abs(Electron_eta) < 2.5)) && Electron_miniPFRelIso_all < 0.40 && abs(Electron_dxy) < 0.05 && abs(Electron_dz) < 0.1 && Electron_sip3d < 4 && Electron_lostHits == 0 && Electron_convVeto == 1 && Electron_tightCharge == 2")
               .Define("Selected_electron_loosett1l_pt","Electron_pt[goodElectronsloosett1l]")
               .Define("Selected_electron_loosett1l_number","int(Selected_electron_loosett1l_pt.size())")
               .Define("Selected_electron_loosett1l_leading_pt","(Selected_electron_loosett1l_number > 0) ? Selected_electron_loosett1l_pt[0]:-100")
               .Define("Selected_electron_loosett1l_subleading_pt","(Selected_electron_loosett1l_number > 1) ? Selected_electron_loosett1l_pt[1]:-100")
               .Define("Selected_electron_loosett1l_sum_two_electrons_pt","(Selected_electron_loosett1l_number > 1) ? Selected_electron_loosett1l_pt[0] + Selected_electron_loosett1l_pt[1]:-100")
               .Define("Selected_electron_loosett1l_sum_all_electrons_pt","Sum(Selected_electron_loosett1l_pt)")
               .Define("Selected_electron_loosett1l_eta","Electron_eta[goodElectronsloosett1l]")
               .Define("Selected_electron_loosett1l_leading_eta","(Selected_electron_loosett1l_number > 0) ? Selected_electron_loosett1l_eta[0]:-10")
               .Define("Selected_electron_loosett1l_subleading_eta","(Selected_electron_loosett1l_number > 1) ? Selected_electron_loosett1l_eta[1]:-10")
               .Define("Selected_electron_loosett1l_phi","Electron_phi[goodElectronsloosett1l]")
               .Define("Selected_electron_loosett1l_mass","Electron_mass[goodElectronsloosett1l]")
               .Define("Selected_electron_loosett1l_transverse_energy","sqrt(Selected_electron_loosett1l_pt*Selected_electron_loosett1l_pt + Selected_electron_loosett1l_mass*Selected_electron_loosett1l_mass)")
               .Define("Selected_electron_loosett1l_charge","Electron_charge[goodElectronsloosett1l]")
               .Define("Selected_electron_loosett1l_charge_sum","(Selected_electron_loosett1l_number > 1) ? Selected_electron_loosett1l_charge[0] + Selected_electron_loosett1l_charge[1]:-100")
               .Define("Selected_electron_loosett1l_deltaeta","(Selected_electron_loosett1l_number > 1) ? abs(Selected_electron_loosett1l_eta[1] - Selected_electron_loosett1l_eta[0]):-10")
               .Define("Selected_electron_loosett1l_deltaphi","(Selected_electron_loosett1l_number > 1) ? abs(ROOT::VecOps::DeltaPhi(Selected_electron_loosett1l_phi[0],Selected_electron_loosett1l_phi[1])):-10")
               .Define("Selected_electron_loosett1l_deltaR","(Selected_electron_loosett1l_number > 1) ? ROOT::VecOps::DeltaR(Selected_electron_loosett1l_eta[0],Selected_electron_loosett1l_eta[1],Selected_electron_loosett1l_phi[0],Selected_electron_loosett1l_phi[1]):-10")
               .Define("Selected_electron_loosett1l_miniPFRelIso_all","Electron_miniPFRelIso_all[goodElectronsloosett1l]")
               .Define("Selected_electron_loosett1l_miniPFRelIso_chg","Electron_miniPFRelIso_chg[goodElectronsloosett1l]")
               .Define("Selected_electron_loosett1l_pfRelIso03_all","Electron_pfRelIso03_all[goodElectronsloosett1l]")
               .Define("Selected_electron_loosett1l_pfRelIso03_chg","Electron_pfRelIso03_chg[goodElectronsloosett1l]")
               .Define("Selected_electron_loosett1l_jetPtRelv2","Electron_jetPtRelv2[goodElectronsloosett1l]")
               .Define("Selected_electron_loosett1l_jetRelIso","Electron_jetRelIso[goodElectronsloosett1l]");
    if(!_isData)
    {
        _rlm = _rlm.Define("Selected_electron_loosett1l_genPartIdx","Electron_genPartIdx[goodElectronsloosett1l]")
                   .Define("Selected_electron_loosett1l_genPartpdgId","ROOT::VecOps::Take(GenPart_pdgId,Selected_electron_loosett1l_genPartIdx)")
                   .Define("Selected_electron_loosett1l_genPartIdxMother","ROOT::VecOps::Take(GenPart_genPartIdxMother,Selected_electron_loosett1l_genPartIdx)")
                   .Define("Selected_electron_loosett1l_genPartpdgIdMother","ROOT::VecOps::Take(GenPart_pdgId,Selected_electron_loosett1l_genPartIdxMother)")
                   .Define("Selected_electron_loosett1l_genPartFlav","Electron_genPartFlav[goodElectronsloosett1l]");
    }
    _rlm = _rlm.Define("Selected_electron_loosett1l_pdgId","Electron_pdgId[goodElectronsloosett1l]")
               .Define("Selected_electron_loosett1l_ip3d","Electron_ip3d[goodElectronsloosett1l]")
               .Define("Selected_electron_loosett1l_sip3d","Electron_sip3d[goodElectronsloosett1l]")
               .Define("Selected_electron_loosett1l_idx", ::good_idx,{"goodElectronsloosett1l"});
    
    _rlm = _rlm.Define("goodElecID", ElectronID(4))
               .Define("goodElectronstemp","goodElecID && Electron_pt > 40 && (abs(Electron_eta) < 1.442 || (abs(Electron_eta) > 1.566 && abs(Electron_eta) < 2.5)) && abs(Electron_dxy) < 0.05 && abs(Electron_dz) < 0.1 && Electron_sip3d < 4 && Electron_lostHits == 0 && Electron_convVeto == 1 && Electron_tightCharge == 2")
               .Define("Selected_electron_temp_pt","Electron_pt[goodElectronstemp]")
               .Define("Selected_electron_temp_number","int(Selected_electron_temp_pt.size())")
               .Define("Selected_electron_temp_leading_pt","(Selected_electron_temp_number > 0) ? Selected_electron_temp_pt[0]:-100")
               .Define("Selected_electron_temp_subleading_pt","(Selected_electron_temp_number > 1) ? Selected_electron_temp_pt[1]:-100")
               .Define("Selected_electron_temp_sum_two_electrons_pt","(Selected_electron_temp_number > 1) ? Selected_electron_temp_pt[0] + Selected_electron_temp_pt[1]:-100")
               .Define("Selected_electron_temp_sum_all_electrons_pt","Sum(Selected_electron_temp_pt)")
               .Define("Selected_electron_temp_eta","Electron_eta[goodElectronstemp]")
               .Define("Selected_electron_temp_leading_eta","(Selected_electron_temp_number > 0) ? Selected_electron_temp_eta[0]:-10")
               .Define("Selected_electron_temp_subleading_eta","(Selected_electron_temp_number > 1) ? Selected_electron_temp_eta[1]:-10")
               .Define("Selected_electron_temp_phi","Electron_phi[goodElectronstemp]")
               .Define("Selected_electron_temp_mass","Electron_mass[goodElectronstemp]")
               .Define("Selected_electron_temp_transverse_energy","sqrt(Selected_electron_temp_pt*Selected_electron_temp_pt + Selected_electron_temp_mass*Selected_electron_temp_mass)")
               .Define("Selected_electron_temp_charge","Electron_charge[goodElectronstemp]")
               .Define("Selected_electron_temp_charge_sum","(Selected_electron_temp_number > 1) ? Selected_electron_temp_charge[0] + Selected_electron_temp_charge[1]:-100")
               .Define("Selected_electron_temp_deltaeta","(Selected_electron_temp_number > 1) ? abs(Selected_electron_temp_eta[1] - Selected_electron_temp_eta[0]):-10")
               .Define("Selected_electron_temp_deltaphi","(Selected_electron_temp_number > 1) ? abs(ROOT::VecOps::DeltaPhi(Selected_electron_temp_phi[0],Selected_electron_temp_phi[1])):-10")
               .Define("Selected_electron_temp_deltaR","(Selected_electron_temp_number > 1) ? ROOT::VecOps::DeltaR(Selected_electron_temp_eta[0],Selected_electron_temp_eta[1],Selected_electron_temp_phi[0],Selected_electron_temp_phi[1]):-10")
               .Define("Selected_electron_temp_miniPFRelIso_all","Electron_miniPFRelIso_all[goodElectronstemp]")
               .Define("Selected_electron_temp_miniPFRelIso_chg","Electron_miniPFRelIso_chg[goodElectronstemp]")
               .Define("Selected_electron_temp_pfRelIso03_all","Electron_pfRelIso03_all[goodElectronstemp]")
               .Define("Selected_electron_temp_pfRelIso03_chg","Electron_pfRelIso03_chg[goodElectronstemp]")
               .Define("Selected_electron_temp_jetPtRelv2","Electron_jetPtRelv2[goodElectronstemp]")
               .Define("Selected_electron_temp_jetRelIso","Electron_jetRelIso[goodElectronstemp]");
    if(!_isData)
    {
        _rlm = _rlm.Define("Selected_electron_temp_genPartIdx","Electron_genPartIdx[goodElectronstemp]")
                   .Define("Selected_electron_temp_genPartpdgId","ROOT::VecOps::Take(GenPart_pdgId,Selected_electron_temp_genPartIdx)")
                   .Define("Selected_electron_temp_genPartIdxMother","ROOT::VecOps::Take(GenPart_genPartIdxMother,Selected_electron_temp_genPartIdx)")
                   .Define("Selected_electron_temp_genPartpdgIdMother","ROOT::VecOps::Take(GenPart_pdgId,Selected_electron_temp_genPartIdxMother)")
                   .Define("Selected_electron_temp_genPartFlav","Electron_genPartFlav[goodElectronstemp]");
    }
    _rlm = _rlm.Define("Selected_electron_temp_pdgId","Electron_pdgId[goodElectronstemp]")
               .Define("Selected_electron_temp_ip3d","Electron_ip3d[goodElectronstemp]")
               .Define("Selected_electron_temp_sip3d","Electron_sip3d[goodElectronstemp]")
               .Define("Selected_electron_temp_idx", ::good_idx,{"goodElectronstemp"})
               .Define("dis_comb_electron_temp",::distinct_comb_1,{"Selected_electron_temp_number"})
               .Define("elec4Rvecs_temp", ::generate_4Rvec,{"Selected_electron_temp_pt","Selected_electron_temp_eta","Selected_electron_temp_phi","Selected_electron_temp_mass"})
               .Define("Selected_electron_temp_px","ROOT::VecOps::Map(elec4Rvecs_temp, FourVecPx)")
               .Define("Selected_electron_temp_py","ROOT::VecOps::Map(elec4Rvecs_temp, FourVecPy)")
               .Define("Selected_electron_temp_pz","ROOT::VecOps::Map(elec4Rvecs_temp, FourVecPz)");

    if(year2 == "2016")
    {
        _rlm = _rlm.Define("Selected_electron_temp_isolated",::electron_isolated_2016,{"Selected_electron_temp_miniPFRelIso_all","Selected_electron_temp_jetRelIso","Selected_electron_temp_jetPtRelv2","Electron_pt","Selected_electron_temp_pt","Electron_eta","Selected_electron_temp_eta"});
    }
    else if(year2 == "2017" || year2 == "2018")
    {
        _rlm = _rlm.Define("Selected_electron_temp_isolated",::electron_isolated_2017_2018,{"Selected_electron_temp_miniPFRelIso_all","Selected_electron_temp_jetRelIso","Selected_electron_temp_jetPtRelv2","Electron_pt","Selected_electron_temp_pt","Electron_eta","Selected_electron_temp_eta"});
    }

    _rlm = _rlm.Define("goodElectrons","Selected_electron_temp_isolated == 1")
               .Define("Selected_electron_pt","Electron_pt[goodElectrons]")
               .Define("Selected_electron_number","int(Selected_electron_pt.size())")
               .Define("Selected_electron_leading_pt","(Selected_electron_number > 0) ? Selected_electron_pt[0]:-100")
               .Define("Selected_electron_subleading_pt","(Selected_electron_number > 1) ? Selected_electron_pt[1]:-100")
               .Define("Selected_electron_sum_two_electrons_pt","(Selected_electron_number > 1) ? Selected_electron_pt[0] + Selected_electron_pt[1]:-100")
               .Define("Selected_electron_sum_all_electrons_pt","Sum(Selected_electron_pt)")
               .Define("Selected_electron_eta","Electron_eta[goodElectrons]")
               .Define("Selected_electron_leading_eta","(Selected_electron_number > 0) ? Selected_electron_eta[0]:-10")
               .Define("Selected_electron_subleading_eta","(Selected_electron_number > 1) ? Selected_electron_eta[1]:-10")
               .Define("Selected_electron_phi","Electron_phi[goodElectrons]")
               .Define("Selected_electron_mass","Electron_mass[goodElectrons]")
               .Define("Selected_electron_transverse_energy","sqrt(Selected_electron_pt*Selected_electron_pt + Selected_electron_mass*Selected_electron_mass)")
               .Define("Selected_electron_charge","Electron_charge[goodElectrons]")
               .Define("Selected_electron_charge_sum","(Selected_electron_number > 1) ? Selected_electron_charge[0] + Selected_electron_charge[1]:-100")
               .Define("Selected_electron_deltaeta","(Selected_electron_number > 1) ? abs(Selected_electron_eta[1] - Selected_electron_eta[0]):-10")
               .Define("Selected_electron_deltaphi","(Selected_electron_number > 1) ? abs(ROOT::VecOps::DeltaPhi(Selected_electron_phi[0],Selected_electron_phi[1])):-10")
               .Define("Selected_electron_deltaR","(Selected_electron_number > 1) ? ROOT::VecOps::DeltaR(Selected_electron_eta[0],Selected_electron_eta[1],Selected_electron_phi[0],Selected_electron_phi[1]):-10")
               .Define("Selected_electron_miniPFRelIso_all","Electron_miniPFRelIso_all[goodElectrons]")
               .Define("Selected_electron_miniPFRelIso_chg","Electron_miniPFRelIso_chg[goodElectrons]")
               .Define("Selected_electron_pfRelIso03_all","Electron_pfRelIso03_all[goodElectrons]")
               .Define("Selected_electron_pfRelIso03_chg","Electron_pfRelIso03_chg[goodElectrons]")
               .Define("Selected_electron_jetPtRelv2","Electron_jetPtRelv2[goodElectrons]")
               .Define("Selected_electron_jetRelIso","Electron_jetRelIso[goodElectrons]");
    if(!_isData)
    {
        _rlm = _rlm.Define("Selected_electron_genPartIdx","Electron_genPartIdx[goodElectrons]")
                   .Define("Selected_electron_genPartpdgId","ROOT::VecOps::Take(GenPart_pdgId,Selected_electron_genPartIdx)")
                   .Define("Selected_electron_genPartIdxMother","ROOT::VecOps::Take(GenPart_genPartIdxMother,Selected_electron_genPartIdx)")
                   .Define("Selected_electron_genPartpdgIdMother","ROOT::VecOps::Take(GenPart_pdgId,Selected_electron_genPartIdxMother)")
                   .Define("Selected_electron_genPartFlav","Electron_genPartFlav[goodElectrons]");
    }
    _rlm = _rlm.Define("Selected_electron_pdgId","Electron_pdgId[goodElectrons]")
               .Define("Selected_electron_ip3d","Electron_ip3d[goodElectrons]")
               .Define("Selected_electron_sip3d","Electron_sip3d[goodElectrons]")
               .Define("Selected_electron_idx", ::good_idx,{"goodElectrons"});

    _rlm = _rlm.Define("elec4vecs", ::generate_4vec,{"Selected_electron_pt","Selected_electron_eta","Selected_electron_phi","Selected_electron_mass"})
               .Define("Vectorial_sum_two_electrons","(Selected_electron_number > 1) ? elec4vecs[0] + elec4vecs[1]:elec4vecs[0]")
               .Define("Vectorial_sum_two_electrons_pt","(Selected_electron_number > 1) ? Vectorial_sum_two_electrons.Pt():-100")
               .Define("Vectorial_sum_two_electrons_eta","(Selected_electron_number > 1) ? Vectorial_sum_two_electrons.Eta():-10")
               .Define("Vectorial_sum_two_electrons_phi","(Selected_electron_number > 1) ? Vectorial_sum_two_electrons.Phi():-10")
               .Define("Vectorial_sum_two_electrons_mass","(Selected_electron_number > 1) ? Vectorial_sum_two_electrons.M():-100");
}

//===============================Find Good Muons===============================================//
//: Define Good Muons in rdata frame
//=============================================================================================//
void TprimeAnalyser::selectMuons(std::string year)
{

    cout<<"select good muons"<<endl;
    if(debug)
    {
        cout<< "================================//================================="<<endl;
        cout<< "Line : "<< __LINE__<<" Function : "<<__FUNCTION__<<endl;
        cout<< "================================//================================="<<endl;
    }

    string year2 = year;
    if(year == "2016preVFP" || year == "2016postVFP")
    {
        year2 = "2016";
    }

    _rlm = _rlm.Define("goodMuonIDloose", MuonID(2));
    _rlm = _rlm.Define("goodMuonsloose","goodMuonIDloose && Muon_pt_corr > 30 && abs(Muon_eta) < 2.4 && Muon_miniPFRelIso_all < 0.40 && abs(Muon_dxy) < 0.05 && abs(Muon_dz) < 0.1")
               .Define("Selected_muon_loose_pt","Muon_pt_corr[goodMuonsloose]")
               .Define("Selected_muon_loose_eta","Muon_eta[goodMuonsloose]")
               .Define("Selected_muon_loose_phi","Muon_phi[goodMuonsloose]")
               .Define("Selected_muon_loose_mass","Muon_mass[goodMuonsloose]")
               .Define("Selected_muon_loose_number","int(Selected_muon_loose_pt.size())")
               .Define("muon4vecs_loose", ::generate_4vec,{"Selected_muon_loose_pt","Selected_muon_loose_eta","Selected_muon_loose_phi","Selected_muon_loose_mass"});

    _rlm = _rlm.Define("goodMuonIDloosett1l", MuonID(3));
    _rlm = _rlm.Define("goodMuonsloosett1l","goodMuonIDloosett1l && Muon_pt_corr > 30 && abs(Muon_eta) < 2.4 && Muon_miniPFRelIso_all < 0.40 && abs(Muon_dxy) < 0.05 && abs(Muon_dz) < 0.1 && Muon_sip3d < 4 && Muon_tightCharge == 2")
               .Define("Selected_muon_loosett1l_pt","Muon_pt_corr[goodMuonsloosett1l]")
               .Define("Selected_muon_loosett1l_number","int(Selected_muon_loosett1l_pt.size())")
               .Define("Selected_muon_loosett1l_leading_pt","(Selected_muon_loosett1l_number > 0) ? Selected_muon_loosett1l_pt[0]:-100")
               .Define("Selected_muon_loosett1l_subleading_pt","(Selected_muon_loosett1l_number > 1) ? Selected_muon_loosett1l_pt[1]:-100")
               .Define("Selected_muon_loosett1l_sum_two_muons_pt","(Selected_muon_loosett1l_number>1) ? Selected_muon_loosett1l_pt[0] + Selected_muon_loosett1l_pt[1]:-100")
               .Define("Selected_muon_loosett1l_sum_all_muons_pt","Sum(Selected_muon_loosett1l_pt)")
               .Define("Selected_muon_loosett1l_eta","Muon_eta[goodMuonsloosett1l]")
               .Define("Selected_muon_loosett1l_leading_eta","(Selected_muon_loosett1l_number > 0) ? Selected_muon_loosett1l_eta[0]:-10")
               .Define("Selected_muon_loosett1l_subleading_eta","(Selected_muon_loosett1l_number > 1) ? Selected_muon_loosett1l_eta[1]:-10")
               .Define("Selected_muon_loosett1l_phi","Muon_phi[goodMuonsloosett1l]")
               .Define("Selected_muon_loosett1l_mass","Muon_mass[goodMuonsloosett1l]")
               .Define("Selected_muon_loosett1l_transverse_energy","sqrt(Selected_muon_loosett1l_pt*Selected_muon_loosett1l_pt + Selected_muon_loosett1l_mass*Selected_muon_loosett1l_mass)")
               .Define("Selected_muon_loosett1l_charge","Muon_charge[goodMuonsloosett1l]")
               .Define("Selected_muon_loosett1l_charge_sum","(Selected_muon_loosett1l_number > 1) ? Selected_muon_loosett1l_charge[0] + Selected_muon_loosett1l_charge[1]:-100")
               .Define("Selected_muon_loosett1l_deltaeta","(Selected_muon_loosett1l_number > 1) ? abs(Selected_muon_loosett1l_eta[1] - Selected_muon_loosett1l_eta[0]):-10")
               .Define("Selected_muon_loosett1l_deltaphi","(Selected_muon_loosett1l_number > 1) ? abs(ROOT::VecOps::DeltaPhi(Selected_muon_loosett1l_phi[0],Selected_muon_loosett1l_phi[1])):-10")
               .Define("Selected_muon_loosett1l_deltaR","(Selected_muon_loosett1l_number >1) ? ROOT::VecOps::DeltaR(Selected_muon_loosett1l_eta[0],Selected_muon_loosett1l_eta[1],Selected_muon_loosett1l_phi[0],Selected_muon_loosett1l_phi[1]):-10")
               .Define("Selected_muon_loosett1l_miniPFRelIso_all","Muon_miniPFRelIso_all[goodMuonsloosett1l]")
               .Define("Selected_muon_loosett1l_miniPFRelIso_chg","Muon_miniPFRelIso_chg[goodMuonsloosett1l]")
               .Define("Selected_muon_loosett1l_pfRelIso03_all","Muon_pfRelIso03_all[goodMuonsloosett1l]")
               .Define("Selected_muon_loosett1l_pfRelIso03_chg","Muon_pfRelIso03_chg[goodMuonsloosett1l]")
               .Define("Selected_muon_loosett1l_pfRelIso04_all","Muon_pfRelIso04_all[goodMuonsloosett1l]")
               .Define("Selected_muon_loosett1l_jetPtRelv2","Muon_jetPtRelv2[goodMuonsloosett1l]")
               .Define("Selected_muon_loosett1l_jetRelIso","Muon_jetRelIso[goodMuonsloosett1l]");
    if(!_isData)
    {
        _rlm = _rlm.Define("Selected_muon_loosett1l_genPartIdx","Muon_genPartIdx[goodMuonsloosett1l]")
                   .Define("Selected_muon_loosett1l_genPartpdgId","ROOT::VecOps::Take(GenPart_pdgId,Selected_muon_loosett1l_genPartIdx)")
                   .Define("Selected_muon_loosett1l_genPartIdxMother","ROOT::VecOps::Take(GenPart_genPartIdxMother,Selected_muon_loosett1l_genPartIdx)")
                   .Define("Selected_muon_loosett1l_genPartpdgIdMother","ROOT::VecOps::Take(GenPart_pdgId,Selected_muon_loosett1l_genPartIdxMother)")
                   .Define("Selected_muon_loosett1l_genPartFlav","Muon_genPartFlav[goodMuonsloosett1l]");
    }
    _rlm = _rlm.Define("Selected_muon_loosett1l_pdgId","Muon_pdgId[goodMuonsloosett1l]")
               .Define("Selected_muon_loosett1l_ip3d","Muon_ip3d[goodMuonsloosett1l]")
               .Define("Selected_muon_loosett1l_sip3d","Muon_sip3d[goodMuonsloosett1l]")
               .Define("Selected_muon_loosett1l_idx", ::good_idx,{"goodMuonsloosett1l"});

    _rlm = _rlm.Define("goodMuonID", MuonID(3))
               .Define("goodMuonstemp","goodMuonID && Muon_pt_corr > 30 && abs(Muon_eta) < 2.4 && abs(Muon_dxy) < 0.05 && abs(Muon_dz) < 0.1 && Muon_sip3d < 4 && Muon_tightCharge == 2")
               .Define("Selected_muon_temp_pt","Muon_pt_corr[goodMuonstemp]")
               .Define("Selected_muon_temp_number","int(Selected_muon_temp_pt.size())")
               .Define("Selected_muon_temp_leading_pt","(Selected_muon_temp_number > 0) ? Selected_muon_temp_pt[0]:-100")
               .Define("Selected_muon_temp_subleading_pt","(Selected_muon_temp_number > 1) ? Selected_muon_temp_pt[1]:-100")
               .Define("Selected_muon_temp_sum_two_muons_pt","(Selected_muon_temp_number > 1) ? Selected_muon_temp_pt[0] + Selected_muon_temp_pt[1]:-100")
               .Define("Selected_muon_temp_sum_all_muons_pt","Sum(Selected_muon_temp_pt)")
               .Define("Selected_muon_temp_eta","Muon_eta[goodMuonstemp]")
               .Define("Selected_muon_temp_leading_eta","(Selected_muon_temp_number > 0) ? Selected_muon_temp_eta[0]:-10")
               .Define("Selected_muon_temp_subleading_eta","(Selected_muon_temp_number > 1) ? Selected_muon_temp_eta[1]:-10")
               .Define("Selected_muon_temp_phi","Muon_phi[goodMuonstemp]")
               .Define("Selected_muon_temp_mass","Muon_mass[goodMuonstemp]")
               .Define("Selected_muon_temp_transverse_energy","sqrt(Selected_muon_temp_pt*Selected_muon_temp_pt + Selected_muon_temp_mass*Selected_muon_temp_mass)")
               .Define("Selected_muon_temp_charge","Muon_charge[goodMuonstemp]")
               .Define("Selected_muon_temp_charge_sum","(Selected_muon_temp_number > 1) ? Selected_muon_temp_charge[0] + Selected_muon_temp_charge[1]:-100")
               .Define("Selected_muon_temp_deltaeta","(Selected_muon_temp_number > 1) ? abs(Selected_muon_temp_eta[1] - Selected_muon_temp_eta[0]):-10")
               .Define("Selected_muon_temp_deltaphi","(Selected_muon_temp_number > 1) ? abs(ROOT::VecOps::DeltaPhi(Selected_muon_temp_phi[0],Selected_muon_temp_phi[1])):-10")
               .Define("Selected_muon_temp_deltaR","(Selected_muon_temp_number > 1) ? ROOT::VecOps::DeltaR(Selected_muon_temp_eta[0],Selected_muon_temp_eta[1],Selected_muon_temp_phi[0],Selected_muon_temp_phi[1]):-10")
               .Define("Selected_muon_temp_miniPFRelIso_all","Muon_miniPFRelIso_all[goodMuonstemp]")
               .Define("Selected_muon_temp_miniPFRelIso_chg","Muon_miniPFRelIso_chg[goodMuonstemp]")
               .Define("Selected_muon_temp_pfRelIso03_all","Muon_pfRelIso03_all[goodMuonstemp]")
               .Define("Selected_muon_temp_pfRelIso03_chg","Muon_pfRelIso03_chg[goodMuonstemp]")
               .Define("Selected_muon_temp_pfRelIso04_all","Muon_pfRelIso04_all[goodMuonstemp]")
               .Define("Selected_muon_temp_jetPtRelv2","Muon_jetPtRelv2[goodMuonstemp]")
               .Define("Selected_muon_temp_jetRelIso","Muon_jetRelIso[goodMuonstemp]");
    if(!_isData)
    {
        _rlm = _rlm.Define("Selected_muon_temp_genPartIdx","Muon_genPartIdx[goodMuonstemp]")
                   .Define("Selected_muon_temp_genPartpdgId","ROOT::VecOps::Take(GenPart_pdgId,Selected_muon_temp_genPartIdx)")
                   .Define("Selected_muon_temp_genPartIdxMother","ROOT::VecOps::Take(GenPart_genPartIdxMother,Selected_muon_temp_genPartIdx)")
                   .Define("Selected_muon_temp_genPartpdgIdMother","ROOT::VecOps::Take(GenPart_pdgId,Selected_muon_temp_genPartIdxMother)")
                   .Define("Selected_muon_temp_genPartFlav","Muon_genPartFlav[goodMuonstemp]");
    }
    _rlm = _rlm.Define("Selected_muon_temp_pdgId","Muon_pdgId[goodMuonstemp]")
               .Define("Selected_muon_temp_ip3d","Muon_ip3d[goodMuonstemp]")
               .Define("Selected_muon_temp_sip3d","Muon_sip3d[goodMuonstemp]")
               .Define("Selected_muon_temp_idx", ::good_idx,{"goodMuonstemp"})
               .Define("dis_comb_muon_temp",::distinct_comb_1,{"Selected_muon_temp_number"})
               .Define("muon4Rvecs_temp", ::generate_4Rvec,{"Selected_muon_temp_pt","Selected_muon_temp_eta","Selected_muon_temp_phi","Selected_muon_temp_mass"})
               .Define("Selected_muon_temp_px","ROOT::VecOps::Map(muon4Rvecs_temp, FourVecPx)")
               .Define("Selected_muon_temp_py","ROOT::VecOps::Map(muon4Rvecs_temp, FourVecPy)")
               .Define("Selected_muon_temp_pz","ROOT::VecOps::Map(muon4Rvecs_temp, FourVecPz)");           

    if(year2 == "2016")
    {
        _rlm = _rlm.Define("Selected_muon_temp_isolated",::muon_isolated_2016,{"Selected_muon_temp_miniPFRelIso_all","Selected_muon_temp_jetRelIso","Selected_muon_temp_jetPtRelv2","Muon_pt_corr","Selected_muon_temp_pt","Muon_eta","Selected_muon_temp_eta"});
    }
    else if(year2 == "2017" || year2 == "2018")
    {
        _rlm = _rlm.Define("Selected_muon_temp_isolated",::muon_isolated_2017_2018,{"Selected_muon_temp_miniPFRelIso_all","Selected_muon_temp_jetRelIso","Selected_muon_temp_jetPtRelv2","Muon_pt_corr","Selected_muon_temp_pt","Muon_eta","Selected_muon_temp_eta"});
    }

    _rlm = _rlm.Define("goodMuons","goodMuonstemp == 1 && Selected_muon_temp_isolated == 1")
               .Define("Selected_muon_pt","Muon_pt_corr[goodMuons]")
               .Define("Selected_muon_number","int(Selected_muon_pt.size())")
               .Define("Selected_muon_leading_pt","(Selected_muon_number > 0) ? Selected_muon_pt[0]:-100")
               .Define("Selected_muon_subleading_pt","(Selected_muon_number > 1) ? Selected_muon_pt[1]:-100")
               .Define("Selected_muon_sum_two_muons_pt","(Selected_muon_number > 1) ? Selected_muon_pt[0] + Selected_muon_pt[1]:-100")
               .Define("Selected_muon_sum_all_muons_pt","Sum(Selected_muon_pt)")
               .Define("Selected_muon_eta","Muon_eta[goodMuons]")
               .Define("Selected_muon_leading_eta","(Selected_muon_number > 0) ? Selected_muon_eta[0]:-10")
               .Define("Selected_muon_subleading_eta","(Selected_muon_number > 1) ? Selected_muon_eta[1]:-10")
               .Define("Selected_muon_phi","Muon_phi[goodMuons]")
               .Define("Selected_muon_mass","Muon_mass[goodMuons]")
               .Define("Selected_muon_transverse_energy","sqrt(Selected_muon_pt*Selected_muon_pt + Selected_muon_mass*Selected_muon_mass)")
               .Define("Selected_muon_charge","Muon_charge[goodMuons]")
               .Define("Selected_muon_charge_sum","(Selected_muon_number > 1) ? Selected_muon_charge[0] + Selected_muon_charge[1]:-100")
               .Define("Selected_muon_deltaeta","(Selected_muon_number > 1) ? abs(Selected_muon_eta[1] - Selected_muon_eta[0]):-10")
               .Define("Selected_muon_deltaphi","(Selected_muon_number > 1) ? abs(ROOT::VecOps::DeltaPhi(Selected_muon_phi[0],Selected_muon_phi[1])):-10")
               .Define("Selected_muon_deltaR","(Selected_muon_number > 1) ? ROOT::VecOps::DeltaR(Selected_muon_eta[0],Selected_muon_eta[1],Selected_muon_phi[0],Selected_muon_phi[1]):-10")
               .Define("Selected_muon_miniPFRelIso_all","Muon_miniPFRelIso_all[goodMuons]")
               .Define("Selected_muon_miniPFRelIso_chg","Muon_miniPFRelIso_chg[goodMuons]")
               .Define("Selected_muon_pfRelIso03_all","Muon_pfRelIso03_all[goodMuons]")
               .Define("Selected_muon_pfRelIso03_chg","Muon_pfRelIso03_chg[goodMuons]")
               .Define("Selected_muon_pfRelIso04_all","Muon_pfRelIso04_all[goodMuons]")
               .Define("Selected_muon_jetPtRelv2","Muon_jetPtRelv2[goodMuons]")
               .Define("Selected_muon_jetRelIso","Muon_jetRelIso[goodMuons]");
    if(!_isData)
    {
        _rlm = _rlm.Define("Selected_muon_genPartIdx","Muon_genPartIdx[goodMuons]")
                   .Define("Selected_muon_genPartpdgId","ROOT::VecOps::Take(GenPart_pdgId,Selected_muon_genPartIdx)")
                   .Define("Selected_muon_genPartIdxMother","ROOT::VecOps::Take(GenPart_genPartIdxMother,Selected_muon_genPartIdx)")
                   .Define("Selected_muon_genPartpdgIdMother","ROOT::VecOps::Take(GenPart_pdgId,Selected_muon_genPartIdxMother)")
                   .Define("Selected_muon_genPartFlav","Muon_genPartFlav[goodMuons]");
    }
    _rlm = _rlm.Define("Selected_muon_pdgId","Muon_pdgId[goodMuons]")
               .Define("Selected_muon_ip3d","Muon_ip3d[goodMuons]")
               .Define("Selected_muon_sip3d","Muon_sip3d[goodMuons]")
               .Define("Selected_muon_idx", ::good_idx,{"goodMuons"});

    _rlm = _rlm.Define("muon4vecs", ::generate_4vec,{"Selected_muon_pt","Selected_muon_eta","Selected_muon_phi","Selected_muon_mass"})
               .Define("Vectorial_sum_two_muons","(Selected_muon_number > 1) ? muon4vecs[0] + muon4vecs[1]:muon4vecs[0]")
               .Define("Vectorial_sum_two_muons_pt","(Selected_muon_number > 1) ? Vectorial_sum_two_muons.Pt():-100")
               .Define("Vectorial_sum_two_muons_eta","(Selected_muon_number > 1) ? Vectorial_sum_two_muons.Eta():-10")
               .Define("Vectorial_sum_two_muons_phi","(Selected_muon_number > 1) ? Vectorial_sum_two_muons.Phi():-10")
               .Define("Vectorial_sum_two_muons_mass","(Selected_muon_number > 1) ? Vectorial_sum_two_muons.M():-100");

    _rlm = _rlm.Define("goodMuonsloosett1l_nontight","goodMuonstemp == 1 && Selected_muon_temp_isolated == 0")
               .Define("Selected_muon_loosett1l_nontight_pt","Muon_pt_corr[goodMuonsloosett1l_nontight]")
               .Define("Selected_muon_loosett1l_nontight_number","int(Selected_muon_loosett1l_nontight_pt.size())")
               .Define("Selected_muon_loosett1l_nontight_eta","Muon_eta[goodMuonsloosett1l_nontight]");
}

void TprimeAnalyser::selectLeptons()
{

    _rlm = _rlm.Define("Selected_electron_muon_loosett1l_pt","ROOT::VecOps::Concatenate(ROOT::VecOps::Concatenate(Selected_electron_loosett1l_pt,Selected_muon_loosett1l_pt),Selected_electron_pt)")
               .Define("Selected_electron_muon_loosett1l_pt_sort","ROOT::VecOps::Reverse(ROOT::VecOps::Argsort(Selected_electron_muon_loosett1l_pt))")
               .Define("Selected_lepton_loosett1l_pt","ROOT::VecOps::Take(Selected_electron_muon_loosett1l_pt, Selected_electron_muon_loosett1l_pt_sort)")
               .Define("Selected_lepton_loosett1l_number","int(Selected_lepton_loosett1l_pt.size())")
               .Define("Selected_lepton_loosett1l_leading_pt","(Selected_lepton_loosett1l_number > 0) ? Selected_lepton_loosett1l_pt[0]:-100")
               .Define("Selected_lepton_loosett1l_subleading_pt","(Selected_lepton_loosett1l_number > 1) ? Selected_lepton_loosett1l_pt[1]:-100")
               .Define("Selected_lepton_loosett1l_sum_two_leptons_pt","(Selected_lepton_loosett1l_number > 1) ? Selected_lepton_loosett1l_pt[0] + Selected_lepton_loosett1l_pt[1]:-100")
               .Define("Selected_lepton_loosett1l_sum_all_leptons_pt","Sum(Selected_lepton_loosett1l_pt)")
               .Define("Selected_lepton_loosett1l_eta","ROOT::VecOps::Take(ROOT::VecOps::Concatenate(ROOT::VecOps::Concatenate(Selected_electron_loosett1l_eta,Selected_muon_loosett1l_eta),Selected_electron_eta),Selected_electron_muon_loosett1l_pt_sort)")
               .Define("Selected_lepton_loosett1l_leading_eta","(Selected_lepton_loosett1l_number > 0) ? Selected_lepton_loosett1l_eta[0]:-10")
               .Define("Selected_lepton_loosett1l_subleading_eta","(Selected_lepton_loosett1l_number > 1) ? Selected_lepton_loosett1l_eta[1]:-10")
               .Define("Selected_lepton_loosett1l_phi","ROOT::VecOps::Take(ROOT::VecOps::Concatenate(ROOT::VecOps::Concatenate(Selected_electron_loosett1l_phi,Selected_muon_loosett1l_phi),Selected_electron_phi),Selected_electron_muon_loosett1l_pt_sort)")
               .Define("Selected_lepton_loosett1l_mass","ROOT::VecOps::Take(ROOT::VecOps::Concatenate(ROOT::VecOps::Concatenate(Selected_electron_loosett1l_mass,Selected_muon_loosett1l_mass),Selected_electron_mass),Selected_electron_muon_loosett1l_pt_sort)")
               .Define("Selected_lepton_loosett1l_transverse_energy","ROOT::VecOps::Take(ROOT::VecOps::Concatenate(ROOT::VecOps::Concatenate(Selected_electron_loosett1l_transverse_energy,Selected_muon_loosett1l_transverse_energy),Selected_electron_transverse_energy),Selected_electron_muon_loosett1l_pt_sort)")
               .Define("Selected_lepton_loosett1l_charge","ROOT::VecOps::Take(ROOT::VecOps::Concatenate(ROOT::VecOps::Concatenate(Selected_electron_loosett1l_charge,Selected_muon_loosett1l_charge),Selected_electron_charge),Selected_electron_muon_loosett1l_pt_sort)")
               .Define("Selected_lepton_loosett1l_charge_sum","(Selected_lepton_loosett1l_number > 1) ? Selected_lepton_loosett1l_charge[0] + Selected_lepton_loosett1l_charge[1]:-100")
               .Define("Selected_lepton_loosett1l_deltaeta","(Selected_lepton_loosett1l_number > 1) ? abs(Selected_lepton_loosett1l_eta[1] - Selected_lepton_loosett1l_eta[0]):-10")
               .Define("Selected_lepton_loosett1l_deltaphi","(Selected_lepton_loosett1l_number > 1) ? abs(ROOT::VecOps::DeltaPhi(Selected_lepton_loosett1l_phi[0],Selected_lepton_loosett1l_phi[1])):-10")
               .Define("Selected_lepton_loosett1l_deltaR","(Selected_lepton_loosett1l_number > 1) ? ROOT::VecOps::DeltaR(Selected_lepton_loosett1l_eta[0],Selected_lepton_loosett1l_eta[1],Selected_lepton_loosett1l_phi[0],Selected_lepton_loosett1l_phi[1]):-10")
               .Define("Selected_lepton_loosett1l_miniPFRelIso_all","ROOT::VecOps::Take(ROOT::VecOps::Concatenate(ROOT::VecOps::Concatenate(Selected_electron_loosett1l_miniPFRelIso_all,Selected_muon_loosett1l_miniPFRelIso_all),Selected_electron_miniPFRelIso_all),Selected_electron_muon_loosett1l_pt_sort)")
               .Define("Selected_lepton_loosett1l_miniPFRelIso_chg","ROOT::VecOps::Take(ROOT::VecOps::Concatenate(ROOT::VecOps::Concatenate(Selected_electron_loosett1l_miniPFRelIso_chg,Selected_muon_loosett1l_miniPFRelIso_chg),Selected_electron_miniPFRelIso_chg),Selected_electron_muon_loosett1l_pt_sort)")
               .Define("Selected_lepton_loosett1l_pfRelIso03_all","ROOT::VecOps::Take(ROOT::VecOps::Concatenate(ROOT::VecOps::Concatenate(Selected_electron_loosett1l_pfRelIso03_all,Selected_muon_loosett1l_pfRelIso03_all),Selected_electron_pfRelIso03_all),Selected_electron_muon_loosett1l_pt_sort)")
               .Define("Selected_lepton_loosett1l_pfRelIso03_chg","ROOT::VecOps::Take(ROOT::VecOps::Concatenate(ROOT::VecOps::Concatenate(Selected_electron_loosett1l_pfRelIso03_chg,Selected_muon_loosett1l_pfRelIso03_chg),Selected_electron_pfRelIso03_chg),Selected_electron_muon_loosett1l_pt_sort)")
               .Define("Selected_lepton_loosett1l_jetPtRelv2","ROOT::VecOps::Take(ROOT::VecOps::Concatenate(ROOT::VecOps::Concatenate(Selected_electron_loosett1l_jetPtRelv2,Selected_muon_loosett1l_jetPtRelv2),Selected_electron_jetPtRelv2),Selected_electron_muon_loosett1l_pt_sort)")
               .Define("Selected_lepton_loosett1l_jetRelIso","ROOT::VecOps::Take(ROOT::VecOps::Concatenate(ROOT::VecOps::Concatenate(Selected_electron_loosett1l_jetRelIso,Selected_muon_loosett1l_jetRelIso),Selected_electron_jetRelIso),Selected_electron_muon_loosett1l_pt_sort)");
    if(!_isData)
    {
        _rlm = _rlm.Define("Selected_lepton_loosett1l_genPartIdx","ROOT::VecOps::Take(ROOT::VecOps::Concatenate(ROOT::VecOps::Concatenate(Selected_electron_loosett1l_genPartIdx,Selected_muon_loosett1l_genPartIdx),Selected_electron_genPartIdx),Selected_electron_muon_loosett1l_pt_sort)")
                   .Define("Selected_lepton_loosett1l_genPartIdxMother","ROOT::VecOps::Take(GenPart_genPartIdxMother,Selected_lepton_loosett1l_genPartIdx)")
                   .Define("Selected_lepton_loosett1l_genPartpdgId","ROOT::VecOps::Take(GenPart_pdgId,Selected_lepton_loosett1l_genPartIdx)")
                   .Define("Selected_lepton_loosett1l_genPartpdgIdMother","ROOT::VecOps::Take(GenPart_pdgId,Selected_lepton_loosett1l_genPartIdxMother)")
                   .Define("Selected_lepton_loosett1l_genPartFlav","ROOT::VecOps::Take(ROOT::VecOps::Concatenate(ROOT::VecOps::Concatenate(Selected_electron_loosett1l_genPartFlav,Selected_muon_loosett1l_genPartFlav),Selected_electron_genPartFlav),Selected_electron_muon_loosett1l_pt_sort)");
    }
    _rlm = _rlm.Define("Selected_lepton_loosett1l_pdgId","ROOT::VecOps::Take(ROOT::VecOps::Concatenate(ROOT::VecOps::Concatenate(Selected_electron_loosett1l_pdgId,Selected_muon_loosett1l_pdgId),Selected_electron_pdgId),Selected_electron_muon_loosett1l_pt_sort)")
               .Define("Selected_lepton1_loosett1l_pdgId","(Selected_lepton_loosett1l_number > 1) ? abs(Selected_lepton_loosett1l_pdgId[0]):0")
               .Define("Selected_lepton2_loosett1l_pdgId","(Selected_lepton_loosett1l_number > 1) ? abs(Selected_lepton_loosett1l_pdgId[1]):0")
               .Define("Selected_lepton12_loosett1l_pdgId","(Selected_lepton_loosett1l_number > 1) ? abs(Selected_lepton_loosett1l_pdgId[0]) + abs(Selected_lepton_loosett1l_pdgId[1]):0")
               .Define("Selected_lepton_loosett1l_ip3d","ROOT::VecOps::Take(ROOT::VecOps::Concatenate(ROOT::VecOps::Concatenate(Selected_electron_loosett1l_ip3d,Selected_muon_loosett1l_ip3d),Selected_electron_ip3d),Selected_electron_muon_loosett1l_pt_sort)")
               .Define("Selected_lepton_loosett1l_sip3d","ROOT::VecOps::Take(ROOT::VecOps::Concatenate(ROOT::VecOps::Concatenate(Selected_electron_loosett1l_sip3d,Selected_muon_loosett1l_sip3d),Selected_electron_sip3d),Selected_electron_muon_loosett1l_pt_sort)")
               .Define("Selected_lepton_loosett1l_idx","ROOT::VecOps::Take(ROOT::VecOps::Concatenate(ROOT::VecOps::Concatenate(Selected_electron_loosett1l_idx,Selected_muon_loosett1l_idx),Selected_electron_idx),Selected_electron_muon_loosett1l_pt_sort)");

    _rlm = _rlm.Define("lep4vecs_loosett1l", ::generate_4vec,{"Selected_lepton_loosett1l_pt","Selected_lepton_loosett1l_eta","Selected_lepton_loosett1l_phi","Selected_lepton_loosett1l_mass"})
               .Define("lep4Rvecs_loosett1l", ::generate_4Rvec,{"Selected_lepton_loosett1l_pt","Selected_lepton_loosett1l_eta","Selected_lepton_loosett1l_phi","Selected_lepton_loosett1l_mass"})
               .Define("Vectorial_sum_two_leptons_loosett1l","(Selected_lepton_loosett1l_number > 1) ? lep4vecs_loosett1l[0] + lep4vecs_loosett1l[1]:lep4vecs_loosett1l[0]")
               .Define("Vectorial_sum_two_leptons_loosett1l_pt","(Selected_lepton_loosett1l_number > 1) ? (lep4vecs_loosett1l[0] + lep4vecs_loosett1l[1]).Pt():-100")
               .Define("Vectorial_sum_two_leptons_loosett1l_eta","(Selected_lepton_loosett1l_number > 1) ? (lep4vecs_loosett1l[0] + lep4vecs_loosett1l[1]).Eta():-10")
               .Define("Vectorial_sum_two_leptons_loosett1l_phi","(Selected_lepton_loosett1l_number > 1) ? (lep4vecs_loosett1l[0] + lep4vecs_loosett1l[1]).Phi():-10")
               .Define("Vectorial_sum_two_leptons_loosett1l_mass","(Selected_lepton_loosett1l_number > 1) ? (lep4vecs_loosett1l[0] + lep4vecs_loosett1l[1]).M():-100")
               .Define("Mass_dilepton_false_loosett1l",::false_mass_dilepton,{"lep4vecs_loosett1l"});

    _rlm = _rlm.Define("Selected_electron_muon_pt","ROOT::VecOps::Concatenate(Selected_electron_pt,Selected_muon_pt)")
               .Define("Selected_electron_muon_pt_sort","ROOT::VecOps::Reverse(ROOT::VecOps::Argsort(Selected_electron_muon_pt))")
               .Define("Selected_lepton_pt","ROOT::VecOps::Take(Selected_electron_muon_pt, Selected_electron_muon_pt_sort)")
               .Define("Selected_lepton_number","int(Selected_lepton_pt.size())")
               .Define("Selected_lepton_leading_pt","(Selected_lepton_number > 0) ? Selected_lepton_pt[0]:-100")
               .Define("Selected_lepton_subleading_pt","(Selected_lepton_number > 1) ? Selected_lepton_pt[1]:-100")
               .Define("Selected_lepton_sum_two_leptons_pt","(Selected_lepton_number > 1) ? Selected_lepton_pt[0] + Selected_lepton_pt[1]:-100")
               .Define("Selected_lepton_sum_all_leptons_pt","Sum(Selected_lepton_pt)")
               .Define("Selected_lepton_eta","ROOT::VecOps::Take(ROOT::VecOps::Concatenate(Selected_electron_eta,Selected_muon_eta),Selected_electron_muon_pt_sort)")
               .Define("Selected_lepton_leading_eta","(Selected_lepton_number > 0) ? Selected_lepton_eta[0]:-10")
               .Define("Selected_lepton_subleading_eta","(Selected_lepton_number > 1) ? Selected_lepton_eta[1]:-10")
               .Define("Selected_lepton_phi","ROOT::VecOps::Take(ROOT::VecOps::Concatenate(Selected_electron_phi,Selected_muon_phi),Selected_electron_muon_pt_sort)")
               .Define("Selected_lepton_mass","ROOT::VecOps::Take(ROOT::VecOps::Concatenate(Selected_electron_mass,Selected_muon_mass),Selected_electron_muon_pt_sort)")
               .Define("Selected_lepton_transverse_energy","ROOT::VecOps::Take(ROOT::VecOps::Concatenate(Selected_electron_transverse_energy,Selected_muon_transverse_energy),Selected_electron_muon_pt_sort)")
               .Define("Selected_lepton_charge","ROOT::VecOps::Take(ROOT::VecOps::Concatenate(Selected_electron_charge,Selected_muon_charge),Selected_electron_muon_pt_sort)")
               .Define("Selected_lepton_charge_sum","(Selected_lepton_number > 1) ? Selected_lepton_charge[0] + Selected_lepton_charge[1]:-100")
               .Define("Selected_lepton_deltaeta","(Selected_lepton_number > 1) ? abs(Selected_lepton_eta[1] - Selected_lepton_eta[0]):-10")
               .Define("Selected_lepton_deltaphi","(Selected_lepton_number > 1) ? abs(ROOT::VecOps::DeltaPhi(Selected_lepton_phi[0],Selected_lepton_phi[1])):-10")
               .Define("Selected_lepton_deltaR","(Selected_lepton_number > 1) ? ROOT::VecOps::DeltaR(Selected_lepton_eta[0],Selected_lepton_eta[1],Selected_lepton_phi[0],Selected_lepton_phi[1]):-10")
               .Define("Selected_lepton_miniPFRelIso_all","ROOT::VecOps::Take(ROOT::VecOps::Concatenate(Selected_electron_miniPFRelIso_all,Selected_muon_miniPFRelIso_all),Selected_electron_muon_pt_sort)")
               .Define("Selected_lepton_miniPFRelIso_chg","ROOT::VecOps::Take(ROOT::VecOps::Concatenate(Selected_electron_miniPFRelIso_chg,Selected_muon_miniPFRelIso_chg),Selected_electron_muon_pt_sort)")
               .Define("Selected_lepton_pfRelIso03_all","ROOT::VecOps::Take(ROOT::VecOps::Concatenate(Selected_electron_pfRelIso03_all,Selected_muon_pfRelIso03_all),Selected_electron_muon_pt_sort)")
               .Define("Selected_lepton_pfRelIso03_chg","ROOT::VecOps::Take(ROOT::VecOps::Concatenate(Selected_electron_pfRelIso03_chg,Selected_muon_pfRelIso03_chg),Selected_electron_muon_pt_sort)")
               .Define("Selected_lepton_jetPtRelv2","ROOT::VecOps::Take(ROOT::VecOps::Concatenate(Selected_electron_jetPtRelv2,Selected_muon_jetPtRelv2),Selected_electron_muon_pt_sort)")
               .Define("Selected_lepton_jetRelIso","ROOT::VecOps::Take(ROOT::VecOps::Concatenate(Selected_electron_jetRelIso,Selected_muon_jetRelIso),Selected_electron_muon_pt_sort)");
    if(!_isData)
    {
        _rlm = _rlm.Define("Selected_lepton_genPartIdx","ROOT::VecOps::Take(ROOT::VecOps::Concatenate(Selected_electron_genPartIdx,Selected_muon_genPartIdx),Selected_electron_muon_pt_sort)")
                   .Define("Selected_lepton_genPartIdxMother","ROOT::VecOps::Take(GenPart_genPartIdxMother,Selected_lepton_genPartIdx)")
                   .Define("Selected_lepton_genPartpdgId","ROOT::VecOps::Take(GenPart_pdgId,Selected_lepton_genPartIdx)")
                   .Define("Selected_lepton_genPartpdgIdMother","ROOT::VecOps::Take(GenPart_pdgId,Selected_lepton_genPartIdxMother)")
                   .Define("Selected_lepton_genPartFlav","ROOT::VecOps::Take(ROOT::VecOps::Concatenate(Selected_electron_genPartFlav,Selected_muon_genPartFlav),Selected_electron_muon_pt_sort)");
    }
    _rlm = _rlm.Define("Selected_lepton_pdgId","ROOT::VecOps::Take(ROOT::VecOps::Concatenate(Selected_electron_pdgId,Selected_muon_pdgId),Selected_electron_muon_pt_sort)")
               .Define("Selected_lepton1_pdgId","(Selected_lepton_number > 0) ? abs(Selected_lepton_pdgId[0]):-1000")
               .Define("Selected_lepton2_pdgId","(Selected_lepton_number > 1) ? abs(Selected_lepton_pdgId[1]):-1000")
               .Define("Selected_lepton12_pdgId","(Selected_lepton_number > 1) ? abs(Selected_lepton_pdgId[0]) + abs(Selected_lepton_pdgId[1]):-1000")
               .Define("Selected_lepton_ip3d","ROOT::VecOps::Take(ROOT::VecOps::Concatenate(Selected_electron_ip3d,Selected_muon_ip3d),Selected_electron_muon_pt_sort)")
               .Define("Selected_lepton_sip3d","ROOT::VecOps::Take(ROOT::VecOps::Concatenate(Selected_electron_sip3d,Selected_muon_sip3d),Selected_electron_muon_pt_sort)")
               .Define("Selected_lepton_idx","ROOT::VecOps::Take(ROOT::VecOps::Concatenate(Selected_electron_idx,Selected_muon_idx),Selected_electron_muon_pt_sort)");

    _rlm = _rlm.Define("lep4vecs", ::generate_4vec,{"Selected_lepton_pt","Selected_lepton_eta","Selected_lepton_phi","Selected_lepton_mass"})
               .Define("lep4Rvecs", ::generate_4Rvec,{"Selected_lepton_pt","Selected_lepton_eta","Selected_lepton_phi","Selected_lepton_mass"})
               .Define("Selected_lepton_energy","ROOT::VecOps::Map(lep4Rvecs, FourVecEnergy)")
               .Define("Selected_lepton_px","ROOT::VecOps::Map(lep4Rvecs, FourVecPx)")
               .Define("Selected_lepton_py","ROOT::VecOps::Map(lep4Rvecs, FourVecPy)")
               .Define("Selected_lepton_pz","ROOT::VecOps::Map(lep4Rvecs, FourVecPz)")
               .Define("Vectorial_sum_two_leptons","(Selected_lepton_number > 1) ? lep4vecs[0] + lep4vecs[1]:lep4vecs[0]")
               .Define("Vectorial_sum_two_leptons_pt","(Selected_lepton_number > 1) ? (lep4vecs[0] + lep4vecs[1]).Pt():-100")
               .Define("Vectorial_sum_two_leptons_eta","(Selected_lepton_number > 1) ? (lep4vecs[0] + lep4vecs[1]).Eta():-10")
               .Define("Vectorial_sum_two_leptons_phi","(Selected_lepton_number > 1) ? (lep4vecs[0] + lep4vecs[1]).Phi():-10")
               .Define("Vectorial_sum_two_leptons_mass","(Selected_lepton_number > 1) ? (lep4vecs[0] + lep4vecs[1]).M():-100")
               .Define("Mass_dilepton_false",::false_mass_dilepton,{"lep4vecs"});
}

void TprimeAnalyser::selectReconstructedLeptons()
{
    // _rlm = _rlm.Define("sel_leptongentopHiggs_loosett1l",::isLfromtopHiggs,{"Selected_lepton_loosett1l_pdgId","GenPart_pdgId","Selected_lepton_loosett1l_genPartIdx","GenPart_genPartIdxMother"})//, "Selected_lepton_loosett1l_pt"})
    //            .Define("LeptonFromtopHiggs_loosett1l_pt","Selected_lepton_loosett1l_pt[sel_leptongentopHiggs_loosett1l]")
    //            .Define("LeptonFromtopHiggs_loosett1l_leading_pt","LeptonFromtopHiggs_loosett1l_pt[0]")
    //            .Define("LeptonFromtopHiggs_loosett1l_subleading_pt","LeptonFromtopHiggs_loosett1l_pt[1]")
    //            .Define("LeptonFromtopHiggs_loosett1l_sum_two_muons_pt","LeptonFromtopHiggs_loosett1l_pt[0] + LeptonFromtopHiggs_loosett1l_pt[1]")
    //            .Define("LeptonFromtopHiggs_loosett1l_sum_all_muons_pt","Sum(LeptonFromtopHiggs_loosett1l_pt)")
    //            .Define("LeptonFromtopHiggs_loosett1l_eta","Selected_lepton_loosett1l_eta[sel_leptongentopHiggs_loosett1l]")
    //            .Define("LeptonFromtopHiggs_loosett1l_phi","Selected_lepton_loosett1l_phi[sel_leptongentopHiggs_loosett1l]")
    //            .Define("LeptonFromtopHiggs_loosett1l_mass","Selected_lepton_loosett1l_mass[sel_leptongentopHiggs_loosett1l]")
    //            .Define("LeptonFromtopHiggs_loosett1l_transverse_energy","sqrt(LeptonFromtopHiggs_loosett1l_pt*LeptonFromtopHiggs_loosett1l_pt + LeptonFromtopHiggs_loosett1l_mass*LeptonFromtopHiggs_loosett1l_mass)")
    //            .Define("LeptonFromtopHiggs_loosett1l_charge","Selected_lepton_loosett1l_charge[sel_leptongentopHiggs_loosett1l]")
    //            .Define("LeptonFromtopHiggs_loosett1l_charge_sum","LeptonFromtopHiggs_loosett1l_charge[0] + LeptonFromtopHiggs_loosett1l_charge[1]")
    //            .Define("LeptonFromtopHiggs_loosett1l_number","int(LeptonFromtopHiggs_loosett1l_pt.size())")
    //            .Define("LeptonFromtopHiggs_loosett1l_deltaeta","abs(LeptonFromtopHiggs_loosett1l_eta[1] - LeptonFromtopHiggs_loosett1l_eta[0])")
    //            .Define("LeptonFromtopHiggs_loosett1l_deltaphi","abs(ROOT::VecOps::DeltaPhi(LeptonFromtopHiggs_loosett1l_phi[0],LeptonFromtopHiggs_loosett1l_phi[1]))")
    //            .Define("LeptonFromtopHiggs_loosett1l_deltaR","ROOT::VecOps::DeltaR(LeptonFromtopHiggs_loosett1l_eta[0],LeptonFromtopHiggs_loosett1l_eta[1],LeptonFromtopHiggs_loosett1l_phi[0],LeptonFromtopHiggs_loosett1l_phi[1])")
    //            .Define("LeptonFromtopHiggs_loosett1l_miniPFRelIso_all","Selected_lepton_loosett1l_miniPFRelIso_all[sel_leptongentopHiggs_loosett1l]")
    //            .Define("LeptonFromtopHiggs_loosett1l_miniPFRelIso_chg","Selected_lepton_loosett1l_miniPFRelIso_chg[sel_leptongentopHiggs_loosett1l]")
    //            .Define("LeptonFromtopHiggs_loosett1l_pfRelIso03_all","Selected_lepton_loosett1l_pfRelIso03_all[sel_leptongentopHiggs_loosett1l]")
    //            .Define("LeptonFromtopHiggs_loosett1l_pfRelIso03_chg","Selected_lepton_loosett1l_pfRelIso03_chg[sel_leptongentopHiggs_loosett1l]")
    //            .Define("LeptonFromtopHiggs_loosett1l_genPartIdx","Selected_lepton_loosett1l_genPartIdx[sel_leptongentopHiggs_loosett1l]")
    //            .Define("LeptonFromtopHiggs_loosett1l_genPartFlav","Selected_lepton_loosett1l_genPartFlav[sel_leptongentopHiggs_loosett1l]")
    //            .Define("LeptonFromtopHiggs_loosett1l_pdgId","Selected_lepton_loosett1l_pdgId[sel_leptongentopHiggs_loosett1l]")
    //            .Define("p4_LeptonFromtopHiggs_loosett1l",::generate_4vec,{"LeptonFromtopHiggs_loosett1l_pt","LeptonFromtopHiggs_loosett1l_eta","LeptonFromtopHiggs_loosett1l_phi","LeptonFromtopHiggs_loosett1l_mass"});

    // _rlm = _rlm.Define("sel_leptongentoporHiggs_loosett1l",::isLfromtoporHiggs,{"LeptonFromtopHiggs_pdgId","GenPart_pdgId","LeptonFromtopHiggs_genPartIdx","GenPart_genPartIdxMother","p4_LeptonFromtopHiggs"})
    //            .Define("LeptonFromtop4vecs_loosett1l","sel_leptongentoporHiggs_loosett1l[0]")
    //            .Define("LeptonFromtop_loosett1l_pt","LeptonFromtop4vecs_loosett1l.Pt()")
    //            .Define("LeptonFromtop_loosett1l_eta","LeptonFromtop4vecs_loosett1l.Eta()")
    //            .Define("LeptonFromtop_loosett1l_phi","LeptonFromtop4vecs_loosett1l.Phi()")
    //            .Define("LeptonFromtop_loosett1l_mass","LeptonFromtop4vecs_loosett1l.M()")
    //            .Define("LeptonFromtop_loosett1l_transverse_energy","sqrt(LeptonFromtop_loosett1l_pt*LeptonFromtop_loosett1l_pt + LeptonFromtop_loosett1l_mass*LeptonFromtop_loosett1l_mass)")
    //            .Define("LeptonFromtop_loosett1l_energy","LeptonFromtop4vecs_loosett1l.E()")
    //            .Define("LeptonFromtop_loosett1l_px","LeptonFromtop4vecs_loosett1l.Px()")
    //            .Define("LeptonFromtop_loosett1l_py","LeptonFromtop4vecs_loosett1l.Py()")
    //            .Define("LeptonFromtop_loosett1l_pz","LeptonFromtop4vecs_loosett1l.Pz()")
    //            .Define("LeptonFromHiggs4vecs_loosett1l","sel_leptongentoporHiggs_loosett1l[1]")
    //            .Define("LeptonFromHiggs_loosett1l_pt","LeptonFromHiggs4vecs_loosett1l.Pt()")
    //            .Define("LeptonFromHiggs_loosett1l_eta","LeptonFromHiggs4vecs_loosett1l.Eta()")
    //            .Define("LeptonFromHiggs_loosett1l_phi","LeptonFromHiggs4vecs_loosett1l.Phi()")
    //            .Define("LeptonFromHiggs_loosett1l_mass","LeptonFromHiggs4vecs_loosett1l.M()")
    //            .Define("LeptonFromHiggs_loosett1l_transverse_energy","sqrt(LeptonFromHiggs_loosett1l_pt*LeptonFromHiggs_loosett1l_pt + LeptonFromHiggs_loosett1l_mass*LeptonFromHiggs_loosett1l_mass)")
    //            .Define("LeptonFromHiggs_loosett1l_energy","LeptonFromHiggs4vecs_loosett1l.E()")
    //            .Define("LeptonFromHiggs_loosett1l_px","LeptonFromHiggs4vecs_loosett1l.Px()")
    //            .Define("LeptonFromHiggs_loosett1l_py","LeptonFromHiggs4vecs_loosett1l.Py()")
    //            .Define("LeptonFromHiggs_loosett1l_pz","LeptonFromHiggs4vecs_loosett1l.Pz()")
    //            .Define("LeptonFromtopHiggs_loosett1l_ordered_pt",::concatenate,{"LeptonFromtop_loosett1l_pt","LeptonFromHiggs_loosett1l_pt"})
    //            .Define("LeptonFromtopHiggs_loosett1l_ordered_px",::concatenate,{"LeptonFromtop_loosett1l_px","LeptonFromHiggs_loosett1l_px"})
    //            .Define("LeptonFromtopHiggs_loosett1l_ordered_py",::concatenate,{"LeptonFromtop_loosett1l_py","LeptonFromHiggs_loosett1l_py"})
    //            .Define("LeptonFromtopHiggs_loosett1l_ordered_pz",::concatenate,{"LeptonFromtop_loosett1l_pz","LeptonFromHiggs_loosett1l_pz"})
    //            .Define("LeptonFromtopHiggs_loosett1l_ordered_energy",::concatenate,{"LeptonFromtop_loosett1l_energy","LeptonFromHiggs_loosett1l_energy"})
    //            .Define("LeptonFromtopHiggs_loosett1l_ordered_mass",::concatenate,{"LeptonFromtop_loosett1l_mass","LeptonFromHiggs_loosett1l_mass"})
    //            .Define("LeptonFromtop_LeptonFromHiggs_loosett1l","LeptonFromtop4vecs_loosett1l + LeptonFromHiggs4vecs_loosett1l")
    //            .Define("LeptonFromtop_LeptonFromHiggs_loosett1l_pt","LeptonFromtop_LeptonFromHiggs_loosett1l.Pt()")
    //            .Define("LeptonFromtop_LeptonFromHiggs_loosett1l_eta","LeptonFromtop_LeptonFromHiggs_loosett1l.Eta()")
    //            .Define("LeptonFromtop_LeptonFromHiggs_loosett1l_phi","LeptonFromtop_LeptonFromHiggs_loosett1l.Phi()")
    //            .Define("LeptonFromtop_LeptonFromHiggs_loosett1l_mass","LeptonFromtop_LeptonFromHiggs_loosett1l.M()")
    //            .Define("LeptonFromtop_LeptonFromHiggs_loosett1l_transverse_energy","sqrt(LeptonFromtop_LeptonFromHiggs_loosett1l_pt*LeptonFromtop_LeptonFromHiggs_loosett1l_pt + LeptonFromtop_LeptonFromHiggs_loosett1l_mass*LeptonFromtop_LeptonFromHiggs_loosett1l_mass)")
    //            .Define("LeptonFromtop_LeptonFromHiggs_loosett1l_deltaeta","abs(LeptonFromtop_loosett1l_eta - LeptonFromHiggs_loosett1l_eta)")
    //            .Define("LeptonFromtop_LeptonFromHiggs_loosett1l_deltaphi","abs(ROOT::VecOps::DeltaPhi(LeptonFromtop_loosett1l_phi,LeptonFromHiggs_loosett1l_phi))")
    //            .Define("LeptonFromtop_LeptonFromHiggs_loosett1l_deltaR","ROOT::VecOps::DeltaR(LeptonFromtop_loosett1l_eta,LeptonFromHiggs_loosett1l_eta,LeptonFromtop_loosett1l_phi,LeptonFromHiggs_loosett1l_phi)");

    // _rlm = _rlm.Define("sel_leptongenW_loosett1l",::isLfromW,{"Selected_lepton_loosett1l_pdgId","GenPart_pdgId","Selected_lepton_loosett1l_genPartIdx","GenPart_genPartIdxMother"})
    //            .Define("LeptonFromW_loosett1l_pt","Selected_lepton_loosett1l_pt[sel_leptongenW_loosett1l]")
    //            .Define("LeptonFromW_loosett1l_number","int(LeptonFromW_loosett1l_pt.size())")
    //            .Define("LeptonFromW_loosett1l_leading_pt","(Selected_lepton_loosett1l_number > 0) ? LeptonFromW_loosett1l_pt[0]:-100")
    //            .Define("LeptonFromW_loosett1l_subleading_pt","(Selected_lepton_loosett1l_number > 1) ? LeptonFromW_loosett1l_pt[1]:-100")
    //            .Define("LeptonFromW_loosett1l_sum_two_muons_pt","(Selected_lepton_loosett1l_number > 1) ? LeptonFromW_loosett1l_pt[0] + LeptonFromW_loosett1l_pt[1]:-100")
    //            .Define("LeptonFromW_loosett1l_sum_all_muons_pt","Sum(LeptonFromW_loosett1l_pt)")
    //            .Define("LeptonFromW_loosett1l_eta","Selected_lepton_loosett1l_eta[sel_leptongenW_loosett1l]")
    //            .Define("LeptonFromW_loosett1l_phi","Selected_lepton_loosett1l_phi[sel_leptongenW_loosett1l]")
    //            .Define("LeptonFromW_loosett1l_mass","Selected_lepton_loosett1l_mass[sel_leptongenW_loosett1l]")
    //            .Define("LeptonFromW_loosett1l_charge","Selected_lepton_loosett1l_charge[sel_leptongenW_loosett1l]")
    //            .Define("LeptonFromW_loosett1l_charge_sum","(Selected_lepton_loosett1l_number > 1) ? LeptonFromW_loosett1l_charge[0] + LeptonFromW_loosett1l_charge[1]:-100")
    //            .Define("LeptonFromW_loosett1l_deltaeta","(Selected_lepton_loosett1l_number > 1) ? abs(LeptonFromW_loosett1l_eta[1] - LeptonFromW_loosett1l_eta[0]):-100")
    //            .Define("LeptonFromW_loosett1l_deltaphi","(Selected_lepton_loosett1l_number > 1) ? abs(ROOT::VecOps::DeltaPhi(LeptonFromW_loosett1l_phi[0],LeptonFromW_loosett1l_phi[1])):-100")
    //            .Define("LeptonFromW_loosett1l_deltaR","(Selected_lepton_loosett1l_number > 1) ? ROOT::VecOps::DeltaR(LeptonFromW_loosett1l_eta[0],LeptonFromW_loosett1l_eta[1],LeptonFromW_loosett1l_phi[0],LeptonFromW_loosett1l_phi[1]):-100")
    //            .Define("LeptonFromW_loosett1l_genPartIdx","Selected_lepton_loosett1l_genPartIdx[sel_leptongenW_loosett1l]")
    //            .Define("LeptonFromW_loosett1l_genPartFlav","Selected_lepton_loosett1l_genPartFlav[sel_leptongenW_loosett1l]")
    //            .Define("LeptonFromW_loosett1l_pdgId","Selected_lepton_loosett1l_pdgId[sel_leptongenW_loosett1l]")
    //            .Define("p4_LeptonFromW_loosett1l",::generate_4vec,{"LeptonFromW_loosett1l_pt","LeptonFromW_loosett1l_eta","LeptonFromW_loosett1l_phi","LeptonFromW_loosett1l_mass"});

    // _rlm = _rlm.Define("sel_muongenW_loosett1l",::isLfromW,{"Selected_muon_loosett1l_pdgId","GenPart_pdgId","Selected_muon_loosett1l_genPartIdx","GenPart_genPartIdxMother"})
    //            .Define("MuonFromW_loosett1l_pt","Selected_muon_loosett1l_pt[sel_muongenW]")
    //            .Define("MuonFromW_loosett1l_charge","Selected_muon_loosett1l_charge[sel_muongenW]")
    //            .Define("MuonFromW_loosett1l_number","int(MuonFromW_pt.size())");

    // _rlm = _rlm.Define("sel_electrongenW_loosett1l",::isLfromW,{"Selected_electron_loosett1l_pdgId","GenPart_pdgId","Selected_electron_loosett1l_genPartIdx","GenPart_genPartIdxMother"})
    //            .Define("ElectronFromW_loosett1l_pt","Selected_electron_loosett1l_pt[sel_electrongenW_loosett1l]")
    //            .Define("ElectronFromW_loosett1l_charge","Selected_electron_loosett1l_charge[sel_electrongenW_loosett1l]")
    //            .Define("ElectronFromW_loosett1l_number","int(ElectronFromW_loosett1l_pt.size())");

    _rlm = _rlm.Define("sel_leptongenMother_loosett1l",::MotherfromL,{"Selected_lepton_loosett1l_pdgId","GenPart_pdgId","Selected_lepton_loosett1l_genPartIdx","GenPart_genPartIdxMother"})
               .Define("MotherfromLepton_loosett1l_pdgId","sel_leptongenMother_loosett1l")
               .Define("MotherfromLepton1_loosett1l_pdgId","abs(sel_leptongenMother_loosett1l[0])")     
               .Define("MotherfromLepton2_loosett1l_pdgId","abs(sel_leptongenMother_loosett1l[1])")
               .Define("MotherfromLepton12_loosett1l_pdgId","abs(sel_leptongenMother_loosett1l[0]) + abs(sel_leptongenMother_loosett1l[1])");

    _rlm = _rlm.Define("lepton_different_charge_loosett1l",::isLdifferentcharge,{"Selected_lepton_loosett1l_pdgId","GenPart_pdgId","Selected_lepton_loosett1l_genPartIdx"})
               .Define("lepton_non_prompt_loosett1l",::isLnonprompt,{"Selected_lepton_loosett1l_genPartFlav"});
    
    // _rlm = _rlm.Define("sel_leptongentopHiggs",::isLfromtopHiggs,{"Selected_lepton_pdgId","GenPart_pdgId","Selected_lepton_genPartIdx","GenPart_genPartIdxMother"})//, "Selected_lepton_pt"})
    //            .Define("LeptonFromtopHiggs_pt","Selected_lepton_pt[sel_leptongentopHiggs]")
    //            .Define("LeptonFromtopHiggs_number","int(LeptonFromtopHiggs_pt.size())");
    //            .Define("LeptonFromtopHiggs_leading_pt","LeptonFromtopHiggs_pt[0]")
    //            .Define("LeptonFromtopHiggs_subleading_pt","LeptonFromtopHiggs_pt[1]")
    //            .Define("LeptonFromtopHiggs_sum_two_muons_pt","LeptonFromtopHiggs_pt[0] + LeptonFromtopHiggs_pt[1]")
    //            .Define("LeptonFromtopHiggs_sum_all_muons_pt","Sum(LeptonFromtopHiggs_pt)")
    //            .Define("LeptonFromtopHiggs_eta","Selected_lepton_eta[sel_leptongentopHiggs]")
    //            .Define("LeptonFromtopHiggs_phi","Selected_lepton_phi[sel_leptongentopHiggs]")
    //            .Define("LeptonFromtopHiggs_mass","Selected_lepton_mass[sel_leptongentopHiggs]")
    //            .Define("LeptonFromtopHiggs_transverse_energy","sqrt(LeptonFromtopHiggs_pt*LeptonFromtopHiggs_pt + LeptonFromtopHiggs_mass*LeptonFromtopHiggs_mass)")
    //            .Define("LeptonFromtopHiggs_charge","Selected_lepton_charge[sel_leptongentopHiggs]")
    //            .Define("LeptonFromtopHiggs_charge_sum","LeptonFromtopHiggs_charge[0] + LeptonFromtopHiggs_charge[1]")
    //            .Define("LeptonFromtopHiggs_deltaeta","abs(LeptonFromtopHiggs_eta[1] - LeptonFromtopHiggs_eta[0])")
    //            .Define("LeptonFromtopHiggs_deltaphi","abs(ROOT::VecOps::DeltaPhi(LeptonFromtopHiggs_phi[0],LeptonFromtopHiggs_phi[1]))")
    //            .Define("LeptonFromtopHiggs_deltaR","ROOT::VecOps::DeltaR(LeptonFromtopHiggs_eta[0],LeptonFromtopHiggs_eta[1],LeptonFromtopHiggs_phi[0],LeptonFromtopHiggs_phi[1])")
    //            .Define("LeptonFromtopHiggs_miniPFRelIso_all","Selected_lepton_miniPFRelIso_all[sel_leptongentopHiggs]")
    //            .Define("LeptonFromtopHiggs_miniPFRelIso_chg","Selected_lepton_miniPFRelIso_chg[sel_leptongentopHiggs]")
    //            .Define("LeptonFromtopHiggs_pfRelIso03_all","Selected_lepton_pfRelIso03_all[sel_leptongentopHiggs]")
    //            .Define("LeptonFromtopHiggs_pfRelIso03_chg","Selected_lepton_pfRelIso03_chg[sel_leptongentopHiggs]")
    //            .Define("LeptonFromtopHiggs_genPartIdx","Selected_lepton_genPartIdx[sel_leptongentopHiggs]")
    //            .Define("LeptonFromtopHiggs_genPartFlav","Selected_lepton_genPartFlav[sel_leptongentopHiggs]")
    //            .Define("LeptonFromtopHiggs_pdgId","Selected_lepton_pdgId[sel_leptongentopHiggs]")
    //            .Define("p4_LeptonFromtopHiggs",::generate_4vec,{"LeptonFromtopHiggs_pt","LeptonFromtopHiggs_eta","LeptonFromtopHiggs_phi","LeptonFromtopHiggs_mass"});

    // _rlm = _rlm.Define("sel_leptongentoporHiggs",::isLfromtoporHiggs,{"LeptonFromtopHiggs_pdgId","GenPart_pdgId","LeptonFromtopHiggs_genPartIdx","GenPart_genPartIdxMother","p4_LeptonFromtopHiggs"})
    //            .Define("LeptonFromtop4vecs","sel_leptongentoporHiggs[0]")
    //            .Define("LeptonFromtop_pt","LeptonFromtop4vecs.Pt()")
    //            .Define("LeptonFromtop_eta","LeptonFromtop4vecs.Eta()")
    //            .Define("LeptonFromtop_phi","LeptonFromtop4vecs.Phi()")
    //            .Define("LeptonFromtop_mass","LeptonFromtop4vecs.M()")
    //            .Define("LeptonFromtop_transverse_energy","sqrt(LeptonFromtop_pt*LeptonFromtop_pt + LeptonFromtop_mass*LeptonFromtop_mass)")
    //            .Define("LeptonFromtop_energy","LeptonFromtop4vecs.E()")
    //            .Define("LeptonFromtop_px","LeptonFromtop4vecs.Px()")
    //            .Define("LeptonFromtop_py","LeptonFromtop4vecs.Py()")
    //            .Define("LeptonFromtop_pz","LeptonFromtop4vecs.Pz()")
    //            .Define("LeptonFromHiggs4vecs","sel_leptongentoporHiggs[1]")
    //            .Define("LeptonFromHiggs_pt","LeptonFromHiggs4vecs.Pt()")
    //            .Define("LeptonFromHiggs_eta","LeptonFromHiggs4vecs.Eta()")
    //            .Define("LeptonFromHiggs_phi","LeptonFromHiggs4vecs.Phi()")
    //            .Define("LeptonFromHiggs_mass","LeptonFromHiggs4vecs.M()")
    //            .Define("LeptonFromHiggs_transverse_energy","sqrt(LeptonFromHiggs_pt*LeptonFromHiggs_pt + LeptonFromHiggs_mass*LeptonFromHiggs_mass)")
    //            .Define("LeptonFromHiggs_energy","LeptonFromHiggs4vecs.E()")
    //            .Define("LeptonFromHiggs_px","LeptonFromHiggs4vecs.Px()")
    //            .Define("LeptonFromHiggs_py","LeptonFromHiggs4vecs.Py()")
    //            .Define("LeptonFromHiggs_pz","LeptonFromHiggs4vecs.Pz()")
    //            .Define("LeptonFromtopHiggs_ordered_pt",::concatenate,{"LeptonFromtop_pt","LeptonFromHiggs_pt"})
    //            .Define("LeptonFromtopHiggs_ordered_px",::concatenate,{"LeptonFromtop_px","LeptonFromHiggs_px"})
    //            .Define("LeptonFromtopHiggs_ordered_py",::concatenate,{"LeptonFromtop_py","LeptonFromHiggs_py"})
    //            .Define("LeptonFromtopHiggs_ordered_pz",::concatenate,{"LeptonFromtop_pz","LeptonFromHiggs_pz"})
    //            .Define("LeptonFromtopHiggs_ordered_energy",::concatenate,{"LeptonFromtop_energy","LeptonFromHiggs_energy"})
    //            .Define("LeptonFromtopHiggs_ordered_mass",::concatenate,{"LeptonFromtop_mass","LeptonFromHiggs_mass"})
    //            .Define("LeptonFromtop_LeptonFromHiggs","LeptonFromtop4vecs + LeptonFromHiggs4vecs")
    //            .Define("LeptonFromtop_LeptonFromHiggs_pt","LeptonFromtop_LeptonFromHiggs.Pt()")
    //            .Define("LeptonFromtop_LeptonFromHiggs_eta","LeptonFromtop_LeptonFromHiggs.Eta()")
    //            .Define("LeptonFromtop_LeptonFromHiggs_phi","LeptonFromtop_LeptonFromHiggs.Phi()")
    //            .Define("LeptonFromtop_LeptonFromHiggs_mass","LeptonFromtop_LeptonFromHiggs.M()")
    //            .Define("LeptonFromtop_LeptonFromHiggs_transverse_energy","sqrt(LeptonFromtop_LeptonFromHiggs_pt*LeptonFromtop_LeptonFromHiggs_pt + LeptonFromtop_LeptonFromHiggs_mass*LeptonFromtop_LeptonFromHiggs_mass)")
    //            .Define("LeptonFromtop_LeptonFromHiggs_deltaeta","abs(LeptonFromtop_eta - LeptonFromHiggs_eta)")
    //            .Define("LeptonFromtop_LeptonFromHiggs_deltaphi","abs(ROOT::VecOps::DeltaPhi(LeptonFromtop_phi,LeptonFromHiggs_phi))")
    //            .Define("LeptonFromtop_LeptonFromHiggs_deltaR","ROOT::VecOps::DeltaR(LeptonFromtop_eta,LeptonFromHiggs_eta,LeptonFromtop_phi,LeptonFromHiggs_phi)");

    // _rlm = _rlm.Define("sel_leptongenW",::isLfromW,{"Selected_lepton_pdgId","GenPart_pdgId","Selected_lepton_genPartIdx","GenPart_genPartIdxMother"})
    //            .Define("LeptonFromW_pt","Selected_lepton_pt[sel_leptongenW]")
    //            .Define("LeptonFromW_number","int(LeptonFromW_pt.size())");
    //            .Define("LeptonFromW_leading_pt","(Selected_lepton_number > 0) ? LeptonFromW_pt[0]:-100")
    //            .Define("LeptonFromW_subleading_pt","(Selected_lepton_number > 1) ? LeptonFromW_pt[1]:-100")
    //            .Define("LeptonFromW_sum_two_muons_pt","(Selected_lepton_number > 1) ? LeptonFromW_pt[0] + LeptonFromW_pt[1]:-100")
    //            .Define("LeptonFromW_sum_all_muons_pt","Sum(LeptonFromW_pt)")
    //            .Define("LeptonFromW_eta","Selected_lepton_eta[sel_leptongenW]")
    //            .Define("LeptonFromW_phi","Selected_lepton_phi[sel_leptongenW]")
    //            .Define("LeptonFromW_mass","Selected_lepton_mass[sel_leptongenW]")
    //            .Define("LeptonFromW_charge","Selected_lepton_charge[sel_leptongenW]")
    //            .Define("LeptonFromW_charge_sum","(Selected_lepton_number > 1) ? LeptonFromW_charge[0] + LeptonFromW_charge[1]:-100")
    //            .Define("LeptonFromW_deltaeta","(Selected_lepton_number > 1) ? abs(LeptonFromW_eta[1] - LeptonFromW_eta[0]):-100")
    //            .Define("LeptonFromW_deltaphi","(Selected_lepton_number > 1) ? abs(ROOT::VecOps::DeltaPhi(LeptonFromW_phi[0],LeptonFromW_phi[1])):-100")
    //            .Define("LeptonFromW_deltaR","(Selected_lepton_number > 1) ? ROOT::VecOps::DeltaR(LeptonFromW_eta[0],LeptonFromW_eta[1],LeptonFromW_phi[0],LeptonFromW_phi[1]):-100")
    //            .Define("LeptonFromW_genPartIdx","Selected_lepton_genPartIdx[sel_leptongenW]")
    //            .Define("LeptonFromW_genPartFlav","Selected_lepton_genPartFlav[sel_leptongenW]")
    //            .Define("LeptonFromW_pdgId","Selected_lepton_pdgId[sel_leptongenW]")
    //            .Define("p4_LeptonFromW",::generate_4vec,{"LeptonFromW_pt","LeptonFromW_eta","LeptonFromW_phi","LeptonFromW_mass"});

    // _rlm = _rlm.Define("sel_muongenW",::isLfromW,{"Selected_muon_pdgId","GenPart_pdgId","Selected_muon_genPartIdx","GenPart_genPartIdxMother"})
    //            .Define("MuonFromW_pt","Selected_muon_pt[sel_muongenW]")
    //            .Define("MuonFromW_charge","Selected_muon_charge[sel_muongenW]")
    //            .Define("MuonFromW_number","int(MuonFromW_pt.size())");

    // _rlm = _rlm.Define("sel_electrongenW",::isLfromW,{"Selected_electron_pdgId","GenPart_pdgId","Selected_electron_genPartIdx","GenPart_genPartIdxMother"})
    //            .Define("ElectronFromW_pt","Selected_electron_pt[sel_electrongenW]")
    //            .Define("ElectronFromW_charge","Selected_electron_charge[sel_electrongenW]")
    //            .Define("ElectronFromW_number","int(ElectronFromW_pt.size())");

    _rlm = _rlm.Define("sel_leptongenMother",::MotherfromL,{"Selected_lepton_pdgId","GenPart_pdgId","Selected_lepton_genPartIdx","GenPart_genPartIdxMother"})
               .Define("MotherfromLepton_pdgId","sel_leptongenMother")
               .Define("MotherfromLepton1_pdgId","abs(sel_leptongenMother[0])")     
               .Define("MotherfromLepton2_pdgId","abs(sel_leptongenMother[1])")
               .Define("MotherfromLepton12_pdgId","abs(sel_leptongenMother[0]) + abs(sel_leptongenMother[1])");

    _rlm = _rlm.Define("lepton_different_charge",::isLdifferentcharge,{"Selected_lepton_pdgId","GenPart_pdgId","Selected_lepton_genPartIdx"})
               .Define("lepton_non_prompt",::isLnonprompt,{"Selected_lepton_genPartFlav"});

    // _rlm = _rlm.Define("GenLeptons","abs(GenPart_pdgId) == 13 || abs(GenPart_pdgId) == 11")
    //            .Define("MotherGenLeptons","GenPart_genPartIdxMother[GenLeptons]")
    //            .Define("MotherGenLeptonsPdgId","ROOT::VecOps::Take(GenPart_pdgId, MotherGenLeptons)")
    //            .Define("IsFromW","abs(MotherGenLeptonsPdgId) == 24")
    //            .Define("GenLeptonsPdgId","GenPart_pdgId[GenLeptons]")
    //            .Define("GenLeptons_pt","GenPart_pt[GenLeptons]")
    //            .Define("GenLeptons_eta","GenPart_eta[GenLeptons]")
    //            .Define("GenLeptons_phi","GenPart_phi[GenLeptons]");

    // _rlm = _rlm.Define("GenMuons","abs(GenPart_pdgId) == 13")
    //            .Define("GenMuonsPdgId","GenPart_pdgId[GenMuons]")
    //            .Define("MotherGenMuons","GenPart_genPartIdxMother[GenMuons]")
    //            .Define("MotherGenMuonsPdgId","ROOT::VecOps::Take(GenPart_pdgId, MotherGenMuons)")
    //            .Define("IsMuonFromW","abs(MotherGenMuonsPdgId) == 24");

    // _rlm = _rlm.Define("GenElectrons","abs(GenPart_pdgId) == 11")
    //            .Define("GenElectronsPdgId","GenPart_pdgId[GenElectrons]")
    //            .Define("MotherGenElectrons","GenPart_genPartIdxMother[GenElectrons]")
    //            .Define("MotherGenElectronsPdgId","ROOT::VecOps::Take(GenPart_pdgId, MotherGenElectrons)")
    //            .Define("IsElectronFromW","abs(MotherGenElectronsPdgId) == 24");

    // _rlm = _rlm.Define("GenW","abs(GenPart_pdgId) == 24")
    //            .Define("MotherGenW","GenPart_genPartIdxMother[GenW]")
    //            .Define("MotherGenWPdgId","ROOT::VecOps::Take(GenPart_pdgId, MotherGenW)")
    //            .Define("IsFromHiggs","abs(MotherGenWPdgId) == 25")
    //            .Define("IsFromtop","abs(MotherGenWPdgId) == 6");

    // _rlm = _rlm.Define("GenNeutrinos","abs(GenPart_pdgId) == 14 || abs(GenPart_pdgId) == 12")
    //            .Define("GenNeutrinosPdgId","GenPart_pdgId[GenNeutrinos]")
    //            .Define("GenNeutrinos_pt","GenPart_pt[GenNeutrinos]")
    //            .Define("GenNeutrinos_eta","GenPart_eta[GenNeutrinos]")
    //            .Define("GenNeutrinos_phi","GenPart_phi[GenNeutrinos]")
    //            .Define("MotherGenNeutrinos","GenPart_genPartIdxMother[GenNeutrinos]")
    //            .Define("MotherGenNeutrinosPdgId","ROOT::VecOps::Take(GenPart_pdgId, MotherGenNeutrinos)")
    //            .Define("IsNeutrinoFromW","abs(MotherGenNeutrinosPdgId) == 24")
    //            .Define("GenNeutrinoFromWPdgId","GenNeutrinosPdgId[IsNeutrinoFromW]")
    //            .Define("GenNeutrinoFromW_pt","GenNeutrinos_pt[IsNeutrinoFromW]")
    //            .Define("GenNeutrinoFromW_eta","GenNeutrinos_eta[IsNeutrinoFromW]")
    //            .Define("GenNeutrinoFromW_phi","GenNeutrinos_phi[IsNeutrinoFromW]")
    //            .Define("GenNeutrinoFromW_number","int(GenNeutrinoFromW_pt.size())")
    //            .Define("MotherNeutrinoFromW","MotherGenNeutrinos[IsNeutrinoFromW]")
    //            .Define("MotherNeutrinoFromWPdgId","ROOT::VecOps::Take(GenPart_pdgId, MotherNeutrinoFromW)")
    //            .Define("MotherNeutrinoFromWMass","ROOT::VecOps::Take(GenPart_mass, MotherNeutrinoFromW)")
    //            .Define("sel_NeutrinoFromWgenHiggs",::isWfromHiggs,{"GenPart_pdgId","MotherNeutrinoFromW","GenPart_genPartIdxMother","GenPart_mass"})
    //            .Define("sel_NeutrinoFromWgentop",::isWfromtop,{"GenPart_pdgId","MotherNeutrinoFromW","GenPart_genPartIdxMother","GenPart_mass"})
    //            .Define("GenNeutrinoFromtop_pt","GenNeutrinoFromW_pt[sel_NeutrinoFromWgentop]")
    //            .Define("GenNeutrinoFromtop_eta","GenNeutrinoFromW_eta[sel_NeutrinoFromWgentop]")
    //            .Define("GenNeutrinoFromtop_phi","GenNeutrinoFromW_phi[sel_NeutrinoFromWgentop]")
    //            .Define("GenNeutrinoFromtop_px","GenNeutrinoFromtop_pt*cos(GenNeutrinoFromtop_phi)")
    //            .Define("GenNeutrinoFromtop_py","GenNeutrinoFromtop_pt*sin(GenNeutrinoFromtop_phi)")
    //            .Define("GenNeutrinoFromtop_number","int(GenNeutrinoFromtop_pt.size())")
    //            .Define("p4_GenNeutrinoFromtop",::generate_4vec_3,{"GenNeutrinoFromtop_pt","GenNeutrinoFromtop_eta","GenNeutrinoFromtop_phi"})
    //            .Define("p4_GenNeutrinoFromtop_first","p4_GenNeutrinoFromtop[0]")
    //            .Define("GenNeutrinoFromtop_first_pt","p4_GenNeutrinoFromtop_first.Pt()")
    //            .Define("GenNeutrinoFromtop_first_eta","p4_GenNeutrinoFromtop_first.Eta()")
    //            .Define("GenNeutrinoFromtop_first_phi","p4_GenNeutrinoFromtop_first.Phi()")
    //            .Define("GenNeutrinoFromHiggs_pt","GenNeutrinoFromW_pt[sel_NeutrinoFromWgenHiggs]")
    //            .Define("GenNeutrinoFromHiggs_eta","GenNeutrinoFromW_eta[sel_NeutrinoFromWgenHiggs]")
    //            .Define("GenNeutrinoFromHiggs_phi","GenNeutrinoFromW_phi[sel_NeutrinoFromWgenHiggs]")
    //            .Define("GenNeutrinoFromHiggs_px","GenNeutrinoFromHiggs_pt*cos(GenNeutrinoFromHiggs_phi)")
    //            .Define("GenNeutrinoFromHiggs_py","GenNeutrinoFromHiggs_pt*sin(GenNeutrinoFromHiggs_phi)")
    //            .Define("GenNeutrinoFromHiggs_number","int(GenNeutrinoFromHiggs_pt.size())")
    //            .Define("p4_GenNeutrinoFromHiggs",::generate_4vec_3,{"GenNeutrinoFromHiggs_pt","GenNeutrinoFromHiggs_eta","GenNeutrinoFromHiggs_phi"})
    //            .Define("p4_GenNeutrinoFromHiggs_first","p4_GenNeutrinoFromHiggs[0]")
    //            .Define("GenNeutrinoFromHiggs_first_pt","p4_GenNeutrinoFromHiggs_first.Pt()")
    //            .Define("GenNeutrinoFromHiggs_first_eta","p4_GenNeutrinoFromHiggs_first.Eta()")
    //            .Define("GenNeutrinoFromHiggs_first_phi","p4_GenNeutrinoFromHiggs_first.Phi()")
    //            .Define("MotherNeutrinoFromHiggsPdgid","MotherNeutrinoFromWPdgId[sel_NeutrinoFromWgenHiggs]")
    //            .Define("MotherNeutrinoFromHiggsMass","MotherNeutrinoFromWMass[sel_NeutrinoFromWgenHiggs]")
    //            .Define("NeutrinoFromtop_NeutrinoFromHiggs","p4_GenNeutrinoFromtop_first + p4_GenNeutrinoFromHiggs_first")
    //            .Define("NeutrinoFromtop_NeutrinoFromHiggs_pt","NeutrinoFromtop_NeutrinoFromHiggs.Pt()")
    //            .Define("NeutrinoFromtop_NeutrinoFromHiggs_eta","NeutrinoFromtop_NeutrinoFromHiggs.Eta()")
    //            .Define("NeutrinoFromtop_NeutrinoFromHiggs_phi","NeutrinoFromtop_NeutrinoFromHiggs.Phi()")
    //            .Define("NeutrinoFromtop_NeutrinoFromHiggs_mass","NeutrinoFromtop_NeutrinoFromHiggs.M()")
    //            .Define("NeutrinoFromtop_NeutrinoFromHiggs_transverse_energy","sqrt(NeutrinoFromtop_NeutrinoFromHiggs_pt*NeutrinoFromtop_NeutrinoFromHiggs_pt + NeutrinoFromtop_NeutrinoFromHiggs_mass*NeutrinoFromtop_NeutrinoFromHiggs_mass)")
    //            .Define("NeutrinoFromtop_NeutrinoFromHiggs_deltaeta","abs(GenNeutrinoFromtop_first_eta - GenNeutrinoFromHiggs_first_eta)")
    //            .Define("NeutrinoFromtop_NeutrinoFromHiggs_deltaphi","abs(ROOT::VecOps::DeltaPhi(GenNeutrinoFromtop_first_phi,GenNeutrinoFromHiggs_first_phi))")
    //            .Define("NeutrinoFromtop_NeutrinoFromHiggs_deltaR","ROOT::VecOps::DeltaR(GenNeutrinoFromtop_first_eta,GenNeutrinoFromHiggs_first_eta,GenNeutrinoFromtop_first_phi,GenNeutrinoFromHiggs_first_phi)");

    // _rlm = _rlm.Define("NeutrinoFromtop_LeptonFromtop","p4_GenNeutrinoFromtop_first + LeptonFromtop4vecs")
    //            .Define("NeutrinoFromtop_LeptonFromtop_pt","NeutrinoFromtop_LeptonFromtop.Pt()")
    //            .Define("NeutrinoFromtop_LeptonFromtop_eta","NeutrinoFromtop_LeptonFromtop.Eta()")
    //            .Define("NeutrinoFromtop_LeptonFromtop_phi","NeutrinoFromtop_LeptonFromtop.Phi()")
    //            .Define("NeutrinoFromtop_LeptonFromtop_mass","NeutrinoFromtop_LeptonFromtop.M()")
    //            .Define("NeutrinoFromtop_LeptonFromtop_transverse_energy","sqrt(NeutrinoFromtop_LeptonFromtop_pt*NeutrinoFromtop_LeptonFromtop_pt + NeutrinoFromtop_LeptonFromtop_mass*NeutrinoFromtop_LeptonFromtop_mass)")
    //            .Define("NeutrinoFromtop_LeptonFromtop_deltaeta","abs(GenNeutrinoFromtop_first_eta - LeptonFromtop_eta)")
    //            .Define("NeutrinoFromtop_LeptonFromtop_deltaphi","abs(ROOT::VecOps::DeltaPhi(GenNeutrinoFromtop_first_phi,LeptonFromtop_phi))")
    //            .Define("NeutrinoFromtop_LeptonFromtop_deltaR","ROOT::VecOps::DeltaR(GenNeutrinoFromtop_first_eta,LeptonFromtop_eta,GenNeutrinoFromtop_first_phi,LeptonFromtop_phi)")
    //            .Define("NeutrinoFromHiggs_LeptonFromHiggs","p4_GenNeutrinoFromHiggs_first + LeptonFromHiggs4vecs")
    //            .Define("NeutrinoFromHiggs_LeptonFromHiggs_pt","NeutrinoFromHiggs_LeptonFromHiggs.Pt()")
    //            .Define("NeutrinoFromHiggs_LeptonFromHiggs_eta","NeutrinoFromHiggs_LeptonFromHiggs.Eta()")
    //            .Define("NeutrinoFromHiggs_LeptonFromHiggs_phi","NeutrinoFromHiggs_LeptonFromHiggs.Phi()")
    //            .Define("NeutrinoFromHiggs_LeptonFromHiggs_mass","NeutrinoFromHiggs_LeptonFromHiggs.M()")
    //            .Define("NeutrinoFromHiggs_LeptonFromHiggs_transverse_energy","sqrt(NeutrinoFromHiggs_LeptonFromHiggs_pt*NeutrinoFromHiggs_LeptonFromHiggs_pt + NeutrinoFromHiggs_LeptonFromHiggs_mass*NeutrinoFromHiggs_LeptonFromHiggs_mass)")
    //            .Define("NeutrinoFromHiggs_LeptonFromHiggs_deltaeta","abs(GenNeutrinoFromHiggs_first_eta - LeptonFromHiggs_eta)")
    //            .Define("NeutrinoFromHiggs_LeptonFromHiggs_deltaphi","abs(ROOT::VecOps::DeltaPhi(GenNeutrinoFromHiggs_first_phi,LeptonFromHiggs_phi))")
    //            .Define("NeutrinoFromHiggs_LeptonFromHiggs_deltaR","ROOT::VecOps::DeltaR(GenNeutrinoFromHiggs_first_eta,LeptonFromHiggs_eta,GenNeutrinoFromHiggs_first_phi,LeptonFromHiggs_phi)");

}

//=================================Select Jets=================================================//
//check the twiki page :    https://twiki.cern.ch/twiki/bin/view/CMS/JetID
//to find jetId working points for the purpose of  your analysis.
    //jetId==2 means: pass tight ID, fail tightLepVeto
    //jetId==6 means: pass tight ID and tightLepVeto ID.
//=============================================================================================//

void TprimeAnalyser::selectJets(std::string year)
{

    cout<<"select good jets"<<endl;
    if(debug)
    {
        cout<< "================================//================================="<<endl;
        cout<< "Line : "<< __LINE__<<" Function : "<<__FUNCTION__<<endl;
        cout<< "================================//================================="<<endl;
    }

    _rlm = _rlm.Define("goodJets_loose","Jet_pt_corr > 20 && abs(Jet_eta) < 4.5 && Jet_jetId >= 6")
               .Define("Selected_jet_loose_pt","Jet_pt_corr[goodJets_loose]")
               .Define("Selected_jet_loose_number","int(Selected_jet_loose_pt.size())")
               .Define("Selected_jet_loose_eta","Jet_eta[goodJets_loose]")
               .Define("Selected_jet_loose_phi","Jet_phi[goodJets_loose]")
               .Define("Selected_jet_loose_mass","Jet_mass_corr[goodJets_loose]");

    _rlm = _rlm.Define("goodJets","(Jet_pt_corr > 30 && Jet_pt_corr < 50 && abs(Jet_eta) < 2.4 && Jet_jetId >= 6 && Jet_puId == 7) || (Jet_pt_corr > 50 && abs(Jet_eta) < 2.4 && Jet_jetId >= 6)")
               .Define("Selected_jet_pt","Jet_pt_corr[goodJets]")
               .Define("Selected_jet_number","int(Selected_jet_pt.size())")
               .Define("Selected_jet_leading_pt","(Selected_jet_number>0) ? Selected_jet_pt[0]:-100")
               .Define("Selected_jet_subleading_pt","(Selected_jet_number>1) ? Selected_jet_pt[1]:-100")
               .Define("Selected_jet_subsubleading_pt","(Selected_jet_number>2) ? Selected_jet_pt[2]:-100")
               .Define("Selected_jet_Ht","Sum(Selected_jet_pt)")
               .Define("Selected_jet_eta","Jet_eta[goodJets]")
               .Define("Selected_jet_phi","Jet_phi[goodJets]")
               .Define("Selected_jet_mass","Jet_mass_corr[goodJets]")
               .Define("Selected_jet_btagDeepB","Jet_btagDeepB[goodJets]")
               .Define("Selected_jet_btagDeepFlavB","Jet_btagDeepFlavB[goodJets]");
    if(!_isData)
    {
        _rlm = _rlm.Define("Selected_jet_genJetIdx","Jet_genJetIdx[goodJets]")
                   .Define("Selected_jet_partonFlavour","Jet_partonFlavour[goodJets]")
                   .Define("Selected_jet_hadronFlavour","Jet_hadronFlavour[goodJets]");
    }
    _rlm = _rlm.Define("Selected_jet_jetId","Jet_jetId[goodJets]")
               .Define("Selected_jet_puId","Jet_puId[goodJets]")
               .Define("jet4vecs", ::generate_4vec,{"Selected_jet_pt","Selected_jet_eta","Selected_jet_phi","Selected_jet_mass"});

    _rlm = _rlm.Define("Lepton_Jet_overlap_loosett1l",::CheckOverlaps,{"jet4vecs","lep4vecs_loosett1l"})
               .Define("Selected_clean_jet_loosett1l_pt","Selected_jet_pt[Lepton_Jet_overlap_loosett1l]")
               .Define("Selected_clean_jet_loosett1l_number","int(Selected_clean_jet_loosett1l_pt.size())")
               .Define("Selected_clean_jet_loosett1l_leading_pt","(Selected_clean_jet_loosett1l_number>0) ? Selected_clean_jet_loosett1l_pt[0]:-100")
               .Define("Selected_clean_jet_loosett1l_subleading_pt","(Selected_clean_jet_loosett1l_number>1) ? Selected_clean_jet_loosett1l_pt[1]:-100")
               .Define("Selected_clean_jet_loosett1l_subsubleading_pt","(Selected_clean_jet_loosett1l_number>2) ? Selected_clean_jet_loosett1l_pt[2]:-100")
               .Define("Selected_clean_jet_loosett1l_Ht","Sum(Selected_clean_jet_loosett1l_pt)")
               .Define("Selected_clean_jet_loosett1l_eta","Selected_jet_eta[Lepton_Jet_overlap_loosett1l]")
               .Define("Selected_clean_jet_loosett1l_phi","Selected_jet_phi[Lepton_Jet_overlap_loosett1l]")
               .Define("Selected_clean_jet_loosett1l_mass","Selected_jet_mass[Lepton_Jet_overlap_loosett1l]")
               .Define("Selected_clean_jet_loosett1l_transverse_energy","sqrt(Selected_clean_jet_loosett1l_pt*Selected_clean_jet_loosett1l_pt + Selected_clean_jet_loosett1l_mass*Selected_clean_jet_loosett1l_mass)")
               .Define("Selected_clean_jet_loosett1l_btagDeepB","Selected_jet_btagDeepB[Lepton_Jet_overlap_loosett1l]")
               .Define("Selected_clean_jet_loosett1l_btagDeepFlavB","Selected_jet_btagDeepFlavB[Lepton_Jet_overlap_loosett1l]");
    if(!_isData)
    {
        _rlm = _rlm.Define("Selected_clean_jet_loosett1l_genJetIdx","Selected_jet_genJetIdx[Lepton_Jet_overlap_loosett1l]")
                   .Define("Selected_clean_jet_loosett1l_partonFlavour","Selected_jet_partonFlavour[Lepton_Jet_overlap_loosett1l]")
                   .Define("Selected_clean_jet_loosett1l_hadronFlavour","Selected_jet_hadronFlavour[Lepton_Jet_overlap_loosett1l]");
    }
    _rlm = _rlm.Define("Selected_clean_jet_loosett1l_jetId","Selected_jet_jetId[Lepton_Jet_overlap_loosett1l]")
               .Define("Selected_clean_jet_loosett1l_puId","Selected_jet_puId[Lepton_Jet_overlap_loosett1l]")
               .Define("cleanjet4vecs_loosett1l", ::generate_4vec,{"Selected_clean_jet_loosett1l_pt","Selected_clean_jet_loosett1l_eta","Selected_clean_jet_loosett1l_phi","Selected_clean_jet_loosett1l_mass"})
               .Define("cleanjet4Rvecs_loosett1l",::generate_4Rvec,{"Selected_clean_jet_loosett1l_pt","Selected_clean_jet_loosett1l_eta","Selected_clean_jet_loosett1l_phi","Selected_clean_jet_loosett1l_mass"})
               .Define("Selected_clean_jet_loosett1l_energy","ROOT::VecOps::Map(cleanjet4Rvecs_loosett1l, FourVecEnergy)")
               .Define("Selected_clean_jet_loosett1l_px","ROOT::VecOps::Map(cleanjet4Rvecs_loosett1l, FourVecPx)")
               .Define("Selected_clean_jet_loosett1l_py","ROOT::VecOps::Map(cleanjet4Rvecs_loosett1l, FourVecPy)")
               .Define("Selected_clean_jet_loosett1l_pz","ROOT::VecOps::Map(cleanjet4Rvecs_loosett1l, FourVecPz)");

    _rlm = _rlm.Define("Lepton_Jet_overlap",::CheckOverlaps,{"jet4vecs","lep4vecs"})
               .Define("Selected_clean_jet_pt","Selected_jet_pt[Lepton_Jet_overlap]")
               .Define("Selected_clean_jet_number","int(Selected_clean_jet_pt.size())")
               .Define("Selected_clean_jet_leading_pt","(Selected_clean_jet_number>0) ? Selected_clean_jet_pt[0]:-100")
               .Define("Selected_clean_jet_subleading_pt","(Selected_clean_jet_number>1) ? Selected_clean_jet_pt[1]:-100")
               .Define("Selected_clean_jet_subsubleading_pt","(Selected_clean_jet_number>2) ? Selected_clean_jet_pt[2]:-100")
               .Define("Selected_clean_jet_Ht","Sum(Selected_clean_jet_pt)")
               .Define("Selected_clean_jet_eta","Selected_jet_eta[Lepton_Jet_overlap]")
               .Define("Selected_clean_jet_phi","Selected_jet_phi[Lepton_Jet_overlap]")
               .Define("Selected_clean_jet_mass","Selected_jet_mass[Lepton_Jet_overlap]")
               .Define("Selected_clean_jet_transverse_energy","sqrt(Selected_clean_jet_pt*Selected_clean_jet_pt + Selected_clean_jet_mass*Selected_clean_jet_mass)")
               .Define("Selected_clean_jet_btagDeepB","Selected_jet_btagDeepB[Lepton_Jet_overlap]")
               .Define("Selected_clean_jet_btagDeepFlavB","Selected_jet_btagDeepFlavB[Lepton_Jet_overlap]");
    if(!_isData)
    {
        _rlm = _rlm.Define("Selected_clean_jet_genJetIdx","Selected_jet_genJetIdx[Lepton_Jet_overlap]")
                   .Define("Selected_clean_jet_partonFlavour","Selected_jet_partonFlavour[Lepton_Jet_overlap]")
                   .Define("Selected_clean_jet_hadronFlavour","Selected_jet_hadronFlavour[Lepton_Jet_overlap]");
    }
    _rlm = _rlm.Define("Selected_clean_jet_jetId","Selected_jet_jetId[Lepton_Jet_overlap]")
               .Define("Selected_clean_jet_puId","Selected_jet_puId[Lepton_Jet_overlap]")
               .Define("cleanjet4vecs", ::generate_4vec,{"Selected_clean_jet_pt","Selected_clean_jet_eta","Selected_clean_jet_phi","Selected_clean_jet_mass"})
               .Define("cleanjet4Rvecs",::generate_4Rvec,{"Selected_clean_jet_pt","Selected_clean_jet_eta","Selected_clean_jet_phi","Selected_clean_jet_mass"})
               .Define("Selected_clean_jet_energy","ROOT::VecOps::Map(cleanjet4Rvecs, FourVecEnergy)")
               .Define("Selected_clean_jet_px","ROOT::VecOps::Map(cleanjet4Rvecs, FourVecPx)")
               .Define("Selected_clean_jet_py","ROOT::VecOps::Map(cleanjet4Rvecs, FourVecPy)")
               .Define("Selected_clean_jet_pz","ROOT::VecOps::Map(cleanjet4Rvecs, FourVecPz)");

    if(year == "2016preVFP")
    {
        _rlm = _rlm.Define("goodJets_btag","goodJets && Jet_btagDeepFlavB > 0.2598");
    }
    if(year == "2016postVFP")
    {
        _rlm = _rlm.Define("goodJets_btag","goodJets && Jet_btagDeepFlavB > 0.2489");
    }
    if(year == "2017")
    {
        _rlm = _rlm.Define("goodJets_btag","goodJets && Jet_btagDeepFlavB > 0.3040");
    }
    if(year == "2018")
    {
        _rlm = _rlm.Define("goodJets_btag","goodJets && Jet_btagDeepFlavB > 0.2783");
    }
    _rlm = _rlm.Define("Selected_bjet_pt","Jet_pt_corr[goodJets_btag]")
               .Define("Selected_bjet_number","int(Selected_bjet_pt.size())")
               .Define("Selected_bjet_leading_pt","(Selected_bjet_number>0) ? Selected_bjet_pt[0]:-100")
               .Define("Selected_bjet_subleading_pt","(Selected_bjet_number>1) ? Selected_bjet_pt[1]:-100")
               .Define("Selected_bjet_subsubleading_pt","(Selected_bjet_number>2) ? Selected_bjet_pt[2]:-100")
               .Define("Selected_bjet_sum_all_jets_pt","Sum(Selected_bjet_pt)")
               .Define("Selected_bjet_eta","Jet_eta[goodJets_btag]")
               .Define("Selected_bjet_phi","Jet_phi[goodJets_btag]")
               .Define("Selected_bjet_mass","Jet_mass_corr[goodJets_btag]")
               .Define("Selected_bjet_btagDeepB","Jet_btagDeepB[goodJets_btag]")
               .Define("Selected_bjet_btagDeepFlavB","Jet_btagDeepFlavB[goodJets_btag]");
    if(!_isData)
    {
        _rlm = _rlm.Define("Selected_bjet_genJetIdx","Jet_genJetIdx[goodJets_btag]")
                   .Define("Selected_bjet_partonFlavour","Jet_partonFlavour[goodJets_btag]")
                   .Define("Selected_bjet_hadronFlavour","Jet_hadronFlavour[goodJets_btag]");
    }
    _rlm = _rlm.Define("Selected_bjet_jetId","Jet_jetId[goodJets_btag]")
               .Define("Selected_bjet_puId","Jet_puId[goodJets_btag]")
               .Define("bjet4vecs", ::generate_4vec,{"Selected_bjet_pt","Selected_bjet_eta","Selected_bjet_phi","Selected_bjet_mass"});

    _rlm = _rlm.Define("Lepton_bJet_overlap_loosett1l",::CheckOverlaps,{"bjet4vecs","lep4vecs_loosett1l"})
               .Define("Selected_clean_bjet_loosett1l_pt","Selected_bjet_pt[Lepton_bJet_overlap_loosett1l]")
               .Define("Selected_clean_bjet_loosett1l_number","int(Selected_clean_bjet_loosett1l_pt.size())")
               .Define("Selected_clean_bjet_loosett1l_leading_pt","(Selected_clean_bjet_loosett1l_number>0) ? Selected_clean_bjet_loosett1l_pt[0]:-100")
               .Define("Selected_clean_bjet_loosett1l_subleading_pt","(Selected_clean_bjet_loosett1l_number>1) ? Selected_clean_bjet_loosett1l_pt[1]:-100")
               .Define("Selected_clean_bjet_loosett1l_subsubleading_pt","(Selected_clean_bjet_loosett1l_number>2) ? Selected_clean_bjet_loosett1l_pt[2]:-100")
               .Define("Selected_clean_bjet_loosett1l_sum_all_jets_pt","Sum(Selected_clean_bjet_loosett1l_pt)")
               .Define("Selected_clean_bjet_loosett1l_eta","Selected_bjet_eta[Lepton_bJet_overlap_loosett1l]")
               .Define("Selected_clean_bjet_loosett1l_phi","Selected_bjet_phi[Lepton_bJet_overlap_loosett1l]")
               .Define("Selected_clean_bjet_loosett1l_mass","Selected_bjet_mass[Lepton_bJet_overlap_loosett1l]")
               .Define("Selected_clean_bjet_loosett1l_transverse_energy","sqrt(Selected_clean_bjet_loosett1l_pt*Selected_clean_bjet_loosett1l_pt + Selected_clean_bjet_loosett1l_mass*Selected_clean_bjet_loosett1l_mass)")
               .Define("Selected_clean_bjet_loosett1l_btagDeepB","Selected_bjet_btagDeepB[Lepton_bJet_overlap_loosett1l]")
               .Define("Selected_clean_bjet_loosett1l_btagDeepFlavB","Selected_bjet_btagDeepFlavB[Lepton_bJet_overlap_loosett1l]");
    if(!_isData)
    {
        _rlm = _rlm.Define("Selected_clean_bjet_loosett1l_genJetIdx","Selected_bjet_genJetIdx[Lepton_bJet_overlap_loosett1l]")
                   .Define("Selected_clean_bjet_loosett1l_partonFlavour","Selected_bjet_partonFlavour[Lepton_bJet_overlap_loosett1l]")
                   .Define("Selected_clean_bjet_loosett1l_hadronFlavour","Selected_bjet_hadronFlavour[Lepton_bJet_overlap_loosett1l]");
    }
    _rlm = _rlm.Define("Selected_clean_bjet_loosett1l_jetId","Selected_bjet_jetId[Lepton_bJet_overlap_loosett1l]")
               .Define("Selected_clean_bjet_loosett1l_puId","Selected_bjet_puId[Lepton_bJet_overlap_loosett1l]")
               .Define("cleanbjet4vecs_loosett1l", ::generate_4vec,{"Selected_clean_bjet_loosett1l_pt","Selected_clean_bjet_loosett1l_eta","Selected_clean_bjet_loosett1l_phi","Selected_clean_bjet_loosett1l_mass"})
               .Define("cleanbjet4Rvecs_loosett1l",::generate_4Rvec,{"Selected_clean_bjet_loosett1l_pt","Selected_clean_bjet_loosett1l_eta","Selected_clean_bjet_loosett1l_phi","Selected_clean_bjet_loosett1l_mass"})
               .Define("Selected_clean_bjet_loosett1l_energy","ROOT::VecOps::Map(cleanbjet4Rvecs_loosett1l, FourVecEnergy)")
               .Define("Selected_clean_bjet_loosett1l_px","ROOT::VecOps::Map(cleanbjet4Rvecs_loosett1l, FourVecPx)")
               .Define("Selected_clean_bjet_loosett1l_py","ROOT::VecOps::Map(cleanbjet4Rvecs_loosett1l, FourVecPy)")
               .Define("Selected_clean_bjet_loosett1l_pz","ROOT::VecOps::Map(cleanbjet4Rvecs_loosett1l, FourVecPz)");

    _rlm = _rlm.Define("Lepton_bJet_overlap",::CheckOverlaps,{"bjet4vecs","lep4vecs"})
               .Define("Selected_clean_bjet_pt","Selected_bjet_pt[Lepton_bJet_overlap]")
               .Define("Selected_clean_bjet_number","int(Selected_clean_bjet_pt.size())")
               .Define("Selected_clean_bjet_leading_pt","(Selected_clean_bjet_number>0) ? Selected_clean_bjet_pt[0]:-100")
               .Define("Selected_clean_bjet_subleading_pt","(Selected_clean_bjet_number>1) ? Selected_clean_bjet_pt[1]:-100")
               .Define("Selected_clean_bjet_subsubleading_pt","(Selected_clean_bjet_number>2) ? Selected_clean_bjet_pt[2]:-100")
               .Define("Selected_clean_bjet_sum_all_jets_pt","Sum(Selected_clean_bjet_pt)")
               .Define("Selected_clean_bjet_eta","Selected_bjet_eta[Lepton_bJet_overlap]")
               .Define("Selected_clean_bjet_phi","Selected_bjet_phi[Lepton_bJet_overlap]")
               .Define("Selected_clean_bjet_mass","Selected_bjet_mass[Lepton_bJet_overlap]")
               .Define("Selected_clean_bjet_transverse_energy","sqrt(Selected_clean_bjet_pt*Selected_clean_bjet_pt + Selected_clean_bjet_mass*Selected_clean_bjet_mass)")
               .Define("Selected_clean_bjet_btagDeepB","Selected_bjet_btagDeepB[Lepton_bJet_overlap]")
               .Define("Selected_clean_bjet_btagDeepFlavB","Selected_bjet_btagDeepFlavB[Lepton_bJet_overlap]");
    if(!_isData)
    {
        _rlm = _rlm.Define("Selected_clean_bjet_genJetIdx","Selected_bjet_genJetIdx[Lepton_bJet_overlap]")
                   .Define("Selected_clean_bjet_partonFlavour","Selected_bjet_partonFlavour[Lepton_bJet_overlap]")
                   .Define("Selected_clean_bjet_hadronFlavour","Selected_bjet_hadronFlavour[Lepton_bJet_overlap]");
    }
    _rlm = _rlm.Define("Selected_clean_bjet_jetId","Selected_bjet_jetId[Lepton_bJet_overlap]")
               .Define("Selected_clean_bjet_puId","Selected_bjet_puId[Lepton_bJet_overlap]")
               .Define("cleanbjet4vecs", ::generate_4vec,{"Selected_clean_bjet_pt","Selected_clean_bjet_eta","Selected_clean_bjet_phi","Selected_clean_bjet_mass"})
               .Define("cleanbjet4Rvecs",::generate_4Rvec,{"Selected_clean_bjet_pt","Selected_clean_bjet_eta","Selected_clean_bjet_phi","Selected_clean_bjet_mass"})
               .Define("Selected_clean_bjet_energy","ROOT::VecOps::Map(cleanbjet4Rvecs, FourVecEnergy)")
               .Define("Selected_clean_bjet_px","ROOT::VecOps::Map(cleanbjet4Rvecs, FourVecPx)")
               .Define("Selected_clean_bjet_py","ROOT::VecOps::Map(cleanbjet4Rvecs, FourVecPy)")
               .Define("Selected_clean_bjet_pz","ROOT::VecOps::Map(cleanbjet4Rvecs, FourVecPz)");

    //For BTagging Efficiency
    if(!_isData)
    {
        _rlm = _rlm.Define("Selected_clean_bjet_btagpass_bcflav_loosett1l","Selected_clean_bjet_loosett1l_hadronFlavour != 0")
                   .Define("Selected_clean_bjet_loosett1l_btagpass_bcflav_pt","Selected_clean_bjet_loosett1l_pt[Selected_clean_bjet_btagpass_bcflav_loosett1l]")
                   .Define("Selected_clean_bjet_loosett1l_btagpass_bcflav_eta","abs(Selected_clean_bjet_loosett1l_eta[Selected_clean_bjet_btagpass_bcflav_loosett1l])")
                   .Define("Selected_clean_jet_all_bcflav_loosett1l","Selected_clean_jet_loosett1l_hadronFlavour != 0")
                   .Define("Selected_clean_jet_loosett1l_all_bcflav_pt","Selected_clean_jet_loosett1l_pt[Selected_clean_jet_all_bcflav_loosett1l]")
                   .Define("Selected_clean_jet_loosett1l_all_bcflav_eta","abs(Selected_clean_jet_loosett1l_eta[Selected_clean_jet_all_bcflav_loosett1l])")
                   .Define("Selected_clean_bjet_btagpass_lflav_loosett1l","Selected_clean_bjet_loosett1l_hadronFlavour == 0")
                   .Define("Selected_clean_bjet_loosett1l_btagpass_lflav_pt","Selected_clean_bjet_loosett1l_pt[Selected_clean_bjet_btagpass_lflav_loosett1l]")
                   .Define("Selected_clean_bjet_loosett1l_btagpass_lflav_eta","abs(Selected_clean_bjet_loosett1l_eta[Selected_clean_bjet_btagpass_lflav_loosett1l])")
                   .Define("Selected_clean_jet_all_lflav_loosett1l","Selected_clean_jet_loosett1l_hadronFlavour == 0")
                   .Define("Selected_clean_jet_loosett1l_all_lflav_pt","Selected_clean_jet_loosett1l_pt[Selected_clean_jet_all_lflav_loosett1l]")
                   .Define("Selected_clean_jet_loosett1l_all_lflav_eta","abs(Selected_clean_jet_loosett1l_eta[Selected_clean_jet_all_lflav_loosett1l])");

        _rlm = _rlm.Define("Selected_clean_bjet_btagpass_bcflav","Selected_clean_bjet_hadronFlavour != 0")
                   .Define("Selected_clean_bjet_btagpass_bcflav_pt","Selected_clean_bjet_pt[Selected_clean_bjet_btagpass_bcflav]")
                   .Define("Selected_clean_bjet_btagpass_bcflav_eta","abs(Selected_clean_bjet_eta[Selected_clean_bjet_btagpass_bcflav])")
                   .Define("Selected_clean_jet_all_bcflav","Selected_clean_jet_hadronFlavour != 0")
                   .Define("Selected_clean_jet_all_bcflav_pt","Selected_clean_jet_pt[Selected_clean_jet_all_bcflav]")
                   .Define("Selected_clean_jet_all_bcflav_eta","abs(Selected_clean_jet_eta[Selected_clean_jet_all_bcflav])")
                   .Define("Selected_clean_bjet_btagpass_lflav","Selected_clean_bjet_hadronFlavour == 0")
                   .Define("Selected_clean_bjet_btagpass_lflav_pt","Selected_clean_bjet_pt[Selected_clean_bjet_btagpass_lflav]")
                   .Define("Selected_clean_bjet_btagpass_lflav_eta","abs(Selected_clean_bjet_eta[Selected_clean_bjet_btagpass_lflav])")
                   .Define("Selected_clean_jet_all_lflav","Selected_clean_jet_hadronFlavour == 0")
                   .Define("Selected_clean_jet_all_lflav_pt","Selected_clean_jet_pt[Selected_clean_jet_all_lflav]")
                   .Define("Selected_clean_jet_all_lflav_eta","abs(Selected_clean_jet_eta[Selected_clean_jet_all_lflav])");
    }
}

void TprimeAnalyser::selectReconstructedJets()
{
    // _rlm = _rlm.Define("sel_partgentopHiggs",::isPfromtopHiggs,{"GenPart_pdgId","GenPart_genPartIdxMother","Selected_lepton_genPartIdx"})
    //            .Define("PartFromtopHiggs_eta","GenPart_eta[sel_partgentopHiggs]")
    //            .Define("PartFromtopHiggs_phi","GenPart_phi[sel_partgentopHiggs]");

    // _rlm = _rlm.Define("sel_jgentopHiggs",::isdR04,{"GenJet_eta","PartFromtopHiggs_eta","GenJet_phi","PartFromtopHiggs_phi"})
    //            .Define("JFromtopHiggs_pt","GenJet_pt[sel_jgentopHiggs]");

    // _rlm = _rlm.Define("sel_jetgentopHiggs",::isJetFromgJet,{"Selected_clean_jet_pt","GenJet_pt","JFromtopHiggs_pt","Selected_clean_jet_genJetIdx"})//,"Selected_clean_jet_eta","Selected_clean_jet_phi","Selected_clean_jet_mass","Selected_clean_jet_jetId","GenJet_eta","GenJet_phi","GenJet_mass"})
    //            .Define("JetFromtopHiggs_pt","Selected_clean_jet_pt[sel_jetgentopHiggs]")
    //            .Define("JetFromtopHiggs_leading_pt","(JetFromtopHiggs_pt.size()>0) ? JetFromtopHiggs_pt[0]:-100")
    //            .Define("JetFromtopHiggs_subleading_pt","(JetFromtopHiggs_pt.size()>1) ? JetFromtopHiggs_pt[1]:-100")
    //            .Define("JetFromtopHiggs_subsubleading_pt","(JetFromtopHiggs_pt.size()>2) ? JetFromtopHiggs_pt[2]:-100")          
    //            .Define("JetFromtopHiggs_Ht","Sum(JetFromtopHiggs_pt)")
    //            .Define("JetFromtopHiggs_eta","Selected_clean_jet_eta[sel_jetgentopHiggs]")
    //            .Define("JetFromtopHiggs_phi","Selected_clean_jet_phi[sel_jetgentopHiggs]")
    //            .Define("JetFromtopHiggs_mass","Selected_clean_jet_mass[sel_jetgentopHiggs]")
    //            .Define("JetFromtopHiggs_transverse_energy","sqrt(JetFromtopHiggs_pt*JetFromtopHiggs_pt + JetFromtopHiggs_mass*JetFromtopHiggs_mass)")
    //            .Define("JetFromtopHiggs_number","int(JetFromtopHiggs_pt.size())")
    //            .Define("JetFromtopHiggs_btagDeepB","Selected_clean_jet_btagDeepB[sel_jetgentopHiggs]")
    //            .Define("JetFromtopHiggs_btagDeepFlavB","Selected_clean_jet_btagDeepFlavB[sel_jetgentopHiggs]")
    //            .Define("JetFromtopHiggs_genJetIdx","Selected_clean_jet_genJetIdx[sel_jetgentopHiggs]")
    //            .Define("JetFromtopHiggs_partonFlavour","Selected_clean_jet_partonFlavour[sel_jetgentopHiggs]")
    //            .Define("JetFromtopHiggs_hadronFlavour","Selected_clean_jet_hadronFlavour[sel_jetgentopHiggs]")
    //            .Define("p4_JetFromtopHiggs",::generate_4vec,{"JetFromtopHiggs_pt","JetFromtopHiggs_eta","JetFromtopHiggs_phi","JetFromtopHiggs_mass"})
    //            .Define("Rp4_JetFromtopHiggs",::generate_4Rvec,{"JetFromtopHiggs_pt","JetFromtopHiggs_eta","JetFromtopHiggs_phi","JetFromtopHiggs_mass"})
    //            .Define("JetFromtopHiggs_energy","ROOT::VecOps::Map(Rp4_JetFromtopHiggs, FourVecEnergy)")
    //            .Define("JetFromtopHiggs_px","ROOT::VecOps::Map(Rp4_JetFromtopHiggs, FourVecPx)")
    //            .Define("JetFromtopHiggs_py","ROOT::VecOps::Map(Rp4_JetFromtopHiggs, FourVecPy)")
    //            .Define("JetFromtopHiggs_pz","ROOT::VecOps::Map(Rp4_JetFromtopHiggs, FourVecPz)");

    // _rlm = _rlm.Define("sel_bjetgentopHiggs",::isJetFromgJet,{"Selected_clean_bjet_pt","GenJet_pt","JFromtopHiggs_pt","Selected_clean_bjet_genJetIdx"})//,"Selected_clean_bjet_eta","Selected_clean_bjet_phi","Selected_clean_bjet_mass","Selected_clean_bjet_jetId","GenJet_eta","GenJet_phi","GenJet_mass"})
    //            .Define("bJetFromtopHiggs_pt","Selected_clean_bjet_pt[sel_bjetgentopHiggs]")
    //            .Define("bJetFromtopHiggs_leading_pt","(bJetFromtopHiggs_pt.size()>0) ? bJetFromtopHiggs_pt[0]:-100")
    //            .Define("bJetFromtopHiggs_subleading_pt","(bJetFromtopHiggs_pt.size()>1) ? bJetFromtopHiggs_pt[1]:-100")
    //            .Define("bJetFromtopHiggs_subsubleading_pt","(bJetFromtopHiggs_pt.size()>2) ? bJetFromtopHiggs_pt[2]:-100")
    //            .Define("bJetFromtopHiggs_Ht","Sum(bJetFromtopHiggs_pt)")
    //            .Define("bJetFromtopHiggs_eta","Selected_clean_bjet_eta[sel_bjetgentopHiggs]")
    //            .Define("bJetFromtopHiggs_phi","Selected_clean_bjet_phi[sel_bjetgentopHiggs]")
    //            .Define("bJetFromtopHiggs_mass","Selected_clean_bjet_mass[sel_bjetgentopHiggs]")
    //            .Define("bJetFromtopHiggs_transverse_energy","sqrt(bJetFromtopHiggs_pt*bJetFromtopHiggs_pt + bJetFromtopHiggs_mass*bJetFromtopHiggs_mass)")
    //            .Define("bJetFromtopHiggs_number","int(bJetFromtopHiggs_pt.size())")
    //            .Define("bJetFromtopHiggs_btagDeepB","Selected_clean_bjet_btagDeepB[sel_bjetgentopHiggs]")
    //            .Define("bJetFromtopHiggs_btagDeepFlavB","Selected_clean_bjet_btagDeepFlavB[sel_bjetgentopHiggs]")
    //            .Define("bJetFromtopHiggs_genJetIdx","Selected_clean_bjet_genJetIdx[sel_bjetgentopHiggs]")
    //            .Define("bJetFromtopHiggs_partonFlavour","Selected_clean_bjet_partonFlavour[sel_bjetgentopHiggs]")
    //            .Define("bJetFromtopHiggs_hadronFlavour","Selected_clean_bjet_hadronFlavour[sel_bjetgentopHiggs]")
    //            .Define("p4_bJetFromtopHiggs",::generate_4vec,{"bJetFromtopHiggs_pt","bJetFromtopHiggs_eta","bJetFromtopHiggs_phi","bJetFromtopHiggs_mass"})
    //            .Define("Rp4_bJetFromtopHiggs",::generate_4Rvec,{"bJetFromtopHiggs_pt","bJetFromtopHiggs_eta","bJetFromtopHiggs_phi","bJetFromtopHiggs_mass"})
    //            .Define("bJetFromtopHiggs_energy","ROOT::VecOps::Map(Rp4_bJetFromtopHiggs, FourVecEnergy)")
    //            .Define("bJetFromtopHiggs_px","ROOT::VecOps::Map(Rp4_bJetFromtopHiggs, FourVecPx)")
    //            .Define("bJetFromtopHiggs_py","ROOT::VecOps::Map(Rp4_bJetFromtopHiggs, FourVecPy)")
    //            .Define("bJetFromtopHiggs_pz","ROOT::VecOps::Map(Rp4_bJetFromtopHiggs, FourVecPz)");

    // _rlm = _rlm.Define("sel_partgenHiggs",::isPfromHiggs,{"GenPart_pdgId","GenPart_genPartIdxMother","Selected_lepton_genPartIdx"})
    //            .Define("PartFromHiggs_eta","GenPart_eta[sel_partgenHiggs]")
    //            .Define("PartFromHiggs_phi","GenPart_phi[sel_partgenHiggs]");

    // _rlm = _rlm.Define("sel_jgenHiggs",::isdR04,{"GenJet_eta","PartFromHiggs_eta","GenJet_phi","PartFromHiggs_phi"})
    //            .Define("JFromHiggs_pt","GenJet_pt[sel_jgenHiggs]");

    // _rlm = _rlm.Define("sel_jetgenHiggs",::isJetFromgJet,{"Selected_clean_jet_pt","GenJet_pt","JFromHiggs_pt","Selected_clean_jet_genJetIdx"})//,"Selected_clean_jet_eta","Selected_clean_jet_phi","Selected_clean_jet_mass","Selected_clean_jet_jetId","GenJet_eta","GenJet_phi","GenJet_mass"})
    //            .Define("JetFromHiggs_pt","Selected_clean_jet_pt[sel_jetgenHiggs]")
    //            .Define("JetFromHiggs_leading_pt","(JetFromHiggs_pt.size()>0) ? JetFromHiggs_pt[0]:-100")
    //            .Define("JetFromHiggs_subleading_pt","(JetFromHiggs_pt.size()>1) ? JetFromHiggs_pt[1]:-100")
    //            .Define("JetFromHiggs_subsubleading_pt","(JetFromHiggs_pt.size()>2) ? JetFromHiggs_pt[2]:-100")
    //            .Define("JetFromHiggs_Ht","Sum(JetFromHiggs_pt)")
    //            .Define("JetFromHiggs_eta","Selected_clean_jet_eta[sel_jetgenHiggs]")
    //            .Define("JetFromHiggs_phi","Selected_clean_jet_phi[sel_jetgenHiggs]")
    //            .Define("JetFromHiggs_mass","Selected_clean_jet_mass[sel_jetgenHiggs]")
    //            .Define("JetFromHiggs_transverse_energy","sqrt(JetFromHiggs_pt*JetFromHiggs_pt + JetFromHiggs_mass*JetFromHiggs_mass)")
    //            .Define("JetFromHiggs_number","int(JetFromHiggs_pt.size())")
    //            .Define("JetFromHiggs_btagDeepB","Selected_clean_jet_btagDeepB[sel_jetgenHiggs]")
    //            .Define("JetFromHiggs_btagDeepFlavB","Selected_clean_jet_btagDeepFlavB[sel_jetgenHiggs]")
    //            .Define("JetFromHiggs_genJetIdx","Selected_clean_jet_genJetIdx[sel_jetgenHiggs]")
    //            .Define("JetFromHiggs_partonFlavour","Selected_clean_jet_partonFlavour[sel_jetgenHiggs]")
    //            .Define("JetFromHiggs_hadronFlavour","Selected_clean_jet_hadronFlavour[sel_jetgenHiggs]")
    //            .Define("p4_JetFromHiggs",::generate_4vec,{"JetFromHiggs_pt","JetFromHiggs_eta","JetFromHiggs_phi","JetFromHiggs_mass"})
    //            .Define("Rp4_JetFromHiggs",::generate_4Rvec,{"JetFromHiggs_pt","JetFromHiggs_eta","JetFromHiggs_phi","JetFromHiggs_mass"})
    //            .Define("JetFromHiggs_energy","ROOT::VecOps::Map(Rp4_JetFromHiggs, FourVecEnergy)")
    //            .Define("JetFromHiggs_px","ROOT::VecOps::Map(Rp4_JetFromHiggs, FourVecPx)")
    //            .Define("JetFromHiggs_py","ROOT::VecOps::Map(Rp4_JetFromHiggs, FourVecPy)")
    //            .Define("JetFromHiggs_pz","ROOT::VecOps::Map(Rp4_JetFromHiggs, FourVecPz)");

    // _rlm = _rlm.Define("sel_partgentop",::isPfromtop,{"GenPart_pdgId","GenPart_genPartIdxMother","Selected_lepton_genPartIdx"})
    //            .Define("PartFromtop_eta","GenPart_eta[sel_partgentop]")
    //            .Define("PartFromtop_phi","GenPart_phi[sel_partgentop]");

    // _rlm = _rlm.Define("sel_jgentop",::isdR04,{"GenJet_eta","PartFromtop_eta","GenJet_phi","PartFromtop_phi"})
    //            .Define("JFromtop_pt","GenJet_pt[sel_jgentop]");

    // _rlm = _rlm.Define("sel_bjetgentop",::isJetFromgJet,{"Selected_clean_bjet_pt","GenJet_pt","JFromtop_pt","Selected_clean_bjet_genJetIdx"})//,"Selected_clean_bjet_eta","Selected_clean_bjet_phi","Selected_clean_bjet_mass","Selected_clean_bjet_jetId","GenJet_eta","GenJet_phi","GenJet_mass"})
    //            .Define("bJetFromtop_pt","Selected_clean_bjet_pt[sel_bjetgentop]")
    //            .Define("bJetFromtop_leading_pt","(bJetFromtop_pt.size()>0) ? bJetFromtop_pt[0]:-100")
    //            .Define("bJetFromtop_subleading_pt","(bJetFromtop_pt.size()>1) ? bJetFromtop_pt[1]:-100")
    //            .Define("bJetFromtop_subsubleading_pt","(bJetFromtop_pt.size()>2) ? bJetFromtop_pt[2]:-100")
    //            .Define("bJetFromtop_Ht","Sum(bJetFromtop_pt)")
    //            .Define("bJetFromtop_eta","Selected_clean_bjet_eta[sel_bjetgentop]")
    //            .Define("bJetFromtop_phi","Selected_clean_bjet_phi[sel_bjetgentop]")
    //            .Define("bJetFromtop_mass","Selected_clean_bjet_mass[sel_bjetgentop]")
    //            .Define("bJetFromtop_transverse_energy","sqrt(bJetFromtop_pt*bJetFromtop_pt + bJetFromtop_mass*bJetFromtop_mass)")
    //            .Define("bJetFromtop_number","int(bJetFromtop_pt.size())")
    //            .Define("bJetFromtop_btagDeepB","Selected_clean_bjet_btagDeepB[sel_bjetgentop]")
    //            .Define("bJetFromtop_btagDeepFlavB","Selected_clean_bjet_btagDeepFlavB[sel_bjetgentop]")
    //            .Define("bJetFromtop_genJetIdx","Selected_clean_bjet_genJetIdx[sel_bjetgentop]")
    //            .Define("bJetFromtop_partonFlavour","Selected_clean_bjet_partonFlavour[sel_bjetgentop]")
    //            .Define("bJetFromtop_hadronFlavour","Selected_clean_bjet_hadronFlavour[sel_bjetgentop]")
    //            .Define("p4_bJetFromtop",::generate_4vec,{"bJetFromtop_pt","bJetFromtop_eta","bJetFromtop_phi","bJetFromtop_mass"})
    //            .Define("Rp4_bJetFromtop",::generate_4Rvec,{"bJetFromtop_pt","bJetFromtop_eta","bJetFromtop_phi","bJetFromtop_mass"})
    //            .Define("bJetFromtop_energy","ROOT::VecOps::Map(Rp4_bJetFromtop, FourVecEnergy)")
    //            .Define("bJetFromtop_px","ROOT::VecOps::Map(Rp4_bJetFromtop, FourVecPx)")
    //            .Define("bJetFromtop_py","ROOT::VecOps::Map(Rp4_bJetFromtop, FourVecPy)")
    //            .Define("bJetFromtop_pz","ROOT::VecOps::Map(Rp4_bJetFromtop, FourVecPz)");

    // _rlm = _rlm.Define("sel_partgentop",::isPfromtoponly,{"GenPart_pdgId","GenPart_genPartIdxMother","Selected_lepton_genPartIdx"})
    //            .Define("PartFromtop_eta","GenPart_eta[sel_partgentop]")
    //            .Define("PartFromtop_phi","GenPart_phi[sel_partgentop]");

    // _rlm = _rlm.Define("sel_jgentop",::isdR04,{"GenJet_eta","PartFromtop_eta","GenJet_phi","PartFromtop_phi"})
    //            .Define("JFromtop_pt","GenJet_pt[sel_jgentop]");

    // _rlm = _rlm.Define("sel_bjetgentop",::isJetFromgJet,{"Selected_clean_bjet_pt","GenJet_pt","JFromtop_pt","Selected_clean_bjet_genJetIdx"})//,"Selected_clean_bjet_eta","Selected_clean_bjet_phi","Selected_clean_bjet_mass","Selected_clean_bjet_jetId","GenJet_eta","GenJet_phi","GenJet_mass"})
    //            .Define("bJetFromtop_pt","Selected_clean_bjet_pt[sel_bjetgentop]")
    //            .Define("bJetFromtop_leading_pt","(bJetFromtop_pt.size()>0) ? bJetFromtop_pt[0]:-100")
    //            .Define("bJetFromtop_subleading_pt","(bJetFromtop_pt.size()>1) ? bJetFromtop_pt[1]:-100")
    //            .Define("bJetFromtop_subsubleading_pt","(bJetFromtop_pt.size()>2) ? bJetFromtop_pt[2]:-100")
    //            .Define("bJetFromtop_Ht","Sum(bJetFromtop_pt)")
    //            .Define("bJetFromtop_eta","Selected_clean_bjet_eta[sel_bjetgentop]")
    //            .Define("bJetFromtop_phi","Selected_clean_bjet_phi[sel_bjetgentop]")
    //            .Define("bJetFromtop_mass","Selected_clean_bjet_mass[sel_bjetgentop]")
    //            .Define("bJetFromtop_transverse_energy","sqrt(bJetFromtop_pt*bJetFromtop_pt + bJetFromtop_mass*bJetFromtop_mass)")
    //            .Define("bJetFromtop_number","int(bJetFromtop_pt.size())")
    //            .Define("bJetFromtop_btagDeepB","Selected_clean_bjet_btagDeepB[sel_bjetgentop]")
    //            .Define("bJetFromtop_btagDeepFlavB","Selected_clean_bjet_btagDeepFlavB[sel_bjetgentop]")
    //            .Define("bJetFromtop_genJetIdx","Selected_clean_bjet_genJetIdx[sel_bjetgentop]")
    //            .Define("bJetFromtop_partonFlavour","Selected_clean_bjet_partonFlavour[sel_bjetgentop]")
    //            .Define("bJetFromtop_hadronFlavour","Selected_clean_bjet_hadronFlavour[sel_bjetgentop]")
    //            .Define("p4_bJetFromtop",::generate_4vec,{"bJetFromtop_pt","bJetFromtop_eta","bJetFromtop_phi","bJetFromtop_mass"})
    //            .Define("p4_bJetFromtop_first","p4_bJetFromtop[0]")
    //            .Define("bJetFromtop_first_pt","p4_bJetFromtop_first.Pt()")
    //            .Define("bJetFromtop_first_eta","p4_bJetFromtop_first.Eta()")
    //            .Define("bJetFromtop_first_phi","p4_bJetFromtop_first.Phi()")
    //            .Define("bJetFromtop_first_mass","p4_bJetFromtop_first.M()")
    //            .Define("bJetFromtop_first_energy","p4_bJetFromtop_first.E()")
    //            .Define("bJetFromtop_first_px","p4_bJetFromtop_first.Px()")
    //            .Define("bJetFromtop_first_py","p4_bJetFromtop_first.Py()")
    //            .Define("bJetFromtop_first_pz","p4_bJetFromtop_first.Pz()")
    //            .Define("Rp4_bJetFromtop_first",::generate_4Rvec_2,{"bJetFromtop_first_pt","bJetFromtop_first_eta","bJetFromtop_first_phi","bJetFromtop_first_mass"});

    // _rlm = _rlm.Define("GenJets_pt","GenJet_pt")
    //            .Define("GenJets_eta","GenJet_eta")
    //            .Define("GenJets_phi","GenJet_phi")
    //            .Define("GenJets_mass","GenJet_mass")
    //            .Define("GenJets_hadronFlavour","GenJet_hadronFlavour")
    //            .Define("GenJets_partonFlavour","GenJet_partonFlavour")
    //            .Define("GenJets_light","abs(GenJets_partonFlavour) < 5")
    //            .Define("GenJets_light_pt","GenJet_pt[GenJets_light]")
    //            .Define("GenJets_light_eta","GenJet_eta[GenJets_light]")
    //            .Define("GenJets_light_phi","GenJet_phi[GenJets_light]")
    //            .Define("GenJets_light_mass","GenJet_mass[GenJets_light]")
    //            .Define("GenJets_light_hadronFlavour","GenJet_hadronFlavour[GenJets_light]")
    //            .Define("GenJets_light_partonFlavour","GenJet_partonFlavour[GenJets_light]");

}

void TprimeAnalyser::selectMET()
{
    if(debug){
        cout<< "================================//================================="<<endl;
        cout<< "Line : "<< __LINE__<<" Function : "<<__FUNCTION__<<endl;
        cout<< "================================//================================="<<endl;
    }

//   _rlm = _rlm.Define("goodMET","MET_pt_corr > 0")
//              .Define("Selected_MET_pt","MET_pt_corr[goodMET]")
//              .Define("Selected_MET_sum_pt_unclustered","MET_sumPtUnclustered[goodMET]")
//              .Define("Selected_MET_phi","MET_phi_corr[goodMET]")
//              .Define("Selected_MET_sum_transverse_energy","MET_sumEt[goodMET]");

    _rlm = _rlm.Define("MET_px","MET_pt_corr*cos(MET_phi_corr)")
               .Define("MET_py","MET_pt_corr*sin(MET_phi_corr)");

}

void TprimeAnalyser::calculateEvWeight(std::string year, std::string process, const std::vector<std::string>& weights)
{
    int _case = 1;
    string year2 = year;
    if(year =="2016preVFP" || year == "2016postVFP")
    {
        year2 = "2016";
    }
    std::vector<std::string> Jets_vars_names = {"Selected_clean_jet_hadronFlavour", "Selected_clean_jet_eta", "Selected_clean_jet_pt", "Selected_clean_jet_btagDeepFlavB"};
    std::vector<std::string> Jets_loosett1l_vars_names = {"Selected_clean_jet_loosett1l_hadronFlavour", "Selected_clean_jet_loosett1l_eta", "Selected_clean_jet_loosett1l_pt", "Selected_clean_jet_loosett1l_btagDeepFlavB"};
    string output_btag_column_name = "btag_SF_";
    string output_btag_loosett1l_column_name = "btag_loosett1l_SF_";
    if(year == "2016preVFP")
    {
        _rlm = calculateBTagSF(_rlm, Jets_vars_names, _case, 0.2598, "M", output_btag_column_name);
        _rlm = calculateBTagSF(_rlm, Jets_loosett1l_vars_names, _case, 0.2598, "M", output_btag_loosett1l_column_name);
    }
    if(year == "2016postVFP")
    {
        _rlm = calculateBTagSF(_rlm, Jets_vars_names, _case, 0.2489, "M", output_btag_column_name);
        _rlm = calculateBTagSF(_rlm, Jets_loosett1l_vars_names, _case, 0.2489, "M", output_btag_loosett1l_column_name);
    }
    if(year == "2017")
    {
        _rlm = calculateBTagSF(_rlm, Jets_vars_names, _case, 0.3040, "M", output_btag_column_name);
        _rlm = calculateBTagSF(_rlm, Jets_loosett1l_vars_names, _case, 0.3040, "M", output_btag_loosett1l_column_name);
    }
    if(year == "2018")
    {
        _rlm = calculateBTagSF(_rlm, Jets_vars_names, _case, 0.2783, "M", output_btag_column_name);
        _rlm = calculateBTagSF(_rlm, Jets_loosett1l_vars_names, _case, 0.2783, "M", output_btag_loosett1l_column_name);
    }
    std::vector<std::string> Jets_pileupjetid_vars_names = {"Selected_clean_jet_eta", "Selected_clean_jet_pt", "Selected_clean_jet_puId"};
    std::vector<std::string> Jets_pileupjetid_loosett1l_vars_names = {"Selected_clean_jet_loosett1l_eta", "Selected_clean_jet_loosett1l_pt", "Selected_clean_jet_loosett1l_puId"};
    string output_pileupjetid_column_name = "pileupjetid_SF_";
    string output_pileupjetid_loosett1l_column_name = "pileupjetid_loosett1l_SF_";
    _rlm = calculatePileupJetIDSF(_rlm, Jets_pileupjetid_vars_names, year2, "T", output_pileupjetid_column_name);
    _rlm = calculatePileupJetIDSF(_rlm, Jets_pileupjetid_loosett1l_vars_names, year2, "T", output_pileupjetid_loosett1l_column_name);

    std::vector<std::string> Muon_vars_names;
    Muon_vars_names = {"Selected_muon_eta", "Selected_muon_pt","Selected_lepton1_pdgId","Selected_lepton_leading_pt"};
    string output_mu_column_name = "muon_SF_";
    _rlm = calculateMuSF(_rlm, Muon_vars_names, output_mu_column_name);
    std::vector<std::string> Muon_loosett1l_vars_names;
    Muon_loosett1l_vars_names = {"Selected_muon_loosett1l_nontight_eta", "Selected_muon_loosett1l_nontight_pt","Selected_lepton1_loosett1l_pdgId","Selected_lepton_loosett1l_leading_pt"};
    string output_mu_loosett1l_column_name = "muon_loosett1l_SF_";
    _rlm = calculateMuSF_loosett1l(_rlm, Muon_loosett1l_vars_names, output_mu_loosett1l_column_name);

    std::vector<std::string> Electron_vars_names;
    Electron_vars_names = {"Selected_electron_eta", "Selected_electron_pt","Selected_lepton1_pdgId","Selected_lepton_leading_pt"};
    string output_ele_column_name = "ele_SF_";
    _rlm = calculateEleSF(_rlm, Electron_vars_names, output_ele_column_name);
    std::vector<std::string> Electron_loosett1l_vars_names;
    Electron_loosett1l_vars_names = {"Selected_electron_loosett1l_eta", "Selected_electron_loosett1l_pt","Selected_lepton1_loosett1l_pdgId","Selected_lepton_loosett1l_leading_pt"};
    string output_ele_loosett1l_column_name = "ele_loosett1l_SF_";
    _rlm = calculateEleSF_loosett1l(_rlm, Electron_loosett1l_vars_names, output_ele_loosett1l_column_name);
    _rlm = applyPrefiringWeight(_rlm);

    topPtReweight(process);
    _rlm = _rlm.Define("TopPtWeight_nom","TopPtWeight[0]")
               .Define("TopPtWeight_up","TopPtWeight[1]")
               .Define("TopPtWeight_down","TopPtWeight[2]");

    // Total event Weight
    _rlm = _rlm.Define("evWeight_wobtagSF_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * muon_loosett1l_SF_central * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * muon_loosett1l_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * muon_loosett1l_SF_central * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom")
               .Define("totbtagSF_loosett1l", "btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central")
               .Define("evWeight_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom")
               .Define("elec_hltUp_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1) ? evWeight_loosett1l : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1 && Selected_lepton1_loosett1l_pdgId == 11) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_SF_central * ele_loosett1l_SF_hlt_systup * ele_SF_hlt_systup * ele_loosett1l_SF_reco_sf * ele_SF_reco_sf * ele_loosett1l_SF_id_sf * ele_SF_id_sf * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0 && Selected_lepton1_loosett1l_pdgId == 11) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * ele_loosett1l_SF_hlt_systup * ele_SF_hlt_systup * ele_loosett1l_SF_reco_sf * ele_SF_reco_sf * ele_loosett1l_SF_id_sf * ele_SF_id_sf * TopPtWeight_nom : (Selected_lepton1_loosett1l_pdgId == 11) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * muon_SF_central * ele_loosett1l_SF_hlt_systup * ele_SF_hlt_systup * ele_loosett1l_SF_reco_sf * ele_SF_reco_sf * ele_loosett1l_SF_id_sf * ele_SF_id_sf * TopPtWeight_nom : evWeight_loosett1l")
               .Define("elec_hltDown_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1) ? evWeight_loosett1l : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1 && Selected_lepton1_loosett1l_pdgId == 11) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_SF_central * ele_loosett1l_SF_hlt_systdown * ele_SF_hlt_systdown * ele_loosett1l_SF_reco_sf * ele_SF_reco_sf * ele_loosett1l_SF_id_sf * ele_SF_id_sf * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0 && Selected_lepton1_loosett1l_pdgId == 11) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * ele_loosett1l_SF_hlt_systdown * ele_SF_hlt_systdown * ele_loosett1l_SF_reco_sf * ele_SF_reco_sf * ele_loosett1l_SF_id_sf * ele_SF_id_sf * TopPtWeight_nom : (Selected_lepton1_loosett1l_pdgId == 11) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * muon_SF_central * ele_loosett1l_SF_hlt_systdown * ele_SF_hlt_systdown * ele_loosett1l_SF_reco_sf * ele_SF_reco_sf * ele_loosett1l_SF_id_sf * ele_SF_id_sf * TopPtWeight_nom : evWeight_loosett1l")
               .Define("syst_elec_hltUp_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1) ? evWeight_loosett1l : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1 && Selected_lepton1_loosett1l_pdgId == 11) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_SF_central * (ele_loosett1l_SF_hlt_sf + ele_SF_hlt_syst) * (ele_SF_hlt_sf + ele_SF_hlt_syst) * ele_loosett1l_SF_reco_sf * ele_SF_reco_sf * ele_loosett1l_SF_id_sf * ele_SF_id_sf * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0 && Selected_lepton1_loosett1l_pdgId == 11) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * (ele_loosett1l_SF_hlt_sf + ele_SF_hlt_syst) * (ele_SF_hlt_sf + ele_SF_hlt_syst) * ele_loosett1l_SF_reco_sf * ele_SF_reco_sf * ele_loosett1l_SF_id_sf * ele_SF_id_sf * TopPtWeight_nom : (Selected_lepton1_loosett1l_pdgId == 11) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * muon_SF_central * (ele_loosett1l_SF_hlt_sf + ele_SF_hlt_syst) * (ele_SF_hlt_sf + ele_SF_hlt_syst) * ele_loosett1l_SF_reco_sf * ele_SF_reco_sf * ele_loosett1l_SF_id_sf * ele_SF_id_sf * TopPtWeight_nom : evWeight_loosett1l")
               .Define("syst_elec_hltDown_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1) ? evWeight_loosett1l : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1 && Selected_lepton1_loosett1l_pdgId == 11) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_SF_central * (ele_loosett1l_SF_hlt_sf - ele_SF_hlt_syst) * (ele_SF_hlt_sf - ele_SF_hlt_syst) * ele_loosett1l_SF_reco_sf * ele_SF_reco_sf * ele_loosett1l_SF_id_sf * ele_SF_id_sf * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0 && Selected_lepton1_loosett1l_pdgId == 11) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * (ele_loosett1l_SF_hlt_sf - ele_SF_hlt_syst) * (ele_SF_hlt_sf - ele_SF_hlt_syst) * ele_loosett1l_SF_reco_sf * ele_SF_reco_sf * ele_loosett1l_SF_id_sf * ele_SF_id_sf * TopPtWeight_nom : (Selected_lepton1_loosett1l_pdgId == 11) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * muon_SF_central * (ele_loosett1l_SF_hlt_sf - ele_SF_hlt_syst) * (ele_SF_hlt_sf - ele_SF_hlt_syst) * ele_loosett1l_SF_reco_sf * ele_SF_reco_sf * ele_loosett1l_SF_id_sf * ele_SF_id_sf * TopPtWeight_nom : evWeight_loosett1l")
               .Define("stat_elec_hlt_"+year2+"Up_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1) ? evWeight_loosett1l : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1 && Selected_lepton1_loosett1l_pdgId == 11) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_SF_central * (ele_loosett1l_SF_hlt_sf + ele_SF_hlt_stat) * (ele_SF_hlt_sf + ele_SF_hlt_stat) * ele_loosett1l_SF_reco_sf * ele_SF_reco_sf * ele_loosett1l_SF_id_sf * ele_SF_id_sf * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0 && Selected_lepton1_loosett1l_pdgId == 11) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * (ele_loosett1l_SF_hlt_sf + ele_SF_hlt_stat) * (ele_SF_hlt_sf + ele_SF_hlt_stat) * ele_loosett1l_SF_reco_sf * ele_SF_reco_sf * ele_loosett1l_SF_id_sf * ele_SF_id_sf * TopPtWeight_nom : (Selected_lepton1_loosett1l_pdgId == 11) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * muon_SF_central * (ele_loosett1l_SF_hlt_sf + ele_SF_hlt_stat) * (ele_SF_hlt_sf + ele_SF_hlt_stat) * ele_loosett1l_SF_reco_sf * ele_SF_reco_sf * ele_loosett1l_SF_id_sf * ele_SF_id_sf * TopPtWeight_nom : evWeight_loosett1l")
               .Define("stat_elec_hlt_"+year2+"Down_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1) ? evWeight_loosett1l : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1 && Selected_lepton1_loosett1l_pdgId == 11) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_SF_central * (ele_loosett1l_SF_hlt_sf - ele_SF_hlt_stat) * (ele_SF_hlt_sf - ele_SF_hlt_stat) * ele_loosett1l_SF_reco_sf * ele_SF_reco_sf * ele_loosett1l_SF_id_sf * ele_SF_id_sf * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0 && Selected_lepton1_loosett1l_pdgId == 11) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * (ele_loosett1l_SF_hlt_sf - ele_SF_hlt_stat) * (ele_SF_hlt_sf - ele_SF_hlt_stat) * ele_loosett1l_SF_reco_sf * ele_SF_reco_sf * ele_loosett1l_SF_id_sf * ele_SF_id_sf * TopPtWeight_nom : (Selected_lepton1_loosett1l_pdgId == 11) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * muon_SF_central * (ele_loosett1l_SF_hlt_sf - ele_SF_hlt_stat) * (ele_SF_hlt_sf - ele_SF_hlt_stat) * ele_loosett1l_SF_reco_sf * ele_SF_reco_sf * ele_loosett1l_SF_id_sf * ele_SF_id_sf * TopPtWeight_nom : evWeight_loosett1l")
               .Define("syst_elec_recoUp_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1) ? evWeight_loosett1l : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_SF_central * ele_loosett1l_SF_hlt_sf * ele_SF_hlt_sf * ele_loosett1l_SF_reco_sfup * ele_SF_reco_sfup * ele_loosett1l_SF_id_sf * ele_SF_id_sf * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * ele_loosett1l_SF_hlt_sf * ele_SF_hlt_sf * ele_loosett1l_SF_reco_sfup * ele_SF_reco_sfup * ele_loosett1l_SF_id_sf * ele_SF_id_sf * TopPtWeight_nom : pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * muon_SF_central * ele_loosett1l_SF_hlt_sf * ele_SF_hlt_sf * ele_loosett1l_SF_reco_sfup * ele_SF_reco_sfup * ele_loosett1l_SF_id_sf * ele_SF_id_sf * TopPtWeight_nom")
               .Define("syst_elec_recoDown_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1) ? evWeight_loosett1l : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_SF_central * ele_loosett1l_SF_hlt_sf * ele_SF_hlt_sf * ele_loosett1l_SF_reco_sfdown * ele_SF_reco_sfdown * ele_loosett1l_SF_id_sf * ele_SF_id_sf * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * ele_loosett1l_SF_hlt_sf * ele_SF_hlt_sf * ele_loosett1l_SF_reco_sfdown * ele_SF_reco_sfdown * ele_loosett1l_SF_id_sf * ele_SF_id_sf * TopPtWeight_nom : pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * muon_SF_central * ele_loosett1l_SF_hlt_sf * ele_SF_hlt_sf * ele_loosett1l_SF_reco_sfdown * ele_SF_reco_sfdown * ele_loosett1l_SF_id_sf * ele_SF_id_sf * TopPtWeight_nom")
               .Define("syst_elec_idUp_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1) ? evWeight_loosett1l : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_SF_central * ele_loosett1l_SF_hlt_sf * ele_SF_hlt_sf * ele_loosett1l_SF_reco_sf * ele_SF_reco_sf * ele_loosett1l_SF_id_sfup * ele_SF_id_sfup * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * ele_loosett1l_SF_hlt_sf * ele_SF_hlt_sf * ele_loosett1l_SF_reco_sf * ele_SF_reco_sf * ele_loosett1l_SF_id_sfup * ele_SF_id_sfup * TopPtWeight_nom : pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * muon_SF_central * ele_loosett1l_SF_hlt_sf * ele_SF_hlt_sf * ele_loosett1l_SF_reco_sf * ele_SF_reco_sf * ele_loosett1l_SF_id_sfup * ele_SF_id_sfup * TopPtWeight_nom")
               .Define("syst_elec_idDown_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1) ? evWeight_loosett1l : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_SF_central * ele_loosett1l_SF_hlt_sf * ele_SF_hlt_sf * ele_loosett1l_SF_reco_sf * ele_SF_reco_sf * ele_loosett1l_SF_id_sfdown * ele_SF_id_sfdown * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * ele_loosett1l_SF_hlt_sf * ele_SF_hlt_sf * ele_loosett1l_SF_reco_sf * ele_SF_reco_sf * ele_loosett1l_SF_id_sfdown * ele_SF_id_sfdown * TopPtWeight_nom : pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * muon_SF_central * ele_loosett1l_SF_hlt_sf * ele_SF_hlt_sf * ele_loosett1l_SF_reco_sf * ele_SF_reco_sf * ele_loosett1l_SF_id_sfdown * ele_SF_id_sfdown * TopPtWeight_nom")
               .Define("muon_hltUp_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1 && Selected_lepton1_loosett1l_pdgId == 13) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_hlt_systup * muon_SF_hlt_systup * muon_loosett1l_SF_reco_sf * muon_SF_reco_sf * muon_loosett1l_SF_id_sf * muon_SF_id_sf * muon_loosett1l_SF_iso_sf * muon_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1 && Selected_lepton1_loosett1l_pdgId == 13) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_SF_hlt_systup * muon_SF_reco_sf * muon_SF_id_sf * muon_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0 && Selected_lepton1_loosett1l_pdgId == 13) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_hlt_systup * muon_loosett1l_SF_reco_sf * muon_loosett1l_SF_id_sf * muon_loosett1l_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : evWeight_loosett1l")
               .Define("muon_hltDown_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1 && Selected_lepton1_loosett1l_pdgId == 13) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_hlt_systdown * muon_SF_hlt_systdown * muon_loosett1l_SF_reco_sf * muon_SF_reco_sf * muon_loosett1l_SF_id_sf * muon_SF_id_sf * muon_loosett1l_SF_iso_sf * muon_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1 && Selected_lepton1_loosett1l_pdgId == 13) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_SF_hlt_systdown * muon_SF_reco_sf * muon_SF_id_sf * muon_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0 && Selected_lepton1_loosett1l_pdgId == 13) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_hlt_systdown * muon_loosett1l_SF_reco_sf * muon_loosett1l_SF_id_sf * muon_loosett1l_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : evWeight_loosett1l")
               .Define("syst_muon_hltUp_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1 && Selected_lepton1_loosett1l_pdgId == 13) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * (muon_loosett1l_SF_hlt_sf + muon_loosett1l_SF_hlt_syst) * (muon_SF_hlt_sf + muon_SF_hlt_syst) * muon_loosett1l_SF_reco_sf * muon_SF_reco_sf * muon_loosett1l_SF_id_sf * muon_SF_id_sf * muon_loosett1l_SF_iso_sf * muon_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1 && Selected_lepton1_loosett1l_pdgId == 13) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * (muon_SF_hlt_sf + muon_SF_hlt_syst) * muon_SF_reco_sf * muon_SF_id_sf * muon_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0 && Selected_lepton1_loosett1l_pdgId == 13) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * (muon_loosett1l_SF_hlt_sf + muon_loosett1l_SF_hlt_syst) * muon_loosett1l_SF_reco_sf * muon_loosett1l_SF_id_sf * muon_loosett1l_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : evWeight_loosett1l")
               .Define("syst_muon_hltDown_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1 && Selected_lepton1_loosett1l_pdgId == 13) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * (muon_loosett1l_SF_hlt_sf - muon_loosett1l_SF_hlt_syst) * (muon_SF_hlt_sf - muon_SF_hlt_syst) * muon_loosett1l_SF_reco_sf * muon_SF_reco_sf * muon_loosett1l_SF_id_sf * muon_SF_id_sf * muon_loosett1l_SF_iso_sf * muon_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1 && Selected_lepton1_loosett1l_pdgId == 13) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * (muon_SF_hlt_sf - muon_SF_hlt_syst) * muon_SF_reco_sf * muon_SF_id_sf * muon_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0 && Selected_lepton1_loosett1l_pdgId == 13) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * (muon_loosett1l_SF_hlt_sf - muon_loosett1l_SF_hlt_syst) * muon_loosett1l_SF_reco_sf * muon_loosett1l_SF_id_sf * muon_loosett1l_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : evWeight_loosett1l")
               .Define("stat_muon_hlt_"+year2+"Up_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1 && Selected_lepton1_loosett1l_pdgId == 13) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * (muon_loosett1l_SF_hlt_sf + muon_loosett1l_SF_hlt_stat) * (muon_SF_hlt_sf + muon_SF_hlt_stat) * muon_loosett1l_SF_reco_sf * muon_SF_reco_sf * muon_loosett1l_SF_id_sf * muon_SF_id_sf * muon_loosett1l_SF_iso_sf * muon_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1 && Selected_lepton1_loosett1l_pdgId == 13) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * (muon_SF_hlt_sf + muon_SF_hlt_stat) * muon_SF_reco_sf * muon_SF_id_sf * muon_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0 && Selected_lepton1_loosett1l_pdgId == 13) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * (muon_loosett1l_SF_hlt_sf + muon_loosett1l_SF_hlt_stat) * muon_loosett1l_SF_reco_sf * muon_loosett1l_SF_id_sf * muon_loosett1l_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : evWeight_loosett1l")
               .Define("stat_muon_hlt_"+year2+"Down_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1 && Selected_lepton1_loosett1l_pdgId == 13) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * (muon_loosett1l_SF_hlt_sf - muon_loosett1l_SF_hlt_stat) * (muon_SF_hlt_sf - muon_SF_hlt_stat) * muon_loosett1l_SF_reco_sf * muon_SF_reco_sf * muon_loosett1l_SF_id_sf * muon_SF_id_sf * muon_loosett1l_SF_iso_sf * muon_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1 && Selected_lepton1_loosett1l_pdgId == 13) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * (muon_SF_hlt_sf - muon_SF_hlt_stat) * muon_SF_reco_sf * muon_SF_id_sf * muon_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0 && Selected_lepton1_loosett1l_pdgId == 13) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * (muon_loosett1l_SF_hlt_sf - muon_loosett1l_SF_hlt_stat) * muon_loosett1l_SF_reco_sf * muon_loosett1l_SF_id_sf * muon_loosett1l_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : evWeight_loosett1l")
               .Define("muon_recoUp_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_hlt_sf * muon_SF_hlt_sf * muon_loosett1l_SF_reco_systup * muon_SF_reco_systup * muon_loosett1l_SF_id_sf * muon_SF_id_sf * muon_loosett1l_SF_iso_sf * muon_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_systup * muon_SF_id_sf * muon_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_hlt_sf * muon_loosett1l_SF_reco_systup * muon_loosett1l_SF_id_sf * muon_loosett1l_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : evWeight_loosett1l")
               .Define("muon_recoDown_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_hlt_sf * muon_SF_hlt_sf * muon_loosett1l_SF_reco_systdown * muon_SF_reco_systdown * muon_loosett1l_SF_id_sf * muon_SF_id_sf * muon_loosett1l_SF_iso_sf * muon_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_systdown * muon_SF_id_sf * muon_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_hlt_sf * muon_loosett1l_SF_reco_systdown * muon_loosett1l_SF_id_sf * muon_loosett1l_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : evWeight_loosett1l")
               .Define("syst_muon_recoUp_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_hlt_sf * muon_SF_hlt_sf * (muon_loosett1l_SF_reco_sf + muon_loosett1l_SF_reco_syst) * (muon_SF_reco_sf + muon_SF_reco_syst) * muon_loosett1l_SF_id_sf * muon_SF_id_sf * muon_loosett1l_SF_iso_sf * muon_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_SF_hlt_sf * (muon_SF_reco_sf + muon_SF_reco_syst) * muon_SF_id_sf * muon_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_hlt_sf * (muon_loosett1l_SF_reco_sf + muon_loosett1l_SF_reco_syst) * muon_loosett1l_SF_id_sf * muon_loosett1l_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : evWeight_loosett1l")
               .Define("syst_muon_recoDown_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_hlt_sf * muon_SF_hlt_sf * (muon_loosett1l_SF_reco_sf - muon_loosett1l_SF_reco_syst) * (muon_SF_reco_sf - muon_SF_reco_syst) * muon_loosett1l_SF_id_sf * muon_SF_id_sf * muon_loosett1l_SF_iso_sf * muon_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_SF_hlt_sf * (muon_SF_reco_sf - muon_SF_reco_syst) * muon_SF_id_sf * muon_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_hlt_sf * (muon_loosett1l_SF_reco_sf - muon_loosett1l_SF_reco_syst) * muon_loosett1l_SF_id_sf * muon_loosett1l_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : evWeight_loosett1l")
               .Define("stat_muon_reco_"+year2+"Up_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_hlt_sf * muon_SF_hlt_sf * (muon_loosett1l_SF_reco_sf + muon_loosett1l_SF_reco_stat) * (muon_SF_reco_sf + muon_SF_reco_stat) * muon_loosett1l_SF_id_sf * muon_SF_id_sf * muon_loosett1l_SF_iso_sf * muon_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_SF_hlt_sf * (muon_SF_reco_sf + muon_SF_reco_stat) * muon_SF_id_sf * muon_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_hlt_sf * (muon_loosett1l_SF_reco_sf + muon_loosett1l_SF_reco_stat) * muon_loosett1l_SF_id_sf * muon_loosett1l_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : evWeight_loosett1l")
               .Define("stat_muon_reco_"+year2+"Down_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_hlt_sf * muon_SF_hlt_sf * (muon_loosett1l_SF_reco_sf - muon_loosett1l_SF_reco_stat) * (muon_SF_reco_sf - muon_SF_reco_stat) * muon_loosett1l_SF_id_sf * muon_SF_id_sf * muon_loosett1l_SF_iso_sf * muon_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_SF_hlt_sf * (muon_SF_reco_sf - muon_SF_reco_stat) * muon_SF_id_sf * muon_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_hlt_sf * (muon_loosett1l_SF_reco_sf - muon_loosett1l_SF_reco_stat) * muon_loosett1l_SF_id_sf * muon_loosett1l_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : evWeight_loosett1l")
               .Define("muon_idUp_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_hlt_sf * muon_SF_hlt_sf * muon_loosett1l_SF_reco_sf * muon_SF_reco_sf * muon_loosett1l_SF_id_systup * muon_SF_id_systup * muon_loosett1l_SF_iso_sf * muon_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_sf * muon_SF_id_systup * muon_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_hlt_sf * muon_loosett1l_SF_reco_sf * muon_loosett1l_SF_id_systup * muon_loosett1l_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : evWeight_loosett1l")
               .Define("muon_idDown_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_hlt_sf * muon_SF_hlt_sf * muon_loosett1l_SF_reco_sf * muon_SF_reco_sf * muon_loosett1l_SF_id_systdown * muon_SF_id_systdown * muon_loosett1l_SF_iso_sf * muon_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_sf * muon_SF_id_systdown * muon_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_hlt_sf * muon_loosett1l_SF_reco_sf * muon_loosett1l_SF_id_systdown * muon_loosett1l_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : evWeight_loosett1l")
               .Define("syst_muon_idUp_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_hlt_sf * muon_SF_hlt_sf * muon_loosett1l_SF_reco_sf * muon_SF_reco_sf * (muon_loosett1l_SF_id_sf + muon_loosett1l_SF_id_syst) * (muon_SF_id_sf + muon_SF_id_syst) * muon_loosett1l_SF_iso_sf * muon_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_sf * (muon_SF_id_sf + muon_SF_id_syst) * muon_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_hlt_sf * muon_loosett1l_SF_reco_sf * (muon_loosett1l_SF_id_sf + muon_loosett1l_SF_id_syst) * muon_loosett1l_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : evWeight_loosett1l")
               .Define("syst_muon_idDown_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_hlt_sf * muon_SF_hlt_sf * muon_loosett1l_SF_reco_sf * muon_SF_reco_sf * (muon_loosett1l_SF_id_sf - muon_loosett1l_SF_id_syst) * (muon_SF_id_sf - muon_SF_id_syst) * muon_loosett1l_SF_iso_sf * muon_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_sf * (muon_SF_id_sf - muon_SF_id_syst) * muon_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_hlt_sf * muon_loosett1l_SF_reco_sf * (muon_loosett1l_SF_id_sf - muon_loosett1l_SF_id_syst) * muon_loosett1l_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : evWeight_loosett1l")
               .Define("stat_muon_id_"+year2+"Up_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_hlt_sf * muon_SF_hlt_sf * muon_loosett1l_SF_reco_sf * muon_SF_reco_sf * (muon_loosett1l_SF_id_sf + muon_loosett1l_SF_id_stat) * (muon_SF_id_sf + muon_SF_id_stat) * muon_loosett1l_SF_iso_sf * muon_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_sf * (muon_SF_id_sf + muon_SF_id_stat) * muon_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_hlt_sf * muon_loosett1l_SF_reco_sf * (muon_loosett1l_SF_id_sf + muon_loosett1l_SF_id_stat) * muon_loosett1l_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : evWeight_loosett1l")
               .Define("stat_muon_id_"+year2+"Down_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_hlt_sf * muon_SF_hlt_sf * muon_loosett1l_SF_reco_sf * muon_SF_reco_sf * (muon_loosett1l_SF_id_sf - muon_loosett1l_SF_id_stat) * (muon_SF_id_sf - muon_SF_id_stat) * muon_loosett1l_SF_iso_sf * muon_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_sf * (muon_SF_id_sf - muon_SF_id_stat) * muon_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_hlt_sf * muon_loosett1l_SF_reco_sf * (muon_loosett1l_SF_id_sf - muon_loosett1l_SF_id_stat) * muon_loosett1l_SF_iso_sf * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : evWeight_loosett1l")
               .Define("muon_isoUp_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_hlt_sf * muon_SF_hlt_sf * muon_loosett1l_SF_reco_sf * muon_SF_reco_sf * muon_loosett1l_SF_id_sf * muon_SF_id_sf * muon_loosett1l_SF_iso_systup * muon_SF_iso_systup * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_sf * muon_SF_id_sf * muon_SF_iso_systup * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_hlt_sf * muon_loosett1l_SF_reco_sf * muon_loosett1l_SF_id_sf * muon_loosett1l_SF_iso_systup * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : evWeight_loosett1l")
               .Define("muon_isoDown_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_hlt_sf * muon_SF_hlt_sf * muon_loosett1l_SF_reco_sf * muon_SF_reco_sf * muon_loosett1l_SF_id_sf * muon_SF_id_sf * muon_loosett1l_SF_iso_systdown * muon_SF_iso_systdown * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_sf * muon_SF_id_sf * muon_SF_iso_systdown * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_hlt_sf * muon_loosett1l_SF_reco_sf * muon_loosett1l_SF_id_sf * muon_loosett1l_SF_iso_systdown * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : evWeight_loosett1l")
               .Define("syst_muon_isoUp_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_hlt_sf * muon_SF_hlt_sf * muon_loosett1l_SF_reco_sf * muon_SF_reco_sf * muon_loosett1l_SF_id_sf * muon_SF_id_sf * (muon_loosett1l_SF_iso_sf + muon_loosett1l_SF_iso_syst) * (muon_SF_iso_sf + muon_SF_iso_syst) * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_sf * muon_SF_id_sf * (muon_SF_iso_sf + muon_SF_iso_syst) * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_hlt_sf * muon_loosett1l_SF_reco_sf * muon_loosett1l_SF_id_sf * (muon_loosett1l_SF_iso_sf + muon_loosett1l_SF_iso_syst) * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : evWeight_loosett1l")
               .Define("syst_muon_isoDown_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_hlt_sf * muon_SF_hlt_sf * muon_loosett1l_SF_reco_sf * muon_SF_reco_sf * muon_loosett1l_SF_id_sf * muon_SF_id_sf * (muon_loosett1l_SF_iso_sf - muon_loosett1l_SF_iso_syst) * (muon_SF_iso_sf - muon_SF_iso_syst) * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_sf * muon_SF_id_sf * (muon_SF_iso_sf - muon_SF_iso_syst) * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_hlt_sf * muon_loosett1l_SF_reco_sf * muon_loosett1l_SF_id_sf * (muon_loosett1l_SF_iso_sf - muon_loosett1l_SF_iso_syst) * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : evWeight_loosett1l")
               .Define("stat_muon_iso_"+year2+"Up_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_hlt_sf * muon_SF_hlt_sf * muon_loosett1l_SF_reco_sf * muon_SF_reco_sf * muon_loosett1l_SF_id_sf * muon_SF_id_sf * (muon_loosett1l_SF_iso_sf + muon_loosett1l_SF_iso_stat) * (muon_SF_iso_sf + muon_SF_iso_stat) * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_sf * muon_SF_id_sf * (muon_SF_iso_sf + muon_SF_iso_stat) * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_hlt_sf * muon_loosett1l_SF_reco_sf * muon_loosett1l_SF_id_sf * (muon_loosett1l_SF_iso_sf + muon_loosett1l_SF_iso_stat) * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : evWeight_loosett1l")
               .Define("stat_muon_iso_"+year2+"Down_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_hlt_sf * muon_SF_hlt_sf * muon_loosett1l_SF_reco_sf * muon_SF_reco_sf * muon_loosett1l_SF_id_sf * muon_SF_id_sf * (muon_loosett1l_SF_iso_sf - muon_loosett1l_SF_iso_stat) * (muon_SF_iso_sf - muon_SF_iso_stat) * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_sf * muon_SF_id_sf * (muon_SF_iso_sf - muon_SF_iso_stat) * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_hlt_sf * muon_loosett1l_SF_reco_sf * muon_loosett1l_SF_id_sf * (muon_loosett1l_SF_iso_sf - muon_loosett1l_SF_iso_stat) * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : evWeight_loosett1l")
               .Define("syst_puUp_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1) ? genWeight * puWeight_plus * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1) ? genWeight * puWeight_plus * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0) ? genWeight * puWeight_plus * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : genWeight * puWeight_plus * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom")
               .Define("syst_puDown_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1) ? genWeight * puWeight_minus * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1) ? genWeight * puWeight_minus * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0) ? genWeight * puWeight_minus * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : genWeight * puWeight_minus * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom")
               .Define("syst_b_correlatedUp_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_up_correlated * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_up_correlated * btag_loosett1l_SF_lflav_central * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_up_correlated * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_up_correlated * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom")
               .Define("syst_b_correlatedDown_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_down_correlated * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_down_correlated * btag_loosett1l_SF_lflav_central * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_down_correlated * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_down_correlated * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom")
               .Define("syst_b_uncorrelated_"+year+"Up_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_up_uncorrelated * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_up_uncorrelated * btag_loosett1l_SF_lflav_central * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_up_uncorrelated * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_up_uncorrelated * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom")
               .Define("syst_b_uncorrelated_"+year+"Down_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_down_uncorrelated * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_down_uncorrelated * btag_loosett1l_SF_lflav_central * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_down_uncorrelated * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_down_uncorrelated * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom")
               .Define("syst_l_correlatedUp_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_up_correlated * muon_loosett1l_SF_central * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_up_correlated * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_up_correlated * muon_loosett1l_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_up_correlated * muon_loosett1l_SF_central * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom")
               .Define("syst_l_correlatedDown_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_down_correlated * muon_loosett1l_SF_central * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_down_correlated * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_down_correlated * muon_loosett1l_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_down_correlated * muon_loosett1l_SF_central * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom")
               .Define("syst_l_uncorrelated_"+year+"Up_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_up_uncorrelated * muon_loosett1l_SF_central * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_up_uncorrelated * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_up_uncorrelated * muon_loosett1l_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_up_uncorrelated * muon_loosett1l_SF_central * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom")
               .Define("syst_l_uncorrelated_"+year+"Down_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_down_uncorrelated * muon_loosett1l_SF_central * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_down_uncorrelated * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_down_uncorrelated * muon_loosett1l_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_down_uncorrelated * muon_loosett1l_SF_central * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom")
               .Define("syst_prefiringUp_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_up * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_up * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0) ? pugenWeight * prefiring_SF_up * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : pugenWeight * prefiring_SF_up * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom")
               .Define("syst_prefiringDown_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_down * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_down * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0) ? pugenWeight * prefiring_SF_down * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : pugenWeight * prefiring_SF_down * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom")
               .Define("syst_pileupjetidUp_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_up * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_up * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_up * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_up * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom")
               .Define("syst_pileupjetidDown_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_down * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_down * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_down * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom : pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_down * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_nom")
               .Define("syst_pt_topUp_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_up : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_up : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_up : pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_up")
               .Define("syst_pt_topDown_loosett1l", "(Selected_muon_loosett1l_number == 2 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_down : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 1) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_down : (Selected_muon_loosett1l_number == 1 && Selected_muon_number == 0) ? pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_down : pugenWeight * prefiring_SF_central * pileupjetid_loosett1l_SF_nom * btag_loosett1l_SF_bcflav_central * btag_loosett1l_SF_lflav_central * muon_loosett1l_SF_central * muon_SF_central * ele_loosett1l_SF_central * ele_SF_central * TopPtWeight_down");

    _rlm = _rlm.Define("evWeight_wobtagSF", "pugenWeight * prefiring_SF_central * pileupjetid_SF_nom * muon_SF_central * ele_SF_central * TopPtWeight_nom")
               .Define("totbtagSF", "btag_SF_bcflav_central * btag_SF_lflav_central")
               .Define("evWeight", "pugenWeight * prefiring_SF_central * pileupjetid_SF_nom * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_central * ele_SF_central * TopPtWeight_nom")
               .Define("elec_hltUp", "(Selected_electron_number != 0 && Selected_lepton1_pdgId == 11) ? pugenWeight * prefiring_SF_central * pileupjetid_SF_nom * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_central * ele_SF_hlt_systup * ele_SF_reco_sf * ele_SF_id_sf * TopPtWeight_nom:evWeight")
               .Define("elec_hltDown", "(Selected_electron_number != 0 && Selected_lepton1_pdgId == 11) ? pugenWeight * prefiring_SF_central * pileupjetid_SF_nom * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_central * ele_SF_hlt_systdown * ele_SF_reco_sf * ele_SF_id_sf * TopPtWeight_nom:evWeight")
               .Define("syst_elec_hltUp", "(Selected_electron_number != 0 && Selected_lepton1_pdgId == 11) ? pugenWeight * prefiring_SF_central * pileupjetid_SF_nom * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_central * (ele_SF_hlt_sf + ele_SF_hlt_syst) * ele_SF_reco_sf * ele_SF_id_sf * TopPtWeight_nom:evWeight")
               .Define("syst_elec_hltDown", "(Selected_electron_number != 0 && Selected_lepton1_pdgId == 11) ? pugenWeight * prefiring_SF_central * pileupjetid_SF_nom * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_central * (ele_SF_hlt_sf - ele_SF_hlt_syst) * ele_SF_reco_sf * ele_SF_id_sf * TopPtWeight_nom:evWeight")
               .Define("stat_elec_hlt_"+year2+"Up", "(Selected_electron_number != 0 && Selected_lepton1_pdgId == 11) ? pugenWeight * prefiring_SF_central * pileupjetid_SF_nom * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_central * (ele_SF_hlt_sf + ele_SF_hlt_stat) * ele_SF_reco_sf * ele_SF_id_sf * TopPtWeight_nom:evWeight")
               .Define("stat_elec_hlt_"+year2+"Down", "(Selected_electron_number != 0 && Selected_lepton1_pdgId == 11) ? pugenWeight * prefiring_SF_central * pileupjetid_SF_nom * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_central * (ele_SF_hlt_sf - ele_SF_hlt_stat) * ele_SF_reco_sf * ele_SF_id_sf * TopPtWeight_nom:evWeight")
               .Define("syst_elec_recoUp", "(Selected_electron_number != 0) ? pugenWeight * prefiring_SF_central * pileupjetid_SF_nom * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_central * ele_SF_hlt_sf * ele_SF_reco_sfup * ele_SF_id_sf * TopPtWeight_nom:evWeight")
               .Define("syst_elec_recoDown", "(Selected_electron_number != 0) ? pugenWeight * prefiring_SF_central * pileupjetid_SF_nom * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_central * ele_SF_hlt_sf * ele_SF_reco_sfdown * ele_SF_id_sf * TopPtWeight_nom:evWeight")
               .Define("syst_elec_idUp", "(Selected_electron_number != 0) ? pugenWeight * prefiring_SF_central * pileupjetid_SF_nom * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_central * ele_SF_hlt_sf * ele_SF_reco_sf * ele_SF_id_sfup * TopPtWeight_nom:evWeight")
               .Define("syst_elec_idDown", "(Selected_electron_number != 0) ? pugenWeight * prefiring_SF_central * pileupjetid_SF_nom * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_central * ele_SF_hlt_sf * ele_SF_reco_sf * ele_SF_id_sfdown * TopPtWeight_nom:evWeight")
               .Define("muon_hltUp", "(Selected_muon_number != 0 && Selected_lepton1_pdgId == 13) ? pugenWeight * prefiring_SF_central * pileupjetid_SF_nom * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_systup * muon_SF_reco_sf * muon_SF_id_sf * muon_SF_iso_sf * ele_SF_central * TopPtWeight_nom:evWeight")
               .Define("muon_hltDown", "(Selected_muon_number != 0 && Selected_lepton1_pdgId == 13) ? pugenWeight * prefiring_SF_central * pileupjetid_SF_nom * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_systdown * muon_SF_reco_sf * muon_SF_id_sf * muon_SF_iso_sf * ele_SF_central * TopPtWeight_nom:evWeight")
               .Define("syst_muon_hltUp", "(Selected_muon_number != 0 && Selected_lepton1_pdgId == 13) ? pugenWeight * prefiring_SF_central * pileupjetid_SF_nom * btag_SF_bcflav_central * btag_SF_lflav_central * (muon_SF_hlt_sf + muon_SF_hlt_syst) * muon_SF_reco_sf * muon_SF_id_sf * muon_SF_iso_sf * ele_SF_central * TopPtWeight_nom:evWeight")
               .Define("syst_muon_hltDown", "(Selected_muon_number != 0 && Selected_lepton1_pdgId == 13) ? pugenWeight * prefiring_SF_central * pileupjetid_SF_nom * btag_SF_bcflav_central * btag_SF_lflav_central * (muon_SF_hlt_sf - muon_SF_hlt_syst) * muon_SF_reco_sf * muon_SF_id_sf * muon_SF_iso_sf * ele_SF_central * TopPtWeight_nom:evWeight")
               .Define("stat_muon_hlt_"+year2+"Up", "(Selected_muon_number != 0 && Selected_lepton1_pdgId == 13) ? pugenWeight * prefiring_SF_central * pileupjetid_SF_nom * btag_SF_bcflav_central * btag_SF_lflav_central * (muon_SF_hlt_sf + muon_SF_hlt_stat) * muon_SF_reco_sf * muon_SF_id_sf * muon_SF_iso_sf * ele_SF_central * TopPtWeight_nom:evWeight")
               .Define("stat_muon_hlt_"+year2+"Down", "(Selected_muon_number != 0 && Selected_lepton1_pdgId == 13) ? pugenWeight * prefiring_SF_central * pileupjetid_SF_nom * btag_SF_bcflav_central * btag_SF_lflav_central * (muon_SF_hlt_sf - muon_SF_hlt_stat) * muon_SF_reco_sf * muon_SF_id_sf * muon_SF_iso_sf * ele_SF_central * TopPtWeight_nom:evWeight")
               .Define("muon_recoUp", "(Selected_muon_number != 0) ? pugenWeight * prefiring_SF_central * pileupjetid_SF_nom * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_systup * muon_SF_id_sf * muon_SF_iso_sf * ele_SF_central * TopPtWeight_nom:evWeight")
               .Define("muon_recoDown", "(Selected_muon_number != 0) ? pugenWeight * prefiring_SF_central * pileupjetid_SF_nom * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_systdown * muon_SF_id_sf * muon_SF_iso_sf * ele_SF_central * TopPtWeight_nom:evWeight")
               .Define("syst_muon_recoUp", "(Selected_muon_number != 0) ? pugenWeight * prefiring_SF_central * pileupjetid_SF_nom * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * (muon_SF_reco_sf + muon_SF_reco_syst) * muon_SF_id_sf * muon_SF_iso_sf * ele_SF_central * TopPtWeight_nom:evWeight")
               .Define("syst_muon_recoDown", "(Selected_muon_number != 0) ? pugenWeight * prefiring_SF_central * pileupjetid_SF_nom * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * (muon_SF_reco_sf - muon_SF_reco_syst) * muon_SF_id_sf * muon_SF_iso_sf * ele_SF_central * TopPtWeight_nom:evWeight")
               .Define("stat_muon_reco_"+year2+"Up", "(Selected_muon_number != 0) ? pugenWeight * prefiring_SF_central * pileupjetid_SF_nom * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * (muon_SF_reco_sf + muon_SF_reco_stat) * muon_SF_id_sf * muon_SF_iso_sf * ele_SF_central * TopPtWeight_nom:evWeight")
               .Define("stat_muon_reco_"+year2+"Down", "(Selected_muon_number != 0) ? pugenWeight * prefiring_SF_central * pileupjetid_SF_nom * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * (muon_SF_reco_sf - muon_SF_reco_stat) * muon_SF_id_sf * muon_SF_iso_sf * ele_SF_central * TopPtWeight_nom:evWeight")
               .Define("muon_idUp", "(Selected_muon_number != 0) ? pugenWeight * prefiring_SF_central * pileupjetid_SF_nom * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_sf * muon_SF_id_systup * muon_SF_iso_sf * ele_SF_central * TopPtWeight_nom:evWeight")
               .Define("muon_idDown", "(Selected_muon_number != 0) ? pugenWeight * prefiring_SF_central * pileupjetid_SF_nom * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_sf * muon_SF_id_systdown * muon_SF_iso_sf * ele_SF_central * TopPtWeight_nom:evWeight")
               .Define("syst_muon_idUp", "(Selected_muon_number != 0) ? pugenWeight * prefiring_SF_central * pileupjetid_SF_nom * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_sf * (muon_SF_id_sf + muon_SF_id_syst) * muon_SF_iso_sf * ele_SF_central * TopPtWeight_nom:evWeight")
               .Define("syst_muon_idDown", "(Selected_muon_number != 0) ? pugenWeight * prefiring_SF_central * pileupjetid_SF_nom * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_sf * (muon_SF_id_sf - muon_SF_id_syst) * muon_SF_iso_sf * ele_SF_central * TopPtWeight_nom:evWeight")
               .Define("stat_muon_id_"+year2+"Up", "(Selected_muon_number != 0) ? pugenWeight * prefiring_SF_central * pileupjetid_SF_nom * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_sf * (muon_SF_id_sf + muon_SF_id_stat) * muon_SF_iso_sf * ele_SF_central * TopPtWeight_nom:evWeight")
               .Define("stat_muon_id_"+year2+"Down", "(Selected_muon_number != 0) ? pugenWeight * prefiring_SF_central * pileupjetid_SF_nom * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_sf * (muon_SF_id_sf - muon_SF_id_stat) * muon_SF_iso_sf * ele_SF_central * TopPtWeight_nom:evWeight")
               .Define("muon_isoUp", "(Selected_muon_number != 0) ? pugenWeight * prefiring_SF_central * pileupjetid_SF_nom * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_sf * muon_SF_id_sf * muon_SF_iso_systup * ele_SF_central * TopPtWeight_nom:evWeight")
               .Define("muon_isoDown", "(Selected_muon_number != 0) ? pugenWeight * prefiring_SF_central * pileupjetid_SF_nom * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_sf * muon_SF_id_sf * muon_SF_iso_systdown * ele_SF_central * TopPtWeight_nom:evWeight")
               .Define("syst_muon_isoUp", "(Selected_muon_number != 0) ? pugenWeight * prefiring_SF_central * pileupjetid_SF_nom * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_sf * muon_SF_id_sf * (muon_SF_iso_sf + muon_SF_iso_syst) * ele_SF_central * TopPtWeight_nom:evWeight")
               .Define("syst_muon_isoDown", "(Selected_muon_number != 0) ? pugenWeight * prefiring_SF_central * pileupjetid_SF_nom * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_sf * muon_SF_id_sf * (muon_SF_iso_sf - muon_SF_iso_syst) * ele_SF_central * TopPtWeight_nom:evWeight")
               .Define("stat_muon_iso_"+year2+"Up", "(Selected_muon_number != 0) ? pugenWeight * prefiring_SF_central * pileupjetid_SF_nom * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_sf * muon_SF_id_sf * (muon_SF_iso_sf + muon_SF_iso_stat) * ele_SF_central * TopPtWeight_nom:evWeight")
               .Define("stat_muon_iso_"+year2+"Down", "(Selected_muon_number != 0) ? pugenWeight * prefiring_SF_central * pileupjetid_SF_nom * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_sf * muon_SF_id_sf * (muon_SF_iso_sf - muon_SF_iso_stat) * ele_SF_central * TopPtWeight_nom:evWeight")
               .Define("syst_puUp", "genWeight * puWeight_plus * prefiring_SF_central * pileupjetid_SF_nom * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_central * ele_SF_central * TopPtWeight_nom")
               .Define("syst_puDown", "genWeight * puWeight_minus * prefiring_SF_central * pileupjetid_SF_nom * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_central * ele_SF_central * TopPtWeight_nom")
               .Define("syst_b_correlatedUp", "pugenWeight * prefiring_SF_central * pileupjetid_SF_nom * btag_SF_bcflav_up_correlated * btag_SF_lflav_central * muon_SF_central * ele_SF_central * TopPtWeight_nom")
               .Define("syst_b_correlatedDown", "pugenWeight * prefiring_SF_central * pileupjetid_SF_nom * btag_SF_bcflav_down_correlated * btag_SF_lflav_central * muon_SF_central * ele_SF_central * TopPtWeight_nom")
               .Define("syst_b_uncorrelated_"+year+"Up", "pugenWeight * prefiring_SF_central * pileupjetid_SF_nom * btag_SF_bcflav_up_uncorrelated * btag_SF_lflav_central * muon_SF_central * ele_SF_central * TopPtWeight_nom")
               .Define("syst_b_uncorrelated_"+year+"Down", "pugenWeight * prefiring_SF_central * pileupjetid_SF_nom * btag_SF_bcflav_down_uncorrelated * btag_SF_lflav_central * muon_SF_central * ele_SF_central * TopPtWeight_nom")
               .Define("syst_l_correlatedUp", "pugenWeight * prefiring_SF_central * pileupjetid_SF_nom * btag_SF_bcflav_central * btag_SF_lflav_up_correlated * muon_SF_central * ele_SF_central * TopPtWeight_nom")
               .Define("syst_l_correlatedDown", "pugenWeight * prefiring_SF_central * pileupjetid_SF_nom * btag_SF_bcflav_central * btag_SF_lflav_down_correlated * muon_SF_central * ele_SF_central * TopPtWeight_nom")
               .Define("syst_l_uncorrelated_"+year+"Up", "pugenWeight * prefiring_SF_central * pileupjetid_SF_nom * btag_SF_bcflav_central * btag_SF_lflav_up_uncorrelated * muon_SF_central * ele_SF_central * TopPtWeight_nom")
               .Define("syst_l_uncorrelated_"+year+"Down", "pugenWeight * prefiring_SF_central * pileupjetid_SF_nom * btag_SF_bcflav_central * btag_SF_lflav_down_uncorrelated * muon_SF_central * ele_SF_central * TopPtWeight_nom")
               .Define("syst_prefiringUp", "pugenWeight * prefiring_SF_up * pileupjetid_SF_nom * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_central * ele_SF_central * TopPtWeight_nom")
               .Define("syst_prefiringDown", "pugenWeight * prefiring_SF_down * pileupjetid_SF_nom * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_central * ele_SF_central * TopPtWeight_nom")
               .Define("syst_pileupjetidUp", "pugenWeight * prefiring_SF_central * pileupjetid_SF_up * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_central * ele_SF_central * TopPtWeight_nom")
               .Define("syst_pileupjetidDown", "pugenWeight * prefiring_SF_central * pileupjetid_SF_down * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_central * ele_SF_central * TopPtWeight_nom")
               .Define("syst_pt_topUp", "pugenWeight * prefiring_SF_central * pileupjetid_SF_nom * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_central * ele_SF_central * TopPtWeight_up")
               .Define("syst_pt_topDown", "pugenWeight * prefiring_SF_central * pileupjetid_SF_nom * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_central * ele_SF_central * TopPtWeight_down");

    if(process != "ww_ds")
    {
        _rlm = addTheorySystematics(_rlm);
        _rlm = _rlm.Define("syst_mescaleDown_loosett1l", "evWeight_loosett1l * LHEScaleWeight[0]")
                   .Define("syst_renscaleDown_loosett1l", "evWeight_loosett1l * LHEScaleWeight[1]")
                   .Define("syst_facscaleDown_loosett1l", "evWeight_loosett1l * LHEScaleWeight[3]")
                   .Define("syst_facscaleUp_loosett1l", "evWeight_loosett1l * LHEScaleWeight[5]")
                   .Define("syst_renscaleUp_loosett1l", "evWeight_loosett1l * LHEScaleWeight[7]")
                   .Define("syst_mescaleUp_loosett1l", "evWeight_loosett1l * LHEScaleWeight[8]");

        _rlm = _rlm.Define("syst_mescaleDown", "evWeight * LHEScaleWeight[0]")
                   .Define("syst_renscaleDown", "evWeight * LHEScaleWeight[1]")
                   .Define("syst_facscaleDown", "evWeight * LHEScaleWeight[3]")
                   .Define("syst_facscaleUp", "evWeight * LHEScaleWeight[5]")
                   .Define("syst_renscaleUp", "evWeight * LHEScaleWeight[7]")
                   .Define("syst_mescaleUp", "evWeight * LHEScaleWeight[8]");

        for(int i = 1; i <= 102; i++)
        {
            _rlm = _rlm.Define("syst_pdf" + std::to_string(i) + "_loosett1l", "evWeight_loosett1l * LHEPdfWeight[" + std::to_string(i) + "]");
            _rlm = _rlm.Define("syst_pdf" + std::to_string(i), "evWeight * LHEPdfWeight[" + std::to_string(i) + "]");
        }

        _rlm = _rlm.Define("syst_pdfalphasUp_loosett1l", "evWeight_loosett1l * LHEPdfWeight[102]")
                   .Define("syst_pdfalphasDown_loosett1l", "evWeight_loosett1l * LHEPdfWeight[101]");

        _rlm = _rlm.Define("syst_pdfalphasUp", "evWeight * LHEPdfWeight[102]")
                   .Define("syst_pdfalphasDown", "evWeight * LHEPdfWeight[101]");
    }
    else
    {
        _rlm = _rlm.Define("syst_mescaleDown_loosett1l", "evWeight_loosett1l")
                   .Define("syst_renscaleDown_loosett1l", "evWeight_loosett1l")
                   .Define("syst_facscaleDown_loosett1l", "evWeight_loosett1l")
                   .Define("syst_facscaleUp_loosett1l", "evWeight_loosett1l")
                   .Define("syst_renscaleUp_loosett1l", "evWeight_loosett1l")
                   .Define("syst_mescaleUp_loosett1l", "evWeight_loosett1l");

        _rlm = _rlm.Define("syst_mescaleDown", "evWeight")
                   .Define("syst_renscaleDown", "evWeight")
                   .Define("syst_facscaleDown", "evWeight")
                   .Define("syst_facscaleUp", "evWeight")
                   .Define("syst_renscaleUp", "evWeight")
                   .Define("syst_mescaleUp", "evWeight");

        for(int i = 1; i <= 102; i++)
        {
            _rlm = _rlm.Define("syst_pdf" + std::to_string(i) + "_loosett1l", "evWeight_loosett1l");
	        _rlm = _rlm.Define("syst_pdf" + std::to_string(i), "evWeight");
        }

        _rlm = _rlm.Define("syst_pdfalphasUp_loosett1l", "evWeight_loosett1l")
                   .Define("syst_pdfalphasDown_loosett1l", "evWeight_loosett1l");

        _rlm = _rlm.Define("syst_pdfalphasUp", "evWeight")
                   .Define("syst_pdfalphasDown", "evWeight");
    }

    // PS: [0] is ISR=2 FSR=1; [1] is ISR=1 FSR=2; [2] is ISR=0.5 FSR=1; [3] is ISR=1 FSR=0.5;
    _rlm = _rlm.Define("syst_isrUp_loosett1l", "evWeight_loosett1l * PSWeight[0]")
               .Define("syst_fsrUp_loosett1l", "evWeight_loosett1l * PSWeight[1]")
               .Define("syst_isrDown_loosett1l", "evWeight_loosett1l * PSWeight[2]")
               .Define("syst_fsrDown_loosett1l", "evWeight_loosett1l * PSWeight[3]");

    _rlm = _rlm.Define("syst_isrUp", "evWeight * PSWeight[0]")
               .Define("syst_fsrUp", "evWeight * PSWeight[1]")
               .Define("syst_isrDown", "evWeight * PSWeight[2]")
               .Define("syst_fsrDown", "evWeight * PSWeight[3]");
    if(process.find("signal") == std::string::npos)
    {
        if(year2 == "2016")
        {
            _rlm = _rlm.Define("flip_evWeight_wobtagSF_loosett1l", "evWeight_wobtagSF_loosett1l*1.11")
                       .Define("flip_totbtagSF_loosett1l", "totbtagSF_loosett1l*1.11")
                       .Define("flip_evWeight_loosett1l", "evWeight_loosett1l*1.11")
                       .Define("flip_evWeight_wobtagSF", "evWeight_wobtagSF*1.11")
                       .Define("flip_totbtagSF", "totbtagSF*1.11")
                       .Define("flip_evWeight", "evWeight*1.11")
                       .Define("fake_evWeight_wobtagSF_loosett1l", "evWeight_wobtagSF_loosett1l")
                       .Define("fake_totbtagSF_loosett1l", "totbtagSF_loosett1l")
                       .Define("fake_evWeight_loosett1l", "evWeight_loosett1l")
                       .Define("fake_evWeight_wobtagSF", "evWeight_wobtagSF")
                       .Define("fake_totbtagSF", "totbtagSF")
                       .Define("fake_evWeight", "evWeight");

            for(const auto& weight : weights)
            {
                _rlm = _rlm.Define("flip_"+weight+"_loosett1l", weight + "_loosett1l*1.11")
                           .Define("flip_"+weight, weight + "*1.11")
                           .Define("fake_"+weight+"_loosett1l", weight)
                           .Define("fake_"+weight, weight);
            }
        }

        if(year2 == "2017")
        {
            _rlm = _rlm.Define("flip_evWeight_wobtagSF_loosett1l", "evWeight_wobtagSF_loosett1l*1.44")
                       .Define("flip_totbtagSF_loosett1l", "totbtagSF_loosett1l*1.44")
                       .Define("flip_evWeight_loosett1l", "evWeight_loosett1l*1.44")
                       .Define("flip_evWeight_wobtagSF", "evWeight_wobtagSF*1.44")
                       .Define("flip_totbtagSF", "totbtagSF*1.44")
                       .Define("flip_evWeight", "evWeight*1.44")
                       .Define("fake_evWeight_wobtagSF_loosett1l", "evWeight_wobtagSF_loosett1l")
                       .Define("fake_totbtagSF_loosett1l", "totbtagSF_loosett1l")
                       .Define("fake_evWeight_loosett1l", "evWeight_loosett1l")
                       .Define("fake_evWeight_wobtagSF", "evWeight_wobtagSF")
                       .Define("fake_totbtagSF", "totbtagSF")
                       .Define("fake_evWeight", "evWeight");

            for(const auto& weight : weights)
            {
                _rlm = _rlm.Define("flip_"+weight+"_loosett1l", weight + "_loosett1l*1.44")
                           .Define("flip_"+weight, weight + "*1.44")
                           .Define("fake_"+weight+"_loosett1l", weight)
                           .Define("fake_"+weight, weight);
            }
        }

        if(year2 == "2018")
        {
            _rlm = _rlm.Define("flip_evWeight_wobtagSF_loosett1l", "evWeight_wobtagSF_loosett1l*1.40")
                       .Define("flip_totbtagSF_loosett1l", "totbtagSF_loosett1l*1.40")
                       .Define("flip_evWeight_loosett1l", "evWeight_loosett1l*1.40")
                       .Define("flip_evWeight_wobtagSF", "evWeight_wobtagSF*1.40")
                       .Define("flip_totbtagSF", "totbtagSF*1.40")
                       .Define("flip_evWeight", "evWeight*1.40")
                       .Define("fake_evWeight_wobtagSF_loosett1l", "evWeight_wobtagSF_loosett1l")
                       .Define("fake_totbtagSF_loosett1l", "totbtagSF_loosett1l")
                       .Define("fake_evWeight_loosett1l", "evWeight_loosett1l")
                       .Define("fake_evWeight_wobtagSF", "evWeight_wobtagSF")
                       .Define("fake_totbtagSF", "totbtagSF")
                       .Define("fake_evWeight", "evWeight");

            for(const auto& weight : weights)
            {
                _rlm = _rlm.Define("flip_"+weight+"_loosett1l", weight + "_loosett1l*1.40")
                           .Define("flip_"+weight, weight + "*1.40")
                           .Define("fake_"+weight+"_loosett1l", weight)
                           .Define("fake_"+weight, weight);
            }
        }
    }
}

//=============================define variables==================================================//
void TprimeAnalyser::defineMoreVars()
{
    if(debug)
    {
        cout<< "================================//================================="<<endl;
        cout<< "Line : "<< __LINE__<<" Function : "<<__FUNCTION__<<endl;
        cout<< "================================//================================="<<endl;
    }
    
    _rlm = _rlm.Define("one", "1.0");
    if (_isData && !isDefined("evWeight"))
	{
        _rlm = _rlm.Define("evWeight_loosett1l", [](){
				return 1.0;
			}, {} );
		_rlm = _rlm.Define("evWeight", [](){
				return 1.0;
			}, {} );
	}

    //combination of particles
    _rlm = _rlm.Define("St_loosett1l","Sum(Selected_electron_loosett1l_pt) + Sum(Selected_electron_pt) + Sum(Selected_muon_loosett1l_pt) + Sum(Selected_clean_jet_pt)")
               .Define("St_with_MET_loosett1l","Sum(Selected_electron_loosett1l_pt) + Sum(Selected_electron_pt) + Sum(Selected_muon_loosett1l_pt) + Sum(Selected_clean_jet_pt) + MET_pt_corr");

    _rlm = _rlm.Define("dis_comb_leptonbjet_loosett1l",::distinct_comb_2,{"Selected_lepton_loosett1l_number","Selected_clean_bjet_loosett1l_number"})
               .Define("Selected_lepton1_loosett1l","ROOT::VecOps::Take(lep4Rvecs_loosett1l, dis_comb_leptonbjet_loosett1l[0])")
               .Define("Selected_clean_bjet1_loosett1l","ROOT::VecOps::Take(cleanbjet4Rvecs_loosett1l, dis_comb_leptonbjet_loosett1l[1])")
               .Define("LeptonbjetFromtop4Rvecs_loosett1l",::sum_two_4Rvecs,{"Selected_lepton1_loosett1l","Selected_clean_bjet1_loosett1l"})
               .Define("LeptonbjetFromtop_loosett1l_pt","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ROOT::VecOps::Map(LeptonbjetFromtop4Rvecs_loosett1l, FourVecPt):Selected_lepton_loosett1l_pt")
               .Define("LeptonbjetFromtop_loosett1l_eta","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ROOT::VecOps::Map(LeptonbjetFromtop4Rvecs_loosett1l, FourVecEta):Selected_lepton_loosett1l_pt")
               .Define("LeptonbjetFromtop_loosett1l_phi","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ROOT::VecOps::Map(LeptonbjetFromtop4Rvecs_loosett1l, FourVecPhi):-Selected_lepton_loosett1l_pt")
               .Define("LeptonbjetFromtop_loosett1l_mass","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ROOT::VecOps::Map(LeptonbjetFromtop4Rvecs_loosett1l, FourVecMass):Selected_lepton_loosett1l_pt")
               .Define("LeptonbjetFromtop_loosett1l_energy","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ROOT::VecOps::Map(LeptonbjetFromtop4Rvecs_loosett1l, FourVecEnergy):Selected_lepton_loosett1l_pt")
               .Define("LeptonbjetFromtop_loosett1l_transverse_energy","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? sqrt(LeptonbjetFromtop_loosett1l_pt*LeptonbjetFromtop_loosett1l_pt + LeptonbjetFromtop_loosett1l_mass*LeptonbjetFromtop_loosett1l_mass):Selected_lepton_loosett1l_pt")
               .Define("LeptonbjetFromtop_loosett1l_deltaeta","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? abs(ROOT::VecOps::Take(Selected_lepton_loosett1l_eta, dis_comb_leptonbjet_loosett1l[0]) - ROOT::VecOps::Take(Selected_clean_bjet_loosett1l_eta, dis_comb_leptonbjet_loosett1l[1])):Selected_lepton_loosett1l_pt")
               .Define("LeptonbjetFromtop_loosett1l_deltaphi","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? abs(ROOT::VecOps::DeltaPhi(ROOT::VecOps::Take(Selected_lepton_loosett1l_phi, dis_comb_leptonbjet_loosett1l[0]),ROOT::VecOps::Take(Selected_clean_bjet_loosett1l_phi, dis_comb_leptonbjet_loosett1l[1]))):Selected_lepton_loosett1l_pt")
               .Define("LeptonbjetFromtop_loosett1l_deltaR","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ROOT::VecOps::DeltaR(ROOT::VecOps::Take(Selected_lepton_loosett1l_eta, dis_comb_leptonbjet_loosett1l[0]),ROOT::VecOps::Take(Selected_clean_bjet_loosett1l_eta, dis_comb_leptonbjet_loosett1l[1]),ROOT::VecOps::Take(Selected_lepton_loosett1l_phi, dis_comb_leptonbjet_loosett1l[0]),ROOT::VecOps::Take(Selected_clean_bjet_loosett1l_phi, dis_comb_leptonbjet_loosett1l[1])):Selected_lepton_loosett1l_pt")
               .Define("LeptonbjetFromtop_loosett1l_transverse_mass","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? sqrt(ROOT::VecOps::Take(Selected_lepton_loosett1l_mass, dis_comb_leptonbjet_loosett1l[0])*ROOT::VecOps::Take(Selected_lepton_loosett1l_mass, dis_comb_leptonbjet_loosett1l[0]) + ROOT::VecOps::Take(Selected_clean_bjet_loosett1l_mass, dis_comb_leptonbjet_loosett1l[1])*ROOT::VecOps::Take(Selected_clean_bjet_loosett1l_mass, dis_comb_leptonbjet_loosett1l[1]) + 2*ROOT::VecOps::Take(Selected_lepton_loosett1l_transverse_energy, dis_comb_leptonbjet_loosett1l[0])*ROOT::VecOps::Take(Selected_clean_bjet_loosett1l_transverse_energy, dis_comb_leptonbjet_loosett1l[1]) - 2*ROOT::VecOps::Take(Selected_lepton_loosett1l_pt, dis_comb_leptonbjet_loosett1l[0])*ROOT::VecOps::Take(Selected_clean_bjet_loosett1l_pt, dis_comb_leptonbjet_loosett1l[1])*cos(LeptonbjetFromtop_loosett1l_deltaphi)):Selected_lepton_loosett1l_pt");

    _rlm = _rlm.Define("Min_deltaR_bJetFromtop_leptonFromtop_loosett1l",::minDR_1,{"Selected_lepton_loosett1l_eta","Selected_clean_bjet_loosett1l_eta","Selected_lepton_loosett1l_phi","Selected_clean_bjet_loosett1l_phi"})
               .Define("Lepton2Fromtop_loosett1l","lep4vecs_loosett1l[Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[0]]")
               .Define("Lepton2Fromtop_loosett1l_pt","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? (lep4vecs_loosett1l[Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[0]]).Pt():-100")
               .Define("Lepton2Fromtop_loosett1l_eta","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? (lep4vecs_loosett1l[Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[0]]).Eta():-10")
               .Define("Lepton2Fromtop_loosett1l_phi","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? (lep4vecs_loosett1l[Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[0]]).Phi():-10")
               .Define("Lepton2Fromtop_loosett1l_mass","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? (lep4vecs_loosett1l[Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[0]]).M():-100")
               .Define("Lepton2Fromtop_loosett1l_transverse_energy","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? sqrt(Lepton2Fromtop_loosett1l_pt*Lepton2Fromtop_loosett1l_pt + Lepton2Fromtop_loosett1l_mass*Lepton2Fromtop_loosett1l_mass):-100")
               .Define("bJet1Fromtop_loosett1l","cleanbjet4vecs_loosett1l[Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[1]]")
               .Define("bJet1Fromtop_loosett1l_pt","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? (cleanbjet4vecs_loosett1l[Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[1]]).Pt():-100")
               .Define("bJet1Fromtop_loosett1l_eta","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? (cleanbjet4vecs_loosett1l[Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[1]]).Eta():-10")
               .Define("bJet1Fromtop_loosett1l_phi","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? (cleanbjet4vecs_loosett1l[Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[1]]).Phi():-10")
               .Define("bJet1Fromtop_loosett1l_mass","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? (cleanbjet4vecs_loosett1l[Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[1]]).M():-100")
               .Define("bJet1Fromtop_loosett1l_transverse_energy","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? sqrt(bJet1Fromtop_loosett1l_pt*bJet1Fromtop_loosett1l_pt + bJet1Fromtop_loosett1l_mass*bJet1Fromtop_loosett1l_mass):-100")
               .Define("Lepton1FromHiggs_loosett1l","(Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[0] == 0) ? lep4vecs_loosett1l[1]:lep4vecs_loosett1l[0]")
               .Define("Lepton1FromHiggs_loosett1l_pt","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ((Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[0] == 0) ? (lep4vecs_loosett1l[1]).Pt():(lep4vecs_loosett1l[0]).Pt()):-100")
               .Define("Lepton1FromHiggs_loosett1l_eta","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ((Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[0] == 0) ? (lep4vecs_loosett1l[1]).Eta():(lep4vecs_loosett1l[0]).Eta()):-10")
               .Define("Lepton1FromHiggs_loosett1l_phi","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ((Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[0] == 0) ? (lep4vecs_loosett1l[1]).Phi():(lep4vecs_loosett1l[0]).Phi()):-10")
    	       .Define("Lepton1FromHiggs_loosett1l_mass","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ((Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[0] == 0) ? (lep4vecs_loosett1l[1]).M():(lep4vecs_loosett1l[0]).M()):-100")
               .Define("Lepton1FromHiggs_loosett1l_transverse_energy","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? sqrt(Lepton1FromHiggs_loosett1l_pt*Lepton1FromHiggs_loosett1l_pt + Lepton1FromHiggs_loosett1l_mass*Lepton1FromHiggs_loosett1l_mass):-100");

    _rlm = _rlm.Define("cleanjet4Rvecs_withoutbjet_loosett1l",::removebjet,{"cleanjet4Rvecs_loosett1l","Selected_clean_jet_loosett1l_pt","bJet1Fromtop_loosett1l_pt"})
               .Define("cleanjet4Rvecs_withoutbjet_loosett1l_pt","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ROOT::VecOps::Map(cleanjet4Rvecs_withoutbjet_loosett1l, FourVecPt):Selected_lepton_loosett1l_pt")
               .Define("cleanjet4Rvecs_withoutbjet_loosett1l_number","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? int(cleanjet4Rvecs_withoutbjet_loosett1l.size()):-10")
               .Define("cleanjet4Rvecs_withoutbjet_loosett1l_eta","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ROOT::VecOps::Map(cleanjet4Rvecs_withoutbjet_loosett1l, FourVecEta):Selected_lepton_loosett1l_pt")
               .Define("cleanjet4Rvecs_withoutbjet_loosett1l_phi","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ROOT::VecOps::Map(cleanjet4Rvecs_withoutbjet_loosett1l, FourVecPhi):Selected_lepton_loosett1l_pt")
               .Define("cleanjet4Rvecs_withoutbjet_loosett1l_mass","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ROOT::VecOps::Map(cleanjet4Rvecs_withoutbjet_loosett1l, FourVecMass):Selected_lepton_loosett1l_pt")
               .Define("cleanjet4Rvecs_withoutbjet_loosett1l_transverse_energy","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? sqrt(cleanjet4Rvecs_withoutbjet_loosett1l_pt*cleanjet4Rvecs_withoutbjet_loosett1l_pt + cleanjet4Rvecs_withoutbjet_loosett1l_mass*cleanjet4Rvecs_withoutbjet_loosett1l_mass):Selected_lepton_loosett1l_pt")
               .Define("cleanjet4vecs_withoutbjet_loosett1l",::generate_4vec,{"cleanjet4Rvecs_withoutbjet_loosett1l_pt","cleanjet4Rvecs_withoutbjet_loosett1l_eta","cleanjet4Rvecs_withoutbjet_loosett1l_phi","cleanjet4Rvecs_withoutbjet_loosett1l_mass"})
               .Define("dis_comb_jets_withoutbjet_loosett1l",::distinct_comb_2_bis,{"cleanjet4Rvecs_withoutbjet_loosett1l_number"})
               .Define("Selected_jet1_loosett1l","ROOT::VecOps::Take(cleanjet4Rvecs_withoutbjet_loosett1l, dis_comb_jets_withoutbjet_loosett1l[0])")
               .Define("Selected_jet2_loosett1l","ROOT::VecOps::Take(cleanjet4Rvecs_withoutbjet_loosett1l, dis_comb_jets_withoutbjet_loosett1l[1])")
               .Define("JetsFromHiggs4Rvecs_loosett1l",::sum_two_4Rvecs,{"Selected_jet1_loosett1l","Selected_jet2_loosett1l"})
               .Define("JetsFromHiggs_loosett1l_pt","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ROOT::VecOps::Map(JetsFromHiggs4Rvecs_loosett1l, FourVecPt):Selected_lepton_loosett1l_pt")
               .Define("JetsFromHiggs_loosett1l_eta","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ROOT::VecOps::Map(JetsFromHiggs4Rvecs_loosett1l, FourVecEta):Selected_lepton_loosett1l_pt")
               .Define("JetsFromHiggs_loosett1l_phi","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ROOT::VecOps::Map(JetsFromHiggs4Rvecs_loosett1l, FourVecPhi):Selected_lepton_loosett1l_pt")
               .Define("JetsFromHiggs_loosett1l_mass","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ROOT::VecOps::Map(JetsFromHiggs4Rvecs_loosett1l, FourVecMass):Selected_lepton_loosett1l_pt")
               .Define("JetsFromHiggs_loosett1l_energy","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ROOT::VecOps::Map(JetsFromHiggs4Rvecs_loosett1l, FourVecEnergy):Selected_lepton_loosett1l_pt")
               .Define("JetsFromHiggs_loosett1l_transverse_energy","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? sqrt(JetsFromHiggs_loosett1l_pt*JetsFromHiggs_loosett1l_pt + JetsFromHiggs_loosett1l_mass*JetsFromHiggs_loosett1l_mass):Selected_lepton_loosett1l_pt")
               .Define("JetsFromHiggs_loosett1l_deltaeta","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? abs(ROOT::VecOps::Take(cleanjet4Rvecs_withoutbjet_loosett1l_eta, dis_comb_jets_withoutbjet_loosett1l[0]) - ROOT::VecOps::Take(cleanjet4Rvecs_withoutbjet_loosett1l_eta, dis_comb_jets_withoutbjet_loosett1l[1])):Selected_lepton_loosett1l_pt")
               .Define("JetsFromHiggs_loosett1l_deltaphi","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? abs(ROOT::VecOps::DeltaPhi(ROOT::VecOps::Take(cleanjet4Rvecs_withoutbjet_loosett1l_phi, dis_comb_jets_withoutbjet_loosett1l[0]),ROOT::VecOps::Take(cleanjet4Rvecs_withoutbjet_loosett1l_phi, dis_comb_jets_withoutbjet_loosett1l[1]))):Selected_lepton_loosett1l_pt")
               .Define("JetsFromHiggs_loosett1l_deltaR","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ROOT::VecOps::DeltaR(ROOT::VecOps::Take(cleanjet4Rvecs_withoutbjet_loosett1l_eta, dis_comb_jets_withoutbjet_loosett1l[0]),ROOT::VecOps::Take(cleanjet4Rvecs_withoutbjet_loosett1l_eta, dis_comb_jets_withoutbjet_loosett1l[1]),ROOT::VecOps::Take(cleanjet4Rvecs_withoutbjet_loosett1l_phi, dis_comb_jets_withoutbjet_loosett1l[0]),ROOT::VecOps::Take(cleanjet4Rvecs_withoutbjet_loosett1l_phi, dis_comb_jets_withoutbjet_loosett1l[1])):Selected_lepton_loosett1l_pt")
               .Define("JetsFromHiggs_loosett1l_transverse_mass","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? sqrt(ROOT::VecOps::Take(cleanjet4Rvecs_withoutbjet_loosett1l_mass, dis_comb_jets_withoutbjet_loosett1l[0])*ROOT::VecOps::Take(cleanjet4Rvecs_withoutbjet_loosett1l_mass, dis_comb_jets_withoutbjet_loosett1l[0]) + ROOT::VecOps::Take(cleanjet4Rvecs_withoutbjet_loosett1l_mass, dis_comb_jets_withoutbjet_loosett1l[1])*ROOT::VecOps::Take(cleanjet4Rvecs_withoutbjet_loosett1l_mass, dis_comb_jets_withoutbjet_loosett1l[1]) + 2*ROOT::VecOps::Take(cleanjet4Rvecs_withoutbjet_loosett1l_transverse_energy, dis_comb_jets_withoutbjet_loosett1l[0])*ROOT::VecOps::Take(cleanjet4Rvecs_withoutbjet_loosett1l_transverse_energy, dis_comb_jets_withoutbjet_loosett1l[1]) - 2*ROOT::VecOps::Take(cleanjet4Rvecs_withoutbjet_loosett1l_pt, dis_comb_jets_withoutbjet_loosett1l[0])*ROOT::VecOps::Take(cleanjet4Rvecs_withoutbjet_loosett1l_pt, dis_comb_jets_withoutbjet_loosett1l[1])*cos(JetsFromHiggs_loosett1l_deltaphi)):Selected_lepton_loosett1l_pt");

    _rlm = _rlm.Define("Min_deltaR_JetsFromHiggs_loosett1l",::minDR_2,{"cleanjet4Rvecs_withoutbjet_loosett1l_eta","cleanjet4Rvecs_withoutbjet_loosett1l_eta","cleanjet4Rvecs_withoutbjet_loosett1l_phi","cleanjet4Rvecs_withoutbjet_loosett1l_phi"})
    	       .Define("Jet1FromHiggs_loosett1l","cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[0]]")
               .Define("Jet1FromHiggs_loosett1l_pt","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[0]].Pt():-100")
               .Define("Jet1FromHiggs_loosett1l_eta","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[0]].Eta():-10")
               .Define("Jet1FromHiggs_loosett1l_phi","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[0]].Phi():-10")
               .Define("Jet1FromHiggs_loosett1l_mass","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[0]].M():-100")
               .Define("Jet1FromHiggs_loosett1l_transverse_energy","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? sqrt(Jet1FromHiggs_loosett1l_pt*Jet1FromHiggs_loosett1l_pt + Jet1FromHiggs_loosett1l_mass*Jet1FromHiggs_loosett1l_mass):-100")
               .Define("Jet2FromHiggs_loosett1l","cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[1]]")
               .Define("Jet2FromHiggs_loosett1l_pt","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[1]].Pt():-100")
               .Define("Jet2FromHiggs_loosett1l_eta","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[1]].Eta():-10")
               .Define("Jet2FromHiggs_loosett1l_phi","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[1]].Phi():-10")
               .Define("Jet2FromHiggs_loosett1l_mass","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[1]].M():-100")
               .Define("Jet2FromHiggs_loosett1l_transverse_energy","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? sqrt(Jet2FromHiggs_loosett1l_pt*Jet2FromHiggs_loosett1l_pt + Jet2FromHiggs_loosett1l_mass*Jet2FromHiggs_loosett1l_mass):-100")
               .Define("Jet1FromHiggs_Jet2FromHiggs_loosett1l","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[0]] + cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[1]]:lep4vecs_loosett1l[0]")
               .Define("Jet1FromHiggs_Jet2FromHiggs_loosett1l_pt","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? (cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[0]] + cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[1]]).Pt():-100")
               .Define("Jet1FromHiggs_Jet2FromHiggs_loosett1l_eta","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? (cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[0]] + cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[1]]).Eta():-10")
               .Define("Jet1FromHiggs_Jet2FromHiggs_loosett1l_phi","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? (cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[0]] + cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[1]]).Phi():-10")
               .Define("Jet1FromHiggs_Jet2FromHiggs_loosett1l_mass","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? (cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[0]] + cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[1]]).M():-100")
               .Define("Jet1FromHiggs_Jet2FromHiggs_loosett1l_transverse_energy","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? sqrt(Jet1FromHiggs_Jet2FromHiggs_loosett1l_pt*Jet1FromHiggs_Jet2FromHiggs_loosett1l_pt + Jet1FromHiggs_Jet2FromHiggs_loosett1l_mass*Jet1FromHiggs_Jet2FromHiggs_loosett1l_mass):-100")
               .Define("Jet1FromHiggs_Jet2FromHiggs_loosett1l_deltaeta","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? abs(Jet2FromHiggs_loosett1l_eta - Jet1FromHiggs_loosett1l_eta):-10")
               .Define("Jet1FromHiggs_Jet2FromHiggs_loosett1l_deltaphi","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? abs(ROOT::VecOps::DeltaPhi(Jet1FromHiggs_loosett1l_phi,Jet2FromHiggs_loosett1l_phi)):-10")
               .Define("Jet1FromHiggs_Jet2FromHiggs_loosett1l_deltaR","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ROOT::VecOps::DeltaR(Jet1FromHiggs_loosett1l_eta,Jet2FromHiggs_loosett1l_eta,Jet1FromHiggs_loosett1l_phi,Jet2FromHiggs_loosett1l_phi):-10")
               .Define("Jet1FromHiggs_Jet2FromHiggs_loosett1l_transverse_mass","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? sqrt(Jet1FromHiggs_loosett1l_mass*Jet1FromHiggs_loosett1l_mass + Jet2FromHiggs_loosett1l_mass*Jet2FromHiggs_loosett1l_mass + 2*Jet1FromHiggs_loosett1l_transverse_energy*Jet2FromHiggs_loosett1l_transverse_energy - 2*Jet1FromHiggs_loosett1l_pt*Jet2FromHiggs_loosett1l_pt*cos(Jet1FromHiggs_Jet2FromHiggs_loosett1l_deltaphi)):-100");

    _rlm = _rlm.Define("Vectorial_sum_three_clean_jets_loosett1l",::Threejets,{"cleanjet4vecs_loosett1l","Selected_clean_jet_loosett1l_eta","Selected_clean_jet_loosett1l_phi","Selected_clean_jet_loosett1l_number","Selected_clean_bjet_loosett1l_number","bJet1Fromtop_loosett1l_eta","bJet1Fromtop_loosett1l_phi"})
               .Define("Vectorial_sum_three_clean_jets_loosett1l_pt","(Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ROOT::VecOps::Map(Vectorial_sum_three_clean_jets_loosett1l, FourVecPt):Selected_clean_jet_loosett1l_pt")
               .Define("Vectorial_sum_three_clean_jets_loosett1l_eta","(Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ROOT::VecOps::Map(Vectorial_sum_three_clean_jets_loosett1l, FourVecEta):Selected_clean_jet_loosett1l_pt")
               .Define("Vectorial_sum_three_clean_jets_loosett1l_phi","(Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ROOT::VecOps::Map(Vectorial_sum_three_clean_jets_loosett1l, FourVecPhi):Selected_clean_jet_loosett1l_pt")
               .Define("Vectorial_sum_three_clean_jets_loosett1l_mass","(Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ROOT::VecOps::Map(Vectorial_sum_three_clean_jets_loosett1l, FourVecMass):Selected_clean_jet_loosett1l_pt")
               .Define("Vectorial_sum_three_clean_jets_loosett1l_mass_offset","(Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? Vectorial_sum_three_clean_jets_loosett1l_mass[0] - 172.7:-100")
               .Define("Vectorial_sum_three_clean_jets_loosett1l_mass_min","(Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? abs(Vectorial_sum_three_clean_jets_loosett1l_mass_offset):-100");

    _rlm = _rlm.Define("Lepton2Fromtop_bJet1Fromtop_loosett1l","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? lep4vecs_loosett1l[Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[0]] + cleanbjet4vecs_loosett1l[Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[1]]:lep4vecs_loosett1l[0]")
               .Define("Lepton2Fromtop_bJet1Fromtop_loosett1l_pt","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? (lep4vecs_loosett1l[Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[0]] + cleanbjet4vecs_loosett1l[Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[1]]).Pt():-100")
               .Define("Lepton2Fromtop_bJet1Fromtop_loosett1l_eta","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? (lep4vecs_loosett1l[Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[0]] + cleanbjet4vecs_loosett1l[Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[1]]).Eta():-10")
               .Define("Lepton2Fromtop_bJet1Fromtop_loosett1l_phi","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? (lep4vecs_loosett1l[Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[0]] + cleanbjet4vecs_loosett1l[Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[1]]).Phi():-10")
               .Define("Lepton2Fromtop_bJet1Fromtop_loosett1l_mass","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? (lep4vecs_loosett1l[Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[0]] + cleanbjet4vecs_loosett1l[Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[1]]).M():-100")
               .Define("Lepton2Fromtop_bJet1Fromtop_loosett1l_transverse_energy","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? sqrt(Lepton2Fromtop_bJet1Fromtop_loosett1l_pt*Lepton2Fromtop_bJet1Fromtop_loosett1l_pt + Lepton2Fromtop_bJet1Fromtop_loosett1l_mass*Lepton2Fromtop_bJet1Fromtop_loosett1l_mass):-100")
               .Define("Lepton2Fromtop_bJet1Fromtop_loosett1l_deltaeta","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? abs(Lepton2Fromtop_loosett1l_eta - bJet1Fromtop_loosett1l_eta):-10")
               .Define("Lepton2Fromtop_bJet1Fromtop_loosett1l_deltaphi","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? abs(ROOT::VecOps::DeltaPhi(Lepton2Fromtop_loosett1l_phi,bJet1Fromtop_loosett1l_phi)):-10")
               .Define("Lepton2Fromtop_bJet1Fromtop_loosett1l_deltaR","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ROOT::VecOps::DeltaR(Lepton2Fromtop_loosett1l_eta,bJet1Fromtop_loosett1l_eta,Lepton2Fromtop_loosett1l_phi,bJet1Fromtop_loosett1l_phi):-10")
               .Define("Lepton2Fromtop_bJet1Fromtop_loosett1l_transverse_mass","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? sqrt(Lepton2Fromtop_loosett1l_mass*Lepton2Fromtop_loosett1l_mass + bJet1Fromtop_loosett1l_mass*bJet1Fromtop_loosett1l_mass + 2*Lepton2Fromtop_loosett1l_transverse_energy*bJet1Fromtop_loosett1l_transverse_energy - 2*Lepton2Fromtop_loosett1l_pt*bJet1Fromtop_loosett1l_pt*cos(Lepton2Fromtop_bJet1Fromtop_loosett1l_deltaphi)):-100")
               .Define("Lepton2Fromtop_Jet1FromHiggs_loosett1l","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? lep4vecs_loosett1l[Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[0]] + cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[0]]:lep4vecs_loosett1l[0]")
               .Define("Lepton2Fromtop_Jet1FromHiggs_loosett1l_pt","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? (lep4vecs_loosett1l[Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[0]] + cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[0]]).Pt():-100")
               .Define("Lepton2Fromtop_Jet1FromHiggs_loosett1l_eta","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? (lep4vecs_loosett1l[Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[0]] + cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[0]]).Eta():-10")
               .Define("Lepton2Fromtop_Jet1FromHiggs_loosett1l_phi","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? (lep4vecs_loosett1l[Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[0]] + cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[0]]).Phi():-10")
               .Define("Lepton2Fromtop_Jet1FromHiggs_loosett1l_mass","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? (lep4vecs_loosett1l[Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[0]] + cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[0]]).M():-100")
               .Define("Lepton2Fromtop_Jet1FromHiggs_loosett1l_transverse_energy","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? sqrt(Lepton2Fromtop_Jet1FromHiggs_loosett1l_pt*Lepton2Fromtop_Jet1FromHiggs_loosett1l_pt + Lepton2Fromtop_Jet1FromHiggs_loosett1l_mass*Lepton2Fromtop_Jet1FromHiggs_loosett1l_mass):-100")
               .Define("Lepton2Fromtop_Jet1FromHiggs_loosett1l_deltaeta","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? abs(Lepton2Fromtop_loosett1l_eta - Jet1FromHiggs_loosett1l_eta):-10")
               .Define("Lepton2Fromtop_Jet1FromHiggs_loosett1l_deltaphi","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? abs(ROOT::VecOps::DeltaPhi(Lepton2Fromtop_loosett1l_phi,Jet1FromHiggs_loosett1l_phi)):-10")
               .Define("Lepton2Fromtop_Jet1FromHiggs_loosett1l_deltaR","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ROOT::VecOps::DeltaR(Lepton2Fromtop_loosett1l_eta,Jet1FromHiggs_loosett1l_eta,Lepton2Fromtop_loosett1l_phi,Jet1FromHiggs_loosett1l_phi):-10")
               .Define("Lepton2Fromtop_Jet1FromHiggs_loosett1l_transverse_mass","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? sqrt(Lepton2Fromtop_loosett1l_mass*Lepton2Fromtop_loosett1l_mass + Jet1FromHiggs_loosett1l_mass*Jet1FromHiggs_loosett1l_mass + 2*Lepton2Fromtop_loosett1l_transverse_energy*Jet1FromHiggs_loosett1l_transverse_energy - 2*Lepton2Fromtop_loosett1l_pt*Jet1FromHiggs_loosett1l_pt*cos(Lepton2Fromtop_Jet1FromHiggs_loosett1l_deltaphi)):-100")
               .Define("Lepton2Fromtop_Jet2FromHiggs_loosett1l","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? lep4vecs_loosett1l[Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[0]] + cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[1]]:lep4vecs_loosett1l[0]")
               .Define("Lepton2Fromtop_Jet2FromHiggs_loosett1l_pt","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? (lep4vecs_loosett1l[Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[0]] + cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[1]]).Pt():-100")
               .Define("Lepton2Fromtop_Jet2FromHiggs_loosett1l_eta","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? (lep4vecs_loosett1l[Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[0]] + cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[1]]).Eta():-10")
               .Define("Lepton2Fromtop_Jet2FromHiggs_loosett1l_phi","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? (lep4vecs_loosett1l[Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[0]] + cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[1]]).Phi():-10")
               .Define("Lepton2Fromtop_Jet2FromHiggs_loosett1l_mass","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? (lep4vecs_loosett1l[Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[0]] + cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[1]]).M():-10")
               .Define("Lepton2Fromtop_Jet2FromHiggs_loosett1l_transverse_energy","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? sqrt(Lepton2Fromtop_Jet2FromHiggs_loosett1l_pt*Lepton2Fromtop_Jet2FromHiggs_loosett1l_pt + Lepton2Fromtop_Jet2FromHiggs_loosett1l_mass*Lepton2Fromtop_Jet2FromHiggs_loosett1l_mass):-100")
               .Define("Lepton2Fromtop_Jet2FromHiggs_loosett1l_deltaeta","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? abs(Lepton2Fromtop_loosett1l_eta - Jet2FromHiggs_loosett1l_eta):-10")
               .Define("Lepton2Fromtop_Jet2FromHiggs_loosett1l_deltaphi","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? abs(ROOT::VecOps::DeltaPhi(Lepton2Fromtop_loosett1l_phi,Jet2FromHiggs_loosett1l_phi)):-10")
               .Define("Lepton2Fromtop_Jet2FromHiggs_loosett1l_deltaR","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ROOT::VecOps::DeltaR(Lepton2Fromtop_loosett1l_eta,Jet2FromHiggs_loosett1l_eta,Lepton2Fromtop_loosett1l_phi,Jet2FromHiggs_loosett1l_phi):-10")
               .Define("Lepton2Fromtop_Jet2FromHiggs_loosett1l_transverse_mass","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? sqrt(Lepton2Fromtop_loosett1l_mass*Lepton2Fromtop_loosett1l_mass + Jet2FromHiggs_loosett1l_mass*Jet2FromHiggs_loosett1l_mass + 2*Lepton2Fromtop_loosett1l_transverse_energy*Jet2FromHiggs_loosett1l_transverse_energy - 2*Lepton2Fromtop_loosett1l_pt*Jet2FromHiggs_loosett1l_pt*cos(Lepton2Fromtop_Jet2FromHiggs_loosett1l_deltaphi)):-100")
               .Define("Lepton1FromHiggs_bJet1Fromtop_loosett1l","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ((Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[0] == 0) ? lep4vecs_loosett1l[1]+cleanbjet4vecs_loosett1l[Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[1]]:lep4vecs_loosett1l[0]+cleanbjet4vecs_loosett1l[Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[1]]):lep4vecs_loosett1l[0]")
               .Define("Lepton1FromHiggs_bJet1Fromtop_loosett1l_pt","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ((Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[0] == 0) ? (lep4vecs_loosett1l[1]+cleanbjet4vecs_loosett1l[Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[1]]).Pt():(lep4vecs_loosett1l[0]+cleanbjet4vecs_loosett1l[Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[1]]).Pt()):-100")
               .Define("Lepton1FromHiggs_bJet1Fromtop_loosett1l_eta","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ((Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[0] == 0) ? (lep4vecs_loosett1l[1]+cleanbjet4vecs_loosett1l[Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[1]]).Eta():(lep4vecs_loosett1l[0]+cleanbjet4vecs_loosett1l[Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[1]]).Eta()):-10")
               .Define("Lepton1FromHiggs_bJet1Fromtop_loosett1l_phi","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ((Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[0] == 0) ? (lep4vecs_loosett1l[1]+cleanbjet4vecs_loosett1l[Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[1]]).Phi():(lep4vecs_loosett1l[0]+cleanbjet4vecs_loosett1l[Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[1]]).Phi()):-10")
               .Define("Lepton1FromHiggs_bJet1Fromtop_loosett1l_mass","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ((Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[0] == 0) ? (lep4vecs_loosett1l[1]+cleanbjet4vecs_loosett1l[Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[1]]).M():(lep4vecs_loosett1l[0]+cleanbjet4vecs_loosett1l[Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[1]]).M()):-100")
               .Define("Lepton1FromHiggs_bJet1Fromtop_loosett1l_transverse_energy","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? sqrt(Lepton1FromHiggs_bJet1Fromtop_loosett1l_pt*Lepton1FromHiggs_bJet1Fromtop_loosett1l_pt + Lepton1FromHiggs_bJet1Fromtop_loosett1l_mass*Lepton1FromHiggs_bJet1Fromtop_loosett1l_mass):-100")
               .Define("Lepton1FromHiggs_bJet1Fromtop_loosett1l_deltaeta","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? abs(Lepton1FromHiggs_loosett1l_eta - bJet1Fromtop_loosett1l_eta):-10")
               .Define("Lepton1FromHiggs_bJet1Fromtop_loosett1l_deltaphi","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? abs(ROOT::VecOps::DeltaPhi(Lepton1FromHiggs_loosett1l_phi,bJet1Fromtop_loosett1l_phi)):-10")
               .Define("Lepton1FromHiggs_bJet1Fromtop_loosett1l_deltaR","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ROOT::VecOps::DeltaR(Lepton1FromHiggs_loosett1l_eta,bJet1Fromtop_loosett1l_eta,Lepton1FromHiggs_loosett1l_phi,bJet1Fromtop_loosett1l_phi):-10")
               .Define("Lepton1FromHiggs_bJet1Fromtop_loosett1l_transverse_mass","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? sqrt(Lepton1FromHiggs_loosett1l_mass*Lepton1FromHiggs_loosett1l_mass + bJet1Fromtop_loosett1l_mass*bJet1Fromtop_loosett1l_mass + 2*Lepton1FromHiggs_loosett1l_transverse_energy*bJet1Fromtop_loosett1l_transverse_energy - 2*Lepton1FromHiggs_loosett1l_pt*bJet1Fromtop_loosett1l_pt*cos(Lepton1FromHiggs_bJet1Fromtop_loosett1l_deltaphi)):-100")
               .Define("Lepton1FromHiggs_Jet1FromHiggs_loosett1l","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ((Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[0] == 0) ? lep4vecs_loosett1l[1]+cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[0]]:lep4vecs_loosett1l[0]+cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[0]]):lep4vecs_loosett1l[0]")
               .Define("Lepton1FromHiggs_Jet1FromHiggs_loosett1l_pt","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ((Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[0] == 0) ? (lep4vecs_loosett1l[1]+cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[0]]).Pt():(lep4vecs_loosett1l[0]+cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[0]]).Pt()):-100")
               .Define("Lepton1FromHiggs_Jet1FromHiggs_loosett1l_eta","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ((Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[0] == 0) ? (lep4vecs_loosett1l[1]+cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[0]]).Eta():(lep4vecs_loosett1l[0]+cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[0]]).Eta()):-10")
               .Define("Lepton1FromHiggs_Jet1FromHiggs_loosett1l_phi","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ((Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[0] == 0) ? (lep4vecs_loosett1l[1]+cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[0]]).Phi():(lep4vecs_loosett1l[0]+cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[0]]).Phi()):-10")
               .Define("Lepton1FromHiggs_Jet1FromHiggs_loosett1l_mass","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ((Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[0] == 0) ? (lep4vecs_loosett1l[1]+cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[0]]).M():(lep4vecs_loosett1l[0]+cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[0]]).M()):-100")
               .Define("Lepton1FromHiggs_Jet1FromHiggs_loosett1l_transverse_energy","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? sqrt(Lepton1FromHiggs_Jet1FromHiggs_loosett1l_pt*Lepton1FromHiggs_Jet1FromHiggs_loosett1l_pt + Lepton1FromHiggs_Jet1FromHiggs_loosett1l_mass*Lepton1FromHiggs_Jet1FromHiggs_loosett1l_mass):-100")
               .Define("Lepton1FromHiggs_Jet1FromHiggs_loosett1l_deltaeta","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? abs(Lepton1FromHiggs_loosett1l_eta - Jet1FromHiggs_loosett1l_eta):-10")
               .Define("Lepton1FromHiggs_Jet1FromHiggs_loosett1l_deltaphi","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? abs(ROOT::VecOps::DeltaPhi(Lepton1FromHiggs_loosett1l_phi,Jet1FromHiggs_loosett1l_phi)):-10")
               .Define("Lepton1FromHiggs_Jet1FromHiggs_loosett1l_deltaR","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ROOT::VecOps::DeltaR(Lepton1FromHiggs_loosett1l_eta,Jet1FromHiggs_loosett1l_eta,Lepton1FromHiggs_loosett1l_phi,Jet1FromHiggs_loosett1l_phi):-10")
               .Define("Lepton1FromHiggs_Jet1FromHiggs_loosett1l_transverse_mass","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? sqrt(Lepton1FromHiggs_loosett1l_mass*Lepton1FromHiggs_loosett1l_mass + Jet1FromHiggs_loosett1l_mass*Jet1FromHiggs_loosett1l_mass + 2*Lepton1FromHiggs_loosett1l_transverse_energy*Jet1FromHiggs_loosett1l_transverse_energy - 2*Lepton1FromHiggs_loosett1l_pt*Jet1FromHiggs_loosett1l_pt*cos(Lepton1FromHiggs_Jet1FromHiggs_loosett1l_deltaphi)):-100")
               .Define("Lepton1FromHiggs_Jet2FromHiggs_loosett1l","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ((Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[0] == 0) ? lep4vecs_loosett1l[1]+cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[1]]:lep4vecs_loosett1l[0]+cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[1]]):lep4vecs_loosett1l[0]")
               .Define("Lepton1FromHiggs_Jet2FromHiggs_loosett1l_pt","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ((Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[0] == 0) ? (lep4vecs_loosett1l[1]+cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[1]]).Pt():(lep4vecs_loosett1l[0]+cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[1]]).Pt()):-100")
               .Define("Lepton1FromHiggs_Jet2FromHiggs_loosett1l_eta","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ((Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[0] == 0) ? (lep4vecs_loosett1l[1]+cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[1]]).Eta():(lep4vecs_loosett1l[0]+cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[1]]).Eta()):-10")
               .Define("Lepton1FromHiggs_Jet2FromHiggs_loosett1l_phi","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ((Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[0] == 0) ? (lep4vecs_loosett1l[1]+cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[1]]).Phi():(lep4vecs_loosett1l[0]+cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[1]]).Phi()):-10")
               .Define("Lepton1FromHiggs_Jet2FromHiggs_loosett1l_mass","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ((Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[0] == 0) ? (lep4vecs_loosett1l[1]+cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[1]]).M():(lep4vecs_loosett1l[0]+cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[1]]).M()):-100")
               .Define("Lepton1FromHiggs_Jet2FromHiggs_loosett1l_transverse_energy","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? sqrt(Lepton1FromHiggs_Jet2FromHiggs_loosett1l_pt*Lepton1FromHiggs_Jet2FromHiggs_loosett1l_pt + Lepton1FromHiggs_Jet2FromHiggs_loosett1l_mass*Lepton1FromHiggs_Jet2FromHiggs_loosett1l_mass):-100")
               .Define("Lepton1FromHiggs_Jet2FromHiggs_loosett1l_deltaeta","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? abs(Lepton1FromHiggs_loosett1l_eta - Jet2FromHiggs_loosett1l_eta):-10")
               .Define("Lepton1FromHiggs_Jet2FromHiggs_loosett1l_deltaphi","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? abs(ROOT::VecOps::DeltaPhi(Lepton1FromHiggs_loosett1l_phi,Jet2FromHiggs_loosett1l_phi)):-10")
               .Define("Lepton1FromHiggs_Jet2FromHiggs_loosett1l_deltaR","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ROOT::VecOps::DeltaR(Lepton1FromHiggs_loosett1l_eta,Jet2FromHiggs_loosett1l_eta,Lepton1FromHiggs_loosett1l_phi,Jet2FromHiggs_loosett1l_phi):-10")
               .Define("Lepton1FromHiggs_Jet2FromHiggs_loosett1l_transverse_mass","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? sqrt(Lepton1FromHiggs_loosett1l_mass*Lepton1FromHiggs_loosett1l_mass + Jet2FromHiggs_loosett1l_mass*Jet2FromHiggs_loosett1l_mass + 2*Lepton1FromHiggs_loosett1l_transverse_energy*Jet2FromHiggs_loosett1l_transverse_energy - 2*Lepton1FromHiggs_loosett1l_pt*Jet2FromHiggs_loosett1l_pt*cos(Lepton1FromHiggs_Jet2FromHiggs_loosett1l_deltaphi)):-100")
               .Define("Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_loosett1l","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ((Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[0] == 0) ? lep4vecs_loosett1l[1]+cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[0]]+cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[1]]:lep4vecs_loosett1l[0]+cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[0]]+cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[1]]):lep4vecs_loosett1l[0]")
               .Define("Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_loosett1l_pt","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ((Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[0] == 0) ? (lep4vecs_loosett1l[1]+cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[0]]+cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[1]]).Pt():(lep4vecs_loosett1l[0]+cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[0]]+cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[1]]).Pt()):-100")
               .Define("Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_loosett1l_eta","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ((Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[0] == 0) ? (lep4vecs_loosett1l[1]+cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[0]]+cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[1]]).Eta():(lep4vecs_loosett1l[0]+cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[0]]+cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[1]]).Eta()):-10")
               .Define("Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_loosett1l_phi","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ((Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[0] == 0) ? (lep4vecs_loosett1l[1]+cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[0]]+cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[1]]).Phi():(lep4vecs_loosett1l[0]+cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[0]]+cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[1]]).Phi()):-10")
               .Define("Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_loosett1l_mass","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ((Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[0] == 0) ? (lep4vecs_loosett1l[1]+cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[0]]+cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[1]]).M():(lep4vecs_loosett1l[0]+cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[0]]+cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[1]]).M()):-100")
               .Define("Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_loosett1l_transverse_energy","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? sqrt(Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_loosett1l_pt*Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_loosett1l_pt + Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_loosett1l_mass*Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_loosett1l_mass):-100")
               .Define("Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_loosett1l_deltaeta","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? abs(Lepton1FromHiggs_loosett1l_eta - Jet1FromHiggs_Jet2FromHiggs_loosett1l_eta):-10")
               .Define("Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_loosett1l_deltaphi","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? abs(ROOT::VecOps::DeltaPhi(Lepton1FromHiggs_loosett1l_phi,Jet1FromHiggs_Jet2FromHiggs_loosett1l_phi)):-10")
               .Define("Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_loosett1l_deltaR","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ROOT::VecOps::DeltaR(Lepton1FromHiggs_loosett1l_eta,Jet1FromHiggs_Jet2FromHiggs_loosett1l_eta,Lepton1FromHiggs_loosett1l_phi,Jet1FromHiggs_Jet2FromHiggs_loosett1l_phi):-10")
               .Define("Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_loosett1l_transverse_mass","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? sqrt(Lepton1FromHiggs_loosett1l_mass*Lepton1FromHiggs_loosett1l_mass + Jet1FromHiggs_Jet2FromHiggs_loosett1l_mass*Jet1FromHiggs_Jet2FromHiggs_loosett1l_mass + 2*Lepton1FromHiggs_loosett1l_transverse_energy*Jet1FromHiggs_Jet2FromHiggs_loosett1l_transverse_energy - 2*Lepton1FromHiggs_loosett1l_pt*Jet1FromHiggs_Jet2FromHiggs_loosett1l_pt*cos(Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_loosett1l_deltaphi)):-100")
               .Define("L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? lep4vecs_loosett1l[0]+lep4vecs_loosett1l[1]+cleanbjet4vecs_loosett1l[Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[1]]+cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[0]]+cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[1]]:lep4vecs_loosett1l[0]")
               .Define("L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_pt","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? (lep4vecs_loosett1l[0]+lep4vecs_loosett1l[1]+cleanbjet4vecs_loosett1l[Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[1]]+cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[0]]+cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[1]]).Pt():-100")
               .Define("L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_eta","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? (lep4vecs_loosett1l[0]+lep4vecs_loosett1l[1]+cleanbjet4vecs_loosett1l[Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[1]]+cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[0]]+cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[1]]).Eta():-10")
               .Define("L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_phi","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? (lep4vecs_loosett1l[0]+lep4vecs_loosett1l[1]+cleanbjet4vecs_loosett1l[Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[1]]+cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[0]]+cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[1]]).Phi():-10")
               .Define("L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_mass","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? (lep4vecs_loosett1l[0]+lep4vecs_loosett1l[1]+cleanbjet4vecs_loosett1l[Min_deltaR_bJetFromtop_leptonFromtop_loosett1l[1]]+cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[0]]+cleanjet4vecs_withoutbjet_loosett1l[Min_deltaR_JetsFromHiggs_loosett1l[1]]).M():-100")
               .Define("L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_px","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_pt*cos(L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_phi):-10")
               .Define("L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_py","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_pt*sin(L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_phi):-10")
               .Define("L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_transverse_energy","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? sqrt(L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_pt*L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_pt + L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_mass*L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_mass):-100")
               .Define("L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_deltaeta","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? abs(Lepton2Fromtop_bJet1Fromtop_loosett1l_eta - Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_loosett1l_eta):-10")
               .Define("L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_deltaphi","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? abs(ROOT::VecOps::DeltaPhi(Lepton2Fromtop_bJet1Fromtop_loosett1l_phi,Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_loosett1l_phi)):-10")
               .Define("L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_deltaR","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? ROOT::VecOps::DeltaR(Lepton2Fromtop_bJet1Fromtop_loosett1l_eta,Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_loosett1l_eta,Lepton2Fromtop_bJet1Fromtop_loosett1l_phi,Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_loosett1l_phi):-10")
               .Define("L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_transverse_mass","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? sqrt(Lepton2Fromtop_bJet1Fromtop_loosett1l_mass*Lepton2Fromtop_bJet1Fromtop_loosett1l_mass + Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_loosett1l_mass*Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_loosett1l_mass + 2*Lepton2Fromtop_bJet1Fromtop_loosett1l_transverse_energy*Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_loosett1l_transverse_energy - 2*Lepton2Fromtop_bJet1Fromtop_loosett1l_pt*Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_loosett1l_pt*cos(L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_deltaphi)):-100")
               .Define("L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_Relative_St","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? (Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_loosett1l_pt + Lepton2Fromtop_bJet1Fromtop_loosett1l_pt) / St_loosett1l:-10")
               .Define("Tprime_transverse_mass_first_loosett1l_600","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? sqrt((Lepton1FromHiggs_loosett1l_transverse_energy + Jet1FromHiggs_loosett1l_transverse_energy + Jet2FromHiggs_loosett1l_transverse_energy + Lepton2Fromtop_loosett1l_transverse_energy + bJet1Fromtop_loosett1l_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*58.66*(58.66 + MET_pt_corr)))*(Lepton1FromHiggs_loosett1l_transverse_energy + Jet1FromHiggs_loosett1l_transverse_energy + Jet2FromHiggs_loosett1l_transverse_energy + Lepton2Fromtop_loosett1l_transverse_energy + bJet1Fromtop_loosett1l_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*58.66*(58.66 + MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_py + MET_py))):-100")
               .Define("Tprime_transverse_mass_second_loosett1l_600","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? sqrt((Lepton1FromHiggs_loosett1l_transverse_energy + Jet1FromHiggs_loosett1l_transverse_energy + Jet2FromHiggs_loosett1l_transverse_energy + Lepton2Fromtop_loosett1l_transverse_energy + bJet1Fromtop_loosett1l_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*58.66*(58.66 - MET_pt_corr)))*(Lepton1FromHiggs_loosett1l_transverse_energy + Jet1FromHiggs_loosett1l_transverse_energy + Jet2FromHiggs_loosett1l_transverse_energy + Lepton2Fromtop_loosett1l_transverse_energy + bJet1Fromtop_loosett1l_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*58.66*(58.66 - MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_py + MET_py))):-100")
               .Define("Tprime_transverse_mass_first_loosett1l_625","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? sqrt((Lepton1FromHiggs_loosett1l_transverse_energy + Jet1FromHiggs_loosett1l_transverse_energy + Jet2FromHiggs_loosett1l_transverse_energy + Lepton2Fromtop_loosett1l_transverse_energy + bJet1Fromtop_loosett1l_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*61.24*(61.24 + MET_pt_corr)))*(Lepton1FromHiggs_loosett1l_transverse_energy + Jet1FromHiggs_loosett1l_transverse_energy + Jet2FromHiggs_loosett1l_transverse_energy + Lepton2Fromtop_loosett1l_transverse_energy + bJet1Fromtop_loosett1l_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*61.24*(61.24 + MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_py + MET_py))):-100")
               .Define("Tprime_transverse_mass_second_loosett1l_625","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? sqrt((Lepton1FromHiggs_loosett1l_transverse_energy + Jet1FromHiggs_loosett1l_transverse_energy + Jet2FromHiggs_loosett1l_transverse_energy + Lepton2Fromtop_loosett1l_transverse_energy + bJet1Fromtop_loosett1l_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*61.24*(61.24 - MET_pt_corr)))*(Lepton1FromHiggs_loosett1l_transverse_energy + Jet1FromHiggs_loosett1l_transverse_energy + Jet2FromHiggs_loosett1l_transverse_energy + Lepton2Fromtop_loosett1l_transverse_energy + bJet1Fromtop_loosett1l_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*61.24*(61.24 - MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_py + MET_py))):-100")
               .Define("Tprime_transverse_mass_first_loosett1l_650","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? sqrt((Lepton1FromHiggs_loosett1l_transverse_energy + Jet1FromHiggs_loosett1l_transverse_energy + Jet2FromHiggs_loosett1l_transverse_energy + Lepton2Fromtop_loosett1l_transverse_energy + bJet1Fromtop_loosett1l_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*63.81*(63.81 + MET_pt_corr)))*(Lepton1FromHiggs_loosett1l_transverse_energy + Jet1FromHiggs_loosett1l_transverse_energy + Jet2FromHiggs_loosett1l_transverse_energy + Lepton2Fromtop_loosett1l_transverse_energy + bJet1Fromtop_loosett1l_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*63.81*(63.81 + MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_py + MET_py))):-100")
               .Define("Tprime_transverse_mass_second_loosett1l_650","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? sqrt((Lepton1FromHiggs_loosett1l_transverse_energy + Jet1FromHiggs_loosett1l_transverse_energy + Jet2FromHiggs_loosett1l_transverse_energy + Lepton2Fromtop_loosett1l_transverse_energy + bJet1Fromtop_loosett1l_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*63.81*(63.81 - MET_pt_corr)))*(Lepton1FromHiggs_loosett1l_transverse_energy + Jet1FromHiggs_loosett1l_transverse_energy + Jet2FromHiggs_loosett1l_transverse_energy + Lepton2Fromtop_loosett1l_transverse_energy + bJet1Fromtop_loosett1l_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*63.81*(63.81 - MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_py + MET_py))):-100")
               .Define("Tprime_transverse_mass_first_loosett1l_675","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? sqrt((Lepton1FromHiggs_loosett1l_transverse_energy + Jet1FromHiggs_loosett1l_transverse_energy + Jet2FromHiggs_loosett1l_transverse_energy + Lepton2Fromtop_loosett1l_transverse_energy + bJet1Fromtop_loosett1l_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*66.39*(66.39 + MET_pt_corr)))*(Lepton1FromHiggs_loosett1l_transverse_energy + Jet1FromHiggs_loosett1l_transverse_energy + Jet2FromHiggs_loosett1l_transverse_energy + Lepton2Fromtop_loosett1l_transverse_energy + bJet1Fromtop_loosett1l_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*66.39*(66.39 + MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_py + MET_py))):-100")
               .Define("Tprime_transverse_mass_second_loosett1l_675","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? sqrt((Lepton1FromHiggs_loosett1l_transverse_energy + Jet1FromHiggs_loosett1l_transverse_energy + Jet2FromHiggs_loosett1l_transverse_energy + Lepton2Fromtop_loosett1l_transverse_energy + bJet1Fromtop_loosett1l_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*66.39*(66.39 - MET_pt_corr)))*(Lepton1FromHiggs_loosett1l_transverse_energy + Jet1FromHiggs_loosett1l_transverse_energy + Jet2FromHiggs_loosett1l_transverse_energy + Lepton2Fromtop_loosett1l_transverse_energy + bJet1Fromtop_loosett1l_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*66.39*(66.39 - MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_py + MET_py))):-100")
               .Define("Tprime_transverse_mass_first_loosett1l_700","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? sqrt((Lepton1FromHiggs_loosett1l_transverse_energy + Jet1FromHiggs_loosett1l_transverse_energy + Jet2FromHiggs_loosett1l_transverse_energy + Lepton2Fromtop_loosett1l_transverse_energy + bJet1Fromtop_loosett1l_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*68.96*(68.96 + MET_pt_corr)))*(Lepton1FromHiggs_loosett1l_transverse_energy + Jet1FromHiggs_loosett1l_transverse_energy + Jet2FromHiggs_loosett1l_transverse_energy + Lepton2Fromtop_loosett1l_transverse_energy + bJet1Fromtop_loosett1l_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*68.96*(68.96 + MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_py + MET_py))):-100")
               .Define("Tprime_transverse_mass_second_loosett1l_700","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? sqrt((Lepton1FromHiggs_loosett1l_transverse_energy + Jet1FromHiggs_loosett1l_transverse_energy + Jet2FromHiggs_loosett1l_transverse_energy + Lepton2Fromtop_loosett1l_transverse_energy + bJet1Fromtop_loosett1l_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*68.96*(68.96 - MET_pt_corr)))*(Lepton1FromHiggs_loosett1l_transverse_energy + Jet1FromHiggs_loosett1l_transverse_energy + Jet2FromHiggs_loosett1l_transverse_energy + Lepton2Fromtop_loosett1l_transverse_energy + bJet1Fromtop_loosett1l_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*68.96*(68.96 - MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_py + MET_py))):-100")
               .Define("Tprime_transverse_mass_first_loosett1l_800","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? sqrt((Lepton1FromHiggs_loosett1l_transverse_energy + Jet1FromHiggs_loosett1l_transverse_energy + Jet2FromHiggs_loosett1l_transverse_energy + Lepton2Fromtop_loosett1l_transverse_energy + bJet1Fromtop_loosett1l_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*79.26*(79.26 + MET_pt_corr)))*(Lepton1FromHiggs_loosett1l_transverse_energy + Jet1FromHiggs_loosett1l_transverse_energy + Jet2FromHiggs_loosett1l_transverse_energy + Lepton2Fromtop_loosett1l_transverse_energy + bJet1Fromtop_loosett1l_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*79.26*(79.26 + MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_py + MET_py))):-100")
               .Define("Tprime_transverse_mass_second_loosett1l_800","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? sqrt((Lepton1FromHiggs_loosett1l_transverse_energy + Jet1FromHiggs_loosett1l_transverse_energy + Jet2FromHiggs_loosett1l_transverse_energy + Lepton2Fromtop_loosett1l_transverse_energy + bJet1Fromtop_loosett1l_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*79.26*(79.26 - MET_pt_corr)))*(Lepton1FromHiggs_loosett1l_transverse_energy + Jet1FromHiggs_loosett1l_transverse_energy + Jet2FromHiggs_loosett1l_transverse_energy + Lepton2Fromtop_loosett1l_transverse_energy + bJet1Fromtop_loosett1l_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*79.26*(79.26 - MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_py + MET_py))):-100")
               .Define("Tprime_transverse_mass_first_loosett1l_900","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? sqrt((Lepton1FromHiggs_loosett1l_transverse_energy + Jet1FromHiggs_loosett1l_transverse_energy + Jet2FromHiggs_loosett1l_transverse_energy + Lepton2Fromtop_loosett1l_transverse_energy + bJet1Fromtop_loosett1l_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*89.56*(89.56 + MET_pt_corr)))*(Lepton1FromHiggs_loosett1l_transverse_energy + Jet1FromHiggs_loosett1l_transverse_energy + Jet2FromHiggs_loosett1l_transverse_energy + Lepton2Fromtop_loosett1l_transverse_energy + bJet1Fromtop_loosett1l_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*89.56*(89.56 + MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_py + MET_py))):-100")
               .Define("Tprime_transverse_mass_second_loosett1l_900","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? sqrt((Lepton1FromHiggs_loosett1l_transverse_energy + Jet1FromHiggs_loosett1l_transverse_energy + Jet2FromHiggs_loosett1l_transverse_energy + Lepton2Fromtop_loosett1l_transverse_energy + bJet1Fromtop_loosett1l_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*89.56*(89.56 - MET_pt_corr)))*(Lepton1FromHiggs_loosett1l_transverse_energy + Jet1FromHiggs_loosett1l_transverse_energy + Jet2FromHiggs_loosett1l_transverse_energy + Lepton2Fromtop_loosett1l_transverse_energy + bJet1Fromtop_loosett1l_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*89.56*(89.56 - MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_py + MET_py))):-100")
               .Define("Tprime_transverse_mass_first_loosett1l_1000","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? sqrt((Lepton1FromHiggs_loosett1l_transverse_energy + Jet1FromHiggs_loosett1l_transverse_energy + Jet2FromHiggs_loosett1l_transverse_energy + Lepton2Fromtop_loosett1l_transverse_energy + bJet1Fromtop_loosett1l_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*99.86*(99.86 + MET_pt_corr)))*(Lepton1FromHiggs_loosett1l_transverse_energy + Jet1FromHiggs_loosett1l_transverse_energy + Jet2FromHiggs_loosett1l_transverse_energy + Lepton2Fromtop_loosett1l_transverse_energy + bJet1Fromtop_loosett1l_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*99.86*(99.86 + MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_py + MET_py))):-100")
               .Define("Tprime_transverse_mass_second_loosett1l_1000","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? sqrt((Lepton1FromHiggs_loosett1l_transverse_energy + Jet1FromHiggs_loosett1l_transverse_energy + Jet2FromHiggs_loosett1l_transverse_energy + Lepton2Fromtop_loosett1l_transverse_energy + bJet1Fromtop_loosett1l_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*99.86*(99.86 - MET_pt_corr)))*(Lepton1FromHiggs_loosett1l_transverse_energy + Jet1FromHiggs_loosett1l_transverse_energy + Jet2FromHiggs_loosett1l_transverse_energy + Lepton2Fromtop_loosett1l_transverse_energy + bJet1Fromtop_loosett1l_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*99.86*(99.86 - MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_py + MET_py))):-100")
               .Define("Tprime_transverse_mass_first_loosett1l_1100","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? sqrt((Lepton1FromHiggs_loosett1l_transverse_energy + Jet1FromHiggs_loosett1l_transverse_energy + Jet2FromHiggs_loosett1l_transverse_energy + Lepton2Fromtop_loosett1l_transverse_energy + bJet1Fromtop_loosett1l_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*110.16*(110.16 + MET_pt_corr)))*(Lepton1FromHiggs_loosett1l_transverse_energy + Jet1FromHiggs_loosett1l_transverse_energy + Jet2FromHiggs_loosett1l_transverse_energy + Lepton2Fromtop_loosett1l_transverse_energy + bJet1Fromtop_loosett1l_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*110.16*(110.16 + MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_py + MET_py))):-100")
               .Define("Tprime_transverse_mass_second_loosett1l_1100","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? sqrt((Lepton1FromHiggs_loosett1l_transverse_energy + Jet1FromHiggs_loosett1l_transverse_energy + Jet2FromHiggs_loosett1l_transverse_energy + Lepton2Fromtop_loosett1l_transverse_energy + bJet1Fromtop_loosett1l_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*110.16*(110.16 - MET_pt_corr)))*(Lepton1FromHiggs_loosett1l_transverse_energy + Jet1FromHiggs_loosett1l_transverse_energy + Jet2FromHiggs_loosett1l_transverse_energy + Lepton2Fromtop_loosett1l_transverse_energy + bJet1Fromtop_loosett1l_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*110.16*(110.16 - MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_py + MET_py))):-100")
               .Define("Tprime_transverse_mass_first_loosett1l_1200","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? sqrt((Lepton1FromHiggs_loosett1l_transverse_energy + Jet1FromHiggs_loosett1l_transverse_energy + Jet2FromHiggs_loosett1l_transverse_energy + Lepton2Fromtop_loosett1l_transverse_energy + bJet1Fromtop_loosett1l_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*120.46*(120.46 + MET_pt_corr)))*(Lepton1FromHiggs_loosett1l_transverse_energy + Jet1FromHiggs_loosett1l_transverse_energy + Jet2FromHiggs_loosett1l_transverse_energy + Lepton2Fromtop_loosett1l_transverse_energy + bJet1Fromtop_loosett1l_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*120.46*(120.46 + MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_py + MET_py))):-100")
               .Define("Tprime_transverse_mass_second_loosett1l_1200","(Selected_lepton_loosett1l_number > 1 && Selected_clean_jet_loosett1l_number >=3 && Selected_clean_bjet_loosett1l_number >= 1) ? sqrt((Lepton1FromHiggs_loosett1l_transverse_energy + Jet1FromHiggs_loosett1l_transverse_energy + Jet2FromHiggs_loosett1l_transverse_energy + Lepton2Fromtop_loosett1l_transverse_energy + bJet1Fromtop_loosett1l_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*120.46*(120.46 - MET_pt_corr)))*(Lepton1FromHiggs_loosett1l_transverse_energy + Jet1FromHiggs_loosett1l_transverse_energy + Jet2FromHiggs_loosett1l_transverse_energy + Lepton2Fromtop_loosett1l_transverse_energy + bJet1Fromtop_loosett1l_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*120.46*(120.46 - MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_py + MET_py))):-100");

    _rlm = _rlm.Define("St","Sum(Selected_electron_pt) + Sum(Selected_muon_pt) + Sum(Selected_clean_jet_pt)")
               .Define("St_with_MET","Sum(Selected_electron_pt) + Sum(Selected_muon_pt) + Sum(Selected_clean_jet_pt) + MET_pt_corr");

    _rlm = _rlm.Define("dis_comb_leptonbjet",::distinct_comb_2,{"Selected_lepton_number","Selected_clean_bjet_number"})
               .Define("Selected_lepton1","ROOT::VecOps::Take(lep4Rvecs, dis_comb_leptonbjet[0])")
               .Define("Selected_clean_bjet1","ROOT::VecOps::Take(cleanbjet4Rvecs, dis_comb_leptonbjet[1])")
               .Define("LeptonbjetFromtop4Rvecs",::sum_two_4Rvecs,{"Selected_lepton1","Selected_clean_bjet1"})
               .Define("LeptonbjetFromtop_pt","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ROOT::VecOps::Map(LeptonbjetFromtop4Rvecs, FourVecPt):Selected_lepton_pt")
               .Define("LeptonbjetFromtop_eta","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ROOT::VecOps::Map(LeptonbjetFromtop4Rvecs, FourVecEta):Selected_lepton_pt")
               .Define("LeptonbjetFromtop_phi","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ROOT::VecOps::Map(LeptonbjetFromtop4Rvecs, FourVecPhi):Selected_lepton_pt")
               .Define("LeptonbjetFromtop_mass","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ROOT::VecOps::Map(LeptonbjetFromtop4Rvecs, FourVecMass):Selected_lepton_pt")
               .Define("LeptonbjetFromtop_energy","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ROOT::VecOps::Map(LeptonbjetFromtop4Rvecs, FourVecEnergy):Selected_lepton_pt")
               .Define("LeptonbjetFromtop_transverse_energy","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt(LeptonbjetFromtop_pt*LeptonbjetFromtop_pt + LeptonbjetFromtop_mass*LeptonbjetFromtop_mass):Selected_lepton_pt")
               .Define("LeptonbjetFromtop_deltaeta","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? abs(ROOT::VecOps::Take(Selected_lepton_eta, dis_comb_leptonbjet[0]) - ROOT::VecOps::Take(Selected_clean_bjet_eta, dis_comb_leptonbjet[1])):Selected_lepton_pt")
               .Define("LeptonbjetFromtop_deltaphi","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? abs(ROOT::VecOps::DeltaPhi(ROOT::VecOps::Take(Selected_lepton_phi, dis_comb_leptonbjet[0]),ROOT::VecOps::Take(Selected_clean_bjet_phi, dis_comb_leptonbjet[1]))):Selected_lepton_pt")
               .Define("LeptonbjetFromtop_deltaR","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ROOT::VecOps::DeltaR(ROOT::VecOps::Take(Selected_lepton_eta, dis_comb_leptonbjet[0]),ROOT::VecOps::Take(Selected_clean_bjet_eta, dis_comb_leptonbjet[1]),ROOT::VecOps::Take(Selected_lepton_phi, dis_comb_leptonbjet[0]),ROOT::VecOps::Take(Selected_clean_bjet_phi, dis_comb_leptonbjet[1])):Selected_lepton_pt")
               .Define("LeptonbjetFromtop_transverse_mass","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt(ROOT::VecOps::Take(Selected_lepton_mass, dis_comb_leptonbjet[0])*ROOT::VecOps::Take(Selected_lepton_mass, dis_comb_leptonbjet[0]) + ROOT::VecOps::Take(Selected_clean_bjet_mass, dis_comb_leptonbjet[1])*ROOT::VecOps::Take(Selected_clean_bjet_mass, dis_comb_leptonbjet[1]) + 2*ROOT::VecOps::Take(Selected_lepton_transverse_energy, dis_comb_leptonbjet[0])*ROOT::VecOps::Take(Selected_clean_bjet_transverse_energy, dis_comb_leptonbjet[1]) - 2*ROOT::VecOps::Take(Selected_lepton_pt, dis_comb_leptonbjet[0])*ROOT::VecOps::Take(Selected_clean_bjet_pt, dis_comb_leptonbjet[1])*cos(LeptonbjetFromtop_deltaphi)):Selected_lepton_pt");

    _rlm = _rlm.Define("Min_deltaR_bJetFromtop_leptonFromtop",::minDR_1,{"Selected_lepton_eta","Selected_clean_bjet_eta","Selected_lepton_phi","Selected_clean_bjet_phi"})
               .Define("Lepton2Fromtop","lep4vecs[Min_deltaR_bJetFromtop_leptonFromtop[0]]")
               .Define("Lepton2Fromtop_pt","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? (lep4vecs[Min_deltaR_bJetFromtop_leptonFromtop[0]]).Pt():-100")
               .Define("Lepton2Fromtop_eta","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? (lep4vecs[Min_deltaR_bJetFromtop_leptonFromtop[0]]).Eta():-10")
               .Define("Lepton2Fromtop_phi","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? (lep4vecs[Min_deltaR_bJetFromtop_leptonFromtop[0]]).Phi():-10")
               .Define("Lepton2Fromtop_mass","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? (lep4vecs[Min_deltaR_bJetFromtop_leptonFromtop[0]]).M():-100")
               .Define("Lepton2Fromtop_transverse_energy","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt(Lepton2Fromtop_pt*Lepton2Fromtop_pt + Lepton2Fromtop_mass*Lepton2Fromtop_mass):-100")
               .Define("bJet1Fromtop","cleanbjet4vecs[Min_deltaR_bJetFromtop_leptonFromtop[1]]")
               .Define("bJet1Fromtop_pt","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? (cleanbjet4vecs[Min_deltaR_bJetFromtop_leptonFromtop[1]]).Pt():-100")
               .Define("bJet1Fromtop_eta","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? (cleanbjet4vecs[Min_deltaR_bJetFromtop_leptonFromtop[1]]).Eta():-10")
               .Define("bJet1Fromtop_phi","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? (cleanbjet4vecs[Min_deltaR_bJetFromtop_leptonFromtop[1]]).Phi():-10")
               .Define("bJet1Fromtop_mass","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? (cleanbjet4vecs[Min_deltaR_bJetFromtop_leptonFromtop[1]]).M():-100")
               .Define("bJet1Fromtop_transverse_energy","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt(bJet1Fromtop_pt*bJet1Fromtop_pt + bJet1Fromtop_mass*bJet1Fromtop_mass):-100")
               .Define("Lepton1FromHiggs","(Min_deltaR_bJetFromtop_leptonFromtop[0] == 0) ? lep4vecs[1]:lep4vecs[0]")
               .Define("Lepton1FromHiggs_pt","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ((Min_deltaR_bJetFromtop_leptonFromtop[0] == 0) ? (lep4vecs[1]).Pt():(lep4vecs[0]).Pt()):-100")
               .Define("Lepton1FromHiggs_eta","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ((Min_deltaR_bJetFromtop_leptonFromtop[0] == 0) ? (lep4vecs[1]).Eta():(lep4vecs[0]).Eta()):-100")
               .Define("Lepton1FromHiggs_phi","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ((Min_deltaR_bJetFromtop_leptonFromtop[0] == 0) ? (lep4vecs[1]).Phi():(lep4vecs[0]).Phi()):-100")
    	       .Define("Lepton1FromHiggs_mass","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ((Min_deltaR_bJetFromtop_leptonFromtop[0] == 0) ? (lep4vecs[1]).M():(lep4vecs[0]).M()):-100")
               .Define("Lepton1FromHiggs_transverse_energy","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt(Lepton1FromHiggs_pt*Lepton1FromHiggs_pt + Lepton1FromHiggs_mass*Lepton1FromHiggs_mass):-100");

    _rlm = _rlm.Define("cleanjet4Rvecs_withoutbjet",::removebjet,{"cleanjet4Rvecs","Selected_clean_jet_pt","bJet1Fromtop_pt"})
               .Define("cleanjet4Rvecs_withoutbjet_pt","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ROOT::VecOps::Map(cleanjet4Rvecs_withoutbjet, FourVecPt):Selected_lepton_pt")
               .Define("cleanjet4Rvecs_withoutbjet_number","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? int(cleanjet4Rvecs_withoutbjet_pt.size()):-10")
               .Define("cleanjet4Rvecs_withoutbjet_eta","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ROOT::VecOps::Map(cleanjet4Rvecs_withoutbjet, FourVecEta):Selected_lepton_pt")
               .Define("cleanjet4Rvecs_withoutbjet_phi","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ROOT::VecOps::Map(cleanjet4Rvecs_withoutbjet, FourVecPhi):Selected_lepton_pt")
               .Define("cleanjet4Rvecs_withoutbjet_mass","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ROOT::VecOps::Map(cleanjet4Rvecs_withoutbjet, FourVecMass):Selected_lepton_pt")
               .Define("cleanjet4Rvecs_withoutbjet_transverse_energy","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt(cleanjet4Rvecs_withoutbjet_pt*cleanjet4Rvecs_withoutbjet_pt + cleanjet4Rvecs_withoutbjet_mass*cleanjet4Rvecs_withoutbjet_mass):Selected_lepton_pt")
               .Define("cleanjet4vecs_withoutbjet",::generate_4vec,{"cleanjet4Rvecs_withoutbjet_pt","cleanjet4Rvecs_withoutbjet_eta","cleanjet4Rvecs_withoutbjet_phi","cleanjet4Rvecs_withoutbjet_mass"})
               .Define("dis_comb_jets_withoutbjet",::distinct_comb_2_bis,{"cleanjet4Rvecs_withoutbjet_number"})
               .Define("Selected_jet1","ROOT::VecOps::Take(cleanjet4Rvecs_withoutbjet, dis_comb_jets_withoutbjet[0])")
               .Define("Selected_jet2","ROOT::VecOps::Take(cleanjet4Rvecs_withoutbjet, dis_comb_jets_withoutbjet[1])")
               .Define("JetsFromHiggs4Rvecs",::sum_two_4Rvecs,{"Selected_jet1","Selected_jet2"})
               .Define("JetsFromHiggs_pt","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ROOT::VecOps::Map(JetsFromHiggs4Rvecs, FourVecPt):Selected_lepton_pt")
               .Define("JetsFromHiggs_eta","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ROOT::VecOps::Map(JetsFromHiggs4Rvecs, FourVecEta):Selected_lepton_pt")
               .Define("JetsFromHiggs_phi","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ROOT::VecOps::Map(JetsFromHiggs4Rvecs, FourVecPhi):Selected_lepton_pt")
               .Define("JetsFromHiggs_mass","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ROOT::VecOps::Map(JetsFromHiggs4Rvecs, FourVecMass):Selected_lepton_pt")
               .Define("JetsFromHiggs_energy","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ROOT::VecOps::Map(JetsFromHiggs4Rvecs, FourVecEnergy):Selected_lepton_pt")
               .Define("JetsFromHiggs_transverse_energy","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt(JetsFromHiggs_pt*JetsFromHiggs_pt + JetsFromHiggs_mass*JetsFromHiggs_mass):Selected_lepton_pt")
               .Define("JetsFromHiggs_deltaeta","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? abs(ROOT::VecOps::Take(cleanjet4Rvecs_withoutbjet_eta, dis_comb_jets_withoutbjet[0]) - ROOT::VecOps::Take(cleanjet4Rvecs_withoutbjet_eta, dis_comb_jets_withoutbjet[1])):Selected_lepton_pt")
               .Define("JetsFromHiggs_deltaphi","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? abs(ROOT::VecOps::DeltaPhi(ROOT::VecOps::Take(cleanjet4Rvecs_withoutbjet_phi, dis_comb_jets_withoutbjet[0]),ROOT::VecOps::Take(cleanjet4Rvecs_withoutbjet_phi, dis_comb_jets_withoutbjet[1]))):Selected_lepton_pt")
               .Define("JetsFromHiggs_deltaR","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ROOT::VecOps::DeltaR(ROOT::VecOps::Take(cleanjet4Rvecs_withoutbjet_eta, dis_comb_jets_withoutbjet[0]),ROOT::VecOps::Take(cleanjet4Rvecs_withoutbjet_eta, dis_comb_jets_withoutbjet[1]),ROOT::VecOps::Take(cleanjet4Rvecs_withoutbjet_phi, dis_comb_jets_withoutbjet[0]),ROOT::VecOps::Take(cleanjet4Rvecs_withoutbjet_phi, dis_comb_jets_withoutbjet[1])):Selected_lepton_pt")
               .Define("JetsFromHiggs_transverse_mass","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt(ROOT::VecOps::Take(cleanjet4Rvecs_withoutbjet_mass, dis_comb_jets_withoutbjet[0])*ROOT::VecOps::Take(cleanjet4Rvecs_withoutbjet_mass, dis_comb_jets_withoutbjet[0]) + ROOT::VecOps::Take(cleanjet4Rvecs_withoutbjet_mass, dis_comb_jets_withoutbjet[1])*ROOT::VecOps::Take(cleanjet4Rvecs_withoutbjet_mass, dis_comb_jets_withoutbjet[1]) + 2*ROOT::VecOps::Take(cleanjet4Rvecs_withoutbjet_transverse_energy, dis_comb_jets_withoutbjet[0])*ROOT::VecOps::Take(cleanjet4Rvecs_withoutbjet_transverse_energy, dis_comb_jets_withoutbjet[1]) - 2*ROOT::VecOps::Take(cleanjet4Rvecs_withoutbjet_pt, dis_comb_jets_withoutbjet[0])*ROOT::VecOps::Take(cleanjet4Rvecs_withoutbjet_pt, dis_comb_jets_withoutbjet[1])*cos(JetsFromHiggs_deltaphi)):Selected_lepton_pt");

    _rlm = _rlm.Define("Min_deltaR_JetsFromHiggs",::minDR_2,{"cleanjet4Rvecs_withoutbjet_eta","cleanjet4Rvecs_withoutbjet_eta","cleanjet4Rvecs_withoutbjet_phi","cleanjet4Rvecs_withoutbjet_phi"})
    	       .Define("Jet1FromHiggs","cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[0]]")
               .Define("Jet1FromHiggs_pt","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? (cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[0]]).Pt():-100")
               .Define("Jet1FromHiggs_eta","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? (cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[0]]).Eta():-10")
               .Define("Jet1FromHiggs_phi","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? (cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[0]]).Phi():-10")
               .Define("Jet1FromHiggs_mass","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? (cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[0]]).M():-100")
               .Define("Jet1FromHiggs_transverse_energy","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt(Jet1FromHiggs_pt*Jet1FromHiggs_pt + Jet1FromHiggs_mass*Jet1FromHiggs_mass):-100")
               .Define("Jet2FromHiggs","cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[1]]")
               .Define("Jet2FromHiggs_pt","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? (cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[1]]).Pt():-100")
               .Define("Jet2FromHiggs_eta","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? (cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[1]]).Eta():-10")
               .Define("Jet2FromHiggs_phi","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? (cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[1]]).Phi():-10")
               .Define("Jet2FromHiggs_mass","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? (cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[1]]).M():-100")
               .Define("Jet2FromHiggs_transverse_energy","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt(Jet2FromHiggs_pt*Jet2FromHiggs_pt + Jet2FromHiggs_mass*Jet2FromHiggs_mass):-100")
               .Define("Jet1FromHiggs_Jet2FromHiggs","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[0]] + cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[0]]:lep4vecs[0]")
               .Define("Jet1FromHiggs_Jet2FromHiggs_pt","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? (cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[0]] + cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[0]]).Pt():-100")
               .Define("Jet1FromHiggs_Jet2FromHiggs_eta","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? (cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[0]] + cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[0]]).Eta():-10")
               .Define("Jet1FromHiggs_Jet2FromHiggs_phi","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? (cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[0]] + cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[0]]).Phi():-10")
               .Define("Jet1FromHiggs_Jet2FromHiggs_mass","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? (cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[0]] + cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[0]]).M():-100")
               .Define("Jet1FromHiggs_Jet2FromHiggs_transverse_energy","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt(Jet1FromHiggs_Jet2FromHiggs_pt*Jet1FromHiggs_Jet2FromHiggs_pt + Jet1FromHiggs_Jet2FromHiggs_mass*Jet1FromHiggs_Jet2FromHiggs_mass):-100")
               .Define("Jet1FromHiggs_Jet2FromHiggs_deltaeta","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? abs(Jet2FromHiggs_eta - Jet1FromHiggs_eta):-10")
               .Define("Jet1FromHiggs_Jet2FromHiggs_deltaphi","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? abs(ROOT::VecOps::DeltaPhi(Jet1FromHiggs_phi,Jet2FromHiggs_phi)):-10")
               .Define("Jet1FromHiggs_Jet2FromHiggs_deltaR","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ROOT::VecOps::DeltaR(Jet1FromHiggs_eta,Jet2FromHiggs_eta,Jet1FromHiggs_phi,Jet2FromHiggs_phi):-10")
               .Define("Jet1FromHiggs_Jet2FromHiggs_transverse_mass","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt(Jet1FromHiggs_mass*Jet1FromHiggs_mass + Jet2FromHiggs_mass*Jet2FromHiggs_mass + 2*Jet1FromHiggs_transverse_energy*Jet2FromHiggs_transverse_energy - 2*Jet1FromHiggs_pt*Jet2FromHiggs_pt*cos(Jet1FromHiggs_Jet2FromHiggs_deltaphi)):-100");

    _rlm = _rlm.Define("Vectorial_sum_three_clean_jets",::Threejets,{"cleanjet4vecs","Selected_clean_jet_eta","Selected_clean_jet_phi","Selected_clean_jet_number","Selected_clean_bjet_number","bJet1Fromtop_eta","bJet1Fromtop_phi"})
               .Define("Vectorial_sum_three_clean_jets_pt","(Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ROOT::VecOps::Map(Vectorial_sum_three_clean_jets, FourVecPt):Selected_clean_jet_pt")
               .Define("Vectorial_sum_three_clean_jets_eta","(Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ROOT::VecOps::Map(Vectorial_sum_three_clean_jets, FourVecEta):Selected_clean_jet_pt")
               .Define("Vectorial_sum_three_clean_jets_phi","(Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ROOT::VecOps::Map(Vectorial_sum_three_clean_jets, FourVecPhi):Selected_clean_jet_pt")
               .Define("Vectorial_sum_three_clean_jets_mass","(Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ROOT::VecOps::Map(Vectorial_sum_three_clean_jets, FourVecMass):Selected_clean_jet_pt")
               .Define("Vectorial_sum_three_clean_jets_mass_offset","(Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? Vectorial_sum_three_clean_jets_mass[0] - 172.7:-100")
               .Define("Vectorial_sum_three_clean_jets_mass_min","(Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? abs(Vectorial_sum_three_clean_jets_mass_offset):-100");

    _rlm = _rlm.Define("Lepton2Fromtop_bJet1Fromtop","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? lep4vecs[Min_deltaR_bJetFromtop_leptonFromtop[0]] + cleanbjet4vecs[Min_deltaR_bJetFromtop_leptonFromtop[1]]:lep4vecs[0]")
               .Define("Lepton2Fromtop_bJet1Fromtop_pt","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? (lep4vecs[Min_deltaR_bJetFromtop_leptonFromtop[0]] + cleanbjet4vecs[Min_deltaR_bJetFromtop_leptonFromtop[1]]).Pt():-100")
               .Define("Lepton2Fromtop_bJet1Fromtop_eta","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? (lep4vecs[Min_deltaR_bJetFromtop_leptonFromtop[0]] + cleanbjet4vecs[Min_deltaR_bJetFromtop_leptonFromtop[1]]).Eta():-10")
               .Define("Lepton2Fromtop_bJet1Fromtop_phi","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? (lep4vecs[Min_deltaR_bJetFromtop_leptonFromtop[0]] + cleanbjet4vecs[Min_deltaR_bJetFromtop_leptonFromtop[1]]).Phi():-10")
               .Define("Lepton2Fromtop_bJet1Fromtop_mass","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? (lep4vecs[Min_deltaR_bJetFromtop_leptonFromtop[0]] + cleanbjet4vecs[Min_deltaR_bJetFromtop_leptonFromtop[1]]).M():-100")
               .Define("Lepton2Fromtop_bJet1Fromtop_transverse_energy","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt(Lepton2Fromtop_bJet1Fromtop_pt*Lepton2Fromtop_bJet1Fromtop_pt + Lepton2Fromtop_bJet1Fromtop_mass*Lepton2Fromtop_bJet1Fromtop_mass):-100")
               .Define("Lepton2Fromtop_bJet1Fromtop_deltaeta","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? abs(Lepton2Fromtop_eta - bJet1Fromtop_eta):-10")
               .Define("Lepton2Fromtop_bJet1Fromtop_deltaphi","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? abs(ROOT::VecOps::DeltaPhi(Lepton2Fromtop_phi,bJet1Fromtop_phi)):-10")
               .Define("Lepton2Fromtop_bJet1Fromtop_deltaR","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ROOT::VecOps::DeltaR(Lepton2Fromtop_eta,bJet1Fromtop_eta,Lepton2Fromtop_phi,bJet1Fromtop_phi):-10")
               .Define("Lepton2Fromtop_bJet1Fromtop_transverse_mass","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt(Lepton2Fromtop_mass*Lepton2Fromtop_mass + bJet1Fromtop_mass*bJet1Fromtop_mass + 2*Lepton2Fromtop_transverse_energy*bJet1Fromtop_transverse_energy - 2*Lepton2Fromtop_pt*bJet1Fromtop_pt*cos(Lepton2Fromtop_bJet1Fromtop_deltaphi)):-100")
               .Define("Lepton2Fromtop_Jet1FromHiggs","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? lep4vecs[Min_deltaR_bJetFromtop_leptonFromtop[0]] + cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[0]]:lep4vecs[0]")
               .Define("Lepton2Fromtop_Jet1FromHiggs_pt","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? (lep4vecs[Min_deltaR_bJetFromtop_leptonFromtop[0]] + cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[0]]).Pt():-100")
               .Define("Lepton2Fromtop_Jet1FromHiggs_eta","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? (lep4vecs[Min_deltaR_bJetFromtop_leptonFromtop[0]] + cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[0]]).Eta():-10")
               .Define("Lepton2Fromtop_Jet1FromHiggs_phi","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? (lep4vecs[Min_deltaR_bJetFromtop_leptonFromtop[0]] + cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[0]]).Phi():-10")
               .Define("Lepton2Fromtop_Jet1FromHiggs_mass","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? (lep4vecs[Min_deltaR_bJetFromtop_leptonFromtop[0]] + cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[0]]).M():-100")
               .Define("Lepton2Fromtop_Jet1FromHiggs_transverse_energy","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt(Lepton2Fromtop_Jet1FromHiggs_pt*Lepton2Fromtop_Jet1FromHiggs_pt + Lepton2Fromtop_Jet1FromHiggs_mass*Lepton2Fromtop_Jet1FromHiggs_mass):-100")
               .Define("Lepton2Fromtop_Jet1FromHiggs_deltaeta","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? abs(Lepton2Fromtop_eta - Jet1FromHiggs_eta):-10")
               .Define("Lepton2Fromtop_Jet1FromHiggs_deltaphi","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? abs(ROOT::VecOps::DeltaPhi(Lepton2Fromtop_phi,Jet1FromHiggs_phi)):-10")
               .Define("Lepton2Fromtop_Jet1FromHiggs_deltaR","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ROOT::VecOps::DeltaR(Lepton2Fromtop_eta,Jet1FromHiggs_eta,Lepton2Fromtop_phi,Jet1FromHiggs_phi):-10")
               .Define("Lepton2Fromtop_Jet1FromHiggs_transverse_mass","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt(Lepton2Fromtop_mass*Lepton2Fromtop_mass + Jet1FromHiggs_mass*Jet1FromHiggs_mass + 2*Lepton2Fromtop_transverse_energy*Jet1FromHiggs_transverse_energy - 2*Lepton2Fromtop_pt*Jet1FromHiggs_pt*cos(Lepton2Fromtop_Jet1FromHiggs_deltaphi)):-100")
               .Define("Lepton2Fromtop_Jet2FromHiggs","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? lep4vecs[Min_deltaR_bJetFromtop_leptonFromtop[0]] + cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[1]]:lep4vecs[0]")
               .Define("Lepton2Fromtop_Jet2FromHiggs_pt","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? (lep4vecs[Min_deltaR_bJetFromtop_leptonFromtop[0]] + cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[1]]).Pt():-100")
               .Define("Lepton2Fromtop_Jet2FromHiggs_eta","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? (lep4vecs[Min_deltaR_bJetFromtop_leptonFromtop[0]] + cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[1]]).Eta():-10")
               .Define("Lepton2Fromtop_Jet2FromHiggs_phi","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? (lep4vecs[Min_deltaR_bJetFromtop_leptonFromtop[0]] + cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[1]]).Phi():-10")
               .Define("Lepton2Fromtop_Jet2FromHiggs_mass","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? (lep4vecs[Min_deltaR_bJetFromtop_leptonFromtop[0]] + cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[1]]).M():-10")
               .Define("Lepton2Fromtop_Jet2FromHiggs_transverse_energy","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt(Lepton2Fromtop_Jet2FromHiggs_pt*Lepton2Fromtop_Jet2FromHiggs_pt + Lepton2Fromtop_Jet2FromHiggs_mass*Lepton2Fromtop_Jet2FromHiggs_mass):-100")
               .Define("Lepton2Fromtop_Jet2FromHiggs_deltaeta","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? abs(Lepton2Fromtop_eta - Jet2FromHiggs_eta):-10")
               .Define("Lepton2Fromtop_Jet2FromHiggs_deltaphi","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? abs(ROOT::VecOps::DeltaPhi(Lepton2Fromtop_phi,Jet2FromHiggs_phi)):-10")
               .Define("Lepton2Fromtop_Jet2FromHiggs_deltaR","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ROOT::VecOps::DeltaR(Lepton2Fromtop_eta,Jet2FromHiggs_eta,Lepton2Fromtop_phi,Jet2FromHiggs_phi):-10")
               .Define("Lepton2Fromtop_Jet2FromHiggs_transverse_mass","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt(Lepton2Fromtop_mass*Lepton2Fromtop_mass + Jet2FromHiggs_mass*Jet2FromHiggs_mass + 2*Lepton2Fromtop_transverse_energy*Jet2FromHiggs_transverse_energy - 2*Lepton2Fromtop_pt*Jet2FromHiggs_pt*cos(Lepton2Fromtop_Jet2FromHiggs_deltaphi)):-100")
               .Define("Lepton1FromHiggs_bJet1Fromtop","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ((Min_deltaR_bJetFromtop_leptonFromtop[0] == 0) ? lep4vecs[1]+cleanbjet4vecs[Min_deltaR_bJetFromtop_leptonFromtop[1]]:lep4vecs[0]+cleanbjet4vecs[Min_deltaR_bJetFromtop_leptonFromtop[1]]):lep4vecs[0]")
               .Define("Lepton1FromHiggs_bJet1Fromtop_pt","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ((Min_deltaR_bJetFromtop_leptonFromtop[0] == 0) ? (lep4vecs[1]+cleanbjet4vecs[Min_deltaR_bJetFromtop_leptonFromtop[1]]).Pt():(lep4vecs[0]+cleanbjet4vecs[Min_deltaR_bJetFromtop_leptonFromtop[1]]).Pt()):-100")
               .Define("Lepton1FromHiggs_bJet1Fromtop_eta","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ((Min_deltaR_bJetFromtop_leptonFromtop[0] == 0) ? (lep4vecs[1]+cleanbjet4vecs[Min_deltaR_bJetFromtop_leptonFromtop[1]]).Eta():(lep4vecs[0]+cleanbjet4vecs[Min_deltaR_bJetFromtop_leptonFromtop[1]]).Eta()):-10")
               .Define("Lepton1FromHiggs_bJet1Fromtop_phi","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ((Min_deltaR_bJetFromtop_leptonFromtop[0] == 0) ? (lep4vecs[1]+cleanbjet4vecs[Min_deltaR_bJetFromtop_leptonFromtop[1]]).Phi():(lep4vecs[0]+cleanbjet4vecs[Min_deltaR_bJetFromtop_leptonFromtop[1]]).Phi()):-10")
               .Define("Lepton1FromHiggs_bJet1Fromtop_mass","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ((Min_deltaR_bJetFromtop_leptonFromtop[0] == 0) ? (lep4vecs[1]+cleanbjet4vecs[Min_deltaR_bJetFromtop_leptonFromtop[1]]).M():(lep4vecs[0]+cleanbjet4vecs[Min_deltaR_bJetFromtop_leptonFromtop[1]]).M()):-100")
               .Define("Lepton1FromHiggs_bJet1Fromtop_transverse_energy","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt(Lepton1FromHiggs_bJet1Fromtop_pt*Lepton1FromHiggs_bJet1Fromtop_pt + Lepton1FromHiggs_bJet1Fromtop_mass*Lepton1FromHiggs_bJet1Fromtop_mass):-100")
               .Define("Lepton1FromHiggs_bJet1Fromtop_deltaeta","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? abs(Lepton1FromHiggs_eta - bJet1Fromtop_eta):-10")
               .Define("Lepton1FromHiggs_bJet1Fromtop_deltaphi","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? abs(ROOT::VecOps::DeltaPhi(Lepton1FromHiggs_phi,bJet1Fromtop_phi)):-10")
               .Define("Lepton1FromHiggs_bJet1Fromtop_deltaR","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ROOT::VecOps::DeltaR(Lepton1FromHiggs_eta,bJet1Fromtop_eta,Lepton1FromHiggs_phi,bJet1Fromtop_phi):-10")
               .Define("Lepton1FromHiggs_bJet1Fromtop_transverse_mass","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt(Lepton1FromHiggs_mass*Lepton1FromHiggs_mass + bJet1Fromtop_mass*bJet1Fromtop_mass + 2*Lepton1FromHiggs_transverse_energy*bJet1Fromtop_transverse_energy - 2*Lepton1FromHiggs_pt*bJet1Fromtop_pt*cos(Lepton1FromHiggs_bJet1Fromtop_deltaphi)):-100")
               .Define("Lepton1FromHiggs_Jet1FromHiggs","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ((Min_deltaR_bJetFromtop_leptonFromtop[0] == 0) ? lep4vecs[1]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[0]]:lep4vecs[0]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[0]]):lep4vecs[0]")
               .Define("Lepton1FromHiggs_Jet1FromHiggs_pt","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ((Min_deltaR_bJetFromtop_leptonFromtop[0] == 0) ? (lep4vecs[1]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[0]]).Pt():(lep4vecs[0]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[0]]).Pt()):-100")
               .Define("Lepton1FromHiggs_Jet1FromHiggs_eta","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ((Min_deltaR_bJetFromtop_leptonFromtop[0] == 0) ? (lep4vecs[1]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[0]]).Eta():(lep4vecs[0]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[0]]).Eta()):-10")
               .Define("Lepton1FromHiggs_Jet1FromHiggs_phi","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ((Min_deltaR_bJetFromtop_leptonFromtop[0] == 0) ? (lep4vecs[1]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[0]]).Phi():(lep4vecs[0]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[0]]).Phi()):-10")
               .Define("Lepton1FromHiggs_Jet1FromHiggs_mass","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ((Min_deltaR_bJetFromtop_leptonFromtop[0] == 0) ? (lep4vecs[1]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[0]]).M():(lep4vecs[0]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[0]]).M()):-100")
               .Define("Lepton1FromHiggs_Jet1FromHiggs_transverse_energy","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt(Lepton1FromHiggs_Jet1FromHiggs_pt*Lepton1FromHiggs_Jet1FromHiggs_pt + Lepton1FromHiggs_Jet1FromHiggs_mass*Lepton1FromHiggs_Jet1FromHiggs_mass):-100")
               .Define("Lepton1FromHiggs_Jet1FromHiggs_deltaeta","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? abs(Lepton1FromHiggs_eta - Jet1FromHiggs_eta):-10")
               .Define("Lepton1FromHiggs_Jet1FromHiggs_deltaphi","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? abs(ROOT::VecOps::DeltaPhi(Lepton1FromHiggs_phi,Jet1FromHiggs_phi)):-10")
               .Define("Lepton1FromHiggs_Jet1FromHiggs_deltaR","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ROOT::VecOps::DeltaR(Lepton1FromHiggs_eta,Jet1FromHiggs_eta,Lepton1FromHiggs_phi,Jet1FromHiggs_phi):-10")
               .Define("Lepton1FromHiggs_Jet1FromHiggs_transverse_mass","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt(Lepton1FromHiggs_mass*Lepton1FromHiggs_mass + Jet1FromHiggs_mass*Jet1FromHiggs_mass + 2*Lepton1FromHiggs_transverse_energy*Jet1FromHiggs_transverse_energy - 2*Lepton1FromHiggs_pt*Jet1FromHiggs_pt*cos(Lepton1FromHiggs_Jet1FromHiggs_deltaphi)):-100")
               .Define("Lepton1FromHiggs_Jet2FromHiggs","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ((Min_deltaR_bJetFromtop_leptonFromtop[0] == 0) ? lep4vecs[1]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[1]]:lep4vecs[0]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[1]]):lep4vecs[0]")
               .Define("Lepton1FromHiggs_Jet2FromHiggs_pt","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ((Min_deltaR_bJetFromtop_leptonFromtop[0] == 0) ? (lep4vecs[1]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[1]]).Pt():(lep4vecs[0]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[1]]).Pt()):-100")
               .Define("Lepton1FromHiggs_Jet2FromHiggs_eta","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ((Min_deltaR_bJetFromtop_leptonFromtop[0] == 0) ? (lep4vecs[1]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[1]]).Eta():(lep4vecs[0]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[1]]).Eta()):-10")
               .Define("Lepton1FromHiggs_Jet2FromHiggs_phi","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ((Min_deltaR_bJetFromtop_leptonFromtop[0] == 0) ? (lep4vecs[1]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[1]]).Phi():(lep4vecs[0]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[1]]).Phi()):-10")
               .Define("Lepton1FromHiggs_Jet2FromHiggs_mass","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ((Min_deltaR_bJetFromtop_leptonFromtop[0] == 0) ? (lep4vecs[1]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[1]]).M():(lep4vecs[0]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[1]]).M()):-100")
               .Define("Lepton1FromHiggs_Jet2FromHiggs_transverse_energy","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt(Lepton1FromHiggs_Jet2FromHiggs_pt*Lepton1FromHiggs_Jet2FromHiggs_pt + Lepton1FromHiggs_Jet2FromHiggs_mass*Lepton1FromHiggs_Jet2FromHiggs_mass):-100")
               .Define("Lepton1FromHiggs_Jet2FromHiggs_deltaeta","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? abs(Lepton1FromHiggs_eta - Jet2FromHiggs_eta):-10")
               .Define("Lepton1FromHiggs_Jet2FromHiggs_deltaphi","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? abs(ROOT::VecOps::DeltaPhi(Lepton1FromHiggs_phi,Jet2FromHiggs_phi)):-10")
               .Define("Lepton1FromHiggs_Jet2FromHiggs_deltaR","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ROOT::VecOps::DeltaR(Lepton1FromHiggs_eta,Jet2FromHiggs_eta,Lepton1FromHiggs_phi,Jet2FromHiggs_phi):-10")
               .Define("Lepton1FromHiggs_Jet2FromHiggs_transverse_mass","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt(Lepton1FromHiggs_mass*Lepton1FromHiggs_mass + Jet2FromHiggs_mass*Jet2FromHiggs_mass + 2*Lepton1FromHiggs_transverse_energy*Jet2FromHiggs_transverse_energy - 2*Lepton1FromHiggs_pt*Jet2FromHiggs_pt*cos(Lepton1FromHiggs_Jet2FromHiggs_deltaphi)):-100")
               .Define("Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ((Min_deltaR_bJetFromtop_leptonFromtop[0] == 0) ? lep4vecs[1]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[0]]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[1]]:lep4vecs[0]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[0]]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[1]]):lep4vecs[0]")
               .Define("Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_pt","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ((Min_deltaR_bJetFromtop_leptonFromtop[0] == 0) ? (lep4vecs[1]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[0]]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[1]]).Pt():(lep4vecs[0]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[0]]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[1]]).Pt()):-100")
               .Define("Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_eta","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ((Min_deltaR_bJetFromtop_leptonFromtop[0] == 0) ? (lep4vecs[1]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[0]]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[1]]).Eta():(lep4vecs[0]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[0]]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[1]]).Eta()):-10")
               .Define("Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_phi","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ((Min_deltaR_bJetFromtop_leptonFromtop[0] == 0) ? (lep4vecs[1]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[0]]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[1]]).Phi():(lep4vecs[0]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[0]]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[1]]).Phi()):-10")
               .Define("Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_mass","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ((Min_deltaR_bJetFromtop_leptonFromtop[0] == 0) ? (lep4vecs[1]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[0]]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[1]]).M():(lep4vecs[0]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[0]]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[1]]).M()):-100")
               .Define("Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_transverse_energy","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt(Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_pt*Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_pt + Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_mass*Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_mass):-100")
               .Define("Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_deltaeta","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? abs(Lepton1FromHiggs_eta - Jet1FromHiggs_Jet2FromHiggs_eta):-10")
               .Define("Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_deltaphi","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? abs(ROOT::VecOps::DeltaPhi(Lepton1FromHiggs_phi,Jet1FromHiggs_Jet2FromHiggs_phi)):-10")
               .Define("Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_deltaR","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ROOT::VecOps::DeltaR(Lepton1FromHiggs_eta,Jet1FromHiggs_Jet2FromHiggs_eta,Lepton1FromHiggs_phi,Jet1FromHiggs_Jet2FromHiggs_phi):-10")
               .Define("Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_transverse_mass","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt(Lepton1FromHiggs_mass*Lepton1FromHiggs_mass + Jet1FromHiggs_Jet2FromHiggs_mass*Jet1FromHiggs_Jet2FromHiggs_mass + 2*Lepton1FromHiggs_transverse_energy*Jet1FromHiggs_Jet2FromHiggs_transverse_energy - 2*Lepton1FromHiggs_pt*Jet1FromHiggs_Jet2FromHiggs_pt*cos(Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_deltaphi)):-100")
               .Define("L2bJ1Fromtop_L1J1J2FromHiggs","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? lep4vecs[0]+lep4vecs[1]+cleanbjet4vecs[Min_deltaR_bJetFromtop_leptonFromtop[1]]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[0]]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[1]]:lep4vecs[0]")
               .Define("L2bJ1Fromtop_L1J1J2FromHiggs_pt","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? (lep4vecs[0]+lep4vecs[1]+cleanbjet4vecs[Min_deltaR_bJetFromtop_leptonFromtop[1]]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[0]]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[1]]).Pt():-100")
               .Define("L2bJ1Fromtop_L1J1J2FromHiggs_eta","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? (lep4vecs[0]+lep4vecs[1]+cleanbjet4vecs[Min_deltaR_bJetFromtop_leptonFromtop[1]]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[0]]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[1]]).Eta():-10")
               .Define("L2bJ1Fromtop_L1J1J2FromHiggs_phi","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? (lep4vecs[0]+lep4vecs[1]+cleanbjet4vecs[Min_deltaR_bJetFromtop_leptonFromtop[1]]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[0]]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[1]]).Phi():-10")
               .Define("L2bJ1Fromtop_L1J1J2FromHiggs_mass","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? (lep4vecs[0]+lep4vecs[1]+cleanbjet4vecs[Min_deltaR_bJetFromtop_leptonFromtop[1]]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[0]]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[1]]).M():-100")
               .Define("L2bJ1Fromtop_L1J1J2FromHiggs_px","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? L2bJ1Fromtop_L1J1J2FromHiggs_pt*cos(L2bJ1Fromtop_L1J1J2FromHiggs_phi):-10")
               .Define("L2bJ1Fromtop_L1J1J2FromHiggs_py","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? L2bJ1Fromtop_L1J1J2FromHiggs_pt*sin(L2bJ1Fromtop_L1J1J2FromHiggs_phi):-10")
               .Define("L2bJ1Fromtop_L1J1J2FromHiggs_transverse_energy","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt(L2bJ1Fromtop_L1J1J2FromHiggs_pt*L2bJ1Fromtop_L1J1J2FromHiggs_pt + L2bJ1Fromtop_L1J1J2FromHiggs_mass*L2bJ1Fromtop_L1J1J2FromHiggs_mass):-100")
               .Define("L2bJ1Fromtop_L1J1J2FromHiggs_deltaeta","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? abs(Lepton2Fromtop_bJet1Fromtop_eta - Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_eta):-10")
               .Define("L2bJ1Fromtop_L1J1J2FromHiggs_deltaphi","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? abs(ROOT::VecOps::DeltaPhi(Lepton2Fromtop_bJet1Fromtop_phi,Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_phi)):-10")
               .Define("L2bJ1Fromtop_L1J1J2FromHiggs_deltaR","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ROOT::VecOps::DeltaR(Lepton2Fromtop_bJet1Fromtop_eta,Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_eta,Lepton2Fromtop_bJet1Fromtop_phi,Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_phi):-10")
               .Define("L2bJ1Fromtop_L1J1J2FromHiggs_transverse_mass","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt(Lepton2Fromtop_bJet1Fromtop_mass*Lepton2Fromtop_bJet1Fromtop_mass + Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_mass*Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_mass + 2*Lepton2Fromtop_bJet1Fromtop_transverse_energy*Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_transverse_energy - 2*Lepton2Fromtop_bJet1Fromtop_pt*Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_pt*cos(L2bJ1Fromtop_L1J1J2FromHiggs_deltaphi)):-100")
               .Define("L2bJ1Fromtop_L1J1J2FromHiggs_Relative_St","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? (Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_pt + Lepton2Fromtop_bJet1Fromtop_pt) / St:-10")
               .Define("Tprime_transverse_mass_first_600","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt((Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*58.66*(58.66 + MET_pt_corr)))*(Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*58.66*(58.66 + MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py))):-100")
               .Define("Tprime_transverse_mass_second_600","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt((Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*58.66*(58.66 - MET_pt_corr)))*(Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*58.66*(58.66 - MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py))):-100")
               .Define("Tprime_transverse_mass_first_625","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt((Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*61.24*(61.24 + MET_pt_corr)))*(Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*61.24*(61.24 + MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py))):-100")
               .Define("Tprime_transverse_mass_second_625","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt((Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*61.24*(61.24 - MET_pt_corr)))*(Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*61.24*(61.24 - MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py))):-100")
               .Define("Tprime_transverse_mass_first_650","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt((Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*63.81*(63.81 + MET_pt_corr)))*(Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*63.81*(63.81 + MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py))):-100")
               .Define("Tprime_transverse_mass_second_650","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt((Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*63.81*(63.81 - MET_pt_corr)))*(Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*63.81*(63.81 - MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py))):-100")
               .Define("Tprime_transverse_mass_first_675","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt((Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*66.39*(66.39 + MET_pt_corr)))*(Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*66.39*(66.39 + MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py))):-100")
               .Define("Tprime_transverse_mass_second_675","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt((Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*66.39*(66.39 - MET_pt_corr)))*(Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*66.39*(66.39 - MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py))):-100")
               .Define("Tprime_transverse_mass_first_700","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt((Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*68.96*(68.96 + MET_pt_corr)))*(Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*68.96*(68.96 + MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py))):-100")
               .Define("Tprime_transverse_mass_second_700","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt((Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*68.96*(68.96 - MET_pt_corr)))*(Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*68.96*(68.96 - MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py))):-100")
               .Define("Tprime_transverse_mass_first_800","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt((Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*79.26*(79.26 + MET_pt_corr)))*(Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*79.26*(79.26 + MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py))):-100")
               .Define("Tprime_transverse_mass_second_800","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt((Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*79.26*(79.26 - MET_pt_corr)))*(Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*79.26*(79.26 - MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py))):-100")
               .Define("Tprime_transverse_mass_first_900","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt((Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*89.56*(89.56 + MET_pt_corr)))*(Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*89.56*(89.56 + MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py))):-100")
               .Define("Tprime_transverse_mass_second_900","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt((Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*89.56*(89.56 - MET_pt_corr)))*(Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*89.56*(89.56 - MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py))):-100")
               .Define("Tprime_transverse_mass_first_1000","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt((Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*99.86*(99.86 + MET_pt_corr)))*(Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*99.86*(99.86 + MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py))):-100")
               .Define("Tprime_transverse_mass_second_1000","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt((Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*99.86*(99.86 - MET_pt_corr)))*(Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*99.86*(99.86 - MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py))):-100")
               .Define("Tprime_transverse_mass_first_1100","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt((Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*110.16*(110.16 + MET_pt_corr)))*(Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*110.16*(110.16 + MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py))):-100")
               .Define("Tprime_transverse_mass_second_1100","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt((Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*110.16*(110.16 - MET_pt_corr)))*(Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*110.16*(110.16 - MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py))):-100")
               .Define("Tprime_transverse_mass_first_1200","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt((Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*120.46*(120.46 + MET_pt_corr)))*(Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*120.46*(120.46 + MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py))):-100")
               .Define("Tprime_transverse_mass_second_1200","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt((Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*120.46*(120.46 - MET_pt_corr)))*(Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*120.46*(120.46 - MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py))):-100");

    // if(!_isData)
    // {
    //     _rlm = _rlm.Define("GenParticles_mass","GenPart_mass")
    //                .Define("GenParticles_pdgId","GenPart_pdgId")
    //                .Define("MotherGenParticles","GenPart_genPartIdxMother")
    //                .Define("MotherGenParticlesPdgId","ROOT::VecOps::Take(GenPart_pdgId,MotherGenParticles)");

    //     _rlm = _rlm.Define("deltaR_Selected_leptonfromtop_Selected_clean_jet",::DR_2,{"LeptonFromtop_eta","Selected_clean_jet_eta","LeptonFromtop_phi","Selected_clean_jet_phi"})
    //                .Define("deltaR_Selected_leptonfromtop_Selected_clean_bjet",::DR_2,{"LeptonFromtop_eta","Selected_clean_bjet_eta","LeptonFromtop_phi","Selected_clean_bjet_phi"})
    //                .Define("deltaR_Selected_leptonfromHiggs_Selected_clean_jet",::DR_2,{"LeptonFromHiggs_eta","Selected_clean_jet_eta","LeptonFromHiggs_phi","Selected_clean_jet_phi"})
    //                .Define("deltaR_Selected_leptonfromHiggs_Selected_clean_bjet",::DR_2,{"LeptonFromHiggs_eta","Selected_clean_bjet_eta","LeptonFromHiggs_phi","Selected_clean_bjet_phi"});

    //     _rlm = _rlm.Define("Pt_leptonfromtop_over_Pt_clean_jet",::ratio_2,{"LeptonFromtop_pt","Selected_clean_jet_pt"})
    //                .Define("Pt_leptonfromtop_over_Pt_clean_bjet",::ratio_2,{"LeptonFromtop_pt","Selected_clean_bjet_pt"})
    //                .Define("Pt_leptonfromHiggs_over_Pt_clean_jet",::ratio_2,{"LeptonFromHiggs_pt","Selected_clean_jet_pt"})
    //                .Define("Pt_leptonfromHiggs_over_Pt_clean_bjet",::ratio_2,{"LeptonFromHiggs_pt","Selected_clean_bjet_pt"});

    //     _rlm = _rlm.Define("Min_deltaR_Selected_leptonfromtop_Selected_clean_jet",::minDR_6,{"LeptonFromtop_eta","Selected_clean_jet_eta","LeptonFromtop_phi","Selected_clean_jet_phi"})
    //                .Define("Min_deltaR_Selected_leptonfromtop_Selected_clean_bjet",::minDR_6,{"LeptonFromtop_eta","Selected_clean_bjet_eta","LeptonFromtop_phi","Selected_clean_bjet_phi"})
    //                .Define("Min_deltaR_Selected_leptonfromHiggs_Selected_clean_jet",::minDR_6,{"LeptonFromHiggs_eta","Selected_clean_jet_eta","LeptonFromHiggs_phi","Selected_clean_jet_phi"})
    //                .Define("Min_deltaR_Selected_leptonfromHiggs_Selected_clean_bjet",::minDR_6,{"LeptonFromHiggs_eta","Selected_clean_bjet_eta","LeptonFromHiggs_phi","Selected_clean_bjet_phi"});

    //     _rlm = _rlm.Define("Min_Pt_leptonfromtop_over_Pt_clean_jet","LeptonFromtop_pt/Selected_clean_jet_pt[0]")
    //                .Define("Min_Pt_leptonfromtop_over_Pt_clean_bjet","LeptonFromtop_pt/Selected_clean_bjet_pt[0]")
    //                .Define("Min_Pt_leptonfromHiggs_over_Pt_clean_jet","LeptonFromHiggs_pt/Selected_clean_jet_pt[0]")
    //                .Define("Min_Pt_leptonfromHiggs_over_Pt_clean_bjet","LeptonFromHiggs_pt/Selected_clean_bjet_pt[0]");

    //     _rlm = _rlm.Define("Max_deltaR_top",::maxDR,{"LeptonFromtop_eta","bJetFromtop_eta","LeptonFromtop_phi","bJetFromtop_phi"})
    //                .Define("bJetFromtop1","p4_bJetFromtop[Max_deltaR_top]")
    //                .Define("bJetFromtop1_pt","bJetFromtop1.Pt()")
    //                .Define("bJetFromtop1_eta","bJetFromtop1.Eta()")
    //                .Define("bJetFromtop1_phi","bJetFromtop1.Phi()")
    //                .Define("bJetFromtop1_mass","bJetFromtop1.M()")
    //                .Define("bJetFromtop1_transverse_energy","sqrt(bJetFromtop1_pt*bJetFromtop1_pt + bJetFromtop1_mass*bJetFromtop1_mass)");

    //     _rlm = _rlm.Define("Min_deltaR_Higgs",::minDR_2,{"JetFromHiggs_eta","JetFromHiggs_eta","JetFromHiggs_phi","JetFromHiggs_phi"})
    //                .Define("JetFromHiggs1","p4_JetFromHiggs[Min_deltaR_Higgs[0]]")
    //                .Define("JetFromHiggs1_pt","JetFromHiggs1.Pt()")
    //                .Define("JetFromHiggs1_eta","JetFromHiggs1.Eta()")
    //                .Define("JetFromHiggs1_phi","JetFromHiggs1.Phi()")
    //                .Define("JetFromHiggs1_mass","JetFromHiggs1.M()")
    //                .Define("JetFromHiggs1_transverse_energy","sqrt(JetFromHiggs1_pt*JetFromHiggs1_pt + JetFromHiggs1_mass*JetFromHiggs1_mass)")
    //                .Define("JetFromHiggs2","p4_JetFromHiggs[Min_deltaR_Higgs[1]]")
    //                .Define("JetFromHiggs2_pt","JetFromHiggs2.Pt()")
    //                .Define("JetFromHiggs2_eta","JetFromHiggs2.Eta()")
    //                .Define("JetFromHiggs2_phi","JetFromHiggs2.Phi()")
    //                .Define("JetFromHiggs2_mass","JetFromHiggs2.M()")
    //                .Define("JetFromHiggs2_transverse_energy","sqrt(JetFromHiggs2_pt*JetFromHiggs2_pt + JetFromHiggs2_mass*JetFromHiggs2_mass)");

    //     _rlm = _rlm.Define("Min_deltaR_Higgs",::minDR_2,{"JetFromtopHiggs_eta","JetFromtopHiggs_eta","JetFromtopHiggs_phi","JetFromtopHiggs_phi","JetFromtopHiggs_pt","bJetFromtop1_pt"})
    //                .Define("JetFromHiggs1","p4_JetFromtopHiggs[Min_deltaR_Higgs[0]]")
    //                .Define("JetFromHiggs1_pt","JetFromHiggs1.Pt()")
    //                .Define("JetFromHiggs1_eta","JetFromHiggs1.Eta()")
    //                .Define("JetFromHiggs1_phi","JetFromHiggs1.Phi()")
    //                .Define("JetFromHiggs1_mass","JetFromHiggs1.M()")
    //                .Define("JetFromHiggs2","p4_JetFromtopHiggs[Min_deltaR_Higgs[1]]")
    //                .Define("JetFromHiggs2_pt","JetFromHiggs2.Pt()")
    //                .Define("JetFromHiggs2_eta","JetFromHiggs2.Eta()")
    //                .Define("JetFromHiggs2_phi","JetFromHiggs2.Phi()")
    //                .Define("JetFromHiggs2_mass","JetFromHiggs2.M()");

    //     _rlm = _rlm.Define("LeptonFromtop_bJetFromtop1","LeptonFromtop4vecs + bJetFromtop1")
    //                .Define("LeptonFromtop_bJetFromtop1_pt","LeptonFromtop_bJetFromtop1.Pt()")
    //                .Define("LeptonFromtop_bJetFromtop1_eta","LeptonFromtop_bJetFromtop1.Eta()")
    //                .Define("LeptonFromtop_bJetFromtop1_phi","LeptonFromtop_bJetFromtop1.Phi()")
    //                .Define("LeptonFromtop_bJetFromtop1_mass","LeptonFromtop_bJetFromtop1.M()")
    //                .Define("LeptonFromtop_bJetFromtop1_transverse_energy","sqrt(LeptonFromtop_bJetFromtop1_pt*LeptonFromtop_bJetFromtop1_pt + LeptonFromtop_bJetFromtop1_mass*LeptonFromtop_bJetFromtop1_mass)")
    //                .Define("LeptonFromtop_bJetFromtop1_deltaeta","abs(LeptonFromtop_eta - bJetFromtop1_eta)")
    //                .Define("LeptonFromtop_bJetFromtop1_deltaphi","abs(ROOT::VecOps::DeltaPhi(LeptonFromtop_phi,bJetFromtop1_phi))")
    //                .Define("LeptonFromtop_bJetFromtop1_deltaR","ROOT::VecOps::DeltaR(LeptonFromtop_eta,bJetFromtop1_eta,LeptonFromtop_phi,bJetFromtop1_phi)")
    //                .Define("LeptonFromtop_bJetFromtop1_transverse_mass","sqrt(LeptonFromtop_mass*LeptonFromtop_mass + bJetFromtop1_mass*bJetFromtop1_mass + 2*LeptonFromtop_transverse_energy*bJetFromtop1_transverse_energy - 2*LeptonFromtop_pt*bJetFromtop1_pt*cos(LeptonFromtop_bJetFromtop1_deltaphi))")
    //                .Define("WFromtop_transverse_mass","sqrt(LeptonFromtop_pt*MET_pt*(1 - cos(LeptonFromtop_phi-(MET_phi+LeptonFromtop_phi-LeptonFromHiggs_phi)/2)))")
    //                .Define("LeptonFromtop_JetFromHiggs1","LeptonFromtop4vecs + JetFromHiggs1")
    //                .Define("LeptonFromtop_JetFromHiggs1_pt","LeptonFromtop_JetFromHiggs1.Pt()")
    //                .Define("LeptonFromtop_JetFromHiggs1_eta","LeptonFromtop_JetFromHiggs1.Eta()")
    //                .Define("LeptonFromtop_JetFromHiggs1_phi","LeptonFromtop_JetFromHiggs1.Phi()")
    //                .Define("LeptonFromtop_JetFromHiggs1_mass","LeptonFromtop_JetFromHiggs1.M()")
    //                .Define("LeptonFromtop_JetFromHiggs1_transverse_energy","sqrt(LeptonFromtop_JetFromHiggs1_pt*LeptonFromtop_JetFromHiggs1_pt + LeptonFromtop_JetFromHiggs1_mass*LeptonFromtop_JetFromHiggs1_mass)")
    //                .Define("LeptonFromtop_JetFromHiggs1_deltaeta","abs(LeptonFromtop_eta - JetFromHiggs1_eta)")
    //                .Define("LeptonFromtop_JetFromHiggs1_deltaphi","abs(ROOT::VecOps::DeltaPhi(LeptonFromtop_phi,JetFromHiggs1_phi))")
    //                .Define("LeptonFromtop_JetFromHiggs1_deltaR","ROOT::VecOps::DeltaR(LeptonFromtop_eta,JetFromHiggs1_eta,LeptonFromtop_phi,JetFromHiggs1_phi)")
    //                .Define("LeptonFromtop_JetFromHiggs1_transverse_mass","sqrt(LeptonFromtop_mass*LeptonFromtop_mass + JetFromHiggs1_mass*JetFromHiggs1_mass + 2*LeptonFromtop_transverse_energy*JetFromHiggs1_transverse_energy - 2*LeptonFromtop_pt*JetFromHiggs1_pt*cos(LeptonFromtop_JetFromHiggs1_deltaphi))")
    //                .Define("LeptonFromtop_JetFromHiggs2","LeptonFromtop4vecs + JetFromHiggs2")
    //                .Define("LeptonFromtop_JetFromHiggs2_pt","LeptonFromtop_JetFromHiggs2.Pt()")
    //                .Define("LeptonFromtop_JetFromHiggs2_eta","LeptonFromtop_JetFromHiggs2.Eta()")
    //                .Define("LeptonFromtop_JetFromHiggs2_phi","LeptonFromtop_JetFromHiggs2.Phi()")
    //                .Define("LeptonFromtop_JetFromHiggs2_mass","LeptonFromtop_JetFromHiggs2.M()")
    //                .Define("LeptonFromtop_JetFromHiggs2_transverse_energy","sqrt(LeptonFromtop_JetFromHiggs2_pt*LeptonFromtop_JetFromHiggs2_pt + LeptonFromtop_JetFromHiggs2_mass*LeptonFromtop_JetFromHiggs2_mass)")
    //                .Define("LeptonFromtop_JetFromHiggs2_deltaeta","abs(LeptonFromtop_eta - JetFromHiggs2_eta)")
    //                .Define("LeptonFromtop_JetFromHiggs2_deltaphi","abs(ROOT::VecOps::DeltaPhi(LeptonFromtop_phi,JetFromHiggs2_phi))")
    //                .Define("LeptonFromtop_JetFromHiggs2_deltaR","ROOT::VecOps::DeltaR(LeptonFromtop_eta,JetFromHiggs2_eta,LeptonFromtop_phi,JetFromHiggs2_phi)")
    //                .Define("LeptonFromtop_JetFromHiggs2_transverse_mass","sqrt(LeptonFromtop_mass*LeptonFromtop_mass + JetFromHiggs2_mass*JetFromHiggs2_mass + 2*LeptonFromtop_transverse_energy*JetFromHiggs2_transverse_energy - 2*LeptonFromtop_pt*JetFromHiggs2_pt*cos(LeptonFromtop_JetFromHiggs2_deltaphi))")
    //                .Define("LeptonFromHiggs_bJetFromtop1","LeptonFromHiggs4vecs + bJetFromtop1")
    //                .Define("LeptonFromHiggs_bJetFromtop1_pt","LeptonFromHiggs_bJetFromtop1.Pt()")
    //                .Define("LeptonFromHiggs_bJetFromtop1_eta","LeptonFromHiggs_bJetFromtop1.Eta()")
    //                .Define("LeptonFromHiggs_bJetFromtop1_phi","LeptonFromHiggs_bJetFromtop1.Phi()")
    //                .Define("LeptonFromHiggs_bJetFromtop1_mass","LeptonFromHiggs_bJetFromtop1.M()")
    //                .Define("LeptonFromHiggs_bJetFromtop1_transverse_energy","sqrt(LeptonFromHiggs_bJetFromtop1_pt*LeptonFromHiggs_bJetFromtop1_pt + LeptonFromHiggs_bJetFromtop1_mass*LeptonFromHiggs_bJetFromtop1_mass)")
    //                .Define("LeptonFromHiggs_bJetFromtop1_deltaeta","abs(LeptonFromHiggs_eta - bJetFromtop1_eta)")
    //                .Define("LeptonFromHiggs_bJetFromtop1_deltaphi","abs(ROOT::VecOps::DeltaPhi(LeptonFromHiggs_phi,bJetFromtop1_phi))")
    //                .Define("LeptonFromHiggs_bJetFromtop1_deltaR","ROOT::VecOps::DeltaR(LeptonFromHiggs_eta,bJetFromtop1_eta,LeptonFromHiggs_phi,bJetFromtop1_phi)")
    //                .Define("LeptonFromHiggs_bJetFromtop1_transverse_mass","sqrt(LeptonFromHiggs_mass*LeptonFromHiggs_mass + bJetFromtop1_mass*bJetFromtop1_mass + 2*LeptonFromHiggs_transverse_energy*bJetFromtop1_transverse_energy - 2*LeptonFromHiggs_pt*bJetFromtop1_pt*cos(LeptonFromHiggs_bJetFromtop1_deltaphi))")
    //                .Define("LeptonFromHiggs_JetFromHiggs1","LeptonFromHiggs4vecs + JetFromHiggs1")
    //                .Define("LeptonFromHiggs_JetFromHiggs1_pt","LeptonFromHiggs_JetFromHiggs1.Pt()")
    //                .Define("LeptonFromHiggs_JetFromHiggs1_eta","LeptonFromHiggs_JetFromHiggs1.Eta()")
    //                .Define("LeptonFromHiggs_JetFromHiggs1_phi","LeptonFromHiggs_JetFromHiggs1.Phi()")
    //                .Define("LeptonFromHiggs_JetFromHiggs1_mass","LeptonFromHiggs_JetFromHiggs1.M()")
    //                .Define("LeptonFromHiggs_JetFromHiggs1_transverse_energy","sqrt(LeptonFromHiggs_JetFromHiggs1_pt*LeptonFromHiggs_JetFromHiggs1_pt + LeptonFromHiggs_JetFromHiggs1_mass*LeptonFromHiggs_JetFromHiggs1_mass)")
    //                .Define("LeptonFromHiggs_JetFromHiggs1_deltaeta","abs(LeptonFromHiggs_eta - JetFromHiggs1_eta)")
    //                .Define("LeptonFromHiggs_JetFromHiggs1_deltaphi","abs(ROOT::VecOps::DeltaPhi(LeptonFromHiggs_phi,JetFromHiggs1_phi))")
    //                .Define("LeptonFromHiggs_JetFromHiggs1_deltaR","ROOT::VecOps::DeltaR(LeptonFromHiggs_eta,JetFromHiggs1_eta,LeptonFromHiggs_phi,JetFromHiggs1_phi)")
    //                .Define("LeptonFromHiggs_JetFromHiggs1_transverse_mass","sqrt(LeptonFromHiggs_mass*LeptonFromHiggs_mass + JetFromHiggs1_mass*JetFromHiggs1_mass + 2*LeptonFromHiggs_transverse_energy*JetFromHiggs1_transverse_energy - 2*LeptonFromHiggs_pt*JetFromHiggs1_pt*cos(LeptonFromHiggs_JetFromHiggs1_deltaphi))")
    //                .Define("LeptonFromHiggs_JetFromHiggs2","LeptonFromHiggs4vecs + JetFromHiggs2")
    //                .Define("LeptonFromHiggs_JetFromHiggs2_pt","LeptonFromHiggs_JetFromHiggs2.Pt()")
    //                .Define("LeptonFromHiggs_JetFromHiggs2_eta","LeptonFromHiggs_JetFromHiggs2.Eta()")
    //                .Define("LeptonFromHiggs_JetFromHiggs2_phi","LeptonFromHiggs_JetFromHiggs2.Phi()")
    //                .Define("LeptonFromHiggs_JetFromHiggs2_mass","LeptonFromHiggs_JetFromHiggs2.M()")
    //                .Define("LeptonFromHiggs_JetFromHiggs2_transverse_energy","sqrt(LeptonFromHiggs_JetFromHiggs2_pt*LeptonFromHiggs_JetFromHiggs2_pt + LeptonFromHiggs_JetFromHiggs2_mass*LeptonFromHiggs_JetFromHiggs2_mass)")
    //                .Define("LeptonFromHiggs_JetFromHiggs2_deltaeta","abs(LeptonFromHiggs_eta - JetFromHiggs2_eta)")
    //                .Define("LeptonFromHiggs_JetFromHiggs2_deltaphi","abs(ROOT::VecOps::DeltaPhi(LeptonFromHiggs_phi,JetFromHiggs2_phi))")
    //                .Define("LeptonFromHiggs_JetFromHiggs2_deltaR","ROOT::VecOps::DeltaR(LeptonFromHiggs_eta,JetFromHiggs2_eta,LeptonFromHiggs_phi,JetFromHiggs2_phi)")
    //                .Define("LeptonFromHiggs_JetFromHiggs2_transverse_mass","sqrt(LeptonFromHiggs_mass*LeptonFromHiggs_mass + JetFromHiggs2_mass*JetFromHiggs2_mass + 2*LeptonFromHiggs_transverse_energy*JetFromHiggs2_transverse_energy - 2*LeptonFromHiggs_pt*JetFromHiggs2_pt*cos(LeptonFromHiggs_JetFromHiggs2_deltaphi))")
    //                .Define("JetFromHiggs1_JetFromHiggs2","JetFromHiggs1 + JetFromHiggs2")
    //                .Define("JetFromHiggs1_JetFromHiggs2_pt","JetFromHiggs1_JetFromHiggs2.Pt()")
    //                .Define("JetFromHiggs1_JetFromHiggs2_eta","JetFromHiggs1_JetFromHiggs2.Eta()")
    //                .Define("JetFromHiggs1_JetFromHiggs2_phi","JetFromHiggs1_JetFromHiggs2.Phi()")
    //                .Define("JetFromHiggs1_JetFromHiggs2_mass","JetFromHiggs1_JetFromHiggs2.M()")
    //                .Define("JetFromHiggs1_JetFromHiggs2_transverse_energy","sqrt(JetFromHiggs1_JetFromHiggs2_pt*JetFromHiggs1_JetFromHiggs2_pt + JetFromHiggs1_JetFromHiggs2_mass*JetFromHiggs1_JetFromHiggs2_mass)")
    //                .Define("JetFromHiggs1_JetFromHiggs2_deltaeta","abs(JetFromHiggs2_eta - JetFromHiggs1_eta)")
    //                .Define("JetFromHiggs1_JetFromHiggs2_deltaphi","abs(ROOT::VecOps::DeltaPhi(JetFromHiggs1_phi,JetFromHiggs2_phi))")
    //                .Define("JetFromHiggs1_JetFromHiggs2_deltaR","ROOT::VecOps::DeltaR(JetFromHiggs1_eta,JetFromHiggs2_eta,JetFromHiggs1_phi,JetFromHiggs2_phi)")
    //                .Define("JetFromHiggs1_JetFromHiggs2_transverse_mass","sqrt(JetFromHiggs1_mass*JetFromHiggs1_mass + JetFromHiggs2_mass*JetFromHiggs2_mass + 2*JetFromHiggs1_transverse_energy*JetFromHiggs2_transverse_energy - 2*JetFromHiggs1_pt*JetFromHiggs2_pt*cos(JetFromHiggs1_JetFromHiggs2_deltaphi))")
    //                .Define("LeptonFromHiggs_JetFromHiggs1_JetFromHiggs2","LeptonFromHiggs4vecs + JetFromHiggs1_JetFromHiggs2")
    //                .Define("LeptonFromHiggs_JetFromHiggs1_JetFromHiggs2_pt","LeptonFromHiggs_JetFromHiggs1_JetFromHiggs2.Pt()")
    //                .Define("LeptonFromHiggs_JetFromHiggs1_JetFromHiggs2_eta","LeptonFromHiggs_JetFromHiggs1_JetFromHiggs2.Eta()")
    //                .Define("LeptonFromHiggs_JetFromHiggs1_JetFromHiggs2_phi","LeptonFromHiggs_JetFromHiggs1_JetFromHiggs2.Phi()")
    //                .Define("LeptonFromHiggs_JetFromHiggs1_JetFromHiggs2_mass","LeptonFromHiggs_JetFromHiggs1_JetFromHiggs2.M()")
    //                .Define("LeptonFromHiggs_JetFromHiggs1_JetFromHiggs2_transverse_energy","sqrt(LeptonFromHiggs_JetFromHiggs1_JetFromHiggs2_pt*LeptonFromHiggs_JetFromHiggs1_JetFromHiggs2_pt + LeptonFromHiggs_JetFromHiggs1_JetFromHiggs2_mass*LeptonFromHiggs_JetFromHiggs1_JetFromHiggs2_mass)")
    //                .Define("LeptonFromHiggs_JetFromHiggs1_JetFromHiggs2_deltaeta","abs(LeptonFromHiggs_eta - JetFromHiggs1_JetFromHiggs2_eta)")
    //                .Define("LeptonFromHiggs_JetFromHiggs1_JetFromHiggs2_deltaphi","abs(ROOT::VecOps::DeltaPhi(LeptonFromHiggs_phi,JetFromHiggs1_JetFromHiggs2_phi))")
    //                .Define("LeptonFromHiggs_JetFromHiggs1_JetFromHiggs2_deltaR","ROOT::VecOps::DeltaR(LeptonFromHiggs_eta,JetFromHiggs1_JetFromHiggs2_eta,LeptonFromHiggs_phi,JetFromHiggs1_JetFromHiggs2_phi)")
    //                .Define("LeptonFromHiggs_JetFromHiggs1_JetFromHiggs2_transverse_mass","sqrt(LeptonFromHiggs_mass*LeptonFromHiggs_mass + JetFromHiggs1_JetFromHiggs2_mass*JetFromHiggs1_JetFromHiggs2_mass + 2*LeptonFromHiggs_transverse_energy*JetFromHiggs1_JetFromHiggs2_transverse_energy - 2*LeptonFromHiggs_pt*LeptonFromHiggs_JetFromHiggs1_JetFromHiggs2_pt*cos(LeptonFromHiggs_JetFromHiggs1_JetFromHiggs2_deltaphi))")
    //                .Define("LbJFromtop1_LJJFromHiggs12","LeptonFromtop_bJetFromtop1 + LeptonFromHiggs_JetFromHiggs1_JetFromHiggs2")
    //                .Define("LbJFromtop1_LJJFromHiggs12_pt","LbJFromtop1_LJJFromHiggs12.Pt()")
    //                .Define("LbJFromtop1_LJJFromHiggs12_eta","LbJFromtop1_LJJFromHiggs12.Eta()")
    //                .Define("LbJFromtop1_LJJFromHiggs12_phi","LbJFromtop1_LJJFromHiggs12.Phi()")
    //                .Define("LbJFromtop1_LJJFromHiggs12_mass","LbJFromtop1_LJJFromHiggs12.M()")
    //                .Define("LbJFromtop1_LJJFromHiggs12_transverse_energy","sqrt(LbJFromtop1_LJJFromHiggs12_pt*LbJFromtop1_LJJFromHiggs12_pt + LbJFromtop1_LJJFromHiggs12_mass*LbJFromtop1_LJJFromHiggs12_mass)")
    //                .Define("LbJFromtop1_LJJFromHiggs12_deltaeta","abs(LeptonFromtop_bJetFromtop1_eta - LeptonFromHiggs_JetFromHiggs1_JetFromHiggs2_eta)")
    //                .Define("LbJFromtop1_LJJFromHiggs12_deltaphi","abs(ROOT::VecOps::DeltaPhi(LeptonFromtop_bJetFromtop1_phi,LeptonFromHiggs_JetFromHiggs1_JetFromHiggs2_phi))")
    //                .Define("LbJFromtop1_LJJFromHiggs12_deltaR","ROOT::VecOps::DeltaR(LeptonFromtop_bJetFromtop1_eta,LeptonFromHiggs_JetFromHiggs1_JetFromHiggs2_eta,LeptonFromtop_bJetFromtop1_phi,LeptonFromHiggs_JetFromHiggs1_JetFromHiggs2_phi)")
    //                .Define("LbJFromtop1_LJJFromHiggs12_transverse_mass","sqrt(LeptonFromtop_bJetFromtop1_mass*LeptonFromtop_bJetFromtop1_mass + LeptonFromHiggs_JetFromHiggs1_JetFromHiggs2_mass*LeptonFromHiggs_JetFromHiggs1_JetFromHiggs2_mass + 2*LeptonFromtop_bJetFromtop1_transverse_energy*LeptonFromHiggs_JetFromHiggs1_JetFromHiggs2_transverse_energy - 2*LeptonFromtop_bJetFromtop1_pt*LeptonFromHiggs_JetFromHiggs1_JetFromHiggs2_pt*cos(LbJFromtop1_LJJFromHiggs12_deltaphi))");
                   
    //     _rlm = _rlm.Define("LeptonFromtop_NeutrinoFromtop","LeptonFromtop4vecs + p4_GenNeutrinoFromtop_first")
    //                .Define("LeptonFromtop_NeutrinoFromtop_pt","LeptonFromtop_NeutrinoFromtop.Pt()")      
    //                .Define("LeptonFromtop_NeutrinoFromtop_eta","LeptonFromtop_NeutrinoFromtop.Eta()") 
    //                .Define("LeptonFromtop_NeutrinoFromtop_phi","LeptonFromtop_NeutrinoFromtop.Phi()") 
    //                .Define("LeptonFromtop_NeutrinoFromtop_mass","LeptonFromtop_NeutrinoFromtop.M()") 
    //                .Define("LeptonFromtop_NeutrinoFromtop_transverse_energy","sqrt(LeptonFromtop_NeutrinoFromtop_pt*LeptonFromtop_NeutrinoFromtop_pt + LeptonFromtop_NeutrinoFromtop_mass*LeptonFromtop_NeutrinoFromtop_mass)")
    //                .Define("LeptonFromtop_NeutrinoFromtop_deltaeta","abs(LeptonFromtop_eta - GenNeutrinoFromtop_first_eta)")
    //                .Define("LeptonFromtop_NeutrinoFromtop_deltaphi","abs(ROOT::VecOps::DeltaPhi(LeptonFromtop_phi,GenNeutrinoFromtop_first_phi))")
    //                .Define("LeptonFromtop_NeutrinoFromtop_deltaR","ROOT::VecOps::DeltaR(LeptonFromtop_eta,GenNeutrinoFromtop_first_eta,LeptonFromtop_phi,GenNeutrinoFromtop_first_phi)")
    //                .Define("LeptonFromtop_NeutrinoFromtop_transverse_mass","sqrt(2*LeptonFromtop_pt*GenNeutrinoFromtop_first_pt*(1 - cos(LeptonFromtop_NeutrinoFromtop_deltaphi)))")
    //                .Define("LeptonFromtop_NeutrinoFromtop_bJetFromtop1","LeptonFromtop_NeutrinoFromtop + bJetFromtop1")
    //                .Define("LeptonFromtop_NeutrinoFromtop_bJetFromtop1_pt","LeptonFromtop_NeutrinoFromtop_bJetFromtop1.Pt()")      
    //                .Define("LeptonFromtop_NeutrinoFromtop_bJetFromtop1_eta","LeptonFromtop_NeutrinoFromtop_bJetFromtop1.Eta()") 
    //                .Define("LeptonFromtop_NeutrinoFromtop_bJetFromtop1_phi","LeptonFromtop_NeutrinoFromtop_bJetFromtop1.Phi()") 
    //                .Define("LeptonFromtop_NeutrinoFromtop_bJetFromtop1_mass","LeptonFromtop_NeutrinoFromtop_bJetFromtop1.M()") 
    //                .Define("LeptonFromtop_NeutrinoFromtop_bJetFromtop1_transverse_energy","sqrt(LeptonFromtop_NeutrinoFromtop_bJetFromtop1_pt*LeptonFromtop_NeutrinoFromtop_bJetFromtop1_pt + LeptonFromtop_NeutrinoFromtop_bJetFromtop1_mass*LeptonFromtop_NeutrinoFromtop_bJetFromtop1_mass)")
    //                .Define("LeptonFromtop_NeutrinoFromtop_bJetFromtop1_deltaeta","abs(LeptonFromtop_NeutrinoFromtop_eta - bJetFromtop1_eta)")
    //                .Define("LeptonFromtop_NeutrinoFromtop_bJetFromtop1_deltaphi","abs(ROOT::VecOps::DeltaPhi(LeptonFromtop_NeutrinoFromtop_phi,bJetFromtop1_phi))")
    //                .Define("LeptonFromtop_NeutrinoFromtop_bJetFromtop1_deltaR","ROOT::VecOps::DeltaR(LeptonFromtop_NeutrinoFromtop_eta,bJetFromtop1_eta,LeptonFromtop_NeutrinoFromtop_phi,bJetFromtop1_phi)")
    //                .Define("LeptonFromtop_NeutrinoFromtop_bJetFromtop1_transverse_mass","sqrt(LeptonFromtop_NeutrinoFromtop_mass*LeptonFromtop_NeutrinoFromtop_mass + bJetFromtop1_mass*bJetFromtop1_mass + 2*LeptonFromtop_NeutrinoFromtop_transverse_energy*bJetFromtop1_transverse_energy - 2*LeptonFromtop_NeutrinoFromtop_pt*bJetFromtop1_pt*cos(LeptonFromtop_NeutrinoFromtop_bJetFromtop1_deltaphi))")
    //                .Define("LeptonFromHiggs_NeutrinoFromHiggs","LeptonFromHiggs4vecs + p4_GenNeutrinoFromHiggs_first")
    //                .Define("LeptonFromHiggs_NeutrinoFromHiggs_pt","LeptonFromHiggs_NeutrinoFromHiggs.Pt()")      
    //                .Define("LeptonFromHiggs_NeutrinoFromHiggs_eta","LeptonFromHiggs_NeutrinoFromHiggs.Eta()") 
    //                .Define("LeptonFromHiggs_NeutrinoFromHiggs_phi","LeptonFromHiggs_NeutrinoFromHiggs.Phi()") 
    //                .Define("LeptonFromHiggs_NeutrinoFromHiggs_mass","LeptonFromHiggs_NeutrinoFromHiggs.M()")
    //                .Define("LeptonFromHiggs_NeutrinoFromHiggs_transverse_energy","sqrt(LeptonFromHiggs_NeutrinoFromHiggs_pt*LeptonFromHiggs_NeutrinoFromHiggs_pt + LeptonFromHiggs_NeutrinoFromHiggs_mass*LeptonFromHiggs_NeutrinoFromHiggs_mass)")
    //                .Define("LeptonFromHiggs_NeutrinoFromHiggs_deltaeta","abs(LeptonFromHiggs_eta - GenNeutrinoFromHiggs_first_eta)")
    //                .Define("LeptonFromHiggs_NeutrinoFromHiggs_deltaphi","abs(ROOT::VecOps::DeltaPhi(LeptonFromHiggs_phi,GenNeutrinoFromHiggs_first_phi))")
    //                .Define("LeptonFromHiggs_NeutrinoFromHiggs_deltaR","ROOT::VecOps::DeltaR(LeptonFromHiggs_eta,GenNeutrinoFromHiggs_first_eta,LeptonFromHiggs_phi,GenNeutrinoFromHiggs_first_phi)")
    //                .Define("LeptonFromHiggs_NeutrinoFromHiggs_transverse_mass","sqrt(2*LeptonFromHiggs_pt*GenNeutrinoFromHiggs_first_pt*(1 - cos(LeptonFromHiggs_NeutrinoFromHiggs_deltaphi)))")
    //                .Define("LeptonFromHiggs_NeutrinoFromHiggs_JetFromHiggs1_JetFromHiggs2","LeptonFromHiggs_NeutrinoFromHiggs + JetFromHiggs1_JetFromHiggs2")
    //                .Define("LeptonFromHiggs_NeutrinoFromHiggs_JetFromHiggs1_JetFromHiggs2_pt","LeptonFromHiggs_NeutrinoFromHiggs_JetFromHiggs1_JetFromHiggs2.Pt()")      
    //                .Define("LeptonFromHiggs_NeutrinoFromHiggs_JetFromHiggs1_JetFromHiggs2_eta","LeptonFromHiggs_NeutrinoFromHiggs_JetFromHiggs1_JetFromHiggs2.Eta()") 
    //                .Define("LeptonFromHiggs_NeutrinoFromHiggs_JetFromHiggs1_JetFromHiggs2_phi","LeptonFromHiggs_NeutrinoFromHiggs_JetFromHiggs1_JetFromHiggs2.Phi()") 
    //                .Define("LeptonFromHiggs_NeutrinoFromHiggs_JetFromHiggs1_JetFromHiggs2_mass","LeptonFromHiggs_NeutrinoFromHiggs_JetFromHiggs1_JetFromHiggs2.M()") 
    //                .Define("LeptonFromHiggs_NeutrinoFromHiggs_JetFromHiggs1_JetFromHiggs2_transverse_energy","sqrt(LeptonFromHiggs_NeutrinoFromHiggs_JetFromHiggs1_JetFromHiggs2_pt*LeptonFromHiggs_NeutrinoFromHiggs_JetFromHiggs1_JetFromHiggs2_pt + LeptonFromHiggs_NeutrinoFromHiggs_JetFromHiggs1_JetFromHiggs2_mass*LeptonFromHiggs_NeutrinoFromHiggs_JetFromHiggs1_JetFromHiggs2_mass)")
    //                .Define("LeptonFromHiggs_NeutrinoFromHiggs_JetFromHiggs1_JetFromHiggs2_deltaeta","abs(LeptonFromHiggs_NeutrinoFromHiggs_eta - JetFromHiggs1_JetFromHiggs2_eta)")
    //                .Define("LeptonFromHiggs_NeutrinoFromHiggs_JetFromHiggs1_JetFromHiggs2_deltaphi","abs(ROOT::VecOps::DeltaPhi(LeptonFromHiggs_NeutrinoFromHiggs_phi,JetFromHiggs1_JetFromHiggs2_phi))")
    //                .Define("LeptonFromHiggs_NeutrinoFromHiggs_JetFromHiggs1_JetFromHiggs2_deltaR","ROOT::VecOps::DeltaR(LeptonFromHiggs_NeutrinoFromHiggs_eta,JetFromHiggs1_JetFromHiggs2_eta,LeptonFromHiggs_NeutrinoFromHiggs_phi,JetFromHiggs1_JetFromHiggs2_phi)")
    //                .Define("LeptonFromHiggs_NeutrinoFromHiggs_JetFromHiggs1_JetFromHiggs2_transverse_mass","sqrt(LeptonFromHiggs_NeutrinoFromHiggs_mass*LeptonFromHiggs_NeutrinoFromHiggs_mass + JetFromHiggs1_JetFromHiggs2_mass*JetFromHiggs1_JetFromHiggs2_mass + 2*LeptonFromHiggs_NeutrinoFromHiggs_transverse_energy*JetFromHiggs1_JetFromHiggs2_transverse_energy - 2*LeptonFromHiggs_NeutrinoFromHiggs_pt*JetFromHiggs1_JetFromHiggs2_pt*cos(LeptonFromHiggs_NeutrinoFromHiggs_JetFromHiggs1_JetFromHiggs2_deltaphi))")
    //                .Define("LNbJFromtop1_LNJJFromHiggs12","LeptonFromtop_NeutrinoFromtop_bJetFromtop1 + LeptonFromHiggs_NeutrinoFromHiggs_JetFromHiggs1_JetFromHiggs2")
    //                .Define("LNbJFromtop1_LNJJFromHiggs12_pt","LNbJFromtop1_LNJJFromHiggs12.Pt()")
    //                .Define("LNbJFromtop1_LNJJFromHiggs12_eta","LNbJFromtop1_LNJJFromHiggs12.Eta()")
    //                .Define("LNbJFromtop1_LNJJFromHiggs12_phi","LNbJFromtop1_LNJJFromHiggs12.Phi()")
    //                .Define("LNbJFromtop1_LNJJFromHiggs12_mass","LNbJFromtop1_LNJJFromHiggs12.M()")
    //                .Define("LNbJFromtop1_LNJJFromHiggs12_px","LNbJFromtop1_LNJJFromHiggs12_pt*cos(LNbJFromtop1_LNJJFromHiggs12_phi)")
    //                .Define("LNbJFromtop1_LNJJFromHiggs12_py","LNbJFromtop1_LNJJFromHiggs12_pt*sin(LNbJFromtop1_LNJJFromHiggs12_phi)")
    //                .Define("LNbJFromtop1_LNJJFromHiggs12_transverse_energy","sqrt(LNbJFromtop1_LNJJFromHiggs12_pt*LNbJFromtop1_LNJJFromHiggs12_pt + LNbJFromtop1_LNJJFromHiggs12_mass*LNbJFromtop1_LNJJFromHiggs12_mass)")
    //                .Define("LNbJFromtop1_LNJJFromHiggs12_deltaeta","abs(LeptonFromtop_NeutrinoFromtop_bJetFromtop1_eta - LeptonFromHiggs_NeutrinoFromHiggs_JetFromHiggs1_JetFromHiggs2_eta)")
    //                .Define("LNbJFromtop1_LNJJFromHiggs12_deltaphi","abs(ROOT::VecOps::DeltaPhi(LeptonFromtop_NeutrinoFromtop_bJetFromtop1_phi,LeptonFromHiggs_NeutrinoFromHiggs_JetFromHiggs1_JetFromHiggs2_phi))")
    //                .Define("LNbJFromtop1_LNJJFromHiggs12_deltaR","ROOT::VecOps::DeltaR(LeptonFromtop_NeutrinoFromtop_bJetFromtop1_eta,LeptonFromHiggs_NeutrinoFromHiggs_JetFromHiggs1_JetFromHiggs2_eta,LeptonFromtop_NeutrinoFromtop_bJetFromtop1_phi,LeptonFromHiggs_NeutrinoFromHiggs_JetFromHiggs1_JetFromHiggs2_phi)")
    //                .Define("LNbJFromtop1_LNJJFromHiggs12_transverse_mass","sqrt(LeptonFromtop_NeutrinoFromtop_bJetFromtop1_mass*LeptonFromtop_NeutrinoFromtop_bJetFromtop1_mass + LeptonFromHiggs_NeutrinoFromHiggs_JetFromHiggs1_JetFromHiggs2_mass*LeptonFromHiggs_NeutrinoFromHiggs_JetFromHiggs1_JetFromHiggs2_mass + 2*LeptonFromtop_NeutrinoFromtop_bJetFromtop1_transverse_energy*LeptonFromHiggs_NeutrinoFromHiggs_JetFromHiggs1_JetFromHiggs2_transverse_energy - 2*LeptonFromtop_NeutrinoFromtop_bJetFromtop1_pt*LeptonFromHiggs_NeutrinoFromHiggs_JetFromHiggs1_JetFromHiggs2_pt*cos(LNbJFromtop1_LNJJFromHiggs12_deltaphi))")
    //                .Define("LNbJFromtop1_LNJJFromHiggs12_Relative_St","(LeptonFromHiggs_NeutrinoFromHiggs_JetFromHiggs1_JetFromHiggs2_pt + LeptonFromtop_NeutrinoFromtop_bJetFromtop1_pt) / St_with_MET")
    //                .Define("Tprime_transverse_mass_Gen","sqrt((LeptonFromHiggs_transverse_energy + GenNeutrinoFromHiggs_first_pt + JetFromHiggs1_transverse_energy + JetFromHiggs2_transverse_energy + LeptonFromtop_transverse_energy + GenNeutrinoFromtop_first_pt + bJetFromtop1_transverse_energy)*(LeptonFromHiggs_transverse_energy + GenNeutrinoFromHiggs_first_pt + JetFromHiggs1_transverse_energy + JetFromHiggs2_transverse_energy + LeptonFromtop_transverse_energy + GenNeutrinoFromtop_first_pt + bJetFromtop1_transverse_energy) - (LNbJFromtop1_LNJJFromHiggs12_px*LNbJFromtop1_LNJJFromHiggs12_px + LNbJFromtop1_LNJJFromHiggs12_py*LNbJFromtop1_LNJJFromHiggs12_py))");

    //     _rlm = _rlm.Define("METFromNeutrinos_pt","sqrt((NeutrinoFromtop_pt*cos(NeutrinoFromtop_phi)+NeutrinoFromHiggs_pt*cos(NeutrinoFromHiggs_phi))*(NeutrinoFromtop_pt*cos(NeutrinoFromtop_phi)+NeutrinoFromHiggs_pt*cos(NeutrinoFromHiggs_phi)) + (NeutrinoFromtop_pt*sin(NeutrinoFromtop_phi)+NeutrinoFromHiggs_pt*sin(NeutrinoFromHiggs_phi))*(NeutrinoFromtop_pt*sin(NeutrinoFromtop_phi)+NeutrinoFromHiggs_pt*sin(NeutrinoFromHiggs_phi)))")
    //                .Define("METFromNeutrinos_phi","tan((NeutrinoFromtop_pt*cos(NeutrinoFromtop_phi)+NeutrinoFromHiggs_pt*cos(NeutrinoFromHiggs_phi)) / (NeutrinoFromtop_pt*sin(NeutrinoFromtop_phi)+NeutrinoFromHiggs_pt*sin(NeutrinoFromHiggs_phi)))")
    //                .Define("LeptonFromtop_NeutrinoFromtop_transverse_mass","sqrt(2*LeptonFromtop_pt*NeutrinoFromtop_pt*(1-cos(LeptonFromtop_phi-NeutrinoFromtop_phi)))")
    //                .Define("LeptonFromHiggs_NeutrinoFromHiggs_transverse_mass","sqrt(2*LeptonFromHiggs_pt*NeutrinoFromHiggs_pt*(1-cos(LeptonFromHiggs_phi-NeutrinoFromHiggs_phi)))");
    // }
}

void TprimeAnalyser::storingVariables(std::string year, bool JEC)
{

    //================================Store variables in tree=======================================//
    // define variables that you want to store
    //==============================================================================================//
    
    string year2 = year;
    if(year == "2016preVFP" || year == "2016postVFP")
    {
        year2 = "2016";
    }

    addVartoStore("evWeight_loosett1l");
    addVartoStore("evWeight");
    addVartoStore("Flag_goodVertices");
    addVartoStore("Flag_globalSuperTightHalo2016Filter");
    addVartoStore("Flag_HBHENoiseFilter");
    addVartoStore("Flag_HBHENoiseIsoFilter");
    addVartoStore("Flag_EcalDeadCellTriggerPrimitiveFilter");
    addVartoStore("Flag_BadPFMuonFilter");
    addVartoStore("Flag_BadPFMuonDzFilter");
    addVartoStore("Flag_hfNoisyHitsFilter");
    addVartoStore("Flag_eeBadScFilter");
    if(year2 == "2017" || year2 == "2018")
    {
        addVartoStore("Flag_ecalBadCalibFilter");
    }
    if(year2 == "2016")
    {
        addVartoStore("HLT_IsoTkMu24");
        addVartoStore("HLT_Ele27_WPTight_Gsf");
        addVartoStore("HLT_PFMET120_PFMHT120_IDTight");
    }
    if(year2 == "2017")
    {
        addVartoStore("HLT_IsoMu27");
        addVartoStore("HLT_Ele32_WPTight_Gsf_L1DoubleEG");
        addVartoStore("HLT_PFMET140_PFMHT140_IDTight");
    }
    if(year2 == "2018")
    {
        addVartoStore("HLT_IsoMu24");
        addVartoStore("HLT_Ele32_WPTight_Gsf");
        addVartoStore("HLT_PFMET120_PFMHT120_IDTight");
    }
    addVartoStore("one");
    addVartoStore("run");
    addVartoStore("luminosityBlock");
    addVartoStore("event");
    if(!_isData)
    {
        // addVartoStore("GenPart_pdgId");
        // addVartoStore("GenPart_genPartIdxMother");
        // addVartoStore("gentopcut");
        // addVartoStore("GenPart_top_pt");
        // addVartoStore("TopPtWeight");
        // addVartoStore("TopPtWeight_nom");
        // addVartoStore("TopPtWeight_up");
        // addVartoStore("TopPtWeight_down");
        addVartoStore("genWeight");
	    // addVartoStore("pugenWeight");
        // addVartoStore("puWeight");
        // addVartoStore("puWeight_plus");
        // addVartoStore("puWeight_minus");
        // addVartoStore("prefiring_SF_central");
        // addVartoStore("prefiring_SF_up");
        // addVartoStore("prefiring_SF_down");
        // addVartoStore("muon_loosett1l_SF_central");
        // addVartoStore("muon_loosett1l_SF_up");
        // addVartoStore("muon_loosett1l_SF_down");
        // addVartoStore("muon_loosett1l_SF_syst");
        // addVartoStore("muon_loosett1l_SF_stat");
        // addVartoStore("muon_loosett1l_SF_iso_sf");
        // addVartoStore("muon_loosett1l_SF_iso_systup");
        // addVartoStore("muon_loosett1l_SF_iso_systdown");
        // addVartoStore("muon_loosett1l_SF_iso_syst");
        // addVartoStore("muon_loosett1l_SF_iso_stat");
        // addVartoStore("muon_SF_central");
        // addVartoStore("muon_SF_up");
        // addVartoStore("muon_SF_down");
        // addVartoStore("muon_SF_syst");
        // addVartoStore("muon_SF_stat");
        // addVartoStore("muon_SF_hlt_sf");
        // addVartoStore("muon_SF_hlt_systup");
        // addVartoStore("muon_SF_hlt_systdown");
        // addVartoStore("muon_SF_hlt_syst");
        // addVartoStore("muon_SF_hlt_stat");
        // addVartoStore("muon_SF_reco_sf");
        // addVartoStore("muon_SF_reco_systup");
        // addVartoStore("muon_SF_reco_systdown");
        // addVartoStore("muon_SF_reco_syst");
        // addVartoStore("muon_SF_reco_stat");
        // addVartoStore("muon_SF_id_sf");
        // addVartoStore("muon_SF_id_systup");
        // addVartoStore("muon_SF_id_systdown");
        // addVartoStore("muon_SF_id_syst");
        // addVartoStore("muon_SF_id_stat");
        // addVartoStore("muon_SF_iso_sf");
        // addVartoStore("muon_SF_iso_systup");
        // addVartoStore("muon_SF_iso_systdown");
        // addVartoStore("muon_SF_iso_syst");
        // addVartoStore("muon_SF_iso_stat");
        // addVartoStore("ele_loosett1l_SF_central");
        // addVartoStore("ele_loosett1l_SF_up");
        // addVartoStore("ele_loosett1l_SF_down");
        // addVartoStore("ele_loosett1l_SF_id_sf");
        // addVartoStore("ele_loosett1l_SF_id_sfup");
        // addVartoStore("ele_loosett1l_SF_id_sfdown");
        // addVartoStore("ele_SF_central");
        // addVartoStore("ele_SF_up");
        // addVartoStore("ele_SF_down");
        // addVartoStore("ele_SF_hlt_sf");
        // addVartoStore("ele_SF_hlt_systup");
        // addVartoStore("ele_SF_hlt_systdown");
        // addVartoStore("ele_SF_hlt_syst");
        // addVartoStore("ele_SF_hlt_stat");
        // addVartoStore("ele_SF_reco_sf");
        // addVartoStore("ele_SF_reco_sfup");
        // addVartoStore("ele_SF_reco_sfdown");
        // addVartoStore("ele_SF_id_sf");
        // addVartoStore("ele_SF_id_sfup");
        // addVartoStore("ele_SF_id_sfdown");
        // addVartoStore("btag_loosett1l_SF_bcflav_central");
        // addVartoStore("btag_loosett1l_SF_bcflav_up_correlated");
        // addVartoStore("btag_loosett1l_SF_bcflav_down_correlated");
        // addVartoStore("btag_loosett1l_SF_bcflav_up_uncorrelated");
        // addVartoStore("btag_loosett1l_SF_bcflav_down_uncorrelated");
        // addVartoStore("btag_loosett1l_SF_lflav_central");
        // addVartoStore("btag_loosett1l_SF_lflav_up_correlated");
        // addVartoStore("btag_loosett1l_SF_lflav_down_correlated");
        // addVartoStore("btag_loosett1l_SF_lflav_up_uncorrelated");
        // addVartoStore("btag_loosett1l_SF_lflav_down_uncorrelated");
        // addVartoStore("btag_SF_bcflav_central");
        // addVartoStore("btag_SF_bcflav_up_correlated");
        // addVartoStore("btag_SF_bcflav_down_correlated");
        // addVartoStore("btag_SF_bcflav_up_uncorrelated");
        // addVartoStore("btag_SF_bcflav_down_uncorrelated");
        // addVartoStore("btag_SF_lflav_central");
        // addVartoStore("btag_SF_lflav_up_correlated");
        // addVartoStore("btag_SF_lflav_down_correlated");
        // addVartoStore("btag_SF_lflav_up_uncorrelated");
        // addVartoStore("btag_SF_lflav_down_uncorrelated");
        // addVartoStore("evWeight_wobtagSF_loosett1l");
        // addVartoStore("totbtagSF_loosett1l");
        // addVartoStore("elec_hltUp_loosett1l");
        // addVartoStore("elec_hltDown_loosett1l");
        // addVartoStore("syst_elec_hltUp_loosett1l");
        // addVartoStore("syst_elec_hltDown_loosett1l");
        // addVartoStore("stat_elec_hlt_"+year2+"Up_loosett1l");
        // addVartoStore("stat_elec_hlt_"+year2+"Down_loosett1l");
        // addVartoStore("syst_elec_recoUp_loosett1l");
        // addVartoStore("syst_elec_recoDown_loosett1l");
        // addVartoStore("syst_elec_idUp_loosett1l");
        // addVartoStore("syst_elec_idDown_loosett1l");
        // addVartoStore("muon_hltUp_loosett1l");
        // addVartoStore("muon_hltDown_loosett1l");
        // addVartoStore("syst_muon_hltUp_loosett1l");
        // addVartoStore("syst_muon_hltDown_loosett1l");
        // addVartoStore("stat_muon_hlt_"+year2+"Up_loosett1l");
        // addVartoStore("stat_muon_hlt_"+year2+"Down_loosett1l");
        // addVartoStore("muon_recoUp_loosett1l");
        // addVartoStore("muon_recoDown_loosett1l");
        // addVartoStore("syst_muon_recoUp_loosett1l");
        // addVartoStore("syst_muon_recoDown_loosett1l");
        // addVartoStore("stat_muon_reco_"+year2+"Up_loosett1l");
        // addVartoStore("stat_muon_reco_"+year2+"Down_loosett1l");
        // addVartoStore("muon_idUp_loosett1l");
        // addVartoStore("muon_idDown_loosett1l");
        // addVartoStore("syst_muon_idUp_loosett1l");
        // addVartoStore("syst_muon_idDown_loosett1l");
        // addVartoStore("stat_muon_id_"+year2+"Up_loosett1l");
        // addVartoStore("stat_muon_id_"+year2+"Down_loosett1l");
        // addVartoStore("muon_isoUp_loosett1l");
        // addVartoStore("muon_isoDown_loosett1l");
        // addVartoStore("syst_muon_isoUp_loosett1l");
        // addVartoStore("syst_muon_isoDown_loosett1l");
        // addVartoStore("stat_muon_iso_"+year2+"Up_loosett1l");
        // addVartoStore("stat_muon_iso_"+year2+"Down_loosett1l");
        // addVartoStore("syst_puUp_loosett1l");
        // addVartoStore("syst_puDown_loosett1l");
        // addVartoStore("syst_b_correlatedUp_loosett1l");
        // addVartoStore("syst_b_correlatedDown_loosett1l");
        // addVartoStore("syst_b_uncorrelated_"+year+"Up_loosett1l");
        // addVartoStore("syst_b_uncorrelated_"+year+"Down_loosett1l");
        // addVartoStore("syst_l_correlatedUp_loosett1l");
        // addVartoStore("syst_l_correlatedDown_loosett1l");
        // addVartoStore("syst_l_uncorrelated_"+year+"Up_loosett1l");
        // addVartoStore("syst_l_uncorrelated_"+year+"Down_loosett1l");
        // addVartoStore("syst_prefiringUp_loosett1l");
        // addVartoStore("syst_prefiringDown_loosett1l");
        // addVartoStore("syst_pileupjetidUp_loosett1l");
        // addVartoStore("syst_pileupjetidDown_loosett1l");
        // addVartoStore("syst_pt_topUp_loosett1l");
        // addVartoStore("syst_pt_topDown_loosett1l");
        // addVartoStore("syst_mescaleUp_loosett1l");
        // addVartoStore("syst_mescaleDown_loosett1l");
        // addVartoStore("syst_renscaleUp_loosett1l");
        // addVartoStore("syst_renscaleDown_loosett1l");
        // addVartoStore("syst_facscaleUp_loosett1l");
        // addVartoStore("syst_facscaleDown_loosett1l");
        // addVartoStore("syst_pdfalphasUp_loosett1l");
        // addVartoStore("syst_pdfalphasDown_loosett1l");
        // addVartoStore("syst_isrUp_loosett1l");
        // addVartoStore("syst_isrDown_loosett1l");
        // addVartoStore("syst_fsrUp_loosett1l");
        // addVartoStore("syst_fsrDown_loosett1l");
        // addVartoStore("evWeight_wobtagSF");
        // addVartoStore("totbtagSF");
        if(JEC == 0)
        {
            // addVartoStore("elec_hltUp");
            // addVartoStore("elec_hltDown");
            addVartoStore("syst_elec_hltUp");
            addVartoStore("syst_elec_hltDown");
            addVartoStore("stat_elec_hlt_"+year2+"Up");
            addVartoStore("stat_elec_hlt_"+year2+"Down");
            addVartoStore("syst_elec_recoUp");
            addVartoStore("syst_elec_recoDown");
            addVartoStore("syst_elec_idUp");
            addVartoStore("syst_elec_idDown");
            // addVartoStore("muon_hltUp");
            // addVartoStore("muon_hltDown");
            addVartoStore("syst_muon_hltUp");
            addVartoStore("syst_muon_hltDown");
            addVartoStore("stat_muon_hlt_"+year2+"Up");
            addVartoStore("stat_muon_hlt_"+year2+"Down");
            // addVartoStore("muon_recoUp");
            // addVartoStore("muon_recoDown");
            addVartoStore("syst_muon_recoUp");
            addVartoStore("syst_muon_recoDown");
            addVartoStore("stat_muon_reco_"+year2+"Up");
            addVartoStore("stat_muon_reco_"+year2+"Down");
            // addVartoStore("muon_idUp");
            // addVartoStore("muon_idDown");
            addVartoStore("syst_muon_idUp");
            addVartoStore("syst_muon_idDown");
            addVartoStore("stat_muon_id_"+year2+"Up");
            addVartoStore("stat_muon_id_"+year2+"Down");
            // addVartoStore("muon_isoUp");
            // addVartoStore("muon_isoDown");
            addVartoStore("syst_muon_isoUp");
            addVartoStore("syst_muon_isoDown");
            addVartoStore("stat_muon_iso_"+year2+"Up");
            addVartoStore("stat_muon_iso_"+year2+"Down");
            addVartoStore("syst_puUp");
            addVartoStore("syst_puDown");
            addVartoStore("syst_b_correlatedUp");
            addVartoStore("syst_b_correlatedDown");
            addVartoStore("syst_b_uncorrelated_"+year+"Up");
            addVartoStore("syst_b_uncorrelated_"+year+"Down");
            addVartoStore("syst_l_correlatedUp");
            addVartoStore("syst_l_correlatedDown");
            addVartoStore("syst_l_uncorrelated_"+year+"Up");
            addVartoStore("syst_l_uncorrelated_"+year+"Down");
            addVartoStore("syst_prefiringUp");
            addVartoStore("syst_prefiringDown");
            addVartoStore("syst_pileupjetidUp");
            addVartoStore("syst_pileupjetidDown");
            addVartoStore("syst_pt_topUp");
            addVartoStore("syst_pt_topDown");
            addVartoStore("syst_mescaleUp");
            addVartoStore("syst_mescaleDown");
            addVartoStore("syst_renscaleUp");
            addVartoStore("syst_renscaleDown");
            addVartoStore("syst_facscaleUp");
            addVartoStore("syst_facscaleDown");
            addVartoStore("syst_pdfalphasUp");
            addVartoStore("syst_pdfalphasDown");
            addVartoStore("syst_isrUp");
            addVartoStore("syst_isrDown");
            addVartoStore("syst_fsrUp");
            addVartoStore("syst_fsrDown");
            for(int i = 1; i < 103; i++)
            {
                addVartoStore("syst_pdf"+std::to_string(i));
            }
            addVartoStore("flip_evWeight_wobtagSF_loosett1l");
            addVartoStore("flip_totbtagSF_loosett1l");
            addVartoStore("flip_evWeight_loosett1l");
        }
        // addVartoStore("flip_syst_elec_hltUp_loosett1l");
        // addVartoStore("flip_syst_elec_hltDown_loosett1l");
        // addVartoStore("flip_stat_elec_hlt_"+year2+"Up_loosett1l");
        // addVartoStore("flip_stat_elec_hlt_"+year2+"Down_loosett1l");
        // addVartoStore("flip_syst_elec_recoUp_loosett1l");
        // addVartoStore("flip_syst_elec_recoDown_loosett1l");
        // addVartoStore("flip_syst_elec_idUp_loosett1l");
        // addVartoStore("flip_syst_elec_idDown_loosett1l");
        // addVartoStore("flip_syst_muon_hltUp_loosett1l");
        // addVartoStore("flip_syst_muon_hltDown_loosett1l");
        // addVartoStore("flip_stat_muon_hlt_"+year2+"Up_loosett1l");
        // addVartoStore("flip_stat_muon_hlt_"+year2+"Down_loosett1l");
        // addVartoStore("flip_syst_muon_recoUp_loosett1l");
        // addVartoStore("flip_syst_muon_recoDown_loosett1l");
        // addVartoStore("flip_stat_muon_reco_"+year2+"Up_loosett1l");
        // addVartoStore("flip_stat_muon_reco_"+year2+"Down_loosett1l");
        // addVartoStore("flip_syst_muon_idUp_loosett1l");
        // addVartoStore("flip_syst_muon_idDown_loosett1l");
        // addVartoStore("flip_stat_muon_id_"+year2+"Up_loosett1l");
        // addVartoStore("flip_stat_muon_id_"+year2+"Down_loosett1l");
        // addVartoStore("flip_syst_muon_isoUp_loosett1l");
        // addVartoStore("flip_syst_muon_isoDown_loosett1l");
        // addVartoStore("flip_stat_muon_iso_"+year2+"Up_loosett1l");
        // addVartoStore("flip_stat_muon_iso_"+year2+"Down_loosett1l");
        // addVartoStore("flip_syst_puUp_loosett1l");
        // addVartoStore("flip_syst_puDown_loosett1l");
        // addVartoStore("flip_syst_b_correlatedUp_loosett1l");
        // addVartoStore("flip_syst_b_correlatedDown_loosett1l");
        // addVartoStore("flip_syst_b_uncorrelated_"+year+"Up_loosett1l");
        // addVartoStore("flip_syst_b_uncorrelated_"+year+"Down_loosett1l");
        // addVartoStore("flip_syst_l_correlatedUp_loosett1l");
        // addVartoStore("flip_syst_l_correlatedDown_loosett1l");
        // addVartoStore("flip_syst_l_uncorrelated_"+year+"Up_loosett1l");
        // addVartoStore("flip_syst_l_uncorrelated_"+year+"Down_loosett1l");
        // addVartoStore("flip_syst_prefiringUp_loosett1l");
        // addVartoStore("flip_syst_prefiringDown_loosett1l");
        // addVartoStore("flip_syst_pileupjetidUp_loosett1l");
        // addVartoStore("flip_syst_pileupjetidDown_loosett1l");
        // addVartoStore("flip_syst_pt_topUp_loosett1l");
        // addVartoStore("flip_syst_pt_topDown_loosett1l");
        // addVartoStore("flip_syst_mescaleUp_loosett1l");
        // addVartoStore("flip_syst_mescaleDown_loosett1l");
        // addVartoStore("flip_syst_renscaleUp_loosett1l");
        // addVartoStore("flip_syst_renscaleDown_loosett1l");
        // addVartoStore("flip_syst_facscaleUp_loosett1l");
        // addVartoStore("flip_syst_facscaleDown_loosett1l");
        // addVartoStore("flip_syst_pdfalphasUp_loosett1l");
        // addVartoStore("flip_syst_pdfalphasDown_loosett1l");
        // addVartoStore("flip_syst_isrUp_loosett1l");
        // addVartoStore("flip_syst_isrDown_loosett1l");
        // addVartoStore("flip_syst_fsrUp_loosett1l");
        // addVartoStore("flip_syst_fsrDown_loosett1l");
        addVartoStore("flip_evWeight_wobtagSF");
        addVartoStore("flip_totbtagSF");
        addVartoStore("flip_evWeight");
        if(JEC == 0)
        {
            addVartoStore("flip_syst_elec_hltUp");
            addVartoStore("flip_syst_elec_hltDown");
            addVartoStore("flip_stat_elec_hlt_"+year2+"Up");
            addVartoStore("flip_stat_elec_hlt_"+year2+"Down");
            addVartoStore("flip_syst_elec_recoUp");
            addVartoStore("flip_syst_elec_recoDown");
            addVartoStore("flip_syst_elec_idUp");
            addVartoStore("flip_syst_elec_idDown");
            addVartoStore("flip_syst_muon_hltUp");
            addVartoStore("flip_syst_muon_hltDown");
            addVartoStore("flip_stat_muon_hlt_"+year2+"Up");
            addVartoStore("flip_stat_muon_hlt_"+year2+"Down");
            addVartoStore("flip_syst_muon_recoUp");
            addVartoStore("flip_syst_muon_recoDown");
            addVartoStore("flip_stat_muon_reco_"+year2+"Up");
            addVartoStore("flip_stat_muon_reco_"+year2+"Down");
            addVartoStore("flip_syst_muon_idUp");
            addVartoStore("flip_syst_muon_idDown");
            addVartoStore("flip_stat_muon_id_"+year2+"Up");
            addVartoStore("flip_stat_muon_id_"+year2+"Down");
            addVartoStore("flip_syst_muon_isoUp");
            addVartoStore("flip_syst_muon_isoDown");
            addVartoStore("flip_stat_muon_iso_"+year2+"Up");
            addVartoStore("flip_stat_muon_iso_"+year2+"Down");
            addVartoStore("flip_syst_puUp");
            addVartoStore("flip_syst_puDown");
            addVartoStore("flip_syst_b_correlatedUp");
            addVartoStore("flip_syst_b_correlatedDown");
            addVartoStore("flip_syst_b_uncorrelated_"+year+"Up");
            addVartoStore("flip_syst_b_uncorrelated_"+year+"Down");
            addVartoStore("flip_syst_l_correlatedUp");
            addVartoStore("flip_syst_l_correlatedDown");
            addVartoStore("flip_syst_l_uncorrelated_"+year+"Up");
            addVartoStore("flip_syst_l_uncorrelated_"+year+"Down");
            addVartoStore("flip_syst_prefiringUp");
            addVartoStore("flip_syst_prefiringDown");
            addVartoStore("flip_syst_pileupjetidUp");
            addVartoStore("flip_syst_pileupjetidDown");
            addVartoStore("flip_syst_pt_topUp");
            addVartoStore("flip_syst_pt_topDown");
            addVartoStore("flip_syst_mescaleUp");
            addVartoStore("flip_syst_mescaleDown");
            addVartoStore("flip_syst_renscaleUp");
            addVartoStore("flip_syst_renscaleDown");
            addVartoStore("flip_syst_facscaleUp");
            addVartoStore("flip_syst_facscaleDown");
            addVartoStore("flip_syst_pdfalphasUp");
            addVartoStore("flip_syst_pdfalphasDown");
            addVartoStore("flip_syst_isrUp");
            addVartoStore("flip_syst_isrDown");
            addVartoStore("flip_syst_fsrUp");
            addVartoStore("flip_syst_fsrDown");
            for(int i = 1; i < 103; i++)
            {
                addVartoStore("flip_syst_pdf"+std::to_string(i));
            }
            addVartoStore("fake_evWeight_wobtagSF_loosett1l");
            addVartoStore("fake_totbtagSF_loosett1l");
            addVartoStore("fake_evWeight_loosett1l");
        }
        // addVartoStore("fake_syst_elec_hltUp_loosett1l");
        // addVartoStore("fake_syst_elec_hltDown_loosett1l");
        // addVartoStore("fake_stat_elec_hlt_"+year2+"Up_loosett1l");
        // addVartoStore("fake_stat_elec_hlt_"+year2+"Down_loosett1l");
        // addVartoStore("fake_syst_elec_recoUp_loosett1l");
        // addVartoStore("fake_syst_elec_recoDown_loosett1l");
        // addVartoStore("fake_syst_elec_idUp_loosett1l");
        // addVartoStore("fake_syst_elec_idDown_loosett1l");
        // addVartoStore("fake_syst_muon_hltUp_loosett1l");
        // addVartoStore("fake_syst_muon_hltDown_loosett1l");
        // addVartoStore("fake_stat_muon_hlt_"+year2+"Up_loosett1l");
        // addVartoStore("fake_stat_muon_hlt_"+year2+"Down_loosett1l");
        // addVartoStore("fake_syst_muon_recoUp_loosett1l");
        // addVartoStore("fake_syst_muon_recoDown_loosett1l");
        // addVartoStore("fake_stat_muon_reco_"+year2+"Up_loosett1l");
        // addVartoStore("fake_stat_muon_reco_"+year2+"Down_loosett1l");
        // addVartoStore("fake_syst_muon_idUp_loosett1l");
        // addVartoStore("fake_syst_muon_idDown_loosett1l");
        // addVartoStore("fake_stat_muon_id_"+year2+"Up_loosett1l");
        // addVartoStore("fake_stat_muon_id_"+year2+"Down_loosett1l");
        // addVartoStore("fake_syst_muon_isoUp_loosett1l");
        // addVartoStore("fake_syst_muon_isoDown_loosett1l");
        // addVartoStore("fake_stat_muon_iso_"+year2+"Up_loosett1l");
        // addVartoStore("fake_stat_muon_iso_"+year2+"Down_loosett1l");
        // addVartoStore("fake_syst_puUp_loosett1l");
        // addVartoStore("fake_syst_puDown_loosett1l");
        // addVartoStore("fake_syst_b_correlatedUp_loosett1l");
        // addVartoStore("fake_syst_b_correlatedDown_loosett1l");
        // addVartoStore("fake_syst_b_uncorrelated_"+year+"Up_loosett1l");
        // addVartoStore("fake_syst_b_uncorrelated_"+year+"Down_loosett1l");
        // addVartoStore("fake_syst_l_correlatedUp_loosett1l");
        // addVartoStore("fake_syst_l_correlatedDown_loosett1l");
        // addVartoStore("fake_syst_l_uncorrelated_"+year+"Up_loosett1l");
        // addVartoStore("fake_syst_l_uncorrelated_"+year+"Down_loosett1l");
        // addVartoStore("fake_syst_prefiringUp_loosett1l");
        // addVartoStore("fake_syst_prefiringDown_loosett1l");
        // addVartoStore("fake_syst_pileupjetidUp_loosett1l");
        // addVartoStore("fake_syst_pileupjetidDown_loosett1l");
        // addVartoStore("fake_syst_pt_topUp_loosett1l");
        // addVartoStore("fake_syst_pt_topDown_loosett1l");
        // addVartoStore("fake_syst_mescaleUp_loosett1l");
        // addVartoStore("fake_syst_mescaleDown_loosett1l");
        // addVartoStore("fake_syst_renscaleUp_loosett1l");
        // addVartoStore("fake_syst_renscaleDown_loosett1l");
        // addVartoStore("fake_syst_facscaleUp_loosett1l");
        // addVartoStore("fake_syst_facscaleDown_loosett1l");
        // addVartoStore("fake_syst_pdfalphasUp_loosett1l");
        // addVartoStore("fake_syst_pdfalphasDown_loosett1l");
        // addVartoStore("fake_syst_isrUp_loosett1l");
        // addVartoStore("fake_syst_isrDown_loosett1l");
        // addVartoStore("fake_syst_fsrUp_loosett1l");
        // addVartoStore("fake_syst_fsrDown_loosett1l");
        addVartoStore("fake_evWeight_wobtagSF");
        addVartoStore("fake_totbtagSF");
        addVartoStore("fake_evWeight");
        if(JEC == 0)
        {
            addVartoStore("fake_syst_elec_hltUp");
            addVartoStore("fake_syst_elec_hltDown");
            addVartoStore("fake_stat_elec_hlt_"+year2+"Up");
            addVartoStore("fake_stat_elec_hlt_"+year2+"Down");
            addVartoStore("fake_syst_elec_recoUp");
            addVartoStore("fake_syst_elec_recoDown");
            addVartoStore("fake_syst_elec_idUp");
            addVartoStore("fake_syst_elec_idDown");
            addVartoStore("fake_syst_muon_hltUp");
            addVartoStore("fake_syst_muon_hltDown");
            addVartoStore("fake_stat_muon_hlt_"+year2+"Up");
            addVartoStore("fake_stat_muon_hlt_"+year2+"Down");
            addVartoStore("fake_syst_muon_recoUp");
            addVartoStore("fake_syst_muon_recoDown");
            addVartoStore("fake_stat_muon_reco_"+year2+"Up");
            addVartoStore("fake_stat_muon_reco_"+year2+"Down");
            addVartoStore("fake_syst_muon_idUp");
            addVartoStore("fake_syst_muon_idDown");
            addVartoStore("fake_stat_muon_id_"+year2+"Up");
            addVartoStore("fake_stat_muon_id_"+year2+"Down");
            addVartoStore("fake_syst_muon_isoUp");
            addVartoStore("fake_syst_muon_isoDown");
            addVartoStore("fake_stat_muon_iso_"+year2+"Up");
            addVartoStore("fake_stat_muon_iso_"+year2+"Down");
            addVartoStore("fake_syst_puUp");
            addVartoStore("fake_syst_puDown");
            addVartoStore("fake_syst_b_correlatedUp");
            addVartoStore("fake_syst_b_correlatedDown");
            addVartoStore("fake_syst_b_uncorrelated_"+year+"Up");
            addVartoStore("fake_syst_b_uncorrelated_"+year+"Down");
            addVartoStore("fake_syst_l_correlatedUp");
            addVartoStore("fake_syst_l_correlatedDown");
            addVartoStore("fake_syst_l_uncorrelated_"+year+"Up");
            addVartoStore("fake_syst_l_uncorrelated_"+year+"Down");
            addVartoStore("fake_syst_prefiringUp");
            addVartoStore("fake_syst_prefiringDown");
            addVartoStore("fake_syst_pileupjetidUp");
            addVartoStore("fake_syst_pileupjetidDown");
            addVartoStore("fake_syst_pt_topUp");
            addVartoStore("fake_syst_pt_topDown");
            addVartoStore("fake_syst_mescaleUp");
            addVartoStore("fake_syst_mescaleDown");
            addVartoStore("fake_syst_renscaleUp");
            addVartoStore("fake_syst_renscaleDown");
            addVartoStore("fake_syst_facscaleUp");
            addVartoStore("fake_syst_facscaleDown");
            addVartoStore("fake_syst_pdfalphasUp");
            addVartoStore("fake_syst_pdfalphasDown");
            addVartoStore("fake_syst_isrUp");
            addVartoStore("fake_syst_isrDown");
            addVartoStore("fake_syst_fsrUp");
            addVartoStore("fake_syst_fsrDown");
            for(int i = 1; i < 103; i++)
            {
                addVartoStore("fake_syst_pdf"+std::to_string(i));
            }
        }
    }

    // electron
    if(JEC == 0)
    {
        // addVartoStore("nElectron");
        // addVartoStore("Electron_cutBased");
        addVartoStore("Electron_pt");
        addVartoStore("Electron_eta");
        addVartoStore("Electron_phi");
        addVartoStore("Electron_mass");
        addVartoStore("Electron_charge");
        // addVartoStore("Electron_dxy"); 
        // addVartoStore("Electron_dz"); 
        addVartoStore("Electron_ip3d");
        addVartoStore("Electron_sip3d");
        // addVartoStore("Electron_lostHits"); 
        // addVartoStore("Electron_convVeto"); 
        // addVartoStore("Electron_tightCharge"); 
        addVartoStore("Electron_miniPFRelIso_all");
        addVartoStore("Electron_miniPFRelIso_chg");
        addVartoStore("Electron_pfRelIso03_all");
        addVartoStore("Electron_pfRelIso03_chg");
        addVartoStore("Electron_jetPtRelv2");
        addVartoStore("Electron_jetRelIso");
        addVartoStore("Electron_pdgId");
        if(!_isData)
        {
            addVartoStore("Electron_genPartIdx");
            addVartoStore("Electron_genPartFlav");
        }
        // addVartoStore("goodElectronsloosett1l");
        addVartoStore("Selected_electron_loosett1l_pt");
        addVartoStore("Selected_electron_loosett1l_number");
        addVartoStore("Selected_electron_loosett1l_leading_pt");
        addVartoStore("Selected_electron_loosett1l_subleading_pt");
        // addVartoStore("Selected_electron_loosett1l_sum_two_electrons_pt");
        // addVartoStore("Selected_electron_loosett1l_sum_all_electrons_pt");
        addVartoStore("Selected_electron_loosett1l_eta");
        addVartoStore("Selected_electron_loosett1l_leading_eta");
        addVartoStore("Selected_electron_loosett1l_subleading_eta");
        addVartoStore("Selected_electron_loosett1l_phi");
        // addVartoStore("Selected_electron_loosett1l_mass");
        // addVartoStore("Selected_electron_loosett1l_transverse_energy");
        addVartoStore("Selected_electron_loosett1l_charge");
        // addVartoStore("Selected_electron_loosett1l_charge_sum");
        // addVartoStore("Selected_electron_loosett1l_deltaeta");
        // addVartoStore("Selected_electron_loosett1l_deltaphi");
        // addVartoStore("Selected_electron_loosett1l_deltaR");
        addVartoStore("Selected_electron_loosett1l_miniPFRelIso_all");
        // addVartoStore("Selected_electron_loosett1l_miniPFRelIso_chg");
        // addVartoStore("Selected_electron_loosett1l_pfRelIso03_all");
        // addVartoStore("Selected_electron_loosett1l_pfRelIso03_chg");
        addVartoStore("Selected_electron_loosett1l_jetPtRelv2");
        addVartoStore("Selected_electron_loosett1l_jetRelIso");
        // if(!_isData)
        // {
        //     addVartoStore("Selected_electron_loosett1l_genPartIdx");
        //     addVartoStore("Selected_electron_loosett1l_genPartpdgId");
        //     addVartoStore("Selected_electron_loosett1l_genPartIdxMother");
        //     addVartoStore("Selected_electron_loosett1l_genPartpdgIdMother");
        //     addVartoStore("Selected_electron_loosett1l_genPartFlav");
        // }
        // addVartoStore("Selected_electron_loosett1l_pdgId");
        // addVartoStore("Selected_electron_loosett1l_ip3d");
        addVartoStore("Selected_electron_loosett1l_sip3d");
        // addVartoStore("Selected_electron_loosett1l_idx");
        // addVartoStore("goodElectronstemp");
        addVartoStore("Selected_electron_temp_pt");
        addVartoStore("Selected_electron_temp_number");
        // addVartoStore("Selected_electron_temp_leading_pt");
        // addVartoStore("Selected_electron_temp_subleading_pt");
        // addVartoStore("Selected_electron_temp_sum_two_electrons_pt");
        // addVartoStore("Selected_electron_temp_sum_all_electrons_pt");
        addVartoStore("Selected_electron_temp_eta");
        // addVartoStore("Selected_electron_temp_leading_eta");
        // addVartoStore("Selected_electron_temp_subleading_eta");
        addVartoStore("Selected_electron_temp_phi");
        // addVartoStore("Selected_electron_temp_mass");
        // addVartoStore("Selected_electron_temp_transverse_energy");
        // addVartoStore("Selected_electron_temp_charge");
        // addVartoStore("Selected_electron_temp_charge_sum");
        // addVartoStore("Selected_electron_temp_deltaeta");
        // addVartoStore("Selected_electron_temp_deltaphi");
        // addVartoStore("Selected_electron_temp_deltaR");
        addVartoStore("Selected_electron_temp_miniPFRelIso_all");
        // addVartoStore("Selected_electron_temp_miniPFRelIso_chg");
        // addVartoStore("Selected_electron_temp_pfRelIso03_all");
        // addVartoStore("Selected_electron_temp_pfRelIso03_chg");
        addVartoStore("Selected_electron_temp_jetPtRelv2");
        addVartoStore("Selected_electron_temp_jetRelIso");
        // if(!_isData)
        // {
        //     addVartoStore("Selected_electron_temp_genPartIdx");
        //     addVartoStore("Selected_electron_temp_genPartpdgId");
        //     addVartoStore("Selected_electron_temp_genPartIdxMother");
        //     addVartoStore("Selected_electron_temp_genPartpdgIdMother");
        //     addVartoStore("Selected_electron_temp_genPartFlav");
        // }
        // addVartoStore("Selected_electron_temp_pdgId");
        // addVartoStore("Selected_electron_temp_ip3d");
        // addVartoStore("Selected_electron_temp_sip3d");
        // addVartoStore("Selected_electron_temp_idx");
        // // addVartoStore("dis_comb_electron_temp");
        // // addVartoStore("elec4Rvecs_temp");
        addVartoStore("Selected_electron_temp_px");
        addVartoStore("Selected_electron_temp_py");
        addVartoStore("Selected_electron_temp_pz");
        // addVartoStore("Selected_electron_temp_isolated");
        // addVartoStore("goodElectrons");
        addVartoStore("Selected_electron_pt");
    }
    addVartoStore("Selected_electron_number");
    if(JEC == 0)
    {
        addVartoStore("Selected_electron_leading_pt");
        addVartoStore("Selected_electron_subleading_pt");
        // addVartoStore("Selected_electron_sum_two_electrons_pt");
        // addVartoStore("Selected_electron_sum_all_electrons_pt");
        addVartoStore("Selected_electron_eta");
        addVartoStore("Selected_electron_leading_eta");
        addVartoStore("Selected_electron_subleading_eta");
        addVartoStore("Selected_electron_phi");
        // addVartoStore("Selected_electron_mass");
        // addVartoStore("Selected_electron_transverse_energy");
        addVartoStore("Selected_electron_charge");
        // addVartoStore("Selected_electron_charge_sum");
        // addVartoStore("Selected_electron_deltaeta");
        // addVartoStore("Selected_electron_deltaphi");
        // addVartoStore("Selected_electron_deltaR");
        addVartoStore("Selected_electron_miniPFRelIso_all");
        // addVartoStore("Selected_electron_miniPFRelIso_chg");
        // addVartoStore("Selected_electron_pfRelIso03_all");
        // addVartoStore("Selected_electron_pfRelIso03_chg");
        addVartoStore("Selected_electron_jetPtRelv2");
        addVartoStore("Selected_electron_jetRelIso");
        // if(!_isData)
        // {
        //     addVartoStore("Selected_electron_genPartIdx");
        //     addVartoStore("Selected_electron_genPartpdgId");
        //     addVartoStore("Selected_electron_genPartIdxMother");
        //     addVartoStore("Selected_electron_genPartpdgIdMother");
        //     addVartoStore("Selected_electron_genPartFlav");
        // }
        // addVartoStore("Selected_electron_pdgId");
        // addVartoStore("Selected_electron_ip3d");
        addVartoStore("Selected_electron_sip3d");
        // addVartoStore("Selected_electron_idx");
        // // addVartoStore("elec4vecs");
        // // addVartoStore("Vectorial_sum_two_electrons");
        // // addVartoStore("Vectorial_sum_two_electrons_pt");
        // // addVartoStore("Vectorial_sum_two_electrons_eta");
        // // addVartoStore("Vectorial_sum_two_electrons_phi");
        // // addVartoStore("Vectorial_sum_two_electrons_mass");

        // muon
        // addVartoStore("nMuon");
        addVartoStore("Muon_pt");
        addVartoStore("Muon_pt_corr");
        addVartoStore("Muon_eta");
        addVartoStore("Muon_phi");
        addVartoStore("Muon_mass");
        addVartoStore("Muon_charge");
        // addVartoStore("Muon_dxy"); 
        // addVartoStore("Muon_dz");
        addVartoStore("Muon_ip3d");
        addVartoStore("Muon_sip3d");
        // addVartoStore("Muon_tightCharge");
        addVartoStore("Muon_miniPFRelIso_all");
        addVartoStore("Muon_miniPFRelIso_chg");
        addVartoStore("Muon_pfRelIso03_all");
        addVartoStore("Muon_pfRelIso03_chg");
        addVartoStore("Muon_pfRelIso04_all");
        addVartoStore("Muon_jetPtRelv2");
        addVartoStore("Muon_jetRelIso");
        addVartoStore("Muon_pdgId");
        if(!_isData)
        {
            addVartoStore("Muon_genPartIdx");
            addVartoStore("Muon_genPartFlav");
        }
        // addVartoStore("goodMuonsloose");
        addVartoStore("Selected_muon_loose_pt");
    }
    addVartoStore("Selected_muon_loose_number");
    if(JEC == 0)
    {
        // addVartoStore("Selected_muon_loose_eta");
        // addVartoStore("Selected_muon_loose_phi");
        // addVartoStore("Selected_muon_loose_mass");
        // // addVartoStore("muon4vecs_loose");
        // addVartoStore("goodMuonsloosett1l");
        addVartoStore("Selected_muon_loosett1l_pt");
    }
    addVartoStore("Selected_muon_loosett1l_number");
    if(JEC == 0)
    {
        addVartoStore("Selected_muon_loosett1l_leading_pt");
        addVartoStore("Selected_muon_loosett1l_subleading_pt");
        // addVartoStore("Selected_muon_loosett1l_sum_two_muons_pt");
        // addVartoStore("Selected_muon_loosett1l_sum_all_muons_pt");   
        addVartoStore("Selected_muon_loosett1l_eta");
        addVartoStore("Selected_muon_loosett1l_leading_eta");
        addVartoStore("Selected_muon_loosett1l_subleading_eta");
        addVartoStore("Selected_muon_loosett1l_phi");
        // addVartoStore("Selected_muon_loosett1l_mass");
        // addVartoStore("Selected_muon_loosett1l_transverse_energy");
        addVartoStore("Selected_muon_loosett1l_charge");
        // addVartoStore("Selected_muon_loosett1l_charge_sum");
        // addVartoStore("Selected_muon_loosett1l_deltaeta");
        // addVartoStore("Selected_muon_loosett1l_deltaphi");
        // addVartoStore("Selected_muon_loosett1l_deltaR");
        addVartoStore("Selected_muon_loosett1l_miniPFRelIso_all");
        // addVartoStore("Selected_muon_loosett1l_miniPFRelIso_chg");
        // addVartoStore("Selected_muon_loosett1l_pfRelIso03_all");
        // addVartoStore("Selected_muon_loosett1l_pfRelIso03_chg");
        // addVartoStore("Selected_muon_loosett1l_pfRelIso04_all");
        addVartoStore("Selected_muon_loosett1l_jetPtRelv2");
        addVartoStore("Selected_muon_loosett1l_jetRelIso");
        // if(!_isData)
        // {
        //     addVartoStore("Selected_muon_loosett1l_genPartIdx");
        //     addVartoStore("Selected_muon_loosett1l_genPartpdgId");
        //     addVartoStore("Selected_muon_loosett1l_genPartIdxMother");
        //     addVartoStore("Selected_muon_loosett1l_genPartpdgIdMother");
        //     addVartoStore("Selected_muon_loosett1l_genPartFlav");
        // }
        // addVartoStore("Selected_muon_loosett1l_pdgId");
        // addVartoStore("Selected_muon_loosett1l_ip3d");
        addVartoStore("Selected_muon_loosett1l_sip3d");
        // addVartoStore("Selected_muon_loosett1l_idx");
        addVartoStore("goodMuonstemp");
        addVartoStore("Selected_muon_temp_pt");
        addVartoStore("Selected_muon_temp_number");
        // addVartoStore("Selected_muon_temp_leading_pt");
        // addVartoStore("Selected_muon_temp_subleading_pt");
        // addVartoStore("Selected_muon_temp_sum_two_muons_pt");
        // addVartoStore("Selected_muon_temp_sum_all_muons_pt");
        addVartoStore("Selected_muon_temp_eta");
        // addVartoStore("Selected_muon_temp_leading_eta");
        // addVartoStore("Selected_muon_temp_subleading_eta");
        addVartoStore("Selected_muon_temp_phi");
        // addVartoStore("Selected_muon_temp_mass");
        // addVartoStore("Selected_muon_temp_transverse_energy");
        // addVartoStore("Selected_muon_temp_charge");
        // addVartoStore("Selected_muon_temp_charge_sum");
        // addVartoStore("Selected_muon_temp_deltaeta");
        // addVartoStore("Selected_muon_temp_deltaphi");
        // addVartoStore("Selected_muon_temp_deltaR");
        addVartoStore("Selected_muon_temp_miniPFRelIso_all");
        // addVartoStore("Selected_muon_temp_miniPFRelIso_chg");
        // addVartoStore("Selected_muon_temp_pfRelIso03_all");
        // addVartoStore("Selected_muon_temp_pfRelIso03_chg");
        // addVartoStore("Selected_muon_temp_pfRelIso04_all");
        addVartoStore("Selected_muon_temp_jetPtRelv2");
        addVartoStore("Selected_muon_temp_jetRelIso");
        // if(!_isData)
        // {
        //     addVartoStore("Selected_muon_temp_genPartIdx");
        //     addVartoStore("Selected_muon_temp_genPartpdgId");
        //     addVartoStore("Selected_muon_temp_genPartIdxMother");
        //     addVartoStore("Selected_muon_temp_genPartpdgIdMother");
        //     addVartoStore("Selected_muon_temp_genPartFlav");
        // }
        // addVartoStore("Selected_muon_temp_pdgId");
        // addVartoStore("Selected_muon_temp_ip3d");
        // addVartoStore("Selected_muon_temp_sip3d");
        // addVartoStore("Selected_muon_temp_idx");
        // // addVartoStore("dis_comb_muon_temp");
        // // addVartoStore("muon4Rvecs_temp");
        addVartoStore("Selected_muon_temp_px");
        addVartoStore("Selected_muon_temp_py");
        addVartoStore("Selected_muon_temp_pz");
        // addVartoStore("Selected_muon_temp_isolated");
        // addVartoStore("goodMuons");
        addVartoStore("Selected_muon_pt");
    }
    addVartoStore("Selected_muon_number");
    if(JEC == 0)
    {
        addVartoStore("Selected_muon_leading_pt");
        addVartoStore("Selected_muon_subleading_pt");
        // addVartoStore("Selected_muon_sum_two_muons_pt");
        // addVartoStore("Selected_muon_sum_all_muons_pt");   
        addVartoStore("Selected_muon_eta");
        addVartoStore("Selected_muon_leading_eta");
        addVartoStore("Selected_muon_subleading_eta");
        addVartoStore("Selected_muon_phi");
        // addVartoStore("Selected_muon_mass");
        // addVartoStore("Selected_muon_transverse_energy");
        addVartoStore("Selected_muon_charge");
        // addVartoStore("Selected_muon_charge_sum");
        // addVartoStore("Selected_muon_deltaeta");
        // addVartoStore("Selected_muon_deltaphi");
        // addVartoStore("Selected_muon_deltaR");
        addVartoStore("Selected_muon_miniPFRelIso_all");
        // addVartoStore("Selected_muon_miniPFRelIso_chg");
        // addVartoStore("Selected_muon_pfRelIso03_all");
        // addVartoStore("Selected_muon_pfRelIso03_chg");
        // addVartoStore("Selected_muon_pfRelIso04_all");
        addVartoStore("Selected_muon_jetPtRelv2");
        addVartoStore("Selected_muon_jetRelIso");
        // if(!_isData)
        // {
        //     addVartoStore("Selected_muon_genPartIdx");
        //     addVartoStore("Selected_muon_genPartpdgId");
        //     addVartoStore("Selected_muon_genPartIdxMother");
        //     addVartoStore("Selected_muon_genPartpdgIdMother");
        //     addVartoStore("Selected_muon_genPartFlav");
        // }
        // addVartoStore("Selected_muon_pdgId");
        // addVartoStore("Selected_muon_ip3d");
        addVartoStore("Selected_muon_sip3d");
        // addVartoStore("Selected_muon_idx");
        // // addVartoStore("muon4vecs");
        // // addVartoStore("Vectorial_sum_two_muons");
        // // addVartoStore("Vectorial_sum_two_muons_pt");
        // // addVartoStore("Vectorial_sum_two_muons_eta");
        // // addVartoStore("Vectorial_sum_two_muons_phi");
        // // addVartoStore("Vectorial_sum_two_muons_mass");
        // addVartoStore("goodMuonsloosett1l_nontight");
        // addVartoStore("Selected_muon_loosett1l_nontight_pt");
        // addVartoStore("Selected_muon_loosett1l_nontight_number");
        // addVartoStore("Selected_muon_loosett1l_nontight_eta");

        // lepton
        // addVartoStore("Selected_electron_muon_loosett1l_pt");
        // addVartoStore("Selected_electron_muon_loosett1l_pt_sort");
        addVartoStore("Selected_lepton_loosett1l_pt");
        addVartoStore("Selected_lepton_loosett1l_number");
        addVartoStore("Selected_lepton_loosett1l_leading_pt");
        addVartoStore("Selected_lepton_loosett1l_subleading_pt");
        addVartoStore("Selected_lepton_loosett1l_sum_two_leptons_pt");
        // addVartoStore("Selected_lepton_loosett1l_sum_all_leptons_pt");
        addVartoStore("Selected_lepton_loosett1l_eta");
        addVartoStore("Selected_lepton_loosett1l_leading_eta");
        addVartoStore("Selected_lepton_loosett1l_subleading_eta");
        addVartoStore("Selected_lepton_loosett1l_phi");
        addVartoStore("Selected_lepton_loosett1l_mass");
        addVartoStore("Selected_lepton_loosett1l_transverse_energy");
        addVartoStore("Selected_lepton_loosett1l_charge");
        addVartoStore("Selected_lepton_loosett1l_charge_sum");
        addVartoStore("Selected_lepton_loosett1l_deltaeta");
        addVartoStore("Selected_lepton_loosett1l_deltaphi");
        addVartoStore("Selected_lepton_loosett1l_deltaR");
        addVartoStore("Selected_lepton_loosett1l_miniPFRelIso_all");
        // addVartoStore("Selected_lepton_loosett1l_miniPFRelIso_chg");
        // addVartoStore("Selected_lepton_loosett1l_pfRelIso03_all");
        // addVartoStore("Selected_lepton_loosett1l_pfRelIso03_chg");
        addVartoStore("Selected_lepton_loosett1l_jetPtRelv2");
        addVartoStore("Selected_lepton_loosett1l_jetRelIso");
        // if(!_isData)
        // {
        //     addVartoStore("Selected_lepton_loosett1l_genPartIdx");
        //     addVartoStore("Selected_lepton_loosett1l_genPartpdgId");
        //     addVartoStore("Selected_lepton_loosett1l_genPartIdxMother");
        //     addVartoStore("Selected_lepton_loosett1l_genPartpdgIdMother");
        //     addVartoStore("Selected_lepton_loosett1l_genPartFlav");
        // }
        // addVartoStore("Selected_lepton_loosett1l_pdgId");
        // addVartoStore("Selected_lepton1_loosett1l_pdgId");
        // addVartoStore("Selected_lepton2_loosett1l_pdgId");
        // addVartoStore("Selected_lepton12_loosett1l_pdgId");
        // addVartoStore("Selected_lepton_loosett1l_ip3d");
        addVartoStore("Selected_lepton_loosett1l_sip3d");
        // addVartoStore("Selected_lepton_loosett1l_idx");
        // addVartoStore("lep4vecs_loosett1l");
        // // addVartoStore("lep4Rvecs_loosett1l");
        // // addVartoStore("Vectorial_sum_two_leptons_loosett1l");
        addVartoStore("Vectorial_sum_two_leptons_loosett1l_pt");
        // addVartoStore("Vectorial_sum_two_leptons_loosett1l_eta");
        // addVartoStore("Vectorial_sum_two_leptons_loosett1l_phi");
        addVartoStore("Vectorial_sum_two_leptons_loosett1l_mass");
        addVartoStore("Mass_dilepton_false_loosett1l");
        // addVartoStore("Selected_electron_muon_pt");
        // addVartoStore("Selected_electron_muon_pt_sort");
        addVartoStore("Selected_lepton_pt");
        addVartoStore("Selected_lepton_number");
        addVartoStore("Selected_lepton_leading_pt");
        addVartoStore("Selected_lepton_subleading_pt");
    }
    addVartoStore("Selected_lepton_sum_two_leptons_pt");
    if(JEC == 0)
    {
        // addVartoStore("Selected_lepton_sum_all_leptons_pt");
        addVartoStore("Selected_lepton_eta");
        addVartoStore("Selected_lepton_leading_eta");
        addVartoStore("Selected_lepton_subleading_eta");
        addVartoStore("Selected_lepton_phi");
        addVartoStore("Selected_lepton_mass");
        addVartoStore("Selected_lepton_transverse_energy");
    }
    addVartoStore("Selected_lepton_charge");
    if(JEC == 0)
    {
        addVartoStore("Selected_lepton_charge_sum");
        addVartoStore("Selected_lepton_deltaeta");
        addVartoStore("Selected_lepton_deltaphi");
    }
    addVartoStore("Selected_lepton_deltaR");
    if(JEC == 0)
    {
        addVartoStore("Selected_lepton_miniPFRelIso_all");
        // addVartoStore("Selected_lepton_miniPFRelIso_chg");
        // addVartoStore("Selected_lepton_pfRelIso03_all");
        // addVartoStore("Selected_lepton_pfRelIso03_chg");
        addVartoStore("Selected_lepton_jetPtRelv2");
        addVartoStore("Selected_lepton_jetRelIso");
        // if(!_isData)
        // {
        //     addVartoStore("Selected_lepton_genPartIdx");
        //     addVartoStore("Selected_lepton_genPartpdgId");
        //     addVartoStore("Selected_lepton_genPartIdxMother");
        //     addVartoStore("Selected_lepton_genPartpdgIdMother");
        //     addVartoStore("Selected_lepton_genPartFlav");
        // }
        // addVartoStore("Selected_lepton_pdgId");
        // addVartoStore("Selected_lepton1_pdgId");
        // addVartoStore("Selected_lepton2_pdgId");
        // addVartoStore("Selected_lepton12_pdgId");
        // addVartoStore("Selected_lepton_ip3d");
        addVartoStore("Selected_lepton_sip3d");
        // addVartoStore("Selected_lepton_idx");
        // addVartoStore("lep4vecs");
        // // addVartoStore("lep4Rvecs");
        // addVartoStore("Selected_lepton_energy");
        // addVartoStore("Selected_lepton_px");
        // addVartoStore("Selected_lepton_py");
        // addVartoStore("Selected_lepton_pz");
        // // addVartoStore("Vectorial_sum_two_leptons");
        addVartoStore("Vectorial_sum_two_leptons_pt");
        // addVartoStore("Vectorial_sum_two_leptons_eta");
        // addVartoStore("Vectorial_sum_two_leptons_phi");
        addVartoStore("Vectorial_sum_two_leptons_mass");
    }
    addVartoStore("Mass_dilepton_false");
    
    // jets
    if(JEC == 0)
    {
        addVartoStore("Jet_pt");
    }
    addVartoStore("Jet_pt_corr");
    if(JEC == 0)
    {
        addVartoStore("Jet_eta");
        addVartoStore("Jet_phi");
        addVartoStore("Jet_mass");
    }
    addVartoStore("Jet_mass_corr");
    if(JEC == 1)
    {
        addVartoStore("Jet_energy_res_corr_fact");
    }
    if(JEC == 0)
    {
        addVartoStore("Jet_jetId");
        addVartoStore("Jet_btagDeepB");
        addVartoStore("Jet_btagDeepFlavB");
        if(!_isData)
        {
            addVartoStore("Jet_genJetIdx");
            addVartoStore("Jet_partonFlavour");
            addVartoStore("Jet_hadronFlavour");
        }
        addVartoStore("Selected_jet_loose_pt");
    }
    addVartoStore("Selected_jet_loose_number");
    if(JEC == 0)
    {
        // addVartoStore("Selected_jet_loose_eta");
        // addVartoStore("Selected_jet_loose_phi");
        // addVartoStore("Selected_jet_loose_mass");
        // addVartoStore("region_Jet_pt_nom");
        addVartoStore("Selected_jet_pt");
        addVartoStore("Selected_jet_number");
        // addVartoStore("Selected_jet_leading_pt");
        // addVartoStore("Selected_jet_subleading_pt");
        // addVartoStore("Selected_jet_subsubleading_pt");
        // addVartoStore("Selected_jet_Ht");
        // addVartoStore("Selected_jet_eta");
        // addVartoStore("Selected_jet_phi");
        // addVartoStore("Selected_jet_mass");
        // addVartoStore("Selected_jet_btagDeepB");
        // addVartoStore("Selected_jet_btagDeepFlavB");
        // if(!_isData)
        // {
        //     addVartoStore("Selected_jet_genJetIdx");
        //     addVartoStore("Selected_jet_partonFlavour");
        //     addVartoStore("Selected_jet_hadronFlavour");
        // }
        // addVartoStore("Selected_jet_jetId");
        // addVartoStore("Selected_jet_puId");
        // addVartoStore("jet4vecs");
        // addVartoStore("Lepton_Jet_overlap_loosett1l");
        addVartoStore("Selected_clean_jet_loosett1l_pt");
        addVartoStore("Selected_clean_jet_loosett1l_number");
        addVartoStore("Selected_clean_jet_loosett1l_leading_pt");
        addVartoStore("Selected_clean_jet_loosett1l_subleading_pt");
        addVartoStore("Selected_clean_jet_loosett1l_subsubleading_pt");
        addVartoStore("Selected_clean_jet_loosett1l_Ht");
        addVartoStore("Selected_clean_jet_loosett1l_eta");
        addVartoStore("Selected_clean_jet_loosett1l_phi");
        addVartoStore("Selected_clean_jet_loosett1l_mass");
        // addVartoStore("Selected_clean_jet_loosett1l_transverse_energy");
        addVartoStore("Selected_clean_jet_loosett1l_btagDeepB");
        addVartoStore("Selected_clean_jet_loosett1l_btagDeepFlavB");
        // if(!_isData)
        // {
        //     addVartoStore("Selected_clean_jet_loosett1l_genJetIdx");
        //     addVartoStore("Selected_clean_jet_loosett1l_partonFlavour");
        //     addVartoStore("Selected_clean_jet_loosett1l_hadronFlavour");
        // }
        // addVartoStore("Selected_clean_jet_loosett1l_jetId");
        // addVartoStore("Selected_clean_jet_loosett1l_puId");
        // addVartoStore("cleanjet4vecs_loosett1l");
        // // addVartoStore("cleanjet4Rvecs_loosett1l");
        // addVartoStore("Selected_clean_jet_loosett1l_energy");
        // addVartoStore("Selected_clean_jet_loosett1l_px");
        // addVartoStore("Selected_clean_jet_loosett1l_py");
        // addVartoStore("Selected_clean_jet_loosett1l_pz");
        // addVartoStore("Lepton_Jet_overlap");
        addVartoStore("Selected_clean_jet_pt");
    }
    addVartoStore("Selected_clean_jet_number");
    if(JEC == 0)
    {
        addVartoStore("Selected_clean_jet_leading_pt");
        addVartoStore("Selected_clean_jet_subleading_pt");
        addVartoStore("Selected_clean_jet_subsubleading_pt");
        addVartoStore("Selected_clean_jet_Ht");
        addVartoStore("Selected_clean_jet_eta");
        addVartoStore("Selected_clean_jet_phi");
        addVartoStore("Selected_clean_jet_mass");
        // addVartoStore("Selected_clean_jet_transverse_energy");
        addVartoStore("Selected_clean_jet_btagDeepB");
        addVartoStore("Selected_clean_jet_btagDeepFlavB");
        // if(!_isData)
        // {
        //     addVartoStore("Selected_clean_jet_genJetIdx");
        //     addVartoStore("Selected_clean_jet_partonFlavour");
        //     addVartoStore("Selected_clean_jet_hadronFlavour");
        // }
        // addVartoStore("Selected_clean_jet_jetId");
        // addVartoStore("Selected_clean_jet_puId");
        // addVartoStore("cleanjet4vecs");
        // // addVartoStore("cleanjet4Rvecs");
        // addVartoStore("Selected_clean_jet_energy");
        // addVartoStore("Selected_clean_jet_px");
        // addVartoStore("Selected_clean_jet_py");
        // addVartoStore("Selected_clean_jet_pz");
        // // addVartoStore("goodJets_btag");
        addVartoStore("Selected_bjet_pt");
        addVartoStore("Selected_bjet_number");
        // addVartoStore("Selected_bjet_leading_pt");
        // addVartoStore("Selected_bjet_subleading_pt");
        // addVartoStore("Selected_bjet_subsubleading_pt");
        // addVartoStore("Selected_bjet_sum_all_jets_pt");
        // addVartoStore("Selected_bjet_eta");
        // addVartoStore("Selected_bjet_phi");
        // addVartoStore("Selected_bjet_mass");
        // addVartoStore("Selected_bjet_btagDeepB");
        // addVartoStore("Selected_bjet_btagDeepFlavB");
        // if(!_isData)
        // {
        //     addVartoStore("Selected_bjet_genJetIdx");
        //     addVartoStore("Selected_bjet_partonFlavour");
        //     addVartoStore("Selected_bjet_hadronFlavour");
        // }
        // addVartoStore("Selected_bjet_jetId");
        // addVartoStore("Selected_bjet_puId");
        // addVartoStore("bjet4vecs");
        // addVartoStore("Lepton_bJet_overlap_loosett1l");
        addVartoStore("Selected_clean_bjet_loosett1l_pt");
        addVartoStore("Selected_clean_bjet_loosett1l_number");
        addVartoStore("Selected_clean_bjet_loosett1l_leading_pt");
        addVartoStore("Selected_clean_bjet_loosett1l_subleading_pt");
        addVartoStore("Selected_clean_bjet_loosett1l_subsubleading_pt");
        addVartoStore("Selected_clean_bjet_loosett1l_sum_all_jets_pt");
        addVartoStore("Selected_clean_bjet_loosett1l_eta");
        addVartoStore("Selected_clean_bjet_loosett1l_phi");
        addVartoStore("Selected_clean_bjet_loosett1l_mass");
        // addVartoStore("Selected_clean_bjet_loosett1l_transverse_energy");
        addVartoStore("Selected_clean_bjet_loosett1l_btagDeepB");
        addVartoStore("Selected_clean_bjet_loosett1l_btagDeepFlavB");
        // if(!_isData)
        // {
        //     addVartoStore("Selected_clean_bjet_loosett1l_genJetIdx");
        //     addVartoStore("Selected_clean_bjet_loosett1l_partonFlavour");
        //     addVartoStore("Selected_clean_bjet_loosett1l_hadronFlavour");
        // }
        // addVartoStore("Selected_clean_bjet_loosett1l_jetId");
        // addVartoStore("Selected_clean_bjet_loosett1l_puId");
        // addVartoStore("cleanbjet4vecs_loosett1l");
        // // addVartoStore("cleanbjet4Rvecs_loosett1l");
        // addVartoStore("Selected_clean_bjet_loosett1l_energy");
        // addVartoStore("Selected_clean_bjet_loosett1l_px");
        // addVartoStore("Selected_clean_bjet_loosett1l_py");
        // addVartoStore("Selected_clean_bjet_loosett1l_pz");
        // addVartoStore("Lepton_bJet_overlap");
        addVartoStore("Selected_clean_bjet_pt");
    }
    addVartoStore("Selected_clean_bjet_number");
    if(JEC == 0)
    {
        addVartoStore("Selected_clean_bjet_leading_pt");
        addVartoStore("Selected_clean_bjet_subleading_pt");
        addVartoStore("Selected_clean_bjet_subsubleading_pt");
        addVartoStore("Selected_clean_bjet_sum_all_jets_pt");
        addVartoStore("Selected_clean_bjet_eta");
        addVartoStore("Selected_clean_bjet_phi");
        addVartoStore("Selected_clean_bjet_mass");
        // addVartoStore("Selected_clean_bjet_transverse_energy");
        addVartoStore("Selected_clean_bjet_btagDeepB");
        addVartoStore("Selected_clean_bjet_btagDeepFlavB");
        // if(!_isData)
        // {
        //     addVartoStore("Selected_clean_bjet_genJetIdx");
        //     addVartoStore("Selected_clean_bjet_partonFlavour");
        //     addVartoStore("Selected_clean_bjet_hadronFlavour");
        // }
        // addVartoStore("Selected_clean_bjet_jetId");
        // addVartoStore("Selected_clean_bjet_puId");
        // addVartoStore("cleanbjet4vecs");
        // // addVartoStore("cleanbjet4Rvecs");
        // addVartoStore("Selected_clean_bjet_energy");
        // addVartoStore("Selected_clean_bjet_px");
        // addVartoStore("Selected_clean_bjet_py");
        // addVartoStore("Selected_clean_bjet_pz");

        // jets BTag Efficiency
        if(!_isData)
        {
            // addVartoStore("Selected_clean_bjet_btagpass_bcflav_loosett1l");
            addVartoStore("Selected_clean_bjet_loosett1l_btagpass_bcflav_pt");
            addVartoStore("Selected_clean_bjet_loosett1l_btagpass_bcflav_eta");
            // addVartoStore("Selected_clean_jet_all_bcflav_loosett1l");
            addVartoStore("Selected_clean_jet_loosett1l_all_bcflav_pt");
            addVartoStore("Selected_clean_jet_loosett1l_all_bcflav_eta");
            // addVartoStore("Selected_clean_bjet_btagpass_lflav_loosett1l");
            addVartoStore("Selected_clean_bjet_loosett1l_btagpass_lflav_pt");
            addVartoStore("Selected_clean_bjet_loosett1l_btagpass_lflav_eta");
            // addVartoStore("Selected_clean_jet_all_lflav_loosett1l");
            addVartoStore("Selected_clean_jet_loosett1l_all_lflav_pt");
            addVartoStore("Selected_clean_jet_loosett1l_all_lflav_eta");
            // addVartoStore("Selected_clean_bjet_btagpass_bcflav");
            addVartoStore("Selected_clean_bjet_btagpass_bcflav_pt");
            addVartoStore("Selected_clean_bjet_btagpass_bcflav_eta");
            // addVartoStore("Selected_clean_jet_all_bcflav");
            addVartoStore("Selected_clean_jet_all_bcflav_pt");
            addVartoStore("Selected_clean_jet_all_bcflav_eta");
            // addVartoStore("Selected_clean_bjet_btagpass_lflav");
            addVartoStore("Selected_clean_bjet_btagpass_lflav_pt");
            addVartoStore("Selected_clean_bjet_btagpass_lflav_eta");
            // addVartoStore("Selected_clean_jet_all_lflav");
            addVartoStore("Selected_clean_jet_all_lflav_pt");
            addVartoStore("Selected_clean_jet_all_lflav_eta");
        }

        // MET
        // addVartoStore("goodMET");
        // addVartoStore("Selected_MET_pt");
        // addVartoStore("Selected_MET_sum_pt_unclustered");
        // addVartoStore("Selected_MET_phi");
        // addVartoStore("Selected_MET_sum_transverse_energy");
        addVartoStore("MET_pt");
        addVartoStore("MET_pt_corr");
        // addVartoStore("MET_sumPtUnclustered");
        addVartoStore("MET_phi");
        addVartoStore("MET_phi_corr");
        // addVartoStore("MET_sumEt");
        // addVartoStore("MET_px");
        // addVartoStore("MET_py");

        // // combination of particles
        addVartoStore("St_loosett1l");
        addVartoStore("St_with_MET_loosett1l");
        // // addVartoStore("dis_comb_leptonbjet_loosett1l");
        // // addVartoStore("Selected_lepton1_loosett1l");
        // // addVartoStore("Selected_clean_bjet1_loosett1l");
        // // addVartoStore("LeptonbjetFromtop4Rvecs_loosett1l");
        // addVartoStore("LeptonbjetFromtop_loosett1l_pt");
        // addVartoStore("LeptonbjetFromtop_loosett1l_eta");
        // addVartoStore("LeptonbjetFromtop_loosett1l_phi");
        // addVartoStore("LeptonbjetFromtop_loosett1l_mass");
        // addVartoStore("LeptonbjetFromtop_loosett1l_energy");
        // addVartoStore("LeptonbjetFromtop_loosett1l_transverse_energy");
        // addVartoStore("LeptonbjetFromtop_loosett1l_deltaeta");
        // addVartoStore("LeptonbjetFromtop_loosett1l_deltaphi");
        // addVartoStore("LeptonbjetFromtop_loosett1l_deltaR");
        // addVartoStore("LeptonbjetFromtop_loosett1l_transverse_mass");
        // // addVartoStore("Min_deltaR_bJetFromtop_leptonFromtop_loosett1l");
        // // addVartoStore("Lepton1FromHiggs_loosett1l");
        // addVartoStore("Lepton1FromHiggs_loosett1l_pt");
        // addVartoStore("Lepton1FromHiggs_loosett1l_eta");
        // addVartoStore("Lepton1FromHiggs_loosett1l_phi");
        // addVartoStore("Lepton1FromHiggs_loosett1l_mass");
        // addVartoStore("Lepton1FromHiggs_loosett1l_transverse_energy");
        // // addVartoStore("Lepton2Fromtop_loosett1l");
        // addVartoStore("Lepton2Fromtop_loosett1l_pt");
        // addVartoStore("Lepton2Fromtop_loosett1l_eta");
        // addVartoStore("Lepton2Fromtop_loosett1l_phi");
        // addVartoStore("Lepton2Fromtop_loosett1l_mass");
        // addVartoStore("Lepton2Fromtop_loosett1l_transverse_energy");
        // // addVartoStore("bJet1Fromtop_loosett1l");
        // addVartoStore("bJet1Fromtop_loosett1l_pt");
        // addVartoStore("bJet1Fromtop_loosett1l_eta");
        // addVartoStore("bJet1Fromtop_loosett1l_phi");
        // addVartoStore("bJet1Fromtop_loosett1l_mass");
        // addVartoStore("bJet1Fromtop_loosett1l_transverse_energy");
        // // addVartoStore("cleanjet4Rvecs_withoutbjet_loosett1l");
        // addVartoStore("cleanjet4Rvecs_withoutbjet_loosett1l_pt");
        // addVartoStore("cleanjet4Rvecs_withoutbjet_loosett1l_number");
        // addVartoStore("cleanjet4Rvecs_withoutbjet_loosett1l_eta");
        // addVartoStore("cleanjet4Rvecs_withoutbjet_loosett1l_phi");
        // addVartoStore("cleanjet4Rvecs_withoutbjet_loosett1l_mass");
        // addVartoStore("cleanjet4Rvecs_withoutbjet_loosett1l_transverse_energy");
        // addVartoStore("cleanjet4vecs_withoutbjet_loosett1l");
        // addVartoStore("dis_comb_jets_withoutbjet_loosett1l");
        // // addVartoStore("Selected_jet1_loosett1l");
        // // addVartoStore("Selected_jet2_loosett1l");
        // // addVartoStore("JetsFromHiggs4Rvecs_loosett1l");
        // addVartoStore("JetsFromHiggs_loosett1l_pt");
        // addVartoStore("JetsFromHiggs_loosett1l_eta");
        // addVartoStore("JetsFromHiggs_loosett1l_phi");
        // addVartoStore("JetsFromHiggs_loosett1l_mass");
        // addVartoStore("JetsFromHiggs_loosett1l_energy");
        // addVartoStore("JetsFromHiggs_loosett1l_transverse_energy");
        // addVartoStore("JetsFromHiggs_loosett1l_deltaeta");
        // addVartoStore("JetsFromHiggs_loosett1l_deltaphi");
        // addVartoStore("JetsFromHiggs_loosett1l_deltaR");
        // addVartoStore("JetsFromHiggs_loosett1l_transverse_mass");
        // addVartoStore("Min_deltaR_JetsFromHiggs_loosett1l");
        // // addVartoStore("Jet1FromHiggs_loosett1l");
        // addVartoStore("Jet1FromHiggs_loosett1l_pt");
        // addVartoStore("Jet1FromHiggs_loosett1l_eta");
        // addVartoStore("Jet1FromHiggs_loosett1l_phi");
        // addVartoStore("Jet1FromHiggs_loosett1l_mass");
        // addVartoStore("Jet1FromHiggs_loosett1l_transverse_energy");
        // // addVartoStore("Jet2FromHiggs_loosett1l");
        // addVartoStore("Jet2FromHiggs_loosett1l_pt");
        // addVartoStore("Jet2FromHiggs_loosett1l_eta");
        // addVartoStore("Jet2FromHiggs_loosett1l_phi");
        // addVartoStore("Jet2FromHiggs_loosett1l_mass");
        // addVartoStore("Jet2FromHiggs_loosett1l_transverse_energy");
        // addVartoStore("Jet1FromHiggs_Jet2FromHiggs_loosett1l");
        // addVartoStore("Jet1FromHiggs_Jet2FromHiggs_loosett1l_pt");
        // addVartoStore("Jet1FromHiggs_Jet2FromHiggs_loosett1l_eta");
        // addVartoStore("Jet1FromHiggs_Jet2FromHiggs_loosett1l_phi");
        // addVartoStore("Jet1FromHiggs_Jet2FromHiggs_loosett1l_mass");
        // addVartoStore("Jet1FromHiggs_Jet2FromHiggs_loosett1l_transverse_energy");
        // addVartoStore("Jet1FromHiggs_Jet2FromHiggs_loosett1l_deltaeta");
        // addVartoStore("Jet1FromHiggs_Jet2FromHiggs_loosett1l_deltaphi");
        // addVartoStore("Jet1FromHiggs_Jet2FromHiggs_loosett1l_deltaR");
        // addVartoStore("Jet1FromHiggs_Jet2FromHiggs_loosett1l_transverse_mass");
        addVartoStore("Vectorial_sum_three_clean_jets_loosett1l_mass");
        addVartoStore("Vectorial_sum_three_clean_jets_loosett1l_mass_offset");
        addVartoStore("Vectorial_sum_three_clean_jets_loosett1l_mass_min");
        // // addVartoStore("Lepton2Fromtop_bJet1Fromtop_loosett1l");
        // addVartoStore("Lepton2Fromtop_bJet1Fromtop_loosett1l_pt");
        // addVartoStore("Lepton2Fromtop_bJet1Fromtop_loosett1l_eta");
        // addVartoStore("Lepton2Fromtop_bJet1Fromtop_loosett1l_phi");
        addVartoStore("Lepton2Fromtop_bJet1Fromtop_loosett1l_mass");
        // addVartoStore("Lepton2Fromtop_bJet1Fromtop_loosett1l_transverse_energy");
        // addVartoStore("Lepton2Fromtop_bJet1Fromtop_loosett1l_deltaeta");
        // addVartoStore("Lepton2Fromtop_bJet1Fromtop_loosett1l_deltaphi");
        // addVartoStore("Lepton2Fromtop_bJet1Fromtop_loosett1l_deltaR");
        // addVartoStore("Lepton2Fromtop_bJet1Fromtop_loosett1l_transverse_mass");
        // // addVartoStore("Lepton2Fromtop_Jet1FromHiggs_loosett1l");
        // addVartoStore("Lepton2Fromtop_Jet1FromHiggs_loosett1l_pt");
        // addVartoStore("Lepton2Fromtop_Jet1FromHiggs_loosett1l_eta");
        // addVartoStore("Lepton2Fromtop_Jet1FromHiggs_loosett1l_phi");
        // addVartoStore("Lepton2Fromtop_Jet1FromHiggs_loosett1l_mass");
        // addVartoStore("Lepton2Fromtop_Jet1FromHiggs_loosett1l_transverse_energy");
        // addVartoStore("Lepton2Fromtop_Jet1FromHiggs_loosett1l_deltaeta");
        // addVartoStore("Lepton2Fromtop_Jet1FromHiggs_loosett1l_deltaphi");
        // addVartoStore("Lepton2Fromtop_Jet1FromHiggs_loosett1l_deltaR");
        // addVartoStore("Lepton2Fromtop_Jet1FromHiggs_loosett1l_transverse_mass");
        // // addVartoStore("Lepton2Fromtop_Jet2FromHiggs_loosett1l");
        // addVartoStore("Lepton2Fromtop_Jet2FromHiggs_loosett1l_pt");
        // addVartoStore("Lepton2Fromtop_Jet2FromHiggs_loosett1l_eta");
        // addVartoStore("Lepton2Fromtop_Jet2FromHiggs_loosett1l_phi");
        // addVartoStore("Lepton2Fromtop_Jet2FromHiggs_loosett1l_mass");
        // addVartoStore("Lepton2Fromtop_Jet2FromHiggs_loosett1l_transverse_energy");
        // addVartoStore("Lepton2Fromtop_Jet2FromHiggs_loosett1l_deltaeta");
        // addVartoStore("Lepton2Fromtop_Jet2FromHiggs_loosett1l_deltaphi");
        // addVartoStore("Lepton2Fromtop_Jet2FromHiggs_loosett1l_deltaR");
        // addVartoStore("Lepton2Fromtop_Jet2FromHiggs_loosett1l_transverse_mass");
        // // addVartoStore("Lepton1FromHiggs_bJet1Fromtop_loosett1l");
        // addVartoStore("Lepton1FromHiggs_bJet1Fromtop_loosett1l_pt");
        // addVartoStore("Lepton1FromHiggs_bJet1Fromtop_loosett1l_eta");
        // addVartoStore("Lepton1FromHiggs_bJet1Fromtop_loosett1l_phi");
        // addVartoStore("Lepton1FromHiggs_bJet1Fromtop_loosett1l_mass");
        // addVartoStore("Lepton1FromHiggs_bJet1Fromtop_loosett1l_transverse_energy");
        // addVartoStore("Lepton1FromHiggs_bJet1Fromtop_loosett1l_deltaeta");
        // addVartoStore("Lepton1FromHiggs_bJet1Fromtop_loosett1l_deltaphi");
        // addVartoStore("Lepton1FromHiggs_bJet1Fromtop_loosett1l_deltaR");
        // addVartoStore("Lepton1FromHiggs_bJet1Fromtop_loosett1l_transverse_mass");
        // // addVartoStore("Lepton1FromHiggs_Jet1FromHiggs_loosett1l");
        // addVartoStore("Lepton1FromHiggs_Jet1FromHiggs_loosett1l_pt");
        // addVartoStore("Lepton1FromHiggs_Jet1FromHiggs_loosett1l_eta");
        // addVartoStore("Lepton1FromHiggs_Jet1FromHiggs_loosett1l_phi");
        // addVartoStore("Lepton1FromHiggs_Jet1FromHiggs_loosett1l_mass");
        // addVartoStore("Lepton1FromHiggs_Jet1FromHiggs_loosett1l_transverse_energy");
        // addVartoStore("Lepton1FromHiggs_Jet1FromHiggs_loosett1l_deltaeta");
        // addVartoStore("Lepton1FromHiggs_Jet1FromHiggs_loosett1l_deltaphi");
        // addVartoStore("Lepton1FromHiggs_Jet1FromHiggs_loosett1l_deltaR");
        // addVartoStore("Lepton1FromHiggs_Jet1FromHiggs_loosett1l_transverse_mass");
        // // addVartoStore("Lepton1FromHiggs_Jet2FromHiggs_loosett1l");
        // addVartoStore("Lepton1FromHiggs_Jet2FromHiggs_loosett1l_pt");
        // addVartoStore("Lepton1FromHiggs_Jet2FromHiggs_loosett1l_eta");
        // addVartoStore("Lepton1FromHiggs_Jet2FromHiggs_loosett1l_phi");
        // addVartoStore("Lepton1FromHiggs_Jet2FromHiggs_loosett1l_mass");
        // addVartoStore("Lepton1FromHiggs_Jet2FromHiggs_loosett1l_transverse_energy");
        // addVartoStore("Lepton1FromHiggs_Jet2FromHiggs_loosett1l_deltaeta");
        // addVartoStore("Lepton1FromHiggs_Jet2FromHiggs_loosett1l_deltaphi");
        // addVartoStore("Lepton1FromHiggs_Jet2FromHiggs_loosett1l_deltaR");
        // addVartoStore("Lepton1FromHiggs_Jet2FromHiggs_loosett1l_transverse_mass");
        // // addVartoStore("Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_loosett1l");
        // addVartoStore("Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_loosett1l_pt");
        // addVartoStore("Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_loosett1l_eta");
        // addVartoStore("Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_loosett1l_phi");
        // addVartoStore("Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_loosett1l_mass");
        // addVartoStore("Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_loosett1l_transverse_energy");
        // addVartoStore("Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_loosett1l_deltaeta");
        // addVartoStore("Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_loosett1l_deltaphi");
        // addVartoStore("Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_loosett1l_deltaR");
        // addVartoStore("Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_loosett1l_transverse_mass");
        // // addVartoStore("L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l");
        // addVartoStore("L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_pt");
        // addVartoStore("L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_eta");
        // addVartoStore("L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_phi");
        // addVartoStore("L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_px");
        // addVartoStore("L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_py");
        // addVartoStore("L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_mass");
        // addVartoStore("L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_transverse_energy");
        // addVartoStore("L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_deltaeta");
        // addVartoStore("L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_deltaphi");
        // addVartoStore("L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_deltaR");
        // addVartoStore("L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_transverse_mass");
        // addVartoStore("L2bJ1Fromtop_L1J1J2FromHiggs_loosett1l_Relative_St");
        addVartoStore("Tprime_transverse_mass_first_loosett1l_600");
        addVartoStore("Tprime_transverse_mass_second_loosett1l_600");
        addVartoStore("Tprime_transverse_mass_first_loosett1l_625");
        addVartoStore("Tprime_transverse_mass_second_loosett1l_625");
        addVartoStore("Tprime_transverse_mass_first_loosett1l_650");
        addVartoStore("Tprime_transverse_mass_second_loosett1l_650");
        addVartoStore("Tprime_transverse_mass_first_loosett1l_675");
        addVartoStore("Tprime_transverse_mass_second_loosett1l_675");
        addVartoStore("Tprime_transverse_mass_first_loosett1l_700");
        addVartoStore("Tprime_transverse_mass_second_loosett1l_700");
        addVartoStore("Tprime_transverse_mass_first_loosett1l_800");
        addVartoStore("Tprime_transverse_mass_second_loosett1l_800");
        addVartoStore("Tprime_transverse_mass_first_loosett1l_900");
        addVartoStore("Tprime_transverse_mass_second_loosett1l_900");
        addVartoStore("Tprime_transverse_mass_first_loosett1l_1000");
        addVartoStore("Tprime_transverse_mass_second_loosett1l_1000");
        addVartoStore("Tprime_transverse_mass_first_loosett1l_1100");
        addVartoStore("Tprime_transverse_mass_second_loosett1l_1100");
        addVartoStore("Tprime_transverse_mass_first_loosett1l_1200");
        addVartoStore("Tprime_transverse_mass_second_loosett1l_1200");
        addVartoStore("St");
        addVartoStore("St_with_MET");
        // // addVartoStore("dis_comb_leptonbjet");
        // // addVartoStore("Selected_lepton1");
        // // addVartoStore("Selected_clean_bjet1");
        // // addVartoStore("LeptonbjetFromtop4Rvecs");
        // addVartoStore("LeptonbjetFromtop_pt");
        // addVartoStore("LeptonbjetFromtop_eta");
        // addVartoStore("LeptonbjetFromtop_phi");
        // addVartoStore("LeptonbjetFromtop_mass");
        // addVartoStore("LeptonbjetFromtop_energy");
        // addVartoStore("LeptonbjetFromtop_transverse_energy");
        // addVartoStore("LeptonbjetFromtop_deltaeta");
        // addVartoStore("LeptonbjetFromtop_deltaphi");
        // addVartoStore("LeptonbjetFromtop_deltaR");
        // addVartoStore("LeptonbjetFromtop_transverse_mass");
        // // addVartoStore("Min_deltaR_bJetFromtop_leptonFromtop");
        // // addVartoStore("Lepton1FromHiggs");
        // addVartoStore("Lepton1FromHiggs_pt");
        // addVartoStore("Lepton1FromHiggs_eta");
        // addVartoStore("Lepton1FromHiggs_phi");
        // addVartoStore("Lepton1FromHiggs_mass");
        // addVartoStore("Lepton1FromHiggs_transverse_energy");
        // // addVartoStore("Lepton2Fromtop");
        // addVartoStore("Lepton2Fromtop_pt");
        // addVartoStore("Lepton2Fromtop_eta");
        // addVartoStore("Lepton2Fromtop_phi");
        // addVartoStore("Lepton2Fromtop_mass");
        // addVartoStore("Lepton2Fromtop_transverse_energy");
        // // addVartoStore("bJet1Fromtop");
        // addVartoStore("bJet1Fromtop_pt");
        // addVartoStore("bJet1Fromtop_eta");
        // addVartoStore("bJet1Fromtop_phi");
        // addVartoStore("bJet1Fromtop_mass");
        // addVartoStore("bJet1Fromtop_transverse_energy");
        // // addVartoStore("cleanjet4Rvecs_withoutbjet");
        // addVartoStore("cleanjet4Rvecs_withoutbjet_pt");
        // addVartoStore("cleanjet4Rvecs_withoutbjet_number");
        // addVartoStore("cleanjet4Rvecs_withoutbjet_eta");
        // addVartoStore("cleanjet4Rvecs_withoutbjet_phi");
        // addVartoStore("cleanjet4Rvecs_withoutbjet_mass");
        // addVartoStore("cleanjet4Rvecs_withoutbjet_transverse_energy");
        // addVartoStore("cleanjet4vecs_withoutbjet");
        // // addVartoStore("dis_comb_jets_withoutbjet");
        // // addVartoStore("Selected_jet1");
        // // addVartoStore("Selected_jet2");
        // // addVartoStore("JetsFromHiggs4Rvecs");
        // addVartoStore("JetsFromHiggs_pt");
        // addVartoStore("JetsFromHiggs_eta");
        // addVartoStore("JetsFromHiggs_phi");
        // addVartoStore("JetsFromHiggs_mass");
        // addVartoStore("JetsFromHiggs_energy");
        // addVartoStore("JetsFromHiggs_transverse_energy");
        // addVartoStore("JetsFromHiggs_deltaeta");
        // addVartoStore("JetsFromHiggs_deltaphi");
        // addVartoStore("JetsFromHiggs_deltaR");
        // addVartoStore("JetsFromHiggs_transverse_mass");
        // addVartoStore("Min_deltaR_JetsFromHiggs");
        // // addVartoStore("Jet1FromHiggs");
        // addVartoStore("Jet1FromHiggs_pt");
        // addVartoStore("Jet1FromHiggs_eta");
        // addVartoStore("Jet1FromHiggs_phi");
        // addVartoStore("Jet1FromHiggs_mass");
        // addVartoStore("Jet1FromHiggs_transverse_energy");
        // // addVartoStore("Jet2FromHiggs");
        // addVartoStore("Jet2FromHiggs_pt");
        // addVartoStore("Jet2FromHiggs_eta");
        // addVartoStore("Jet2FromHiggs_phi");
        // addVartoStore("Jet2FromHiggs_mass");
        // addVartoStore("Jet2FromHiggs_transverse_energy");
        // // addVartoStore("Jet1FromHiggs_Jet2FromHiggs");
        // addVartoStore("Jet1FromHiggs_Jet2FromHiggs_pt");
        // addVartoStore("Jet1FromHiggs_Jet2FromHiggs_eta");
        // addVartoStore("Jet1FromHiggs_Jet2FromHiggs_phi");
        // addVartoStore("Jet1FromHiggs_Jet2FromHiggs_mass");
        // addVartoStore("Jet1FromHiggs_Jet2FromHiggs_transverse_energy");
        // addVartoStore("Jet1FromHiggs_Jet2FromHiggs_deltaeta");
        // addVartoStore("Jet1FromHiggs_Jet2FromHiggs_deltaphi");
        // addVartoStore("Jet1FromHiggs_Jet2FromHiggs_deltaR");
        // addVartoStore("Jet1FromHiggs_Jet2FromHiggs_transverse_mass");
        addVartoStore("Vectorial_sum_three_clean_jets_mass");
        addVartoStore("Vectorial_sum_three_clean_jets_mass_offset");
    }
    addVartoStore("Vectorial_sum_three_clean_jets_mass_min");
    if(JEC == 0)
    {
        // // addVartoStore("Lepton2Fromtop_bJet1Fromtop");
        // addVartoStore("Lepton2Fromtop_bJet1Fromtop_pt");
        // addVartoStore("Lepton2Fromtop_bJet1Fromtop_eta");
        // addVartoStore("Lepton2Fromtop_bJet1Fromtop_phi");
        addVartoStore("Lepton2Fromtop_bJet1Fromtop_mass");
        // addVartoStore("Lepton2Fromtop_bJet1Fromtop_transverse_energy");
        // addVartoStore("Lepton2Fromtop_bJet1Fromtop_deltaeta");
        // addVartoStore("Lepton2Fromtop_bJet1Fromtop_deltaphi");
        // addVartoStore("Lepton2Fromtop_bJet1Fromtop_deltaR");
        // addVartoStore("Lepton2Fromtop_bJet1Fromtop_transverse_mass");
        // // addVartoStore("Lepton2Fromtop_Jet1FromHiggs");
        // addVartoStore("Lepton2Fromtop_Jet1FromHiggs_pt");
        // addVartoStore("Lepton2Fromtop_Jet1FromHiggs_eta");
        // addVartoStore("Lepton2Fromtop_Jet1FromHiggs_phi");
        // addVartoStore("Lepton2Fromtop_Jet1FromHiggs_mass");
        // addVartoStore("Lepton2Fromtop_Jet1FromHiggs_transverse_energy");
        // addVartoStore("Lepton2Fromtop_Jet1FromHiggs_deltaeta");
        // addVartoStore("Lepton2Fromtop_Jet1FromHiggs_deltaphi");
        // addVartoStore("Lepton2Fromtop_Jet1FromHiggs_deltaR");
        // addVartoStore("Lepton2Fromtop_Jet1FromHiggs_transverse_mass");
        // // addVartoStore("Lepton2Fromtop_Jet2FromHiggs");
        // addVartoStore("Lepton2Fromtop_Jet2FromHiggs_pt");
        // addVartoStore("Lepton2Fromtop_Jet2FromHiggs_eta");
        // addVartoStore("Lepton2Fromtop_Jet2FromHiggs_phi");
        // addVartoStore("Lepton2Fromtop_Jet2FromHiggs_mass");
        // addVartoStore("Lepton2Fromtop_Jet2FromHiggs_transverse_energy");
        // addVartoStore("Lepton2Fromtop_Jet2FromHiggs_deltaeta");
        // addVartoStore("Lepton2Fromtop_Jet2FromHiggs_deltaphi");
        // addVartoStore("Lepton2Fromtop_Jet2FromHiggs_deltaR");
        // addVartoStore("Lepton2Fromtop_Jet2FromHiggs_transverse_mass");
        // // addVartoStore("Lepton1FromHiggs_bJet1Fromtop");
        // addVartoStore("Lepton1FromHiggs_bJet1Fromtop_pt");
        // addVartoStore("Lepton1FromHiggs_bJet1Fromtop_eta");
        // addVartoStore("Lepton1FromHiggs_bJet1Fromtop_phi");
        // addVartoStore("Lepton1FromHiggs_bJet1Fromtop_mass");
        // addVartoStore("Lepton1FromHiggs_bJet1Fromtop_transverse_energy");
        // addVartoStore("Lepton1FromHiggs_bJet1Fromtop_deltaeta");
        // addVartoStore("Lepton1FromHiggs_bJet1Fromtop_deltaphi");
        // addVartoStore("Lepton1FromHiggs_bJet1Fromtop_deltaR");
        // addVartoStore("Lepton1FromHiggs_bJet1Fromtop_transverse_mass");
        // // addVartoStore("Lepton1FromHiggs_Jet1FromHiggs");
        // addVartoStore("Lepton1FromHiggs_Jet1FromHiggs_pt");
        // addVartoStore("Lepton1FromHiggs_Jet1FromHiggs_eta");
        // addVartoStore("Lepton1FromHiggs_Jet1FromHiggs_phi");
        // addVartoStore("Lepton1FromHiggs_Jet1FromHiggs_mass");
        // addVartoStore("Lepton1FromHiggs_Jet1FromHiggs_transverse_energy");
        // addVartoStore("Lepton1FromHiggs_Jet1FromHiggs_deltaeta");
        // addVartoStore("Lepton1FromHiggs_Jet1FromHiggs_deltaphi");
        // addVartoStore("Lepton1FromHiggs_Jet1FromHiggs_deltaR");
        // addVartoStore("Lepton1FromHiggs_Jet1FromHiggs_transverse_mass");
        // // addVartoStore("Lepton1FromHiggs_Jet2FromHiggs");
        // addVartoStore("Lepton1FromHiggs_Jet2FromHiggs_pt");
        // addVartoStore("Lepton1FromHiggs_Jet2FromHiggs_eta");
        // addVartoStore("Lepton1FromHiggs_Jet2FromHiggs_phi");
        // addVartoStore("Lepton1FromHiggs_Jet2FromHiggs_mass");
        // addVartoStore("Lepton1FromHiggs_Jet2FromHiggs_transverse_energy");
        // addVartoStore("Lepton1FromHiggs_Jet2FromHiggs_deltaeta");
        // addVartoStore("Lepton1FromHiggs_Jet2FromHiggs_deltaphi");
        // addVartoStore("Lepton1FromHiggs_Jet2FromHiggs_deltaR");
        // addVartoStore("Lepton1FromHiggs_Jet2FromHiggs_transverse_mass");
        // // addVartoStore("Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs");
        // addVartoStore("Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_pt");
        // addVartoStore("Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_eta");
        // addVartoStore("Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_phi");
        // addVartoStore("Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_mass");
        // addVartoStore("Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_transverse_energy");
        // addVartoStore("Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_deltaeta");
        // addVartoStore("Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_deltaphi");
        // addVartoStore("Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_deltaR");
        // addVartoStore("Lepton1FromHiggs_Jet1FromHiggs_Jet2FromHiggs_transverse_mass");
        // // addVartoStore("L2bJ1Fromtop_L1J1J2FromHiggs");
        // addVartoStore("L2bJ1Fromtop_L1J1J2FromHiggs_pt");
        // addVartoStore("L2bJ1Fromtop_L1J1J2FromHiggs_eta");
        // addVartoStore("L2bJ1Fromtop_L1J1J2FromHiggs_phi");
        // addVartoStore("L2bJ1Fromtop_L1J1J2FromHiggs_px");
        // addVartoStore("L2bJ1Fromtop_L1J1J2FromHiggs_py");
        // addVartoStore("L2bJ1Fromtop_L1J1J2FromHiggs_mass");
        // addVartoStore("L2bJ1Fromtop_L1J1J2FromHiggs_transverse_energy");
        // addVartoStore("L2bJ1Fromtop_L1J1J2FromHiggs_deltaeta");
        // addVartoStore("L2bJ1Fromtop_L1J1J2FromHiggs_deltaphi");
        // addVartoStore("L2bJ1Fromtop_L1J1J2FromHiggs_deltaR");
        // addVartoStore("L2bJ1Fromtop_L1J1J2FromHiggs_transverse_mass");
        // addVartoStore("L2bJ1Fromtop_L1J1J2FromHiggs_Relative_St");
    }
    addVartoStore("Tprime_transverse_mass_first_600");
    addVartoStore("Tprime_transverse_mass_second_600");
    addVartoStore("Tprime_transverse_mass_first_625");
    addVartoStore("Tprime_transverse_mass_second_625");
    addVartoStore("Tprime_transverse_mass_first_650");
    addVartoStore("Tprime_transverse_mass_second_650");
    addVartoStore("Tprime_transverse_mass_first_675");
    addVartoStore("Tprime_transverse_mass_second_675");
    addVartoStore("Tprime_transverse_mass_first_700");
    addVartoStore("Tprime_transverse_mass_second_700");
    addVartoStore("Tprime_transverse_mass_first_800");
    addVartoStore("Tprime_transverse_mass_second_800");
    addVartoStore("Tprime_transverse_mass_first_900");
    addVartoStore("Tprime_transverse_mass_second_900");
    addVartoStore("Tprime_transverse_mass_first_1000");
    addVartoStore("Tprime_transverse_mass_second_1000");
    addVartoStore("Tprime_transverse_mass_first_1100");
    addVartoStore("Tprime_transverse_mass_second_1100");
    addVartoStore("Tprime_transverse_mass_first_1200");
    addVartoStore("Tprime_transverse_mass_second_1200");

    if(!_isData)
    {
        // addVartoStore("GenParticles_mass");
        // addVartoStore("GenParticles_pdgId");
        // addVartoStore("MotherGenParticles");
        // addVartoStore("MotherGenParticlesPdgId");
        // addVartoStore("GenJets_pt");
        // addVartoStore("GenJets_eta");
        // addVartoStore("GenJets_phi");
        // addVartoStore("GenJets_mass");
        // addVartoStore("GenJets_hadronFlavour");
        // addVartoStore("GenJets_partonFlavour");
        // addVartoStore("GenJets_light");
        // addVartoStore("GenJets_light_pt");
        // addVartoStore("GenJets_light_eta");
        // addVartoStore("GenJets_light_phi");
        // addVartoStore("GenJets_light_mass");
        // addVartoStore("GenJets_light_hadronFlavour");
        // addVartoStore("GenJets_light_partonFlavour");

        // // reconstructed leptons
        // // addVartoStore("sel_leptongentopHiggs_loosett1l");
        // addVartoStore("LeptonFromtopHiggs_loosett1l_pt");
        // addVartoStore("LeptonFromtopHiggs_loosett1l_number");
        // addVartoStore("LeptonFromtopHiggs_loosett1l_leading_pt");
        // addVartoStore("LeptonFromtopHiggs_loosett1l_subleading_pt");
        // addVartoStore("LeptonFromtopHiggs_loosett1l_sum_two_muons_pt");
        // addVartoStore("LeptonFromtopHiggs_loosett1l_sum_all_muons_pt");  
        // addVartoStore("LeptonFromtopHiggs_loosett1l_eta");
        // addVartoStore("LeptonFromtopHiggs_loosett1l_phi");
        // addVartoStore("LeptonFromtopHiggs_loosett1l_mass");
        // addVartoStore("LeptonFromtopHiggs_loosett1l_transverse_energy");
        // addVartoStore("LeptonFromtopHiggs_loosett1l_charge");
        // addVartoStore("LeptonFromtopHiggs_loosett1l_charge_sum");
        // addVartoStore("LeptonFromtopHiggs_loosett1l_deltaeta");
        // addVartoStore("LeptonFromtopHiggs_loosett1l_deltaphi");
        // addVartoStore("LeptonFromtopHiggs_loosett1l_deltaR");
        // addVartoStore("LeptonFromtopHiggs_loosett1l_miniPFRelIso_all");
        // addVartoStore("LeptonFromtopHiggs_loosett1l_miniPFRelIso_chg");
        // addVartoStore("LeptonFromtopHiggs_loosett1l_pfRelIso03_all");
        // addVartoStore("LeptonFromtopHiggs_loosett1l_pfRelIso03_chg");
        // addVartoStore("LeptonFromtopHiggs_loosett1l_genPartIdx");
        // addVartoStore("LeptonFromtopHiggs_loosett1l_genPartFlav");
        // addVartoStore("LeptonFromtopHiggs_loosett1l_pdgId"); 
        // addVartoStore("p4_LeptonFromtopHiggs_loosett1l");
        // // addVartoStore("sel_leptongentoporHiggs_loosett1l");
        // addVartoStore("LeptonFromtop4vecs_loosett1l");
        // addVartoStore("LeptonFromtop_loosett1l_pt");
        // addVartoStore("LeptonFromtop_loosett1l_eta");
        // addVartoStore("LeptonFromtop_loosett1l_phi");
        // addVartoStore("LeptonFromtop_loosett1l_mass");
        // addVartoStore("LeptonFromtop_loosett1l_transverse_energy");
        // addVartoStore("LeptonFromtop_loosett1l_energy");
        // addVartoStore("LeptonFromtop_loosett1l_px");
        // addVartoStore("LeptonFromtop_loosett1l_py");
        // addVartoStore("LeptonFromtop_loosett1l_pz");
        // addVartoStore("LeptonFromHiggs4vecs_loosett1l");
        // addVartoStore("LeptonFromHiggs_loosett1l_pt");
        // addVartoStore("LeptonFromHiggs_loosett1l_eta");
        // addVartoStore("LeptonFromHiggs_loosett1l_phi");
        // addVartoStore("LeptonFromHiggs_loosett1l_mass");
        // addVartoStore("LeptonFromHiggs_loosett1l_transverse_energy");
        // addVartoStore("LeptonFromHiggs_loosett1l_energy");
        // addVartoStore("LeptonFromHiggs_loosett1l_px");
        // addVartoStore("LeptonFromHiggs_loosett1l_py");
        // addVartoStore("LeptonFromHiggs_loosett1l_pz");
        // addVartoStore("LeptonFromtopHiggs_loosett1l_ordered_pt");
        // addVartoStore("LeptonFromtopHiggs_loosett1l_ordered_px");
        // addVartoStore("LeptonFromtopHiggs_loosett1l_ordered_py");
        // addVartoStore("LeptonFromtopHiggs_loosett1l_ordered_pz");
        // addVartoStore("LeptonFromtopHiggs_loosett1l_ordered_energy");
        // addVartoStore("LeptonFromtopHiggs_loosett1l_ordered_mass");
        // addVartoStore("LeptonFromtop_LeptonFromHiggs_loosett1l");
        // addVartoStore("LeptonFromtop_LeptonFromHiggs_loosett1l_pt");
        // addVartoStore("LeptonFromtop_LeptonFromHiggs_loosett1l_eta");
        // addVartoStore("LeptonFromtop_LeptonFromHiggs_loosett1l_phi");
        // addVartoStore("LeptonFromtop_LeptonFromHiggs_loosett1l_mass");
        // addVartoStore("LeptonFromtop_LeptonFromHiggs_loosett1l_transverse_energy");
        // addVartoStore("LeptonFromtop_LeptonFromHiggs_loosett1l_deltaeta");
        // addVartoStore("LeptonFromtop_LeptonFromHiggs_loosett1l_deltaphi");
        // addVartoStore("LeptonFromtop_LeptonFromHiggs_loosett1l_deltaR");
        // // addVartoStore("sel_leptongenW_loosett1l");
        // addVartoStore("LeptonFromW_loosett1l_pt");
        // addVartoStore("LeptonFromW_loosett1l_number");
        // addVartoStore("LeptonFromW_loosett1l_leading_pt");
        // addVartoStore("LeptonFromW_loosett1l_subleading_pt");
        // addVartoStore("LeptonFromW_loosett1l_sum_two_muons_pt");
        // addVartoStore("LeptonFromW_loosett1l_sum_all_muons_pt");  
        // addVartoStore("LeptonFromW_loosett1l_eta");
        // addVartoStore("LeptonFromW_loosett1l_phi");
        // addVartoStore("LeptonFromW_loosett1l_mass");
        // addVartoStore("LeptonFromW_loosett1l_charge");
        // addVartoStore("LeptonFromW_loosett1l_charge_sum");
        // addVartoStore("LeptonFromW_loosett1l_deltaeta");
        // addVartoStore("LeptonFromW_loosett1l_deltaphi");
        // addVartoStore("LeptonFromW_loosett1l_deltaR");
        // addVartoStore("LeptonFromW_loosett1l_genPartIdx");
        // addVartoStore("LeptonFromW_loosett1l_genPartFlav");
        // addVartoStore("LeptonFromW_loosett1l_pdgId"); 
        // // addVartoStore("p4_LeptonFromW_loosett1l");
        // // addVartoStore("sel_muongenW_loosett1l");
        // addVartoStore("MuonFromW_loosett1l_pt");
        // addVartoStore("MuonFromW_loosett1l_charge");
        // addVartoStore("MuonFromW_loosett1l_number");
        // // addVartoStore("sel_electrongenW_loosett1l");
        // addVartoStore("ElectronFromW_loosett1l_pt");
        // addVartoStore("ElectronFromW_loosett1l_charge");
        // addVartoStore("ElectronFromW_loosett1l_number");
        // addVartoStore("sel_leptongenMother_loosett1l");
        // addVartoStore("MotherfromLepton_loosett1l_pdgId");
        // addVartoStore("MotherfromLepton1_loosett1l_pdgId");
        // addVartoStore("MotherfromLepton2_loosett1l_pdgId");
        // addVartoStore("MotherfromLepton12_loosett1l_pdgId");
        if(JEC == 0)
        {
            addVartoStore("lepton_different_charge_loosett1l");
            addVartoStore("lepton_non_prompt_loosett1l");
        }
        // // addVartoStore("sel_leptongentopHiggs");
        // addVartoStore("LeptonFromtopHiggs_pt");
        // addVartoStore("LeptonFromtopHiggs_number");
        // addVartoStore("LeptonFromtopHiggs_leading_pt");
        // addVartoStore("LeptonFromtopHiggs_subleading_pt");
        // addVartoStore("LeptonFromtopHiggs_sum_two_muons_pt");
        // addVartoStore("LeptonFromtopHiggs_sum_all_muons_pt");  
        // addVartoStore("LeptonFromtopHiggs_eta");
        // addVartoStore("LeptonFromtopHiggs_phi");
        // addVartoStore("LeptonFromtopHiggs_mass");
        // addVartoStore("LeptonFromtopHiggs_transverse_energy");
        // addVartoStore("LeptonFromtopHiggs_charge");
        // addVartoStore("LeptonFromtopHiggs_charge_sum");
        // addVartoStore("LeptonFromtopHiggs_deltaeta");
        // addVartoStore("LeptonFromtopHiggs_deltaphi");
        // addVartoStore("LeptonFromtopHiggs_deltaR");
        // addVartoStore("LeptonFromtopHiggs_miniPFRelIso_all");
        // addVartoStore("LeptonFromtopHiggs_miniPFRelIso_chg");
        // addVartoStore("LeptonFromtopHiggs_pfRelIso03_all");
        // addVartoStore("LeptonFromtopHiggs_pfRelIso03_chg");
        // addVartoStore("LeptonFromtopHiggs_genPartIdx");
        // addVartoStore("LeptonFromtopHiggs_genPartFlav");
        // addVartoStore("LeptonFromtopHiggs_pdgId"); 
        // addVartoStore("p4_LeptonFromtopHiggs");
        // // addVartoStore("sel_leptongentoporHiggs");
        // addVartoStore("LeptonFromtop4vecs");
        // addVartoStore("LeptonFromtop_pt");
        // addVartoStore("LeptonFromtop_eta");
        // addVartoStore("LeptonFromtop_phi");
        // addVartoStore("LeptonFromtop_mass");
        // addVartoStore("LeptonFromtop_transverse_energy");
        // addVartoStore("LeptonFromtop_energy");
        // addVartoStore("LeptonFromtop_px");
        // addVartoStore("LeptonFromtop_py");
        // addVartoStore("LeptonFromtop_pz");
        // addVartoStore("LeptonFromHiggs4vecs");
        // addVartoStore("LeptonFromHiggs_pt");
        // addVartoStore("LeptonFromHiggs_eta");
        // addVartoStore("LeptonFromHiggs_phi");
        // addVartoStore("LeptonFromHiggs_mass");
        // addVartoStore("LeptonFromHiggs_transverse_energy");
        // addVartoStore("LeptonFromHiggs_energy");
        // addVartoStore("LeptonFromHiggs_px");
        // addVartoStore("LeptonFromHiggs_py");
        // addVartoStore("LeptonFromHiggs_pz");
        // addVartoStore("LeptonFromtopHiggs_ordered_pt");
        // addVartoStore("LeptonFromtopHiggs_ordered_px");
        // addVartoStore("LeptonFromtopHiggs_ordered_py");
        // addVartoStore("LeptonFromtopHiggs_ordered_pz");
        // addVartoStore("LeptonFromtopHiggs_ordered_energy");
        // addVartoStore("LeptonFromtopHiggs_ordered_mass");
        // addVartoStore("LeptonFromtop_LeptonFromHiggs");
        // addVartoStore("LeptonFromtop_LeptonFromHiggs_pt");
        // addVartoStore("LeptonFromtop_LeptonFromHiggs_eta");
        // addVartoStore("LeptonFromtop_LeptonFromHiggs_phi");
        // addVartoStore("LeptonFromtop_LeptonFromHiggs_mass");
        // addVartoStore("LeptonFromtop_LeptonFromHiggs_transverse_energy");
        // addVartoStore("LeptonFromtop_LeptonFromHiggs_deltaeta");
        // addVartoStore("LeptonFromtop_LeptonFromHiggs_deltaphi");
        // addVartoStore("LeptonFromtop_LeptonFromHiggs_deltaR");
        // // addVartoStore("sel_leptongenW");
        // addVartoStore("LeptonFromW_pt");
        // addVartoStore("LeptonFromW_number");
        // addVartoStore("LeptonFromW_leading_pt");
        // addVartoStore("LeptonFromW_subleading_pt");
        // addVartoStore("LeptonFromW_sum_two_muons_pt");
        // addVartoStore("LeptonFromW_sum_all_muons_pt");  
        // addVartoStore("LeptonFromW_eta");
        // addVartoStore("LeptonFromW_phi");
        // addVartoStore("LeptonFromW_mass");
        // addVartoStore("LeptonFromW_charge");
        // addVartoStore("LeptonFromW_charge_sum");
        // addVartoStore("LeptonFromW_deltaeta");
        // addVartoStore("LeptonFromW_deltaphi");
        // addVartoStore("LeptonFromW_deltaR");
        // addVartoStore("LeptonFromW_genPartIdx");
        // addVartoStore("LeptonFromW_genPartFlav");
        // addVartoStore("LeptonFromW_pdgId"); 
        // // addVartoStore("p4_LeptonFromW");
        // // addVartoStore("sel_muongenW");
        // addVartoStore("MuonFromW_pt");
        // addVartoStore("MuonFromW_charge");
        // addVartoStore("MuonFromW_number");
        // // addVartoStore("sel_electrongenW");
        // addVartoStore("ElectronFromW_pt");
        // addVartoStore("ElectronFromW_charge");
        // addVartoStore("ElectronFromW_number");
        // addVartoStore("sel_leptongenMother");
        // addVartoStore("MotherfromLepton_pdgId");
        // addVartoStore("MotherfromLepton1_pdgId");
        // addVartoStore("MotherfromLepton2_pdgId");
        // addVartoStore("MotherfromLepton12_pdgId");
        addVartoStore("lepton_different_charge");
        addVartoStore("lepton_non_prompt");
        // addVartoStore("GenLeptons");
        // addVartoStore("MotherGenLeptons");
        // addVartoStore("MotherGenLeptonsPdgId");
        // addVartoStore("IsFromW");
        // addVartoStore("GenLeptonsPdgId");
        // addVartoStore("GenLeptons_pt");
        // addVartoStore("GenLeptons_eta");
        // addVartoStore("GenLeptons_phi");
        // addVartoStore("GenMuons");
        // addVartoStore("GenMuonsPdgId");
        // addVartoStore("MotherGenMuons");
        // addVartoStore("MotherGenMuonsPdgId");
        // addVartoStore("IsMuonFromW");
        // addVartoStore("GenElectrons");
        // addVartoStore("GenElectronsPdgId");
        // addVartoStore("MotherGenElectrons");
        // addVartoStore("MotherGenElectronsPdgId");
        // addVartoStore("IsElectronFromW");
        // addVartoStore("GenW");
        // addVartoStore("MotherGenW");
        // addVartoStore("MotherGenWPdgId");
        // addVartoStore("IsFromHiggs");
        // addVartoStore("IsFromtop");

        // // reconstructed neutrinos
        // addVartoStore("GenNeutrinos");
        // addVartoStore("GenNeutrinosPdgId");
        // addVartoStore("GenNeutrinos_pt");
        // addVartoStore("GenNeutrinos_eta");
        // addVartoStore("GenNeutrinos_phi");
        // addVartoStore("MotherGenNeutrinos");
        // addVartoStore("MotherGenNeutrinosPdgId");
        // addVartoStore("IsNeutrinoFromW");
        // addVartoStore("GenNeutrinoFromWPdgId");
        // addVartoStore("GenNeutrinoFromW_pt");
        // addVartoStore("GenNeutrinoFromW_number");
        // addVartoStore("GenNeutrinoFromW_eta");
        // addVartoStore("GenNeutrinoFromW_phi");
        // addVartoStore("MotherNeutrinoFromW");
        // addVartoStore("MotherNeutrinoFromWPdgId");
        // addVartoStore("MotherNeutrinoFromWMass");
        // // addVartoStore("sel_NeutrinoFromWgenHiggs");
        // // addVartoStore("sel_NeutrinoFromWgentop");
        // addVartoStore("GenNeutrinoFromtop_pt");
        // addVartoStore("GenNeutrinoFromtop_number");
        // addVartoStore("GenNeutrinoFromtop_eta");
        // addVartoStore("GenNeutrinoFromtop_phi");
        // addVartoStore("GenNeutrinoFromtop_px");
        // addVartoStore("GenNeutrinoFromtop_py");
        // addVartoStore("p4_GenNeutrinoFromtop");
        // addVartoStore("p4_GenNeutrinoFromtop_first");
        // addVartoStore("GenNeutrinoFromtop_first_pt");
        // addVartoStore("GenNeutrinoFromtop_first_eta");
        // addVartoStore("GenNeutrinoFromtop_first_phi");
        // addVartoStore("GenNeutrinoFromHiggs_pt");
        // addVartoStore("GenNeutrinoFromHiggs_number");
        // addVartoStore("GenNeutrinoFromHiggs_eta");
        // addVartoStore("GenNeutrinoFromHiggs_phi");
        // addVartoStore("GenNeutrinoFromHiggs_px");
        // addVartoStore("GenNeutrinoFromHiggs_py");
        // addVartoStore("p4_GenNeutrinoFromHiggs");
        // addVartoStore("p4_GenNeutrinoFromHiggs_first");
        // addVartoStore("GenNeutrinoFromHiggs_first_pt");
        // addVartoStore("GenNeutrinoFromHiggs_first_eta");
        // addVartoStore("GenNeutrinoFromHiggs_first_phi");
        // addVartoStore("MotherNeutrinoFromHiggsPdgid");
        // addVartoStore("MotherNeutrinoFromHiggsMass");
        // addVartoStore("NeutrinoFromtop_NeutrinoFromHiggs");
        // addVartoStore("NeutrinoFromtop_NeutrinoFromHiggs_pt");
        // addVartoStore("NeutrinoFromtop_NeutrinoFromHiggs_eta");
        // addVartoStore("NeutrinoFromtop_NeutrinoFromHiggs_phi");
        // addVartoStore("NeutrinoFromtop_NeutrinoFromHiggs_mass");
        // addVartoStore("NeutrinoFromtop_NeutrinoFromHiggs_transverse_energy");
        // addVartoStore("NeutrinoFromtop_NeutrinoFromHiggs_deltaeta");
        // addVartoStore("NeutrinoFromtop_NeutrinoFromHiggs_deltaphi");
        // addVartoStore("NeutrinoFromtop_NeutrinoFromHiggs_deltaR");
        // addVartoStore("NeutrinoFromtop_LeptonFromtop");
        // addVartoStore("NeutrinoFromtop_LeptonFromtop_pt");
        // addVartoStore("NeutrinoFromtop_LeptonFromtop_eta");
        // addVartoStore("NeutrinoFromtop_LeptonFromtop_phi");
        // addVartoStore("NeutrinoFromtop_LeptonFromtop_mass");
        // addVartoStore("NeutrinoFromtop_LeptonFromtop_transverse_energy");
        // addVartoStore("NeutrinoFromtop_LeptonFromtop_deltaeta");
        // addVartoStore("NeutrinoFromtop_LeptonFromtop_deltaphi");
        // addVartoStore("NeutrinoFromtop_LeptonFromtop_deltaR");
        // addVartoStore("NeutrinoFromHiggs_LeptonFromHiggs");
        // addVartoStore("NeutrinoFromHiggs_LeptonFromHiggs_pt");
        // addVartoStore("NeutrinoFromHiggs_LeptonFromHiggs_eta");
        // addVartoStore("NeutrinoFromHiggs_LeptonFromHiggs_phi");
        // addVartoStore("NeutrinoFromHiggs_LeptonFromHiggs_mass");
        // addVartoStore("NeutrinoFromHiggs_LeptonFromHiggs_transverse_energy");
        // addVartoStore("NeutrinoFromHiggs_LeptonFromHiggs_deltaeta");
        // addVartoStore("NeutrinoFromHiggs_LeptonFromHiggs_deltaphi");
        // addVartoStore("NeutrinoFromHiggs_LeptonFromHiggs_deltaR");

        // // reconstructed jets
        // addVartoStore("JFromtopHiggs_pt");
        // addVartoStore("sel_jetgentopHiggs");
        // addVartoStore("JetFromtopHiggs_pt");
        // addVartoStore("JetFromtopHiggs_number");
        // addVartoStore("JetFromtopHiggs_leading_pt");
        // addVartoStore("JetFromtopHiggs_subleading_pt");
        // addVartoStore("JetFromtopHiggs_subsubleading_pt");
        // addVartoStore("JetFromtopHiggs_Ht");
        // addVartoStore("JetFromtopHiggs_eta");
        // addVartoStore("JetFromtopHiggs_phi");
        // addVartoStore("JetFromtopHiggs_mass");
        // addVartoStore("JetFromtopHiggs_transverse_energy");
        // addVartoStore("JetFromtopHiggs_btagDeepB");
        // addVartoStore("JetFromtopHiggs_btagDeepFlavB");
        // addVartoStore("JetFromtopHiggs_genJetIdx");
        // addVartoStore("JetFromtopHiggs_partonFlavour");
        // addVartoStore("JetFromtopHiggs_hadronFlavour");
        // addVartoStore("p4_JetFromtopHiggs");
        // addVartoStore("Rp4_JetFromtopHiggs");
        // addVartoStore("JetFromtopHiggs_energy");
        // addVartoStore("JetFromtopHiggs_px");
        // addVartoStore("JetFromtopHiggs_py");
        // addVartoStore("JetFromtopHiggs_pz");
        // addVartoStore("sel_bjetgentopHiggs");
        // addVartoStore("bJetFromtopHiggs_pt");
        // addVartoStore("bJetFromtopHiggs_number");
        // addVartoStore("bJetFromtopHiggs_leading_pt");
        // addVartoStore("bJetFromtopHiggs_subleading_pt");
        // addVartoStore("bJetFromtopHiggs_subsubleading_pt");
        // addVartoStore("bJetFromtopHiggs_Ht");
        // addVartoStore("bJetFromtopHiggs_eta");
        // addVartoStore("bJetFromtopHiggs_phi");
        // addVartoStore("bJetFromtopHiggs_mass");
        // addVartoStore("bJetFromtopHiggs_transverse_energy");
        // addVartoStore("bJetFromtopHiggs_btagDeepB");
        // addVartoStore("bJetFromtopHiggs_btagDeepFlavB");
        // addVartoStore("bJetFromtopHiggs_genJetIdx");
        // addVartoStore("bJetFromtopHiggs_partonFlavour");
        // addVartoStore("bJetFromtopHiggs_hadronFlavour");
        // addVartoStore("p4_bJetFromtopHiggs");
        // addVartoStore("Rp4_bJetFromtopHiggs");
        // addVartoStore("bJetFromtopHiggs_energy");
        // addVartoStore("bJetFromtopHiggs_px");
        // addVartoStore("bJetFromtopHiggs_py");
        // addVartoStore("bJetFromtopHiggs_pz");
        // // addVartoStore("JFromHiggs_pt");
        // // addVartoStore("sel_jetgenHiggs");
        // addVartoStore("JetFromHiggs_pt");
        // addVartoStore("JetFromHiggs_number");
        // addVartoStore("JetFromHiggs_leading_pt");
        // addVartoStore("JetFromHiggs_subleading_pt");
        // addVartoStore("JetFromHiggs_subsubleading_pt");
        // addVartoStore("JetFromHiggs_Ht");
        // addVartoStore("JetFromHiggs_eta");
        // addVartoStore("JetFromHiggs_phi");
        // addVartoStore("JetFromHiggs_mass");
        // addVartoStore("JetFromHiggs_transverse_energy");
        // addVartoStore("JetFromHiggs_btagDeepB");
        // addVartoStore("JetFromHiggs_btagDeepFlavB");
        // addVartoStore("JetFromHiggs_genJetIdx");
        // addVartoStore("JetFromHiggs_partonFlavour");
        // addVartoStore("JetFromHiggs_hadronFlavour");
        // // addVartoStore("p4_JetFromHiggs");
        // // addVartoStore("Rp4_JetFromHiggs");
        // addVartoStore("JetFromHiggs_energy");
        // addVartoStore("JetFromHiggs_px");
        // addVartoStore("JetFromHiggs_py");
        // addVartoStore("JetFromHiggs_pz");
        // // addVartoStore("JFromtop_pt");
        // // addVartoStore("sel_bjetgentop");
        // addVartoStore("bJetFromtop_pt");
        // addVartoStore("bJetFromtop_number");
        // addVartoStore("bJetFromtop_leading_pt");
        // addVartoStore("bJetFromtop_subleading_pt");
        // addVartoStore("bJetFromtop_subsubleading_pt");
        // addVartoStore("bJetFromtop_Ht");
        // addVartoStore("bJetFromtop_eta");
        // addVartoStore("bJetFromtop_phi");
        // addVartoStore("bJetFromtop_mass");
        // addVartoStore("bJetFromtop_transverse_energy");
        // addVartoStore("bJetFromtop_btagDeepB");
        // addVartoStore("bJetFromtop_btagDeepFlavB");
        // addVartoStore("bJetFromtop_genJetIdx");
        // addVartoStore("bJetFromtop_partonFlavour");
        // addVartoStore("bJetFromtop_hadronFlavour");
        // // addVartoStore("p4_bJetFromtop");
        // // addVartoStore("Rp4_bJetFromtop");
        // addVartoStore("bJetFromtop_energy");
        // addVartoStore("bJetFromtop_px");
        // addVartoStore("bJetFromtop_py");
        // addVartoStore("bJetFromtop_pz");
        // // addVartoStore("p4_bJetFromtop_first");
        // addVartoStore("bJetFromtop_first_pt");
        // addVartoStore("bJetFromtop_first_eta");
        // addVartoStore("bJetFromtop_first_phi");
        // addVartoStore("bJetFromtop_first_mass");
        // addVartoStore("bJetFromtop_first_energy");
        // addVartoStore("bJetFromtop_first_px");
        // addVartoStore("bJetFromtop_first_py");
        // addVartoStore("bJetFromtop_first_pz");
        // // addVartoStore("Rp4_bJetFromtop_first");
        // addVartoStore("Max_deltaR_top");
        // // addVartoStore("Min_deltaR_top");
        // addVartoStore("bJetFromtop1");
        // addVartoStore("bJetFromtop1_pt");
        // addVartoStore("bJetFromtop1_eta");
        // addVartoStore("bJetFromtop1_phi");
        // addVartoStore("bJetFromtop1_mass");
        // addVartoStore("bJetFromtop1_transverse_energy");   
        // // addVartoStore("Min_deltaR_Higgs");
        // addVartoStore("JetFromHiggs1");
        // addVartoStore("JetFromHiggs1_pt");
        // addVartoStore("JetFromHiggs1_eta");
        // addVartoStore("JetFromHiggs1_phi");
        // addVartoStore("JetFromHiggs1_mass");
        // addVartoStore("JetFromHiggs1_transverse_energy");
        // addVartoStore("JetFromHiggs2");
        // addVartoStore("JetFromHiggs2_pt");
        // addVartoStore("JetFromHiggs2_eta");
        // addVartoStore("JetFromHiggs2_phi");
        // addVartoStore("JetFromHiggs2_mass");
        // addVartoStore("JetFromHiggs2_transverse_energy");

        // // combination of reconstructed particles
        // // addVartoStore("deltaR_Selected_leptonfromtop_Selected_clean_jet");
        // // addVartoStore("deltaR_Selected_leptonfromtop_Selected_clean_bjet");
        // // addVartoStore("deltaR_Selected_leptonfromHiggs_Selected_clean_jet");
        // // addVartoStore("deltaR_Selected_leptonfromHiggs_Selected_clean_bjet");
        // // addVartoStore("Pt_leptonfromtop_over_Pt_clean_jet");
        // // addVartoStore("Pt_leptonfromtop_over_Pt_clean_bjet");
        // // addVartoStore("Pt_leptonfromHiggs_over_Pt_clean_jet");
        // // addVartoStore("Pt_leptonfromHiggs_over_Pt_clean_bjet");
        // // addVartoStore("Min_deltaR_Selected_leptonfromtop_Selected_clean_jet");
        // // addVartoStore("Min_deltaR_Selected_leptonfromtop_Selected_clean_bjet");
        // // addVartoStore("Min_deltaR_Selected_leptonfromHiggs_Selected_clean_jet");
        // // addVartoStore("Min_deltaR_Selected_leptonfromHiggs_Selected_clean_bjet");
        // // addVartoStore("Min_Pt_leptonfromtop_over_Pt_clean_jet");
        // // addVartoStore("Min_Pt_leptonfromtop_over_Pt_clean_bjet");
        // // addVartoStore("Min_Pt_leptonfromHiggs_over_Pt_clean_jet");
        // // addVartoStore("Min_Pt_leptonfromHiggs_over_Pt_clean_bjet");
        // addVartoStore("LeptonFromtop_bJetFromtop1");
        // addVartoStore("LeptonFromtop_bJetFromtop1_pt");
        // addVartoStore("LeptonFromtop_bJetFromtop1_eta");
        // addVartoStore("LeptonFromtop_bJetFromtop1_phi");
        // addVartoStore("LeptonFromtop_bJetFromtop1_mass");
        // addVartoStore("LeptonFromtop_bJetFromtop1_transverse_energy");
        // addVartoStore("LeptonFromtop_bJetFromtop1_deltaeta");
        // addVartoStore("LeptonFromtop_bJetFromtop1_deltaphi");
        // addVartoStore("LeptonFromtop_bJetFromtop1_deltaR");
        // addVartoStore("LeptonFromtop_bJetFromtop1_transverse_mass");
        // // addVartoStore("WFromtop_transverse_mass");
        // addVartoStore("LeptonFromtop_JetFromHiggs1");
        // addVartoStore("LeptonFromtop_JetFromHiggs1_pt");
        // addVartoStore("LeptonFromtop_JetFromHiggs1_eta");
        // addVartoStore("LeptonFromtop_JetFromHiggs1_phi");
        // addVartoStore("LeptonFromtop_JetFromHiggs1_mass");
        // addVartoStore("LeptonFromtop_JetFromHiggs1_transverse_energy");
        // addVartoStore("LeptonFromtop_JetFromHiggs1_deltaeta");
        // addVartoStore("LeptonFromtop_JetFromHiggs1_deltaphi");
        // addVartoStore("LeptonFromtop_JetFromHiggs1_deltaR");
        // addVartoStore("LeptonFromtop_JetFromHiggs1_transverse_mass");
        // addVartoStore("LeptonFromtop_JetFromHiggs2");
        // addVartoStore("LeptonFromtop_JetFromHiggs2_pt");
        // addVartoStore("LeptonFromtop_JetFromHiggs2_eta");
        // addVartoStore("LeptonFromtop_JetFromHiggs2_phi");
        // addVartoStore("LeptonFromtop_JetFromHiggs2_mass");
        // addVartoStore("LeptonFromtop_JetFromHiggs2_transverse_energy");
        // addVartoStore("LeptonFromtop_JetFromHiggs2_deltaeta");
        // addVartoStore("LeptonFromtop_JetFromHiggs2_deltaphi");
        // addVartoStore("LeptonFromtop_JetFromHiggs2_deltaR");
        // addVartoStore("LeptonFromtop_JetFromHiggs2_transverse_mass");
        // addVartoStore("LeptonFromHiggs_bJetFromtop1");
        // addVartoStore("LeptonFromHiggs_bJetFromtop1_pt");
        // addVartoStore("LeptonFromHiggs_bJetFromtop1_eta");
        // addVartoStore("LeptonFromHiggs_bJetFromtop1_phi");
        // addVartoStore("LeptonFromHiggs_bJetFromtop1_mass");
        // addVartoStore("LeptonFromHiggs_bJetFromtop1_transverse_energy");
        // addVartoStore("LeptonFromHiggs_bJetFromtop1_deltaeta");
        // addVartoStore("LeptonFromHiggs_bJetFromtop1_deltaphi");
        // addVartoStore("LeptonFromHiggs_bJetFromtop1_deltaR");
        // addVartoStore("LeptonFromHiggs_bJetFromtop1_transverse_mass");
        // addVartoStore("LeptonFromHiggs_JetFromHiggs1");
        // addVartoStore("LeptonFromHiggs_JetFromHiggs1_pt");
        // addVartoStore("LeptonFromHiggs_JetFromHiggs1_eta");
        // addVartoStore("LeptonFromHiggs_JetFromHiggs1_phi");
        // addVartoStore("LeptonFromHiggs_JetFromHiggs1_mass");
        // addVartoStore("LeptonFromHiggs_JetFromHiggs1_transverse_energy");
        // addVartoStore("LeptonFromHiggs_JetFromHiggs1_deltaeta");
        // addVartoStore("LeptonFromHiggs_JetFromHiggs1_deltaphi");
        // addVartoStore("LeptonFromHiggs_JetFromHiggs1_deltaR");
        // addVartoStore("LeptonFromHiggs_JetFromHiggs1_transverse_mass");
        // addVartoStore("LeptonFromHiggs_JetFromHiggs2");
        // addVartoStore("LeptonFromHiggs_JetFromHiggs2_pt");
        // addVartoStore("LeptonFromHiggs_JetFromHiggs2_eta");
        // addVartoStore("LeptonFromHiggs_JetFromHiggs2_phi");
        // addVartoStore("LeptonFromHiggs_JetFromHiggs2_mass");
        // addVartoStore("LeptonFromHiggs_JetFromHiggs2_transverse_energy");
        // addVartoStore("LeptonFromHiggs_JetFromHiggs2_deltaeta");
        // addVartoStore("LeptonFromHiggs_JetFromHiggs2_deltaphi");
        // addVartoStore("LeptonFromHiggs_JetFromHiggs2_deltaR"); 
        // addVartoStore("LeptonFromHiggs_JetFromHiggs2_transverse_mass");
        // addVartoStore("JetFromHiggs1_JetFromHiggs2");
        // addVartoStore("JetFromHiggs1_JetFromHiggs2_pt");
        // addVartoStore("JetFromHiggs1_JetFromHiggs2_eta");
        // addVartoStore("JetFromHiggs1_JetFromHiggs2_phi");
        // addVartoStore("JetFromHiggs1_JetFromHiggs2_mass");
        // addVartoStore("JetFromHiggs1_JetFromHiggs2_transverse_energy");
        // addVartoStore("JetFromHiggs1_JetFromHiggs2_deltaeta");
        // addVartoStore("JetFromHiggs1_JetFromHiggs2_deltaphi");
        // addVartoStore("JetFromHiggs1_JetFromHiggs2_deltaR");
        // addVartoStore("JetFromHiggs1_JetFromHiggs2_transverse_mass");
        // addVartoStore("LeptonFromHiggs_JetFromHiggs1_JetFromHiggs2");
        // addVartoStore("LeptonFromHiggs_JetFromHiggs1_JetFromHiggs2_pt");
        // addVartoStore("LeptonFromHiggs_JetFromHiggs1_JetFromHiggs2_eta");
        // addVartoStore("LeptonFromHiggs_JetFromHiggs1_JetFromHiggs2_phi");
        // addVartoStore("LeptonFromHiggs_JetFromHiggs1_JetFromHiggs2_mass");
        // addVartoStore("LeptonFromHiggs_JetFromHiggs1_JetFromHiggs2_transverse_energy");
        // addVartoStore("LeptonFromHiggs_JetFromHiggs1_JetFromHiggs2_deltaeta");
        // addVartoStore("LeptonFromHiggs_JetFromHiggs1_JetFromHiggs2_deltaphi");
        // addVartoStore("LeptonFromHiggs_JetFromHiggs1_JetFromHiggs2_deltaR");
        // addVartoStore("LeptonFromHiggs_JetFromHiggs1_JetFromHiggs2_transverse_mass");
        // addVartoStore("LbJFromtop1_LJJFromHiggs12");
        // addVartoStore("LbJFromtop1_LJJFromHiggs12_pt");
        // addVartoStore("LbJFromtop1_LJJFromHiggs12_eta");
        // addVartoStore("LbJFromtop1_LJJFromHiggs12_phi");
        // addVartoStore("LbJFromtop1_LJJFromHiggs12_mass");
        // addVartoStore("LbJFromtop1_LJJFromHiggs12_transverse_energy");
        // addVartoStore("LbJFromtop1_LJJFromHiggs12_deltaeta");
        // addVartoStore("LbJFromtop1_LJJFromHiggs12_deltaphi");
        // addVartoStore("LbJFromtop1_LJJFromHiggs12_deltaR");
        // addVartoStore("LbJFromtop1_LJJFromHiggs12_transverse_mass");
        // addVartoStore("LeptonFromtop_NeutrinoFromtop");
        // addVartoStore("LeptonFromtop_NeutrinoFromtop_pt");
        // addVartoStore("LeptonFromtop_NeutrinoFromtop_eta"); 
        // addVartoStore("LeptonFromtop_NeutrinoFromtop_phi"); 
        // addVartoStore("LeptonFromtop_NeutrinoFromtop_mass"); 
        // addVartoStore("LeptonFromtop_NeutrinoFromtop_transverse_energy");
        // addVartoStore("LeptonFromtop_NeutrinoFromtop_deltaeta");
        // addVartoStore("LeptonFromtop_NeutrinoFromtop_deltaphi");
        // addVartoStore("LeptonFromtop_NeutrinoFromtop_deltaR");
        // addVartoStore("LeptonFromtop_NeutrinoFromtop_transverse_mass");
        // addVartoStore("LeptonFromtop_NeutrinoFromtop_bJetFromtop1");
        // addVartoStore("LeptonFromtop_NeutrinoFromtop_bJetFromtop1_pt");
        // addVartoStore("LeptonFromtop_NeutrinoFromtop_bJetFromtop1_eta"); 
        // addVartoStore("LeptonFromtop_NeutrinoFromtop_bJetFromtop1_phi"); 
        // addVartoStore("LeptonFromtop_NeutrinoFromtop_bJetFromtop1_mass"); 
        // addVartoStore("LeptonFromtop_NeutrinoFromtop_bJetFromtop1_transverse_energy");
        // addVartoStore("LeptonFromtop_NeutrinoFromtop_bJetFromtop1_deltaeta");
        // addVartoStore("LeptonFromtop_NeutrinoFromtop_bJetFromtop1_deltaphi");
        // addVartoStore("LeptonFromtop_NeutrinoFromtop_bJetFromtop1_deltaR");
        // addVartoStore("LeptonFromtop_NeutrinoFromtop_bJetFromtop1_transverse_mass");
        // addVartoStore("LeptonFromHiggs_NeutrinoFromHiggs");
        // addVartoStore("LeptonFromHiggs_NeutrinoFromHiggs_pt");
        // addVartoStore("LeptonFromHiggs_NeutrinoFromHiggs_eta"); 
        // addVartoStore("LeptonFromHiggs_NeutrinoFromHiggs_phi"); 
        // addVartoStore("LeptonFromHiggs_NeutrinoFromHiggs_mass");
        // addVartoStore("LeptonFromHiggs_NeutrinoFromHiggs_transverse_energy");
        // addVartoStore("LeptonFromHiggs_NeutrinoFromHiggs_deltaeta");
        // addVartoStore("LeptonFromHiggs_NeutrinoFromHiggs_deltaphi");
        // addVartoStore("LeptonFromHiggs_NeutrinoFromHiggs_deltaR");
        // addVartoStore("LeptonFromHiggs_NeutrinoFromHiggs_transverse_mass");
        // addVartoStore("LeptonFromHiggs_NeutrinoFromHiggs_JetFromHiggs1_JetFromHiggs2");
        // addVartoStore("LeptonFromHiggs_NeutrinoFromHiggs_JetFromHiggs1_JetFromHiggs2_pt");
        // addVartoStore("LeptonFromHiggs_NeutrinoFromHiggs_JetFromHiggs1_JetFromHiggs2_eta"); 
        // addVartoStore("LeptonFromHiggs_NeutrinoFromHiggs_JetFromHiggs1_JetFromHiggs2_phi"); 
        // addVartoStore("LeptonFromHiggs_NeutrinoFromHiggs_JetFromHiggs1_JetFromHiggs2_mass"); 
        // addVartoStore("LeptonFromHiggs_NeutrinoFromHiggs_JetFromHiggs1_JetFromHiggs2_transverse_energy");
        // addVartoStore("LeptonFromHiggs_NeutrinoFromHiggs_JetFromHiggs1_JetFromHiggs2_deltaeta");
        // addVartoStore("LeptonFromHiggs_NeutrinoFromHiggs_JetFromHiggs1_JetFromHiggs2_deltaphi");
        // addVartoStore("LeptonFromHiggs_NeutrinoFromHiggs_JetFromHiggs1_JetFromHiggs2_deltaR");
        // addVartoStore("LeptonFromHiggs_NeutrinoFromHiggs_JetFromHiggs1_JetFromHiggs1_transverse_mass");
        // addVartoStore("LNbJFromtop1_LNJJFromHiggs12");
        // addVartoStore("LNbJFromtop1_LNJJFromHiggs12_pt");
        // addVartoStore("LNbJFromtop1_LNJJFromHiggs12_eta");
        // addVartoStore("LNbJFromtop1_LNJJFromHiggs12_phi");
        // addVartoStore("LNbJFromtop1_LNJJFromHiggs12_mass");
        // addVartoStore("LNbJFromtop1_LNJJFromHiggs12_px");
        // addVartoStore("LNbJFromtop1_LNJJFromHiggs12_py");
        // addVartoStore("LNbJFromtop1_LNJJFromHiggs12_transverse_energy");
        // addVartoStore("LNbJFromtop1_LNJJFromHiggs12_deltaeta");
        // addVartoStore("LNbJFromtop1_LNJJFromHiggs12_deltaphi");
        // addVartoStore("LNbJFromtop1_LNJJFromHiggs12_deltaR");
        // addVartoStore("LNbJFromtop1_LNJJFromHiggs12_transverse_mass");
        // addVartoStore("LNbJFromtop1_LNJJFromHiggs12_Relative_St");
        // addVartoStore("Tprime_transverse_mass_Gen");
        
        // addVartoStore("METFromNeutrinos_pt");
        // addVartoStore("METFromNeutrinos_phi");
        // addVartoStore("LeptonFromtop_NeutrinoFromtop_transverse_mass");
        // addVartoStore("LeptonFromHiggs_NeutrinoFromHiggs_transverse_mass");
    }
}

ROOT::RDF::RNode TprimeAnalyser::redefineVars(RNode _rlm, std::string year, std::string process)
{
    string year2 = year;
    if(year == "2016preVFP" || year == "2016postVFP")
    {
     year2 = "2016";
    }

    _rlm = _rlm.Redefine("goodJets","(Jet_pt_corr > 30 && Jet_pt_corr < 50 && abs(Jet_eta) < 2.4 && Jet_jetId >= 6 && Jet_puId == 7) || (Jet_pt_corr > 50 && abs(Jet_eta) < 2.4 && Jet_jetId >= 6)")
               .Redefine("Selected_jet_pt","Jet_pt_corr[goodJets]")
               .Redefine("Selected_jet_number","int(Selected_jet_pt.size())")
               .Redefine("Selected_jet_eta","Jet_eta[goodJets]")
               .Redefine("Selected_jet_phi","Jet_phi[goodJets]")
               .Redefine("Selected_jet_mass","Jet_mass_corr[goodJets]")
               .Redefine("Selected_jet_btagDeepFlavB","Jet_btagDeepFlavB[goodJets]")
               .Redefine("Selected_jet_hadronFlavour","Jet_hadronFlavour[goodJets]")
               .Redefine("Selected_jet_puId","Jet_puId[goodJets]")
               .Redefine("jet4vecs", ::generate_4vec,{"Selected_jet_pt","Selected_jet_eta","Selected_jet_phi","Selected_jet_mass"});

    _rlm = _rlm.Redefine("Lepton_Jet_overlap",::CheckOverlaps,{"jet4vecs","lep4vecs"})
               .Redefine("Selected_clean_jet_pt","Selected_jet_pt[Lepton_Jet_overlap]")
               .Redefine("Selected_clean_jet_number","int(Selected_clean_jet_pt.size())")
               .Redefine("Selected_clean_jet_eta","Selected_jet_eta[Lepton_Jet_overlap]")
               .Redefine("Selected_clean_jet_phi","Selected_jet_phi[Lepton_Jet_overlap]")
               .Redefine("Selected_clean_jet_mass","Selected_jet_mass[Lepton_Jet_overlap]")
               .Redefine("Selected_clean_jet_btagDeepFlavB","Selected_jet_btagDeepFlavB[Lepton_Jet_overlap]")
               .Redefine("Selected_clean_jet_hadronFlavour","Selected_jet_hadronFlavour[Lepton_Jet_overlap]")
               .Redefine("Selected_clean_jet_puId","Selected_jet_puId[Lepton_Jet_overlap]")
               .Redefine("cleanjet4vecs", ::generate_4vec,{"Selected_clean_jet_pt","Selected_clean_jet_eta","Selected_clean_jet_phi","Selected_clean_jet_mass"})
               .Redefine("cleanjet4Rvecs",::generate_4Rvec,{"Selected_clean_jet_pt","Selected_clean_jet_eta","Selected_clean_jet_phi","Selected_clean_jet_mass"});

    if(year == "2016preVFP")
    {
        _rlm = _rlm.Redefine("goodJets_btag","goodJets && Jet_btagDeepFlavB > 0.2598");
    }
    if(year == "2016postVFP")
    {
        _rlm = _rlm.Redefine("goodJets_btag","goodJets && Jet_btagDeepFlavB > 0.2489");
    }
    if(year == "2017")
    {
        _rlm = _rlm.Redefine("goodJets_btag","goodJets && Jet_btagDeepFlavB > 0.3040");
    }
    if(year == "2018")
    {
        _rlm = _rlm.Redefine("goodJets_btag","goodJets && Jet_btagDeepFlavB > 0.2783");
    }
    _rlm = _rlm.Redefine("Selected_bjet_pt","Jet_pt_corr[goodJets_btag]")
               .Redefine("Selected_bjet_number","int(Selected_bjet_pt.size())")
               .Redefine("Selected_bjet_eta","Jet_eta[goodJets_btag]")
               .Redefine("Selected_bjet_phi","Jet_phi[goodJets_btag]")
               .Redefine("Selected_bjet_mass","Jet_mass_corr[goodJets_btag]")
               .Redefine("Selected_bjet_puId","Jet_puId[goodJets_btag]")
               .Redefine("bjet4vecs", ::generate_4vec,{"Selected_bjet_pt","Selected_bjet_eta","Selected_bjet_phi","Selected_bjet_mass"});

    _rlm = _rlm.Redefine("Lepton_bJet_overlap",::CheckOverlaps,{"bjet4vecs","lep4vecs"})
               .Redefine("Selected_clean_bjet_pt","Selected_bjet_pt[Lepton_bJet_overlap]")
               .Redefine("Selected_clean_bjet_number","int(Selected_clean_bjet_pt.size())")
               .Redefine("Selected_clean_bjet_eta","Selected_bjet_eta[Lepton_bJet_overlap]")
               .Redefine("Selected_clean_bjet_phi","Selected_bjet_phi[Lepton_bJet_overlap]")
               .Redefine("Selected_clean_bjet_mass","Selected_bjet_mass[Lepton_bJet_overlap]")
               .Redefine("Selected_clean_bjet_puId","Selected_bjet_puId[Lepton_bJet_overlap]")
               .Redefine("cleanbjet4vecs", ::generate_4vec,{"Selected_clean_bjet_pt","Selected_clean_bjet_eta","Selected_clean_bjet_phi","Selected_clean_bjet_mass"});

    _rlm = _rlm.Redefine("MET_px","MET_pt_corr*cos(MET_phi_corr)")
               .Redefine("MET_py","MET_pt_corr*sin(MET_phi_corr)");

    _rlm = _rlm.Redefine("Min_deltaR_bJetFromtop_leptonFromtop",::minDR_1,{"Selected_lepton_eta","Selected_clean_bjet_eta","Selected_lepton_phi","Selected_clean_bjet_phi"})
               .Redefine("Lepton2Fromtop","lep4vecs[Min_deltaR_bJetFromtop_leptonFromtop[0]]")
               .Redefine("Lepton2Fromtop_pt","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? (lep4vecs[Min_deltaR_bJetFromtop_leptonFromtop[0]]).Pt():-100")
               .Redefine("Lepton2Fromtop_mass","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? (lep4vecs[Min_deltaR_bJetFromtop_leptonFromtop[0]]).M():-100")
               .Redefine("Lepton2Fromtop_transverse_energy","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt(Lepton2Fromtop_pt*Lepton2Fromtop_pt + Lepton2Fromtop_mass*Lepton2Fromtop_mass):-100")
               .Redefine("bJet1Fromtop","cleanbjet4vecs[Min_deltaR_bJetFromtop_leptonFromtop[1]]")
               .Redefine("bJet1Fromtop_pt","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? (cleanbjet4vecs[Min_deltaR_bJetFromtop_leptonFromtop[1]]).Pt():-100")
               .Redefine("bJet1Fromtop_eta","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? (cleanbjet4vecs[Min_deltaR_bJetFromtop_leptonFromtop[1]]).Eta():-10")
               .Redefine("bJet1Fromtop_phi","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? (cleanbjet4vecs[Min_deltaR_bJetFromtop_leptonFromtop[1]]).Phi():-10")
               .Redefine("bJet1Fromtop_mass","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? (cleanbjet4vecs[Min_deltaR_bJetFromtop_leptonFromtop[1]]).M():-100")
               .Redefine("bJet1Fromtop_transverse_energy","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt(bJet1Fromtop_pt*bJet1Fromtop_pt + bJet1Fromtop_mass*bJet1Fromtop_mass):-100")
               .Redefine("Lepton1FromHiggs","(Min_deltaR_bJetFromtop_leptonFromtop[0] == 0) ? lep4vecs[1]:lep4vecs[0]")
               .Redefine("Lepton1FromHiggs_pt","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ((Min_deltaR_bJetFromtop_leptonFromtop[0] == 0) ? (lep4vecs[1]).Pt():(lep4vecs[0]).Pt()):-100")
    	       .Redefine("Lepton1FromHiggs_mass","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ((Min_deltaR_bJetFromtop_leptonFromtop[0] == 0) ? (lep4vecs[1]).M():(lep4vecs[0]).M()):-100")
               .Redefine("Lepton1FromHiggs_transverse_energy","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt(Lepton1FromHiggs_pt*Lepton1FromHiggs_pt + Lepton1FromHiggs_mass*Lepton1FromHiggs_mass):-100");

    _rlm = _rlm.Redefine("cleanjet4Rvecs_withoutbjet",::removebjet,{"cleanjet4Rvecs","Selected_clean_jet_pt","bJet1Fromtop_pt"})
               .Redefine("cleanjet4Rvecs_withoutbjet_pt","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ROOT::VecOps::Map(cleanjet4Rvecs_withoutbjet, FourVecPt):Selected_lepton_pt")
               .Redefine("cleanjet4Rvecs_withoutbjet_number","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? int(cleanjet4Rvecs_withoutbjet_pt.size()):-10")
               .Redefine("cleanjet4Rvecs_withoutbjet_eta","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ROOT::VecOps::Map(cleanjet4Rvecs_withoutbjet, FourVecEta):Selected_lepton_pt")
               .Redefine("cleanjet4Rvecs_withoutbjet_phi","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ROOT::VecOps::Map(cleanjet4Rvecs_withoutbjet, FourVecPhi):Selected_lepton_pt")
               .Redefine("cleanjet4Rvecs_withoutbjet_mass","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ROOT::VecOps::Map(cleanjet4Rvecs_withoutbjet, FourVecMass):Selected_lepton_pt")
               .Redefine("cleanjet4vecs_withoutbjet",::generate_4vec,{"cleanjet4Rvecs_withoutbjet_pt","cleanjet4Rvecs_withoutbjet_eta","cleanjet4Rvecs_withoutbjet_phi","cleanjet4Rvecs_withoutbjet_mass"});

    _rlm = _rlm.Redefine("Min_deltaR_JetsFromHiggs",::minDR_2,{"cleanjet4Rvecs_withoutbjet_eta","cleanjet4Rvecs_withoutbjet_eta","cleanjet4Rvecs_withoutbjet_phi","cleanjet4Rvecs_withoutbjet_phi"})
    	       .Redefine("Jet1FromHiggs","cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[0]]")
               .Redefine("Jet1FromHiggs_pt","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? (cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[0]]).Pt():-100")
               .Redefine("Jet1FromHiggs_mass","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? (cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[0]]).M():-100")
               .Redefine("Jet1FromHiggs_transverse_energy","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt(Jet1FromHiggs_pt*Jet1FromHiggs_pt + Jet1FromHiggs_mass*Jet1FromHiggs_mass):-100")
               .Redefine("Jet2FromHiggs","cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[1]]")
               .Redefine("Jet2FromHiggs_pt","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? (cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[1]]).Pt():-100")
               .Redefine("Jet2FromHiggs_mass","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? (cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[1]]).M():-100")
               .Redefine("Jet2FromHiggs_transverse_energy","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt(Jet2FromHiggs_pt*Jet2FromHiggs_pt + Jet2FromHiggs_mass*Jet2FromHiggs_mass):-100");

    _rlm = _rlm.Redefine("Vectorial_sum_three_clean_jets",::Threejets,{"cleanjet4vecs","Selected_clean_jet_eta","Selected_clean_jet_phi","Selected_clean_jet_number","Selected_clean_bjet_number","bJet1Fromtop_eta","bJet1Fromtop_phi"})
               .Redefine("Vectorial_sum_three_clean_jets_mass","(Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? ROOT::VecOps::Map(Vectorial_sum_three_clean_jets, FourVecMass):Selected_clean_jet_pt")
               .Redefine("Vectorial_sum_three_clean_jets_mass_offset","(Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? Vectorial_sum_three_clean_jets_mass[0] - 172.7:-100")
               .Redefine("Vectorial_sum_three_clean_jets_mass_min","(Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? abs(Vectorial_sum_three_clean_jets_mass_offset):-100");

    _rlm = _rlm.Redefine("L2bJ1Fromtop_L1J1J2FromHiggs","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? lep4vecs[0]+lep4vecs[1]+cleanbjet4vecs[Min_deltaR_bJetFromtop_leptonFromtop[1]]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[0]]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[1]]:lep4vecs[0]")
               .Redefine("L2bJ1Fromtop_L1J1J2FromHiggs_pt","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? (lep4vecs[0]+lep4vecs[1]+cleanbjet4vecs[Min_deltaR_bJetFromtop_leptonFromtop[1]]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[0]]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[1]]).Pt():-100")
               .Redefine("L2bJ1Fromtop_L1J1J2FromHiggs_phi","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? (lep4vecs[0]+lep4vecs[1]+cleanbjet4vecs[Min_deltaR_bJetFromtop_leptonFromtop[1]]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[0]]+cleanjet4vecs_withoutbjet[Min_deltaR_JetsFromHiggs[1]]).Phi():-10")
               .Redefine("L2bJ1Fromtop_L1J1J2FromHiggs_px","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? L2bJ1Fromtop_L1J1J2FromHiggs_pt*cos(L2bJ1Fromtop_L1J1J2FromHiggs_phi):-10")
               .Redefine("L2bJ1Fromtop_L1J1J2FromHiggs_py","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? L2bJ1Fromtop_L1J1J2FromHiggs_pt*sin(L2bJ1Fromtop_L1J1J2FromHiggs_phi):-10")
               .Redefine("Tprime_transverse_mass_first_600","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt((Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*58.66*(58.66 + MET_pt_corr)))*(Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*58.66*(58.66 + MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py))):-100")
               .Redefine("Tprime_transverse_mass_second_600","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt((Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*58.66*(58.66 - MET_pt_corr)))*(Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*58.66*(58.66 - MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py))):-100")
               .Redefine("Tprime_transverse_mass_first_625","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt((Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*61.24*(61.24 + MET_pt_corr)))*(Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*61.24*(61.24 + MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py))):-100")
               .Redefine("Tprime_transverse_mass_second_625","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt((Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*61.24*(61.24 - MET_pt_corr)))*(Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*61.24*(61.24 - MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py))):-100")
               .Redefine("Tprime_transverse_mass_first_650","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt((Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*63.81*(63.81 + MET_pt_corr)))*(Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*63.81*(63.81 + MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py))):-100")
               .Redefine("Tprime_transverse_mass_second_650","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt((Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*63.81*(63.81 - MET_pt_corr)))*(Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*63.81*(63.81 - MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py))):-100")
               .Redefine("Tprime_transverse_mass_first_675","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt((Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*66.39*(66.39 + MET_pt_corr)))*(Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*66.39*(66.39 + MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py))):-100")
               .Redefine("Tprime_transverse_mass_second_675","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt((Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*66.39*(66.39 - MET_pt_corr)))*(Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*66.39*(66.39 - MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py))):-100")
               .Redefine("Tprime_transverse_mass_first_700","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt((Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*68.96*(68.96 + MET_pt_corr)))*(Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*68.96*(68.96 + MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py))):-100")
               .Redefine("Tprime_transverse_mass_second_700","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt((Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*68.96*(68.96 - MET_pt_corr)))*(Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*68.96*(68.96 - MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py))):-100")
               .Redefine("Tprime_transverse_mass_first_800","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt((Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*79.26*(79.26 + MET_pt_corr)))*(Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*79.26*(79.26 + MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py))):-100")
               .Redefine("Tprime_transverse_mass_second_800","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt((Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*79.26*(79.26 - MET_pt_corr)))*(Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*79.26*(79.26 - MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py))):-100")
               .Redefine("Tprime_transverse_mass_first_900","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt((Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*89.56*(89.56 + MET_pt_corr)))*(Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*89.56*(89.56 + MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py))):-100")
               .Redefine("Tprime_transverse_mass_second_900","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt((Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*89.56*(89.56 - MET_pt_corr)))*(Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*89.56*(89.56 - MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py))):-100")
               .Redefine("Tprime_transverse_mass_first_1000","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt((Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*99.86*(99.86 + MET_pt_corr)))*(Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*99.86*(99.86 + MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py))):-100")
               .Redefine("Tprime_transverse_mass_second_1000","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt((Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*99.86*(99.86 - MET_pt_corr)))*(Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*99.86*(99.86 - MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py))):-100")
               .Redefine("Tprime_transverse_mass_first_1100","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt((Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*110.16*(110.16 + MET_pt_corr)))*(Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*110.16*(110.16 + MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py))):-100")
               .Redefine("Tprime_transverse_mass_second_1100","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt((Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*110.16*(110.16 - MET_pt_corr)))*(Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*110.16*(110.16 - MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py))):-100")
               .Redefine("Tprime_transverse_mass_first_1200","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt((Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*120.46*(120.46 + MET_pt_corr)))*(Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*120.46*(120.46 + MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py))):-100")
               .Redefine("Tprime_transverse_mass_second_1200","(Selected_lepton_number > 1 && Selected_clean_jet_number >= 3 && Selected_clean_bjet_number >= 1) ? sqrt((Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*120.46*(120.46 - MET_pt_corr)))*(Lepton1FromHiggs_transverse_energy + Jet1FromHiggs_transverse_energy + Jet2FromHiggs_transverse_energy + Lepton2Fromtop_transverse_energy + bJet1Fromtop_transverse_energy + sqrt(MET_pt_corr*MET_pt_corr + 4*120.46*(120.46 - MET_pt_corr))) - ((L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px)*(L2bJ1Fromtop_L1J1J2FromHiggs_px + MET_px) + (L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py)*(L2bJ1Fromtop_L1J1J2FromHiggs_py + MET_py))):-100");

    // recalculate weights
    int _case = 1;
    std::vector<std::string> Jets_vars_names = {"Selected_clean_jet_hadronFlavour", "Selected_clean_jet_eta", "Selected_clean_jet_pt", "Selected_clean_jet_btagDeepFlavB"};
    string output_btag_column_name = "btag_SF_";
    if(year == "2016preVFP")
    {
        _rlm = calculateBTagSF(_rlm, Jets_vars_names, _case, 0.2598, "M", output_btag_column_name, 1);
    }
    if(year == "2016postVFP")
    {
        _rlm = calculateBTagSF(_rlm, Jets_vars_names, _case, 0.2489, "M", output_btag_column_name, 1);
    }
    if(year == "2017")
    {
        _rlm = calculateBTagSF(_rlm, Jets_vars_names, _case, 0.3040, "M", output_btag_column_name, 1);
    }
    if(year == "2018")
    {
        _rlm = calculateBTagSF(_rlm, Jets_vars_names, _case, 0.2783, "M", output_btag_column_name, 1);
    }
    std::vector<std::string> Jets_pileupjetid_vars_names = {"Selected_clean_jet_eta", "Selected_clean_jet_pt", "Selected_clean_jet_puId"};
    string output_pileupjetid_column_name = "pileupjetid_SF_";
    _rlm = calculatePileupJetIDSF(_rlm, Jets_pileupjetid_vars_names, year2, "T", output_pileupjetid_column_name, 1);

    _rlm = _rlm.Redefine("evWeight_wobtagSF", "pugenWeight * prefiring_SF_central * pileupjetid_SF_nom * muon_SF_central * ele_SF_central * TopPtWeight_nom")
               .Redefine("totbtagSF", "btag_SF_bcflav_central * btag_SF_lflav_central")
               .Redefine("evWeight", "pugenWeight * prefiring_SF_central * pileupjetid_SF_nom * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_central * ele_SF_central * TopPtWeight_nom");

    if(process.find("signal") == std::string::npos)
    {
        if(year2 == "2016")
        {
            _rlm = _rlm.Redefine("flip_evWeight_wobtagSF", "evWeight_wobtagSF*1.11")
                       .Redefine("flip_totbtagSF", "totbtagSF*1.11")
                       .Redefine("flip_evWeight", "evWeight*1.11")
                       .Redefine("fake_evWeight_wobtagSF", "evWeight_wobtagSF")
                       .Redefine("fake_totbtagSF", "totbtagSF")
                       .Redefine("fake_evWeight", "evWeight");
        }

        if(year2 == "2017")
        {
            _rlm = _rlm.Redefine("flip_evWeight_wobtagSF", "evWeight_wobtagSF*1.44")
                       .Redefine("flip_totbtagSF", "totbtagSF*1.44")
                       .Redefine("flip_evWeight", "evWeight*1.44")
                       .Redefine("fake_evWeight_wobtagSF", "evWeight_wobtagSF")
                       .Redefine("fake_totbtagSF", "totbtagSF")
                       .Redefine("fake_evWeight", "evWeight");
        }

        if(year2 == "2018")
        {
            _rlm = _rlm.Redefine("flip_evWeight_wobtagSF", "evWeight_wobtagSF*1.40")
                       .Redefine("flip_totbtagSF", "totbtagSF*1.40")
                       .Redefine("flip_evWeight", "evWeight*1.40")
                       .Redefine("fake_evWeight_wobtagSF", "evWeight_wobtagSF")
                       .Redefine("fake_totbtagSF", "totbtagSF")
                       .Redefine("fake_evWeight", "evWeight");
        }
    }

    return _rlm;
}

void TprimeAnalyser::bookHists(std::string year, std::string process, bool BTagEff, bool Combine, bool JEC, const std::vector<std::tuple<std::string, std::string, int, double, double, std::string>>& variables_MC, const std::vector<std::tuple<std::string, std::string, int, double, double, std::string>>& variables, const std::vector<std::string>& weights)
{

    if(debug)
    {
        if(BTagEff == 0)
        {
            if(Combine == 0)
            {
                cout<<"\n================= Function Call: bookHists =================\n";
                cout<<"Variables:"<<endl;
                if(!_isData)
                {
                    for (const auto& var : variables_MC)
                    {
                        cout<<"Histogram Name: "<<std::get<0>(var)
                                 <<", Histogram Title: "<<std::get<1>(var)
                                 <<", Bins: "<<std::get<2>(var)
                                 <<", Min: "<<std::get<3>(var)
                                 <<", Max: "<<std::get<4>(var)
                                 <<", Variable Name: "<<std::get<5>(var)<<endl;
                    }
                }
                for (const auto& var : variables)
                {
                    cout<<"Histogram Name: "<<std::get<0>(var)
                             <<", Histogram Title: "<<std::get<1>(var)
                             <<", Bins: "<<std::get<2>(var)
                             <<", Min: "<<std::get<3>(var)
                             <<", Max: "<<std::get<4>(var)
                             <<", Variable Name: "<<std::get<5>(var)<<endl;
                }
            }
            if(Combine == 1 && !_isData && JEC == 0)
            {
                cout<<"Weights:"<<endl;
	            if(weights.size()>0)
                {
	                for(const auto& weight : weights)
                    {
                        cout<<"Weight: "<<weight<<endl;
	                }
	            }
    	        else
                {
                    cout<<"Weight: No Weights are Provided!"<<endl;
	            }
            }
            cout<<"============================================================\n";
        }
    }

    string year2 = year;
    if(year == "2016preVFP" || year == "2016postVFP")
    {
        year2 = "2016";
    }

    string list_masses[10] = {"600", "625", "650", "675", "700", "800", "900", "1000", "1100", "1200"};

    if(BTagEff == 0)
    {
        if(process.find("signal") != std::string::npos)
        {
            string mass_nominal = process.substr(process.find('l')+1);
            int idx = 0;
            for (int j = 0; j < 10; j++)
            {
                if(list_masses[j] == mass_nominal)
                {
                    idx = j;
                    break;
                }
            }

            for(int i = 0; i < 4; i++)
            {
                if(Combine == 0)
                {
                    for(const auto& item : variables_MC)
                    {
                        string histogramName = std::get<0>(item);
                        string histogramTitle = std::get<1>(item);
                        int nBins = std::get<2>(item);
                        double minValue = std::get<3>(item);
                        double maxValue = std::get<4>(item);
                        string variableName = std::get<5>(item);
                        if(variableName.find("loosett1l") == std::string::npos)
                        {
                            if(histogramName.find("weight") != std::string::npos)
                            {
                                add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "one",std::to_string(i)+"_0_0_"+std::to_string(idx)+"_0");
                                add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "one",std::to_string(i)+"_0_0_"+std::to_string(idx)+"_1");
                                add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "one",std::to_string(i)+"_0_1_"+std::to_string(idx)+"_0");
                                add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "one",std::to_string(i)+"_0_2_"+std::to_string(idx)+"_0");
                                add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "one",std::to_string(i)+"_0_2_"+std::to_string(idx)+"_1");
                            }
                            else if(histogramName.find("Mother") != std::string::npos)
                            {
                                add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "evWeight",std::to_string(i)+"_0_0_"+std::to_string(idx)+"_0");
                                add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "evWeight",std::to_string(i)+"_0_0_"+std::to_string(idx)+"_1");
                                add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "evWeight",std::to_string(i)+"_0_1_"+std::to_string(idx)+"_0");
                            }
                            else
                            {
                                add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "one",std::to_string(i)+"_0_0_"+std::to_string(idx)+"_0");
                                add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "one",std::to_string(i)+"_0_0_"+std::to_string(idx)+"_1");
                                add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "one",std::to_string(i)+"_0_1_"+std::to_string(idx)+"_0");
                            }
                        }
                        else
                        {
                            if(histogramName.find("Mother") != std::string::npos)
                            {
                                add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(idx)+"_0");
                                add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(idx)+"_1");
                            }
                            else
                            {
                                add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "one",std::to_string(i)+"_0_2_"+std::to_string(idx)+"_0");
                                add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "one",std::to_string(i)+"_0_2_"+std::to_string(idx)+"_1");
                            }
                        }
                    }
                    add1DHist({"hnevents","Number(Events); Number(Events);Events;", 2, -0.5, 1.5}, "one", "evWeight",std::to_string(i)+"_0_0");
                    add1DHist({"hnevents","Number(Events); Number(Events);Events;", 2, -0.5, 1.5}, "one", "evWeight",std::to_string(i)+"_0_1");
                    add1DHist({"hnevents","Number(Events); Number(Events);Events;", 2, -0.5, 1.5}, "one", "evWeight_loosett1l",std::to_string(i)+"_0_2");
                    for(const auto& item : variables)
                    {
                        string histogramName = std::get<0>(item);
                        string histogramTitle = std::get<1>(item);
                        int nBins = std::get<2>(item);
                        double minValue = std::get<3>(item);
                        double maxValue = std::get<4>(item);
                        string variableName = std::get<5>(item);
                        if(variableName.find("loosett1l") == std::string::npos)
                        {
                            // add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "evWeight",std::to_string(i)+"_0");
                            add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "evWeight",std::to_string(i)+"_0_0_"+std::to_string(idx)+"_0");
                            add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "evWeight",std::to_string(i)+"_0_0_"+std::to_string(idx)+"_1");
                            add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "evWeight",std::to_string(i)+"_0_1_"+std::to_string(idx)+"_0");
                        }
                        else
                        {
                            add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(idx)+"_0");
                            add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(idx)+"_1");
                        }
                    }
                    // add1DHist( {"Tprime_transverse_mass_First","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_first_"+mass_nominal,"evWeight",std::to_string(i)+"_0_0");
                    // add1DHist( {"Tprime_transverse_mass_Second","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_second_"+mass_nominal,"evWeight",std::to_string(i)+"_0_0");
                    // add1DHist( {"Tprime_transverse_mass_First","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 14, 0.0, 2000.0}, "Tprime_transverse_mass_first_"+mass_nominal,"evWeight",std::to_string(i)+"_0_0_0_0_0");
                    // add1DHist( {"Tprime_transverse_mass_Second","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_second_"+mass_nominal,"evWeight",std::to_string(i)+"_0_0_0_0_0");
                    // add1DHist( {"Mass_Sum_Three_Jets","M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 10, 0.0, 1000.0}, "Vectorial_sum_three_clean_jets_mass","evWeight",std::to_string(i)+"_0_0_"+std::to_string(idx)+"_0");
                    // add1DHist( {"Mass_Sum_Three_Jets","M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 5, 75.0, 275.0}, "Vectorial_sum_three_clean_jets_mass","evWeight",std::to_string(i)+"_0_0_"+std::to_string(idx)+"_1");
                    // add1DHist( {"Mass_Sum_Three_Jets","M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 10, 0.0, 1000.0}, "Vectorial_sum_three_clean_jets_mass","evWeight",std::to_string(i)+"_0_1_"+std::to_string(idx)+"_0");
                    // add1DHist( {"Mass_Sum_Three_Jets","M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 5, 75.0, 275.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass","evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(idx)+"_0");
                    // add1DHist( {"Mass_Sum_Three_Jets","M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 5, 75.0, 275.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass","evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(idx)+"_1");
                    // add1DHist( {"Mass_Sum_Three_Jets_offset","M_{inv}(3j) - m_{top}; M_{inv}(3j) - m_{top} (GeV);Events;", 10, -200.0, 800.0}, "Vectorial_sum_three_clean_jets_mass_offset","evWeight",std::to_string(i)+"_0_0_"+std::to_string(idx)+"_0");
                    // add1DHist( {"Mass_Sum_Three_Jets_offset","M_{inv}(3j) - m_{top}; M_{inv}(3j) - m_{top} (GeV);Events;", 5, -100.0, 100.0}, "Vectorial_sum_three_clean_jets_mass_offset","evWeight",std::to_string(i)+"_0_0_"+std::to_string(idx)+"_1");
                    // add1DHist( {"Mass_Sum_Three_Jets_offset","M_{inv}(3j) - m_{top}; M_{inv}(3j) - m_{top} (GeV);Events;", 10, -200.0, 800.0}, "Vectorial_sum_three_clean_jets_mass_offset","evWeight",std::to_string(i)+"_0_1_"+std::to_string(idx)+"_0");
                    // add1DHist( {"Mass_Sum_Three_Jets_offset","M_{inv}(3j) - m_{top}; M_{inv}(3j) - m_{top} (GeV);Events;", 5, -100.0, 100.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass_offset","evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(idx)+"_0");
                    // add1DHist( {"Mass_Sum_Three_Jets_offset","M_{inv}(3j) - m_{top}; M_{inv}(3j) - m_{top} (GeV);Events;", 5, -100.0, 100.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass_offset","evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(idx)+"_1");
                    // add1DHist( {"Mass_Sum_Three_Jets_min","|M_{inv}(3j) - m_{top}|; |M_{inv}(3j) - m_{top}| (GeV);Events;", 10, 0.0, 500.0}, "Vectorial_sum_three_clean_jets_mass_min","evWeight",std::to_string(i)+"_0_0_"+std::to_string(idx)+"_0");
                    // add1DHist( {"Mass_Sum_Three_Jets_min","|M_{inv}(3j) - m_{top}|; |M_{inv}(3j) - m_{top}| (GeV);Events;", 5, 0.0, 100.0}, "Vectorial_sum_three_clean_jets_mass_min","evWeight",std::to_string(i)+"_0_0_"+std::to_string(idx)+"_1");
                    // add1DHist( {"Mass_Sum_Three_Jets_min","|M_{inv}(3j) - m_{top}|; |M_{inv}(3j) - m_{top}| (GeV);Events;", 10, 0.0, 500.0}, "Vectorial_sum_three_clean_jets_mass_min","evWeight",std::to_string(i)+"_0_1_"+std::to_string(idx)+"_0");
                    // add1DHist( {"Mass_Sum_Three_Jets_min","|M_{inv}(3j) - m_{top}|; |M_{inv}(3j) - m_{top}| (GeV);Events;", 5, 0.0, 100.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass_min","evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(idx)+"_0");
                    // add1DHist( {"Mass_Sum_Three_Jets_min","|M_{inv}(3j) - m_{top}|; |M_{inv}(3j) - m_{top}| (GeV);Events;", 5, 0.0, 100.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass_min","evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(idx)+"_1");
                    if(year == "2018")
                    {
                        add2DHist( {"Eta_Jets_Phi_Jets","Eta * Phi (jets); Eta (jets); Phi (jets);", 25, -2.5, 2.5, 35, -3.5, 3.5}, "Selected_clean_jet_eta","Selected_clean_jet_phi","evWeight",std::to_string(i)+"_0_0_"+std::to_string(idx)+"_0");
                        add2DHist( {"Eta_Jets_Phi_Jets","Eta * Phi (jets); Eta (jets); Phi (jets);", 25, -2.5, 2.5, 35, -3.5, 3.5}, "Selected_clean_jet_eta","Selected_clean_jet_phi","evWeight",std::to_string(i)+"_0_0_"+std::to_string(idx)+"_1");
                        add2DHist( {"Eta_Jets_Phi_Jets","Eta * Phi (jets); Eta (jets); Phi (jets);", 25, -2.5, 2.5, 35, -3.5, 3.5}, "Selected_clean_jet_eta","Selected_clean_jet_phi","evWeight",std::to_string(i)+"_0_1_"+std::to_string(idx)+"_0");
                        add2DHist( {"Eta_Jets_Phi_Jets","Eta * Phi (jets); Eta (jets); Phi (jets);", 25, -2.5, 2.5, 35, -3.5, 3.5}, "Selected_clean_jet_loosett1l_eta","Selected_clean_jet_loosett1l_phi","evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(idx)+"_0");
                        add2DHist( {"Eta_Jets_Phi_Jets","Eta * Phi (jets); Eta (jets); Phi (jets);", 25, -2.5, 2.5, 35, -3.5, 3.5}, "Selected_clean_jet_loosett1l_eta","Selected_clean_jet_loosett1l_phi","evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(idx)+"_1");
                    }
                    add1DHist( {"Tprime_transverse_mass_First","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_first_"+mass_nominal,"evWeight",std::to_string(i)+"_0_0_"+std::to_string(idx)+"_0");
                    add1DHist( {"Tprime_transverse_mass_First","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_first_"+mass_nominal,"evWeight",std::to_string(i)+"_0_0_"+std::to_string(idx)+"_1");
                    add1DHist( {"Tprime_transverse_mass_First","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_first_"+mass_nominal,"evWeight",std::to_string(i)+"_0_1_"+std::to_string(idx)+"_0");
                    add1DHist( {"Tprime_transverse_mass_First","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_first_loosett1l_"+mass_nominal,"evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(idx)+"_0");
                    add1DHist( {"Tprime_transverse_mass_First","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_first_loosett1l_"+mass_nominal,"evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(idx)+"_1");
                    add1DHist( {"Tprime_transverse_mass_Second","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_second_"+mass_nominal,"evWeight",std::to_string(i)+"_0_0_"+std::to_string(idx)+"_0");
                    add1DHist( {"Tprime_transverse_mass_Second","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_second_"+mass_nominal,"evWeight",std::to_string(i)+"_0_0_"+std::to_string(idx)+"_1");
                    add1DHist( {"Tprime_transverse_mass_Second","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_second_"+mass_nominal,"evWeight",std::to_string(i)+"_0_1_"+std::to_string(idx)+"_0");
                    add1DHist( {"Tprime_transverse_mass_Second","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_second_loosett1l_"+mass_nominal,"evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(idx)+"_0");
                    add1DHist( {"Tprime_transverse_mass_Second","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_second_loosett1l_"+mass_nominal,"evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(idx)+"_1");
                }
                if(Combine == 1 && JEC == 0)
                {
                    add1DHist( {"signal_evWeight","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 8, 300.0, 1500.0}, "Tprime_transverse_mass_first_"+mass_nominal,"evWeight",std::to_string(i)+"_0_0_"+std::to_string(idx)+"_0");
                    add1DHist( {"signal_evWeight","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 8, 300.0, 1500.0}, "Tprime_transverse_mass_first_"+mass_nominal,"evWeight",std::to_string(i)+"_0_0_"+std::to_string(idx)+"_1");
                    // add1DHist( {"signal_evWeight","Number(jets); Number(jets);Events;", 5, 3.0, 8.0}, "Selected_clean_jet_number","evWeight",std::to_string(i)+"_0_1_"+std::to_string(idx)+"_0");
                    // add1DHist( {"signal_evWeight","M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 5, 125.0, 225.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass","evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(idx)+"_0");
                    // add1DHist( {"signal_evWeight","M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 5, 125.0, 225.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass","evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(idx)+"_1");
                    for(const auto& weight : weights)
                    {
                        add1DHist( {Form("signal_%s",weight.c_str()),"M_{T,T\'}; M_{T,T\'} (GeV);Events;", 8, 300.0, 1500.0}, "Tprime_transverse_mass_first_"+mass_nominal,weight,std::to_string(i)+"_0_0_"+std::to_string(idx)+"_0");
                        add1DHist( {Form("signal_%s",weight.c_str()),"M_{T,T\'}; M_{T,T\'} (GeV);Events;", 8, 300.0, 1500.0}, "Tprime_transverse_mass_first_"+mass_nominal,weight,std::to_string(i)+"_0_0_"+std::to_string(idx)+"_1");
                        // add1DHist( {Form("signal_%s",weight.c_str()),"Number(jets); Number(jets);Events;", 5, 3.0, 8.0}, "Selected_clean_jet_number",weight,std::to_string(i)+"_0_1_"+std::to_string(idx)+"_0");
                        // add1DHist( {Form("signal_%s",weight.c_str()),"M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 5, 125.0, 225.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass",weight + "_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(idx)+"_0");
                        // add1DHist( {Form("signal_%s",weight.c_str()),"M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 5, 125.0, 225.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass",weight + "_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(idx)+"_1");
                    }
                }
                else if(Combine == 1 && JEC == 1)
                {
                    add1DHist( {"signal_evWeight","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 8, 300.0, 1500.0}, "Tprime_transverse_mass_first_"+mass_nominal,"evWeight",std::to_string(i)+"_0_0_"+std::to_string(idx)+"_0");
                    add1DHist( {"signal_evWeight","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 8, 300.0, 1500.0}, "Tprime_transverse_mass_first_"+mass_nominal,"evWeight",std::to_string(i)+"_0_0_"+std::to_string(idx)+"_1");
                    // add1DHist( {"signal_evWeight","Number(jets); Number(jets);Events;", 5, 3.0, 8.0}, "Selected_clean_jet_number","evWeight",std::to_string(i)+"_0_1_"+std::to_string(idx)+"_0");
                    // add1DHist( {"signal_evWeight","M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 5, 125.0, 225.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass","evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(idx)+"_0");
                    // add1DHist( {"signal_evWeight","M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 5, 125.0, 225.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass","evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(idx)+"_1");
                }
            }
        }
        else if(_isData)
        {
            for(int i = 0; i < 4; i++)
            {
                for(int j = 0; j < 10; j++)
                {
                    if(Combine == 0)
                    {
                        // add1DHist({"hnevents","Number(Events); Number(Events);Events;", 2, -0.5, 1.5}, "one", "evWeight",std::to_string(i)+"_0_0");
                        // add1DHist({"hnevents","Number(Events); Number(Events);Events;", 2, -0.5, 1.5}, "one", "evWeight",std::to_string(i)+"_0_1");
                        // add1DHist({"hnevents","Number(Events); Number(Events);Events;", 2, -0.5, 1.5}, "one", "evWeight_loosett1l",std::to_string(i)+"_0_2");
                        for(const auto& item : variables)
                        {
                            string histogramName = std::get<0>(item);
                            string histogramTitle = std::get<1>(item);
                            int nBins = std::get<2>(item);
                            double minValue = std::get<3>(item);
                            double maxValue = std::get<4>(item);
                            string variableName = std::get<5>(item);
                            if(variableName.find("loosett1l") == std::string::npos)
                            {
                                // add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "evWeight",std::to_string(i)+"_0");
                                add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0");
                                add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1");
                                add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "evWeight",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0");
                            }
                            else
                            {
                                add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0");
                                add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1");
                            }
                        }
                        // add1DHist( {"Mass_Sum_Three_Jets","M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 10, 0.0, 1000.0}, "Vectorial_sum_three_clean_jets_mass","evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0");
                        // add1DHist( {"Mass_Sum_Three_Jets","M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 5, 75.0, 275.0}, "Vectorial_sum_three_clean_jets_mass","evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1");
                        // add1DHist( {"Mass_Sum_Three_Jets","M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 10, 0.0, 1000.0}, "Vectorial_sum_three_clean_jets_mass","evWeight",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0");
                        // add1DHist( {"Mass_Sum_Three_Jets","M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 5, 75.0, 275.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass","evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0");
                        // add1DHist( {"Mass_Sum_Three_Jets","M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 5, 75.0, 275.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass","evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1");
                        // add1DHist( {"Mass_Sum_Three_Jets_offset","M_{inv}(3j) - m_{top}; M_{inv}(3j) - m_{top} (GeV);Events;", 10, -200.0, 800.0}, "Vectorial_sum_three_clean_jets_mass_offset","evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0");
                        // add1DHist( {"Mass_Sum_Three_Jets_offset","M_{inv}(3j) - m_{top}; M_{inv}(3j) - m_{top} (GeV);Events;", 5, -100.0, 100.0}, "Vectorial_sum_three_clean_jets_mass_offset","evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1");
                        // add1DHist( {"Mass_Sum_Three_Jets_offset","M_{inv}(3j) - m_{top}; M_{inv}(3j) - m_{top} (GeV);Events;", 10, -200.0, 800.0}, "Vectorial_sum_three_clean_jets_mass_offset","evWeight",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0");
                        // add1DHist( {"Mass_Sum_Three_Jets_offset","M_{inv}(3j) - m_{top}; M_{inv}(3j) - m_{top} (GeV);Events;", 5, -100.0, 100.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass_offset","evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0");
                        // add1DHist( {"Mass_Sum_Three_Jets_offset","M_{inv}(3j) - m_{top}; M_{inv}(3j) - m_{top} (GeV);Events;", 5, -100.0, 100.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass_offset","evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1");
                        // add1DHist( {"Mass_Sum_Three_Jets_min","|M_{inv}(3j) - m_{top}|; |M_{inv}(3j) - m_{top}| (GeV);Events;", 10, 0.0, 500.0}, "Vectorial_sum_three_clean_jets_mass_min","evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0");
                        // add1DHist( {"Mass_Sum_Three_Jets_min","|M_{inv}(3j) - m_{top}|; |M_{inv}(3j) - m_{top}| (GeV);Events;", 5, 0.0, 100.0}, "Vectorial_sum_three_clean_jets_mass_min","evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1");
                        // add1DHist( {"Mass_Sum_Three_Jets_min","|M_{inv}(3j) - m_{top}|; |M_{inv}(3j) - m_{top}| (GeV);Events;", 10, 0.0, 500.0}, "Vectorial_sum_three_clean_jets_mass_min","evWeight",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0");
                        // add1DHist( {"Mass_Sum_Three_Jets_min","|M_{inv}(3j) - m_{top}|; |M_{inv}(3j) - m_{top}| (GeV);Events;", 5, 0.0, 100.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass_min","evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0");
                        // add1DHist( {"Mass_Sum_Three_Jets_min","|M_{inv}(3j) - m_{top}|; |M_{inv}(3j) - m_{top}| (GeV);Events;", 5, 0.0, 100.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass_min","evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1");
                        // waiting for the unblinding to plot the transverse mass with data in the SR
                        if(year == "2018")
                        {
                            add2DHist( {"Eta_Jets_Phi_Jets","Eta * Phi (jets); Eta (jets); Phi (jets);", 25, -2.5, 2.5, 35, -3.5, 3.5}, "Selected_clean_jet_eta","Selected_clean_jet_phi","evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0");
                            add2DHist( {"Eta_Jets_Phi_Jets","Eta * Phi (jets); Eta (jets); Phi (jets);", 25, -2.5, 2.5, 35, -3.5, 3.5}, "Selected_clean_jet_eta","Selected_clean_jet_phi","evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1");
                            add2DHist( {"Eta_Jets_Phi_Jets","Eta * Phi (jets); Eta (jets); Phi (jets);", 25, -2.5, 2.5, 35, -3.5, 3.5}, "Selected_clean_jet_eta","Selected_clean_jet_phi","evWeight",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0");
                            add2DHist( {"Eta_Jets_Phi_Jets","Eta * Phi (jets); Eta (jets); Phi (jets);", 25, -2.5, 2.5, 35, -3.5, 3.5}, "Selected_clean_jet_loosett1l_eta","Selected_clean_jet_loosett1l_phi","evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0");
                            add2DHist( {"Eta_Jets_Phi_Jets","Eta * Phi (jets); Eta (jets); Phi (jets);", 25, -2.5, 2.5, 35, -3.5, 3.5}, "Selected_clean_jet_loosett1l_eta","Selected_clean_jet_loosett1l_phi","evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1");
                        }
                        // add1DHist( {"Tprime_transverse_mass_First","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_first_"+list_masses[j],"evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0");
                        add1DHist( {"Tprime_transverse_mass_First","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_first_"+list_masses[j],"evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1");
                        add1DHist( {"Tprime_transverse_mass_First","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_first_"+list_masses[j],"evWeight",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0");
                        add1DHist( {"Tprime_transverse_mass_First","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_first_loosett1l_"+list_masses[j],"evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0");
                        add1DHist( {"Tprime_transverse_mass_First","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_first_loosett1l_"+list_masses[j],"evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1");
                        // add1DHist( {"Tprime_transverse_mass_Second","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_second_"+list_masses[j],"evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0");
                        add1DHist( {"Tprime_transverse_mass_Second","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_second_"+list_masses[j],"evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1");
                        add1DHist( {"Tprime_transverse_mass_Second","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_second_"+list_masses[j],"evWeight",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0");
                        add1DHist( {"Tprime_transverse_mass_Second","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_second_loosett1l_"+list_masses[j],"evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0");
                        add1DHist( {"Tprime_transverse_mass_Second","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_second_loosett1l_"+list_masses[j],"evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1");
                    }
                    if(Combine == 1)
                    {
                        // add1DHist( {"data_obs_evWeight","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 8, 300.0, 1500.0}, "Tprime_transverse_mass_first_"+list_masses[j],"evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0");
                        add1DHist( {"data_obs_evWeight","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 8, 300.0, 1500.0}, "Tprime_transverse_mass_first_"+list_masses[j],"evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1");
                        // add1DHist( {"data_obs_evWeight","Number(jets); Number(jets);Events;", 5, 3.0, 8.0}, "Selected_clean_jet_number","evWeight",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0");
                        // add1DHist( {"data_obs_evWeight","M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 5, 125.0, 225.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass","evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0");
                        // add1DHist( {"data_obs_evWeight","M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 5, 125.0, 225.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass","evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1");
                    }
                }
            }
        }
        else
        {
            for(int i = 0; i < 4; i++)
            {
                for(int j = 0; j < 10; j++)
                {
                    if(Combine == 0)
                    {
                        for(const auto& item : variables_MC)
                        {
                            string histogramName = std::get<0>(item);
                            string histogramTitle = std::get<1>(item);
                            int nBins = std::get<2>(item);
                            double minValue = std::get<3>(item);
                            double maxValue = std::get<4>(item);
                            string variableName = std::get<5>(item);
                            if(variableName.find("loosett1l") == std::string::npos)
                            {
                                if(histogramName.find("weight") != std::string::npos)
                                {
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "one",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0");
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "one",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1");
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "one",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0");
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "one",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0");
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "one",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1");
                                }
                                else if(histogramName.find("Mother") != std::string::npos)
                                {
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0");
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1");
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "evWeight",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0");
                                }
                                else
                                {
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "one",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0");
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "one",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1");
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "one",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0");
                                }

                                if(histogramName.find("weight") != std::string::npos)
                                {
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "one",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0_0");
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "one",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1_0");
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "one",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0_0");
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "one",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0_0");
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "one",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1_0");
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "one",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0_1");
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "one",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1_1");
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "one",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0_1");
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "one",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0_1");
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "one",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1_1");
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "one",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0_2");
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "one",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1_2");
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "one",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0_2");
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "one",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0_2");
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "one",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1_2");
                                }
                                else if(histogramName.find("Mother") != std::string::npos)
                                {
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0_0");
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1_0");
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "evWeight",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0_0");
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "flip_evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0_1");
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "flip_evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1_1");
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "flip_evWeight",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0_1");
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "fake_evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0_2");
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "fake_evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1_2");
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "fake_evWeight",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0_2");
                                }
                                else
                                {
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "one",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0_0");
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "one",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1_0");
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "one",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0_0");
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "one",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0_1");
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "one",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1_1");
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "one",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0_1");
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "one",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0_2");
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "one",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1_2");
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "one",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0_2");
                                }
                            }
                            else
                            {
                                if(histogramName.find("Mother") != std::string::npos)
                                {
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0");
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1");
                                }
                                else
                                {
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "one",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0");
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "one",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1");
                                }

                                if(histogramName.find("Mother") != std::string::npos)
                                {
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0_0");
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "flip_evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0_1");
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "fake_evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0_2");
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1_0");
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "flip_evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1_1");
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "fake_evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1_2");
                                }
                                else
                                {
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "one",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0_0");
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "one",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0_1");
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "one",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0_2");
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "one",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1_0");
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "one",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1_1");
                                    add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "one",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1_2");
                                }
                            }
                        }
                        add1DHist({"hnevents","Number(Events); Number(Events);Events;", 2, -0.5, 1.5}, "one", "evWeight",std::to_string(i)+"_0_0");
                        add1DHist({"hnevents","Number(Events); Number(Events);Events;", 2, -0.5, 1.5}, "one", "evWeight",std::to_string(i)+"_0_1");
                        add1DHist({"hnevents","Number(Events); Number(Events);Events;", 2, -0.5, 1.5}, "one", "evWeight_loosett1l",std::to_string(i)+"_0_2");
                        add1DHist({"hnevents","Number(Events); Number(Events);Events;", 2, -0.5, 1.5}, "one", "flip_evWeight",std::to_string(i)+"_0_0");
                        add1DHist({"hnevents","Number(Events); Number(Events);Events;", 2, -0.5, 1.5}, "one", "flip_evWeight",std::to_string(i)+"_0_1");
                        add1DHist({"hnevents","Number(Events); Number(Events);Events;", 2, -0.5, 1.5}, "one", "flip_evWeight_loosett1l",std::to_string(i)+"_0_2");
                        add1DHist({"hnevents","Number(Events); Number(Events);Events;", 2, -0.5, 1.5}, "one", "fake_evWeight",std::to_string(i)+"_0_0");
                        add1DHist({"hnevents","Number(Events); Number(Events);Events;", 2, -0.5, 1.5}, "one", "fake_evWeight",std::to_string(i)+"_0_1");
                        add1DHist({"hnevents","Number(Events); Number(Events);Events;", 2, -0.5, 1.5}, "one", "fake_evWeight_loosett1l",std::to_string(i)+"_0_2");
                        for(const auto& item : variables)
                        {
                            string histogramName = std::get<0>(item);
                            string histogramTitle = std::get<1>(item);
                            int nBins = std::get<2>(item);
                            double minValue = std::get<3>(item);
                            double maxValue = std::get<4>(item);
                            string variableName = std::get<5>(item);
                            if(variableName.find("loosett1l") == std::string::npos)
                            {
                                // add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "evWeight",std::to_string(i)+"_0_0");
                                add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0");
                                add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1");
                                add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "evWeight",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0");
                            }
                            else
                            {
                                add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0");
                                add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1");
                            }

                            if(variableName.find("loosett1l") == std::string::npos)
                            {
                                add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0_0");
                                add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1_0");
                                add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "evWeight",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0_0");
                                add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "flip_evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0_1");
                                add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "flip_evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1_1");
                                add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "flip_evWeight",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0_1");
                                add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "fake_evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0_2");
                                add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "fake_evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1_2");
                                add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "fake_evWeight",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0_2");
                            }
                            else
                            {
                                add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0_0");
                                add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "flip_evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0_1");
                                add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "fake_evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0_2");
                                add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1_0");
                                add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "flip_evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1_1");
                                add1DHist({histogramName.c_str(), histogramTitle.c_str(), nBins, minValue, maxValue}, variableName.c_str(), "fake_evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1_2");
                            }
                        }
                        // if(j == 4) add1DHist( {"Tprime_transverse_mass_First","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_first_"+list_masses[j],"evWeight",std::to_string(i)+"_0_0");
                        // // add1DHist( {"Tprime_transverse_mass_First","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 14, 0.0, 2000.0}, "Tprime_transverse_mass_first_"+list_masses[j],"evWeight",std::to_string(i)+"_0_0_0_0_0");
                        // add1DHist( {"Tprime_transverse_mass_Second","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_second_"+list_masses[j],"evWeight",std::to_string(i)+"_0_0_0_0_0");
                        // add1DHist( {"Mass_Sum_Three_Jets","M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 10, 0.0, 1000.0}, "Vectorial_sum_three_clean_jets_mass","evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0");
                        // add1DHist( {"Mass_Sum_Three_Jets","M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 5, 75.0, 275.0}, "Vectorial_sum_three_clean_jets_mass","evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1");
                        // add1DHist( {"Mass_Sum_Three_Jets","M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 10, 0.0, 1000.0}, "Vectorial_sum_three_clean_jets_mass","evWeight",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0");
                        // add1DHist( {"Mass_Sum_Three_Jets","M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 5, 75.0, 275.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass","evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0");
                        // add1DHist( {"Mass_Sum_Three_Jets","M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 5, 75.0, 275.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass","evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1");
                        // add1DHist( {"Mass_Sum_Three_Jets_offset","M_{inv}(3j) - m_{top}; M_{inv}(3j) - m_{top} (GeV);Events;", 10, -200.0, 800.0}, "Vectorial_sum_three_clean_jets_mass_offset","evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0");
                        // add1DHist( {"Mass_Sum_Three_Jets_offset","M_{inv}(3j) - m_{top}; M_{inv}(3j) - m_{top} (GeV);Events;", 5, -100.0, 100.0}, "Vectorial_sum_three_clean_jets_mass_offset","evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1");
                        // add1DHist( {"Mass_Sum_Three_Jets_offset","M_{inv}(3j) - m_{top}; M_{inv}(3j) - m_{top} (GeV);Events;", 10, -200.0, 800.0}, "Vectorial_sum_three_clean_jets_mass_offset","evWeight",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0");
                        // add1DHist( {"Mass_Sum_Three_Jets_offset","M_{inv}(3j) - m_{top}; M_{inv}(3j) - m_{top} (GeV);Events;", 5, -100.0, 100.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass_offset","evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0");
                        // add1DHist( {"Mass_Sum_Three_Jets_offset","M_{inv}(3j) - m_{top}; M_{inv}(3j) - m_{top} (GeV);Events;", 5, -100.0, 100.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass_offset","evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1");
                        // add1DHist( {"Mass_Sum_Three_Jets_min","|M_{inv}(3j) - m_{top}|; |M_{inv}(3j) - m_{top}| (GeV);Events;", 10, 0.0, 500.0}, "Vectorial_sum_three_clean_jets_mass_min","evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0");
                        // add1DHist( {"Mass_Sum_Three_Jets_min","|M_{inv}(3j) - m_{top}|; |M_{inv}(3j) - m_{top}| (GeV);Events;", 5, 0.0, 100.0}, "Vectorial_sum_three_clean_jets_mass_min","evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1");
                        // add1DHist( {"Mass_Sum_Three_Jets_min","|M_{inv}(3j) - m_{top}|; |M_{inv}(3j) - m_{top}| (GeV);Events;", 10, 0.0, 500.0}, "Vectorial_sum_three_clean_jets_mass_min","evWeight",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0");
                        // add1DHist( {"Mass_Sum_Three_Jets_min","|M_{inv}(3j) - m_{top}|; |M_{inv}(3j) - m_{top}| (GeV);Events;", 5, 0.0, 100.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass_min","evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0");
                        // add1DHist( {"Mass_Sum_Three_Jets_min","|M_{inv}(3j) - m_{top}|; |M_{inv}(3j) - m_{top}| (GeV);Events;", 5, 0.0, 100.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass_min","evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1");
                        add1DHist( {"Tprime_transverse_mass_First","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_first_"+list_masses[j],"evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0");
                        add1DHist( {"Tprime_transverse_mass_First","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_first_"+list_masses[j],"evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1");
                        add1DHist( {"Tprime_transverse_mass_First","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_first_"+list_masses[j],"evWeight",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0");
                        add1DHist( {"Tprime_transverse_mass_First","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_first_loosett1l_"+list_masses[j],"evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0");
                        add1DHist( {"Tprime_transverse_mass_First","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_first_loosett1l_"+list_masses[j],"evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1");
                        add1DHist( {"Tprime_transverse_mass_Second","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_second_"+list_masses[j],"evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0");
                        add1DHist( {"Tprime_transverse_mass_Second","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_second_"+list_masses[j],"evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1");
                        add1DHist( {"Tprime_transverse_mass_Second","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_second_"+list_masses[j],"evWeight",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0");
                        add1DHist( {"Tprime_transverse_mass_Second","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_second_"+list_masses[j],"evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0");
                        add1DHist( {"Tprime_transverse_mass_Second","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_second_"+list_masses[j],"evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1");
    
                        // add1DHist( {"Mass_Sum_Three_Jets","M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 10, 0.0, 1000.0}, "Vectorial_sum_three_clean_jets_mass","evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0_0");
                        // add1DHist( {"Mass_Sum_Three_Jets","M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 5, 75.0, 275.0}, "Vectorial_sum_three_clean_jets_mass","evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1_0");
                        // add1DHist( {"Mass_Sum_Three_Jets","M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 10, 0.0, 1000.0}, "Vectorial_sum_three_clean_jets_mass","evWeight",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0_0");
                        // add1DHist( {"Mass_Sum_Three_Jets","M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 5, 75.0, 275.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass","evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0_0");
                        // add1DHist( {"Mass_Sum_Three_Jets","M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 5, 75.0, 275.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass","evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1_0");
                        // add1DHist( {"Mass_Sum_Three_Jets","M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 10, 0.0, 1000.0}, "Vectorial_sum_three_clean_jets_mass","flip_evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0_1");
                        // add1DHist( {"Mass_Sum_Three_Jets","M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 5, 75.0, 275.0}, "Vectorial_sum_three_clean_jets_mass","flip_evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1_1");
                        // add1DHist( {"Mass_Sum_Three_Jets","M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 10, 0.0, 1000.0}, "Vectorial_sum_three_clean_jets_mass","flip_evWeight",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0_1");
                        // add1DHist( {"Mass_Sum_Three_Jets","M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 5, 75.0, 275.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass","flip_evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0_1");
                        // add1DHist( {"Mass_Sum_Three_Jets","M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 5, 75.0, 275.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass","flip_evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1_1");
                        // add1DHist( {"Mass_Sum_Three_Jets","M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 10, 0.0, 1000.0}, "Vectorial_sum_three_clean_jets_mass","fake_evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0_2");
                        // add1DHist( {"Mass_Sum_Three_Jets","M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 5, 75.0, 275.0}, "Vectorial_sum_three_clean_jets_mass","fake_evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1_2");
                        // add1DHist( {"Mass_Sum_Three_Jets","M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 10, 0.0, 1000.0}, "Vectorial_sum_three_clean_jets_mass","fake_evWeight",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0_2");
                        // add1DHist( {"Mass_Sum_Three_Jets","M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 5, 75.0, 275.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass","fake_evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0_2");
                        // add1DHist( {"Mass_Sum_Three_Jets","M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 5, 75.0, 275.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass","fake_evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1_2");
                        // add1DHist( {"Mass_Sum_Three_Jets_offset","M_{inv}(3j) - m_{top}; M_{inv}(3j) - m_{top} (GeV);Events;", 10, -200.0, 800.0}, "Vectorial_sum_three_clean_jets_mass_offset","evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0_0");
                        // add1DHist( {"Mass_Sum_Three_Jets_offset","M_{inv}(3j) - m_{top}; M_{inv}(3j) - m_{top} (GeV);Events;", 5, -100.0, 100.0}, "Vectorial_sum_three_clean_jets_mass_offset","evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1_0");
                        // add1DHist( {"Mass_Sum_Three_Jets_offset","M_{inv}(3j) - m_{top}; M_{inv}(3j) - m_{top} (GeV);Events;", 10, -200.0, 800.0}, "Vectorial_sum_three_clean_jets_mass_offset","evWeight",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0_0");
                        // add1DHist( {"Mass_Sum_Three_Jets_offset","M_{inv}(3j) - m_{top}; M_{inv}(3j) - m_{top} (GeV);Events;", 5, -100.0, 100.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass_offset","evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0_0");
                        // add1DHist( {"Mass_Sum_Three_Jets_offset","M_{inv}(3j) - m_{top}; M_{inv}(3j) - m_{top} (GeV);Events;", 5, -100.0, 100.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass_offset","evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1_0");
                        // add1DHist( {"Mass_Sum_Three_Jets_offset","M_{inv}(3j) - m_{top}; M_{inv}(3j) - m_{top} (GeV);Events;", 10, -200.0, 800.0}, "Vectorial_sum_three_clean_jets_mass_offset","flip_evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0_1");
                        // add1DHist( {"Mass_Sum_Three_Jets_offset","M_{inv}(3j) - m_{top}; M_{inv}(3j) - m_{top} (GeV);Events;", 5, -100.0, 100.0}, "Vectorial_sum_three_clean_jets_mass_offset","flip_evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1_1");
                        // add1DHist( {"Mass_Sum_Three_Jets_offset","M_{inv}(3j) - m_{top}; M_{inv}(3j) - m_{top} (GeV);Events;", 10, -200.0, 800.0}, "Vectorial_sum_three_clean_jets_mass_offset","flip_evWeight",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0_1");
                        // add1DHist( {"Mass_Sum_Three_Jets_offset","M_{inv}(3j) - m_{top}; M_{inv}(3j) - m_{top} (GeV);Events;", 5, -100.0, 100.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass_offset","flip_evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0_1");
                        // add1DHist( {"Mass_Sum_Three_Jets_offset","M_{inv}(3j) - m_{top}; M_{inv}(3j) - m_{top} (GeV);Events;", 5, -100.0, 100.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass_offset","flip_evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1_1");
                        // add1DHist( {"Mass_Sum_Three_Jets_offset","M_{inv}(3j) - m_{top}; M_{inv}(3j) - m_{top} (GeV);Events;", 10, -200.0, 800.0}, "Vectorial_sum_three_clean_jets_mass_offset","fake_evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0_2");
                        // add1DHist( {"Mass_Sum_Three_Jets_offset","M_{inv}(3j) - m_{top}; M_{inv}(3j) - m_{top} (GeV);Events;", 5, -100.0, 100.0}, "Vectorial_sum_three_clean_jets_mass_offset","fake_evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1_2");
                        // add1DHist( {"Mass_Sum_Three_Jets_offset","M_{inv}(3j) - m_{top}; M_{inv}(3j) - m_{top} (GeV);Events;", 10, -200.0, 800.0}, "Vectorial_sum_three_clean_jets_mass_offset","fake_evWeight",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0_2");
                        // add1DHist( {"Mass_Sum_Three_Jets_offset","M_{inv}(3j) - m_{top}; M_{inv}(3j) - m_{top} (GeV);Events;", 5, -100.0, 100.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass_offset","fake_evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0_2");
                        // add1DHist( {"Mass_Sum_Three_Jets_offset","M_{inv}(3j) - m_{top}; M_{inv}(3j) - m_{top} (GeV);Events;", 5, -100.0, 100.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass_offset","fake_evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1_2");
                        // add1DHist( {"Mass_Sum_Three_Jets_min","|M_{inv}(3j) - m_{top}|; |M_{inv}(3j) - m_{top}| (GeV);Events;", 10, 0.0, 500.0}, "Vectorial_sum_three_clean_jets_mass_min","evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0_0");
                        // add1DHist( {"Mass_Sum_Three_Jets_min","|M_{inv}(3j) - m_{top}|; |M_{inv}(3j) - m_{top}| (GeV);Events;", 5, 0.0, 100.0}, "Vectorial_sum_three_clean_jets_mass_min","evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1_0");
                        // add1DHist( {"Mass_Sum_Three_Jets_min","|M_{inv}(3j) - m_{top}|; |M_{inv}(3j) - m_{top}| (GeV);Events;", 10, 0.0, 500.0}, "Vectorial_sum_three_clean_jets_mass_min","evWeight",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0_0");
                        // add1DHist( {"Mass_Sum_Three_Jets_min","|M_{inv}(3j) - m_{top}|; |M_{inv}(3j) - m_{top}| (GeV);Events;", 5, 0.0, 100.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass_min","evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0_0");
                        // add1DHist( {"Mass_Sum_Three_Jets_min","|M_{inv}(3j) - m_{top}|; |M_{inv}(3j) - m_{top}| (GeV);Events;", 5, 0.0, 100.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass_min","evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1_0");
                        // add1DHist( {"Mass_Sum_Three_Jets_min","|M_{inv}(3j) - m_{top}|; |M_{inv}(3j) - m_{top}| (GeV);Events;", 10, 0.0, 500.0}, "Vectorial_sum_three_clean_jets_mass_min","flip_evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0_1");
                        // add1DHist( {"Mass_Sum_Three_Jets_min","|M_{inv}(3j) - m_{top}|; |M_{inv}(3j) - m_{top}| (GeV);Events;", 5, 0.0, 100.0}, "Vectorial_sum_three_clean_jets_mass_min","flip_evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1_1");
                        // add1DHist( {"Mass_Sum_Three_Jets_min","|M_{inv}(3j) - m_{top}|; |M_{inv}(3j) - m_{top}| (GeV);Events;", 10, 0.0, 500.0}, "Vectorial_sum_three_clean_jets_mass_min","flip_evWeight",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0_1");
                        // add1DHist( {"Mass_Sum_Three_Jets_min","|M_{inv}(3j) - m_{top}|; |M_{inv}(3j) - m_{top}| (GeV);Events;", 5, 0.0, 100.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass_min","flip_evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0_1");
                        // add1DHist( {"Mass_Sum_Three_Jets_min","|M_{inv}(3j) - m_{top}|; |M_{inv}(3j) - m_{top}| (GeV);Events;", 5, 0.0, 100.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass_min","flip_evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1_1");
                        // add1DHist( {"Mass_Sum_Three_Jets_min","|M_{inv}(3j) - m_{top}|; |M_{inv}(3j) - m_{top}| (GeV);Events;", 10, 0.0, 500.0}, "Vectorial_sum_three_clean_jets_mass_min","fake_evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0_2");
                        // add1DHist( {"Mass_Sum_Three_Jets_min","|M_{inv}(3j) - m_{top}|; |M_{inv}(3j) - m_{top}| (GeV);Events;", 5, 0.0, 100.0}, "Vectorial_sum_three_clean_jets_mass_min","fake_evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1_2");
                        // add1DHist( {"Mass_Sum_Three_Jets_min","|M_{inv}(3j) - m_{top}|; |M_{inv}(3j) - m_{top}| (GeV);Events;", 10, 0.0, 500.0}, "Vectorial_sum_three_clean_jets_mass_min","fake_evWeight",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0_2");
                        // add1DHist( {"Mass_Sum_Three_Jets_min","|M_{inv}(3j) - m_{top}|; |M_{inv}(3j) - m_{top}| (GeV);Events;", 5, 0.0, 100.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass_min","fake_evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0_2");
                        // add1DHist( {"Mass_Sum_Three_Jets_min","|M_{inv}(3j) - m_{top}|; |M_{inv}(3j) - m_{top}| (GeV);Events;", 5, 0.0, 100.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass_min","fake_evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1_2");
                        if(year == "2018")
                        {
                            add2DHist( {"Eta_Jets_Phi_Jets","Eta * Phi (jets); Eta (jets); Phi (jets);", 25, -2.5, 2.5, 35, -3.5, 3.5}, "Selected_clean_jet_eta","Selected_clean_jet_phi","evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0");
                            add2DHist( {"Eta_Jets_Phi_Jets","Eta * Phi (jets); Eta (jets); Phi (jets);", 25, -2.5, 2.5, 35, -3.5, 3.5}, "Selected_clean_jet_eta","Selected_clean_jet_phi","evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1");
                            add2DHist( {"Eta_Jets_Phi_Jets","Eta * Phi (jets); Eta (jets); Phi (jets);", 25, -2.5, 2.5, 35, -3.5, 3.5}, "Selected_clean_jet_eta","Selected_clean_jet_phi","evWeight",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0");
                            add2DHist( {"Eta_Jets_Phi_Jets","Eta * Phi (jets); Eta (jets); Phi (jets);", 25, -2.5, 2.5, 35, -3.5, 3.5}, "Selected_clean_jet_loosett1l_eta","Selected_clean_jet_loosett1l_phi","evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0");
                            add2DHist( {"Eta_Jets_Phi_Jets","Eta * Phi (jets); Eta (jets); Phi (jets);", 25, -2.5, 2.5, 35, -3.5, 3.5}, "Selected_clean_jet_loosett1l_eta","Selected_clean_jet_loosett1l_phi","evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1");
                        }
                        add1DHist( {"Tprime_transverse_mass_First","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_first_"+list_masses[j],"evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0");
                        add1DHist( {"Tprime_transverse_mass_First","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_first_"+list_masses[j],"evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0_0");
                        add1DHist( {"Tprime_transverse_mass_First","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_first_"+list_masses[j],"evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1_0");
                        add1DHist( {"Tprime_transverse_mass_First","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_first_"+list_masses[j],"evWeight",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0_0");
                        add1DHist( {"Tprime_transverse_mass_First","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_first_loosett1l_"+list_masses[j],"evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0_0");
                        add1DHist( {"Tprime_transverse_mass_First","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_first_loosett1l_"+list_masses[j],"evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1_0");
                        add1DHist( {"Tprime_transverse_mass_First","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_first_"+list_masses[j],"flip_evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0_1");
                        add1DHist( {"Tprime_transverse_mass_First","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_first_"+list_masses[j],"flip_evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1_1");
                        add1DHist( {"Tprime_transverse_mass_First","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_first_"+list_masses[j],"flip_evWeight",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0_1");
                        add1DHist( {"Tprime_transverse_mass_First","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_first_loosett1l_"+list_masses[j],"flip_evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0_1");
                        add1DHist( {"Tprime_transverse_mass_First","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_first_loosett1l_"+list_masses[j],"flip_evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1_1");
                        add1DHist( {"Tprime_transverse_mass_First","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_first_"+list_masses[j],"fake_evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0_2");
                        add1DHist( {"Tprime_transverse_mass_First","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_first_"+list_masses[j],"fake_evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1_2");
                        add1DHist( {"Tprime_transverse_mass_First","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_first_"+list_masses[j],"fake_evWeight",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0_2");
                        add1DHist( {"Tprime_transverse_mass_First","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_first_loosett1l_"+list_masses[j],"fake_evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0_2");
                        add1DHist( {"Tprime_transverse_mass_First","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_first_loosett1l_"+list_masses[j],"fake_evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1_2");
                        add1DHist( {"Tprime_transverse_mass_Second","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_second_"+list_masses[j],"evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0_0");
                        add1DHist( {"Tprime_transverse_mass_Second","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_second_"+list_masses[j],"evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1_0");
                        add1DHist( {"Tprime_transverse_mass_Second","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_second_"+list_masses[j],"evWeight",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0_0");
                        add1DHist( {"Tprime_transverse_mass_Second","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_second_loosett1l_"+list_masses[j],"evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0_0");
                        add1DHist( {"Tprime_transverse_mass_Second","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_second_loosett1l_"+list_masses[j],"evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1_0");
                        add1DHist( {"Tprime_transverse_mass_Second","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_second_"+list_masses[j],"flip_evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0_1");
                        add1DHist( {"Tprime_transverse_mass_Second","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_second_"+list_masses[j],"flip_evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1_1");
                        add1DHist( {"Tprime_transverse_mass_Second","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_second_"+list_masses[j],"flip_evWeight",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0_1");
                        add1DHist( {"Tprime_transverse_mass_Second","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_second_loosett1l_"+list_masses[j],"flip_evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0_1");
                        add1DHist( {"Tprime_transverse_mass_Second","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_second_loosett1l_"+list_masses[j],"flip_evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1_1");
                        add1DHist( {"Tprime_transverse_mass_Second","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_second_"+list_masses[j],"fake_evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0_2");
                        add1DHist( {"Tprime_transverse_mass_Second","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_second_"+list_masses[j],"fake_evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1_2");
                        add1DHist( {"Tprime_transverse_mass_Second","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_second_"+list_masses[j],"fake_evWeight",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0_2");
                        add1DHist( {"Tprime_transverse_mass_Second","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_second_loosett1l_"+list_masses[j],"fake_evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0_2");
                        add1DHist( {"Tprime_transverse_mass_Second","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 10, 0.0, 1500.0}, "Tprime_transverse_mass_second_loosett1l_"+list_masses[j],"fake_evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1_2");
                    }
                    if(Combine == 1 && JEC == 0)
                    {
                        string process2 = process;
                        if(process.find("ww") != std::string::npos)
                        {
                            process2 = "multibosons";
                        }
                        if(process.find("tt2l") != std::string::npos || process.find("tt1l") != std::string::npos || process.find("tt0l") != std::string::npos || process.find("singletop") != std::string::npos || process.find("dy") != std::string::npos || process.find("wjets") != std::string::npos)
                        {
                            process2 = "others";
                        }

                        add1DHist( {Form("%s_evWeight",process2.c_str()),"M_{T,T\'}; M_{T,T\'} (GeV);Events;", 8, 300.0, 1500.0}, "Tprime_transverse_mass_first_"+list_masses[j],"evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0");
                        add1DHist( {Form("%s_evWeight",process2.c_str()),"M_{T,T\'}; M_{T,T\'} (GeV);Events;", 8, 300.0, 1500.0}, "Tprime_transverse_mass_first_"+list_masses[j],"evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1");
                        // add1DHist( {Form("%s_evWeight",process2.c_str()),"Number(jets); Number(jets);Events;", 5, 3.0, 8.0}, "Selected_clean_jet_number","evWeight",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0");
                        // add1DHist( {Form("%s_evWeight",process2.c_str()),"M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 5, 125.0, 225.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass","evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0");
                        // add1DHist( {Form("%s_evWeight",process2.c_str()),"M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 5, 125.0, 225.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass","evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1");
                        for(const auto& weight : weights)
                        {
                            add1DHist( {Form("%s_%s",process2.c_str(),weight.c_str()),"M_{T,T\'}; M_{T,T\'} (GeV);Events;", 8, 300.0, 1500.0}, "Tprime_transverse_mass_first_"+list_masses[j],weight,std::to_string(i)+"_0_0_"+std::to_string(j)+"_0");
                            add1DHist( {Form("%s_%s",process2.c_str(),weight.c_str()),"M_{T,T\'}; M_{T,T\'} (GeV);Events;", 8, 300.0, 1500.0}, "Tprime_transverse_mass_first_"+list_masses[j],weight,std::to_string(i)+"_0_0_"+std::to_string(j)+"_1");
                            // add1DHist( {Form("%s_%s",process2.c_str(),weight.c_str()),"Number(jets); Number(jets);Events;", 5, 3.0, 8.0}, "Selected_clean_jet_number",weight,std::to_string(i)+"_0_1_"+std::to_string(j)+"_0");
                            // add1DHist( {Form("%s_%s",process2.c_str(),weight.c_str()),"M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 5, 125.0, 225.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass",weight + "_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0");
                            // add1DHist( {Form("%s_%s",process2.c_str(),weight.c_str()),"M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 5, 125.0, 225.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass",weight + "_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1");
                        }

                        add1DHist( {Form("%s_evWeight",process2.c_str()),"M_{T,T\'}; M_{T,T\'} (GeV);Events;", 8, 300.0, 1500.0}, "Tprime_transverse_mass_first_"+list_masses[j],"evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0_0");
                        add1DHist( {Form("%s_evWeight",process2.c_str()),"M_{T,T\'}; M_{T,T\'} (GeV);Events;", 8, 300.0, 1500.0}, "Tprime_transverse_mass_first_"+list_masses[j],"evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1_0");
                        // add1DHist( {Form("%s_evWeight",process2.c_str()),"Number(jets); Number(jets);Events;", 5, 3.0, 8.0}, "Selected_clean_jet_number","evWeight",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0_0");
                        // add1DHist( {Form("%s_evWeight",process2.c_str()),"M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 5, 125.0, 225.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass","evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0_0");
                        // add1DHist( {Form("%s_evWeight",process2.c_str()),"M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 5, 125.0, 225.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass","evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1_0");
                        add1DHist( {"flip_evWeight","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 8, 300.0, 1500.0}, "Tprime_transverse_mass_first_"+list_masses[j],"flip_evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0_1");
                        add1DHist( {"flip_evWeight","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 8, 300.0, 1500.0}, "Tprime_transverse_mass_first_"+list_masses[j],"flip_evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1_1");
                        // add1DHist( {"flip_evWeight","Number(jets); Number(jets);Events;", 5, 3.0, 8.0}, "Selected_clean_jet_number","flip_evWeight",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0_1");
                        // add1DHist( {"flip_evWeight","M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 5, 125.0, 225.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass","flip_evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0_1");
                        // add1DHist( {"flip_evWeight","M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 5, 125.0, 225.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass","flip_evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1_1");
                        add1DHist( {"fake_evWeight","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 8, 300.0, 1500.0}, "Tprime_transverse_mass_first_"+list_masses[j],"fake_evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0_2");
                        add1DHist( {"fake_evWeight","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 8, 300.0, 1500.0}, "Tprime_transverse_mass_first_"+list_masses[j],"fake_evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1_2");
                        // add1DHist( {"fake_evWeight","Number(jets); Number(jets);Events;", 5, 3.0, 8.0}, "Selected_clean_jet_number","fake_evWeight",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0_2");
                        // add1DHist( {"fake_evWeight","M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 5, 125.0, 225.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass","fake_evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0_2");
                        // add1DHist( {"fake_evWeight","M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 5, 125.0, 225.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass","fake_evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1_2");
                        for(const auto& weight : weights)
                        {
                            add1DHist( {Form("%s_%s",process2.c_str(),weight.c_str()),"M_{T,T\'}; M_{T,T\'} (GeV);Events;", 8, 300.0, 1500.0}, "Tprime_transverse_mass_first_"+list_masses[j],weight,std::to_string(i)+"_0_0_"+std::to_string(j)+"_0_0");
                            add1DHist( {Form("%s_%s",process2.c_str(),weight.c_str()),"M_{T,T\'}; M_{T,T\'} (GeV);Events;", 8, 300.0, 1500.0}, "Tprime_transverse_mass_first_"+list_masses[j],weight,std::to_string(i)+"_0_0_"+std::to_string(j)+"_1_0");
                            // add1DHist( {Form("%s_%s",process2.c_str(),weight.c_str()),"Number(jets); Number(jets);Events;", 5, 3.0, 8.0}, "Selected_clean_jet_number",weight,std::to_string(i)+"_0_1_"+std::to_string(j)+"_0_0");
                            // add1DHist( {Form("%s_%s",process2.c_str(),weight.c_str()),"M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 5, 125.0, 225.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass",weight + "_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0_0");
                            // add1DHist( {Form("%s_%s",process2.c_str(),weight.c_str()),"M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 5, 125.0, 225.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass",weight + "_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1_0");
                            add1DHist( {Form("flip_%s",weight.c_str()),"M_{T,T\'}; M_{T,T\'} (GeV);Events;", 8, 300.0, 1500.0}, "Tprime_transverse_mass_first_"+list_masses[j],"flip_"+weight,std::to_string(i)+"_0_0_"+std::to_string(j)+"_0_1");
                            add1DHist( {Form("flip_%s",weight.c_str()),"M_{T,T\'}; M_{T,T\'} (GeV);Events;", 8, 300.0, 1500.0}, "Tprime_transverse_mass_first_"+list_masses[j],"flip_"+weight,std::to_string(i)+"_0_0_"+std::to_string(j)+"_1_1");
                            // add1DHist( {Form("flip_%s",weight.c_str()),"Number(jets); Number(jets);Events;", 5, 3.0, 8.0}, "Selected_clean_jet_number","flip_"+weight,std::to_string(i)+"_0_1_"+std::to_string(j)+"_0_1");
                            // add1DHist( {Form("flip_%s",weight.c_str()),"M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 5, 125.0, 225.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass","flip_"+weight+"_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0_1");
                            // add1DHist( {Form("flip_%s",weight.c_str()),"M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 5, 125.0, 225.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass","flip_"+weight+"_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1_1");
                            add1DHist( {Form("fake_%s",weight.c_str()),"M_{T,T\'}; M_{T,T\'} (GeV);Events;", 8, 300.0, 1500.0}, "Tprime_transverse_mass_first_"+list_masses[j],"fake_"+weight,std::to_string(i)+"_0_0_"+std::to_string(j)+"_0_2");
                            add1DHist( {Form("fake_%s",weight.c_str()),"M_{T,T\'}; M_{T,T\'} (GeV);Events;", 8, 300.0, 1500.0}, "Tprime_transverse_mass_first_"+list_masses[j],"fake_"+weight,std::to_string(i)+"_0_0_"+std::to_string(j)+"_1_2");
                            // add1DHist( {Form("fake_%s",weight.c_str()),"Number(jets); Number(jets);Events;", 5, 3.0, 8.0}, "Selected_clean_jet_number","fake_"+weight,std::to_string(i)+"_0_1_"+std::to_string(j)+"_0_2");
                            // add1DHist( {Form("fake_%s",weight.c_str()),"M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 5, 125.0, 225.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass","fake_"+weight+"_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0_2");
                            // add1DHist( {Form("fake_%s",weight.c_str()),"M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 5, 125.0, 225.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass","fake_"+weight+"_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1_2");
                        }
                    }
                    else if(Combine == 1 && JEC == 1)
                    {
                        string process2 = process;
                        if(process.find("ww") != std::string::npos)
                        {
                            process2 = "multibosons";
                        }
                        if(process.find("tt2l") != std::string::npos || process.find("tt1l") != std::string::npos || process.find("tt0l") != std::string::npos || process.find("singletop") != std::string::npos || process.find("dy") != std::string::npos || process.find("wjets") != std::string::npos)
                        {
                            process2 = "others";
                        }

                        add1DHist( {Form("%s_evWeight",process2.c_str()),"M_{T,T\'}; M_{T,T\'} (GeV);Events;", 8, 300.0, 1500.0}, "Tprime_transverse_mass_first_"+list_masses[j],"evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0");
                        add1DHist( {Form("%s_evWeight",process2.c_str()),"M_{T,T\'}; M_{T,T\'} (GeV);Events;", 8, 300.0, 1500.0}, "Tprime_transverse_mass_first_"+list_masses[j],"evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1");
                        // add1DHist( {Form("%s_evWeight",process2.c_str()),"Number(jets); Number(jets);Events;", 5, 3.0, 8.0}, "Selected_clean_jet_number","evWeight",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0");
                        // add1DHist( {Form("%s_evWeight",process2.c_str()),"M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 5, 125.0, 225.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass","evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0");
                        // add1DHist( {Form("%s_evWeight",process2.c_str()),"M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 5, 125.0, 225.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass","evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1");
                        add1DHist( {Form("%s_evWeight",process2.c_str()),"M_{T,T\'}; M_{T,T\'} (GeV);Events;", 8, 300.0, 1500.0}, "Tprime_transverse_mass_first_"+list_masses[j],"evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0_0");
                        add1DHist( {Form("%s_evWeight",process2.c_str()),"M_{T,T\'}; M_{T,T\'} (GeV);Events;", 8, 300.0, 1500.0}, "Tprime_transverse_mass_first_"+list_masses[j],"evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1_0");
                        // add1DHist( {Form("%s_evWeight",process2.c_str()),"Number(jets); Number(jets);Events;", 5, 3.0, 8.0}, "Selected_clean_jet_number","evWeight",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0_0");
                        // add1DHist( {Form("%s_evWeight",process2.c_str()),"M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 5, 125.0, 225.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass","evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0_0");
                        // add1DHist( {Form("%s_evWeight",process2.c_str()),"M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 5, 125.0, 225.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass","evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1_0");
                        add1DHist( {"flip_evWeight","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 8, 300.0, 1500.0}, "Tprime_transverse_mass_first_"+list_masses[j],"flip_evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0_1");
                        add1DHist( {"flip_evWeight","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 8, 300.0, 1500.0}, "Tprime_transverse_mass_first_"+list_masses[j],"flip_evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1_1");
                        // add1DHist( {"flip_evWeight","Number(jets); Number(jets);Events;", 5, 3.0, 8.0}, "Selected_clean_jet_number","flip_evWeight",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0_1");
                        // add1DHist( {"flip_evWeight","M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 5, 125.0, 225.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass","flip_evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0_1");
                        // add1DHist( {"flip_evWeight","M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 5, 125.0, 225.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass","flip_evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1_1");
                        add1DHist( {"fake_evWeight","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 8, 300.0, 1500.0}, "Tprime_transverse_mass_first_"+list_masses[j],"fake_evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_0_2");
                        add1DHist( {"fake_evWeight","M_{T,T\'}; M_{T,T\'} (GeV);Events;", 8, 300.0, 1500.0}, "Tprime_transverse_mass_first_"+list_masses[j],"fake_evWeight",std::to_string(i)+"_0_0_"+std::to_string(j)+"_1_2");
                        // add1DHist( {"fake_evWeight","Number(jets); Number(jets);Events;", 5, 3.0, 8.0}, "Selected_clean_jet_number","fake_evWeight",std::to_string(i)+"_0_1_"+std::to_string(j)+"_0_2");
                        // add1DHist( {"fake_evWeight","M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 5, 125.0, 225.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass","fake_evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_0_2");
                        // add1DHist( {"fake_evWeight","M_{inv}(3j); M_{inv}(3j) (GeV);Events;", 5, 125.0, 225.0}, "Vectorial_sum_three_clean_jets_loosett1l_mass","fake_evWeight_loosett1l",std::to_string(i)+"_0_2_"+std::to_string(j)+"_1_2");
                    }
                }
            }
        }
    }

    if(BTagEff == 1)
    {
        add2DHist( {"Eta_Pt_bJets_BtagPass_bcFlav","Eta * p_{T}(bJets) passing Btag (bc flavour); Eta(bjet) (GeV); p_{T}(bjet) (GeV);", 10, 0.0, 2.5, 20, 0.0, 1000.}, "Selected_clean_bjet_btagpass_bcflav_eta","Selected_clean_bjet_btagpass_bcflav_pt","evWeight_wobtagSF","0_0_0_0");
        add2DHist( {"Eta_Pt_Jets_All_bcFlav","Eta * p_{T}(Jets) (bc flavour); Eta(jet) (GeV); p_{T}(jet) (GeV);", 10, 0.0, 2.5, 20, 0.0, 1000.}, "Selected_clean_jet_all_bcflav_eta","Selected_clean_jet_all_bcflav_pt","evWeight_wobtagSF","0_0_0_0");
        add2DHist( {"Eta_Pt_bJets_BtagPass_lFlav","Eta * p_{T}(bJets) passing Btag (light flavour); Eta(bjet) (GeV); p_{T}(bjet) (GeV);", 10, 0.0, 2.5, 20, 0.0, 1000.}, "Selected_clean_bjet_btagpass_lflav_eta","Selected_clean_bjet_btagpass_lflav_pt","evWeight_wobtagSF","0_0_0_0");
        add2DHist( {"Eta_Pt_Jets_All_lFlav","Eta * p_{T}(Jets) (light flavour); Eta(jet) (GeV); p_{T}(jet) (GeV);", 10, 0.0, 2.5, 20, 0.0, 1000.}, "Selected_clean_jet_all_lflav_eta","Selected_clean_jet_all_lflav_pt","evWeight_wobtagSF","0_0_0_0");
        add2DHist( {"Eta_Pt_bJets_BtagPass_bcFlav","Eta * p_{T}(bJets) passing Btag (bc flavour); Eta(bjet) (GeV); p_{T}(bjet) (GeV);", 10, 0.0, 2.5, 20, 0.0, 1000.}, "Selected_clean_bjet_loosett1l_btagpass_bcflav_eta","Selected_clean_bjet_loosett1l_btagpass_bcflav_pt","evWeight_wobtagSF_loosett1l","0_0_1_0");
        add2DHist( {"Eta_Pt_Jets_All_bcFlav","Eta * p_{T}(Jets) (bc flavour); Eta(jet) (GeV); p_{T}(jet) (GeV);", 10, 0.0, 2.5, 20, 0.0, 1000.}, "Selected_clean_jet_loosett1l_all_bcflav_eta","Selected_clean_jet_loosett1l_all_bcflav_pt","evWeight_wobtagSF_loosett1l","0_0_1_0");
        add2DHist( {"Eta_Pt_bJets_BtagPass_lFlav","Eta * p_{T}(bJets) passing Btag (light flavour); Eta(bjet) (GeV); p_{T}(bjet) (GeV);", 10, 0.0, 2.5, 20, 0.0, 1000.}, "Selected_clean_bjet_loosett1l_btagpass_lflav_eta","Selected_clean_bjet_loosett1l_btagpass_lflav_pt","evWeight_wobtagSF_loosett1l","0_0_1_0");
        add2DHist( {"Eta_Pt_Jets_All_lFlav","Eta * p_{T}(Jets) (light flavour); Eta(jet) (GeV); p_{T}(jet) (GeV);", 10, 0.0, 2.5, 20, 0.0, 1000.}, "Selected_clean_jet_loosett1l_all_lflav_eta","Selected_clean_jet_loosett1l_all_lflav_pt","evWeight_wobtagSF_loosett1l","0_0_1_0");
    }

    // // histograms for the getQuantiles method
    // if(process.find("signal") == std::string::npos)
    // {
    //     // add2DHist( {"Tprime_transverse_mass_DeltaR_Leptons","M_{T,T\'} * \\Delta R (l_{1},l_{2}); M_{T,T\'} (GeV); \\Delta R (l_{1},l_{2});", 10, 0.0, 1500.0, 30, 0.0, 6.0}, "Tprime_transverse_mass_first_700","Selected_lepton_deltaR","evWeight","3_0_0");
    //     for(int i = 0; i < 10; i++)
    //     {
    //         // add2DHist( {Form("Tprime_transverse_mass_First_%s_Sum_Pt_Two_Leptons",list_masses[i].c_str()),"M_{T,T\'} * Pt(l_{1}) + Pt(l_{2}); M_{T,T\'} (GeV); Pt(l_{1}) + Pt(l_{2}) (GeV);", 10, 0.0, 1500.0, 30, 0.0, 600.0}, "Tprime_transverse_mass_first_"+list_masses[i],"Selected_lepton_sum_two_leptons_pt","evWeight","3_0_0");
    //         add2DHist( {Form("Tprime_transverse_mass_First_%s_Mass_Sum_Three_Jets_min",list_masses[i].c_str()),"M_{T,T\'} * |M_{inv}(3j) - m_{top}|; M_{T,T\'} (GeV); |M_{inv}(3j) - m_{top}| (GeV);", 10, 0.0, 1500.0, 10, 0.0, 200.0}, "Tprime_transverse_mass_first_"+list_masses[i],"Vectorial_sum_three_clean_jets_mass_min","evWeight","3_0_0_"+std::to_string(i));
    //     }
    // }
}

void TprimeAnalyser::setupCuts_and_Hists(std::string year, std::string process, bool BTagEff, bool Hist, bool JEC, const std::vector<std::string>& Cuts, const std::vector<std::string>& Masses, std::string second_cut)
{

	//ROOT::EnableImplicitMT();
	cout<<"setting up definitions, cuts, and histograms" <<endl;

    string year2 = year;
    if(year == "2016preVFP" || year == "2016postVFP")
    {
        year2 = "2016";
    }

    // if(!_isData && Hist == 0)
    // {
    //     add1DHist( {"genweight","Genweight of Events; Genweight of Events;Events;", 40, -400.0, 400.0}, "genWeight","one","");
    // }

    // add1DHist({"hnevents","Number(Events); Number(Events);Events;", 2, -0.5, 1.5}, "one", "evWeight","3");
    
	for(auto &x : _hist1dinfovector)
	{
		string hpost = "_nocut";

		if(x.mincutstep.length() == 0)
		{
			helper_1DHistCreator(std::string(x.hmodel.fName)+hpost, std::string(x.hmodel.fTitle)+hpost, x.hmodel.fNbinsX, x.hmodel.fXLow, x.hmodel.fXUp, x.varname, x.weightname, &_rlm);
		}
	}

	//for 2D histograms
	for(auto &x : _hist2dinfovector)
	{
		string hpost = "_nocut";

		if(x.mincutstep.length() == 0)
		{
			helper_2DHistCreator(std::string(x.hmodel.fName)+hpost,  std::string(x.hmodel.fTitle)+hpost, x.hmodel.fNbinsX, x.hmodel.fXLow, x.hmodel.fXUp, x.hmodel.fNbinsY, x.hmodel.fYLow, x.hmodel.fYUp, x.varname1, x.varname2, x.weightname, &_rlm);
		}
	}

	_rnt.setRNode(&_rlm);

    for(auto &acut : _cutinfovector)
	{
        string cutname = "cut_"+ acut.idx;
	    string hpost = "_"+cutname;
       	string parent_idx;
        parent_idx = acut.idx;
        // cout<<"parent_idx = "<<parent_idx<<endl;
        RNode *r = _rnt.getParent(parent_idx)->getRNode();
        auto rnext = new RNode(r->Define(cutname, acut.cutdefinition));

        int count = 0;
        for(int i = 0; i < acut.idx.length(); i++)
        {
            if(acut.idx[i] == '_')count++;
        }

        if(!_isData && !_jercunctag.empty() && Hist == 0 && JEC == 1 && count > 0 && acut.idx[0] == '3' && acut.idx[2] != '0')
        {
            cout<<"JES uncertainties activated"<<endl;
            int jes_unc_idx = 1;
            for(auto _unc_name : _jercunctag)
            {
                string jec_unc_up_reg_idx = acut.idx.substr(0,2)+std::to_string(jes_unc_idx);
                string jec_unc_down_reg_idx = acut.idx.substr(0,2)+std::to_string(jes_unc_idx+1);
                string jet_pt_name;
    	        string jet_mass_name;
            	string met_pt_name;
                string met_phi_name;

                if(acut.idx.find(jec_unc_up_reg_idx) == 0 && (acut.idx.length() == jec_unc_up_reg_idx.length() || (acut.idx.length() > jec_unc_up_reg_idx.length() && acut.idx[jec_unc_up_reg_idx.length()] == '_')))
                {
                    jet_pt_name = "Jet_pt_"+_unc_name+"_up";
                    jet_mass_name = "Jet_mass_"+_unc_name+"_up";
                	*rnext = rnext->Redefine("Jet_pt_corr",jet_pt_name);
                    *rnext = rnext->Redefine("Jet_mass_corr",jet_mass_name);

                    met_pt_name = "MET_pt_corr_"+_unc_name+"_up";
                    met_phi_name = "MET_phi_corr_"+_unc_name+"_up";
            	    *rnext = rnext->Redefine("MET_pt_corr",met_pt_name);
                    *rnext = rnext->Redefine("MET_phi_corr",met_phi_name);
    	            *rnext = redefineVars(*rnext, year, process);
    	            *rnext = rnext->Redefine(cutname, acut.cutdefinition);
                }
                if(acut.idx.find(jec_unc_down_reg_idx) == 0 && (acut.idx.length() == jec_unc_down_reg_idx.length() || (acut.idx.length() > jec_unc_down_reg_idx.length() && acut.idx[jec_unc_down_reg_idx.length()] == '_')))
                {
                    jet_pt_name = "Jet_pt_"+_unc_name+"_down";
                    jet_mass_name = "Jet_mass_"+_unc_name+"_down";
                    *rnext = rnext->Redefine("Jet_pt_corr",jet_pt_name);
                    *rnext = rnext->Redefine("Jet_mass_corr",jet_mass_name);

                    met_pt_name = "MET_pt_corr_"+_unc_name+"_down";
                    met_phi_name = "MET_phi_corr_"+_unc_name+"_down";
                    *rnext = rnext->Redefine("MET_pt_corr",met_pt_name);
        	        *rnext = rnext->Redefine("MET_phi_corr",met_phi_name);
            	    *rnext = redefineVars(*rnext, year, process);
            	    *rnext = rnext->Redefine(cutname, acut.cutdefinition);
                }
                jes_unc_idx += 2;
            }
        }
        *rnext = rnext->Filter(cutname);

        for(auto &cut : Cuts)
        {
            for(auto &mass: Masses)
            {
                string region;
                string region_temp;
                if(cut.find("mumu") != std::string::npos)
                {
                    region = "0_";
                }
                else if(cut.find("muel") != std::string::npos)
                {
                    region = "1_";
                }
                else if(cut.find("elel") != std::string::npos)
                {
                    region = "2_";
                }
                else if(cut.find("Total") != std::string::npos)
                {
                    region = "3_";
                }
                region += "0_";
                if(cut.find("SR") != std::string::npos && cut.find("MR_tt1l") == std::string::npos)
                {
                    region += "0_";
                    if(mass.find("600") != std::string::npos)
                    {
                        region += "0_0";
                    }
                    else if(mass.find("625") != std::string::npos)
                    {
                        region += "1_0";
                    }
                    else if(mass.find("650") != std::string::npos)
                    {
                        region += "2_0";
                    }
                    else if(mass.find("675") != std::string::npos)
                    {
                        region += "3_0";
                    }
                    else if(mass.find("700") != std::string::npos)
                    {
                        region += "4_0";
                    }
                    else if(mass.find("800") != std::string::npos)
                    {
                        region += "5_0";
                    }
                    else if(mass.find("900") != std::string::npos)
                    {
                        region += "6_0";
                    }
                    else if(mass.find("1000") != std::string::npos)
                    {
                        region += "7_0";
                    }
                    else if(mass.find("1100") != std::string::npos)
                    {
                        region += "8_0";
                    }
                    else if(mass.find("1200") != std::string::npos)
                    {
                        region += "9_0";
                    }
                }
                else if(cut.find("MR_tt2l") != std::string::npos)
                {
                    region += "1_";
                    if(mass.find("600") != std::string::npos)
                    {
                        region += "0_0";
                    }
                    else if(mass.find("625") != std::string::npos)
                    {
                        region += "1_0";
                    }
                    else if(mass.find("650") != std::string::npos)
                    {
                        region += "2_0";
                    }
                    else if(mass.find("675") != std::string::npos)
                    {
                        region += "3_0";
                    }
                    else if(mass.find("700") != std::string::npos)
                    {
                        region += "4_0";
                    }
                    else if(mass.find("800") != std::string::npos)
                    {
                        region += "5_0";
                    }
                    else if(mass.find("900") != std::string::npos)
                    {
                        region += "6_0";
                    }
                    else if(mass.find("1000") != std::string::npos)
                    {
                        region += "7_0";
                    }
                    else if(mass.find("1100") != std::string::npos)
                    {
                        region += "8_0";
                    }
                    else if(mass.find("1200") != std::string::npos)
                    {
                        region += "9_0";
                    }
                }
                else if(cut.find("MR_tt1l_SR") != std::string::npos)
                {
                    region += "2_";
                    if(mass.find("600") != std::string::npos)
                    {
                        region += "0_0";
                    }
                    else if(mass.find("625") != std::string::npos)
                    {
                        region += "1_0";
                    }
                    else if(mass.find("650") != std::string::npos)
                    {
                        region += "2_0";
                    }
                    else if(mass.find("675") != std::string::npos)
                    {
                        region += "3_0";
                    }
                    else if(mass.find("700") != std::string::npos)
                    {
                        region += "4_0";
                    }
                    else if(mass.find("800") != std::string::npos)
                    {
                        region += "5_0";
                    }
                    else if(mass.find("900") != std::string::npos)
                    {
                        region += "6_0";
                    }
                    else if(mass.find("1000") != std::string::npos)
                    {
                        region += "7_0";
                    }
                    else if(mass.find("1100") != std::string::npos)
                    {
                        region += "8_0";
                    }
                    else if(mass.find("1200") != std::string::npos)
                    {
                        region += "9_0";
                    }
                }
                else if(cut.find("MR_tt1l_CR_ttX") != std::string::npos)
                {
                    region += "2_";
                    if(mass.find("600") != std::string::npos)
                    {
                        region += "0_1";
                    }
                    else if(mass.find("625") != std::string::npos)
                    {
                        region += "1_1";
                    }
                    else if(mass.find("650") != std::string::npos)
                    {
                        region += "2_1";
                    }
                    else if(mass.find("675") != std::string::npos)
                    {
                        region += "3_1";
                    }
                    else if(mass.find("700") != std::string::npos)
                    {
                        region += "4_1";
                    }
                    else if(mass.find("800") != std::string::npos)
                    {
                        region += "5_1";
                    }
                    else if(mass.find("900") != std::string::npos)
                    {
                        region += "6_1";
                    }
                    else if(mass.find("1000") != std::string::npos)
                    {
                        region += "7_1";
                    }
                    else if(mass.find("1100") != std::string::npos)
                    {
                        region += "8_1";
                    }
                    else if(mass.find("1200") != std::string::npos)
                    {
                        region += "9_1";
                    }
                }
                else if(cut.find("CR_ttX") != std::string::npos && cut.find("MR_tt1l") == std::string::npos)
                {
                    region += "0_";
                    if(mass.find("600") != std::string::npos)
                    {
                        region += "0_1";
                    }
                    else if(mass.find("625") != std::string::npos)
                    {
                        region += "1_1";
                    }
                    else if(mass.find("650") != std::string::npos)
                    {
                        region += "2_1";
                    }
                    else if(mass.find("675") != std::string::npos)
                    {
                        region += "3_1";
                    }
                    else if(mass.find("700") != std::string::npos)
                    {
                        region += "4_1";
                    }
                    else if(mass.find("800") != std::string::npos)
                    {
                        region += "5_1";
                    }
                    else if(mass.find("900") != std::string::npos)
                    {
                        region += "6_1";
                    }
                    else if(mass.find("1000") != std::string::npos)
                    {
                        region += "7_1";
                    }
                    else if(mass.find("1100") != std::string::npos)
                    {
                        region += "8_1";
                    }
                    else if(mass.find("1200") != std::string::npos)
                    {
                        region += "9_1";
                    }
                }
                // cout<<"region = "<<region<<", acut.idx = "<<acut.idx<<endl;
                region_temp = region;

                // // trigger efficiencies
                // if(acut.idx == "0" || acut.idx == "0_0" || acut.idx == "1" || acut.idx == "1_0" || acut.idx == "2" || acut.idx == "2_0")
                // // separate preselection criteria
                // if(acut.idx == "3" || acut.idx == "3_0" || acut.idx == "3_0_0" || acut.idx == "3_0_0_0" || acut.idx == "3_0_0_0_0" || acut.idx == "3_0_0_0_0_0")
                // // first cut-based selection
                // if(acut.idx == "0_0_0" || acut.idx == "0_0_0_0" || acut.idx == "0_0_0_0_0_0" || acut.idx == "0_0_0_0_0" || acut.idx == "1_0_0" || acut.idx == "1_0_0_0" || acut.idx == "1_0_0_0_0" || acut.idx == "1_0_0_0_0_0" || acut.idx == "2_0_0" || acut.idx == "2_0_0_0" || acut.idx == "2_0_0_0_0" || acut.idx == "2_0_0_0_0_0" || acut.idx == "3_0_0" || acut.idx == "3_0_0_0" || acut.idx == "3_0_0_0_0" || acut.idx == "3_0_0_0_0_0")
                // // separate DeltaR cut from the preselection
                // if(acut.idx == "3_0_0" || acut.idx == "3_0_0_4" || acut.idx == "3_0_0_4_0" || acut.idx == "3_0_0_4_0_0")
                // // optimize the selection
                // if(acut.idx == "3_0_0_0" || acut.idx == "3_0_0_1" || acut.idx == "3_0_0_2" || acut.idx == "3_0_0_3" || acut.idx == "3_0_0_4" || acut.idx == "3_0_0_5" || acut.idx == "3_0_0_6" || acut.idx == "3_0_0_7" || acut.idx == "3_0_0_8" || acut.idx == "3_0_0_9")
                if((BTagEff == 1 && count == 3) || (BTagEff == 0 && JEC == 0 && region == acut.idx && count < 5))
                {
                    // for 1DHistograms
                    for(auto &x : _hist1dinfovector)
                    {
                        // cout<<"std::string(x.hmodel.fName)+hpost = "<<std::string(x.hmodel.fName)+hpost<<endl;
                        // cout<<"x.mincutstep.length() = "<<x.mincutstep.length()<<", x.mincutstep = "<<x.mincutstep<<endl;
                        if((acut.idx).compare(0, x.mincutstep.length(), x.mincutstep) == 0) // && std::string(x.hmodel.fName).find("Tprime_transverse_mass_First") != std::string::npos)
	                    {
                            // cout<<"std::string(x.hmodel.fName)+hpost = "<<std::string(x.hmodel.fName)+hpost<<endl;
	        		        helper_1DHistCreator(std::string(x.hmodel.fName)+hpost, std::string(x.hmodel.fTitle)+hpost, x.hmodel.fNbinsX, x.hmodel.fXLow, x.hmodel.fXUp, x.varname, x.weightname, rnext);
    	                }
	                }

            		// for 2DHistograms
    	            for(auto &x : _hist2dinfovector)
                    {
                        // cout<<"std::string(x.hmodel.fName)+hpost = "<<std::string(x.hmodel.fName)+hpost<<endl;
		                if((acut.idx).compare(0, x.mincutstep.length(), x.mincutstep) == 0)
	                    {
                            helper_2DHistCreator(std::string(x.hmodel.fName)+hpost, std::string(x.hmodel.fTitle)+hpost, x.hmodel.fNbinsX, x.hmodel.fXLow, x.hmodel.fXUp, x.hmodel.fNbinsY, x.hmodel.fYLow, x.hmodel.fYUp, x.varname1, x.varname2, x.weightname, rnext);
			            }
		            }
                }

                if(BTagEff == 0 && JEC == 0 && process.find("signal") == std::string::npos && !_isData && acut.idx.find(region) != std::string::npos && count == 5)
                {
                    // prompt leptons
                    region = region_temp + "_0";
                    // cout<<"region_temp = "<<region_temp<<", region = "<<region<<endl;
                    for(auto &x : _hist1dinfovector)
	                {
                        // cout<<"std::string(x.hmodel.fName)+hpost = "<<std::string(x.hmodel.fName)+hpost<<endl;
                        if((acut.idx).compare(0, x.mincutstep.length(), x.mincutstep) == 0)// && std::string(x.hmodel.fName).find("Tprime_transverse_mass_First") != std::string::npos)
			            {
                            // cout<<"std::string(x.hmodel.fName)+hpost = "<<std::string(x.hmodel.fName)+hpost<<endl;
	                        helper_1DHistCreator(std::string(x.hmodel.fName)+hpost, std::string(x.hmodel.fTitle)+hpost, x.hmodel.fNbinsX, x.hmodel.fXLow, x.hmodel.fXUp, x.varname, x.weightname, rnext);
	                    }
		            }

                    // flip leptons
                    region = region_temp + "_1";
                    for(auto &x : _hist1dinfovector)
		            {
                        if((acut.idx).compare(0, x.mincutstep.length(), x.mincutstep) == 0)// && std::string(x.hmodel.fName).find("Tprime_transverse_mass_First") != std::string::npos)
                        {
                            helper_1DHistCreator(std::string(x.hmodel.fName)+hpost, std::string(x.hmodel.fTitle)+hpost, x.hmodel.fNbinsX, x.hmodel.fXLow, x.hmodel.fXUp, x.varname, x.weightname, rnext);
	                    }
	                }

                    // fake leptons
                    region = region_temp + "_2";
                    for(auto &x : _hist1dinfovector)
                    {
                        if((acut.idx).compare(0, x.mincutstep.length(), x.mincutstep) == 0)// && std::string(x.hmodel.fName).find("Tprime_transverse_mass_First") != std::string::npos)
		                {
                            helper_1DHistCreator(std::string(x.hmodel.fName)+hpost, std::string(x.hmodel.fTitle)+hpost, x.hmodel.fNbinsX, x.hmodel.fXLow, x.hmodel.fXUp, x.varname, x.weightname, rnext);
		                }
                    }
                }
            }
        }

        if(!_isData && Hist == 1 && JEC == 1 && acut.idx.substr(0,1) != "3")
        {
            if(process.find("signal") != std::string::npos)
            {
                string list_masses[10] = {"600", "625", "650", "675", "700", "800", "900", "1000", "1100", "1200"};
                string mass_nominal = process.substr(process.find('l')+1);
                string hpost2;
                string hpost_total;
                int idx = 0;
                for (int j = 0; j < 10; j++)
                {
                    if(list_masses[j] == mass_nominal)
                    {
                        idx = j;
                        break;
                    }
                }

                if(count == 4)
                {
                    if(second_cut == '1')
                    {
                        hpost_total = "_syst_JES_Absolute_"+year2+"Up_cut_"+acut.idx.substr(0,1)+"_1_0_"+idx+"_"+acut.idx[8];
                    }
                    else if(second_cut == '2')
                    {
                        hpost_total = "_syst_JES_Absolute_"+year2+"Down_cut_"+acut.idx.substr(0,1)+"_2_0_"+idx+"_"+acut.idx[8];
                    }
                    else if(second_cut == '3')
                    {
                        hpost_total = "_syst_JES_AbsoluteUp_cut_"+acut.idx.substr(0,1)+"_3_0_"+idx+"_"+acut.idx[8];
                    }
                    else if(second_cut == '4')
                    {
                        hpost_total = "_syst_JES_AbsoluteDown_cut_"+acut.idx.substr(0,1)+"_4_0_"+idx+"_"+acut.idx[8];
                    }
                    else if(second_cut == '5')
                    {
                        hpost_total = "_syst_JES_BBEC1_"+year2+"Up_cut_"+acut.idx.substr(0,1)+"_5_0_"+idx+"_"+acut.idx[8];
                    }
                    else if(second_cut == '6')
                    {
                        hpost_total = "_syst_JES_BBEC1_"+year2+"Down_cut_"+acut.idx.substr(0,1)+"_6_0_"+idx+"_"+acut.idx[8];
                    }
                    else if(second_cut == '7')
                    {
                        hpost_total = "_syst_JES_BBEC1Up_cut_"+acut.idx.substr(0,1)+"_7_0_"+idx+"_"+acut.idx[8];
                    }
                    else if(second_cut == '8')
                    {
                        hpost_total = "_syst_JES_BBEC1Down_cut_"+acut.idx.substr(0,1)+"_8_0_"+idx+"_"+acut.idx[8];
                    }
                    else if(second_cut == '9')
                    {
                        hpost_total = "_syst_JES_EC2_"+year2+"Up_cut_"+acut.idx.substr(0,1)+"_9_0_"+idx+"_"+acut.idx[8];
                    }
                    else if(second_cut == "10")
                    {
                        hpost_total = "_syst_JES_EC2_"+year2+"Down_cut_"+acut.idx.substr(0,1)+"_10_0_"+idx+"_"+acut.idx[8];
                    }
                    else if(second_cut == "11")
                    {
                        hpost_total = "_syst_JES_EC2Up_cut_"+acut.idx.substr(0,1)+"_11_0_"+idx+"_"+acut.idx[8];
                    }
                    else if(second_cut == "12")
                    {
                        hpost_total = "_syst_JES_EC2Down_cut_"+acut.idx.substr(0,1)+"_12_0_"+idx+"_"+acut.idx[8];
                    }
                    else if(second_cut == "13")
                    {
                        hpost_total = "_syst_JES_FlavorQCDUp_cut_"+acut.idx.substr(0,1)+"_13_0_"+idx+"_"+acut.idx[8];
                    }
                    else if(second_cut == "14")
                    {
                        hpost_total = "_syst_JES_FlavorQCDDown_cut_"+acut.idx.substr(0,1)+"_14_0_"+idx+"_"+acut.idx[8];
                    }
                    else if(second_cut == "15")
                    {
                        hpost_total = "_syst_JES_HF_"+year2+"Up_cut_"+acut.idx.substr(0,1)+"_15_0_"+idx+"_"+acut.idx[8];
                    }
                    else if(second_cut == "16")
                    {
                        hpost_total = "_syst_JES_HF_"+year2+"Down_cut_"+acut.idx.substr(0,1)+"_16_0_"+idx+"_"+acut.idx[8];
                    }
                    else if(second_cut == "17")
                    {
                        hpost_total = "_syst_JES_HFUp_cut_"+acut.idx.substr(0,1)+"_17_0_"+idx+"_"+acut.idx[8];
                    }
                    else if(second_cut == "18")
                    {
                        hpost_total = "_syst_JES_HFDown_cut_"+acut.idx.substr(0,1)+"_18_0_"+idx+"_"+acut.idx[8];
                    }
                    else if(second_cut == "19")
                    {
                        hpost_total = "_syst_JES_RelativeSample_"+year2+"Up_cut_"+acut.idx.substr(0,1)+"_19_0_"+idx+"_"+acut.idx[8];
                    }
                    else if(second_cut == "20")
                    {
                        hpost_total = "_syst_JES_RelativeSample_"+year2+"Down_cut_"+acut.idx.substr(0,1)+"_20_0_"+idx+"_"+acut.idx[8];
                    }
                    else if(second_cut == "21")
                    {
                        hpost_total = "_syst_JES_RelativeBalUp_cut_"+acut.idx.substr(0,1)+"_21_0_"+idx+"_"+acut.idx[8];
                    }
                    else if(second_cut == "22")
                    {
                        hpost_total = "_syst_JES_RelativeBalDown_cut_"+acut.idx.substr(0,1)+"_22_0_"+idx+"_"+acut.idx[8];
                    }

                    if(acut.idx[8] == '0')
                    {
                        hpost2 = "_cut_"+acut.idx.substr(0,1)+"_0_0_"+idx+"_0";
                    }
                    else if(acut.idx[8] == '1')
                    {
                        hpost2 = "_cut_"+acut.idx.substr(0,1)+"_0_0_"+idx+"_1";
                    }
                    for(auto &x : _hist1dinfovector)
                    {
                        if(acut.idx.compare(0, x.mincutstep.length(), x.mincutstep) == 0)
	                    {
                            // cout<<"std::string(x.hmodel.fName)+hpost = "<<std::string(x.hmodel.fName)+hpost_total<<endl;
                    		helper_1DHistCreator(std::string(x.hmodel.fName).erase(std::string(x.hmodel.fName).length()-9, 9)+hpost_total, std::string(x.hmodel.fTitle)+hpost_total, x.hmodel.fNbinsX, x.hmodel.fXLow, x.hmodel.fXUp, x.varname, x.weightname, rnext);
    	                }
	                }
                }
            }
            else
            {
                for(int i = 0; i < 10; i++)
                {
                    string hpost2;
                    string hpost_total;
                    string process2 = process;
                    if(process.find("ww") != std::string::npos)
                    {
                        process2 = "multibosons";
                    }
                    if(process.find("tt2l") != std::string::npos || process.find("tt1l") != std::string::npos || process.find("tt0l") != std::string::npos || process.find("singletop") != std::string::npos || process.find("dy") != std::string::npos || process.find("wjets") != std::string::npos)
                    {
                        process2 = "others";
                    }

                    if(count == 5)
                    {
                        if(second_cut == '1')
                        {
                            hpost_total = "_syst_JES_Absolute_"+year2+"Up_cut_"+acut.idx.substr(0,1)+"_1_0_"+i+"_"+acut.idx[8];
                        }
                        else if(second_cut == '2')
                        {
                            hpost_total = "_syst_JES_Absolute_"+year2+"Down_cut_"+acut.idx.substr(0,1)+"_2_0_"+i+"_"+acut.idx[8];
                        }
                        else if(second_cut == '3')
                        {
                            hpost_total = "_syst_JES_AbsoluteUp_cut_"+acut.idx.substr(0,1)+"_3_0_"+i+"_"+acut.idx[8];
                        }
                        else if(second_cut == '4')
                        {
                            hpost_total = "_syst_JES_AbsoluteDown_cut_"+acut.idx.substr(0,1)+"_4_0_"+i+"_"+acut.idx[8];
                        }
                        else if(second_cut == '5')
                        {
                            hpost_total = "_syst_JES_BBEC1_"+year2+"Up_cut_"+acut.idx.substr(0,1)+"_5_0_"+i+"_"+acut.idx[8];
                        }
                        else if(second_cut == '6')
                        {
                            hpost_total = "_syst_JES_BBEC1_"+year2+"Down_cut_"+acut.idx.substr(0,1)+"_6_0_"+i+"_"+acut.idx[8];
                        }
                        else if(second_cut == '7')
                        {
                            hpost_total = "_syst_JES_BBEC1Up_cut_"+acut.idx.substr(0,1)+"_7_0_"+i+"_"+acut.idx[8];
                        }
                        else if(second_cut == '8')
                        {
                            hpost_total = "_syst_JES_BBEC1Down_cut_"+acut.idx.substr(0,1)+"_8_0_"+i+"_"+acut.idx[8];
                        }
                        else if(second_cut == '9')
                        {
                            hpost_total = "_syst_JES_EC2_"+year2+"Up_cut_"+acut.idx.substr(0,1)+"_9_0_"+i+"_"+acut.idx[8];
                        }
                        else if(second_cut == "10")
                        {
                            hpost_total = "_syst_JES_EC2_"+year2+"Down_cut_"+acut.idx.substr(0,1)+"_10_0_"+i+"_"+acut.idx[8];
                        }
                        else if(second_cut == "11")
                        {
                            hpost_total = "_syst_JES_EC2Up_cut_"+acut.idx.substr(0,1)+"_11_0_"+i+"_"+acut.idx[8];
                        }
                        else if(second_cut == "12")
                        {
                            hpost_total = "_syst_JES_EC2Down_cut_"+acut.idx.substr(0,1)+"_12_0_"+i+"_"+acut.idx[8];
                        }
                        else if(second_cut == "13")
                        {
                            hpost_total = "_syst_JES_FlavorQCDUp_cut_"+acut.idx.substr(0,1)+"_13_0_"+i+"_"+acut.idx[8];
                        }
                        else if(second_cut == "14")
                        {
                            hpost_total = "_syst_JES_FlavorQCDDown_cut_"+acut.idx.substr(0,1)+"_14_0_"+i+"_"+acut.idx[8];
                        }
                        else if(second_cut == "15")
                        {
                            hpost_total = "_syst_JES_HF_"+year2+"Up_cut_"+acut.idx.substr(0,1)+"_15_0_"+i+"_"+acut.idx[8];
                        }
                        else if(second_cut == "16")
                        {
                            hpost_total = "_syst_JES_HF_"+year2+"Down_cut_"+acut.idx.substr(0,1)+"_16_0_"+i+"_"+acut.idx[8];
                        }
                        else if(second_cut == "17")
                        {
                            hpost_total = "_syst_JES_HFUp_cut_"+acut.idx.substr(0,1)+"_17_0_"+i+"_"+acut.idx[8];
                        }
                        else if(second_cut == "18")
                        {
                            hpost_total = "_syst_JES_HFDown_cut_"+acut.idx.substr(0,1)+"_18_0_"+i+"_"+acut.idx[8];
                        }
                        else if(second_cut == "19")
                        {
                            hpost_total = "_syst_JES_RelativeSample_"+year2+"Up_cut_"+acut.idx.substr(0,1)+"_19_0_"+i+"_"+acut.idx[8];
                        }
                        else if(second_cut == "20")
                        {
                            hpost_total = "_syst_JES_RelativeSample_"+year2+"Down_cut_"+acut.idx.substr(0,1)+"_20_0_"+i+"_"+acut.idx[8];
                        }
                        else if(second_cut == "21")
                        {
                            hpost_total = "_syst_JES_RelativeBalUp_cut_"+acut.idx.substr(0,1)+"_21_0_"+i+"_"+acut.idx[8];
                        }
                        else if(second_cut == "22")
                        {
                            hpost_total = "_syst_JES_RelativeBalDown_cut_"+acut.idx.substr(0,1)+"_22_0_"+i+"_"+acut.idx[8];
                        }
                    
                        // prompt leptons
                        if(acut.idx[8] == '0')
                        {
                            hpost2 = "_cut_"+acut.idx.substr(0,1)+"_0_0_"+i+"_0_0";
                        }
                        else if(acut.idx[8] == '1')
                        {
                            hpost2 = "_cut_"+acut.idx.substr(0,1)+"_0_0_"+i+"_1_0";
                        }
                        for(auto &x : _hist1dinfovector)
	                    {
                            if(hpost == hpost2 && acut.idx == x.mincutstep)
	    		            {
		                        helper_1DHistCreator(std::string(x.hmodel.fName).erase(std::string(x.hmodel.fName).length()-9, 9)+hpost_total+"_0", std::string(x.hmodel.fTitle)+hpost_total+"_0", x.hmodel.fNbinsX, x.hmodel.fXLow, x.hmodel.fXUp, x.varname, x.weightname, rnext);
	                        }
	                    }

                        // flip leptons
                        if(acut.idx[8] == '0')
                        {
                            hpost2 = "_cut_"+acut.idx.substr(0,1)+"_0_0_"+i+"_0_1";
                        }
                        else if(acut.idx[8] == '1')
                        {
                            hpost2 = "_cut_"+acut.idx.substr(0,1)+"_0_0_"+i+"_1_1";
                        }
                        for(auto &x : _hist1dinfovector)
		                {
                            if(hpost == hpost2 && acut.idx == x.mincutstep)
                            {
                                helper_1DHistCreator(std::string(x.hmodel.fName).erase(std::string(x.hmodel.fName).length()-9, 9)+hpost_total+"_1", std::string(x.hmodel.fTitle)+hpost_total+"_1", x.hmodel.fNbinsX, x.hmodel.fXLow, x.hmodel.fXUp, x.varname, x.weightname, rnext);
	                        }
    	                }

                        // fake leptons
                        if(acut.idx[8] == '0')
                        {
                            hpost2 = "_cut_"+acut.idx.substr(0,1)+"_0_0_"+i+"_0_2";
                        }
                        else if(acut.idx[8] == '1')
                        {
                            hpost2 = "_cut_"+acut.idx.substr(0,1)+"_0_0_"+i+"_1_2";
                        }
                        for(auto &x : _hist1dinfovector)
                        {
                            if(hpost == hpost2 && acut.idx == x.mincutstep)
        	                {
                                helper_1DHistCreator(std::string(x.hmodel.fName).erase(std::string(x.hmodel.fName).length()-9, 9)+hpost_total+"_2", std::string(x.hmodel.fTitle)+hpost_total+"_2", x.hmodel.fNbinsX, x.hmodel.fXLow, x.hmodel.fXUp, x.varname, x.weightname, rnext);
    	                    }
                        }
                    }
                }
            }
        }
        _rnt.addDaughter(rnext, acut.idx, parent_idx);
	}
}

//================================Selected Object Definitions====================================//

void TprimeAnalyser::setupObjects(std::string year, std::string process, const std::vector<std::string>& weights)
{
    // Object selection will be defined in sequence.
    // Selected objects will be stored in new vectors.
    selectElectrons(year);
    selectMuons(year);
    selectLeptons();
    if(!_isData)
    {
        selectReconstructedLeptons();
    }
    selectJets(year);
    if(!_isData)
    {
        selectReconstructedJets();
    }
    if(year == "2018")
    {
        removeHEMEvents();
    }
    selectMET();
    defineMoreVars();
    if(!_isData)
    {
        this->calculateEvWeight(year, process, weights);
    }
}

void TprimeAnalyser::setupAnalysis(std::string year, std::string process, bool BTagEff, bool Hist, bool Combine, bool JEC, const std::vector<std::tuple<std::string, std::string, int, double, double, std::string>>& variables_MC, const std::vector<std::tuple<std::string, std::string, int, double, double, std::string>>& variables, const std::vector<std::string>& weights, const std::vector<std::string>& Cuts, const std::vector<std::string>& Masses, const std::vector<std::string>& getQuantiles_ptleptons, const std::vector<std::string>& getQuantiles_massjets, const std::vector<std::string>& getQuantiles_ptleptons_loosett1l, const std::vector<std::string>& getQuantiles_massjets_loosett1l, std::string second_cut)
{
    if(debug)
    {
        cout<< "================================//================================="<<endl;
        cout<< "Line : "<< __LINE__<<" Function : "<<__FUNCTION__<<endl;
        cout<< "================================//================================="<<endl;
    }
 	cout<<"year===="<< _year<< "==runtype=== "<< _runtype <<endl;
    string pt_cut = "30.0";
    defineJESUncRegion(pt_cut);
    defineCuts(year, process, BTagEff, Hist, Combine, JEC, getQuantiles_ptleptons, getQuantiles_massjets, getQuantiles_ptleptons_loosett1l, getQuantiles_massjets_loosett1l);
    if(Hist == 0)
    {
        storingVariables(year, JEC);
    }
    if(Hist == 1)
    {
        bookHists(year, process, BTagEff, Combine, JEC, variables_MC, variables, weights);
    }
    this->setupCuts_and_Hists(year, process, BTagEff, Hist, JEC, Cuts, Masses, second_cut);
    setupTree();
}
