import os
import sys
import ROOT
import math
from array import array

# Define the base directory where the dataset folders are located
date = '2025_02_09_15'
# year = '2016preVFP'
# year = '2016postVFP'
# year = '2017'
year = '2018'

Lumi = 59.8
if('2016preVFP' in year):
    Lumi = 19.5
elif('2016postVFP' in year):
    Lumi = 16.8
elif('2017' in year):
    Lumi = 41.5
Samples = [

    # 'Signal600',
    # 'Signal625',
    # 'Signal650',
    # 'Signal675',
    'Signal700',
    # 'Signal800',
    # 'Signal900',
    # 'Signal1000',
    # 'Signal1100',
    # 'Signal1200',
    'DY_10_50',
    'DY_4_50_70',
    'DY_4_50_100',
    'DY_4_50_200',
    'DY_4_50_400',
    'DY_4_50_600',
    'TT_2L',
    'TT_SL',
    'TT_Had',
    'ttW',
    'ttH',
    'ttZ',
    # 'ttZ_1_10',
    # 'ttZ_10',
    # 'ttg_2L',
    # 'ttg_SL',
    # 'ttg_Had',
    'tW_antitop',
    'tW_top',
    'tHq',
    'tHW',
    'tZq',
    'WW',
    'WW_DS',
    'WJets',
    'WH_plus',
    'WH_minus',
    'WZ',
    'ZH',
    'ZZ_2L2Nu',
    'ZZ_2L2Q',
    'ZZ_4L',
    'WWW',
    'WWZ',
    'WZZ',
    'ZZZ',
]

histo_deltaR_punzi = ROOT.TH1D("DeltaR_Leptons_punzi","\\Delta R (l_{1},l_{2}) (Punzi_{FOM}); \\Delta R (l_{1},l_{2});Punzi_{FOM};", 30, 0.0, 6.0)
histo_sumpt_punzi = ROOT.TH1D("Sum_Pt_Two_Leptons_punzi","p_{T}(l_{1}) + p_{T}(l_{2}) (Punzi_{FOM}); p_{T}(l_{1}) + p_{T}(l_{2}) (GeV);Punzi_{FOM};", 30, 0.0, 600.0)
punzi_deltaR_signal = [None] * 30
punzi_deltaR_background = [None] * 30
punzi_sumpt_signal = [None] * 30
punzi_sumpt_background = [None] * 30

for sample in Samples:

    inputFile = ROOT.TFile.Open('analyzed/' + date + '/Merged/' + sample + '_UL' + year + '.root', 'READ')

    genhisto = inputFile.Get('genweight_nocut;1')
    sumofweights = genhisto.GetEntries()*genhisto.GetMean()
    if(sample == 'Signal600'):
        scale = 176.4*Lumi/sumofweights
    elif(sample == 'Signal625'):
        scale = 148.9*Lumi/sumofweights
    elif(sample == 'Signal650'):
        scale = 121.3*Lumi/sumofweights
    elif(sample == 'Signal675'):
        scale = 105.0*Lumi/sumofweights
    elif(sample == 'Signal700'):
        scale = 88.6*Lumi/sumofweights
    elif(sample == 'Signal800'):
        scale = 45.9*Lumi/sumofweights
    elif(sample == 'Signal900'):
        scale = 25.1*Lumi/sumofweights
    elif(sample == 'Signal1000'):
        scale = 14.5*Lumi/sumofweights
    elif(sample == 'Signal1100'):
        scale = 8.67*Lumi/sumofweights
    elif(sample == 'Signal1200'):
        scale = 5.36*Lumi/sumofweights
    elif(sample == 'DY_10_50'):
        scale = 18610000*Lumi/sumofweights
    elif(sample == 'DY_4_50_70'):
        scale = 301200*Lumi/sumofweights
    elif(sample == 'DY_4_50_100'):
        scale = 224200*Lumi/sumofweights
    elif(sample == 'DY_4_50_200'):
        scale = 37200*Lumi/sumofweights
    elif(sample == 'DY_4_50_400'):
        scale = 3581*Lumi/sumofweights
    elif(sample == 'DY_4_50_600'):
        scale = 1124*Lumi/sumofweights
    elif(sample == 'TT_2L'):
        scale = 88341.9*Lumi/sumofweights
    elif(sample == 'TT_SL'):
        scale = 365457.4*Lumi/sumofweights
    elif(sample == 'TT_Had'):
        scale = 377960.7*Lumi/sumofweights
    elif(sample == 'ttW'):
        scale = 235*Lumi/sumofweights
    elif(sample == 'ttH'):
        scale = 211.2*Lumi/sumofweights
    elif(sample == 'ttZ'):
        scale = 859*Lumi/sumofweights
    elif('tW' in sample):
        scale = 39650*Lumi/sumofweights
    elif(sample == 'tHq'):
        scale = 70.96*Lumi/sumofweights
    elif(sample == 'tHW'):
        scale = 15.61*Lumi/sumofweights
    elif(sample == 'tZq'):
        scale = 73.58*Lumi/sumofweights
    elif(sample == 'WW'):
        scale = 12178*Lumi/sumofweights
    elif(sample == 'WW_DS'):
        scale = 223.2*Lumi/sumofweights
    elif(sample == 'WJets'):
        scale = 61334900*Lumi/sumofweights
    elif('WH' in sample):
        scale = 31.3*Lumi/sumofweights
    elif(sample == 'WZ'):
        scale = 4429.7*Lumi/sumofweights
    elif(sample == 'ZH'):
        scale = 185.8*Lumi/sumofweights
    elif(sample == 'ZZ_2L2Nu'):
        scale = 564.4*Lumi/sumofweights
    elif(sample == 'ZZ_2L2Q'):
        scale = 1973*Lumi/sumofweights
    elif(sample == 'ZZ_4L'):
        scale = 1256*Lumi/sumofweights
    elif(sample == 'WWW'):
        scale = 208.6*Lumi/sumofweights
    elif(sample == 'WWZ'):
        scale = 165.1*Lumi/sumofweights
    elif(sample == 'WZZ'):
        scale = 55.65*Lumi/sumofweights
    elif(sample == 'ZZZ'):
        scale = 14.76*Lumi/sumofweights
    print(sample)
    print(sumofweights)
    print(scale)

    histo_deltaR = inputFile.Get('DeltaR_Leptons_cut_3_0_0')
    histo_sumpt = inputFile.Get('Sum_Pt_Two_Leptons_cut_3_0_0_0')
    histo_deltaR.Scale(scale)
    histo_sumpt.Scale(scale)

    for i in range(29):
        histo_deltaR_value = histo_deltaR.GetBinContent(i)
        histo_sumpt_value = histo_sumpt.GetBinContent(i)
        if('Signal' in sample):
            punzi_deltaR_signal[i] = histo_deltaR_value
            punzi_sumpt_signal[i] = histo_sumpt_value
        else:
            if(sample == 'DY_10_50'):
                punzi_deltaR_background[i] = histo_deltaR_value
                punzi_sumpt_background[i] = histo_sumpt_value
            else:
                punzi_deltaR_background[i] += histo_deltaR_value
                punzi_sumpt_background[i] += histo_sumpt_value

for i in range(29):
    punzi_deltaR = punzi_deltaR_signal[i] / (math.sqrt(punzi_deltaR_background[i])+0.1+0.2*punzi_deltaR_background[i])
    histo_deltaR_punzi.SetBinContent(i,punzi_deltaR)
    punzi_sumpt = punzi_sumpt_signal[i] / (math.sqrt(punzi_sumpt_background[i])+0.1+0.2*punzi_sumpt_background[i])
    histo_sumpt_punzi.SetBinContent(i,punzi_sumpt)

c1 = ROOT.TCanvas('\\Delta R (l_{1},l_{2}) (Punzi_{FOM})', '\\Delta R (l_{1},l_{2}) (Punzi_{FOM})', 0, 30)
histo_deltaR_punzi.GetXaxis().SetRangeUser(0, 4.5)
histo_deltaR_punzi.GetYaxis().SetRangeUser(0, 0.06)
histo_deltaR_punzi.SetStats(0)
histo_deltaR_punzi.Draw('HIST')
c1.Modified()
c1.Update()

c2 = ROOT.TCanvas('p_{T}(l_{1}) + p_{T}(l_{2}) (Punzi_{FOM})', 'p_{T}(l_{1}) + p_{T}(l_{2}) (Punzi_{FOM})', 0, 30)
histo_sumpt_punzi.GetXaxis().SetRangeUser(0, 450)
histo_sumpt_punzi.GetYaxis().SetRangeUser(0, 0.065)
histo_sumpt_punzi.SetStats(0)
histo_sumpt_punzi.Draw('HIST')
c2.Modified()
c2.Update()

output_file_path = 'analyzed/' + date + '/Merged/Punzi.root'
output_file = ROOT.TFile.Open(output_file_path, 'RECREATE')
histo_deltaR_punzi.Write()
histo_sumpt_punzi.Write()
output_file.Close()
inputFile.Close()

print('Process completed.')
