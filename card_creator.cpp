#include "card_creator.hpp"
#include "sample.hpp"

#include <iostream>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>

#include "TH1F.h"
#include "TFile.h"

using namespace std;

unsigned int block_proc = 40; //30;
unsigned int block_syst = 34; //24;
unsigned int block_grp  = 25; //15;

/////////////////////////////////
// Constructor
/////////////////////////////////

Card::Card(){}

/////////////////////////////////
// Private methods
/////////////////////////////////

std::string Card::completeBlock(std::string const& word_p, unsigned int blockSize_p)
{ 

    int size = blockSize_p - word_p.size();
    // cout << "completeBlock " << word_p << " " <<size<<endl;

    if(size <= 0)
    {
        cout<<"Error with "+word_p+" block in datacard"<<endl;
        return "";
    }
    std::string spaces;
    for(int i = 0; i < size; ++i)
    {
        spaces += " ";
    }
    return word_p+spaces;
}

/////////////////////////////////
// Public methods
/////////////////////////////////

void Card::addSeparator()
{
    datacard += "--------------------------------------------------------------------------------------------- \n";
}

void Card::addLine(std::string const& line)
{
    datacard += line + '\n';
}

void Card::addGlobalParameter(namelist const& groupList_p,int numberOfBins)
{
    datacard += "imax "+std::to_string(numberOfBins)+" number of bins\n";
    datacard += "jmax "+std::to_string(groupList_p.size()-1)+" number of background processes\n";
    datacard += "kmax * number of nuisance parameters\n";
}

void Card::addInputsProcess(std::string const& directory_p, std::string const& rootfile_p)
{
    datacard += "shapes * * "+directory_p+rootfile_p+" $PROCESS $PROCESS_$SYSTEMATIC\n";
    datacard += "shapes sig * "+directory_p+rootfile_p+" $PROCESS $PROCESS_$SYSTEMATIC\n";
}

void Card::addChanels(std::string const& observable_p, double numberOfEvents_p)
{
    datacard += "bin "+observable_p+"\n";
    datacard += "observation "+std::to_string(numberOfEvents_p)+"\n";
}

void Card::addProcToCard(std::string const& observable_p, namelist const& groupList_p)
{

    std::string line0 = completeBlock("bin", block_proc);
    std::string line1 = completeBlock("process", block_proc);
    std::string line2 = line1;
    std::string line3 = completeBlock("rate", block_proc);

    for(int i = 0; i < int(groupList_p.size()); ++i)
    {
        line0 += completeBlock(observable_p, block_grp);
        line1 += completeBlock(groupList_p[i], block_grp);
        // if (groupList_p[0] == "signal")
        // {
        line2 += completeBlock(std::to_string(i), block_grp);
        // }
        // else
        // {
        //     line2 += completeBlock(std::to_string(i-1), block_grp);
        // }
        line3 += completeBlock("-1", block_grp);
    }
    datacard += line0+'\n'+line1+'\n'+line2+'\n'+line3+'\n'; 
}

void Card::addProcSystToCard(std::string const& systName_p, std::string const& shape_p, namelist const& groupList_p, std::string const& process_p, std::string const& value_p, std::string const& process2_p)
{

    //cout << "addProcSystToCard "<<systName_p<<" "<<shape_p<<" "<<process_p<< " "<<value_p<<endl;
    datacard += completeBlock(systName_p, block_syst) 
            + completeBlock(shape_p, block_proc-block_syst);

    for(size_t i = 0; i < groupList_p.size(); ++i)
    {
        if(groupList_p[i] == process_p || groupList_p[i] == process2_p)
        {
            datacard += completeBlock(value_p, block_grp);
        }
        else
        {
            datacard += completeBlock("-", block_grp);
        }
    }
    datacard += '\n';
}

void Card::addSystToCard(std::string const& systName_p, std::string const& shape_p, namelist const& groupList_p, std::string const& value_p, std::string region)
{
    datacard += completeBlock(systName_p, block_syst) 
             + completeBlock(shape_p, block_proc-block_syst);

    for(size_t i = 0; i < groupList_p.size(); ++i)
    {
        if(systName_p.find("syst_fsr_signal") != std::string::npos)
        {
            if(groupList_p[i] == "signal")
            {
                datacard += completeBlock(value_p, block_grp);
            }
            else
            {
                datacard += completeBlock("-", block_grp);
            }
        }
        else if(systName_p.find("syst_fsr_ttx") != std::string::npos)
        {
            if(groupList_p[i] == "ttx")
            {
                datacard += completeBlock(value_p, block_grp);
            }
            else
            {
                datacard += completeBlock("-", block_grp);
            }
        }
        else if(systName_p.find("syst_fsr_multibosons") != std::string::npos)
        {
            if(groupList_p[i] == "multibosons")
            {
                datacard += completeBlock(value_p, block_grp);
            }
            else
            {
                datacard += completeBlock("-", block_grp);
            }
        }
        else if(systName_p.find("syst_fsr_others") != std::string::npos)
        {
            if(groupList_p[i] == "others")
            {
                datacard += completeBlock(value_p, block_grp);
            }
            else
            {
                datacard += completeBlock("-", block_grp);
            }
        }
        else if(systName_p.find("syst_mr") != std::string::npos)
        {
            if(systName_p.find("syst_mr_flip") != std::string::npos && groupList_p[i] == "flip")
            {
                datacard += completeBlock(value_p, block_grp);
            }
            else if(systName_p.find("syst_mr_fake") != std::string::npos && groupList_p[i] == "fake")
            {
                datacard += completeBlock(value_p, block_grp);
            }
            else
            {
                datacard += completeBlock("-", block_grp);
            }
        }
        else if(systName_p.find("syst_tr") != std::string::npos)
        {
            if(systName_p.find("syst_tr_flip") != std::string::npos && groupList_p[i] == "flip")
            {
                datacard += completeBlock(value_p, block_grp);
            }
            else if(systName_p.find("syst_tr_fake") != std::string::npos && groupList_p[i] == "fake")
            {
                datacard += completeBlock(value_p, block_grp);
            }
            else
            {
                datacard += completeBlock("-", block_grp);
            }
        }
	    else if(groupList_p[i] != "flip" && groupList_p[i] != "fake")
        {
            if(systName_p == "syst_pt_top")
            {
                if(groupList_p[i] == "flip" || groupList_p[i] == "fake" || groupList_p[i] == "others")
                {
                    datacard += completeBlock(value_p, block_grp);
                }
                else
                {
                    datacard += completeBlock("-", block_grp);
                }
            }
            else
            {
                datacard += completeBlock(value_p, block_grp);
            }
        }
        else
        {
            datacard += completeBlock("-", block_grp);
        }
    }

    datacard += '\n';

}

void Card::addRateToCard(namelist const& groupList_p, namelist const& groupRateList_p, namelist const& systematicsRate_p, std::string year)
{
    for(size_t i = 0; i < groupRateList_p.size(); ++i)
    {
        if(i == 0)
        {
            i += 1;
        }
        // cout<<"i = "<<i<<endl;
        std::string line;
        if(groupRateList_p[i] == "flip" || groupRateList_p[i] == "fake")
        {
            line = completeBlock("r"+groupRateList_p[i]+"_"+year, block_syst) 
                 + completeBlock("lnN", block_proc-block_syst);
        }
        else
        {
            line = completeBlock("r"+groupRateList_p[i], block_syst) 
                 + completeBlock("lnN", block_proc-block_syst);
        }
        for(size_t j = 0; j < groupList_p.size(); ++j)
        {
            // cout<<"j = "<<j<<endl;
            if(i == j && groupRateList_p[i] != "tt")
            {
                line += completeBlock(systematicsRate_p[j], block_grp);
            }
            else if(i != 0 && i == j && groupRateList_p[i] == "tt")
            {
                j -= 1;
            }
            else
            {
                line += completeBlock("-", block_grp);
            }
        }
        datacard += line + '\n';
    }
}

void Card::printCard()
{
    cout << " >> datacard :" << endl;
    cout << datacard << endl;
}

void Card::saveCard(std::string const& name_p)
{
    std::ofstream file(name_p);
    file << datacard;
    file.close();
}

int card_creator(std::string year)
{

    // std::string cuts[12] = {"mumu_SR","mumu_MR_tt2l","mumu_MR_tt1l_SR","mumu_MR_tt1l_CR_ttX","mumu_CR_ttX","muel_SR","muel_MR_tt2l","muel_MR_tt1l_SR","muel_MR_tt1l_CR_ttX","muel_CR_ttX","elel_SR","elel_MR_tt2l","elel_MR_tt1l_SR","elel_MR_tt1l_CR_ttX","elel_CR_ttX"};
    std::string cuts[6] = {"mumu_SR","mumu_CR_ttX","muel_SR","muel_CR_ttX","elel_SR","elel_CR_ttX"};
    // std::string cuts[1] = {"mumu_CR_ttX"};
    std::string list_masses[10] = {"600", "625", "650", "675", "700", "800", "900", "1000", "1100", "1200"};
    // std::string list_masses[1] = {"700"};
    double Binning[6] = {0.015, 0.2, 0.01, 0.2, 0.001, 0.1}; // rebinning threshold for mumu_SR, mumu_CR_ttX, muel_SR, muel_CR_ttX, elel_SR, elel_CR_ttX for 2016 and 2017 (x2 for 2018)
    // double Binning[1] = {0.2};
    bool with_rebinning = 1;
    int iyear = -1;
    if(year == "2016")
    {
        iyear = 0;
        systematicList = systematicList_2016;
    }
    else if(year == "2017")
    {
        iyear = 1;
        systematicList = systematicList_2017;
    }
    else if(year == "2018")
    {
        iyear = 2;
        systematicList = systematicList_2018;
    }

    //Lumi flat uncertainties
    std::string lumi_flat_uncorr_inc[3] = {"1.01", "1.02", "1.015"};
    std::string lumi_flat_corr_inc_2016_2018[3] = {"1.006", "1.009", "1.02"};
    std::string lumi_flat_corr_inc_2017_2018[2] = {"1.006", "1.002"};

    for(int i = 0; i < sizeof(cuts)/sizeof(cuts[0]); i++)
    {
        cout<<"cuts[i] = "<<cuts[i]<<endl;
        std::string observable="mt_tprime";
        double binning = Binning[i];
        if(iyear == 2)
        {
            binning += binning;
        }
        std::string name = observable;

        for(int j = 0; j < sizeof(list_masses)/sizeof(list_masses[0]); j++)
        {
            cout<<"list_masses[j] = "<<list_masses[j]<<endl;
            Card datacard;
            namelist processList_modified = processList;
            namelist processRateList_modified = processRateList;
            namelist systematicRate_modified = systematicRate;
            
            string f_data_name = "./combine/" + list_masses[j] + "/" + year + "/inclusive/inputs/" + observable + "_" + cuts[i] + ".root";
            TFile* f_data = new TFile(f_data_name.c_str(), "UPDATE");
            TH1F* h_signal = (TH1F*) f_data->Get("signal");
            if(h_signal->GetEntries() <=0)
            {
                cout<<"ALERT: NO SIGNAL IN THE CHANNEL "<<cuts[i]<<" WITH THE MASS M = "<<list_masses[j]<<" GeV."<<endl;
            }
            int k = 1;
            for(int l = 1; l < processList.size(); l++)
            {
                cout<<"process = "<<processList[l].c_str()<<endl;
                TH1F* h_process = (TH1F*) f_data->Get(processList[l].c_str());
                if(h_process->GetEntries() <= 0)
                {
                    processList_modified.erase(processList_modified.begin() + k);
                    systematicRate_modified.erase(systematicRate_modified.begin() + k);
                    if(processList[l].find("tt2l") == std::string::npos && processList[l].find("tt1l") == std::string::npos && processList[l].find("tt0l") == std::string::npos)
                    {
                        processRateList_modified.erase(processRateList_modified.begin() + k);
                    }
                }
                else if(cuts[i].find("mumu") != std::string::npos and processList[l].find("flip") != std::string::npos)
                {
                    processList_modified.erase(processList_modified.begin() + k);
                    systematicRate_modified.erase(systematicRate_modified.begin() + k);
                    processRateList_modified.erase(processRateList_modified.begin() + k);
                }
                else
                {
                    k++;
                }
            }

            int nBins = 0;
            int firstNonEmptyBin = 0;
            int lastNonEmptyBin = 0;
            int newNBins = 0;
            double XMin = 0;
            double XMax = 0;
            double width = 0;
            double limitfirstnonEmptyBin = 0;
            double limitlastnonEmptyBin = 0;
            vector<double> newBinEdges;
            if(with_rebinning == 1)
            {
                if(cuts[i].find("CR_ttX") != std::string::npos)
                {
                    h_signal = (TH1F*) f_data->Get("ttx");
                }
                nBins = h_signal->GetNbinsX();
                firstNonEmptyBin = 1;
                lastNonEmptyBin = nBins;
                XMin = h_signal->GetXaxis()->GetXmin();
                XMax = h_signal->GetXaxis()->GetXmax();
                width = 150;
                limitfirstnonEmptyBin = XMin;
                limitlastnonEmptyBin = XMax;
                newBinEdges.push_back(XMin);
                for(int n = 1; n <= nBins; n++)
                {
                    double bin_content = h_signal->GetBinContent(n);
                    if(bin_content >= binning) break;

                    if(n < nBins)
                    {
                        double content_next = h_signal->GetBinContent(n+1);
                        double error_next = h_signal->GetBinError(n+1);
                        double content_curr = h_signal->GetBinContent(n);
                        double error_curr = h_signal->GetBinError(n);

                        h_signal->SetBinContent(n+1, content_next+content_curr);
                        h_signal->SetBinError(n+1, sqrt(error_next*error_next + error_curr*error_curr));

                        h_signal->SetBinContent(n, 0);
                        h_signal->SetBinError(n, 0);
                    }
                    limitfirstnonEmptyBin += width;
                }

                for(int n = nBins; n >= 1; n--)
                {
                    double bin_content = h_signal->GetBinContent(n);
                    if(bin_content >= binning) break;

                    if(n > 0)
                    {
                        double content_prev = h_signal->GetBinContent(n-1);
                        double error_prev = h_signal->GetBinError(n-1);
                        double content_curr = h_signal->GetBinContent(n);
                        double error_curr = h_signal->GetBinError(n);

                        h_signal->SetBinContent(n-1, content_prev+content_curr);
                        h_signal->SetBinError(n-1, sqrt(error_prev*error_prev + error_curr*error_curr));

                        h_signal->SetBinContent(n, 0);
                        h_signal->SetBinError(n, 0);
                    }
                    limitlastnonEmptyBin -= width;
                }

                for(int k = 1; k <= nBins; k++)
                {
                    if(h_signal->GetBinContent(k) > binning)
                    {
                        firstNonEmptyBin = k;
                        break;
                    }
                }

                for(int k = nBins; k >= 1; k--)
                {
                    if(h_signal->GetBinContent(k) > binning)
                    {
                        lastNonEmptyBin = k;
                        break;
                    }
                }
            
                newNBins = lastNonEmptyBin - firstNonEmptyBin + 1;
                for(int k = 1; k <= newNBins; k++)
                {
                    if(k == 1)
                    {
                        newBinEdges.push_back(limitfirstnonEmptyBin + width);
                    }
                    else if(k == newNBins)
                    {
                        newBinEdges.push_back(XMax);
                    }
                    else
                    {
                        newBinEdges.push_back(limitfirstnonEmptyBin + k*width);
                    }
                }
            
                TH1F* newh_signal;
                if(cuts[i].find("SR") != std::string::npos)
                {
                    newh_signal = new TH1F("signal", h_signal->GetTitle(), newNBins, &newBinEdges[0]);
                }
                if(cuts[i].find("CR_ttX") != std::string::npos)
                {
                    newh_signal = new TH1F("ttx", h_signal->GetTitle(), newNBins, &newBinEdges[0]);
                }
                for(int k = firstNonEmptyBin; k <= lastNonEmptyBin; ++k)
                {
                    int newBin = k - firstNonEmptyBin + 1;
                    newh_signal->SetBinContent(newBin, h_signal->GetBinContent(k));
                    newh_signal->SetBinError(newBin, h_signal->GetBinError(k));
                }
                newh_signal->Write(newh_signal->GetName(),TObject::kOverwrite);
            }

            for(int l = 0; l < processList_modified.size(); l++)
            {
                cout<<"process = "<<processList_modified[l].c_str()<<endl;
                TH1F* h_process = (TH1F*) f_data->Get((processList_modified[l]).c_str());
                if(with_rebinning == 1)
                {
                    if((processList_modified[l].find("signal") == std::string::npos && cuts[i].find("SR") != std::string::npos) || (processList_modified[l].find("signal") == std::string::npos && processList_modified[l].find("ttx") == std::string::npos && cuts[i].find("CR_ttX") != std::string::npos))
                    {
                        TH1F *newh_process = new TH1F((processList_modified[l]).c_str(), h_process->GetTitle(), newNBins, &newBinEdges[0]);
                        for(int n = 1; n <= firstNonEmptyBin-1; n++)
                        {
                            double content_next = h_process->GetBinContent(n+1);
                            double error_next = h_process->GetBinError(n+1);
                            double content_curr = h_process->GetBinContent(n);
                            double error_curr = h_process->GetBinError(n);
                            h_process->SetBinContent(n+1, content_next+content_curr);
                            h_process->SetBinError(n+1, sqrt(error_next*error_next + error_curr*error_curr));
                            h_process->SetBinContent(n, 0);
                            h_process->SetBinError(n, 0);
                        }

                        for(int n = nBins; n >= lastNonEmptyBin+1; n--)
                        {
                            double content_prev = h_process->GetBinContent(n-1);
                            double error_prev = h_process->GetBinError(n-1);
                            double content_curr = h_process->GetBinContent(n);
                            double error_curr = h_process->GetBinError(n);
                            h_process->SetBinContent(n-1, content_prev+content_curr);
                            h_process->SetBinError(n-1, sqrt(error_prev*error_prev + error_curr*error_curr));
                            h_process->SetBinContent(n, 0);
                            h_process->SetBinError(n, 0);
                        }

                        for(int k = firstNonEmptyBin; k <= lastNonEmptyBin; k++)
                        {
                            int newBin = k - firstNonEmptyBin + 1;
                            newh_process->SetBinContent(newBin, h_process->GetBinContent(k));
                            newh_process->SetBinError(newBin, h_process->GetBinError(k));
                        }
                        newh_process->Write(newh_process->GetName(),TObject::kOverwrite);
                
                        for(int n = 1; n < newh_process->GetNbinsX()+1; n++)
                        {
                            double bin_content = newh_process->GetBinContent(n);
                            if(bin_content <= 0)
                            {
                                newh_process->SetBinContent(n,0.000001);
                                newh_process->SetName((processList_modified[l]).c_str());
                                newh_process->Write(newh_process->GetName(),TObject::kOverwrite);
                            }
                        }
                    }
                    else if(processList_modified[l].find("ttx") != std::string::npos && cuts[i].find("CR_ttX") != std::string::npos)
                    {
                        h_process = (TH1F*) f_data->Get("signal");
                        TH1F *newh_process = new TH1F("signal", h_process->GetTitle(), newNBins, &newBinEdges[0]);
                        for(int n = 1; n <= firstNonEmptyBin-1; n++)
                        {
                            double content_next = h_process->GetBinContent(n+1);
                            double error_next = h_process->GetBinError(n+1);
                            double content_curr = h_process->GetBinContent(n);
                            double error_curr = h_process->GetBinError(n);
                            h_process->SetBinContent(n+1, content_next+content_curr);
                            h_process->SetBinError(n+1, sqrt(error_next*error_next + error_curr*error_curr));
                            h_process->SetBinContent(n, 0);
                            h_process->SetBinError(n, 0);
                        }

                        for(int n = nBins; n >= lastNonEmptyBin+1; n--)
                        {
                            double content_prev = h_process->GetBinContent(n-1);
                            double error_prev = h_process->GetBinError(n-1);
                            double content_curr = h_process->GetBinContent(n);
                            double error_curr = h_process->GetBinError(n);
                            h_process->SetBinContent(n-1, content_prev+content_curr);
                            h_process->SetBinError(n-1, sqrt(error_prev*error_prev + error_curr*error_curr));
                            h_process->SetBinContent(n, 0);
                            h_process->SetBinError(n, 0);
                        }

                        for(int k = firstNonEmptyBin; k <= lastNonEmptyBin; k++)
                        {
                            int newBin = k - firstNonEmptyBin + 1;
                            newh_process->SetBinContent(newBin, h_process->GetBinContent(k));
                            newh_process->SetBinError(newBin, h_process->GetBinError(k));
                        }
                        newh_process->Write(newh_process->GetName(),TObject::kOverwrite);
 
                        for(int n = 1; n < newh_process->GetNbinsX()+1; n++)
                        {
                            double bin_content = newh_process->GetBinContent(n);
                            if(bin_content <= 0)
                            {
                                newh_process->SetBinContent(n,0.000001);
                                newh_process->SetName("signal");
                                newh_process->Write(newh_process->GetName(),TObject::kOverwrite);
                            }
                        }
                    }
                }
                else
                {
                    for(int n = 1; n < h_process->GetNbinsX()+1; n++)
                    {
                        double bin_content = h_process->GetBinContent(n);
                        if(bin_content <= 0)
                        {
                            // cout<<"h_process = "<<h_process->GetName()<<", bin = "<<n<<", bin_content = "<<bin_content<<endl;
                            h_process->SetBinContent(n,0.000001);
                            h_process->SetName((processList_modified[l]).c_str());
                            h_process->Write(h_process->GetName(),TObject::kOverwrite);
                        }
                    }
                }

                for(int m = 0; m < systematicList.size(); m++)
                {
                    cout<<"systematic = "<<systematicList[m]<<endl;
                    if((processList_modified[l] != "flip" && processList_modified[l] != "fake" && systematicList[m].find("fsr") == std::string::npos && systematicList[m].find("flip") == std::string::npos && systematicList[m].find("fake") == std::string::npos) || (processList_modified[l] == "signal" && systematicList[m].find("syst_fsr_signal") != std::string::npos) || (processList_modified[l] == "ttx" && systematicList[m].find("syst_fsr_ttx") != std::string::npos) || (processList_modified[l] == "multibosons" && systematicList[m].find("syst_fsr_multibosons") != std::string::npos) || (processList_modified[l] == "others" && systematicList[m].find("syst_fsr_others") != std::string::npos) || (processList_modified[l] == "flip" && systematicList[m].find("flip") != std::string::npos) || (processList_modified[l] == "fake" && ((systematicList[m].find("fake_SR") != std::string::npos && cuts[i].find("SR") != std::string::npos) || (systematicList[m].find("fake_CR_ttX") != std::string::npos && cuts[i].find("CR_ttX") != std::string::npos))))
                    {
                        TH1F* h_process = (TH1F*) f_data->Get((processList_modified[l]).c_str());
                        TH1F* h_processUp = (TH1F*) f_data->Get((processList_modified[l]+"_"+systematicList[m]+"Up").c_str());
                        TH1F* h_processDown = (TH1F*) f_data->Get((processList_modified[l]+"_"+systematicList[m]+"Down").c_str());
                        if(with_rebinning == 1)
                        {
                            if(cuts[i].find("SR") != std::string::npos || (processList_modified[l].find("signal") == std::string::npos && cuts[i].find("CR_ttX") != std::string::npos))
                            {
                                TH1F *newh_processUp = new TH1F((processList_modified[l]+"_"+systematicList[m]+"Up").c_str(), h_processUp->GetTitle(), newNBins, &newBinEdges[0]);
                                TH1F *newh_processDown = new TH1F((processList_modified[l]+"_"+systematicList[m]+"Down").c_str(), h_processDown->GetTitle(), newNBins, &newBinEdges[0]);
                                
                                for(int n = 1; n <= firstNonEmptyBin-1; n++)
                                {
                                    double content_next = h_processUp->GetBinContent(n+1);
                                    double error_next = h_processUp->GetBinError(n+1);
                                    double content_curr = h_processUp->GetBinContent(n);
                                    double error_curr = h_processUp->GetBinError(n);

                                    h_processUp->SetBinContent(n+1, content_next+content_curr);
                                    h_processUp->SetBinError(n+1, sqrt(error_next*error_next + error_curr*error_curr));
                                    h_processUp->SetBinContent(n, 0);
                                    h_processUp->SetBinError(n, 0);
                                }
                                for(int n = nBins; n >= lastNonEmptyBin+1; n--)
                                {
                                    double content_prev = h_processUp->GetBinContent(n-1);
                                    double error_prev = h_processUp->GetBinError(n-1);
                                    double content_curr = h_processUp->GetBinContent(n);
                                    double error_curr = h_processUp->GetBinError(n);

                                    h_processUp->SetBinContent(n-1, content_prev+content_curr);
                                    h_processUp->SetBinError(n-1, sqrt(error_prev*error_prev + error_curr*error_curr));
                                    h_processUp->SetBinContent(n, 0);
                                    h_processUp->SetBinError(n, 0);
                                }
                                for(int k = firstNonEmptyBin; k <= lastNonEmptyBin; k++)
                                {
                                    int newBin = k - firstNonEmptyBin + 1;
                                    newh_processUp->SetBinContent(newBin, h_processUp->GetBinContent(k));
                                    newh_processUp->SetBinError(newBin, h_processUp->GetBinError(k));
                                }
                                newh_processUp->Write(newh_processUp->GetName(),TObject::kOverwrite);

                                for(int n = 1; n <= firstNonEmptyBin-1; n++)
                                {
                                    double content_next = h_processDown->GetBinContent(n+1);
                                    double error_next = h_processDown->GetBinError(n+1);
                                    double content_curr = h_processDown->GetBinContent(n);
                                    double error_curr = h_processDown->GetBinError(n);

                                    h_processDown->SetBinContent(n+1, content_next+content_curr);
                                    h_processDown->SetBinError(n+1, sqrt(error_next*error_next + error_curr*error_curr));
                                    h_processDown->SetBinContent(n, 0);
                                    h_processDown->SetBinError(n, 0);
                                }
                                for(int n = nBins; n >= lastNonEmptyBin+1; n--)
                                {
                                    double content_prev = h_processDown->GetBinContent(n-1);
                                    double error_prev = h_processDown->GetBinError(n-1);
                                    double content_curr = h_processDown->GetBinContent(n);
                                    double error_curr = h_processDown->GetBinError(n);

                                    h_processDown->SetBinContent(n-1, content_prev+content_curr);
                                    h_processDown->SetBinError(n-1, sqrt(error_prev*error_prev + error_curr*error_curr));
                                    h_processDown->SetBinContent(n, 0);
                                    h_processDown->SetBinError(n, 0);
                                }
                                for(int k = firstNonEmptyBin; k <= lastNonEmptyBin; k++)
                                {
                                    int newBin = k - firstNonEmptyBin + 1;
                                    newh_processDown->SetBinContent(newBin, h_processDown->GetBinContent(k));
                                    newh_processDown->SetBinError(newBin, h_processDown->GetBinError(k));
                                }
                                newh_processDown->Write(newh_processDown->GetName(),TObject::kOverwrite);

                                for(int n = 1; n < h_process->GetNbinsX()+1; n++)
                                {
                                    double bin_content = h_process->GetBinContent(n);
                                    if(bin_content <= 0.000001)
                                    {
                                        newh_processUp->SetBinContent(n,0.000001);
                                        newh_processUp->SetName((processList_modified[l]+"_"+systematicList[m]+"Up").c_str());
                                        newh_processUp->Write(newh_processUp->GetName(),TObject::kOverwrite);
                                        newh_processDown->SetBinContent(n,0.000001);
                                        newh_processDown->SetName((processList_modified[l]+"_"+systematicList[m]+"Down").c_str());
                                        newh_processDown->Write(newh_processDown->GetName(),TObject::kOverwrite);
                                    }
                                    else
                                    {
                                        double bin_contentUp = newh_processUp->GetBinContent(n);
                                        double bin_contentDown = newh_processDown->GetBinContent(n);
                                        if(bin_contentUp <= 0)
                                        {
                                            newh_processUp->SetBinContent(n,bin_content-(bin_contentDown-bin_content));
                                            newh_processUp->SetName((processList_modified[l]+"_"+systematicList[m]+"Up").c_str());
                                            newh_processUp->Write(newh_processUp->GetName(),TObject::kOverwrite);
                                            bin_contentUp = newh_processUp->GetBinContent(n);
                                            if(bin_contentUp <= 0)
                                            {
                                                newh_processUp->SetBinContent(n,0.000001);
                                                newh_processUp->SetName((processList_modified[l]+"_"+systematicList[m]+"Up").c_str());
                                                newh_processUp->Write(newh_processUp->GetName(),TObject::kOverwrite);
                                            }
                                        }
                                        if(bin_contentDown <= 0)
                                        {
                                            newh_processDown->SetBinContent(n,bin_content-(bin_contentUp-bin_content));
                                            newh_processDown->SetName((processList_modified[l]+"_"+systematicList[m]+"Down").c_str());
                                            newh_processDown->Write(newh_processDown->GetName(),TObject::kOverwrite);
                                            bin_contentDown = newh_processDown->GetBinContent(n);
                                            if(bin_contentDown <= 0)
                                            {
                                                newh_processDown->SetBinContent(n,0.000001);
                                                newh_processDown->SetName((processList_modified[l]+"_"+systematicList[m]+"Down").c_str());
                                                newh_processDown->Write(newh_processDown->GetName(),TObject::kOverwrite);
                                            }
                                        }
                                    }
                                }
                            }

                            if(processList_modified[l].find("ttx") != std::string::npos && cuts[i].find("CR_ttX") != std::string::npos && systematicList[m].find("syst_fsr_ttx") == std::string::npos)
                            {
                                h_process = (TH1F*) f_data->Get("signal");
                                h_processUp = (TH1F*) f_data->Get(("signal_"+systematicList[m]+"Up").c_str());
                                h_processDown = (TH1F*) f_data->Get(("signal_"+systematicList[m]+"Down").c_str());
                                TH1F *newh_processUp = new TH1F(("signal_"+systematicList[m]+"Up").c_str(), h_processUp->GetTitle(), newNBins, &newBinEdges[0]);
                                TH1F *newh_processDown = new TH1F(("signal_"+systematicList[m]+"Down").c_str(), h_processDown->GetTitle(), newNBins, &newBinEdges[0]);
                                
                                for(int n = 1; n <= firstNonEmptyBin-1; n++)
                                {
                                    double content_next = h_processUp->GetBinContent(n+1);
                                    double error_next = h_processUp->GetBinError(n+1);
                                    double content_curr = h_processUp->GetBinContent(n);
                                    double error_curr = h_processUp->GetBinError(n);
                                    
                                    h_processUp->SetBinContent(n+1, content_next+content_curr);
                                    h_processUp->SetBinError(n+1, sqrt(error_next*error_next + error_curr*error_curr));
                                    h_processUp->SetBinContent(n, 0);
                                    h_processUp->SetBinError(n, 0);
                                }
                                for(int n = nBins; n >= lastNonEmptyBin+1; n--)
                                {
                                    double content_prev = h_processUp->GetBinContent(n-1);
                                    double error_prev = h_processUp->GetBinError(n-1);
                                    double content_curr = h_processUp->GetBinContent(n);
                                    double error_curr = h_processUp->GetBinError(n);
                                    
                                    h_processUp->SetBinContent(n-1, content_prev+content_curr);
                                    h_processUp->SetBinError(n-1, sqrt(error_prev*error_prev + error_curr*error_curr));
                                    h_processUp->SetBinContent(n, 0);
                                    h_processUp->SetBinError(n, 0);
                                }
                                for(int k = firstNonEmptyBin; k <= lastNonEmptyBin; k++)
                                {
                                    int newBin = k - firstNonEmptyBin + 1;
                                    newh_processUp->SetBinContent(newBin, h_processUp->GetBinContent(k));
                                    newh_processUp->SetBinError(newBin, h_processUp->GetBinError(k));
                                }
                                newh_processUp->Write(newh_processUp->GetName(),TObject::kOverwrite);

                                for(int n = 1; n <= firstNonEmptyBin-1; n++)
                                {
                                    double content_next = h_processDown->GetBinContent(n+1);
                                    double error_next = h_processDown->GetBinError(n+1);
                                    double content_curr = h_processDown->GetBinContent(n);
                                    double error_curr = h_processDown->GetBinError(n);

                                    h_processDown->SetBinContent(n+1, content_next+content_curr);
                                    h_processDown->SetBinError(n+1, sqrt(error_next*error_next + error_curr*error_curr));
                                    h_processDown->SetBinContent(n, 0);
                                    h_processDown->SetBinError(n, 0);
                                }
                                for(int n = nBins; n >= lastNonEmptyBin+1; n--)
                                {
                                    double content_prev = h_processDown->GetBinContent(n-1);
                                    double error_prev = h_processDown->GetBinError(n-1);
                                    double content_curr = h_processDown->GetBinContent(n);
                                    double error_curr = h_processDown->GetBinError(n);

                                    h_processDown->SetBinContent(n-1, content_prev+content_curr);
                                    h_processDown->SetBinError(n-1, sqrt(error_prev*error_prev + error_curr*error_curr));
                                    h_processDown->SetBinContent(n, 0);
                                    h_processDown->SetBinError(n, 0);
                                }
                                for(int k = firstNonEmptyBin; k <= lastNonEmptyBin; k++)
                                {
                                    int newBin = k - firstNonEmptyBin + 1;
                                    newh_processDown->SetBinContent(newBin, h_processDown->GetBinContent(k));
                                    newh_processDown->SetBinError(newBin, h_processDown->GetBinError(k));
                                }
                                newh_processDown->Write(newh_processDown->GetName(),TObject::kOverwrite);
                        
                                for(int n = 1; n < h_process->GetNbinsX()+1; n++)
                                {
                                    double bin_content = h_process->GetBinContent(n);
                                    if(bin_content <= 0.000001)
                                    {
                                        newh_processUp->SetBinContent(n,0.000001);
                                        newh_processUp->SetName((processList_modified[l]+"_"+systematicList[m]+"Up").c_str());
                                        newh_processUp->Write(newh_processUp->GetName(),TObject::kOverwrite);
                                        newh_processDown->SetBinContent(n,0.000001);
                                        newh_processDown->SetName((processList_modified[l]+"_"+systematicList[m]+"Down").c_str());
                                        newh_processDown->Write(newh_processDown->GetName(),TObject::kOverwrite);
                                    }
                                    else
                                    {
                                        double bin_contentUp = newh_processUp->GetBinContent(n);
                                        double bin_contentDown = newh_processDown->GetBinContent(n);
                                        if(bin_contentUp <= 0)
                                        {
                                            newh_processUp->SetBinContent(n,bin_content-(bin_contentDown-bin_content));
                                            newh_processUp->SetName((processList_modified[l]+"_"+systematicList[m]+"Up").c_str());
                                            newh_processUp->Write(newh_processUp->GetName(),TObject::kOverwrite);
                                            bin_contentUp = newh_processUp->GetBinContent(n);
                                            if(bin_contentUp <= 0)
                                            {
                                                newh_processUp->SetBinContent(n,0.000001);
                                                newh_processUp->SetName((processList_modified[l]+"_"+systematicList[m]+"Up").c_str());
                                                newh_processUp->Write(newh_processUp->GetName(),TObject::kOverwrite);
                                            }
                                        }
                                        if(bin_contentDown <= 0)
                                        {
                                            newh_processDown->SetBinContent(n,bin_content-(bin_contentUp-bin_content));
                                            newh_processDown->SetName((processList_modified[l]+"_"+systematicList[m]+"Down").c_str());
                                            newh_processDown->Write(newh_processDown->GetName(),TObject::kOverwrite);
                                            bin_contentDown = newh_processDown->GetBinContent(n);
                                            if(bin_contentDown <= 0)
                                            {
                                                newh_processDown->SetBinContent(n,0.000001);
                                                newh_processDown->SetName((processList_modified[l]+"_"+systematicList[m]+"Down").c_str());
                                                newh_processDown->Write(newh_processDown->GetName(),TObject::kOverwrite);
                                            }
                                        }
                                    }
                                }
                            }

                            if(processList_modified[l].find("ttx") != std::string::npos && cuts[i].find("CR_ttX") != std::string::npos && systematicList[m].find("syst_fsr_ttx") != std::string::npos)
                            {
                                h_process = (TH1F*) f_data->Get("signal");
                                h_processUp = (TH1F*) f_data->Get("signal_syst_fsr_signalUp");
                                h_processDown = (TH1F*) f_data->Get("signal_syst_fsr_signalDown");
                                TH1F *newh_processUp = new TH1F("signal_syst_fsr_signalUp", h_processUp->GetTitle(), newNBins, &newBinEdges[0]);
                                TH1F *newh_processDown = new TH1F("signal_syst_fsr_signalDown", h_processDown->GetTitle(), newNBins, &newBinEdges[0]);
                                
                                for(int n = 1; n <= firstNonEmptyBin-1; n++)
                                {
                                    double content_next = h_processUp->GetBinContent(n+1);
                                    double error_next = h_processUp->GetBinError(n+1);
                                    double content_curr = h_processUp->GetBinContent(n);
                                    double error_curr = h_processUp->GetBinError(n);
                                    cout<<"content_next= "<<content_next<<", content_curr = "<<content_curr<<endl;

                                    h_processUp->SetBinContent(n+1, content_next+content_curr);
                                    h_processUp->SetBinError(n+1, sqrt(error_next*error_next + error_curr*error_curr));
                                    h_processUp->SetBinContent(n, 0);
                                    h_processUp->SetBinError(n, 0);
                                }
                                for(int n = nBins; n >= lastNonEmptyBin+1; n--)
                                {
                                    double content_prev = h_processUp->GetBinContent(n-1);
                                    double error_prev = h_processUp->GetBinError(n-1);
                                    double content_curr = h_processUp->GetBinContent(n);
                                    double error_curr = h_processUp->GetBinError(n);
                                    
                                    h_processUp->SetBinContent(n-1, content_prev+content_curr);
                                    h_processUp->SetBinError(n-1, sqrt(error_prev*error_prev + error_curr*error_curr));
                                    h_processUp->SetBinContent(n, 0);
                                    h_processUp->SetBinError(n, 0);
                                }
                                for(int k = firstNonEmptyBin; k <= lastNonEmptyBin; k++)
                                {
                                    int newBin = k - firstNonEmptyBin + 1;
                                    newh_processUp->SetBinContent(newBin, h_processUp->GetBinContent(k));
                                    newh_processUp->SetBinError(newBin, h_processUp->GetBinError(k));
                                }
                                newh_processUp->Write(newh_processUp->GetName(),TObject::kOverwrite);

                                for(int n = 1; n <= firstNonEmptyBin-1; n++)
                                {
                                    double content_next = h_processDown->GetBinContent(n+1);
                                    double error_next = h_processDown->GetBinError(n+1);
                                    double content_curr = h_processDown->GetBinContent(n);
                                    double error_curr = h_processDown->GetBinError(n);

                                    h_processDown->SetBinContent(n+1, content_next+content_curr);
                                    h_processDown->SetBinError(n+1, sqrt(error_next*error_next + error_curr*error_curr));
                                    h_processDown->SetBinContent(n, 0);
                                    h_processDown->SetBinError(n, 0);
                                }
                                for(int n = nBins; n >= lastNonEmptyBin+1; n--)
                                {
                                    double content_prev = h_processDown->GetBinContent(n-1);
                                    double error_prev = h_processDown->GetBinError(n-1);
                                    double content_curr = h_processDown->GetBinContent(n);
                                    double error_curr = h_processDown->GetBinError(n);

                                    h_processDown->SetBinContent(n-1, content_prev+content_curr);
                                    h_processDown->SetBinError(n-1, sqrt(error_prev*error_prev + error_curr*error_curr));
                                    h_processDown->SetBinContent(n, 0);
                                    h_processDown->SetBinError(n, 0);
                                }
                                for(int k = firstNonEmptyBin; k <= lastNonEmptyBin; k++)
                                {
                                    int newBin = k - firstNonEmptyBin + 1;
                                    newh_processDown->SetBinContent(newBin, h_processDown->GetBinContent(k));
                                    newh_processDown->SetBinError(newBin, h_processDown->GetBinError(k));
                                }
                                newh_processDown->Write(newh_processDown->GetName(),TObject::kOverwrite);
                        
                                for(int n = 1; n < h_process->GetNbinsX()+1; n++)
                                {
                                    double bin_content = h_process->GetBinContent(n);
                                    if(bin_content <= 0.000001)
                                    {
                                        newh_processUp->SetBinContent(n,0.000001);
                                        newh_processUp->SetName((processList_modified[l]+"_"+systematicList[m]+"Up").c_str());
                                        newh_processUp->Write(newh_processUp->GetName(),TObject::kOverwrite);
                                        newh_processDown->SetBinContent(n,0.000001);
                                        newh_processDown->SetName((processList_modified[l]+"_"+systematicList[m]+"Down").c_str());
                                        newh_processDown->Write(newh_processDown->GetName(),TObject::kOverwrite);
                                    }
                                    else
                                    {
                                        double bin_contentUp = newh_processUp->GetBinContent(n);
                                        double bin_contentDown = newh_processDown->GetBinContent(n);
                                        if(bin_contentUp <= 0)
                                        {
                                            newh_processUp->SetBinContent(n,bin_content-(bin_contentDown-bin_content));
                                            newh_processUp->SetName((processList_modified[l]+"_"+systematicList[m]+"Up").c_str());
                                            newh_processUp->Write(newh_processUp->GetName(),TObject::kOverwrite);
                                            bin_contentUp = newh_processUp->GetBinContent(n);
                                            if(bin_contentUp <= 0)
                                            {
                                                newh_processUp->SetBinContent(n,0.000001);
                                                newh_processUp->SetName((processList_modified[l]+"_"+systematicList[m]+"Up").c_str());
                                                newh_processUp->Write(newh_processUp->GetName(),TObject::kOverwrite);
                                            }
                                        }
                                        if(bin_contentDown <= 0)
                                        {
                                            newh_processDown->SetBinContent(n,bin_content-(bin_contentUp-bin_content));
                                            newh_processDown->SetName((processList_modified[l]+"_"+systematicList[m]+"Down").c_str());
                                            newh_processDown->Write(newh_processDown->GetName(),TObject::kOverwrite);
                                            bin_contentDown = newh_processDown->GetBinContent(n);
                                            if(bin_contentDown <= 0)
                                            {
                                                newh_processDown->SetBinContent(n,0.000001);
                                                newh_processDown->SetName((processList_modified[l]+"_"+systematicList[m]+"Down").c_str());
                                                newh_processDown->Write(newh_processDown->GetName(),TObject::kOverwrite);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            for(int n = 1; n < h_process->GetNbinsX()+1; n++)
                            {
                                double bin_content = h_process->GetBinContent(n);
                                if(bin_content <= 0.000001)
                                {
                                    h_processUp->SetBinContent(n,0.000001);
                                    h_processUp->SetName((processList_modified[l]+"_"+systematicList[m]+"Up").c_str());
                                    h_processUp->Write(h_processUp->GetName(),TObject::kOverwrite);
                                    h_processDown->SetBinContent(n,0.000001);
                                    h_processDown->SetName((processList_modified[l]+"_"+systematicList[m]+"Down").c_str());
                                    h_processDown->Write(h_processDown->GetName(),TObject::kOverwrite);
                                }
                                else
                                {
                                    double bin_contentUp = h_processUp->GetBinContent(n);
                                    double bin_contentDown = h_processDown->GetBinContent(n);
                                    if(bin_contentUp <= 0)
                                    {
                                        h_processUp->SetBinContent(n,bin_content-(bin_contentDown-bin_content));
                                        h_processUp->SetName((processList_modified[l]+"_"+systematicList[m]+"Up").c_str());
                                        h_processUp->Write(h_processUp->GetName(),TObject::kOverwrite);
                                        bin_contentUp = h_processUp->GetBinContent(n);
                                        if(bin_contentUp <= 0)
                                        {
                                            h_processUp->SetBinContent(n,0.000001);
                                            h_processUp->SetName((processList_modified[l]+"_"+systematicList[m]+"Up").c_str());
                                            h_processUp->Write(h_processUp->GetName(),TObject::kOverwrite);
                                        }
                                    }
                                    if(bin_contentDown <= 0)
                                    {
                                        h_processDown->SetBinContent(n,bin_content-(bin_contentUp-bin_content));
                                        h_processDown->SetName((processList_modified[l]+"_"+systematicList[m]+"Down").c_str());
                                        h_processDown->Write(h_processDown->GetName(),TObject::kOverwrite);
                                        bin_contentDown = h_processDown->GetBinContent(n);
                                        if(bin_contentDown <= 0)
                                        {
                                            h_processDown->SetBinContent(n,0.000001);
                                            h_processDown->SetName((processList_modified[l]+"_"+systematicList[m]+"Down").c_str());
                                            h_processDown->Write(h_processDown->GetName(),TObject::kOverwrite);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            TH1F* h_data = (TH1F*) f_data->Get("data_obs");
            double numberOfEvents = 0;
            if(with_rebinning == 1)
            {
                TH1F* newh_data = new TH1F("data_obs", h_data->GetTitle(), newNBins, &newBinEdges[0]);
                for(int k = firstNonEmptyBin; k <= lastNonEmptyBin; ++k)
                {
                    int newBin = k - firstNonEmptyBin + 1;
                    newh_data->SetBinContent(newBin, h_data->GetBinContent(k));
                    newh_data->SetBinError(newBin, h_data->GetBinError(k));
                }
                newh_data->Write(newh_data->GetName(),TObject::kOverwrite);
                numberOfEvents = newh_data->Integral();
                cout << "Number of events in data : " << numberOfEvents << endl;
            }
            else
            {
                numberOfEvents = h_data->Integral();
                cout << "Number of events in data : " << numberOfEvents << endl;
            }
            datacard.addGlobalParameter(processList_modified);
            datacard.addSeparator();
            cout << "Open input file"<<endl;
            datacard.addInputsProcess("./inputs/"+list_masses[j]+"/"+year+"/", name+"_"+cuts[i]+".root");
            datacard.addSeparator();
            cout << observable << " " << numberOfEvents <<endl;
            datacard.addChanels(observable, numberOfEvents);
            datacard.addSeparator();
            datacard.addProcToCard(observable, processList_modified);
            datacard.addSeparator();
            cout << "Bkgd norm" << endl;
            datacard.addRateToCard(processList_modified, processRateList_modified, systematicRate_modified, year);
            cout << "Weights nuisances (exp)" <<endl;

            for(std::string const& syst : systematicList)
            {
                if(cuts[i].find("mumu") != std::string::npos && syst.find("flip") != std::string::npos)
                {
                    continue;
                }
                // else
                // {
                //     datacard.addSystToCard(syst, "shape", processList_modified);
                // }
                else if(cuts[i].find("SR") != std::string::npos)
                {
                    if(syst.find("CR_ttX") != std::string::npos)
                    {
                        continue;
                    }
                    else
                    {
                        datacard.addSystToCard(syst, "shape", processList_modified, "1", "SR");
                    }
                }
                else if(cuts[i].find("CR_ttX") != std::string::npos)
                {
                    if(syst.find("SR") != std::string::npos)
                    {
                        continue;
                    }
                    else
                    {
                        datacard.addSystToCard(syst, "shape", processList_modified, "1", "CR_ttX");
                    }
                }
            }

            cout << "Rate nuisances"<<endl;
            datacard.addSystToCard("lumi_uncor_"+year, "lnN", processList_modified, lumi_flat_uncorr_inc[iyear]);
            datacard.addSystToCard("lumi_cor_2016_2018", "lnN", processList_modified, lumi_flat_corr_inc_2016_2018[iyear]);
            if(iyear == 1 || iyear == 2)
            {
                datacard.addSystToCard("lumi_cor_2017_2018", "lnN", processList_modified, lumi_flat_corr_inc_2017_2018[iyear-1]);
            }
            datacard.addSeparator();
            datacard.addLine("* autoMCStats 0");
            datacard.saveCard("combine/"+list_masses[j]+"/"+year+"/inclusive/inputs/"+name+"_datacard_"+cuts[i]+".txt");
            // datacard.printCard();
            f_data->Close();
        }
    }

    cout << "Finished !!!" << endl;
    return 0;
}