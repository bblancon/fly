import os
import sys
import ROOT
from array import array

# Define the base directory where the dataset folders are located
date = '2024_12_11_10'
year = '2016preVFP'
# year = '2016postVFP'
# year = '2017'
# year = '2018'
ROOT.TH1.SetDefaultSumw2()

inputFile_MC_numerator = ROOT.TFile.Open('analyzed/' + date + '/Merged/Signal700_UL' + year + '_numerator.root', 'READ')
inputFile_MC_denominator = ROOT.TFile.Open('analyzed/' + date + '/Merged/Signal700_UL' + year + '_denominator.root', 'READ')
inputFile_Data_numerator = ROOT.TFile.Open('analyzed/' + date + '/Merged/Data_UL' + year + '_numerator.root', 'READ')
inputFile_Data_denominator = ROOT.TFile.Open('analyzed/' + date + '/Merged/Data_UL' + year + '_denominator.root', 'READ')
# inputFile_Data_numerator = ROOT.TFile.Open('analyzed/' + date + '/Merged/Signal700_UL' + year + '_numerator.root', 'READ')
# inputFile_Data_denominator = ROOT.TFile.Open('analyzed/' + date + '/Merged/Signal700_UL' + year + '_denominator.root', 'READ')

histo_MC_numerator_elec = inputFile_MC_numerator.Get('Leading_Pt_Leptons_cut_2_0')
histo_MC_denominator_elec = inputFile_MC_denominator.Get('Leading_Pt_Leptons_cut_2_0')
histo_Data_numerator_elec = inputFile_Data_numerator.Get('Leading_Pt_Leptons_cut_2_0')
histo_Data_denominator_elec = inputFile_Data_denominator.Get('Leading_Pt_Leptons_cut_2_0')
# if '2016' in year:
#     histo_Data_numerator_elec = inputFile_Data_numerator.Get('Leading_Pt_Leptons_cut_1_0')
#     histo_Data_denominator_elec = inputFile_Data_denominator.Get('Leading_Pt_Leptons_cut_1_0')
histo_MC_elec = histo_MC_numerator_elec.Clone('Trigger_efficiency_MC_elec_UL' +year)
histo_MC_denominator_clone_elec = histo_MC_denominator_elec.Clone()
histo_Data_elec = histo_Data_numerator_elec.Clone('Trigger_efficiency_Data_elec_UL' +year)
histo_Data_denominator_clone_elec = histo_Data_denominator_elec.Clone()

histo_MC_numerator_muons = inputFile_MC_numerator.Get('Leading_Pt_Leptons_cut_0_0')
histo_MC_denominator_muons = inputFile_MC_denominator.Get('Leading_Pt_Leptons_cut_0_0')
histo_Data_numerator_muons = inputFile_Data_numerator.Get('Leading_Pt_Leptons_cut_0_0')
histo_Data_denominator_muons = inputFile_Data_denominator.Get('Leading_Pt_Leptons_cut_0_0')
histo_MC_muons = histo_MC_numerator_muons.Clone('Trigger_efficiency_MC_muons_UL' +year)
histo_MC_denominator_clone_muons = histo_MC_denominator_muons.Clone()
histo_Data_muons = histo_Data_numerator_muons.Clone('Trigger_efficiency_Data_muons_UL' +year)
histo_Data_denominator_clone_muons = histo_Data_denominator_muons.Clone()

histo_MC_elec.Rebin(2)
histo_MC_denominator_clone_elec.Rebin(2)
histo_Data_elec.Rebin(2)
histo_Data_denominator_clone_elec.Rebin(2)
histo_MC_muons.Rebin(2)
histo_MC_denominator_clone_muons.Rebin(2)
histo_Data_muons.Rebin(2)
histo_Data_denominator_clone_muons.Rebin(2)

histo_MC_elec.Divide(histo_MC_elec,histo_MC_denominator_clone_elec,1,1,"B")
histo_Data_elec.Divide(histo_Data_elec,histo_Data_denominator_clone_elec,1,1,"B")
histo_MC_muons.Divide(histo_MC_muons,histo_MC_denominator_clone_muons,1,1,"B")
histo_Data_muons.Divide(histo_Data_muons,histo_Data_denominator_clone_muons,1,1,"B")

c1 = ROOT.TCanvas('Trigger_efficiency_elec_UL' +year, 'Trigger efficiency for electrons (' + year + ')', 0, 300)
histo_MC_elec.SetLineColor(ROOT.kRed)
histo_MC_elec.SetLineWidth(3)
histo_MC_elec.GetXaxis().SetRangeUser(0, 300)
histo_MC_elec.GetYaxis().SetRangeUser(0.05, 1.2)
histo_MC_elec.SetTitle('Trigger efficiency for electrons (' + year + ')')
histo_MC_elec.GetXaxis().SetTitle('Pt of the leading lepton (GeV)')
histo_MC_elec.GetYaxis().SetTitle('Efficiency')
histo_MC_elec.SetStats(0)
histo_MC_elec.Draw('HISTP')
graph_MC_elec = ROOT.TGraphAsymmErrors(histo_MC_elec)
for i in range(1, histo_MC_elec.GetNbinsX() + 1):
    bin_content = histo_MC_elec.GetBinContent(i)
    if(bin_content > 1):
        bin_content = 1
    bin_error = histo_MC_elec.GetBinError(i)
    upper_error = bin_error
    if bin_content + bin_error > 1.0:
        upper_error = 1.0 - bin_content
    graph_MC_elec.SetPointEYlow(i-1, bin_error)
    graph_MC_elec.SetPointEYhigh(i-1, upper_error)
    graph_MC_elec.SetPoint(i-1, histo_MC_elec.GetBinCenter(i), bin_content)
graph_MC_elec.SetLineColor(ROOT.kRed)
graph_MC_elec.SetLineWidth(3)
graph_MC_elec.Draw('PSAME')
histo_Data_elec.SetLineColor(ROOT.kBlack)
histo_Data_elec.SetLineWidth(3)
histo_Data_elec.GetXaxis().SetRangeUser(0, 300)
histo_Data_elec.GetYaxis().SetRangeUser(0.05, 1.2)
histo_Data_elec.SetTitle('Trigger efficiency for electrons (' + year + ')')
histo_Data_elec.GetXaxis().SetTitle('Pt of the leading lepton (GeV)')
histo_Data_elec.GetYaxis().SetTitle('Efficiency')
histo_Data_elec.SetStats(0)
histo_Data_elec.Draw('HISTPSAME')
graph_Data_elec = ROOT.TGraphAsymmErrors(histo_Data_elec)
for i in range(1, histo_Data_elec.GetNbinsX() + 1):
    bin_content = histo_Data_elec.GetBinContent(i)
    bin_error = histo_Data_elec.GetBinError(i)
    upper_error = bin_error
    if bin_content + bin_error > 1.0:
        upper_error = 1.0 - bin_content
    graph_Data_elec.SetPointEYlow(i-1, bin_error)
    graph_Data_elec.SetPointEYhigh(i-1, upper_error)
    graph_Data_elec.SetPoint(i-1, histo_Data_elec.GetBinCenter(i), bin_content)
graph_Data_elec.SetLineColor(ROOT.kBlack)
graph_Data_elec.SetLineWidth(3)
graph_Data_elec.Draw('PSAME')
legend = ROOT.TLegend(0.5, 0.8, 0.9, 0.9)
legend.AddEntry(histo_MC_elec, 'MC (Signal sample, m_{T\'} = 700 GeV)', 'l')
legend.AddEntry(histo_Data_elec, 'Data (MET sample)', 'l')
legend.Draw()
c1.Modified()
c1.Update()

c2 = ROOT.TCanvas('Trigger_efficiency_muons_UL' +year, 'Trigger efficiency for muons (' + year + ')', 0, 300)
histo_MC_muons.SetLineColor(ROOT.kRed)
histo_MC_muons.SetLineWidth(3)
histo_MC_muons.GetXaxis().SetRangeUser(0, 300)
histo_MC_muons.GetYaxis().SetRangeUser(0.05, 1.2)
histo_MC_muons.SetTitle('Trigger efficiency for muons (' + year + ')')
histo_MC_muons.GetXaxis().SetTitle('Pt of the leading lepton (GeV)')
histo_MC_muons.GetYaxis().SetTitle('Efficiency')
histo_MC_muons.SetStats(0)
histo_MC_muons.Draw('HISTP')
graph_MC_muons = ROOT.TGraphAsymmErrors(histo_MC_muons)
for i in range(1, histo_MC_muons.GetNbinsX() + 1):
    bin_content = histo_MC_muons.GetBinContent(i)
    if(bin_content > 1):
        bin_content = 1
    bin_error = histo_MC_muons.GetBinError(i)
    upper_error = bin_error
    if bin_content + bin_error > 1.0:
        upper_error = 1.0 - bin_content
    graph_MC_muons.SetPointEYlow(i-1, bin_error)
    graph_MC_muons.SetPointEYhigh(i-1, upper_error)
    graph_MC_muons.SetPoint(i-1, histo_MC_muons.GetBinCenter(i), bin_content)
graph_MC_muons.SetLineColor(ROOT.kRed)
graph_MC_muons.SetLineWidth(3)
graph_MC_muons.Draw('PSAME')
histo_Data_muons.SetLineColor(ROOT.kBlack)
histo_Data_muons.SetLineWidth(3)
histo_Data_muons.GetXaxis().SetRangeUser(0, 300)
histo_Data_muons.GetYaxis().SetRangeUser(0.05, 1.2)
histo_Data_muons.SetTitle('Trigger efficiency for muons (' + year + ')')
histo_Data_muons.GetXaxis().SetTitle('Pt of the leading lepton (GeV)')
histo_Data_muons.GetYaxis().SetTitle('Efficiency')
histo_Data_muons.SetStats(0)
histo_Data_muons.Draw('HISTPSAME')
graph_Data_muons = ROOT.TGraphAsymmErrors(histo_Data_muons)
for i in range(1, histo_Data_muons.GetNbinsX() + 1):
    bin_content = histo_Data_muons.GetBinContent(i)
    bin_error = histo_Data_muons.GetBinError(i)
    upper_error = bin_error
    if bin_content + bin_error > 1.0:
        upper_error = 1.0 - bin_content
    graph_Data_muons.SetPointEYlow(i-1, bin_error)
    graph_Data_muons.SetPointEYhigh(i-1, upper_error)
    graph_Data_muons.SetPoint(i-1, histo_Data_muons.GetBinCenter(i), bin_content)
graph_Data_muons.SetLineColor(ROOT.kBlack)
graph_Data_muons.SetLineWidth(3)
graph_Data_muons.Draw('PSAME')
legend_2 = ROOT.TLegend(0.5, 0.8, 0.9, 0.9)
legend_2.AddEntry(histo_MC_muons, 'MC (Signal sample, m_{T\'} = 700 GeV)', 'l')
legend_2.AddEntry(histo_Data_muons, 'Data (MET sample)', 'l')
legend_2.Draw()
c2.Modified()
c2.Update()

output_file_path = 'analyzed/' + date + '/Merged/Trigger_efficiency_UL' + year + '.root'
output_file = ROOT.TFile.Open(output_file_path, 'RECREATE')
histo_MC_elec.SetName('Trigger_efficiency_MC_elec_UL' + year)
histo_Data_elec.SetName('Trigger_efficiency_Data_elec_UL' + year)
histo_MC_muons.SetName('Trigger_efficiency_MC_muons_UL' + year)
histo_Data_muons.SetName('Trigger_efficiency_Data_muons_UL' + year)
histo_MC_elec.Write()
histo_Data_elec.Write()
histo_MC_muons.Write()
histo_Data_muons.Write()
c1.Write()
c2.Write()
output_file.Close()

print('Process completed.')
