#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 14 11:01:46 2018

@author: Suyong Choi (Department of Physics, Korea University suyong@korea.ac.kr)

This script applies nanoaod processing to one file
"""

import sys
import cppyy
import ROOT
import re
import os
from importlib import import_module
from argparse import ArgumentParser
from btagging_efficiency_binning import btageff_dataset_dict

if __name__=='__main__':
    parser = ArgumentParser(usage="%prog inputfile outputfile jobconfmod year process Combine JEC cut_index Data")
    parser.add_argument("inputfile")
    parser.add_argument("outputfile")
    parser.add_argument("jobconfmod")
    parser.add_argument("year")
    parser.add_argument("process")
    parser.add_argument("Combine")
    parser.add_argument("JEC")
    parser.add_argument("cut_index")
    parser.add_argument("Data")
    args = parser.parse_args()
    inputfile = args.inputfile
    outputfile = args.outputfile
    jobconfmod = args.jobconfmod
    year = args.year
    process = args.process
    Combine = args.Combine
    JEC = args.JEC
    cut_index = args.cut_index
    Data = args.Data
    if(Combine == 'False'):
        Combine = bool(0)
    if(Combine == 'True'):
        Combine = bool(1)
    if(JEC == 'False'):
        JEC = bool(0)
    if(JEC == 'True'):
        JEC = bool(1)
    if(Data == 'False'):
        Data = bool(0)
    if(Data == 'True'):
        Data = bool(1)

    Cut = ''
    Mass = ''
    print(year)
    print(cut_index)
    parts = cut_index.split('_')
    second_cut = parts[1]
    third_cut = parts[2]
    if not JEC:
        fourth_cut = parts[3]
        fifth_cut = parts[4]

    if not JEC:
        if(third_cut == '0' and fifth_cut == '0'):
            Cut = '_SR'
        elif(third_cut == '1' and fifth_cut == '0'):
            Cut = '_MR_tt2l'
        elif(third_cut == '2' and fifth_cut == '0'):
            Cut = '_MR_tt1l_SR'
        elif(third_cut == '2' and fifth_cut == '1'):
            Cut = '_MR_tt1l_CR_ttX'
        elif(third_cut == '0' and fifth_cut == '1'):
            Cut = '_CR_ttX'

        if not Combine:
            Cuts = [
                'mumu' + Cut, 'muel' + Cut, 'elel' + Cut, 'Total' + Cut
            ]
        else:
            Cuts = [
                'mumu' + Cut, 'muel' + Cut, 'elel' + Cut
            ]
    else:
        Cuts = [
            'mumu', 'muel', 'elel'
        ]
    
    if not JEC:
        if(fourth_cut == '0'):
            Mass = '600'
        elif(fourth_cut == '1'):
            Mass = '625'
        elif(fourth_cut == '2'):
            Mass = '650'
        elif(fourth_cut == '3'):
            Mass = '675'
        elif(fourth_cut == '4'):
            Mass = '700'
        elif(fourth_cut == '5'):
            Mass = '800'
        elif(fourth_cut == '6'):
            Mass = '900'
        elif(fourth_cut == '7'):
            Mass = '1000'
        elif(fourth_cut == '8'):
            Mass = '1100'
        elif(fourth_cut == '9'):
            Mass = '1200'
    else:
        Mass = ''
    Masses = [
        Mass
    ]

    # load job configuration python module and get bjects
    mod = import_module(jobconfmod)
    config = getattr(mod, 'config')
    procflags = getattr(mod, 'procflags')
    print(config)

    intreename = "outputTree"
    outtreename = "outputTree"
    saveallbranches = 0

    # load compiled C++ library into ROOT/python
    cppyy.load_reflection_info("libcorrectionlib.so")
    cppyy.load_reflection_info("libMathMore.so")
    cppyy.load_reflection_info("libnanoadrdframe.so")
    t = ROOT.TChain(intreename)
    t.Add(inputfile)
    print("Inside process one file..!!")

    aproc = ROOT.TprimeAnalyser(t, outputfile)
    sys.stdout.flush()
    aproc.setParams(year, config['runtype'],config['datatype'], Data)
    sys.stdout.flush()
    aproc.setupAnalysis(year, process, 0, 1, Combine, JEC, config['variables_MC'], config['variables'], config ['weights'], Cuts, Masses, config['getQuantiles_ptleptons'], config['getQuantiles_massjets'], config['getQuantiles_ptleptons_loosett1l'], config['getQuantiles_massjets_loosett1l'], second_cut)
    sys.stdout.flush()
    aproc.run(saveallbranches, outtreename, process, 1, JEC)
