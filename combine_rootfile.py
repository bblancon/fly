#!/usr/bin/env python3
import ROOT
import os
import sys
from os import listdir
ROOT.TH1D.AddDirectory(False)

def dir_checker(directory):
    if not os.path.exists(directory):
        os.mkdir(directory)

date = '2025_02_25_19'
# year='2016'
year='2017'
# year='2018'
Lumi = 59.8
if('2016' in year):
    Lumi = 36.3
elif('2017' in year):
    Lumi = 41.5
Masses = ['600', '625', '650', '675', '700', '800', '900', '1000', '1100', '1200']
# Masses = ['700']
Samples = [

    'Signal',
    'DY_10_50',
    'DY_4_50_70',
    'DY_4_50_100',
    'DY_4_50_200',
    'DY_4_50_400',
    'DY_4_50_600',
    'TT_2L',
    'TT_SL',
    'TT_Had',
    'ttW',
    'ttH',
    'ttZ',
    'tW_antitop',
    'tW_top',
    'tHq',
    'tHW',
    'tZq',
    'WW',
    'WW_DS',
    'WJets',
    'WH_plus',
    'WH_minus',
    'WZ',
    'ZH',
    'ZZ_2L2Nu',
    'ZZ_2L2Q',
    'ZZ_4L',
    'WWW',
    'WWZ',
    'WZZ',
    'ZZZ',
    'Data',
]

Cuts = [

    'mumu_SR',
    'mumu_CR_ttX',
    'muel_SR',
    'muel_CR_ttX',
    'elel_SR',
    'elel_CR_ttX'

]

Weights = [

    'evWeight',
    'syst_elec_hltUp',
    'syst_elec_hltDown',
    'stat_elec_hlt_'+year+'Up',
    'stat_elec_hlt_'+year+'Down',
    'syst_elec_recoUp',
    'syst_elec_recoDown',
    'syst_elec_idUp',
    'syst_elec_idDown',
    'syst_muon_hltUp',
    'syst_muon_hltDown',
    'stat_muon_hlt_'+year+'Up',
    'stat_muon_hlt_'+year+'Down',
    'syst_muon_recoUp',
    'syst_muon_recoDown',
    'stat_muon_reco_'+year+'Up',
    'stat_muon_reco_'+year+'Down',
    'syst_muon_idUp',
    'syst_muon_idDown',
    'stat_muon_id_'+year+'Up',
    'stat_muon_id_'+year+'Down',
    'syst_muon_isoUp',
    'syst_muon_isoDown',
    'stat_muon_iso_'+year+'Up',
    'stat_muon_iso_'+year+'Down',
    'syst_puUp',
    'syst_puDown',
    'syst_b_correlatedUp',
    'syst_b_correlatedDown',
    'syst_b_uncorrelated_'+year+'Up',
    'syst_b_uncorrelated_'+year+'Down',
    'syst_l_correlatedUp',
    'syst_l_correlatedDown',
    'syst_l_uncorrelated_'+year+'Up',
    'syst_l_uncorrelated_'+year+'Down',
    'syst_prefiringUp',
    'syst_prefiringDown',
    'syst_pileupjetidUp',
    'syst_pileupjetidDown',
    'syst_pt_topUp',
    'syst_pt_topDown',
    'syst_isrUp',
    'syst_isrDown',
    'syst_fsrUp',
    'syst_fsrDown'

]

Weights_QCD = [

    'syst_mescaleUp',
    'syst_mescaleDown',
    'syst_renscaleUp',
    'syst_renscaleDown',
    'syst_facscaleUp',
    'syst_facscaleDown'

]

Weights_PDF = ['syst_pdf'+str(k) for k in range(1,103)]

Weights_JEC = [

    'syst_JES_Absolute_'+year+'Up',
    'syst_JES_Absolute_'+year+'Down',
    'syst_JES_AbsoluteUp',
    'syst_JES_AbsoluteDown',
    'syst_JES_BBEC1_'+year+'Up',
    'syst_JES_BBEC1_'+year+'Down',
    'syst_JES_BBEC1Up',
    'syst_JES_BBEC1Down',
    'syst_JES_EC2_'+year+'Up',
    'syst_JES_EC2_'+year+'Down',
    'syst_JES_EC2Up',
    'syst_JES_EC2Down',
    'syst_JES_FlavorQCDUp',
    'syst_JES_FlavorQCDDown',
    'syst_JES_HF_'+year+'Up',
    'syst_JES_HF_'+year+'Down',
    'syst_JES_HFUp',
    'syst_JES_HFDown',
    'syst_JES_RelativeSample_'+year+'Up',
    'syst_JES_RelativeSample_'+year+'Down',
    'syst_JES_RelativeBalUp',
    'syst_JES_RelativeBalDown'

]

Weights += Weights_QCD + Weights_PDF

Weights_DD_flip = [

    'evWeight',
    'syst_mr_flipUp',
    'syst_mr_flipDown',
    'syst_tr_flipUp',
    'syst_tr_flipDown'
]

Weights_DD_fake = [

    'evWeight',
    'syst_mr_fakeUp',
    'syst_mr_fakeDown',
    'syst_tr_fakeUp',
    'syst_tr_fakeDown'
]

files = (
    (Samples, Cuts, Masses, Weights)
    )

for cut in files[1]:

    for mass in files[2]:

        region = ''
        observable = 'mt_tprime'

        if('mumu' in cut):
            region = '0_'
        elif('muel' in cut):
            region = '1_'
        elif('elel' in cut):
            region = '2_'
        region += '0_'
        # print(cut)
        # print(region)
        # print(mass)

        if('SR' in cut and not 'MR' in cut):
            region += '0_'
            if('600' in mass):
                region += '0_0'
            elif('625' in mass):
                region += '1_0'
            elif('650' in mass):
                region += '2_0'
            elif('675' in mass):
                region += '3_0'
            elif('700' in mass):
                region += '4_0'
            elif('800' in mass):
                region += '5_0'
            elif('900' in mass):
                region += '6_0'
            elif('1000' in mass):
                region += '7_0'
            elif('1100' in mass):
                region += '8_0'
            elif('1200' in mass):
                region += '9_0'
        elif('MR_tt2l' in cut):
            region += '1_'
            observable = 'm_3j'
            if('600' in mass):
                region += '0_0'
            elif('625' in mass):
                region += '1_0'
            elif('650' in mass):
                region += '2_0'
            elif('675' in mass):
                region += '3_0'
            elif('700' in mass):
                region += '4_0'
            elif('800' in mass):
                region += '5_0'
            elif('900' in mass):
                region += '6_0'
            elif('1000' in mass):
                region += '7_0'
            elif('1100' in mass):
                region += '8_0'
            elif('1200' in mass):
                region += '9_0'
        elif('MR_tt1l_SR' in cut):
            region += '2_'
            observable = 'm_3j'
            if('600' in mass):
                region += '0_0'
            elif('625' in mass):
                region += '1_0'
            elif('650' in mass):
                region += '2_0'
            elif('675' in mass):
                region += '3_0'
            elif('700' in mass):
                region += '4_0'
            elif('800' in mass):
                region += '5_0'
            elif('900' in mass):
                region += '6_0'
            elif('1000' in mass):
                region += '7_0'
            elif('1100' in mass):
                region += '8_0'
            elif('1200' in mass):
                region += '9_0'
        elif('MR_tt1l_CR_ttX' in cut):
            region += '2_'
            observable = 'm_3j'
            if('600' in mass):
                region += '0_1'
            elif('625' in mass):
                region += '1_1'
            elif('650' in mass):
                region += '2_1'
            elif('675' in mass):
                region += '3_1'
            elif('700' in mass):
                region += '4_1'
            elif('800' in mass):
                region += '5_1'
            elif('900' in mass):
                region += '6_1'
            elif('1000' in mass):
                region += '7_1'
            elif('1100' in mass):
                region += '8_1'
            elif('1200' in mass):
                region += '9_1'
        elif('CR_ttX' in cut and not 'MR' in cut):
            region += '0_'
            if('600' in mass):
                region += '0_1'
            elif('625' in mass):
                region += '1_1'
            elif('650' in mass):
                region += '2_1'
            elif('675' in mass):
                region += '3_1'
            elif('700' in mass):
                region += '4_1'
            elif('800' in mass):
                region += '5_1'
            elif('900' in mass):
                region += '6_1'
            elif('1000' in mass):
                region += '7_1'
            elif('1100' in mass):
                region += '8_1'
            elif('1200' in mass):
                region += '9_1'
        print(region)

        Asimov = True
        if('CR_ttX' in cut):
            Asimov = False

        output = 'combine/' + mass + '/' + year + '/'
        dir_checker(output)

        output = 'combine/' + mass + '/' + year + '/inclusive/'
        dir_checker(output)

        output = 'combine/' + mass + '/' + year + '/inclusive/inputs/'
        dir_checker(output)

        outputFile = ROOT.TFile.Open('combine/' + mass + '/' + year + '/inclusive/inputs/' + observable + '_' + cut + '.root','RECREATE')

        name = []
        name_process = []

        for sample in files[0]:

            if('Signal' in sample):
                process = 'signal'
                sample = 'Signal' + mass
            elif('DY' in sample or 'TT_2L' in sample or 'TT_SL' in sample or 'TT_Had' in sample or 'tW_antitop' in sample or 'tW_top' in sample or 'tHq' in sample or 'tHW' in sample or 'tZq' in sample or 'WJets' in sample):
                process = 'others'
            elif('ttW' in sample or 'ttH' in sample or 'ttZ' in sample or 'ttg' in sample):
                process = 'ttx'
            elif('WW' in sample or 'WH' in sample or 'WZ' in sample or 'ZH' in sample or 'ZZ' in sample):
                process = 'multibosons'
            name_process.append(process)

            if('Data' in sample):
                process = 'data_obs'

            if(year == '2016' and 'Data' not in sample):
                inputFile = ROOT.TFile.Open('analyzed/' + date + '/Merged/' + sample + '_UL2016preVFP.root')
                inputFile2 = ROOT.TFile.Open('analyzed/' + date + '/Merged/' + sample + '_UL2016postVFP.root')
            else:
                inputFile = ROOT.TFile.Open('analyzed/' + date + '/Merged/' + sample + '_UL' + year + '.root')
            
            if('Data' not in sample):
                genhisto = inputFile.Get('genweight_nocut;1')
                sumofweights = genhisto.GetEntries()*genhisto.GetMean()
            if(sample == 'Signal600'):
                scale = 176.4*Lumi/sumofweights
            elif(sample == 'Signal625'):
                scale = 148.9*Lumi/sumofweights
            elif(sample == 'Signal650'):
                scale = 121.3*Lumi/sumofweights
            elif(sample == 'Signal675'):
                scale = 105.0*Lumi/sumofweights
            elif(sample == 'Signal700'):
                scale = 88.6*Lumi/sumofweights
            elif(sample == 'Signal800'):
                scale = 45.9*Lumi/sumofweights
            elif(sample == 'Signal900'):
                scale = 25.1*Lumi/sumofweights
            elif(sample == 'Signal1000'):
                scale = 14.5*Lumi/sumofweights
            elif(sample == 'Signal1100'):
                scale = 8.67*Lumi/sumofweights
            elif(sample == 'Signal1200'):
                scale = 5.36*Lumi/sumofweights
            if(sample == 'DY_10_50'):
                scale = 18610000*Lumi/sumofweights
            elif(sample == 'DY_4_50_70'):
                scale = 301200*Lumi/sumofweights
            elif(sample == 'DY_4_50_100'):
                scale = 224200*Lumi/sumofweights
            elif(sample == 'DY_4_50_200'):
                scale = 37200*Lumi/sumofweights
            elif(sample == 'DY_4_50_400'):
                scale = 3581*Lumi/sumofweights
            elif(sample == 'DY_4_50_600'):
                scale = 1124*Lumi/sumofweights
            elif(sample == 'TT_2L'):
                scale = 88341.9*Lumi/sumofweights
            elif(sample == 'TT_SL'):
                scale = 365457.4*Lumi/sumofweights
            elif(sample == 'TT_Had'):
                scale = 377960.7*Lumi/sumofweights
            elif(sample == 'ttW'):
                scale = 235*Lumi/sumofweights
            elif(sample == 'ttH'):
                scale = 211.2*Lumi/sumofweights
            elif(sample == 'ttZ'):
                scale = 859*Lumi/sumofweights
            elif('tW' in sample):
                scale = 39650*Lumi/sumofweights
            elif(sample == 'tHq'):
                scale = 70.96*Lumi/sumofweights
            elif(sample == 'tHW'):
                scale = 15.61*Lumi/sumofweights
            elif(sample == 'tZq'):
                scale = 73.58*Lumi/sumofweights
            elif(sample == 'WW'):
                scale = 12178*Lumi/sumofweights
            elif(sample == 'WW_DS'):
                scale = 223.2*Lumi/sumofweights
            elif(sample == 'WJets'):
                scale = 61334900*Lumi/sumofweights
            elif('WH' in sample):
                scale = 31.3*Lumi/sumofweights
            elif(sample == 'WZ'):
                scale = 4429.7*Lumi/sumofweights
            elif(sample == 'ZH'):
                scale = 185.8*Lumi/sumofweights
            elif(sample == 'ZZ_2L2Nu'):
                scale = 564.4*Lumi/sumofweights
            elif(sample == 'ZZ_2L2Q'):
                scale = 1973*Lumi/sumofweights
            elif(sample == 'ZZ_4L'):
                scale = 1256*Lumi/sumofweights
            elif(sample == 'WWW'):
                scale = 208.6*Lumi/sumofweights
            elif(sample == 'WWZ'):
                scale = 165.1*Lumi/sumofweights
            elif(sample == 'WZZ'):
                scale = 55.65*Lumi/sumofweights
            elif(sample == 'ZZZ'):
                scale = 14.76*Lumi/sumofweights
            elif('Data' in sample):
                scale = 1

            if(year == '2016'):
                if('Data' not in sample):
                    genhisto = inputFile2.Get('genweight_nocut;1')
                    sumofweights = genhisto.GetEntries()*genhisto.GetMean()
                if(sample == 'Signal600'):
                    scale2 = 176.4*Lumi/sumofweights
                elif(sample == 'Signal625'):
                    scale2 = 148.9*Lumi/sumofweights
                elif(sample == 'Signal650'):
                    scale2 = 121.3*Lumi/sumofweights
                elif(sample == 'Signal675'):
                    scale2 = 105.0*Lumi/sumofweights
                elif(sample == 'Signal700'):
                    scale2 = 88.6*Lumi/sumofweights
                elif(sample == 'Signal800'):
                    scale2 = 45.9*Lumi/sumofweights
                elif(sample == 'Signal900'):
                    scale2 = 25.1*Lumi/sumofweights
                elif(sample == 'Signal1000'):
                    scale2 = 14.5*Lumi/sumofweights
                elif(sample == 'Signal1100'):
                    scale2 = 8.67*Lumi/sumofweights
                elif(sample == 'Signal1200'):
                    scale2 = 5.36*Lumi/sumofweights
                if(sample == 'DY_10_50'):
                    scale2 = 18610000*Lumi/sumofweights
                elif(sample == 'DY_4_50_70'):
                    scale2 = 301200*Lumi/sumofweights
                elif(sample == 'DY_4_50_100'):
                    scale2 = 224200*Lumi/sumofweights
                elif(sample == 'DY_4_50_200'):
                    scale2 = 37200*Lumi/sumofweights
                elif(sample == 'DY_4_50_400'):
                    scale2 = 3581*Lumi/sumofweights
                elif(sample == 'DY_4_50_600'):
                    scale2 = 1124*Lumi/sumofweights
                elif(sample == 'TT_2L'):
                    scale2 = 88341.9*Lumi/sumofweights
                elif(sample == 'TT_SL'):
                    scale2 = 365457.4*Lumi/sumofweights
                elif(sample == 'TT_Had'):
                    scale2 = 377960.7*Lumi/sumofweights
                elif(sample == 'ttW'):
                    scale2 = 235*Lumi/sumofweights
                elif(sample == 'ttH'):
                    scale2 = 211.2*Lumi/sumofweights
                elif(sample == 'ttZ'):
                    scale2 = 859*Lumi/sumofweights
                elif('tW' in sample):
                    scale2 = 39650*Lumi/sumofweights
                elif(sample == 'tHq'):
                    scale2 = 70.96*Lumi/sumofweights
                elif(sample == 'tHW'):
                    scale2 = 15.61*Lumi/sumofweights
                elif(sample == 'tZq'):
                    scale2 = 73.58*Lumi/sumofweights
                elif(sample == 'WW'):
                    scale2 = 12178*Lumi/sumofweights
                elif(sample == 'WW_DS'):
                    scale2 = 223.2*Lumi/sumofweights
                elif(sample == 'WJets'):
                    scale2 = 61334900*Lumi/sumofweights
                elif('WH' in sample):
                    scale2 = 31.3*Lumi/sumofweights
                elif(sample == 'WZ'):
                    scale2 = 4429.7*Lumi/sumofweights
                elif(sample == 'ZH'):
                    scale2 = 185.8*Lumi/sumofweights
                elif(sample == 'ZZ_2L2Nu'):
                    scale2 = 564.4*Lumi/sumofweights
                elif(sample == 'ZZ_2L2Q'):
                    scale2 = 1973*Lumi/sumofweights
                elif(sample == 'ZZ_4L'):
                    scale2 = 1256*Lumi/sumofweights
                elif(sample == 'WWW'):
                    scale2 = 208.6*Lumi/sumofweights
                elif(sample == 'WWZ'):
                    scale2 = 165.1*Lumi/sumofweights
                elif(sample == 'WZZ'):
                    scale2 = 55.65*Lumi/sumofweights
                elif(sample == 'ZZZ'):
                    scale2 = 14.76*Lumi/sumofweights
                elif('Data' in sample):
                    scale2 = 1
            # print(sample)
            # print(sumofweights)
            # print(scale)
            # print(scale2)

            for weight in files[3]:
                # print(weight)
                if('Signal' in sample) or ('Data' in sample):
                    histo = inputFile.Get(process + '_' + weight + '_cut_' + region + ';1')
                    if('syst_b_uncorrelated_2016' in weight or 'syst_l_uncorrelated_2016' in weight):
                        histo = inputFile.Get(process + '_' + weight.replace('2016','2016preVFP') + '_cut_' + region + ';1')
                    if(year == '2016' and 'Data' not in sample):
                        histo2 = inputFile2.Get(process + '_' + weight + '_cut_' + region + ';1')
                        if('syst_b_uncorrelated_2016' in weight or 'syst_l_uncorrelated_2016' in weight):
                            histo2 = inputFile2.Get(process + '_' + weight.replace('2016','2016postVFP') + '_cut_' + region + ';1')
                else:
                    histo = inputFile.Get(process + '_' + weight + '_cut_' + region + '_0;1')
                    if('syst_b_uncorrelated_2016' in weight or 'syst_l_uncorrelated_2016' in weight):
                        histo = inputFile.Get(process + '_' + weight.replace('2016','2016preVFP') + '_cut_' + region + '_0;1')
                    if(year == '2016'):
                        histo2 = inputFile2.Get(process + '_' + weight + '_cut_' + region + '_0;1')
                        if('syst_b_uncorrelated_2016' in weight or 'syst_l_uncorrelated_2016' in weight):
                            histo2 = inputFile2.Get(process + '_' + weight.replace('2016','2016postVFP') + '_cut_' + region + '_0;1')
                if('Data' not in sample) or ('Data' in sample and Asimov == False):
                    histo_scaled = histo.Clone()
                    histo_scaled.Scale(scale)
                    if('syst_b_uncorrelated_2016' in weight or 'syst_l_uncorrelated_2016' in weight):
                        histo_scaled2 = histo2.Clone()
                        histo_scaled2.Scale(scale2)
                        name.append(process + '_' + weight.replace('2016','2016postVFP'))
                        outputFile.WriteObject(histo_scaled2, process + '_' + weight.replace('2016','2016postVFP'))
                    elif(year == '2016' and 'syst_b_uncorrelated_2016' not in weight and 'syst_l_uncorrelated_2016' not in weight and 'Data' not in sample):
                        histo_scaled2 = histo2.Clone()
                        histo_scaled2.Scale(scale2)
                        histo_scaled.Add(histo_scaled2)
                    if('evWeight' in weight):
                        name.append(process)
                        outputFile.WriteObject(histo_scaled, process)
                        if(Asimov == True and 'Signal' not in sample):
                            process_asimov = 'data_obs'
                            name.append(process_asimov)
                            outputFile.WriteObject(histo_scaled, process_asimov)
                    else:
                        if('fsr' in weight):
                            weight = weight.replace('fsr','fsr_' + process)
                        if('syst_b_uncorrelated_2016' in weight or 'syst_l_uncorrelated_2016' in weight):
                            name.append(process + '_' + weight.replace('2016','2016preVFP'))
                            outputFile.WriteObject(histo_scaled, process + '_' + weight.replace('2016','2016preVFP'))
                        else:
                            name.append(process + '_' + weight)
                            outputFile.WriteObject(histo_scaled, process + '_' + weight)
                if('Data' in sample):
                    break

            # jec scales
            for weight in Weights_JEC:
                if((year == '2016' and ('DY_10_50' not in sample and 'DY_4_50_70' not in sample and 'DY_4_50_100' not in sample and 'DY_4_50_200' not in sample and 'WJets' not in sample and 'ZZ_2L2Nu' not in sample) or year == '2017' and ('DY_10_50' not in sample and 'DY_4_50_70' not in sample and 'DY_4_50_100' not in sample and 'DY_4_50_200' not in sample and 'TT_Had' not in sample and 'WJets' not in sample and 'ZZ_2L2Nu' not in sample) or year == '2018' and ('DY_10_50' not in sample and 'DY_4_50_70' not in sample and 'DY_4_50_100' not in sample and 'WJets' not in sample and 'ZZ_2L2Nu' not in sample)) and 'Data' not in sample):
                    # print(weight)
                    region_new = region[:2] + str(Weights_JEC.index(weight)+1) + region[3:]
                    # print(region_new)
                    if('Signal' in sample):
                        histo = inputFile.Get(process + '_' + weight + '_cut_' + region_new + ';1')
                        if(year == '2016'):
                            histo2 = inputFile2.Get(process + '_' + weight + '_cut_' + region_new + ';1')
                    else:
                        histo = inputFile.Get(process + '_' + weight + '_cut_' + region_new + '_0;1')
                        if(year == '2016'):
                            histo2 = inputFile2.Get(process + '_' + weight + '_cut_' + region_new + '_0;1')
                    histo_scaled = histo.Clone()
                    histo_scaled.Scale(scale)
                    if(year == '2016'):
                        histo_scaled2 = histo2.Clone()
                        histo_scaled2.Scale(scale2)
                        histo_scaled.Add(histo_scaled2)
                    name.append(process + '_' + weight)
                    outputFile.WriteObject(histo_scaled, process + '_' + weight)

        # merge histograms with the same name
        name.sort()
        # print(name)
        for i in range(len(name) - 1):
            if(i >= len(name) - 1):
                break
            j = 1
            while(name[i] == name[i+j]):
                j += 1
                if(i+j == len(name)):
                    break
            if(j > 1):
                histo1 = outputFile.Get(name[i] + ';1')
                for k in range(j-1):
                    number = str(k+2)
                    histo2 = outputFile.Get(name[i] + ';' + number)
                    histo1.Add(histo2)
                    outputFile.Delete(name[i] + ';' + number)
                    name.pop(i+1)
                outputFile.Delete(name[i] + ';1')
                outputFile.WriteObject(histo1, name[i])

        name_process = list(dict.fromkeys(name_process))
        name_process.sort()
        print(name_process)

        # create one global qcd scale with renormalization, factorization and matrix elements contributions
        for process in name_process:
            histo = outputFile.Get(process + ';1')
            histo_nom = histo.Clone()
            nbin = histo_nom.GetNbinsX()
            binmin = histo_nom.GetXaxis().GetXmin()
            binmax = histo_nom.GetXaxis().GetXmax()

            histo_up = ROOT.TH1D(f'{process}_syst_qcdscaleUp', f'{process}_syst_qcdscaleUp', nbin, binmin, binmax)
            histo_down = ROOT.TH1D(f'{process}_syst_qcdscaleDown', f'{process}_syst_qcdscaleDown', nbin, binmin, binmax)

            for i in range(nbin+1):
                min_val = histo_nom.GetBinContent(1 + i)
                max_val = histo_nom.GetBinContent(1 + i)
                for weight in Weights_QCD:
                    histo = outputFile.Get(process + '_' + weight + ';1')
                    min_val = min(min_val, histo.GetBinContent(1 + i))
                    max_val = max(max_val, histo.GetBinContent(1 + i))
                histo_up.SetBinContent(1 + i, max_val)
                histo_down.SetBinContent(1 + i, min_val)

            if(histo_up.Integral() != 0 and histo_down.Integral() != 0):
                histo_up.Scale(histo_nom.Integral()/histo_up.Integral())
                histo_down.Scale(histo_nom.Integral()/histo_down.Integral())

            for weight in Weights_QCD:
                outputFile.Delete(process + '_' + weight + ';1')
            if(process != 'others'):
                outputFile.WriteObject(histo_up,process + '_syst_qcdscaleUp')
                outputFile.WriteObject(histo_down,process + '_syst_qcdscaleDown')
            else:
                outputFile.WriteObject(histo_nom,process + '_syst_qcdscaleUp')
                outputFile.WriteObject(histo_nom,process + '_syst_qcdscaleDown')    

        # create one global pdf scale
        for process in name_process:
            histo = outputFile.Get(process + ';1')
            histo_nom = histo.Clone()
            nbin = histo_nom.GetNbinsX()
            binmin = histo_nom.GetXaxis().GetXmin()
            binmax = histo_nom.GetXaxis().GetXmax()

            histo_up = ROOT.TH1D(f'{process}_syst_pdfasUp', f'{process}_syst_pdfasUp', nbin, binmin, binmax)
            histo_down = ROOT.TH1D(f'{process}_syst_pdfasDown', f'{process}_syst_pdfasDown', nbin, binmin, binmax)

            for i in range(nbin+1):
                diff = 0
                diff_as = 0
                for weight in Weights_PDF:
                    histo = outputFile.Get(process + '_' + weight + ';1')
                    if(weight != 'pdf101' and weight != 'pdf102'):
                        diff += (histo.GetBinContent(1 + i) - histo_nom.GetBinContent(1 + i)) ** 2
                    elif(weight == 'pdf101'):
                        diff_as += histo.GetBinContent(1 + i)
                    elif(weight == 'pdf102'):
                        diff_as -= histo.GetBinContent(1 + i)
                diff_as /= 2.0
                diff_as = diff_as ** 2
                histo_up.SetBinContent(1 + i, histo_nom.GetBinContent(1 + i) + (diff + diff_as) ** 0.5)
                histo_down.SetBinContent(1 + i, histo_nom.GetBinContent(1 + i) - (diff + diff_as) ** 0.5)

            if(histo_up.Integral() != 0 and histo_down.Integral() != 0):
                histo_up.Scale(histo_nom.Integral()/histo_up.Integral())
                histo_down.Scale(histo_nom.Integral()/histo_down.Integral())

            for weight in Weights_PDF:
                outputFile.Delete(process + '_' + weight + ';1')
            if(process != 'others'):
                outputFile.WriteObject(histo_up,process + '_syst_pdfasUp')
                outputFile.WriteObject(histo_down,process + '_syst_pdfasDown')
            else:
                outputFile.WriteObject(histo_nom,process + '_syst_pdfasUp')
                outputFile.WriteObject(histo_nom,process + '_syst_pdfasDown')
        inputFile.Close()

        # add data-driven flips and fakes
        inputFile = ROOT.TFile.Open('analyzed/' + date + '/Merged/Flip_' + mass + '_UL' + year + '.root')
        for weight in Weights_DD_flip:
            if(region.startswith('0_0_0')):
                region_new = '1' + region[1:]
                histo = inputFile.Get('flip_' + weight + '_cut_' + region_new + '_1;1')
                histo.Add(histo,-1)
            else:
                histo = inputFile.Get('flip_' + weight + '_cut_' + region + '_1;1')
            n_bins = histo.GetNbinsX() - 2
            x_min = histo.GetXaxis().GetBinLowEdge(3)
            x_max = histo.GetXaxis().GetBinUpEdge(histo.GetNbinsX())
            histo_modified = ROOT.TH1F(histo.GetName(), 'M_{T,T\'}; M_{T,T\'} (GeV);Events;', n_bins, x_min, x_max)
            for i in range(3, 10):
                bin_content = histo.GetBinContent(i)
                bin_error = histo.GetBinError(i)
                histo_modified.SetBinContent(i - 2, bin_content)
                histo_modified.SetBinError(i - 2, bin_error)
            if('evWeight' in weight):
                outputFile.WriteObject(histo_modified, 'flip')
                if(Asimov == True):
                    process_asimov = 'data_obs'
                    name.append(process_asimov)
                    outputFile.WriteObject(histo_modified, process_asimov)
            else:
                outputFile.WriteObject(histo_modified, 'flip_' + weight.replace('flip','flip_'+year))
        inputFile.Close()

        inputFile = ROOT.TFile.Open('analyzed/' + date + '/Merged/Fake_' + mass + '_UL' + year + '.root')
        for weight in Weights_DD_fake:
            histo = inputFile.Get('fake_' + weight + '_cut_' + region + '_2;1')
            n_bins = histo.GetNbinsX() - 2
            x_min = histo.GetXaxis().GetBinLowEdge(3)
            x_max = histo.GetXaxis().GetBinUpEdge(histo.GetNbinsX())
            histo_modified = ROOT.TH1F(histo.GetName(), 'M_{T,T\'}; M_{T,T\'} (GeV);Events;', n_bins, x_min, x_max)
            for i in range(3, 10):
                bin_content = histo.GetBinContent(i)
                bin_error = histo.GetBinError(i)
                histo_modified.SetBinContent(i - 2, bin_content)
                histo_modified.SetBinError(i - 2, bin_error)
            if('evWeight' in weight):
                outputFile.WriteObject(histo_modified, 'fake')
                if(Asimov == True):
                    process_asimov = 'data_obs'
                    name.append(process_asimov)
                    outputFile.WriteObject(histo_modified, process_asimov)
            else:
                if('SR' in cut):
                    outputFile.WriteObject(histo_modified, 'fake_' + weight.replace('fake','fake_SR_'+year))
                if('CR_ttX' in cut):
                    outputFile.WriteObject(histo_modified, 'fake_' + weight.replace('fake','fake_CR_ttX_'+year))
        inputFile.Close()

        # merge data histograms
        name.sort()
        # print(name)
        for i in range(len(name) - 1):
            if(i >= len(name) - 1):
                break
            j = 1
            while(name[i] == name[i+j]):
                j += 1
                if(i+j == len(name)):
                    break
            if(j > 1):
                histo1 = outputFile.Get(name[i] + ';1')
                for k in range(j-1):
                    number = str(k+2)
                    histo2 = outputFile.Get(name[i] + ';' + number)
                    histo1.Add(histo2)
                    outputFile.Delete(name[i] + ';' + number)
                    name.pop(i+1)
                outputFile.Delete(name[i] + ';1')
                outputFile.WriteObject(histo1, name[i])

# Merge histograms for total up/down variations
Cuts_total = [

    'Total_SR',
    'Total_CR_ttX',

]

os.system('rm combine/*/' + year + '/inclusive/inputs/*Total*.root')
for cut_total in Cuts_total:

    parts = cut_total.split('_',1)
    region = parts[1]

    for mass in files[2]:

        inputFiles = [ROOT.TFile.Open('combine/' + mass + '/' + year + '/inclusive/inputs/' + inputFile) for inputFile in listdir('combine/' + mass + '/' + year + '/inclusive/inputs/') if '.root' in inputFile and region in inputFile]
        outputFile = ROOT.TFile.Open('combine/' + mass + '/' + year + '/inclusive/inputs/mt_tprime_' + cut_total + '.root','RECREATE')

        for key in inputFiles[0].GetListOfKeys():
            hist_name = key.GetName()
            hist_total = inputFiles[0].Get(hist_name).Clone()
            hist_total.SetDirectory(0)

            for f in inputFiles[1:]:
                hist = f.Get(hist_name)
                if hist:
                    hist_total.Add(hist)
                else:
                    print(f'Histogram {hist_name} not found in one of the files.')

            outputFile.cd()
            hist_total.Write(hist_name)

        outputFile.Close()

        for inputFile in inputFiles:
            inputFile.Close()

print('Done')