#!/usr/bin/env python3

import ROOT
import os
import shutil
import sys

def tree_exists(filename):

    file = ROOT.TFile(filename)
    if not file:
        return False

    tree = file.Get('outputTree')
    if not tree:
        return False


    if tree.GetEntries() == 0:
        return False

    return True

def list_data_filemanes(root):
    listFiles=[]

    for dir in os.listdir(root):
        #Search in root/UL*
        if dir.startswith('UL'):
            fullPath = os.path.join(root,dir)
            #fullpath is now root/ULYEAR
            for file in os.listdir(fullPath):
                filepath=os.path.join(fullPath,file)
                if('Data' in filepath):
                    if os.path.isdir(filepath):
                        #filepath is root/ULYEAR/Data_SAMPLE_YEAR
                        for true_file in os.listdir(filepath):
                            true_file_path=os.path.join(filepath,true_file)
                            #true file is root/ULYEAR/Data_SAMPLE_YEAR/*.root
                            if true_file.endswith('.root'):
                                listFiles.append(true_file_path)
    return listFiles

def list_mc_filemanes(root):
    listFiles=[]

    for dir in os.listdir(root):
        #Search in root/UL*
        if dir.startswith('UL'):
            fullPath = os.path.join(root,dir)
            #fullpath is now root/ULYEAR
            for file in os.listdir(fullPath):
                filepath=os.path.join(fullPath,file)
                if('Data' not in filepath):
                    if os.path.isdir(filepath):
                        #filepath is root/ULYEAR/SAMPLE
                        for true_file in os.listdir(filepath):
                            true_file_path=os.path.join(filepath,true_file)
                            #true file is root/ULYEAR/SAMPLE/*.root
                            if true_file.endswith('.root'):
                                listFiles.append(true_file_path)
    return listFiles

def list_folder(root):
    listFolder=[]

    for dir in os.listdir(root):
        #Search in root/UL*
        if dir.startswith('UL'):
            fullPath = os.path.join(root,dir)
            #fullpath is now root/ULYEAR
            for file in os.listdir(fullPath):
                filepath=os.path.join(fullPath,file)
                if('Data' in filepath):
                    if os.path.isdir(filepath):
                        listFiles=[]
                        #filepath is root/ULYEAR/Data_SAMPLE_YEAR
                        for true_file in os.listdir(filepath):
                            true_file_path=os.path.join(filepath,true_file)
                            #true file is root/ULYEAR/Data_SAMPLE_YEAR/*.root
                            if true_file.endswith('.root'):
                                listFiles.append(true_file_path)
                        # print(true_file_path)
                        # print(listFiles)
                    if not listFiles:
                        listFolder.append(filepath)
    return listFolder

root = sys.argv[1]
typ = sys.argv[2]

if typ == 'MC':
    list=list_mc_filemanes(root)
else:
    list=list_data_filemanes(root)
print(len(list))
i=0
for file in list:
    if not tree_exists(file):
        print('Removing '+file)
        os.remove(file)
        # file_out = file.replace('.root','.out')
        # os.remove(file_out)
        i=i+1
        # print(i)
        pass
if(i > 0):
    print(str(i)+' files removed ( '+str(i/len(list))+' )')
else:
    print('No files removed!')

if typ == 'DATA':
    listFolder=list_folder(root)
    # print((listFolder))
    j=0
    for folder in listFolder:
        print('Removing '+folder)
        shutil.rmtree(folder)
        j=j+1
    print(str(j)+' folders removed')