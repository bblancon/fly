
btageff_dataset_dict = {
    'All': {
        'extension': 'All',
        'pt_bins': [0., 50., 100., 150., 200., 250., 300., 350., 400., 450., 500., 550., 600., 650., 700., 750., 800., 850., 900., 950., 1000.],
        'eta_bins': [0., 0.25, 0.50, 0.75, 1., 1.25, 1.5, 1.75, 2., 2.25, 2.5]
    },
    # Add more datasets with their corresponding extensions, pt_bins, and eta_bins
}
