import os
import sys
import ROOT
import math
from array import array

# Define the base directory where the dataset folders are located
date = '2025_02_12_21'
# year = '2016preVFP'
# year = '2016postVFP'
# year = '2017'
year = '2018'

Lumi = 59.8
if('2016preVFP' in year):
    Lumi = 19.5
elif('2016postVFP' in year):
    Lumi = 16.8
elif('2017' in year):
    Lumi = 41.5
Samples = [

    # 'Signal600',
    # 'Signal625',
    # 'Signal650',
    # 'Signal675',
    'Signal700',
    # 'Signal800',
    # 'Signal900',
    # 'Signal1000',
    # 'Signal1100',
    # 'Signal1200',
    'DY_10_50',
    'DY_4_50_70',
    'DY_4_50_100',
    'DY_4_50_200',
    'DY_4_50_400',
    'DY_4_50_600',
    'TT_2L',
    'TT_SL',
    'TT_Had',
    'ttW',
    'ttH',
    'ttZ',
    'tW_antitop',
    'tW_top',
    'tHq',
    'tHW',
    'tZq',
    'WW',
    'WW_DS',
    'WJets',
    'WH_plus',
    'WH_minus',
    'WZ',
    'ZH',
    'ZZ_2L2Nu',
    'ZZ_2L2Q',
    'ZZ_4L',
    'WWW',
    'WWZ',
    'WZZ',
    'ZZZ',
]

signal_mumu = [None] * 11
background_mumu = [None] * 11
signal_muel = [None] * 11
background_muel = [None] * 11
signal_elel = [None] * 11
background_elel = [None] * 11

for sample in Samples:

    inputFile = ROOT.TFile.Open('analyzed/' + date + '/Merged/' + sample + '_UL' + year + '.root', 'READ')
    print('analyzed/' + date + '/Merged/' + sample + '_UL' + year + '.root')

    genhisto = inputFile.Get('genweight_nocut;1')
    sumofweights = genhisto.GetEntries()*genhisto.GetMean()
    if(sample == 'Signal600'):
        scale = 176.4*Lumi/sumofweights
    elif(sample == 'Signal625'):
        scale = 148.9*Lumi/sumofweights
    elif(sample == 'Signal650'):
        scale = 121.3*Lumi/sumofweights
    elif(sample == 'Signal675'):
        scale = 105.0*Lumi/sumofweights
    elif(sample == 'Signal700'):
        scale = 88.6*Lumi/sumofweights
    elif(sample == 'Signal800'):
        scale = 45.9*Lumi/sumofweights
    elif(sample == 'Signal900'):
        scale = 25.1*Lumi/sumofweights
    elif(sample == 'Signal1000'):
        scale = 14.5*Lumi/sumofweights
    elif(sample == 'Signal1100'):
        scale = 8.67*Lumi/sumofweights
    elif(sample == 'Signal1200'):
        scale = 5.36*Lumi/sumofweights
    elif(sample == 'DY_10_50'):
        scale = 18610000*Lumi/sumofweights
    elif(sample == 'DY_4_50_70'):
        scale = 301200*Lumi/sumofweights
    elif(sample == 'DY_4_50_100'):
        scale = 224200*Lumi/sumofweights
    elif(sample == 'DY_4_50_200'):
        scale = 37200*Lumi/sumofweights
    elif(sample == 'DY_4_50_400'):
        scale = 3581*Lumi/sumofweights
    elif(sample == 'DY_4_50_600'):
        scale = 1124*Lumi/sumofweights
    elif(sample == 'TT_2L'):
        scale = 88341.9*Lumi/sumofweights
    elif(sample == 'TT_SL'):
        scale = 365457.4*Lumi/sumofweights
    elif(sample == 'TT_Had'):
        scale = 377960.7*Lumi/sumofweights
    elif(sample == 'ttW'):
        scale = 235*Lumi/sumofweights
    elif(sample == 'ttH'):
        scale = 211.2*Lumi/sumofweights
    elif(sample == 'ttZ'):
        scale = 859*Lumi/sumofweights
    elif('tW' in sample):
        scale = 39650*Lumi/sumofweights
    elif(sample == 'tHq'):
        scale = 70.96*Lumi/sumofweights
    elif(sample == 'tHW'):
        scale = 15.61*Lumi/sumofweights
    elif(sample == 'tZq'):
        scale = 73.58*Lumi/sumofweights
    elif(sample == 'WW'):
        scale = 12178*Lumi/sumofweights
    elif(sample == 'WW_DS'):
        scale = 223.2*Lumi/sumofweights
    elif(sample == 'WJets'):
        scale = 61334900*Lumi/sumofweights
    elif('WH' in sample):
        scale = 31.3*Lumi/sumofweights
    elif(sample == 'WZ'):
        scale = 4429.7*Lumi/sumofweights
    elif(sample == 'ZH'):
        scale = 185.8*Lumi/sumofweights
    elif(sample == 'ZZ_2L2Nu'):
        scale = 564.4*Lumi/sumofweights
    elif(sample == 'ZZ_2L2Q'):
        scale = 1973*Lumi/sumofweights
    elif(sample == 'ZZ_4L'):
        scale = 1256*Lumi/sumofweights
    elif(sample == 'WWW'):
        scale = 208.6*Lumi/sumofweights
    elif(sample == 'WWZ'):
        scale = 165.1*Lumi/sumofweights
    elif(sample == 'WZZ'):
        scale = 55.65*Lumi/sumofweights
    elif(sample == 'ZZZ'):
        scale = 14.76*Lumi/sumofweights
    print(sample)
    print(sumofweights)
    print(scale)

    # histo_mumu = inputFile.Get('St_cut_0_0_0_0_0_0')
    # histo_mumu.Scale(scale)
    # histo_muel = inputFile.Get('St_cut_1_0_0_0_0_0')
    # histo_muel.Scale(scale)
    # histo_elel = inputFile.Get('St_cut_2_0_0_0_0_0')
    # histo_elel.Scale(scale)
    # histo_mumu = inputFile.Get('Tprime_transverse_mass_First_cut_0_0_0_0_0_0')
    # histo_mumu.Scale(scale)
    # histo_muel = inputFile.Get('Tprime_transverse_mass_First_cut_1_0_0_0_0_0')
    # histo_muel.Scale(scale)
    # histo_elel = inputFile.Get('Tprime_transverse_mass_First_cut_2_0_0_0_0_0')
    # histo_elel.Scale(scale)
    # histo_mumu = inputFile.Get('Tprime_transverse_mass_Second_cut_0_0_0_0_0_0')
    # histo_mumu.Scale(scale)
    # histo_muel = inputFile.Get('Tprime_transverse_mass_Second_cut_1_0_0_0_0_0')
    # histo_muel.Scale(scale)
    # histo_elel = inputFile.Get('Tprime_transverse_mass_Second_cut_2_0_0_0_0_0')
    # histo_elel.Scale(scale)
    histo_mumu = inputFile.Get('Tprime_transverse_mass_First_cut_0_0_0_4_0')
    histo_mumu.Scale(scale)
    histo_muel = inputFile.Get('Tprime_transverse_mass_First_cut_1_0_0_4_0')
    histo_muel.Scale(scale)
    histo_elel = inputFile.Get('Tprime_transverse_mass_First_cut_2_0_0_4_0')
    histo_elel.Scale(scale)

    for i in range(11):
        print(i)
        histo_value_mumu = histo_mumu.GetBinContent(i)
        histo_value_muel = histo_muel.GetBinContent(i)
        histo_value_elel = histo_elel.GetBinContent(i)
        if('Signal' in sample):
            signal_mumu[i] = histo_value_mumu
            signal_muel[i] = histo_value_muel
            signal_elel[i] = histo_value_elel
        else:
            if(sample == 'DY_10_50'):
                background_mumu[i] = histo_value_mumu
                background_muel[i] = histo_value_muel
                background_elel[i] = histo_value_elel
            else:
                background_mumu[i] += histo_value_mumu
                background_muel[i] += histo_value_muel
                background_elel[i] += histo_value_elel
        print(signal_mumu[i])
        print(signal_muel[i])
        print(signal_elel[i])
        print(background_mumu[i])
        print(background_muel[i])
        print(background_elel[i])

LLR = 0

for i in range(11):
    print(i)
    print(signal_mumu[i])
    print(background_mumu[i])
    print(signal_muel[i])
    print(background_muel[i])
    print(signal_elel[i])
    print(background_elel[i])
    if(background_mumu[i] == 0):
        LLR += 0
    else:
        LLR += ((signal_mumu[i] + background_mumu[i]) * math.log(1 + signal_mumu[i]/background_mumu[i]) - signal_mumu[i])
    if(background_muel[i] == 0):
        LLR += 0
    else:
        LLR += ((signal_muel[i] + background_muel[i]) * math.log(1 + signal_muel[i]/background_muel[i]) - signal_muel[i])
    if(background_elel[i] == 0):
        LLR += 0
    else:
        LLR += ((signal_elel[i] + background_elel[i]) * math.log(1 + signal_elel[i]/background_elel[i]) - signal_elel[i])

significance = math.sqrt(2*LLR)
print(significance)
inputFile.Close()

print('Process completed.')
