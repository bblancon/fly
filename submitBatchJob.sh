#!/bin/bash
#
#SBATCH --spread-job
# mail-type=BEGIN, END, FAIL, REQUEUE, ALL, STAGE_OUT, TIME_LIMIT_90
#SBATCH --mail-type=FAIL
#SBATCH --mail-user=b.blancon@ip2i.in2p3.fr
#SBATCH --mem=16G

export X509_USER_PROXY=$HOME/x509up_u2459
export EOS_MGM_URL=root://lyoeos.in2p3.fr

inputFile=$(cat ${1})
input_file="$1"
output_dir=${2}
year=${3}
era=${4}
process=${5}
BTagEff=${6}
Hist=${7}
Combine=${8}
JEC=${9}
Data=${10}

root='.root'

# Get the line corresponding to the current SLURM array task
line=$(sed -n "${SLURM_ARRAY_TASK_ID}p" "$input_file")

# Check if the line is empty
if [ -z "$line" ]; then
    echo "No input line found for SLURM_ARRAY_TASK_ID=${SLURM_ARRAY_TASK_ID}"
    exit 1
fi

# Extract the filename from the path
filename=$(basename "$line")

# Construct output and log file paths
output_file="${output_dir}${filename}"
log_file="${output_dir}${filename%.root}.out"

# Debugging statements
echo "SLURM_ARRAY_TASK_ID: $SLURM_ARRAY_TASK_ID"
echo "Processing file: $line"
echo "Output directory: $output_dir"
echo "Output file: $output_file"
echo "Log file: $log_file"

# Run your processing script
./processonefile.py "$line" "$output_file" jobconfiganalysis$year "$year" "$era" "$process" "$BTagEff" "$Hist" "$Combine" "$JEC" "$Data"  > "$log_file" 2>&1
#wait