date=${1}

# Data
# 2016preVFP
# cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_0_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_0_0_0_0_0.root
# cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_0_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_1_0_0_0_0.root
# cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_0_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_2_0_0_0_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_0_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_0_0_0_0_1.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_0_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_1_0_0_0_1.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_0_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_2_0_0_0_1.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_1_0_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_0_0_1_0_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_1_0_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_1_0_1_0_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_1_0_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_2_0_1_0_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_0_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_0_0_2_0_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_0_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_1_0_2_0_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_0_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_2_0_2_0_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_0_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_0_0_2_0_1.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_0_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_1_0_2_0_1.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_0_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_2_0_2_0_1.root
# cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_1_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_0_0_0_1_0.root
# cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_1_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_1_0_0_1_0.root
# cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_1_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_2_0_0_1_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_1_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_0_0_0_1_1.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_1_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_1_0_0_1_1.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_1_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_2_0_0_1_1.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_1_1_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_0_0_1_1_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_1_1_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_1_0_1_1_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_1_1_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_2_0_1_1_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_1_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_0_0_2_1_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_1_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_1_0_2_1_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_1_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_2_0_2_1_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_1_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_0_0_2_1_1.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_1_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_1_0_2_1_1.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_1_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_2_0_2_1_1.root
# cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_2_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_0_0_0_2_0.root
# cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_2_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_1_0_0_2_0.root
# cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_2_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_2_0_0_2_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_2_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_0_0_0_2_1.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_2_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_1_0_0_2_1.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_2_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_2_0_0_2_1.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_1_2_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_0_0_1_2_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_1_2_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_1_0_1_2_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_1_2_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_2_0_1_2_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_2_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_0_0_2_2_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_2_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_1_0_2_2_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_2_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_2_0_2_2_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_2_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_0_0_2_2_1.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_2_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_1_0_2_2_1.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_2_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_2_0_2_2_1.root
# cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_3_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_0_0_0_3_0.root
# cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_3_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_1_0_0_3_0.root
# cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_3_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_2_0_0_3_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_3_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_0_0_0_3_1.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_3_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_1_0_0_3_1.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_3_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_2_0_0_3_1.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_1_3_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_0_0_1_3_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_1_3_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_1_0_1_3_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_1_3_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_2_0_1_3_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_3_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_0_0_2_3_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_3_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_1_0_2_3_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_3_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_2_0_2_3_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_3_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_0_0_2_3_1.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_3_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_1_0_2_3_1.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_3_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_2_0_2_3_1.root
# cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_4_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_0_0_0_4_0.root
# cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_4_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_1_0_0_4_0.root
# cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_4_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_2_0_0_4_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_4_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_0_0_0_4_1.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_4_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_1_0_0_4_1.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_4_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_2_0_0_4_1.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_1_4_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_0_0_1_4_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_1_4_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_1_0_1_4_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_1_4_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_2_0_1_4_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_4_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_0_0_2_4_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_4_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_1_0_2_4_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_4_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_2_0_2_4_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_4_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_0_0_2_4_1.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_4_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_1_0_2_4_1.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_4_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_2_0_2_4_1.root
# cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_5_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_0_0_0_5_0.root
# cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_5_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_1_0_0_5_0.root
# cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_5_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_2_0_0_5_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_5_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_0_0_0_5_1.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_5_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_1_0_0_5_1.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_5_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_2_0_0_5_1.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_1_5_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_0_0_1_5_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_1_5_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_1_0_1_5_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_1_5_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_2_0_1_5_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_5_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_0_0_2_5_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_5_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_1_0_2_5_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_5_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_2_0_2_5_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_5_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_0_0_2_5_1.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_5_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_1_0_2_5_1.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_5_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_2_0_2_5_1.root
# cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_6_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_0_0_0_6_0.root
# cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_6_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_1_0_0_6_0.root
# cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_6_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_2_0_0_6_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_6_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_0_0_0_6_1.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_6_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_1_0_0_6_1.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_6_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_2_0_0_6_1.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_1_6_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_0_0_1_6_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_1_6_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_1_0_1_6_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_1_6_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_2_0_1_6_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_6_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_0_0_2_6_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_6_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_1_0_2_6_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_6_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_2_0_2_6_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_6_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_0_0_2_6_1.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_6_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_1_0_2_6_1.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_6_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_2_0_2_6_1.root
# cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_7_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_0_0_0_7_0.root
# cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_7_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_1_0_0_7_0.root
# cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_7_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_2_0_0_7_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_7_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_0_0_0_7_1.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_7_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_1_0_0_7_1.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_7_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_2_0_0_7_1.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_1_7_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_0_0_1_7_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_1_7_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_1_0_1_7_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_1_7_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_2_0_1_7_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_7_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_0_0_2_7_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_7_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_1_0_2_7_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_7_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_2_0_2_7_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_7_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_0_0_2_7_1.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_7_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_1_0_2_7_1.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_7_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_2_0_2_7_1.root
# cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_8_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_0_0_0_8_0.root
# cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_8_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_1_0_0_8_0.root
# cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_8_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_2_0_0_8_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_8_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_0_0_0_8_1.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_8_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_1_0_0_8_1.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_8_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_2_0_0_8_1.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_1_8_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_0_0_1_8_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_1_8_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_1_0_1_8_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_1_8_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_2_0_1_8_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_8_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_0_0_2_8_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_8_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_1_0_2_8_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_8_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_2_0_2_8_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_8_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_0_0_2_8_1.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_8_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_1_0_2_8_1.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_8_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_2_0_2_8_1.root
# cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_9_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_0_0_0_9_0.root
# cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_9_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_1_0_0_9_0.root
# cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_9_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_2_0_0_9_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_9_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_0_0_0_9_1.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_9_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_1_0_0_9_1.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_0_9_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_2_0_0_9_1.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_1_9_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_0_0_1_9_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_1_9_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_1_0_1_9_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_1_9_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_2_0_1_9_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_9_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_0_0_2_9_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_9_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_1_0_2_9_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_9_0.root analyzed/"$date"/Merged/Data_UL2016preVFP_2_0_2_9_0.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_9_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_0_0_2_9_1.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_9_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_1_0_2_9_1.root
cp analyzed/"$date"/Merged/Data_UL2016preVFP_3_0_2_9_1.root analyzed/"$date"/Merged/Data_UL2016preVFP_2_0_2_9_1.root

# 2016postVFP
# cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_0_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_0_0_0_0_0.root
# cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_0_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_1_0_0_0_0.root
# cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_0_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_2_0_0_0_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_0_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_0_0_0_0_1.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_0_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_1_0_0_0_1.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_0_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_2_0_0_0_1.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_1_0_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_0_0_1_0_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_1_0_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_1_0_1_0_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_1_0_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_2_0_1_0_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_0_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_0_0_2_0_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_0_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_1_0_2_0_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_0_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_2_0_2_0_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_0_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_0_0_2_0_1.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_0_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_1_0_2_0_1.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_0_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_2_0_2_0_1.root
# cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_1_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_0_0_0_1_0.root
# cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_1_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_1_0_0_1_0.root
# cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_1_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_2_0_0_1_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_1_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_0_0_0_1_1.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_1_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_1_0_0_1_1.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_1_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_2_0_0_1_1.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_1_1_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_0_0_1_1_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_1_1_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_1_0_1_1_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_1_1_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_2_0_1_1_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_1_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_0_0_2_1_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_1_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_1_0_2_1_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_1_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_2_0_2_1_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_1_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_0_0_2_1_1.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_1_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_1_0_2_1_1.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_1_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_2_0_2_1_1.root
# cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_2_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_0_0_0_2_0.root
# cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_2_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_1_0_0_2_0.root
# cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_2_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_2_0_0_2_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_2_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_0_0_0_2_1.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_2_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_1_0_0_2_1.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_2_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_2_0_0_2_1.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_1_2_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_0_0_1_2_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_1_2_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_1_0_1_2_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_1_2_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_2_0_1_2_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_2_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_0_0_2_2_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_2_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_1_0_2_2_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_2_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_2_0_2_2_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_2_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_0_0_2_2_1.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_2_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_1_0_2_2_1.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_2_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_2_0_2_2_1.root
# cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_3_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_0_0_0_3_0.root
# cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_3_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_1_0_0_3_0.root
# cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_3_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_2_0_0_3_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_3_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_0_0_0_3_1.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_3_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_1_0_0_3_1.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_3_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_2_0_0_3_1.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_1_3_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_0_0_1_3_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_1_3_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_1_0_1_3_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_1_3_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_2_0_1_3_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_3_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_0_0_2_3_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_3_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_1_0_2_3_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_3_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_2_0_2_3_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_3_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_0_0_2_3_1.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_3_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_1_0_2_3_1.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_3_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_2_0_2_3_1.root
# cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_4_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_0_0_0_4_0.root
# cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_4_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_1_0_0_4_0.root
# cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_4_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_2_0_0_4_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_4_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_0_0_0_4_1.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_4_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_1_0_0_4_1.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_4_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_2_0_0_4_1.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_1_4_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_0_0_1_4_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_1_4_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_1_0_1_4_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_1_4_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_2_0_1_4_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_4_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_0_0_2_4_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_4_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_1_0_2_4_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_4_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_2_0_2_4_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_4_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_0_0_2_4_1.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_4_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_1_0_2_4_1.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_4_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_2_0_2_4_1.root
# cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_5_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_0_0_0_5_0.root
# cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_5_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_1_0_0_5_0.root
# cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_5_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_2_0_0_5_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_5_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_0_0_0_5_1.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_5_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_1_0_0_5_1.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_5_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_2_0_0_5_1.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_1_5_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_0_0_1_5_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_1_5_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_1_0_1_5_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_1_5_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_2_0_1_5_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_5_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_0_0_2_5_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_5_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_1_0_2_5_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_5_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_2_0_2_5_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_5_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_0_0_2_5_1.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_5_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_1_0_2_5_1.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_5_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_2_0_2_5_1.root
# cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_6_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_0_0_0_6_0.root
# cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_6_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_1_0_0_6_0.root
# cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_6_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_2_0_0_6_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_6_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_0_0_0_6_1.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_6_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_1_0_0_6_1.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_6_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_2_0_0_6_1.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_1_6_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_0_0_1_6_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_1_6_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_1_0_1_6_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_1_6_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_2_0_1_6_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_6_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_0_0_2_6_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_6_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_1_0_2_6_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_6_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_2_0_2_6_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_6_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_0_0_2_6_1.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_6_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_1_0_2_6_1.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_6_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_2_0_2_6_1.root
# cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_7_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_0_0_0_7_0.root
# cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_7_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_1_0_0_7_0.root
# cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_7_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_2_0_0_7_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_7_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_0_0_0_7_1.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_7_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_1_0_0_7_1.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_7_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_2_0_0_7_1.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_1_7_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_0_0_1_7_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_1_7_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_1_0_1_7_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_1_7_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_2_0_1_7_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_7_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_0_0_2_7_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_7_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_1_0_2_7_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_7_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_2_0_2_7_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_7_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_0_0_2_7_1.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_7_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_1_0_2_7_1.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_7_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_2_0_2_7_1.root
# cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_8_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_0_0_0_8_0.root
# cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_8_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_1_0_0_8_0.root
# cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_8_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_2_0_0_8_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_8_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_0_0_0_8_1.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_8_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_1_0_0_8_1.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_8_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_2_0_0_8_1.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_1_8_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_0_0_1_8_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_1_8_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_1_0_1_8_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_1_8_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_2_0_1_8_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_8_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_0_0_2_8_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_8_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_1_0_2_8_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_8_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_2_0_2_8_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_8_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_0_0_2_8_1.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_8_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_1_0_2_8_1.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_8_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_2_0_2_8_1.root
# cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_9_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_0_0_0_9_0.root
# cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_9_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_1_0_0_9_0.root
# cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_9_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_2_0_0_9_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_9_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_0_0_0_9_1.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_9_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_1_0_0_9_1.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_0_9_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_2_0_0_9_1.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_1_9_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_0_0_1_9_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_1_9_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_1_0_1_9_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_1_9_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_2_0_1_9_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_9_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_0_0_2_9_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_9_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_1_0_2_9_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_9_0.root analyzed/"$date"/Merged/Data_UL2016postVFP_2_0_2_9_0.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_9_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_0_0_2_9_1.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_9_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_1_0_2_9_1.root
cp analyzed/"$date"/Merged/Data_UL2016postVFP_3_0_2_9_1.root analyzed/"$date"/Merged/Data_UL2016postVFP_2_0_2_9_1.root

# 2017
# cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_0_0.root analyzed/"$date"/Merged/Data_UL2017_0_0_0_0_0.root
# cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_0_0.root analyzed/"$date"/Merged/Data_UL2017_1_0_0_0_0.root
# cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_0_0.root analyzed/"$date"/Merged/Data_UL2017_2_0_0_0_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_0_1.root analyzed/"$date"/Merged/Data_UL2017_0_0_0_0_1.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_0_1.root analyzed/"$date"/Merged/Data_UL2017_1_0_0_0_1.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_0_1.root analyzed/"$date"/Merged/Data_UL2017_2_0_0_0_1.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_1_0_0.root analyzed/"$date"/Merged/Data_UL2017_0_0_1_0_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_1_0_0.root analyzed/"$date"/Merged/Data_UL2017_1_0_1_0_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_1_0_0.root analyzed/"$date"/Merged/Data_UL2017_2_0_1_0_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_0_0.root analyzed/"$date"/Merged/Data_UL2017_0_0_2_0_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_0_0.root analyzed/"$date"/Merged/Data_UL2017_1_0_2_0_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_0_0.root analyzed/"$date"/Merged/Data_UL2017_2_0_2_0_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_0_1.root analyzed/"$date"/Merged/Data_UL2017_0_0_2_0_1.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_0_1.root analyzed/"$date"/Merged/Data_UL2017_1_0_2_0_1.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_0_1.root analyzed/"$date"/Merged/Data_UL2017_2_0_2_0_1.root
# cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_1_0.root analyzed/"$date"/Merged/Data_UL2017_0_0_0_1_0.root
# cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_1_0.root analyzed/"$date"/Merged/Data_UL2017_1_0_0_1_0.root
# cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_1_0.root analyzed/"$date"/Merged/Data_UL2017_2_0_0_1_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_1_1.root analyzed/"$date"/Merged/Data_UL2017_0_0_0_1_1.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_1_1.root analyzed/"$date"/Merged/Data_UL2017_1_0_0_1_1.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_1_1.root analyzed/"$date"/Merged/Data_UL2017_2_0_0_1_1.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_1_1_0.root analyzed/"$date"/Merged/Data_UL2017_0_0_1_1_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_1_1_0.root analyzed/"$date"/Merged/Data_UL2017_1_0_1_1_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_1_1_0.root analyzed/"$date"/Merged/Data_UL2017_2_0_1_1_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_1_0.root analyzed/"$date"/Merged/Data_UL2017_0_0_2_1_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_1_0.root analyzed/"$date"/Merged/Data_UL2017_1_0_2_1_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_1_0.root analyzed/"$date"/Merged/Data_UL2017_2_0_2_1_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_1_1.root analyzed/"$date"/Merged/Data_UL2017_0_0_2_1_1.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_1_1.root analyzed/"$date"/Merged/Data_UL2017_1_0_2_1_1.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_1_1.root analyzed/"$date"/Merged/Data_UL2017_2_0_2_1_1.root
# cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_2_0.root analyzed/"$date"/Merged/Data_UL2017_0_0_0_2_0.root
# cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_2_0.root analyzed/"$date"/Merged/Data_UL2017_1_0_0_2_0.root
# cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_2_0.root analyzed/"$date"/Merged/Data_UL2017_2_0_0_2_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_2_1.root analyzed/"$date"/Merged/Data_UL2017_0_0_0_2_1.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_2_1.root analyzed/"$date"/Merged/Data_UL2017_1_0_0_2_1.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_2_1.root analyzed/"$date"/Merged/Data_UL2017_2_0_0_2_1.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_1_2_0.root analyzed/"$date"/Merged/Data_UL2017_0_0_1_2_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_1_2_0.root analyzed/"$date"/Merged/Data_UL2017_1_0_1_2_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_1_2_0.root analyzed/"$date"/Merged/Data_UL2017_2_0_1_2_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_2_0.root analyzed/"$date"/Merged/Data_UL2017_0_0_2_2_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_2_0.root analyzed/"$date"/Merged/Data_UL2017_1_0_2_2_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_2_0.root analyzed/"$date"/Merged/Data_UL2017_2_0_2_2_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_2_1.root analyzed/"$date"/Merged/Data_UL2017_0_0_2_2_1.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_2_1.root analyzed/"$date"/Merged/Data_UL2017_1_0_2_2_1.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_2_1.root analyzed/"$date"/Merged/Data_UL2017_2_0_2_2_1.root
# cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_3_0.root analyzed/"$date"/Merged/Data_UL2017_0_0_0_3_0.root
# cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_3_0.root analyzed/"$date"/Merged/Data_UL2017_1_0_0_3_0.root
# cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_3_0.root analyzed/"$date"/Merged/Data_UL2017_2_0_0_3_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_3_1.root analyzed/"$date"/Merged/Data_UL2017_0_0_0_3_1.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_3_1.root analyzed/"$date"/Merged/Data_UL2017_1_0_0_3_1.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_3_1.root analyzed/"$date"/Merged/Data_UL2017_2_0_0_3_1.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_1_3_0.root analyzed/"$date"/Merged/Data_UL2017_0_0_1_3_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_1_3_0.root analyzed/"$date"/Merged/Data_UL2017_1_0_1_3_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_1_3_0.root analyzed/"$date"/Merged/Data_UL2017_2_0_1_3_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_3_0.root analyzed/"$date"/Merged/Data_UL2017_0_0_2_3_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_3_0.root analyzed/"$date"/Merged/Data_UL2017_1_0_2_3_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_3_0.root analyzed/"$date"/Merged/Data_UL2017_2_0_2_3_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_3_1.root analyzed/"$date"/Merged/Data_UL2017_0_0_2_3_1.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_3_1.root analyzed/"$date"/Merged/Data_UL2017_1_0_2_3_1.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_3_1.root analyzed/"$date"/Merged/Data_UL2017_2_0_2_3_1.root
# cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_4_0.root analyzed/"$date"/Merged/Data_UL2017_0_0_0_4_0.root
# cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_4_0.root analyzed/"$date"/Merged/Data_UL2017_1_0_0_4_0.root
# cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_4_0.root analyzed/"$date"/Merged/Data_UL2017_2_0_0_4_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_4_1.root analyzed/"$date"/Merged/Data_UL2017_0_0_0_4_1.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_4_1.root analyzed/"$date"/Merged/Data_UL2017_1_0_0_4_1.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_4_1.root analyzed/"$date"/Merged/Data_UL2017_2_0_0_4_1.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_1_4_0.root analyzed/"$date"/Merged/Data_UL2017_0_0_1_4_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_1_4_0.root analyzed/"$date"/Merged/Data_UL2017_1_0_1_4_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_1_4_0.root analyzed/"$date"/Merged/Data_UL2017_2_0_1_4_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_4_0.root analyzed/"$date"/Merged/Data_UL2017_0_0_2_4_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_4_0.root analyzed/"$date"/Merged/Data_UL2017_1_0_2_4_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_4_0.root analyzed/"$date"/Merged/Data_UL2017_2_0_2_4_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_4_1.root analyzed/"$date"/Merged/Data_UL2017_0_0_2_4_1.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_4_1.root analyzed/"$date"/Merged/Data_UL2017_1_0_2_4_1.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_4_1.root analyzed/"$date"/Merged/Data_UL2017_2_0_2_4_1.root
# cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_5_0.root analyzed/"$date"/Merged/Data_UL2017_0_0_0_5_0.root
# cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_5_0.root analyzed/"$date"/Merged/Data_UL2017_1_0_0_5_0.root
# cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_5_0.root analyzed/"$date"/Merged/Data_UL2017_2_0_0_5_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_5_1.root analyzed/"$date"/Merged/Data_UL2017_0_0_0_5_1.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_5_1.root analyzed/"$date"/Merged/Data_UL2017_1_0_0_5_1.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_5_1.root analyzed/"$date"/Merged/Data_UL2017_2_0_0_5_1.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_1_5_0.root analyzed/"$date"/Merged/Data_UL2017_0_0_1_5_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_1_5_0.root analyzed/"$date"/Merged/Data_UL2017_1_0_1_5_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_1_5_0.root analyzed/"$date"/Merged/Data_UL2017_2_0_1_5_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_5_0.root analyzed/"$date"/Merged/Data_UL2017_0_0_2_5_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_5_0.root analyzed/"$date"/Merged/Data_UL2017_1_0_2_5_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_5_0.root analyzed/"$date"/Merged/Data_UL2017_2_0_2_5_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_5_1.root analyzed/"$date"/Merged/Data_UL2017_0_0_2_5_1.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_5_1.root analyzed/"$date"/Merged/Data_UL2017_1_0_2_5_1.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_5_1.root analyzed/"$date"/Merged/Data_UL2017_2_0_2_5_1.root
# cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_6_0.root analyzed/"$date"/Merged/Data_UL2017_0_0_0_6_0.root
# cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_6_0.root analyzed/"$date"/Merged/Data_UL2017_1_0_0_6_0.root
# cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_6_0.root analyzed/"$date"/Merged/Data_UL2017_2_0_0_6_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_6_1.root analyzed/"$date"/Merged/Data_UL2017_0_0_0_6_1.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_6_1.root analyzed/"$date"/Merged/Data_UL2017_1_0_0_6_1.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_6_1.root analyzed/"$date"/Merged/Data_UL2017_2_0_0_6_1.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_1_6_0.root analyzed/"$date"/Merged/Data_UL2017_0_0_1_6_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_1_6_0.root analyzed/"$date"/Merged/Data_UL2017_1_0_1_6_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_1_6_0.root analyzed/"$date"/Merged/Data_UL2017_2_0_1_6_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_6_0.root analyzed/"$date"/Merged/Data_UL2017_0_0_2_6_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_6_0.root analyzed/"$date"/Merged/Data_UL2017_1_0_2_6_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_6_0.root analyzed/"$date"/Merged/Data_UL2017_2_0_2_6_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_6_1.root analyzed/"$date"/Merged/Data_UL2017_0_0_2_6_1.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_6_1.root analyzed/"$date"/Merged/Data_UL2017_1_0_2_6_1.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_6_1.root analyzed/"$date"/Merged/Data_UL2017_2_0_2_6_1.root
# cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_7_0.root analyzed/"$date"/Merged/Data_UL2017_0_0_0_7_0.root
# cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_7_0.root analyzed/"$date"/Merged/Data_UL2017_1_0_0_7_0.root
# cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_7_0.root analyzed/"$date"/Merged/Data_UL2017_2_0_0_7_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_7_1.root analyzed/"$date"/Merged/Data_UL2017_0_0_0_7_1.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_7_1.root analyzed/"$date"/Merged/Data_UL2017_1_0_0_7_1.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_7_1.root analyzed/"$date"/Merged/Data_UL2017_2_0_0_7_1.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_1_7_0.root analyzed/"$date"/Merged/Data_UL2017_0_0_1_7_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_1_7_0.root analyzed/"$date"/Merged/Data_UL2017_1_0_1_7_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_1_7_0.root analyzed/"$date"/Merged/Data_UL2017_2_0_1_7_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_7_0.root analyzed/"$date"/Merged/Data_UL2017_0_0_2_7_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_7_0.root analyzed/"$date"/Merged/Data_UL2017_1_0_2_7_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_7_0.root analyzed/"$date"/Merged/Data_UL2017_2_0_2_7_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_7_1.root analyzed/"$date"/Merged/Data_UL2017_0_0_2_7_1.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_7_1.root analyzed/"$date"/Merged/Data_UL2017_1_0_2_7_1.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_7_1.root analyzed/"$date"/Merged/Data_UL2017_2_0_2_7_1.root
# cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_8_0.root analyzed/"$date"/Merged/Data_UL2017_0_0_0_8_0.root
# cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_8_0.root analyzed/"$date"/Merged/Data_UL2017_1_0_0_8_0.root
# cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_8_0.root analyzed/"$date"/Merged/Data_UL2017_2_0_0_8_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_8_1.root analyzed/"$date"/Merged/Data_UL2017_0_0_0_8_1.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_8_1.root analyzed/"$date"/Merged/Data_UL2017_1_0_0_8_1.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_8_1.root analyzed/"$date"/Merged/Data_UL2017_2_0_0_8_1.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_1_8_0.root analyzed/"$date"/Merged/Data_UL2017_0_0_1_8_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_1_8_0.root analyzed/"$date"/Merged/Data_UL2017_1_0_1_8_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_1_8_0.root analyzed/"$date"/Merged/Data_UL2017_2_0_1_8_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_8_0.root analyzed/"$date"/Merged/Data_UL2017_0_0_2_8_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_8_0.root analyzed/"$date"/Merged/Data_UL2017_1_0_2_8_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_8_0.root analyzed/"$date"/Merged/Data_UL2017_2_0_2_8_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_8_1.root analyzed/"$date"/Merged/Data_UL2017_0_0_2_8_1.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_8_1.root analyzed/"$date"/Merged/Data_UL2017_1_0_2_8_1.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_8_1.root analyzed/"$date"/Merged/Data_UL2017_2_0_2_8_1.root
# cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_9_0.root analyzed/"$date"/Merged/Data_UL2017_0_0_0_9_0.root
# cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_9_0.root analyzed/"$date"/Merged/Data_UL2017_1_0_0_9_0.root
# cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_9_0.root analyzed/"$date"/Merged/Data_UL2017_2_0_0_9_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_9_1.root analyzed/"$date"/Merged/Data_UL2017_0_0_0_9_1.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_9_1.root analyzed/"$date"/Merged/Data_UL2017_1_0_0_9_1.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_0_9_1.root analyzed/"$date"/Merged/Data_UL2017_2_0_0_9_1.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_1_9_0.root analyzed/"$date"/Merged/Data_UL2017_0_0_1_9_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_1_9_0.root analyzed/"$date"/Merged/Data_UL2017_1_0_1_9_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_1_9_0.root analyzed/"$date"/Merged/Data_UL2017_2_0_1_9_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_9_0.root analyzed/"$date"/Merged/Data_UL2017_0_0_2_9_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_9_0.root analyzed/"$date"/Merged/Data_UL2017_1_0_2_9_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_9_0.root analyzed/"$date"/Merged/Data_UL2017_2_0_2_9_0.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_9_1.root analyzed/"$date"/Merged/Data_UL2017_0_0_2_9_1.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_9_1.root analyzed/"$date"/Merged/Data_UL2017_1_0_2_9_1.root
cp analyzed/"$date"/Merged/Data_UL2017_3_0_2_9_1.root analyzed/"$date"/Merged/Data_UL2017_2_0_2_9_1.root

# 2018
# cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_0_0.root analyzed/"$date"/Merged/Data_UL2018_0_0_0_0_0.root
# cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_0_0.root analyzed/"$date"/Merged/Data_UL2018_1_0_0_0_0.root
# cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_0_0.root analyzed/"$date"/Merged/Data_UL2018_2_0_0_0_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_0_1.root analyzed/"$date"/Merged/Data_UL2018_0_0_0_0_1.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_0_1.root analyzed/"$date"/Merged/Data_UL2018_1_0_0_0_1.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_0_1.root analyzed/"$date"/Merged/Data_UL2018_2_0_0_0_1.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_1_0_0.root analyzed/"$date"/Merged/Data_UL2018_0_0_1_0_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_1_0_0.root analyzed/"$date"/Merged/Data_UL2018_1_0_1_0_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_1_0_0.root analyzed/"$date"/Merged/Data_UL2018_2_0_1_0_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_0_0.root analyzed/"$date"/Merged/Data_UL2018_0_0_2_0_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_0_0.root analyzed/"$date"/Merged/Data_UL2018_1_0_2_0_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_0_0.root analyzed/"$date"/Merged/Data_UL2018_2_0_2_0_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_0_1.root analyzed/"$date"/Merged/Data_UL2018_0_0_2_0_1.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_0_1.root analyzed/"$date"/Merged/Data_UL2018_1_0_2_0_1.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_0_1.root analyzed/"$date"/Merged/Data_UL2018_2_0_2_0_1.root
# cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_1_0.root analyzed/"$date"/Merged/Data_UL2018_0_0_0_1_0.root
# cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_1_0.root analyzed/"$date"/Merged/Data_UL2018_1_0_0_1_0.root
# cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_1_0.root analyzed/"$date"/Merged/Data_UL2018_2_0_0_1_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_1_1.root analyzed/"$date"/Merged/Data_UL2018_0_0_0_1_1.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_1_1.root analyzed/"$date"/Merged/Data_UL2018_1_0_0_1_1.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_1_1.root analyzed/"$date"/Merged/Data_UL2018_2_0_0_1_1.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_1_1_0.root analyzed/"$date"/Merged/Data_UL2018_0_0_1_1_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_1_1_0.root analyzed/"$date"/Merged/Data_UL2018_1_0_1_1_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_1_1_0.root analyzed/"$date"/Merged/Data_UL2018_2_0_1_1_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_1_0.root analyzed/"$date"/Merged/Data_UL2018_0_0_2_1_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_1_0.root analyzed/"$date"/Merged/Data_UL2018_1_0_2_1_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_1_0.root analyzed/"$date"/Merged/Data_UL2018_2_0_2_1_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_1_1.root analyzed/"$date"/Merged/Data_UL2018_0_0_2_1_1.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_1_1.root analyzed/"$date"/Merged/Data_UL2018_1_0_2_1_1.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_1_1.root analyzed/"$date"/Merged/Data_UL2018_2_0_2_1_1.root
# cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_2_0.root analyzed/"$date"/Merged/Data_UL2018_0_0_0_2_0.root
# cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_2_0.root analyzed/"$date"/Merged/Data_UL2018_1_0_0_2_0.root
# cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_2_0.root analyzed/"$date"/Merged/Data_UL2018_2_0_0_2_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_2_1.root analyzed/"$date"/Merged/Data_UL2018_0_0_0_2_1.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_2_1.root analyzed/"$date"/Merged/Data_UL2018_1_0_0_2_1.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_2_1.root analyzed/"$date"/Merged/Data_UL2018_2_0_0_2_1.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_1_2_0.root analyzed/"$date"/Merged/Data_UL2018_0_0_1_2_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_1_2_0.root analyzed/"$date"/Merged/Data_UL2018_1_0_1_2_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_1_2_0.root analyzed/"$date"/Merged/Data_UL2018_2_0_1_2_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_2_0.root analyzed/"$date"/Merged/Data_UL2018_0_0_2_2_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_2_0.root analyzed/"$date"/Merged/Data_UL2018_1_0_2_2_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_2_0.root analyzed/"$date"/Merged/Data_UL2018_2_0_2_2_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_2_1.root analyzed/"$date"/Merged/Data_UL2018_0_0_2_2_1.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_2_1.root analyzed/"$date"/Merged/Data_UL2018_1_0_2_2_1.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_2_1.root analyzed/"$date"/Merged/Data_UL2018_2_0_2_2_1.root
# cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_3_0.root analyzed/"$date"/Merged/Data_UL2018_0_0_0_3_0.root
# cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_3_0.root analyzed/"$date"/Merged/Data_UL2018_1_0_0_3_0.root
# cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_3_0.root analyzed/"$date"/Merged/Data_UL2018_2_0_0_3_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_3_1.root analyzed/"$date"/Merged/Data_UL2018_0_0_0_3_1.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_3_1.root analyzed/"$date"/Merged/Data_UL2018_1_0_0_3_1.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_3_1.root analyzed/"$date"/Merged/Data_UL2018_2_0_0_3_1.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_1_3_0.root analyzed/"$date"/Merged/Data_UL2018_0_0_1_3_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_1_3_0.root analyzed/"$date"/Merged/Data_UL2018_1_0_1_3_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_1_3_0.root analyzed/"$date"/Merged/Data_UL2018_2_0_1_3_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_3_0.root analyzed/"$date"/Merged/Data_UL2018_0_0_2_3_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_3_0.root analyzed/"$date"/Merged/Data_UL2018_1_0_2_3_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_3_0.root analyzed/"$date"/Merged/Data_UL2018_2_0_2_3_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_3_1.root analyzed/"$date"/Merged/Data_UL2018_0_0_2_3_1.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_3_1.root analyzed/"$date"/Merged/Data_UL2018_1_0_2_3_1.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_3_1.root analyzed/"$date"/Merged/Data_UL2018_2_0_2_3_1.root
# cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_4_0.root analyzed/"$date"/Merged/Data_UL2018_0_0_0_4_0.root
# cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_4_0.root analyzed/"$date"/Merged/Data_UL2018_1_0_0_4_0.root
# cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_4_0.root analyzed/"$date"/Merged/Data_UL2018_2_0_0_4_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_4_1.root analyzed/"$date"/Merged/Data_UL2018_0_0_0_4_1.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_4_1.root analyzed/"$date"/Merged/Data_UL2018_1_0_0_4_1.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_4_1.root analyzed/"$date"/Merged/Data_UL2018_2_0_0_4_1.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_1_4_0.root analyzed/"$date"/Merged/Data_UL2018_0_0_1_4_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_1_4_0.root analyzed/"$date"/Merged/Data_UL2018_1_0_1_4_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_1_4_0.root analyzed/"$date"/Merged/Data_UL2018_2_0_1_4_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_4_0.root analyzed/"$date"/Merged/Data_UL2018_0_0_2_4_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_4_0.root analyzed/"$date"/Merged/Data_UL2018_1_0_2_4_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_4_0.root analyzed/"$date"/Merged/Data_UL2018_2_0_2_4_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_4_1.root analyzed/"$date"/Merged/Data_UL2018_0_0_2_4_1.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_4_1.root analyzed/"$date"/Merged/Data_UL2018_1_0_2_4_1.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_4_1.root analyzed/"$date"/Merged/Data_UL2018_2_0_2_4_1.root
# cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_5_0.root analyzed/"$date"/Merged/Data_UL2018_0_0_0_5_0.root
# cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_5_0.root analyzed/"$date"/Merged/Data_UL2018_1_0_0_5_0.root
# cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_5_0.root analyzed/"$date"/Merged/Data_UL2018_2_0_0_5_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_5_1.root analyzed/"$date"/Merged/Data_UL2018_0_0_0_5_1.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_5_1.root analyzed/"$date"/Merged/Data_UL2018_1_0_0_5_1.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_5_1.root analyzed/"$date"/Merged/Data_UL2018_2_0_0_5_1.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_1_5_0.root analyzed/"$date"/Merged/Data_UL2018_0_0_1_5_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_1_5_0.root analyzed/"$date"/Merged/Data_UL2018_1_0_1_5_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_1_5_0.root analyzed/"$date"/Merged/Data_UL2018_2_0_1_5_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_5_0.root analyzed/"$date"/Merged/Data_UL2018_0_0_2_5_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_5_0.root analyzed/"$date"/Merged/Data_UL2018_1_0_2_5_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_5_0.root analyzed/"$date"/Merged/Data_UL2018_2_0_2_5_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_5_1.root analyzed/"$date"/Merged/Data_UL2018_0_0_2_5_1.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_5_1.root analyzed/"$date"/Merged/Data_UL2018_1_0_2_5_1.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_5_1.root analyzed/"$date"/Merged/Data_UL2018_2_0_2_5_1.root
# cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_6_0.root analyzed/"$date"/Merged/Data_UL2018_0_0_0_6_0.root
# cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_6_0.root analyzed/"$date"/Merged/Data_UL2018_1_0_0_6_0.root
# cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_6_0.root analyzed/"$date"/Merged/Data_UL2018_2_0_0_6_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_6_1.root analyzed/"$date"/Merged/Data_UL2018_0_0_0_6_1.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_6_1.root analyzed/"$date"/Merged/Data_UL2018_1_0_0_6_1.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_6_1.root analyzed/"$date"/Merged/Data_UL2018_2_0_0_6_1.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_1_6_0.root analyzed/"$date"/Merged/Data_UL2018_0_0_1_6_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_1_6_0.root analyzed/"$date"/Merged/Data_UL2018_1_0_1_6_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_1_6_0.root analyzed/"$date"/Merged/Data_UL2018_2_0_1_6_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_6_0.root analyzed/"$date"/Merged/Data_UL2018_0_0_2_6_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_6_0.root analyzed/"$date"/Merged/Data_UL2018_1_0_2_6_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_6_0.root analyzed/"$date"/Merged/Data_UL2018_2_0_2_6_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_6_1.root analyzed/"$date"/Merged/Data_UL2018_0_0_2_6_1.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_6_1.root analyzed/"$date"/Merged/Data_UL2018_1_0_2_6_1.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_6_1.root analyzed/"$date"/Merged/Data_UL2018_2_0_2_6_1.root
# cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_7_0.root analyzed/"$date"/Merged/Data_UL2018_0_0_0_7_0.root
# cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_7_0.root analyzed/"$date"/Merged/Data_UL2018_1_0_0_7_0.root
# cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_7_0.root analyzed/"$date"/Merged/Data_UL2018_2_0_0_7_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_7_1.root analyzed/"$date"/Merged/Data_UL2018_0_0_0_7_1.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_7_1.root analyzed/"$date"/Merged/Data_UL2018_1_0_0_7_1.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_7_1.root analyzed/"$date"/Merged/Data_UL2018_2_0_0_7_1.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_1_7_0.root analyzed/"$date"/Merged/Data_UL2018_0_0_1_7_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_1_7_0.root analyzed/"$date"/Merged/Data_UL2018_1_0_1_7_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_1_7_0.root analyzed/"$date"/Merged/Data_UL2018_2_0_1_7_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_7_0.root analyzed/"$date"/Merged/Data_UL2018_0_0_2_7_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_7_0.root analyzed/"$date"/Merged/Data_UL2018_1_0_2_7_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_7_0.root analyzed/"$date"/Merged/Data_UL2018_2_0_2_7_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_7_1.root analyzed/"$date"/Merged/Data_UL2018_0_0_2_7_1.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_7_1.root analyzed/"$date"/Merged/Data_UL2018_1_0_2_7_1.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_7_1.root analyzed/"$date"/Merged/Data_UL2018_2_0_2_7_1.root
# cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_8_0.root analyzed/"$date"/Merged/Data_UL2018_0_0_0_8_0.root
# cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_8_0.root analyzed/"$date"/Merged/Data_UL2018_1_0_0_8_0.root
# cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_8_0.root analyzed/"$date"/Merged/Data_UL2018_2_0_0_8_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_8_1.root analyzed/"$date"/Merged/Data_UL2018_0_0_0_8_1.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_8_1.root analyzed/"$date"/Merged/Data_UL2018_1_0_0_8_1.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_8_1.root analyzed/"$date"/Merged/Data_UL2018_2_0_0_8_1.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_1_8_0.root analyzed/"$date"/Merged/Data_UL2018_0_0_1_8_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_1_8_0.root analyzed/"$date"/Merged/Data_UL2018_1_0_1_8_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_1_8_0.root analyzed/"$date"/Merged/Data_UL2018_2_0_1_8_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_8_0.root analyzed/"$date"/Merged/Data_UL2018_0_0_2_8_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_8_0.root analyzed/"$date"/Merged/Data_UL2018_1_0_2_8_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_8_0.root analyzed/"$date"/Merged/Data_UL2018_2_0_2_8_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_8_1.root analyzed/"$date"/Merged/Data_UL2018_0_0_2_8_1.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_8_1.root analyzed/"$date"/Merged/Data_UL2018_1_0_2_8_1.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_8_1.root analyzed/"$date"/Merged/Data_UL2018_2_0_2_8_1.root
# cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_9_0.root analyzed/"$date"/Merged/Data_UL2018_0_0_0_9_0.root
# cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_9_0.root analyzed/"$date"/Merged/Data_UL2018_1_0_0_9_0.root
# cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_9_0.root analyzed/"$date"/Merged/Data_UL2018_2_0_0_9_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_9_1.root analyzed/"$date"/Merged/Data_UL2018_0_0_0_9_1.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_9_1.root analyzed/"$date"/Merged/Data_UL2018_1_0_0_9_1.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_0_9_1.root analyzed/"$date"/Merged/Data_UL2018_2_0_0_9_1.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_1_9_0.root analyzed/"$date"/Merged/Data_UL2018_0_0_1_9_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_1_9_0.root analyzed/"$date"/Merged/Data_UL2018_1_0_1_9_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_1_9_0.root analyzed/"$date"/Merged/Data_UL2018_2_0_1_9_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_9_0.root analyzed/"$date"/Merged/Data_UL2018_0_0_2_9_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_9_0.root analyzed/"$date"/Merged/Data_UL2018_1_0_2_9_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_9_0.root analyzed/"$date"/Merged/Data_UL2018_2_0_2_9_0.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_9_1.root analyzed/"$date"/Merged/Data_UL2018_0_0_2_9_1.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_9_1.root analyzed/"$date"/Merged/Data_UL2018_1_0_2_9_1.root
cp analyzed/"$date"/Merged/Data_UL2018_3_0_2_9_1.root analyzed/"$date"/Merged/Data_UL2018_2_0_2_9_1.root