#!/usr/bin/env python
# -*- coding: utf-8 -*-

from ROOT import RDataFrame
import numpy as np
import math
import os
import sys
import CMS_lumi
import array
import tdrstyle
from array import array
import pandas as pd
import matplotlib.pyplot as plt
import ROOT
from ROOT import gStyle
from pathlib import Path
import inspect
import ctypes

ROOT.TH1D.AddDirectory(False)

STACKED = 1
NORMALIZED = 0

# Colors
BLACK = "\u001b[0;30m"
RED = "\u001b[0;31m"
HRED = "\u001b[1;31m"
GREEN = "\u001b[0;32m"
YELLOW = "\u001b[0;33m"
BLUE = "\u001b[0;34m"
PURPLE = "\u001b[0;35m"
CYAN = "\u001b[0;36m"
WHITE = "\u001b[0;37m"
GRAY = "\u001b[0;90m"
LRED = "\u001b[1;91m"
LGREEN = "\u001b[1;92m"
LYELLOW = "\u001b[1;93m"
LBLUE = "\u001b[1;94m"
LPURPLE = "\u001b[1;95m"
LCYAN = "\u001b[1;96m"
BWHITE = "\u001b[1;97m"
RESET = "\u001b[0m"
DGRAY = "\u001b[1;30m"
LGRAY = "\u001b[0;37m"
BRED = "\u001b[1;31m"
BGREEN = "\u001b[1;32m"
BYELLOW = "\u001b[1;33m"
BBLUE = "\u001b[1;34m"
BPURPLE = "\u001b[1;35m"
BCYAN = "\u001b[1;36m"
# Reset
RESET = "\033[0m"


class StackHists:

    def __init__(self, ROOT, tdrStyle, integer_lumi, verbose=False):
        self.verbose = verbose
        self.c1, self.data_file, self.fill_alpha = None, None, None

        self.mc_file_list, self.mc_label_list = [], []
        self.mc_color_list, self.color_list, self.mc_pattern_list, self.pattern_list = [], [], [], []
        self.sum_of_weights_hist_name = ''

        # Flip leptons Fit specific objects
        self.mc_flip_files_forFit, self.mc_flip_files_intt2lregion_forFit, self.mc_flip_xsec_forFit, self.mc_flip_sf_forFit = [], [], [], []
        self.Flip_dD_h_copy_intt2lregion_forPlot = ''
        self.Flip_dD_h_copy_forFlipPlot_SR = ''
        self.Flip_dD_h_copy_forFlipPlot_CRttX = ''
        self.preFit = True
        self.tt2lregion_data_file = ''
        self.dataDriven_Flip_normalization = 1.0
        self.histName_forFlipFit_SR_mumu = ''
        self.histName_forFlipFit_CRttX_mumu = ''
        self.histName_forFlipDD_mumu = ''
        self.histName_forFlip_dD_SR_mumu = ''
        self.histName_forFlip_dD_SR_mumu_systshape_Up = ''
        self.histName_forFlip_dD_SR_mumu_systshape_Down = ''
        self.histName_forFlip_dD_SR_mumu_systmc_Up = ''
        self.histName_forFlip_dD_SR_mumu_systmc_Down = ''
        self.histName_forFlip_dD_CRttX_mumu = ''
        self.histName_forFlip_dD_CRttX_mumu_systshape_Up = ''
        self.histName_forFlip_dD_CRttX_mumu_systshape_Down = ''
        self.histName_forFlip_dD_CRttX_mumu_systmc_Up = ''
        self.histName_forFlip_dD_CRttX_mumu_systmc_Down = ''
        self.histName_forFlipFit_SR_emu = ''
        self.histName_forFlipFit_CRttX_emu = ''
        self.histName_forFlipDD_emu = ''
        self.histName_forFlip_dD_SR_emu = ''
        self.histName_forFlip_dD_SR_emu_systshape_Up = ''
        self.histName_forFlip_dD_SR_emu_systshape_Down = ''
        self.histName_forFlip_dD_SR_emu_systmc_Up = ''
        self.histName_forFlip_dD_SR_emu_systmc_Down = ''
        self.histName_forFlip_dD_CRttX_emu = ''
        self.histName_forFlip_dD_CRttX_emu_systshape_Up = ''
        self.histName_forFlip_dD_CRttX_emu_systshape_Down = ''
        self.histName_forFlip_dD_CRttX_emu_systmc_Up = ''
        self.histName_forFlip_dD_CRttX_emu_systmc_Down = ''
        self.histName_forFlipFit_SR_ee = ''
        self.histName_forFlipFit_CRttX_ee = ''
        self.histName_forFlipDD_ee = ''
        self.histName_forFlip_dD_SR_ee = ''
        self.histName_forFlip_dD_SR_ee_systshape_Up = ''
        self.histName_forFlip_dD_SR_ee_systshape_Down = ''
        self.histName_forFlip_dD_SR_ee_systmc_Up = ''
        self.histName_forFlip_dD_SR_ee_systmc_Down = ''
        self.histName_forFlip_dD_CRttX_ee = ''
        self.histName_forFlip_dD_CRttX_ee_systshape_Up = ''
        self.histName_forFlip_dD_CRttX_ee_systshape_Down = ''
        self.histName_forFlip_dD_CRttX_ee_systmc_Up = ''
        self.histName_forFlip_dD_CRttX_ee_systmc_Down = ''

        # Fake leptons Fit specific objects
        self.mc_fake_files_forFit, self.mc_fake_files_intt1lregion_forFit, self.mc_fake_xsec_forFit, self.mc_fake_sf_forFit = [], [], [], []
        self.Fake_dD_h_copy_intt1lregion_forPlot_SR = ''
        self.Fake_dD_h_copy_intt1lregion_forPlot_CRttX = ''
        self.Fake_dD_h_copy_forFakePlot_SR = ''
        self.Fake_dD_h_copy_forFakePlot_CRttX = ''
        self.tt1lregion_data_file = ''
        self.dataDriven_Fake_normalization = 1.0
        self.histName_forFakeFit_SR_mumu = ''
        self.histName_forFakeFit_CRttX_mumu = ''
        self.histName_forFakeDD_mumu_SR = ''
        self.histName_forFakeDD_mumu_CRttX = ''
        self.histName_forFake_dD_SR_mumu = ''
        self.histName_forFake_dD_SR_mumu_systshape_Up = ''
        self.histName_forFake_dD_SR_mumu_systshape_Down = ''
        self.histName_forFake_dD_SR_mumu_systmc_Up = ''
        self.histName_forFake_dD_SR_mumu_systmc_Down = ''
        self.histName_forFake_dD_CRttX_mumu = ''
        self.histName_forFake_dD_CRttX_mumu_systshape_Up = ''
        self.histName_forFake_dD_CRttX_mumu_systshape_Down = ''
        self.histName_forFake_dD_CRttX_mumu_systmc_Up = ''
        self.histName_forFake_dD_CRttX_mumu_systmc_Down = ''
        self.histName_forFakeFit_SR_emu = ''
        self.histName_forFakeFit_CRttX_emu = ''
        self.histName_forFakeDD_emu_SR = ''
        self.histName_forFakeDD_emu_CRttX = ''
        self.histName_forFake_dD_SR_emu = ''
        self.histName_forFake_dD_SR_emu_systshape_Up = ''
        self.histName_forFake_dD_SR_emu_systshape_Down = ''
        self.histName_forFake_dD_SR_emu_systmc_Up = ''
        self.histName_forFake_dD_SR_emu_systmc_Down = ''
        self.histName_forFake_dD_CRttX_emu = ''
        self.histName_forFake_dD_CRttX_emu_systshape_Up = ''
        self.histName_forFake_dD_CRttX_emu_systshape_Down = ''
        self.histName_forFake_dD_CRttX_emu_systmc_Up = ''
        self.histName_forFake_dD_CRttX_emu_systmc_Down = ''
        self.histName_forFakeFit_SR_ee = ''
        self.histName_forFakeFit_CRttX_ee = ''
        self.histName_forFakeDD_ee_SR = ''
        self.histName_forFakeDD_ee_CRttX = ''
        self.histName_forFake_dD_SR_ee = ''
        self.histName_forFake_dD_SR_ee_systshape_Up = ''
        self.histName_forFake_dD_SR_ee_systshape_Down = ''
        self.histName_forFake_dD_SR_ee_systmc_Up = ''
        self.histName_forFake_dD_SR_ee_systmc_Down = ''
        self.histName_forFake_dD_CRttX_ee = ''
        self.histName_forFake_dD_CRttX_ee_systshape_Up = ''
        self.histName_forFake_dD_CRttX_ee_systshape_Down = ''
        self.histName_forFake_dD_CRttX_ee_systmc_Up = ''
        self.histName_forFake_dD_CRttX_ee_systmc_Down = ''

        self.data_file_list = []  # you can have more than one data file
        self.xsec_list, self.event_num_list, self.sf_list, self.resf_list, self.sumOfWeights_list = [], [], [], [], []

        self.mc_root_files, self.mc_counter_hist_files, self.mc_file_counter_hist_list = [], [], []
        self.data_root_files = []

        self.integer_lumi = integer_lumi

        self.stackOrder = [] # Order of the stack histogram stack contributions

        # bounding box location for the legends
        self.legend_x1, self.legend_y1, self.legend_x2, self.legend_y2 = 0.32, 0.78, 0.89, 0.89

        self.histogram_list, self.x_titles, self.y_titles, self.draw_modes, self.draw_options = [], [], [], [], []
        self.is_logy, self.bin_lists, self.underflow_bin = [], [], []
        self.ymin, self.ymax = [], []  # histogram maximum values

        # Initialize tdrStyle
        self.tdrStyle = tdrstyle.setTDRStyle(ROOT, tdrStyle)

        # Set tdrStyle
        ROOT.gROOT.SetStyle("tdrStyle")
        self.ROOT = ROOT

        self.extra_text_hists = []

        # Stuff for plotter
        self.iso_cut_iso_mu = ''
        self.iso_cut_rev_iso_mu = ''

        # Print all defined variables if verbosity is requested
        if verbose:
            print("~~~~~~~~~~~~~~~~~ StackHists class attributes ~~~~~~~~~~~~~~~~~")
            for k, v in self.__dict__.items():
                print(k, ":", v)
            print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")

    def __del__(self):
        print("Destruct")

    def add_mctt2l_channel_forFit(self, root_file, xsec=1.0):
        """Add Flip Monte Carlo Channel for Flip Fit
        """
        if os.path.isfile(root_file):
            self.mc_flip_files_forFit.append(root_file)
            self.mc_flip_xsec_forFit.append(xsec)
        else:
            print(f'Cannot add file {root_file}, it does not exist')
            print('Please Check')
            sys.exit(-1)

        parts = root_file.split('/')
        #if parts[-1].endswith('.root'):
        #    parts.insert(-1, self.Flipregion_folder.replace('/',''))
        
        root_file_intt2lregion = '/'.join(parts)

        if os.path.isfile(root_file_intt2lregion):
            self.mc_flip_files_intt2lregion_forFit.append(root_file_intt2lregion)
            print(f'adding file {root_file_intt2lregion}')
        else:
            print(f'Cannot add file {root_file_intt2lregion}, it does not exist')
            print('Please Check')
            sys.exit(-1)

        pass

    def add_mctt1l_channel_forFit(self, root_file, xsec=1.0):
        """Add Fake Monte Carlo Channel for Fake Fit
        """
        if os.path.isfile(root_file):
            self.mc_fake_files_forFit.append(root_file)
            self.mc_fake_xsec_forFit.append(xsec)
        else:
            print(f'Cannot add file {root_file}, it does not exist')
            print('Please Check')
            sys.exit(-1)

        parts = root_file.split('/')
        #if parts[-1].endswith('.root'):
        #    parts.insert(-1, self.Fakeregion_folder.replace('/',''))
        
        root_file_intt1lregion = '/'.join(parts)

        if os.path.isfile(root_file_intt1lregion):
            self.mc_fake_files_intt1lregion_forFit.append(root_file_intt1lregion)
            print(f'adding file {root_file_intt1lregion}')
        else:
            print(f'Cannot add file {root_file_intt1lregion}, it does not exist')
            print('Please Check')
            sys.exit(-1)

        pass

    def make_plot(self, histograms_info, outFile, case):
        print("\033[91mInside make_plot function\033[0m")

        canva = ROOT.TCanvas("", "", 1200, 900)
        if case == "Flip":
            legend = ROOT.TLegend(0.57, 0.6, 0.975, 0.95)  # Adjust legend position
        elif "Fake" in case:
            legend = ROOT.TLegend(0.52, 0.6, 0.975, 0.95)  # Adjust legend position
        max_y = 0.0  # Variable to store the maximum y-height

        for i, info in enumerate(histograms_info):
            histogram, color, title = info
            histogram.SetStats(ROOT.kFALSE)
            histogram.SetLineColor(color)
            histogram.SetLineWidth(3)  # Increase line width for better visibility
            histogram.SetMarkerStyle(20)  # Add markers
            histogram.SetMarkerColor(color)
            histogram.SetMarkerSize(1.0)
            if i == 0:
                histogram.Draw()
            else:
                histogram.Draw("SAME")

            # Update max_y if the histogram's maximum value is greater
            if histogram.GetMaximum() > max_y:
                max_y = histogram.GetMaximum()

            if case == "Flip":
                legend.SetTextSize(0.035)
            elif case == "Fake SR":
                legend.SetTextSize(0.03)
            elif case == "Fake CRttX":
                legend.SetTextSize(0.027)
            legend.AddEntry(histogram, title, "l")

        histograms_info[0][0].GetXaxis().SetTitle('M_{T,T\'} (GeV)')
        histograms_info[0][0].GetXaxis().SetTitleSize(0.04)  # Adjust axis label size
        histograms_info[0][0].GetXaxis().SetTitleOffset(1.2)  # Adjust axis label position

        histograms_info[0][0].GetYaxis().SetTitle("Events/bin (Normalized)")  # Add a label to the y-axis
        histograms_info[0][0].GetYaxis().SetTitleSize(0.04)  # Adjust axis label size
        histograms_info[0][0].GetYaxis().SetTitleOffset(1.4)  # Adjust axis label position

        # Set the maximum y-axis value 1.4 times above max_y
        max_y *= 1.4
        histograms_info[0][0].GetYaxis().SetRangeUser(0, max_y)

        legend.Draw()
        canva.SaveAs(outFile)

    def prepare_Flipfit(self, outPath, year, mass):
        zipped_lists = zip(self.mc_file_list,
                           self.mc_root_files, 
                           self.sf_list, 
                           range(len(self.xsec_list)))

        flip_zipped_list = zip(self.mc_flip_files_forFit,
                              self.mc_flip_xsec_forFit,
                              range(len(self.mc_flip_xsec_forFit)))

        # dataDriven_normalization_fromMC_FlipSR_mumu = 0.0
        dataDriven_normalization_fromMC_FlipSR_emu = 0.0
        dataDriven_normalization_fromMC_FlipSR_ee = 0.0
        # Flipmc_in_SR_h_mumu = None
        Flipmc_in_SR_h_emu = None
        Flipmc_in_SR_h_ee = None
        # dataDriven_normalization_fromMC_FlipCRttX_mumu = 0.0
        dataDriven_normalization_fromMC_FlipCRttX_emu = 0.0
        dataDriven_normalization_fromMC_FlipCRttX_ee = 0.0
        # Flipmc_in_CRttX_h_mumu = None
        Flipmc_in_CRttX_h_emu = None
        Flipmc_in_CRttX_h_ee = None
        # looping over all MC samples in SR and CR ttX to get the normalization
        for mc_flip_file, xsec_flip, range_id_flip in flip_zipped_list:
            print(f'{GREEN}mc_flip_file = {mc_flip_file}{RESET}')
            # print(f'{GREEN}xsec_flip = {xsec_flip}{RESET}')
            # print(f'{GREEN}range_id_flip = {range_id_flip}{RESET}')
            file = self.ROOT.TFile.Open(str(mc_flip_file))
            # hist_forFlipFit_SR_mumu = file.Get(self.histName_forFlipFit_SR_mumu)
            hist_forFlipFit_SR_emu = file.Get(self.histName_forFlipFit_SR_emu)
            hist_forFlipFit_SR_ee = file.Get(self.histName_forFlipFit_SR_ee)
            if hist_forFlipFit_SR_emu is None or hist_forFlipFit_SR_ee is None:
                print('Could not fetch histogram for fit')
                exit(1)
            # hist_forFlipFit_CRttX_mumu = file.Get(self.histName_forFlipFit_CRttX_mumu)
            hist_forFlipFit_CRttX_emu = file.Get(self.histName_forFlipFit_CRttX_emu)
            hist_forFlipFit_CRttX_ee = file.Get(self.histName_forFlipFit_CRttX_ee)
            if hist_forFlipFit_CRttX_emu is None or hist_forFlipFit_CRttX_ee is None:
                print('Could not fetch histogram for fit')
                exit(1)
            sumOfWeights_h = file.Get(self.sum_of_weights_hist_name)
            sumOfWeights = sumOfWeights_h.GetEntries()*sumOfWeights_h.GetMean()
            scaling = (xsec_flip * self.integer_lumi)/sumOfWeights
            print(f'{BCYAN} Weights 1: {scaling}{RESET}')
            # hist_forFlipFit_SR_mumu.Scale(scaling)
            hist_forFlipFit_SR_emu.Scale(scaling)
            hist_forFlipFit_SR_ee.Scale(scaling)
            # hist_forFlipFit_CRttX_mumu.Scale(scaling)
            hist_forFlipFit_CRttX_emu.Scale(scaling)
            hist_forFlipFit_CRttX_ee.Scale(scaling)
            # dataDriven_normalization_fromMC_FlipSR_mumu += hist_forFlipFit_SR_mumu.Integral() # normalization factor
            # if Flipmc_in_SR_h_mumu == None:
            #     Flipmc_in_SR_h_mumu = hist_forFlipFit_SR_mumu.Clone('Flipmc_in_SR_h_mumu')
            # else:
            #     Flipmc_in_SR_h_mumu.Add(hist_forFlipFit_SR_mumu)
            dataDriven_normalization_fromMC_FlipSR_emu += hist_forFlipFit_SR_emu.Integral()
            if Flipmc_in_SR_h_emu == None:
                Flipmc_in_SR_h_emu = hist_forFlipFit_SR_emu.Clone('Flipmc_in_SR_h_emu')
            else:
                Flipmc_in_SR_h_emu.Add(hist_forFlipFit_SR_emu)
            dataDriven_normalization_fromMC_FlipSR_ee += hist_forFlipFit_SR_ee.Integral()
            if Flipmc_in_SR_h_ee == None:
                Flipmc_in_SR_h_ee = hist_forFlipFit_SR_ee.Clone('Flipmc_in_SR_h_ee')
            else:
                Flipmc_in_SR_h_ee.Add(hist_forFlipFit_SR_ee)
            # dataDriven_normalization_fromMC_FlipCRttX_mumu += hist_forFlipFit_CRttX_mumu.Integral() # normalization factor
            # if Flipmc_in_CRttX_h_mumu == None:
            #     Flipmc_in_CRttX_h_mumu = hist_forFlipFit_CRttX_mumu.Clone('Flipmc_in_CRttX_h_mumu')
            # else:
            #     Flipmc_in_CRttX_h_mumu.Add(hist_forFlipFit_CRttX_mumu)
            dataDriven_normalization_fromMC_FlipCRttX_emu += hist_forFlipFit_CRttX_emu.Integral()
            if Flipmc_in_CRttX_h_emu == None:
                Flipmc_in_CRttX_h_emu = hist_forFlipFit_CRttX_emu.Clone('Flipmc_in_CRttX_h_emu')
            else:
                Flipmc_in_CRttX_h_emu.Add(hist_forFlipFit_CRttX_emu)
            dataDriven_normalization_fromMC_FlipCRttX_ee += hist_forFlipFit_CRttX_ee.Integral()
            if Flipmc_in_CRttX_h_ee == None:
                Flipmc_in_CRttX_h_ee = hist_forFlipFit_CRttX_ee.Clone('Flipmc_in_CRttX_h_ee')
            else:
                Flipmc_in_CRttX_h_ee.Add(hist_forFlipFit_CRttX_ee)
            file.Close()
        # print(f'{GREEN}Flip dataDriven normalization in SR mumu = {dataDriven_normalization_fromMC_FlipSR_mumu}{RESET}')
        print(f'{GREEN}Flip dataDriven normalization in SR emu = {dataDriven_normalization_fromMC_FlipSR_emu}{RESET}')
        print(f'{GREEN}Flip dataDriven normalization in SR ee = {dataDriven_normalization_fromMC_FlipSR_ee}{RESET}')
        # print(f'{GREEN}Flip dataDriven normalization in CR ttX mumu = {dataDriven_normalization_fromMC_FlipCRttX_mumu}{RESET}')
        print(f'{GREEN}Flip dataDriven normalization in CR ttX emu = {dataDriven_normalization_fromMC_FlipCRttX_emu}{RESET}')
        print(f'{GREEN}Flip dataDriven normalization in CR ttX ee = {dataDriven_normalization_fromMC_FlipCRttX_ee}{RESET}')

        if dataDriven_normalization_fromMC_FlipSR_emu==0.0 or dataDriven_normalization_fromMC_FlipCRttX_emu==0.0 or dataDriven_normalization_fromMC_FlipSR_ee==0.0 or dataDriven_normalization_fromMC_FlipCRttX_ee==0.0:
            print('dataDriven normalization scaling from Flip MC is null.\nExiting program.')
            exit(1)

        counterForDataFiles = 0
        # load data file in MR tt2l
        atfile = self.ROOT.TFile(self.tt2lregion_data_file)
        print(f'{BGREEN}Using {self.tt2lregion_data_file} for Data for Flip fit{RESET}')
        data_hist_forFlipFit = atfile.Get(self.histName_forFlipDD_emu)

        nbins = data_hist_forFlipFit.GetNbinsX()
        xmin = data_hist_forFlipFit.GetXaxis().GetXmin()
        xmax = data_hist_forFlipFit.GetXaxis().GetXmax()
        
        #if counterForDataFiles>1: 
        #    print(f'{BLUE}Careful, there is more than 1 root file during Flip fit{RESET}')

        flip_data_driven_processes_mumu = []
        tt2l_processes_mumu = []
        remaining_processes_mumu = []
        flip_data_driven_processes_emu = []
        tt2l_processes_emu = []
        remaining_processes_emu = []
        flip_data_driven_processes_ee = []
        tt2l_processes_ee = []
        remaining_processes_ee = []

        tt2l_h_mumu = self.ROOT.TH1D('tt2l_mumu', 'M_{T,T\'}; M_{T,T\'} (GeV);Events;', nbins, xmin, xmax)
        other_mc_h_mumu = self.ROOT.TH1D('other_mc_mumu', 'M_{T,T\'}; M_{T,T\'} (GeV);Events;', nbins, xmin, xmax)
        FlipdD_h_mumu = self.ROOT.TH1D('flip_dd_mumu', 'M_{T,T\'}; M_{T,T\'} (GeV);Events;', nbins, xmin, xmax)
        tt2l_h_emu = self.ROOT.TH1D('tt2l_emu', 'M_{T,T\'}; M_{T,T\'} (GeV);Events;', nbins, xmin, xmax)
        other_mc_h_emu = self.ROOT.TH1D('other_mc_emu', 'M_{T,T\'}; M_{T,T\'} (GeV);Events;', nbins, xmin, xmax)
        FlipdD_h_emu = self.ROOT.TH1D('flip_dd_emu', 'M_{T,T\'}; M_{T,T\'} (GeV);Events;', nbins, xmin, xmax)
        tt2l_h_ee = self.ROOT.TH1D('tt2l_ee', 'M_{T,T\'}; M_{T,T\'} (GeV);Events;', nbins, xmin, xmax)
        other_mc_h_ee = self.ROOT.TH1D('other_mc_ee', 'M_{T,T\'}; M_{T,T\'} (GeV);Events;', nbins, xmin, xmax)
        FlipdD_h_ee = self.ROOT.TH1D('flip_dd_ee', 'M_{T,T\'}; M_{T,T\'} (GeV);Events;', nbins, xmin, xmax)

        # Iterate over the zipped list
        for mc_file_name, mc_TFile, sf, idx in zipped_lists:
            # Check the process name and categorize accordingly
            hist_mumu = mc_TFile.Get(self.histName_forFlipDD_mumu)
            hist_emu = mc_TFile.Get(self.histName_forFlipDD_emu)
            hist_ee = mc_TFile.Get(self.histName_forFlipDD_ee)
            if 'Flip_dataDriven' in mc_file_name:
                FlipdD_h_mumu.Add(hist_mumu)
                flip_data_driven_processes_mumu.append((mc_file_name, sf, idx))
                FlipdD_h_emu.Add(hist_emu)
                flip_data_driven_processes_emu.append((mc_file_name, sf, idx))
                FlipdD_h_ee.Add(hist_ee)
                flip_data_driven_processes_ee.append((mc_file_name, sf, idx))
            elif 'TT_2L' in mc_file_name:
                hist_mumu = mc_TFile.Get(self.histName_forFlipDD_mumu + '_0')
                hist_emu = mc_TFile.Get(self.histName_forFlipDD_emu + '_0')
                hist_ee = mc_TFile.Get(self.histName_forFlipDD_ee + '_0')
                # hist_mumu.Scale(sf)
                # tt2l_h_mumu.Add(hist_mumu)
                # tt2l_processes_mumu.append((mc_file_name, sf, idx))
                hist_emu.Scale(sf)
                tt2l_h_emu.Add(hist_emu)
                tt2l_processes_emu.append((mc_file_name, sf, idx))
                hist_ee.Scale(sf)
                tt2l_h_ee.Add(hist_ee)
                tt2l_processes_ee.append((mc_file_name, sf, idx))
                hist_mumu = mc_TFile.Get(self.histName_forFlipDD_mumu + '_2')
                hist_emu = mc_TFile.Get(self.histName_forFlipDD_emu + '_2')
                hist_ee = mc_TFile.Get(self.histName_forFlipDD_ee + '_2')
                hist_mumu.Scale(sf)
                other_mc_h_mumu.Add(hist_mumu)
                remaining_processes_mumu.append((mc_file_name, sf, idx))
                hist_emu.Scale(sf)
                other_mc_h_emu.Add(hist_emu)
                remaining_processes_emu.append((mc_file_name, sf, idx))
                hist_ee.Scale(sf)
                other_mc_h_ee.Add(hist_ee)
                remaining_processes_ee.append((mc_file_name, sf, idx))
            elif not 'Fake_dataDriven' in mc_file_name:
                hist_mumu.Scale(sf)
                other_mc_h_mumu.Add(hist_mumu)
                remaining_processes_mumu.append((mc_file_name, sf, idx))
                hist_emu.Scale(sf)
                other_mc_h_emu.Add(hist_emu)
                remaining_processes_emu.append((mc_file_name, sf, idx))
                hist_ee.Scale(sf)
                other_mc_h_ee.Add(hist_ee)
                remaining_processes_ee.append((mc_file_name, sf, idx))

        # print(tt2l_h_mumu.Integral())
        print(tt2l_h_emu.Integral())
        print(tt2l_h_ee.Integral())
        FlipdD_h_mumu.Add(other_mc_h_mumu,-1)
        Flip_dD_h_copy_intt2lregion_forPlot_mumu = FlipdD_h_mumu.Clone()
        # dataDriven_integral_before_anyScaling_mumu = FlipdD_h_mumu.Integral()
        # print(f'{GREEN}Flip DataDriven Integral Before Scaling it to MC mumu = {dataDriven_integral_before_anyScaling_mumu}{RESET}')
        # FlipdD_h_mumu.Scale(dataDriven_normalization_fromMC_FlipSR_mumu/dataDriven_integral_before_anyScaling_mumu)
        # Flip_dD_h_copy_forFlipPlot_SR_mumu = FlipdD_h_mumu.Clone()
        # print(f'{GREEN}Flip DataDriven Integral After Scaling it to MC in SR mumu = {FlipdD_h_mumu.Integral()}{RESET}')
        # FlipdD_h_mumu.Scale(dataDriven_normalization_fromMC_FlipCRttX_mumu/dataDriven_normalization_fromMC_FlipSR_mumu)
        # Flip_dD_h_copy_forFlipPlot_CRttX_mumu = FlipdD_h_mumu.Clone()
        # print(f'{GREEN}Flip DataDriven Integral After Scaling it to MC in CR ttX mumu = {FlipdD_h_mumu.Integral()}{RESET}')
        FlipdD_h_emu.Add(other_mc_h_emu,-1)
        Flip_dD_h_copy_intt2lregion_forPlot_emu = FlipdD_h_emu.Clone()
        dataDriven_integral_before_anyScaling_emu = FlipdD_h_emu.Integral()
        print(f'{GREEN}Flip DataDriven Integral Before Scaling it to MC emu = {dataDriven_integral_before_anyScaling_emu}{RESET}')
        FlipdD_h_emu.Scale(dataDriven_normalization_fromMC_FlipSR_emu/dataDriven_integral_before_anyScaling_emu)
        Flip_dD_h_copy_forFlipPlot_SR_emu = FlipdD_h_emu.Clone()
        print(f'{GREEN}Flip DataDriven Integral After Scaling it to MC in SR emu = {Flip_dD_h_copy_forFlipPlot_SR_emu.Integral()}{RESET}')
        FlipdD_h_emu.Scale(dataDriven_normalization_fromMC_FlipCRttX_emu/dataDriven_normalization_fromMC_FlipSR_emu)
        Flip_dD_h_copy_forFlipPlot_CRttX_emu = FlipdD_h_emu.Clone()
        print(f'{GREEN}Flip DataDriven Integral After Scaling it to MC in CR ttX emu = {Flip_dD_h_copy_forFlipPlot_CRttX_emu.Integral()}{RESET}')
        FlipdD_h_ee.Add(other_mc_h_ee,-1)
        Flip_dD_h_copy_intt2lregion_forPlot_ee = FlipdD_h_ee.Clone()
        dataDriven_integral_before_anyScaling_ee = FlipdD_h_ee.Integral()
        print(f'{GREEN}Flip DataDriven Integral Before Scaling it to MC ee = {dataDriven_integral_before_anyScaling_ee}{RESET}')  
        FlipdD_h_ee.Scale(dataDriven_normalization_fromMC_FlipSR_ee/dataDriven_integral_before_anyScaling_ee)
        Flip_dD_h_copy_forFlipPlot_SR_ee = FlipdD_h_ee.Clone()
        print(f'{GREEN}Flip DataDriven Integral After Scaling it to MC in SR ee = {Flip_dD_h_copy_forFlipPlot_SR_ee.Integral()}{RESET}')
        FlipdD_h_ee.Scale(dataDriven_normalization_fromMC_FlipCRttX_ee/dataDriven_normalization_fromMC_FlipSR_ee)
        Flip_dD_h_copy_forFlipPlot_CRttX_ee = FlipdD_h_ee.Clone()
        print(f'{GREEN}Flip DataDriven Integral After Scaling it to MC in CR ttX ee = {Flip_dD_h_copy_forFlipPlot_CRttX_ee.Integral()}{RESET}')

        self.Flip_dD_h_copy_intt2lregion_forPlot = Flip_dD_h_copy_intt2lregion_forPlot_mumu.Clone()
        self.Flip_dD_h_copy_intt2lregion_forPlot.Add(Flip_dD_h_copy_intt2lregion_forPlot_emu)
        self.Flip_dD_h_copy_intt2lregion_forPlot.Add(Flip_dD_h_copy_intt2lregion_forPlot_ee)
        self.Flip_dD_h_copy_forFlipPlot_SR = Flip_dD_h_copy_forFlipPlot_SR_emu.Clone()
        self.Flip_dD_h_copy_forFlipPlot_SR.Add(Flip_dD_h_copy_forFlipPlot_SR_ee)
        print(f'{GREEN}Flip DataDriven Integral After Scaling it to MC in SR = {self.Flip_dD_h_copy_forFlipPlot_SR.Integral()}{RESET}')
        self.Flip_dD_h_copy_forFlipPlot_CRttX = Flip_dD_h_copy_forFlipPlot_CRttX_emu.Clone()
        self.Flip_dD_h_copy_forFlipPlot_CRttX.Add(Flip_dD_h_copy_forFlipPlot_CRttX_ee)
        print(f'{GREEN}Flip DataDriven Integral After Scaling it to MC in CR ttX = {self.Flip_dD_h_copy_forFlipPlot_CRttX.Integral()}{RESET}')

        if self.preFit:
            print(f'{BPURPLE}Inside preFit{RESET}')
            # CMS_lumi.extraText += 'prefit'
            # CMS_lumi.outputFileName += '_prefit'
            all_mc_SF = 1.0
            tt2l_SF = 1.0
            Flip_dataDriven_SF = 1.0

        print("all_other_mc_sf =", f'{all_mc_SF:.2f}')
        print("tt2l_sf =", f'{tt2l_SF:.2f}')
        print("Flip_dataDriven_sf =", f'{Flip_dataDriven_SF:.2f}')

        # print(f'flip_data_driven_processes_mumu={flip_data_driven_processes_mumu}')
        # print(f'tt2l_processeses_mumu={tt2l_processeses_mumu}')
        # print(f'remaining_processes_mumu={remaining_processes_mumu}')
        # print(f'flip_data_driven_processes_emu={flip_data_driven_processes_emu}')
        # print(f'tt2l_processeses_emu={tt2l_processeses_emu}')
        # print(f'remaining_processes_emu={remaining_processes_emu}')
        # print(f'flip_data_driven_processes_ee={flip_data_driven_processes_ee}')
        # print(f'tt2l_processeses_ee={tt2l_processeses_ee}')
        # print(f'remaining_processes_ee={remaining_processes_ee}')

        # Applying Flip_dataDriven_SF to Flip data driven processes (scaling to lumi)
        # for flip_data_driven_process in flip_data_driven_processes_mumu:
        #     # print(f'{GREEN}Applying dataDriven_normalization to {flip_data_driven_process[0].split("/")[-1].replace(".root","")}{RESET}')
        #     self.sf_list[flip_data_driven_process[2]] *= Flip_dataDriven_SF # * self.dataDriven_Flip_normalization

        for flip_data_driven_process in flip_data_driven_processes_emu:
            # print(f'{GREEN}Applying dataDriven_normalization to {flip_data_driven_process[0].split("/")[-1].replace(".root","")}{RESET}')
            self.sf_list[flip_data_driven_process[2]] *= Flip_dataDriven_SF # * self.dataDriven_Flip_normalization

        for flip_data_driven_process in flip_data_driven_processes_ee:
            # print(f'{GREEN}Applying dataDriven_normalization to {flip_data_driven_process[0].split("/")[-1].replace(".root","")}{RESET}')
            self.sf_list[flip_data_driven_process[2]] *= Flip_dataDriven_SF # * self.dataDriven_Flip_normalization

        # for tt2l_process in tt2l_processes_mumu:
        #     # print(f'{GREEN}Applying tt2l_SF to {tt2l_process[0].split("/")[-1].replace(".root","")}{RESET}')
        #     self.sf_list[tt2l_process[2]] *= tt2l_SF
        
        for tt2l_process in tt2l_processes_emu:
            # print(f'{GREEN}Applying tt2l_SF to {tt2l_process[0].split("/")[-1].replace(".root","")}{RESET}')
            self.sf_list[tt2l_process[2]] *= tt2l_SF
        
        for tt2l_process in tt2l_processes_ee:
            # print(f'{GREEN}Applying tt2l_SF to {tt2l_process[0].split("/")[-1].replace(".root","")}{RESET}')
            self.sf_list[tt2l_process[2]] *= tt2l_SF

        # Applying all_mc_SF to remaining processes
        # for remaining_process in remaining_processes_mumu:
        #     # print(f'{GREEN}Applying all_other_mc_SF to {remaining_process[0].split("/")[-1].replace(".root","")}{RESET}')
        #     self.sf_list[remaining_process[2]] *= all_mc_SF

        for remaining_process in remaining_processes_emu:
            # print(f'{GREEN}Applying all_other_mc_SF to {remaining_process[0].split("/")[-1].replace(".root","")}{RESET}')
            self.sf_list[remaining_process[2]] *= all_mc_SF

        for remaining_process in remaining_processes_ee:
            # print(f'{GREEN}Applying all_other_mc_SF to {remaining_process[0].split("/")[-1].replace(".root","")}{RESET}')
            self.sf_list[remaining_process[2]] *= all_mc_SF

        Flipmc_in_SR_h = Flipmc_in_SR_h_emu.Clone()
        Flipmc_in_SR_h.Add(Flipmc_in_SR_h_ee)
        Flipmc_in_CRttX_h = Flipmc_in_CRttX_h_emu.Clone()
        Flipmc_in_CRttX_h.Add(Flipmc_in_CRttX_h_ee)
        mcFlip_intt2lregion_hist = tt2l_h_emu.Clone()
        mcFlip_intt2lregion_hist.Add(tt2l_h_ee)
        Flip_dD_h_copy_intt2lregion_forPlot_Clone = self.Flip_dD_h_copy_intt2lregion_forPlot.Clone()
        Flip_dD_h_copy_intt2lregion_forPlot_Clone.Scale(1/Flip_dD_h_copy_intt2lregion_forPlot_Clone.Integral())
        Flipmc_in_SR_h.Scale(1/Flipmc_in_SR_h.Integral())
        Flipmc_in_CRttX_h.Scale(1/Flipmc_in_CRttX_h.Integral())
        mcFlip_intt2lregion_hist.Scale(1/mcFlip_intt2lregion_hist.Integral())
        histosToPlot = [
            [Flip_dD_h_copy_intt2lregion_forPlot_Clone, ROOT.kRed, "Data in MR tt2l (bkg subt)"],
            [mcFlip_intt2lregion_hist, ROOT.kGreen, "MC Charge flip in MR tt2l"],
            [Flipmc_in_SR_h, ROOT.kBlue, "MC Charge flip in SR"],
            [Flipmc_in_CRttX_h, ROOT.kMagenta, "MC Charge flip in CR ttX"]
        ]

        self.make_plot(histosToPlot, f"{outPath}plots/comparison_Flip_plot_Signal" + mass + "_UL" + year + ".png", "Flip")

        # save histograms for combine
        # Flip_dD_h_copy_forFlipPlot_SR_mumu_systshape_Up = tt2l_h_mumu.Clone()
        # Flip_dD_h_copy_forFlipPlot_SR_mumu_systshape_Up.Scale(dataDriven_normalization_fromMC_FlipSR_mumu/dataDriven_integral_before_anyScaling_mumu)
        # Flip_dD_h_copy_forFlipPlot_SR_mumu_systshape_Down = Flip_dD_h_copy_intt2lregion_forPlot_mumu.Clone()
        # Flip_dD_h_copy_forFlipPlot_SR_mumu_systshape_Down.Add(Flip_dD_h_copy_intt2lregion_forPlot_mumu)
        # Flip_dD_h_copy_forFlipPlot_SR_mumu_systshape_Down.Add(tt2l_h_mumu,-1)
        # Flip_dD_h_copy_forFlipPlot_SR_mumu_systshape_Down.Scale(dataDriven_normalization_fromMC_FlipSR_mumu/dataDriven_integral_before_anyScaling_mumu)
        # Flip_dD_h_copy_forFlipPlot_SR_mumu_systmc_Up = Flip_dD_h_copy_forFlipPlot_SR_mumu.Clone()
        # Flip_dD_h_copy_forFlipPlot_SR_mumu_systmc_Up.Add(Flip_dD_h_copy_forFlipPlot_SR_mumu_systshape_Up)
        # Flip_dD_h_copy_forFlipPlot_SR_mumu_systmc_Up.Add(Flipmc_in_SR_h_mumu,-1)
        # Flip_dD_h_copy_forFlipPlot_SR_mumu_systmc_Down = Flip_dD_h_copy_forFlipPlot_SR_mumu.Clone()
        # Flip_dD_h_copy_forFlipPlot_SR_mumu_systmc_Down.Add(Flip_dD_h_copy_forFlipPlot_SR_mumu_systshape_Up,-1)
        # Flip_dD_h_copy_forFlipPlot_SR_mumu_systmc_Down.Add(Flipmc_in_SR_h_mumu)
        # Flip_dD_h_copy_forFlipPlot_CRttX_mumu_systshape_Up = tt2l_h_mumu.Clone()
        # Flip_dD_h_copy_forFlipPlot_CRttX_mumu_systshape_Up.Scale(dataDriven_normalization_fromMC_FlipCRttX_mumu/dataDriven_integral_before_anyScaling_mumu)
        # Flip_dD_h_copy_forFlipPlot_CRttX_mumu_systshape_Down = Flip_dD_h_copy_intt2lregion_forPlot_mumu.Clone()
        # Flip_dD_h_copy_forFlipPlot_CRttX_mumu_systshape_Down.Add(Flip_dD_h_copy_intt2lregion_forPlot_mumu)
        # Flip_dD_h_copy_forFlipPlot_CRttX_mumu_systshape_Down.Add(tt2l_h_mumu,-1)
        # Flip_dD_h_copy_forFlipPlot_CRttX_mumu_systshape_Down.Scale(dataDriven_normalization_fromMC_FlipCRttX_mumu/dataDriven_integral_before_anyScaling_mumu)
        # Flip_dD_h_copy_forFlipPlot_CRttX_mumu_systmc_Up = Flip_dD_h_copy_forFlipPlot_CRttX_mumu.Clone()
        # Flip_dD_h_copy_forFlipPlot_CRttX_mumu_systmc_Up.Add(Flip_dD_h_copy_forFlipPlot_CRttX_mumu_systshape_Up)
        # Flip_dD_h_copy_forFlipPlot_CRttX_mumu_systmc_Up.Add(Flipmc_in_CRttX_h_mumu,-1)
        # Flip_dD_h_copy_forFlipPlot_CRttX_mumu_systmc_Down = Flip_dD_h_copy_forFlipPlot_CRttX_mumu.Clone()
        # Flip_dD_h_copy_forFlipPlot_CRttX_mumu_systmc_Down.Add(Flip_dD_h_copy_forFlipPlot_CRttX_mumu_systshape_Up,-1)
        # Flip_dD_h_copy_forFlipPlot_CRttX_mumu_systmc_Down.Add(Flipmc_in_CRttX_h_mumu)
        Flip_dD_h_copy_forFlipPlot_SR_emu_systshape_Up = tt2l_h_emu.Clone()
        Flip_dD_h_copy_forFlipPlot_SR_emu_systshape_Up.Scale(dataDriven_normalization_fromMC_FlipSR_emu/dataDriven_integral_before_anyScaling_emu)
        Flip_dD_h_copy_forFlipPlot_SR_emu_systshape_Down = Flip_dD_h_copy_intt2lregion_forPlot_emu.Clone()
        Flip_dD_h_copy_forFlipPlot_SR_emu_systshape_Down.Add(Flip_dD_h_copy_intt2lregion_forPlot_emu)
        Flip_dD_h_copy_forFlipPlot_SR_emu_systshape_Down.Add(tt2l_h_emu,-1)
        Flip_dD_h_copy_forFlipPlot_SR_emu_systshape_Down.Scale(dataDriven_normalization_fromMC_FlipSR_emu/dataDriven_integral_before_anyScaling_emu)
        Flip_dD_h_copy_forFlipPlot_SR_emu_systmc_Up = Flip_dD_h_copy_forFlipPlot_SR_emu.Clone()
        Flip_dD_h_copy_forFlipPlot_SR_emu_systmc_Up.Add(Flip_dD_h_copy_forFlipPlot_SR_emu_systshape_Up)
        Flip_dD_h_copy_forFlipPlot_SR_emu_systmc_Up.Add(Flipmc_in_SR_h_emu,-1)
        Flip_dD_h_copy_forFlipPlot_SR_emu_systmc_Down = Flip_dD_h_copy_forFlipPlot_SR_emu.Clone()
        Flip_dD_h_copy_forFlipPlot_SR_emu_systmc_Down.Add(Flip_dD_h_copy_forFlipPlot_SR_emu_systshape_Up,-1)
        Flip_dD_h_copy_forFlipPlot_SR_emu_systmc_Down.Add(Flipmc_in_SR_h_emu)
        Flip_dD_h_copy_forFlipPlot_CRttX_emu_systshape_Up = tt2l_h_emu.Clone()
        Flip_dD_h_copy_forFlipPlot_CRttX_emu_systshape_Up.Scale(dataDriven_normalization_fromMC_FlipCRttX_emu/dataDriven_integral_before_anyScaling_emu)
        Flip_dD_h_copy_forFlipPlot_CRttX_emu_systshape_Down = Flip_dD_h_copy_intt2lregion_forPlot_emu.Clone()
        Flip_dD_h_copy_forFlipPlot_CRttX_emu_systshape_Down.Add(Flip_dD_h_copy_intt2lregion_forPlot_emu)
        Flip_dD_h_copy_forFlipPlot_CRttX_emu_systshape_Down.Add(tt2l_h_emu,-1)
        Flip_dD_h_copy_forFlipPlot_CRttX_emu_systshape_Down.Scale(dataDriven_normalization_fromMC_FlipCRttX_emu/dataDriven_integral_before_anyScaling_emu)
        Flip_dD_h_copy_forFlipPlot_CRttX_emu_systmc_Up = Flip_dD_h_copy_forFlipPlot_CRttX_emu.Clone()
        Flip_dD_h_copy_forFlipPlot_CRttX_emu_systmc_Up.Add(Flip_dD_h_copy_forFlipPlot_CRttX_emu_systshape_Up)
        Flip_dD_h_copy_forFlipPlot_CRttX_emu_systmc_Up.Add(Flipmc_in_CRttX_h_emu,-1)
        Flip_dD_h_copy_forFlipPlot_CRttX_emu_systmc_Down = Flip_dD_h_copy_forFlipPlot_CRttX_emu.Clone()
        Flip_dD_h_copy_forFlipPlot_CRttX_emu_systmc_Down.Add(Flip_dD_h_copy_forFlipPlot_CRttX_emu_systshape_Up,-1)
        Flip_dD_h_copy_forFlipPlot_CRttX_emu_systmc_Down.Add(Flipmc_in_CRttX_h_emu)
        Flip_dD_h_copy_forFlipPlot_SR_ee_systshape_Up = tt2l_h_ee.Clone()
        Flip_dD_h_copy_forFlipPlot_SR_ee_systshape_Up.Scale(dataDriven_normalization_fromMC_FlipSR_ee/dataDriven_integral_before_anyScaling_ee)
        Flip_dD_h_copy_forFlipPlot_SR_ee_systshape_Down = Flip_dD_h_copy_intt2lregion_forPlot_ee.Clone()
        Flip_dD_h_copy_forFlipPlot_SR_ee_systshape_Down.Add(Flip_dD_h_copy_intt2lregion_forPlot_ee)
        Flip_dD_h_copy_forFlipPlot_SR_ee_systshape_Down.Add(tt2l_h_ee,-1)
        Flip_dD_h_copy_forFlipPlot_SR_ee_systshape_Down.Scale(dataDriven_normalization_fromMC_FlipSR_ee/dataDriven_integral_before_anyScaling_ee)
        Flip_dD_h_copy_forFlipPlot_SR_ee_systmc_Up = Flip_dD_h_copy_forFlipPlot_SR_ee.Clone()
        Flip_dD_h_copy_forFlipPlot_SR_ee_systmc_Up.Add(Flip_dD_h_copy_forFlipPlot_SR_ee_systshape_Up)
        Flip_dD_h_copy_forFlipPlot_SR_ee_systmc_Up.Add(Flipmc_in_SR_h_ee,-1)
        Flip_dD_h_copy_forFlipPlot_SR_ee_systmc_Down = Flip_dD_h_copy_forFlipPlot_SR_ee.Clone()
        Flip_dD_h_copy_forFlipPlot_SR_ee_systmc_Down.Add(Flip_dD_h_copy_forFlipPlot_SR_ee_systshape_Up,-1)
        Flip_dD_h_copy_forFlipPlot_SR_ee_systmc_Down.Add(Flipmc_in_SR_h_ee)
        Flip_dD_h_copy_forFlipPlot_CRttX_ee_systshape_Up = tt2l_h_ee.Clone()
        Flip_dD_h_copy_forFlipPlot_CRttX_ee_systshape_Up.Scale(dataDriven_normalization_fromMC_FlipCRttX_ee/dataDriven_integral_before_anyScaling_ee)
        Flip_dD_h_copy_forFlipPlot_CRttX_ee_systshape_Down = Flip_dD_h_copy_intt2lregion_forPlot_ee.Clone()
        Flip_dD_h_copy_forFlipPlot_CRttX_ee_systshape_Down.Add(Flip_dD_h_copy_intt2lregion_forPlot_ee)
        Flip_dD_h_copy_forFlipPlot_CRttX_ee_systshape_Down.Add(tt2l_h_ee,-1)
        Flip_dD_h_copy_forFlipPlot_CRttX_ee_systshape_Down.Scale(dataDriven_normalization_fromMC_FlipCRttX_ee/dataDriven_integral_before_anyScaling_ee)
        Flip_dD_h_copy_forFlipPlot_CRttX_ee_systmc_Up = Flip_dD_h_copy_forFlipPlot_CRttX_ee.Clone()
        Flip_dD_h_copy_forFlipPlot_CRttX_ee_systmc_Up.Add(Flip_dD_h_copy_forFlipPlot_CRttX_ee_systshape_Up)
        Flip_dD_h_copy_forFlipPlot_CRttX_ee_systmc_Up.Add(Flipmc_in_CRttX_h_ee,-1)
        Flip_dD_h_copy_forFlipPlot_CRttX_ee_systmc_Down = Flip_dD_h_copy_forFlipPlot_CRttX_ee.Clone()
        Flip_dD_h_copy_forFlipPlot_CRttX_ee_systmc_Down.Add(Flip_dD_h_copy_forFlipPlot_CRttX_ee_systshape_Up,-1)
        Flip_dD_h_copy_forFlipPlot_CRttX_ee_systmc_Down.Add(Flipmc_in_CRttX_h_ee)

        # print(Flip_dD_h_copy_forFlipPlot_SR_mumu_systshape_Up.Integral())
        # print(Flip_dD_h_copy_forFlipPlot_SR_mumu_systshape_Down.Integral())
        # print(Flip_dD_h_copy_forFlipPlot_SR_mumu_systmc_Up.Integral())
        # print(Flip_dD_h_copy_forFlipPlot_SR_mumu_systmc_Down.Integral())
        print(Flip_dD_h_copy_forFlipPlot_SR_emu_systshape_Up.Integral())
        print(Flip_dD_h_copy_forFlipPlot_SR_emu_systshape_Down.Integral())
        print(Flip_dD_h_copy_forFlipPlot_SR_emu_systmc_Up.Integral())
        print(Flip_dD_h_copy_forFlipPlot_SR_emu_systmc_Down.Integral())
        print(Flip_dD_h_copy_forFlipPlot_SR_ee_systshape_Up.Integral())
        print(Flip_dD_h_copy_forFlipPlot_SR_ee_systshape_Down.Integral())
        print(Flip_dD_h_copy_forFlipPlot_SR_ee_systmc_Up.Integral())
        print(Flip_dD_h_copy_forFlipPlot_SR_ee_systmc_Down.Integral())
        # print(Flip_dD_h_copy_forFlipPlot_CRttX_mumu_systshape_Up.Integral())
        # print(Flip_dD_h_copy_forFlipPlot_CRttX_mumu_systshape_Down.Integral())
        # print(Flip_dD_h_copy_forFlipPlot_CRttX_mumu_systmc_Up.Integral())
        # print(Flip_dD_h_copy_forFlipPlot_CRttX_mumu_systmc_Down.Integral())
        print(Flip_dD_h_copy_forFlipPlot_CRttX_emu_systshape_Up.Integral())
        print(Flip_dD_h_copy_forFlipPlot_CRttX_emu_systshape_Down.Integral())
        print(Flip_dD_h_copy_forFlipPlot_CRttX_emu_systmc_Up.Integral())
        print(Flip_dD_h_copy_forFlipPlot_CRttX_emu_systmc_Down.Integral())
        print(Flip_dD_h_copy_forFlipPlot_CRttX_ee_systshape_Up.Integral())
        print(Flip_dD_h_copy_forFlipPlot_CRttX_ee_systshape_Down.Integral())
        print(Flip_dD_h_copy_forFlipPlot_CRttX_ee_systmc_Up.Integral())
        print(Flip_dD_h_copy_forFlipPlot_CRttX_ee_systmc_Down.Integral())
        
        outputFile = ROOT.TFile.Open(outPath + 'Flip_' + mass + '_UL' + year + '.root','RECREATE')
        # outputFile.WriteObject(Flip_dD_h_copy_forFlipPlot_SR_mumu, self.histName_forFlip_dD_SR_mumu)
        # outputFile.WriteObject(Flip_dD_h_copy_forFlipPlot_SR_mumu_systshape_Up, self.histName_forFlip_dD_SR_mumu_systshape_Up)
        # outputFile.WriteObject(Flip_dD_h_copy_forFlipPlot_SR_mumu_systshape_Down, self.histName_forFlip_dD_SR_mumu_systshape_Down)
        # outputFile.WriteObject(Flip_dD_h_copy_forFlipPlot_SR_mumu_systmc_Up, self.histName_forFlip_dD_SR_mumu_systmc_Up)
        # outputFile.WriteObject(Flip_dD_h_copy_forFlipPlot_SR_mumu_systmc_Down, self.histName_forFlip_dD_SR_mumu_systmc_Down)
        # outputFile.WriteObject(Flip_dD_h_copy_forFlipPlot_CRttX_mumu, self.histName_forFlip_dD_CRttX_mumu)
        # outputFile.WriteObject(Flip_dD_h_copy_forFlipPlot_CRttX_mumu_systshape_Up, self.histName_forFlip_dD_CRttX_mumu_systshape_Up)
        # outputFile.WriteObject(Flip_dD_h_copy_forFlipPlot_CRttX_mumu_systshape_Down, self.histName_forFlip_dD_CRttX_mumu_systshape_Down)
        # outputFile.WriteObject(Flip_dD_h_copy_forFlipPlot_CRttX_mumu_systmc_Up, self.histName_forFlip_dD_CRttX_mumu_systmc_Up)
        # outputFile.WriteObject(Flip_dD_h_copy_forFlipPlot_CRttX_mumu_systmc_Down, self.histName_forFlip_dD_CRttX_mumu_systmc_Down)
        outputFile.WriteObject(Flip_dD_h_copy_forFlipPlot_SR_emu, self.histName_forFlip_dD_SR_emu)
        outputFile.WriteObject(Flip_dD_h_copy_forFlipPlot_SR_emu_systshape_Up, self.histName_forFlip_dD_SR_emu_systshape_Up)
        outputFile.WriteObject(Flip_dD_h_copy_forFlipPlot_SR_emu_systshape_Down, self.histName_forFlip_dD_SR_emu_systshape_Down)
        outputFile.WriteObject(Flip_dD_h_copy_forFlipPlot_SR_emu_systmc_Up, self.histName_forFlip_dD_SR_emu_systmc_Up)
        outputFile.WriteObject(Flip_dD_h_copy_forFlipPlot_SR_emu_systmc_Down, self.histName_forFlip_dD_SR_emu_systmc_Down)
        outputFile.WriteObject(Flip_dD_h_copy_forFlipPlot_CRttX_emu, self.histName_forFlip_dD_CRttX_emu)
        outputFile.WriteObject(Flip_dD_h_copy_forFlipPlot_CRttX_emu_systshape_Up, self.histName_forFlip_dD_CRttX_emu_systshape_Up)
        outputFile.WriteObject(Flip_dD_h_copy_forFlipPlot_CRttX_emu_systshape_Down, self.histName_forFlip_dD_CRttX_emu_systshape_Down)
        outputFile.WriteObject(Flip_dD_h_copy_forFlipPlot_CRttX_emu_systmc_Up, self.histName_forFlip_dD_CRttX_emu_systmc_Up)
        outputFile.WriteObject(Flip_dD_h_copy_forFlipPlot_CRttX_emu_systmc_Down, self.histName_forFlip_dD_CRttX_emu_systmc_Down)
        outputFile.WriteObject(Flip_dD_h_copy_forFlipPlot_SR_ee, self.histName_forFlip_dD_SR_ee)
        outputFile.WriteObject(Flip_dD_h_copy_forFlipPlot_SR_ee_systshape_Up, self.histName_forFlip_dD_SR_ee_systshape_Up)
        outputFile.WriteObject(Flip_dD_h_copy_forFlipPlot_SR_ee_systshape_Down, self.histName_forFlip_dD_SR_ee_systshape_Down)
        outputFile.WriteObject(Flip_dD_h_copy_forFlipPlot_SR_ee_systmc_Up, self.histName_forFlip_dD_SR_ee_systmc_Up)
        outputFile.WriteObject(Flip_dD_h_copy_forFlipPlot_SR_ee_systmc_Down, self.histName_forFlip_dD_SR_ee_systmc_Down)
        outputFile.WriteObject(Flip_dD_h_copy_forFlipPlot_CRttX_ee, self.histName_forFlip_dD_CRttX_ee)
        outputFile.WriteObject(Flip_dD_h_copy_forFlipPlot_CRttX_ee_systshape_Up, self.histName_forFlip_dD_CRttX_ee_systshape_Up)
        outputFile.WriteObject(Flip_dD_h_copy_forFlipPlot_CRttX_ee_systshape_Down, self.histName_forFlip_dD_CRttX_ee_systshape_Down)
        outputFile.WriteObject(Flip_dD_h_copy_forFlipPlot_CRttX_ee_systmc_Up, self.histName_forFlip_dD_CRttX_ee_systmc_Up)
        outputFile.WriteObject(Flip_dD_h_copy_forFlipPlot_CRttX_ee_systmc_Down, self.histName_forFlip_dD_CRttX_ee_systmc_Down)
        outputFile.Close()

    def prepare_Fakefit(self, outPath, year, mass):
        zipped_lists = zip(self.mc_file_list,
                           self.mc_root_files, 
                           self.sf_list, 
                           range(len(self.xsec_list)))

        fake_zipped_list = zip(self.mc_fake_files_forFit,
                              self.mc_fake_xsec_forFit,
                              range(len(self.mc_fake_xsec_forFit)))

        dataDriven_normalization_fromMC_FakeSR_mumu = 0.0
        dataDriven_normalization_fromMC_FakeSR_emu = 0.0
        dataDriven_normalization_fromMC_FakeSR_ee = 0.0
        Fakemc_in_SR_h_mumu = None
        Fakemc_in_SR_h_emu = None
        Fakemc_in_SR_h_ee = None
        dataDriven_normalization_fromMC_FakeCRttX_mumu = 0.0
        dataDriven_normalization_fromMC_FakeCRttX_emu = 0.0
        dataDriven_normalization_fromMC_FakeCRttX_ee = 0.0
        Fakemc_in_CRttX_h_mumu = None
        Fakemc_in_CRttX_h_emu = None
        Fakemc_in_CRttX_h_ee = None
        # looping over all MC samples in SR and CR ttX to get the normalization
        for mc_fake_file, xsec_fake, range_id_fake in fake_zipped_list:
            print(f'{GREEN}mc_fake_file = {mc_fake_file}{RESET}')
            # print(f'{GREEN}xsec_fake = {xsec_fake}{RESET}')
            # print(f'{GREEN}range_id_fake = {range_id_fake}{RESET}')
            file = self.ROOT.TFile.Open(str(mc_fake_file))
            hist_forFakeFit_SR_mumu = file.Get(self.histName_forFakeFit_SR_mumu)
            hist_forFakeFit_SR_emu = file.Get(self.histName_forFakeFit_SR_emu)
            hist_forFakeFit_SR_ee = file.Get(self.histName_forFakeFit_SR_ee)
            if hist_forFakeFit_SR_emu is None or hist_forFakeFit_SR_ee is None:
                print('Could not fetch histogram for fit')
                exit(1)
            hist_forFakeFit_CRttX_mumu = file.Get(self.histName_forFakeFit_CRttX_mumu)
            hist_forFakeFit_CRttX_emu = file.Get(self.histName_forFakeFit_CRttX_emu)
            hist_forFakeFit_CRttX_ee = file.Get(self.histName_forFakeFit_CRttX_ee)
            if hist_forFakeFit_CRttX_emu is None or hist_forFakeFit_CRttX_ee is None:
                print('Could not fetch histogram for fit')
                exit(1)
            sumOfWeights_h = file.Get(self.sum_of_weights_hist_name)
            sumOfWeights = sumOfWeights_h.GetEntries()*sumOfWeights_h.GetMean()
            scaling = (xsec_fake * self.integer_lumi)/sumOfWeights
            print(f'{BCYAN} Weights 1: {scaling}{RESET}')
            hist_forFakeFit_SR_mumu.Scale(scaling)
            hist_forFakeFit_SR_emu.Scale(scaling)
            hist_forFakeFit_SR_ee.Scale(scaling)
            hist_forFakeFit_CRttX_mumu.Scale(scaling)
            hist_forFakeFit_CRttX_emu.Scale(scaling)
            hist_forFakeFit_CRttX_ee.Scale(scaling)
            dataDriven_normalization_fromMC_FakeSR_mumu += hist_forFakeFit_SR_mumu.Integral() # normalization factor
            if Fakemc_in_SR_h_mumu == None:
                Fakemc_in_SR_h_mumu = hist_forFakeFit_SR_mumu.Clone('Fakemc_in_SR_h_mumu')
            else:
                Fakemc_in_SR_h_mumu.Add(hist_forFakeFit_SR_mumu)
            dataDriven_normalization_fromMC_FakeSR_emu += hist_forFakeFit_SR_emu.Integral()
            if Fakemc_in_SR_h_emu == None:
                Fakemc_in_SR_h_emu = hist_forFakeFit_SR_emu.Clone('Fakemc_in_SR_h_emu')
            else:
                Fakemc_in_SR_h_emu.Add(hist_forFakeFit_SR_emu)
            dataDriven_normalization_fromMC_FakeSR_ee += hist_forFakeFit_SR_ee.Integral()
            if Fakemc_in_SR_h_ee == None:
                Fakemc_in_SR_h_ee = hist_forFakeFit_SR_ee.Clone('Fakemc_in_SR_h_ee')
            else:
                Fakemc_in_SR_h_ee.Add(hist_forFakeFit_SR_ee)
            dataDriven_normalization_fromMC_FakeCRttX_mumu += hist_forFakeFit_CRttX_mumu.Integral() # normalization factor
            if Fakemc_in_CRttX_h_mumu == None:
                Fakemc_in_CRttX_h_mumu = hist_forFakeFit_CRttX_mumu.Clone('Fakemc_in_CRttX_h_mumu')
            else:
                Fakemc_in_CRttX_h_mumu.Add(hist_forFakeFit_CRttX_mumu)
            dataDriven_normalization_fromMC_FakeCRttX_emu += hist_forFakeFit_CRttX_emu.Integral()
            if Fakemc_in_CRttX_h_emu == None:
                Fakemc_in_CRttX_h_emu = hist_forFakeFit_CRttX_emu.Clone('Fakemc_in_CRttX_h_emu')
            else:
                Fakemc_in_CRttX_h_emu.Add(hist_forFakeFit_CRttX_emu)
            dataDriven_normalization_fromMC_FakeCRttX_ee += hist_forFakeFit_CRttX_ee.Integral()
            if Fakemc_in_CRttX_h_ee == None:
                Fakemc_in_CRttX_h_ee = hist_forFakeFit_CRttX_ee.Clone('Fakemc_in_CRttX_h_ee')
            else:
                Fakemc_in_CRttX_h_ee.Add(hist_forFakeFit_CRttX_ee)
            file.Close()
        print(f'{GREEN}Fake dataDriven normalization in SR mumu = {dataDriven_normalization_fromMC_FakeSR_mumu}{RESET}')
        print(f'{GREEN}Fake dataDriven normalization in SR emu = {dataDriven_normalization_fromMC_FakeSR_emu}{RESET}')
        print(f'{GREEN}Fake dataDriven normalization in SR ee = {dataDriven_normalization_fromMC_FakeSR_ee}{RESET}')
        print(f'{GREEN}Fake dataDriven normalization in CR ttX mumu = {dataDriven_normalization_fromMC_FakeCRttX_mumu}{RESET}')
        print(f'{GREEN}Fake dataDriven normalization in CR ttX emu = {dataDriven_normalization_fromMC_FakeCRttX_emu}{RESET}')
        print(f'{GREEN}Fake dataDriven normalization in CR ttX ee = {dataDriven_normalization_fromMC_FakeCRttX_ee}{RESET}')

        if dataDriven_normalization_fromMC_FakeSR_emu==0.0 or dataDriven_normalization_fromMC_FakeCRttX_emu==0.0 or dataDriven_normalization_fromMC_FakeSR_ee==0.0 or dataDriven_normalization_fromMC_FakeCRttX_ee==0.0:
            print('dataDriven normalization scaling from Fake MC is null.\nExiting program.')
            exit(1)

        counterForDataFiles = 0
        # load data file in MR tt1l
        atfile = self.ROOT.TFile(self.tt1lregion_data_file)
        print(f'{BGREEN}Using {self.tt1lregion_data_file} for Data for Fake fit{RESET}')
        data_hist_forFakeFit = atfile.Get(self.histName_forFakeDD_mumu_SR)

        nbins = data_hist_forFakeFit.GetNbinsX()
        xmin = data_hist_forFakeFit.GetXaxis().GetXmin()
        xmax = data_hist_forFakeFit.GetXaxis().GetXmax()
        
        #if counterForDataFiles>1: 
        #    print(f'{BLUE}Careful, there is more than 1 root file during Fake fit{RESET}')

        fake_data_driven_processes_mumu_SR = []
        non_prompt_processes_mumu_SR = []
        remaining_processes_mumu_SR = []
        fake_data_driven_processes_emu_SR = []
        non_prompt_processes_emu_SR = []
        remaining_processes_emu_SR = []
        fake_data_driven_processes_ee_SR = []
        non_prompt_processes_ee_SR = []
        remaining_processes_ee_SR = []
        fake_data_driven_processes_mumu_CRttX = []
        non_prompt_processes_mumu_CRttX = []
        remaining_processes_mumu_CRttX = []
        fake_data_driven_processes_emu_CRttX = []
        non_prompt_processes_emu_CRttX = []
        remaining_processes_emu_CRttX = []
        fake_data_driven_processes_ee_CRttX = []
        non_prompt_processes_ee_CRttX = []
        remaining_processes_ee_CRttX = []

        tt1l_h_mumu_SR = self.ROOT.TH1D('tt1l_mumu', 'M_{T,T\'}; M_{T,T\'} (GeV);Events;', nbins, xmin, xmax)
        other_mc_h_mumu_SR = self.ROOT.TH1D('other_mc_mumu', 'M_{T,T\'}; M_{T,T\'} (GeV);Events;', nbins, xmin, xmax)
        FakedD_h_mumu_SR = self.ROOT.TH1D('fake_dd_mumu', 'M_{T,T\'}; M_{T,T\'} (GeV);Events;', nbins, xmin, xmax)
        tt1l_h_emu_SR = self.ROOT.TH1D('tt1l_emu', 'M_{T,T\'}; M_{T,T\'} (GeV);Events;', nbins, xmin, xmax)
        other_mc_h_emu_SR = self.ROOT.TH1D('other_mc_emu', 'M_{T,T\'}; M_{T,T\'} (GeV);Events;', nbins, xmin, xmax)
        FakedD_h_emu_SR = self.ROOT.TH1D('fake_dd_emu', 'M_{T,T\'}; M_{T,T\'} (GeV);Events;', nbins, xmin, xmax)
        tt1l_h_ee_SR = self.ROOT.TH1D('tt1l_ee', 'M_{T,T\'}; M_{T,T\'} (GeV);Events;', nbins, xmin, xmax)
        other_mc_h_ee_SR = self.ROOT.TH1D('other_mc_ee', 'M_{T,T\'}; M_{T,T\'} (GeV);Events;', nbins, xmin, xmax)
        FakedD_h_ee_SR = self.ROOT.TH1D('fake_dd_ee', 'M_{T,T\'}; M_{T,T\'} (GeV);Events;', nbins, xmin, xmax)
        tt1l_h_mumu_CRttX = self.ROOT.TH1D('tt1l_mumu', 'M_{T,T\'}; M_{T,T\'} (GeV);Events;', nbins, xmin, xmax)
        other_mc_h_mumu_CRttX = self.ROOT.TH1D('other_mc_mumu', 'M_{T,T\'}; M_{T,T\'} (GeV);Events;', nbins, xmin, xmax)
        FakedD_h_mumu_CRttX = self.ROOT.TH1D('fake_dd_mumu', 'M_{T,T\'}; M_{T,T\'} (GeV);Events;', nbins, xmin, xmax)
        tt1l_h_emu_CRttX = self.ROOT.TH1D('tt1l_emu', 'M_{T,T\'}; M_{T,T\'} (GeV);Events;', nbins, xmin, xmax)
        other_mc_h_emu_CRttX = self.ROOT.TH1D('other_mc_emu', 'M_{T,T\'}; M_{T,T\'} (GeV);Events;', nbins, xmin, xmax)
        FakedD_h_emu_CRttX = self.ROOT.TH1D('fake_dd_emu', 'M_{T,T\'}; M_{T,T\'} (GeV);Events;', nbins, xmin, xmax)
        tt1l_h_ee_CRttX = self.ROOT.TH1D('tt1l_ee', 'M_{T,T\'}; M_{T,T\'} (GeV);Events;', nbins, xmin, xmax)
        other_mc_h_ee_CRttX = self.ROOT.TH1D('other_mc_ee', 'M_{T,T\'}; M_{T,T\'} (GeV);Events;', nbins, xmin, xmax)
        FakedD_h_ee_CRttX = self.ROOT.TH1D('fake_dd_ee', 'M_{T,T\'}; M_{T,T\'} (GeV);Events;', nbins, xmin, xmax)

        # Iterate over the zipped list
        for mc_file_name, mc_TFile, sf, idx in zipped_lists:
            # print(mc_file_name)
            # Check the process name and categorize accordingly
            hist_mumu_SR = mc_TFile.Get(self.histName_forFakeDD_mumu_SR)
            hist_emu_SR = mc_TFile.Get(self.histName_forFakeDD_emu_SR)
            hist_ee_SR = mc_TFile.Get(self.histName_forFakeDD_ee_SR)
            hist_mumu_CRttX = mc_TFile.Get(self.histName_forFakeDD_mumu_CRttX)
            hist_emu_CRttX = mc_TFile.Get(self.histName_forFakeDD_emu_CRttX)
            hist_ee_CRttX = mc_TFile.Get(self.histName_forFakeDD_ee_CRttX)
            if 'Fake_dataDriven' in mc_file_name:
                FakedD_h_mumu_SR.Add(hist_mumu_SR)
                fake_data_driven_processes_mumu_SR.append((mc_file_name, sf, idx))
                FakedD_h_emu_SR.Add(hist_emu_SR)
                fake_data_driven_processes_emu_SR.append((mc_file_name, sf, idx))
                FakedD_h_ee_SR.Add(hist_ee_SR)
                fake_data_driven_processes_ee_SR.append((mc_file_name, sf, idx))
                FakedD_h_mumu_CRttX.Add(hist_mumu_CRttX)
                fake_data_driven_processes_mumu_CRttX.append((mc_file_name, sf, idx))
                FakedD_h_emu_CRttX.Add(hist_emu_CRttX)
                fake_data_driven_processes_emu_CRttX.append((mc_file_name, sf, idx))
                FakedD_h_ee_CRttX.Add(hist_ee_CRttX)
                fake_data_driven_processes_ee_CRttX.append((mc_file_name, sf, idx))
            elif 'Signal' in mc_file_name:
                hist_mumu_SR.Scale(sf)
                other_mc_h_mumu_SR.Add(hist_mumu_SR)
                remaining_processes_mumu_SR.append((mc_file_name, sf, idx))
                hist_emu_SR.Scale(sf)
                other_mc_h_emu_SR.Add(hist_emu_SR)
                remaining_processes_emu_SR.append((mc_file_name, sf, idx))
                hist_ee_SR.Scale(sf)
                other_mc_h_ee_SR.Add(hist_ee_SR)
                remaining_processes_ee_SR.append((mc_file_name, sf, idx))
                hist_mumu_CRttX.Scale(sf)
                other_mc_h_mumu_CRttX.Add(hist_mumu_CRttX)
                remaining_processes_mumu_CRttX.append((mc_file_name, sf, idx))
                hist_emu_CRttX.Scale(sf)
                other_mc_h_emu_CRttX.Add(hist_emu_CRttX)
                remaining_processes_emu_CRttX.append((mc_file_name, sf, idx))
                hist_ee_CRttX.Scale(sf)
                other_mc_h_ee_CRttX.Add(hist_ee_CRttX)
                remaining_processes_ee_CRttX.append((mc_file_name, sf, idx))
            elif not 'Flip_dataDriven' in mc_file_name:
                hist_mumu_SR = mc_TFile.Get(self.histName_forFakeDD_mumu_SR + '_2')
                hist_emu_SR = mc_TFile.Get(self.histName_forFakeDD_emu_SR + '_2')
                hist_ee_SR = mc_TFile.Get(self.histName_forFakeDD_ee_SR + '_2')
                hist_mumu_SR.Scale(sf)
                tt1l_h_mumu_SR.Add(hist_mumu_SR)
                non_prompt_processes_mumu_SR.append((mc_file_name, sf, idx))
                hist_emu_SR.Scale(sf)
                tt1l_h_emu_SR.Add(hist_emu_SR)
                non_prompt_processes_emu_SR.append((mc_file_name, sf, idx))
                hist_ee_SR.Scale(sf)
                tt1l_h_ee_SR.Add(hist_ee_SR)
                non_prompt_processes_ee_SR.append((mc_file_name, sf, idx))
                hist_mumu_SR = mc_TFile.Get(self.histName_forFakeDD_mumu_SR + '_0')
                hist_emu_SR = mc_TFile.Get(self.histName_forFakeDD_emu_SR + '_0')
                hist_ee_SR = mc_TFile.Get(self.histName_forFakeDD_ee_SR + '_0')
                hist_mumu_SR.Scale(sf)
                other_mc_h_mumu_SR.Add(hist_mumu_SR)
                remaining_processes_mumu_SR.append((mc_file_name, sf, idx))
                hist_emu_SR.Scale(sf)
                other_mc_h_emu_SR.Add(hist_emu_SR)
                remaining_processes_emu_SR.append((mc_file_name, sf, idx))
                hist_ee_SR.Scale(sf)
                other_mc_h_ee_SR.Add(hist_ee_SR)
                remaining_processes_ee_SR.append((mc_file_name, sf, idx))
                hist_mumu_SR = mc_TFile.Get(self.histName_forFakeDD_mumu_SR + '_1')
                hist_emu_SR = mc_TFile.Get(self.histName_forFakeDD_emu_SR + '_1')
                hist_ee_SR = mc_TFile.Get(self.histName_forFakeDD_ee_SR + '_1')
                hist_mumu_SR.Scale(sf)
                other_mc_h_mumu_SR.Add(hist_mumu_SR)
                remaining_processes_mumu_SR.append((mc_file_name, sf, idx))
                hist_emu_SR.Scale(sf)
                other_mc_h_emu_SR.Add(hist_emu_SR)
                remaining_processes_emu_SR.append((mc_file_name, sf, idx))
                hist_ee_SR.Scale(sf)
                other_mc_h_ee_SR.Add(hist_ee_SR)
                remaining_processes_ee_SR.append((mc_file_name, sf, idx))
                hist_mumu_CRttX = mc_TFile.Get(self.histName_forFakeDD_mumu_CRttX + '_2')
                hist_emu_CRttX = mc_TFile.Get(self.histName_forFakeDD_emu_CRttX + '_2')
                hist_ee_CRttX = mc_TFile.Get(self.histName_forFakeDD_ee_CRttX + '_2')
                hist_mumu_CRttX.Scale(sf)
                tt1l_h_mumu_CRttX.Add(hist_mumu_CRttX)
                non_prompt_processes_mumu_CRttX.append((mc_file_name, sf, idx))
                hist_emu_CRttX.Scale(sf)
                tt1l_h_emu_CRttX.Add(hist_emu_CRttX)
                non_prompt_processes_emu_CRttX.append((mc_file_name, sf, idx))
                hist_ee_CRttX.Scale(sf)
                tt1l_h_ee_CRttX.Add(hist_ee_CRttX)
                non_prompt_processes_ee_CRttX.append((mc_file_name, sf, idx))
                hist_mumu_CRttX = mc_TFile.Get(self.histName_forFakeDD_mumu_CRttX + '_0')
                hist_emu_CRttX = mc_TFile.Get(self.histName_forFakeDD_emu_CRttX + '_0')
                hist_ee_CRttX = mc_TFile.Get(self.histName_forFakeDD_ee_CRttX + '_0')
                hist_mumu_CRttX.Scale(sf)
                other_mc_h_mumu_CRttX.Add(hist_mumu_CRttX)
                remaining_processes_mumu_CRttX.append((mc_file_name, sf, idx))
                hist_emu_CRttX.Scale(sf)
                other_mc_h_emu_CRttX.Add(hist_emu_CRttX)
                remaining_processes_emu_CRttX.append((mc_file_name, sf, idx))
                hist_ee_CRttX.Scale(sf)
                other_mc_h_ee_CRttX.Add(hist_ee_CRttX)
                remaining_processes_ee_CRttX.append((mc_file_name, sf, idx))
                hist_mumu_CRttX = mc_TFile.Get(self.histName_forFakeDD_mumu_CRttX + '_1')
                hist_emu_CRttX = mc_TFile.Get(self.histName_forFakeDD_emu_CRttX + '_1')
                hist_ee_CRttX = mc_TFile.Get(self.histName_forFakeDD_ee_CRttX + '_1')
                hist_mumu_CRttX.Scale(sf)
                other_mc_h_mumu_CRttX.Add(hist_mumu_CRttX)
                remaining_processes_mumu_CRttX.append((mc_file_name, sf, idx))
                hist_emu_CRttX.Scale(sf)
                other_mc_h_emu_CRttX.Add(hist_emu_CRttX)
                remaining_processes_emu_CRttX.append((mc_file_name, sf, idx))
                hist_ee_CRttX.Scale(sf)
                other_mc_h_ee_CRttX.Add(hist_ee_CRttX)
                remaining_processes_ee_CRttX.append((mc_file_name, sf, idx))

        print(tt1l_h_mumu_SR.Integral())
        print(tt1l_h_emu_SR.Integral())
        print(tt1l_h_ee_SR.Integral())
        print(tt1l_h_mumu_CRttX.Integral())
        print(tt1l_h_emu_CRttX.Integral())
        print(tt1l_h_ee_CRttX.Integral())
        FakedD_h_mumu_SR.Add(other_mc_h_mumu_SR,-1)
        Fake_dD_h_copy_intt1lregion_forPlot_mumu_SR = FakedD_h_mumu_SR.Clone()
        dataDriven_integral_before_anyScaling_mumu_SR = FakedD_h_mumu_SR.Integral()
        print(f'{GREEN}Fake DataDriven Integral Before Scaling it to MC in SR mumu = {dataDriven_integral_before_anyScaling_mumu_SR}{RESET}')
        FakedD_h_mumu_SR.Scale(dataDriven_normalization_fromMC_FakeSR_mumu/dataDriven_integral_before_anyScaling_mumu_SR)
        Fake_dD_h_copy_forFakePlot_SR_mumu = FakedD_h_mumu_SR.Clone()
        print(f'{GREEN}Fake DataDriven Integral After Scaling it to MC in SR mumu = {Fake_dD_h_copy_forFakePlot_SR_mumu.Integral()}{RESET}')
        FakedD_h_mumu_CRttX.Add(other_mc_h_mumu_CRttX,-1)
        Fake_dD_h_copy_intt1lregion_forPlot_mumu_CRttX = FakedD_h_mumu_CRttX.Clone()
        dataDriven_integral_before_anyScaling_mumu_CRttX = FakedD_h_mumu_CRttX.Integral()
        print(f'{GREEN}Fake DataDriven Integral Before Scaling it to MC mumu in CR ttX = {dataDriven_integral_before_anyScaling_mumu_CRttX}{RESET}')
        FakedD_h_mumu_CRttX.Scale(dataDriven_normalization_fromMC_FakeCRttX_mumu/dataDriven_integral_before_anyScaling_mumu_CRttX)
        Fake_dD_h_copy_forFakePlot_CRttX_mumu = FakedD_h_mumu_CRttX.Clone()
        print(f'{GREEN}Fake DataDriven Integral After Scaling it to MC in CR ttX mumu = {Fake_dD_h_copy_forFakePlot_CRttX_mumu.Integral()}{RESET}')
        FakedD_h_emu_SR.Add(other_mc_h_emu_SR,-1)
        Fake_dD_h_copy_intt1lregion_forPlot_emu_SR = FakedD_h_emu_SR.Clone()
        dataDriven_integral_before_anyScaling_emu_SR = FakedD_h_emu_SR.Integral()
        print(f'{GREEN}Fake DataDriven Integral Before Scaling it to MC in SR emu = {dataDriven_integral_before_anyScaling_emu_SR}{RESET}')
        FakedD_h_emu_SR.Scale(dataDriven_normalization_fromMC_FakeSR_emu/dataDriven_integral_before_anyScaling_emu_SR)
        Fake_dD_h_copy_forFakePlot_SR_emu = FakedD_h_emu_SR.Clone()
        print(f'{GREEN}Fake DataDriven Integral After Scaling it to MC in SR emu = {Fake_dD_h_copy_forFakePlot_SR_emu.Integral()}{RESET}')
        FakedD_h_emu_CRttX.Add(other_mc_h_emu_CRttX,-1)
        Fake_dD_h_copy_intt1lregion_forPlot_emu_CRttX = FakedD_h_emu_CRttX.Clone()
        dataDriven_integral_before_anyScaling_emu_CRttX = FakedD_h_emu_CRttX.Integral()
        print(f'{GREEN}Fake DataDriven Integral Before Scaling it to MC emu in CR ttX = {dataDriven_integral_before_anyScaling_emu_CRttX}{RESET}')
        FakedD_h_emu_CRttX.Scale(dataDriven_normalization_fromMC_FakeCRttX_emu/dataDriven_integral_before_anyScaling_emu_CRttX)
        Fake_dD_h_copy_forFakePlot_CRttX_emu = FakedD_h_emu_CRttX.Clone()
        print(f'{GREEN}Fake DataDriven Integral After Scaling it to MC in CR ttX emu = {Fake_dD_h_copy_forFakePlot_CRttX_emu.Integral()}{RESET}')
        FakedD_h_ee_SR.Add(other_mc_h_ee_SR,-1)
        Fake_dD_h_copy_intt1lregion_forPlot_ee_SR = FakedD_h_ee_SR.Clone()
        dataDriven_integral_before_anyScaling_ee_SR = FakedD_h_ee_SR.Integral()
        print(f'{GREEN}Fake DataDriven Integral Before Scaling it to MC in SR ee = {dataDriven_integral_before_anyScaling_ee_SR}{RESET}')
        FakedD_h_ee_SR.Scale(dataDriven_normalization_fromMC_FakeSR_ee/dataDriven_integral_before_anyScaling_ee_SR)
        Fake_dD_h_copy_forFakePlot_SR_ee = FakedD_h_ee_SR.Clone()
        print(f'{GREEN}Fake DataDriven Integral After Scaling it to MC in SR ee = {Fake_dD_h_copy_forFakePlot_SR_ee.Integral()}{RESET}')
        FakedD_h_ee_CRttX.Add(other_mc_h_ee_CRttX,-1)
        Fake_dD_h_copy_intt1lregion_forPlot_ee_CRttX = FakedD_h_ee_CRttX.Clone()
        dataDriven_integral_before_anyScaling_ee_CRttX = FakedD_h_ee_CRttX.Integral()
        print(f'{GREEN}Fake DataDriven Integral Before Scaling it to MC ee in CR ttX = {dataDriven_integral_before_anyScaling_ee_CRttX}{RESET}')
        FakedD_h_ee_CRttX.Scale(dataDriven_normalization_fromMC_FakeCRttX_ee/dataDriven_integral_before_anyScaling_ee_CRttX)
        Fake_dD_h_copy_forFakePlot_CRttX_ee = FakedD_h_ee_CRttX.Clone()
        print(f'{GREEN}Fake DataDriven Integral After Scaling it to MC in CRttX ee = {Fake_dD_h_copy_forFakePlot_CRttX_ee.Integral()}{RESET}')

        self.Fake_dD_h_copy_intt1lregion_forPlot_SR = Fake_dD_h_copy_intt1lregion_forPlot_mumu_SR.Clone()
        self.Fake_dD_h_copy_intt1lregion_forPlot_SR.Add(Fake_dD_h_copy_intt1lregion_forPlot_emu_SR)
        self.Fake_dD_h_copy_intt1lregion_forPlot_SR.Add(Fake_dD_h_copy_intt1lregion_forPlot_ee_SR)
        self.Fake_dD_h_copy_forFakePlot_SR = Fake_dD_h_copy_forFakePlot_SR_mumu.Clone()
        self.Fake_dD_h_copy_forFakePlot_SR.Add(Fake_dD_h_copy_forFakePlot_SR_emu)
        self.Fake_dD_h_copy_forFakePlot_SR.Add(Fake_dD_h_copy_forFakePlot_SR_ee)
        print(f'{GREEN}Fake DataDriven Integral After Scaling it to MC in SR = {self.Fake_dD_h_copy_forFakePlot_SR.Integral()}{RESET}')
        self.Fake_dD_h_copy_intt1lregion_forPlot_CRttX = Fake_dD_h_copy_intt1lregion_forPlot_mumu_CRttX.Clone()
        self.Fake_dD_h_copy_intt1lregion_forPlot_CRttX.Add(Fake_dD_h_copy_intt1lregion_forPlot_emu_CRttX)
        self.Fake_dD_h_copy_intt1lregion_forPlot_CRttX.Add(Fake_dD_h_copy_intt1lregion_forPlot_ee_CRttX)
        self.Fake_dD_h_copy_forFakePlot_CRttX = Fake_dD_h_copy_forFakePlot_CRttX_mumu.Clone()
        self.Fake_dD_h_copy_forFakePlot_CRttX.Add(Fake_dD_h_copy_forFakePlot_CRttX_emu)
        self.Fake_dD_h_copy_forFakePlot_CRttX.Add(Fake_dD_h_copy_forFakePlot_CRttX_ee)
        print(f'{GREEN}Fake DataDriven Integral After Scaling it to MC in CR ttX = {self.Fake_dD_h_copy_forFakePlot_CRttX.Integral()}{RESET}')

        if self.preFit:
            print(f'{BPURPLE}Inside preFit{RESET}')
            # CMS_lumi.extraText += 'prefit'
            # CMS_lumi.outputFileName += '_prefit'
            all_mc_SF = 1.0
            tt1l_SF = 1.0
            Fake_dataDriven_SF = 1.0

        print("all_other_mc_sf =", f'{all_mc_SF:.2f}')
        print("tt1l_sf =", f'{tt1l_SF:.2f}')
        print("Fake_dataDriven_sf =", f'{Fake_dataDriven_SF:.2f}')

        # print(f'fake_data_driven_processes_mumu_SR={fake_data_driven_processes_mumu_SR}')
        # print(f'non_prompt_processeses_mumu_SR={non_prompt_processeses_mumu_SR}')
        # print(f'remaining_processes_mumu_SR={remaining_processes_mumu_SR}')
        # print(f'fake_data_driven_processes_emu_SR={fake_data_driven_processes_emu_SR}')
        # print(f'non_prompt_processeses_emu_SR={non_prompt_processeses_emu_SR}')
        # print(f'remaining_processes_emu_SR={remaining_processes_emu_SR}')
        # print(f'fake_data_driven_processes_ee_SR={fake_data_driven_processes_ee_SR}')
        # print(f'non_prompt_processeses_ee_SR={non_prompt_processeses_ee_SR}')
        # print(f'remaining_processes_ee_SR={remaining_processes_ee_SR}')
        # print(f'fake_data_driven_processes_mumu_CRttX={fake_data_driven_processes_mumu_CRttX}')
        # print(f'non_prompt_processeses_mumu_CRttX={non_prompt_processeses_mumu_CRttX}')
        # print(f'remaining_processes_mumu_CRttX={remaining_processes_mumu_CRttX}')
        # print(f'fake_data_driven_processes_emu_CRttX={fake_data_driven_processes_emu_CRttX}')
        # print(f'non_prompt_processeses_emu_CRttX={non_prompt_processeses_emu_CRttX}')
        # print(f'remaining_processes_emu_CRttX={remaining_processes_emu_CRttX}')
        # print(f'fake_data_driven_processes_ee_CRttX={fake_data_driven_processes_ee_CRttX}')
        # print(f'non_prompt_processeses_ee_CRttX={non_prompt_processeses_ee_CRttX}')
        # print(f'remaining_processes_ee_CRttX={remaining_processes_ee_CRttX}')

        # Applying Fake_dataDriven_SF to Fake data driven processes (scaling to lumi)
        for fake_data_driven_process in fake_data_driven_processes_mumu_SR:
            # print(f'{GREEN}Applying dataDriven_normalization to {fake_data_driven_process[0].split("/")[-1].replace(".root","")}{RESET}')
            self.sf_list[fake_data_driven_process[2]] *= Fake_dataDriven_SF # * self.dataDriven_Fake_normalization

        for fake_data_driven_process in fake_data_driven_processes_emu_SR:
            # print(f'{GREEN}Applying dataDriven_normalization to {fake_data_driven_process[0].split("/")[-1].replace(".root","")}{RESET}')
            self.sf_list[fake_data_driven_process[2]] *= Fake_dataDriven_SF # * self.dataDriven_Fake_normalization

        for fake_data_driven_process in fake_data_driven_processes_ee_SR:
            # print(f'{GREEN}Applying dataDriven_normalization to {fake_data_driven_process[0].split("/")[-1].replace(".root","")}{RESET}')
            self.sf_list[fake_data_driven_process[2]] *= Fake_dataDriven_SF # * self.dataDriven_Fake_normalization

        for non_prompt_process in non_prompt_processes_mumu_SR:
            # print(f'{GREEN}Applying tt1l_SF to {non_prompt_process[0].split("/")[-1].replace(".root","")}{RESET}')
            self.sf_list[non_prompt_process[2]] *= tt1l_SF
        
        for non_prompt_process in non_prompt_processes_emu_SR:
            # print(f'{GREEN}Applying tt1l_SF to {non_prompt_process[0].split("/")[-1].replace(".root","")}{RESET}')
            self.sf_list[non_prompt_process[2]] *= tt1l_SF
        
        for non_prompt_process in non_prompt_processes_ee_SR:
            # print(f'{GREEN}Applying tt1l_SF to {non_prompt_process[0].split("/")[-1].replace(".root","")}{RESET}')
            self.sf_list[non_prompt_process[2]] *= tt1l_SF

        # Applying all_mc_SF to remaining processes
        for remaining_process in remaining_processes_mumu_SR:
            # print(f'{GREEN}Applying all_other_mc_SF to {remaining_process[0].split("/")[-1].replace(".root","")}{RESET}')
            self.sf_list[remaining_process[2]] *= all_mc_SF

        for remaining_process in remaining_processes_emu_SR:
            # print(f'{GREEN}Applying all_other_mc_SF to {remaining_process[0].split("/")[-1].replace(".root","")}{RESET}')
            self.sf_list[remaining_process[2]] *= all_mc_SF

        for remaining_process in remaining_processes_ee_SR:
            # print(f'{GREEN}Applying all_other_mc_SF to {remaining_process[0].split("/")[-1].replace(".root","")}{RESET}')
            self.sf_list[remaining_process[2]] *= all_mc_SF

        for fake_data_driven_process in fake_data_driven_processes_mumu_CRttX:
            # print(f'{GREEN}Applying dataDriven_normalization to {fake_data_driven_process[0].split("/")[-1].replace(".root","")}{RESET}')
            self.sf_list[fake_data_driven_process[2]] *= Fake_dataDriven_SF # * self.dataDriven_Fake_normalization

        for fake_data_driven_process in fake_data_driven_processes_emu_CRttX:
            # print(f'{GREEN}Applying dataDriven_normalization to {fake_data_driven_process[0].split("/")[-1].replace(".root","")}{RESET}')
            self.sf_list[fake_data_driven_process[2]] *= Fake_dataDriven_SF # * self.dataDriven_Fake_normalization

        for fake_data_driven_process in fake_data_driven_processes_ee_CRttX:
            # print(f'{GREEN}Applying dataDriven_normalization to {fake_data_driven_process[0].split("/")[-1].replace(".root","")}{RESET}')
            self.sf_list[fake_data_driven_process[2]] *= Fake_dataDriven_SF # * self.dataDriven_Fake_normalization

        for non_prompt_process in non_prompt_processes_mumu_CRttX:
            # print(f'{GREEN}Applying tt1l_SF to {non_prompt_process[0].split("/")[-1].replace(".root","")}{RESET}')
            self.sf_list[non_prompt_process[2]] *= tt1l_SF
        
        for non_prompt_process in non_prompt_processes_emu_CRttX:
            # print(f'{GREEN}Applying tt1l_SF to {non_prompt_process[0].split("/")[-1].replace(".root","")}{RESET}')
            self.sf_list[non_prompt_process[2]] *= tt1l_SF
        
        for non_prompt_process in non_prompt_processes_ee_CRttX:
            # print(f'{GREEN}Applying tt1l_SF to {non_prompt_process[0].split("/")[-1].replace(".root","")}{RESET}')
            self.sf_list[non_prompt_process[2]] *= tt1l_SF

        # Applying all_mc_SF to remaining processes
        for remaining_process in remaining_processes_mumu_CRttX:
            # print(f'{GREEN}Applying all_other_mc_SF to {remaining_process[0].split("/")[-1].replace(".root","")}{RESET}')
            self.sf_list[remaining_process[2]] *= all_mc_SF

        for remaining_process in remaining_processes_emu_CRttX:
            # print(f'{GREEN}Applying all_other_mc_SF to {remaining_process[0].split("/")[-1].replace(".root","")}{RESET}')
            self.sf_list[remaining_process[2]] *= all_mc_SF

        for remaining_process in remaining_processes_ee_CRttX:
            # print(f'{GREEN}Applying all_other_mc_SF to {remaining_process[0].split("/")[-1].replace(".root","")}{RESET}')
            self.sf_list[remaining_process[2]] *= all_mc_SF

        Fakemc_in_SR_h = Fakemc_in_SR_h_mumu.Clone()
        Fakemc_in_SR_h.Add(Fakemc_in_SR_h_emu)
        Fakemc_in_SR_h.Add(Fakemc_in_SR_h_ee)
        Fakemc_in_CRttX_h = Fakemc_in_CRttX_h_mumu.Clone()
        Fakemc_in_CRttX_h.Add(Fakemc_in_CRttX_h_emu)
        Fakemc_in_CRttX_h.Add(Fakemc_in_CRttX_h_ee)
        mcFake_intt1lregion_hist_SR = tt1l_h_mumu_SR.Clone()
        mcFake_intt1lregion_hist_SR.Add(tt1l_h_emu_SR)
        mcFake_intt1lregion_hist_SR.Add(tt1l_h_ee_SR)
        mcFake_intt1lregion_hist_CRttX = tt1l_h_mumu_CRttX.Clone()
        mcFake_intt1lregion_hist_CRttX.Add(tt1l_h_emu_CRttX)
        mcFake_intt1lregion_hist_CRttX.Add(tt1l_h_ee_CRttX)
        Fake_dD_h_copy_intt1lregion_forPlot_SR_Clone = self.Fake_dD_h_copy_intt1lregion_forPlot_SR.Clone()
        Fake_dD_h_copy_intt1lregion_forPlot_CRttX_Clone = self.Fake_dD_h_copy_intt1lregion_forPlot_CRttX.Clone()
        Fakemc_in_SR_h.Scale(1/Fakemc_in_SR_h.Integral())
        Fakemc_in_CRttX_h.Scale(1/Fakemc_in_CRttX_h.Integral())
        mcFake_intt1lregion_hist_SR.Scale(1/mcFake_intt1lregion_hist_SR.Integral())
        mcFake_intt1lregion_hist_CRttX.Scale(1/mcFake_intt1lregion_hist_CRttX.Integral())
        Fake_dD_h_copy_intt1lregion_forPlot_SR_Clone.Scale(1/Fake_dD_h_copy_intt1lregion_forPlot_SR_Clone.Integral())
        Fake_dD_h_copy_intt1lregion_forPlot_CRttX_Clone.Scale(1/Fake_dD_h_copy_intt1lregion_forPlot_CRttX_Clone.Integral())
        histosToPlot = [
            [Fake_dD_h_copy_intt1lregion_forPlot_SR_Clone, ROOT.kRed, "Data in MR tt1l for SR (bkg subt)"],
            [mcFake_intt1lregion_hist_SR, ROOT.kGreen, "MC Non prompt in MR tt1l for SR"],
            [Fakemc_in_SR_h, ROOT.kBlue, "MC Non prompt in SR"]
        ]
        self.make_plot(histosToPlot, f"{outPath}plots/comparison_Fake_SR_plot_Signal" + mass + "_UL" + year + ".png", "Fake SR")
        histosToPlot = [
            [Fake_dD_h_copy_intt1lregion_forPlot_CRttX_Clone, ROOT.kRed, "Data in MR tt1l for CR ttX (bkg subt)"],
            [mcFake_intt1lregion_hist_CRttX, ROOT.kGreen, "MC Non prompt in MR tt1l for CR ttX"],
            [Fakemc_in_CRttX_h, ROOT.kMagenta, "MC Non prompt in CR ttX"]
        ]
        self.make_plot(histosToPlot, f"{outPath}plots/comparison_Fake_CRttX_plot_Signal" + mass + "_UL" + year + ".png", "Fake CRttX")

        # save histograms for combine
        Fake_dD_h_copy_forFakePlot_SR_mumu_systshape_Up = tt1l_h_mumu_SR.Clone()
        Fake_dD_h_copy_forFakePlot_SR_mumu_systshape_Up.Scale(dataDriven_normalization_fromMC_FakeSR_mumu/dataDriven_integral_before_anyScaling_mumu_SR)
        Fake_dD_h_copy_forFakePlot_SR_mumu_systshape_Down = Fake_dD_h_copy_intt1lregion_forPlot_mumu_SR.Clone()
        Fake_dD_h_copy_forFakePlot_SR_mumu_systshape_Down.Add(Fake_dD_h_copy_intt1lregion_forPlot_mumu_SR)
        Fake_dD_h_copy_forFakePlot_SR_mumu_systshape_Down.Add(tt1l_h_mumu_SR,-1)
        Fake_dD_h_copy_forFakePlot_SR_mumu_systshape_Down.Scale(dataDriven_normalization_fromMC_FakeSR_mumu/dataDriven_integral_before_anyScaling_mumu_SR)
        Fake_dD_h_copy_forFakePlot_SR_mumu_systmc_Up = Fake_dD_h_copy_forFakePlot_SR_mumu.Clone()
        Fake_dD_h_copy_forFakePlot_SR_mumu_systmc_Up.Add(Fake_dD_h_copy_forFakePlot_SR_mumu_systshape_Up)
        Fake_dD_h_copy_forFakePlot_SR_mumu_systmc_Up.Add(Fakemc_in_SR_h_mumu,-1)
        Fake_dD_h_copy_forFakePlot_SR_mumu_systmc_Down = Fake_dD_h_copy_forFakePlot_SR_mumu.Clone()
        Fake_dD_h_copy_forFakePlot_SR_mumu_systmc_Down.Add(Fake_dD_h_copy_forFakePlot_SR_mumu_systshape_Up,-1)
        Fake_dD_h_copy_forFakePlot_SR_mumu_systmc_Down.Add(Fakemc_in_SR_h_mumu)
        Fake_dD_h_copy_forFakePlot_CRttX_mumu_systshape_Up = tt1l_h_mumu_CRttX.Clone()
        Fake_dD_h_copy_forFakePlot_CRttX_mumu_systshape_Up.Scale(dataDriven_normalization_fromMC_FakeCRttX_mumu/dataDriven_integral_before_anyScaling_mumu_CRttX)
        Fake_dD_h_copy_forFakePlot_CRttX_mumu_systshape_Down = Fake_dD_h_copy_intt1lregion_forPlot_mumu_CRttX.Clone()
        Fake_dD_h_copy_forFakePlot_CRttX_mumu_systshape_Down.Add(Fake_dD_h_copy_intt1lregion_forPlot_mumu_CRttX)
        Fake_dD_h_copy_forFakePlot_CRttX_mumu_systshape_Down.Add(tt1l_h_mumu_CRttX,-1)
        Fake_dD_h_copy_forFakePlot_CRttX_mumu_systshape_Down.Scale(dataDriven_normalization_fromMC_FakeCRttX_mumu/dataDriven_integral_before_anyScaling_mumu_CRttX)
        Fake_dD_h_copy_forFakePlot_CRttX_mumu_systmc_Up = Fake_dD_h_copy_forFakePlot_CRttX_mumu.Clone()
        Fake_dD_h_copy_forFakePlot_CRttX_mumu_systmc_Up.Add(Fake_dD_h_copy_forFakePlot_CRttX_mumu_systshape_Up)
        Fake_dD_h_copy_forFakePlot_CRttX_mumu_systmc_Up.Add(Fakemc_in_CRttX_h_mumu,-1)
        Fake_dD_h_copy_forFakePlot_CRttX_mumu_systmc_Down = Fake_dD_h_copy_forFakePlot_CRttX_mumu.Clone()
        Fake_dD_h_copy_forFakePlot_CRttX_mumu_systmc_Down.Add(Fake_dD_h_copy_forFakePlot_CRttX_mumu_systshape_Up,-1)
        Fake_dD_h_copy_forFakePlot_CRttX_mumu_systmc_Down.Add(Fakemc_in_CRttX_h_mumu)
        Fake_dD_h_copy_forFakePlot_SR_emu_systshape_Up = tt1l_h_emu_SR.Clone()
        Fake_dD_h_copy_forFakePlot_SR_emu_systshape_Up.Scale(dataDriven_normalization_fromMC_FakeSR_emu/dataDriven_integral_before_anyScaling_emu_SR)
        Fake_dD_h_copy_forFakePlot_SR_emu_systshape_Down = Fake_dD_h_copy_intt1lregion_forPlot_emu_SR.Clone()
        Fake_dD_h_copy_forFakePlot_SR_emu_systshape_Down.Add(Fake_dD_h_copy_intt1lregion_forPlot_emu_SR)
        Fake_dD_h_copy_forFakePlot_SR_emu_systshape_Down.Add(tt1l_h_emu_SR,-1)
        Fake_dD_h_copy_forFakePlot_SR_emu_systshape_Down.Scale(dataDriven_normalization_fromMC_FakeSR_emu/dataDriven_integral_before_anyScaling_emu_SR)
        Fake_dD_h_copy_forFakePlot_SR_emu_systmc_Up = Fake_dD_h_copy_forFakePlot_SR_emu.Clone()
        Fake_dD_h_copy_forFakePlot_SR_emu_systmc_Up.Add(Fake_dD_h_copy_forFakePlot_SR_emu_systshape_Up)
        Fake_dD_h_copy_forFakePlot_SR_emu_systmc_Up.Add(Fakemc_in_SR_h_emu,-1)
        Fake_dD_h_copy_forFakePlot_SR_emu_systmc_Down = Fake_dD_h_copy_forFakePlot_SR_emu.Clone()
        Fake_dD_h_copy_forFakePlot_SR_emu_systmc_Down.Add(Fake_dD_h_copy_forFakePlot_SR_emu_systshape_Up,-1)
        Fake_dD_h_copy_forFakePlot_SR_emu_systmc_Down.Add(Fakemc_in_SR_h_emu)
        Fake_dD_h_copy_forFakePlot_CRttX_emu_systshape_Up = tt1l_h_emu_CRttX.Clone()
        Fake_dD_h_copy_forFakePlot_CRttX_emu_systshape_Up.Scale(dataDriven_normalization_fromMC_FakeCRttX_emu/dataDriven_integral_before_anyScaling_emu_CRttX)
        Fake_dD_h_copy_forFakePlot_CRttX_emu_systshape_Down = Fake_dD_h_copy_intt1lregion_forPlot_emu_CRttX.Clone()
        Fake_dD_h_copy_forFakePlot_CRttX_emu_systshape_Down.Add(Fake_dD_h_copy_intt1lregion_forPlot_emu_CRttX)
        Fake_dD_h_copy_forFakePlot_CRttX_emu_systshape_Down.Add(tt1l_h_emu_CRttX,-1)
        Fake_dD_h_copy_forFakePlot_CRttX_emu_systshape_Down.Scale(dataDriven_normalization_fromMC_FakeCRttX_emu/dataDriven_integral_before_anyScaling_emu_CRttX)
        Fake_dD_h_copy_forFakePlot_CRttX_emu_systmc_Up = Fake_dD_h_copy_forFakePlot_CRttX_emu.Clone()
        Fake_dD_h_copy_forFakePlot_CRttX_emu_systmc_Up.Add(Fake_dD_h_copy_forFakePlot_CRttX_emu_systshape_Up)
        Fake_dD_h_copy_forFakePlot_CRttX_emu_systmc_Up.Add(Fakemc_in_CRttX_h_emu,-1)
        Fake_dD_h_copy_forFakePlot_CRttX_emu_systmc_Down = Fake_dD_h_copy_forFakePlot_CRttX_emu.Clone()
        Fake_dD_h_copy_forFakePlot_CRttX_emu_systmc_Down.Add(Fake_dD_h_copy_forFakePlot_CRttX_emu_systshape_Up,-1)
        Fake_dD_h_copy_forFakePlot_CRttX_emu_systmc_Down.Add(Fakemc_in_CRttX_h_emu)
        Fake_dD_h_copy_forFakePlot_SR_ee_systshape_Up = tt1l_h_ee_SR.Clone()
        Fake_dD_h_copy_forFakePlot_SR_ee_systshape_Up.Scale(dataDriven_normalization_fromMC_FakeSR_ee/dataDriven_integral_before_anyScaling_ee_SR)
        Fake_dD_h_copy_forFakePlot_SR_ee_systshape_Down = Fake_dD_h_copy_intt1lregion_forPlot_ee_SR.Clone()
        Fake_dD_h_copy_forFakePlot_SR_ee_systshape_Down.Add(Fake_dD_h_copy_intt1lregion_forPlot_ee_SR)
        Fake_dD_h_copy_forFakePlot_SR_ee_systshape_Down.Add(tt1l_h_ee_SR,-1)
        Fake_dD_h_copy_forFakePlot_SR_ee_systshape_Down.Scale(dataDriven_normalization_fromMC_FakeSR_ee/dataDriven_integral_before_anyScaling_ee_SR)
        Fake_dD_h_copy_forFakePlot_SR_ee_systmc_Up = Fake_dD_h_copy_forFakePlot_SR_ee.Clone()
        Fake_dD_h_copy_forFakePlot_SR_ee_systmc_Up.Add(Fake_dD_h_copy_forFakePlot_SR_ee_systshape_Up)
        Fake_dD_h_copy_forFakePlot_SR_ee_systmc_Up.Add(Fakemc_in_SR_h_ee,-1)
        Fake_dD_h_copy_forFakePlot_SR_ee_systmc_Down = Fake_dD_h_copy_forFakePlot_SR_ee.Clone()
        Fake_dD_h_copy_forFakePlot_SR_ee_systmc_Down.Add(Fake_dD_h_copy_forFakePlot_SR_ee_systshape_Up,-1)
        Fake_dD_h_copy_forFakePlot_SR_ee_systmc_Down.Add(Fakemc_in_SR_h_ee)
        Fake_dD_h_copy_forFakePlot_CRttX_ee_systshape_Up = tt1l_h_ee_CRttX.Clone()
        Fake_dD_h_copy_forFakePlot_CRttX_ee_systshape_Up.Scale(dataDriven_normalization_fromMC_FakeCRttX_ee/dataDriven_integral_before_anyScaling_ee_CRttX)
        Fake_dD_h_copy_forFakePlot_CRttX_ee_systshape_Down = Fake_dD_h_copy_intt1lregion_forPlot_ee_CRttX.Clone()
        Fake_dD_h_copy_forFakePlot_CRttX_ee_systshape_Down.Add(Fake_dD_h_copy_intt1lregion_forPlot_ee_CRttX)
        Fake_dD_h_copy_forFakePlot_CRttX_ee_systshape_Down.Add(tt1l_h_ee_CRttX,-1)
        Fake_dD_h_copy_forFakePlot_CRttX_ee_systshape_Down.Scale(dataDriven_normalization_fromMC_FakeCRttX_ee/dataDriven_integral_before_anyScaling_ee_CRttX)
        Fake_dD_h_copy_forFakePlot_CRttX_ee_systmc_Up = Fake_dD_h_copy_forFakePlot_CRttX_ee.Clone()
        Fake_dD_h_copy_forFakePlot_CRttX_ee_systmc_Up.Add(Fake_dD_h_copy_forFakePlot_CRttX_ee_systshape_Up)
        Fake_dD_h_copy_forFakePlot_CRttX_ee_systmc_Up.Add(Fakemc_in_CRttX_h_ee,-1)
        Fake_dD_h_copy_forFakePlot_CRttX_ee_systmc_Down = Fake_dD_h_copy_forFakePlot_CRttX_ee.Clone()
        Fake_dD_h_copy_forFakePlot_CRttX_ee_systmc_Down.Add(Fake_dD_h_copy_forFakePlot_CRttX_ee_systshape_Up,-1)
        Fake_dD_h_copy_forFakePlot_CRttX_ee_systmc_Down.Add(Fakemc_in_CRttX_h_ee)

        print(Fake_dD_h_copy_forFakePlot_SR_mumu_systshape_Up.Integral())
        print(Fake_dD_h_copy_forFakePlot_SR_mumu_systshape_Down.Integral())
        print(Fake_dD_h_copy_forFakePlot_SR_mumu_systmc_Up.Integral())
        print(Fake_dD_h_copy_forFakePlot_SR_mumu_systmc_Down.Integral())
        print(Fake_dD_h_copy_forFakePlot_SR_emu_systshape_Up.Integral())
        print(Fake_dD_h_copy_forFakePlot_SR_emu_systshape_Down.Integral())
        print(Fake_dD_h_copy_forFakePlot_SR_emu_systmc_Up.Integral())
        print(Fake_dD_h_copy_forFakePlot_SR_emu_systmc_Down.Integral())
        print(Fake_dD_h_copy_forFakePlot_SR_ee_systshape_Up.Integral())
        print(Fake_dD_h_copy_forFakePlot_SR_ee_systshape_Down.Integral())
        print(Fake_dD_h_copy_forFakePlot_SR_ee_systmc_Up.Integral())
        print(Fake_dD_h_copy_forFakePlot_SR_ee_systmc_Down.Integral())
        print(Fake_dD_h_copy_forFakePlot_CRttX_mumu_systshape_Up.Integral())
        print(Fake_dD_h_copy_forFakePlot_CRttX_mumu_systshape_Down.Integral())
        print(Fake_dD_h_copy_forFakePlot_CRttX_mumu_systmc_Up.Integral())
        print(Fake_dD_h_copy_forFakePlot_CRttX_mumu_systmc_Down.Integral())
        print(Fake_dD_h_copy_forFakePlot_CRttX_emu_systshape_Up.Integral())
        print(Fake_dD_h_copy_forFakePlot_CRttX_emu_systshape_Down.Integral())
        print(Fake_dD_h_copy_forFakePlot_CRttX_emu_systmc_Up.Integral())
        print(Fake_dD_h_copy_forFakePlot_CRttX_emu_systmc_Down.Integral())
        print(Fake_dD_h_copy_forFakePlot_CRttX_ee_systshape_Up.Integral())
        print(Fake_dD_h_copy_forFakePlot_CRttX_ee_systshape_Down.Integral())
        print(Fake_dD_h_copy_forFakePlot_CRttX_ee_systmc_Up.Integral())
        print(Fake_dD_h_copy_forFakePlot_CRttX_ee_systmc_Down.Integral())
        
        outputFile = ROOT.TFile.Open(outPath + 'Fake_' + mass + '_UL' + year + '.root','RECREATE')
        outputFile.WriteObject(Fake_dD_h_copy_forFakePlot_SR_mumu, self.histName_forFake_dD_SR_mumu)
        outputFile.WriteObject(Fake_dD_h_copy_forFakePlot_SR_mumu_systshape_Up, self.histName_forFake_dD_SR_mumu_systshape_Up)
        outputFile.WriteObject(Fake_dD_h_copy_forFakePlot_SR_mumu_systshape_Down, self.histName_forFake_dD_SR_mumu_systshape_Down)
        outputFile.WriteObject(Fake_dD_h_copy_forFakePlot_SR_mumu_systmc_Up, self.histName_forFake_dD_SR_mumu_systmc_Up)
        outputFile.WriteObject(Fake_dD_h_copy_forFakePlot_SR_mumu_systmc_Down, self.histName_forFake_dD_SR_mumu_systmc_Down)
        outputFile.WriteObject(Fake_dD_h_copy_forFakePlot_CRttX_mumu, self.histName_forFake_dD_CRttX_mumu)
        outputFile.WriteObject(Fake_dD_h_copy_forFakePlot_CRttX_mumu_systshape_Up, self.histName_forFake_dD_CRttX_mumu_systshape_Up)
        outputFile.WriteObject(Fake_dD_h_copy_forFakePlot_CRttX_mumu_systshape_Down, self.histName_forFake_dD_CRttX_mumu_systshape_Down)
        outputFile.WriteObject(Fake_dD_h_copy_forFakePlot_CRttX_mumu_systmc_Up, self.histName_forFake_dD_CRttX_mumu_systmc_Up)
        outputFile.WriteObject(Fake_dD_h_copy_forFakePlot_CRttX_mumu_systmc_Down, self.histName_forFake_dD_CRttX_mumu_systmc_Down)
        outputFile.WriteObject(Fake_dD_h_copy_forFakePlot_SR_emu, self.histName_forFake_dD_SR_emu)
        outputFile.WriteObject(Fake_dD_h_copy_forFakePlot_SR_emu_systshape_Up, self.histName_forFake_dD_SR_emu_systshape_Up)
        outputFile.WriteObject(Fake_dD_h_copy_forFakePlot_SR_emu_systshape_Down, self.histName_forFake_dD_SR_emu_systshape_Down)
        outputFile.WriteObject(Fake_dD_h_copy_forFakePlot_SR_emu_systmc_Up, self.histName_forFake_dD_SR_emu_systmc_Up)
        outputFile.WriteObject(Fake_dD_h_copy_forFakePlot_SR_emu_systmc_Down, self.histName_forFake_dD_SR_emu_systmc_Down)
        outputFile.WriteObject(Fake_dD_h_copy_forFakePlot_CRttX_emu, self.histName_forFake_dD_CRttX_emu)
        outputFile.WriteObject(Fake_dD_h_copy_forFakePlot_CRttX_emu_systshape_Up, self.histName_forFake_dD_CRttX_emu_systshape_Up)
        outputFile.WriteObject(Fake_dD_h_copy_forFakePlot_CRttX_emu_systshape_Down, self.histName_forFake_dD_CRttX_emu_systshape_Down)
        outputFile.WriteObject(Fake_dD_h_copy_forFakePlot_CRttX_emu_systmc_Up, self.histName_forFake_dD_CRttX_emu_systmc_Up)
        outputFile.WriteObject(Fake_dD_h_copy_forFakePlot_CRttX_emu_systmc_Down, self.histName_forFake_dD_CRttX_emu_systmc_Down)
        outputFile.WriteObject(Fake_dD_h_copy_forFakePlot_SR_ee, self.histName_forFake_dD_SR_ee)
        outputFile.WriteObject(Fake_dD_h_copy_forFakePlot_SR_ee_systshape_Up, self.histName_forFake_dD_SR_ee_systshape_Up)
        outputFile.WriteObject(Fake_dD_h_copy_forFakePlot_SR_ee_systshape_Down, self.histName_forFake_dD_SR_ee_systshape_Down)
        outputFile.WriteObject(Fake_dD_h_copy_forFakePlot_SR_ee_systmc_Up, self.histName_forFake_dD_SR_ee_systmc_Up)
        outputFile.WriteObject(Fake_dD_h_copy_forFakePlot_SR_ee_systmc_Down, self.histName_forFake_dD_SR_ee_systmc_Down)
        outputFile.WriteObject(Fake_dD_h_copy_forFakePlot_CRttX_ee, self.histName_forFake_dD_CRttX_ee)
        outputFile.WriteObject(Fake_dD_h_copy_forFakePlot_CRttX_ee_systshape_Up, self.histName_forFake_dD_CRttX_ee_systshape_Up)
        outputFile.WriteObject(Fake_dD_h_copy_forFakePlot_CRttX_ee_systshape_Down, self.histName_forFake_dD_CRttX_ee_systshape_Down)
        outputFile.WriteObject(Fake_dD_h_copy_forFakePlot_CRttX_ee_systmc_Up, self.histName_forFake_dD_CRttX_ee_systmc_Up)
        outputFile.WriteObject(Fake_dD_h_copy_forFakePlot_CRttX_ee_systmc_Down, self.histName_forFake_dD_CRttX_ee_systmc_Down)
        outputFile.Close()

    def prepare_root_files(self):
        """Open ROOT files where histograms reside in MC and data
        """
        for mc_file, mc_cntr_hist_file in zip(self.mc_file_list, self.mc_file_counter_hist_list):
            t_file_mc = self.ROOT.TFile(mc_file)
            if mc_cntr_hist_file == "":
                mc_cntr_hist_file = mc_file
            t_file_mc_cntr_hist = self.ROOT.TFile(mc_cntr_hist_file)
            self.mc_root_files.append(t_file_mc)
            self.mc_counter_hist_files.append(t_file_mc_cntr_hist)

        """
        for mc_file in self.data_file_list:
            t_file_mc = self.ROOT.TFile(mc_file)
            self.data_root_files.append(t_file_mc)
        """

    def open_root_file(self, file_path, mode='READ'):
        try:
            return self.ROOT.TFile(file_path, mode)
        except Exception as e:
            print(f"Error opening file {file_path}: {e}")
            exit(1)

    def calculate_sum_of_weights(self, t_file, mc_file, is_data_driven):
        if is_data_driven:
            return 1.0
        else:
            histogram = t_file.Get(self.sum_of_weights_hist_name)
            if histogram:
                return histogram.GetEntries()*histogram.GetMean()
            else:
                print(f"Histogram {self.sum_of_weights_hist_name} not found in {mc_file}")
                exit(1)

    def process_mc_file(self, mc_file, mc_cntr_hist_file, xsec, range_id, is_data_driven):
        t_file_mc_cntr_hist = self.open_root_file(mc_cntr_hist_file)
        if not t_file_mc_cntr_hist:
            return

        sum_of_weights = self.calculate_sum_of_weights(t_file_mc_cntr_hist, mc_cntr_hist_file, is_data_driven)
        if sum_of_weights is None or sum_of_weights == 0:
            print(f'sumOfWeights not found.\n Exiting process.')
            exit(1)

        scaling_factor = 1 if is_data_driven else (xsec * self.integer_lumi / sum_of_weights)
        self.sf_list[range_id] *= scaling_factor
        self.sumOfWeights_list[range_id] = sum_of_weights
        print(f"{mc_file.split('/')[-1].replace('.root', '')} scaling factor: {self.sf_list[range_id]}\n {RED}{self.sum_of_weights_hist_name} = {self.sumOfWeights_list[range_id]}{RESET}\n {BRED}xsec = {xsec} pb{RESET}")

    def prepare_scaling_factors(self):
        zipped_lists = zip(self.mc_file_list, 
                           self.mc_file_counter_hist_list, 
                           self.xsec_list, 
                           range(len(self.xsec_list)))

        for mc_file, mc_cntr_hist_file, xsec, range_id in zipped_lists:
            is_data_driven = 'dataDriven' in mc_file
            mc_cntr_hist_file = mc_file if mc_cntr_hist_file == "" else mc_cntr_hist_file
            self.process_mc_file(mc_file, mc_cntr_hist_file, xsec, range_id, is_data_driven)

    def setupStyle(self, color_list=None, pattern_list=None, alpha=1.0):
        """Setup style
        """
        self.fill_alpha = alpha
        if color_list is None:
            # self.color_list = [self.ROOT.TColor.GetColor('#bf0000'), # Signal
            #                    self.ROOT.TColor.GetColor('#5b27a3'), # tt2l
            #                    self.ROOT.TColor.GetColor('#e00dd9'), # tt1l
            #                    self.ROOT.TColor.GetColor('#0de0e0'), # tt0l
            #                    self.ROOT.TColor.GetColor('#ffbf00'), # Singletop
            #                    self.ROOT.TColor.GetColor('#036303'), # ttX
            #                    self.ROOT.TColor.GetColor('#0000ff'), # Multibosons
            #                    self.ROOT.TColor.GetColor('#0ee60e'), # WJets
            #                    self.ROOT.TColor.GetColor('#02e002'), # DY
            #                    ]
            self.color_list = [self.ROOT.TColor.GetColor('#e00000'), # Signal
                               self.ROOT.TColor.GetColor('#5b27a3'), # tt2l
                               self.ROOT.TColor.GetColor('#02e002'), # tt1l
                               self.ROOT.TColor.GetColor('#036303'), # tt0l
                               self.ROOT.TColor.GetColor('#0000ff'), # Singletop
                               self.ROOT.TColor.GetColor('#e00dd9'), # ttX
                               self.ROOT.TColor.GetColor('#ffbf00'), # Multibosons
                               self.ROOT.TColor.GetColor('#0ee60e'), # WJets
                               self.ROOT.TColor.GetColor('#0de0e0'), # DY
                               ]
        else:
            self.color_list = color_list

        if pattern_list is None:
            for i in range(10):
                self.pattern_list.append(1001)
        else:
            self.pattern_list = pattern_list

        # TODO check if it is still working
        # self.ROOT.gROOT.SetStyle("tdrStyle")
        # tdrstyle.setTDRStyle()
        pass

    def addChannel(self, root_file, label, color_index, pattern_index=0, isMC=True, xsec=1.0, event_num=1.0, scale_factor=1.0, counter_histogram_root="", sumOfWeights=1.0):
        """Add channel
        """
        if os.path.isfile(root_file):
            if isMC:
                self.mc_file_list.append(root_file)
                self.mc_file_counter_hist_list.append(counter_histogram_root)
                self.mc_label_list.append(label)  # if same label, then the histograms will be added together
                self.mc_color_list.append(color_index)
                self.mc_pattern_list.append(pattern_index)
                self.xsec_list.append(xsec)
                self.event_num_list.append(event_num)
                self.sf_list.append(scale_factor)
                self.resf_list.append(scale_factor)
                self.sumOfWeights_list.append(sumOfWeights)
            else:
                self.data_file_list.append(root_file)
        else:
            print(f'Cannot add file {root_file}, it does not exist')
            print('Please Check')
            sys.exit(-1)
        pass

    def addHistogram(self, hist_name, x_title="", y_title="", draw_mode=STACKED, draw_option="", is_logy=False, underflow_bin=False, ymin=-1111, ymax=-1111, binlist=[], extratext=''):
        """Add histogram
        """
        self.histogram_list.append(hist_name)
        self.x_titles.append(x_title)
        self.y_titles.append(y_title)
        self.draw_modes.append(draw_mode)
        self.draw_options.append(draw_option)
        self.is_logy.append(is_logy)
        self.underflow_bin.append(underflow_bin)
        self.ymin.append(ymin)
        self.ymax.append(ymax)
        self.bin_lists.append(binlist)
        self.extra_text_hists.append(extratext)

    def MakeCumulative(self, hist, low, high, below):
        out = hist.Clone(hist.GetName() + '_cumul')
        out.Reset()
        prev = 0
        if below:
            to_scan = range(low, high)
        else:
            to_scan = range(high - 1, low - 1, -1)
        for ix in to_scan:
            val = prev + hist.GetBinContent(ix)
            out.SetBinContent(ix, val)
            prev = val
        return out

    def draw(self, subplot, outputPathway, signal, imageList, Flip, Fake, Paper, year, mass):
        self.prepare_root_files()
        self.prepare_scaling_factors()
        if Flip:
            self.prepare_Flipfit(outputPathway, year, mass)
        if Fake:
            self.prepare_Fakefit(outputPathway, year, mass)

        self.c1 = self.ROOT.TCanvas("c1", "c1")
        # self.c1.Print("plots.pdf[")
        for hist_name, x_title, y_title, mode, draw_option, is_logy, underflow_bin, ymin, ymax, binlist, extraText in zip(self.histogram_list,
                                                                                                                                    self.x_titles,
                                                                                                                                    self.y_titles,
                                                                                                                                    self.draw_modes,
                                                                                                                                    self.draw_options,
                                                                                                                                    self.is_logy,
                                                                                                                                    self.underflow_bin,
                                                                                                                                    self.ymin,
                                                                                                                                    self.ymax,
                                                                                                                                    self.bin_lists,
                                                                                                                                    self.extra_text_hists):
            self.createStacks(hist_name, x_title, y_title, mode, outputPathway, signal, imageList, Flip, Fake, Paper, underflow_bin, draw_option, is_logy, ymin, ymax, binlist, subplot, extraText, year, mass)

        # self.c1.Print("plots.pdf]")
        self.c1.Close()

    def createStacks(self, hist_name, xtitle, ytitle, mode, outPath, signal, imageList, Flip, Fake, Paper, underflow_bin, option="", isLogy=False, ymin=-1111, ymax=-1111, binlist=[], subplot='', extraText='', Year ='', mass='700'):

        print('\n')
        print(f'{HRED}Creating {hist_name} histogram{RESET}')
        print('\n')
        
        # Global Variables
        hs = self.ROOT.THStack()
        tl = self.ROOT.TLegend(self.legend_x1, self.legend_y1, self.legend_x2, self.legend_y2)
        tl.SetNColumns(4)
        tl.SetTextAlign(12)
        tl.SetMargin(0.2)
        tl.SetColumnSeparation(0.02)
        tl.SetBorderSize(0)
        tl.SetFillStyle(0)
        hist_group = dict()
        labellist = []

        if outPath != '':
            if outPath[-1] != '/': outPath += '/'
        path = outPath + "plots/"
        if not os.path.exists(path):
            os.mkdir(path)

        # adding signal contribution 
        signal_hist = None
        signal_histlist = []
        # adding BKG contributions
        mc_hist_sum = None

        xbins = []
        nrebins = -1
        if len(binlist)>0:
            xbins = array.array('d', binlist)
            nrebins = len(binlist)-1

     ########################## Get Monte Carlos histos info and divide them in Signals and Backgrounds ##########################

        position = hist_name.find('cut_')
        cut_index = hist_name[position:]
        count = cut_index.count('_')
        third_cut = cut_index[8] #'0'
        fifth_cut = cut_index[12] #'0'
        if(third_cut == '0' and fifth_cut == '0'):
            Region = 'SR'
        elif(third_cut == '0' and fifth_cut == '1'):
            Region = 'CR_ttX'
        elif(third_cut == '1' and fifth_cut == '0'):
            Region = 'MR_tt2l'
        elif(third_cut == '2' and fifth_cut == '0'):
            Region = 'MR_tt1l_SR'
        elif(third_cut == '2' and fifth_cut == '1'):
            Region = 'MR_tt1l_CR_ttX'
        # if(count < 5):
        #     for ifile in range(len(self.mc_file_list)):
        #         ahist = self.mc_root_files[ifile].Get(hist_name)
        #         if nrebins>0:
        #             ahist = ahist.Rebin(nrebins,hist_name+'rebinned_mc'+str(ifile),xbins)
            
        #         if ahist == None:
        #             print("histogram %s not found in %s"%(hist_name, self.mc_file_list[ifile]))
        #             print("quitting")
        #             sys.exit(-1)
        #         else:
        #             if underflow_bin:
        #                 underflow = ahist.GetBinContent(0)
        #                 first_bin_content = ahist.GetBinContent(1)
        #                 new_first_bin_content = first_bin_content + underflow
        #                 ahist.SetBinContent(1, new_first_bin_content)
        #                 ahist.SetBinContent(0, 0)
        #             # Handling overflow
        #             overflow = ahist.GetBinContent(ahist.GetNbinsX() + 1)
        #             last_bin_content = ahist.GetBinContent(ahist.GetNbinsX())
        #             new_last_bin_content = last_bin_content + overflow
        #             ahist.SetBinContent(ahist.GetNbinsX(), new_last_bin_content)
        #             ahist.SetBinContent(ahist.GetNbinsX() + 1, 0)
        #             ahist.Scale(self.sf_list[ifile])  

        #             # group by labels
        #             label = self.mc_label_list[ifile]
        #             if label not in hist_group:
        #                 hist_group[label] = ahist
        #                 labellist.append(label) # need to take care of the order
        #             else:
        #                 hist_group[label].Add(ahist)

        #             if 'Signal' not in label:
        #                 if mc_hist_sum == None:
        #                     mc_hist_sum = ahist.Clone("mc_hist_sum")
        #                 else:
        #                     mc_hist_sum.Add(ahist)
        #                 ahist.SetFillColorAlpha(self.color_list[self.mc_color_list[ifile]], self.fill_alpha)
        #                 ahist.SetLineColor(self.color_list[self.mc_color_list[ifile]])
        #                 ahist.SetFillStyle(self.pattern_list[self.mc_pattern_list[ifile]])

        #             # Signal
        #             else:
        #                 if signal_hist is None:
        #                     signal_hist = ahist
        #                 else:
        #                     signal_hist.Add(ahist)
        #                 ahist.SetFillColorAlpha(self.color_list[self.mc_color_list[ifile]], self.fill_alpha)
        #                 ahist.SetLineColor(self.color_list[self.mc_color_list[ifile]])
        #                 ahist.SetFillStyle(self.pattern_list[self.mc_pattern_list[ifile]])
        #                 signal_histlist.append(ahist)

        #         if mode == NORMALIZED:
        #             ahist.SetMarkerSize(5)

        # elif(count == 5):
        #     for ifile in range(len(self.mc_file_list)):
        #         ahist = self.mc_root_files[ifile].Get(hist_name)

        #     if nrebins>0:
        #         ahist = ahist.Rebin(nrebins,hist_name+'rebinned_mc'+str(ifile),xbins)
        #     if ahist == None:
        #         print("histogram %s not found in %s"%(hist_name, self.mc_file_list[ifile]))
        #         print("quitting")
        #         sys.exit(-1)
        #     else:
        #         if underflow_bin:
        #             underflow = ahist.GetBinContent(0)
        #             first_bin_content = ahist.GetBinContent(1)
        #             new_first_bin_content = first_bin_content + underflow
        #             ahist.SetBinContent(1, new_first_bin_content)
        #             ahist.SetBinContent(0, 0)

        #         # Handling overflow
        #         overflow = ahist.GetBinContent(ahist.GetNbinsX() + 1)
        #         last_bin_content = ahist.GetBinContent(ahist.GetNbinsX())
        #         new_last_bin_content = last_bin_content + overflow
        #         ahist.SetBinContent(ahist.GetNbinsX(), new_last_bin_content)
        #         ahist.SetBinContent(ahist.GetNbinsX() + 1, 0)
        #         ahist.Scale(self.sf_list[ifile])

        #         # group by labels
        #         label = self.mc_label_list[ifile]
        #         if label not in hist_group:
        #             hist_group[label] = ahist
        #             labellist.append(label) # need to take care of the order
        #         else:
        #             hist_group[label].Add(ahist)

        #         if 'Signal' not in label:
        #             if mc_hist_sum == None:
        #                 mc_hist_sum = ahist.Clone("mc_hist_sum")
        #             else:
        #                 mc_hist_sum.Add(ahist)
        #             ahist.SetFillColorAlpha(self.color_list[self.mc_color_list[ifile]], self.fill_alpha)
        #             ahist.SetLineColor(self.color_list[self.mc_color_list[ifile]])
        #             ahist.SetFillStyle(self.pattern_list[self.mc_pattern_list[ifile]])

        #         # Signal
        #         else:
        #             if signal_hist is None:
        #                 signal_hist = ahist
        #             else:
        #                 signal_hist.Add(ahist)
        #             ahist.SetFillColorAlpha(self.color_list[self.mc_color_list[ifile]], self.fill_alpha)
        #             ahist.SetLineColor(self.color_list[self.mc_color_list[ifile]])
        #             ahist.SetFillStyle(self.pattern_list[self.mc_pattern_list[ifile]])                    
        #             # ahist.SetLineColor(self.color_list[self.mc_color_list[ifile]])
        #             signal_histlist.append(ahist)

        #     if mode == NORMALIZED:
        #         ahist.SetMarkerSize(5)

        # # not separate the backgrounds with the flips and the fakes
        # if(count < 7):
        #     # Signal
        #     print(self.mc_root_files[0].GetName())
        #     ahist = self.mc_root_files[0].Get(hist_name)
        #     if nrebins>0:
        #         ahist = ahist.Rebin(nrebins,hist_name+'rebinned_mc'+str(0),xbins)
            
        #     if ahist == None:
        #         print("histogram %s not found in %s"%(hist_name, self.mc_file_list[0]))
        #         print("quitting")
        #         sys.exit(-1)
        #     else:
        #         if underflow_bin:
        #             underflow = ahist.GetBinContent(0)
        #             first_bin_content = ahist.GetBinContent(1)
        #             new_first_bin_content = first_bin_content + underflow
        #             ahist.SetBinContent(1, new_first_bin_content)
        #             ahist.SetBinContent(0, 0)
        #         # Handling overflow
        #         overflow = ahist.GetBinContent(ahist.GetNbinsX() + 1)
        #         last_bin_content = ahist.GetBinContent(ahist.GetNbinsX())
        #         new_last_bin_content = last_bin_content + overflow
        #         ahist.SetBinContent(ahist.GetNbinsX(), new_last_bin_content)
        #         ahist.SetBinContent(ahist.GetNbinsX() + 1, 0)
        #         ahist.Scale(self.sf_list[0])

        #         label = self.mc_label_list[0]
        #         if label not in hist_group:
        #             hist_group[label] = ahist
        #             labellist.append(label) # need to take care of the order
        #         else:
        #             hist_group[label].Add(ahist)

        #         if signal_hist is None:
        #             signal_hist = ahist
        #         else:
        #             signal_hist.Add(ahist)
        #         ahist.SetFillColorAlpha(self.color_list[self.mc_color_list[0]], self.fill_alpha)
        #         ahist.SetLineColor(self.color_list[self.mc_color_list[0]])
        #         ahist.SetFillStyle(self.pattern_list[self.mc_pattern_list[0]])
        #         signal_histlist.append(ahist)

        #         error = ctypes.c_double(0.)
        #         histo_int = ahist.IntegralAndError(0,ahist.GetNbinsX() + 1,error)
        #         print(histo_int)
        #         print(error.value)

        #     if mode == NORMALIZED:
        #         ahist.SetMarkerSize(5)

        #     # Background other than minor backgrounds
        #     ahistOther_list = []
        #     ahistOther_total = None
        #     for ifile in range(1,len(self.mc_file_list)):
        #         print(self.mc_root_files[ifile].GetName())
        #         label = self.mc_label_list[ifile]
        #         if(Region == 'SR' and (label == 't#bar{t}2l' or label == 't#bar{t}1l' or label == 't#bar{t}X' or label == 'Singletop' or label == 'Multibosons')) or (Region == 'CR_ttX' and (label == 't#bar{t}2l' or label == 't#bar{t}1l' or label == 't#bar{t}X' or label == 'Multibosons')) or (Region == 'MR_tt2l' and (label == 't#bar{t}2l' or label == 'Singletop')) or ((Region == 'MR_tt1l_SR' or Region == 'MR_tt1l_CR_ttX') and (label == 't#bar{t}2l' or label == 't#bar{t}1l' or label == 'Singletop' or label == 't#bar{t}X')):
        #         # if(label == 't#bar{t}2l' or label == 't#bar{t}1l' or label == 't#bar{t}X' or label == 'Singletop' or label == 'Multibosons'):
        #             ahist = self.mc_root_files[ifile].Get(hist_name)
        #             if nrebins>0:
        #                 ahist = ahist.Rebin(nrebins,hist_name+'rebinned_mc'+str(ifile),xbins)

        #             if ahist == None:
        #                 print("histogram %s not found in %s"%(hist_name, self.mc_file_list[ifile]))
        #                 print("quitting")
        #                 sys.exit(-1)
        #             else:
        #                 print(ahist.GetEntries())
        #                 if underflow_bin:
        #                     underflow = ahist.GetBinContent(0)
        #                     first_bin_content = ahist.GetBinContent(1)
        #                     new_first_bin_content = first_bin_content + underflow
        #                     ahist.SetBinContent(1, new_first_bin_content)
        #                     ahist.SetBinContent(0, 0)
        #                 # Handling overflow
        #                 overflow = ahist.GetBinContent(ahist.GetNbinsX() + 1)
        #                 last_bin_content = ahist.GetBinContent(ahist.GetNbinsX())
        #                 new_last_bin_content = last_bin_content + overflow
        #                 ahist.SetBinContent(ahist.GetNbinsX(), new_last_bin_content)
        #                 ahist.SetBinContent(ahist.GetNbinsX() + 1, 0)
        #                 ahist.Scale(self.sf_list[ifile])

        #                 # group by labels
        #                 label = self.mc_label_list[ifile]
        #                 if label not in hist_group:
        #                     hist_group[label] = ahist
        #                     labellist.append(label) # need to take care of the order
        #                 else:
        #                     hist_group[label].Add(ahist)

        #                 if mc_hist_sum == None:
        #                     mc_hist_sum = ahist.Clone("mc_hist_sum")
        #                 else:
        #                     mc_hist_sum.Add(ahist)
        #                 ahist.SetFillColorAlpha(self.color_list[self.mc_color_list[ifile]], self.fill_alpha)
        #                 ahist.SetLineColor(self.color_list[self.mc_color_list[ifile]])
        #                 ahist.SetFillStyle(self.pattern_list[self.mc_pattern_list[ifile]])

        #                 error = ctypes.c_double(0.)
        #                 histo_int = ahist.IntegralAndError(0,ahist.GetNbinsX() + 1,error)
        #                 print(histo_int)
        #                 print(error.value)

        #             if mode == NORMALIZED:
        #                 ahist.SetMarkerSize(5)

        #         # Other minor backgrounds
        #         else:
        #             ahistOther = self.mc_root_files[ifile].Get(hist_name)
        #             if nrebins>0:
        #                 ahistOther = ahistOther.Rebin(nrebins,hist_name+'rebinned_other'+str(ifile),xbins)

        #             if ahistOther == None:
        #                 print("histogram %s not found in %s"%(hist_name, self.mc_file_list[ifile]))
        #                 print("quitting")
        #                 sys.exit(-1)
        #             else:
        #                 if underflow_bin:
        #                     underflow_Other = ahistOther.GetBinContent(0)
        #                     first_bin_content_Other = ahistOther.GetBinContent(1)
        #                     new_first_bin_content_Other = first_bin_content_Other + underflow_Other
        #                     ahistOther.SetBinContent(1, new_first_bin_content_Other)
        #                     ahistOther.SetBinContent(0, 0)
        #                 # Handling overflow
        #                 nb_entriesOther = ahistOther.GetEntries()
        #                 overflow_Other = ahistOther.GetBinContent(ahistOther.GetNbinsX() + 1)
        #                 last_bin_content_Other = ahistOther.GetBinContent(ahistOther.GetNbinsX())
        #                 new_last_bin_content_Other = last_bin_content_Other + overflow_Other
        #                 ahistOther.SetBinContent(ahistOther.GetNbinsX(), new_last_bin_content_Other)
        #                 ahistOther.SetBinContent(ahistOther.GetNbinsX() + 1, 0)
        #                 ahistOther.SetBinContent(ahistOther.GetNbinsX() + 1, 0)
        #                 ahistOther.SetEntries(nb_entriesOther)
        #                 ahistOther.Scale(self.sf_list[ifile])

        #                 ahistOther_list.append(ahistOther)
        #                 if ahistOther_total == None:
        #                     ahistOther_total = ahistOther
        #                 else:
        #                     ahistOther_total.Add(ahistOther)

        #     hist_group['Others'] = ahistOther_total
        #     labellist.append('Others')
        #     if mc_hist_sum == None:
        #         mc_hist_sum = ahistOther_total.Clone("mc_hist_sum")
        #     else:
        #         mc_hist_sum.Add(ahistOther_total)
        #     mc_hist_sum.Add(ahistOther_total)
        #     ahistOther_total.SetFillColorAlpha(self.ROOT.kOrange+3, self.fill_alpha)
        #     ahistOther_total.SetLineColor(self.ROOT.kOrange+3)
        #     ahistOther_total.SetFillStyle(self.pattern_list[self.mc_pattern_list[0]])

        #     error = ctypes.c_double(0.)
        #     histo_int = ahistOther_total.IntegralAndError(0,ahistOther_total.GetNbinsX() + 1,error)
        #     print(histo_int)
        #     print(error.value)

        #     if mode == NORMALIZED:
        #         ahistOther_total.SetMarkerSize(5)

        # separate the backgrounds with the flips and the fakes
        if(count == 5):
            if(not Flip and not Fake):
                # Signal
                print(self.mc_root_files[0].GetName())
                ahist = self.mc_root_files[0].Get(hist_name)
                if nrebins>0:
                    ahist = ahist.Rebin(nrebins,hist_name+'rebinned_mc'+str(0),xbins)
            
                if ahist == None:
                    print("histogram %s not found in %s"%(hist_name, self.mc_file_list[0]))
                    print("quitting")
                    sys.exit(-1)
                else:
                    if underflow_bin:
                        underflow = ahist.GetBinContent(0)
                        first_bin_content = ahist.GetBinContent(1)
                        new_first_bin_content = first_bin_content + underflow
                        ahist.SetBinContent(1, new_first_bin_content)
                        ahist.SetBinContent(0, 0)
                    # Handling overflow
                    overflow = ahist.GetBinContent(ahist.GetNbinsX() + 1)
                    last_bin_content = ahist.GetBinContent(ahist.GetNbinsX())
                    new_last_bin_content = last_bin_content + overflow
                    ahist.SetBinContent(ahist.GetNbinsX(), new_last_bin_content)
                    ahist.SetBinContent(ahist.GetNbinsX() + 1, 0)
                    ahist.Scale(self.sf_list[0])

                    label = self.mc_label_list[0]
                    if label not in hist_group:
                        hist_group[label] = ahist
                        labellist.append(label) # need to take care of the order
                    else:
                        hist_group[label].Add(ahist)

                    if signal_hist is None:
                        signal_hist = ahist
                    else:
                        signal_hist.Add(ahist)
                    ahist.SetFillColorAlpha(self.color_list[self.mc_color_list[0]], self.fill_alpha)
                    ahist.SetLineColor(self.color_list[self.mc_color_list[0]])
                    ahist.SetFillStyle(self.pattern_list[self.mc_pattern_list[0]])
                    signal_histlist.append(ahist)

                    error = ctypes.c_double(0.)
                    histo_int = ahist.IntegralAndError(0,ahist.GetNbinsX() + 1,error)
                    print(histo_int)
                    print(error.value)

                if mode == NORMALIZED:
                    ahist.SetMarkerSize(5)

                # Background other than flip and fake
                ahistOther_list = []
                ahistOther_total = None
                for ifile in range(1,len(self.mc_file_list)):
                    print(self.mc_root_files[ifile].GetName())
                    label = self.mc_label_list[ifile]
                    if((Region == 'SR' or Region == 'CR_ttX') and (label == 't#bar{t}X' or label == 'Multibosons')) or (Region == 'MR_tt2l' and (label == 't#bar{t}2l' or label == 'Singletop')) or ((Region == 'MR_tt1l_SR' or Region == 'MR_tt1l_CR_ttX') and label == 't#bar{t}X'):
                        ahist = self.mc_root_files[ifile].Get(hist_name + '_0')
                        if nrebins>0:
                            ahist = ahist.Rebin(nrebins,hist_name+'rebinned_mc'+str(ifile),xbins)

                        if ahist == None:
                            print("histogram %s not found in %s"%(hist_name, self.mc_file_list[ifile]))
                            print("quitting")
                            sys.exit(-1)
                        else:
                            if underflow_bin:
                                underflow = ahist.GetBinContent(0)
                                first_bin_content = ahist.GetBinContent(1)
                                new_first_bin_content = first_bin_content + underflow
                                ahist.SetBinContent(1, new_first_bin_content)
                                ahist.SetBinContent(0, 0)
                            # Handling overflow
                            overflow = ahist.GetBinContent(ahist.GetNbinsX() + 1)
                            last_bin_content = ahist.GetBinContent(ahist.GetNbinsX())
                            new_last_bin_content = last_bin_content + overflow
                            ahist.SetBinContent(ahist.GetNbinsX(), new_last_bin_content)
                            ahist.SetBinContent(ahist.GetNbinsX() + 1, 0)
                            ahist.Scale(self.sf_list[ifile])
                            print(ahist.Integral())

                            # group by labels
                            label = self.mc_label_list[ifile]
                            if label not in hist_group:
                                hist_group[label] = ahist
                                labellist.append(label) # need to take care of the order
                            else:
                                hist_group[label].Add(ahist)

                            if mc_hist_sum == None:
                                mc_hist_sum = ahist.Clone("mc_hist_sum")
                            else:
                                mc_hist_sum.Add(ahist)
                            ahist.SetFillColorAlpha(self.color_list[self.mc_color_list[ifile]], self.fill_alpha)
                            ahist.SetLineColor(self.color_list[self.mc_color_list[ifile]])
                            ahist.SetFillStyle(self.pattern_list[self.mc_pattern_list[ifile]])

                            error = ctypes.c_double(0.)
                            histo_int = ahist.IntegralAndError(0,ahist.GetNbinsX() + 1,error)
                            print(histo_int)
                            print(error.value)

                            if mode == NORMALIZED:
                                ahist.SetMarkerSize(5)

                    # Other minor backgrounds
                    else:
                        ahistOther = self.mc_root_files[ifile].Get(hist_name + '_0')
                        if nrebins>0:
                            ahistOther = ahistOther.Rebin(nrebins,hist_name+'rebinned_other'+str(ifile),xbins)

                        if ahistOther == None:
                            print("histogram %s not found in %s"%(hist_name, self.mc_file_list[ifile]))
                            print("quitting")
                            sys.exit(-1)
                        else:
                            if underflow_bin:
                                underflow_Other = ahistOther.GetBinContent(0)
                                first_bin_content_Other = ahistOther.GetBinContent(1)
                                new_first_bin_content_Other = first_bin_content_Other + underflow_Other
                                ahistOther.SetBinContent(1, new_first_bin_content_Other)
                                ahistOther.SetBinContent(0, 0)
                            # Handling overflow
                            nb_entriesOther = ahistOther.GetEntries()
                            overflow_Other = ahistOther.GetBinContent(ahistOther.GetNbinsX() + 1)
                            last_bin_content_Other = ahistOther.GetBinContent(ahistOther.GetNbinsX())
                            new_last_bin_content_Other = last_bin_content_Other + overflow_Other
                            ahistOther.SetBinContent(ahistOther.GetNbinsX(), new_last_bin_content_Other)
                            ahistOther.SetBinContent(ahistOther.GetNbinsX() + 1, 0)
                            ahistOther.SetBinContent(ahistOther.GetNbinsX() + 1, 0)
                            ahistOther.SetEntries(nb_entriesOther)
                            ahistOther.Scale(self.sf_list[ifile])
                            print(ahistOther.Integral())

                            ahistOther_list.append(ahistOther)
                            if ahistOther_total == None:
                                ahistOther_total = ahistOther
                            else:
                                ahistOther_total.Add(ahistOther)

                # flip except in MR tt2l
                if Region != 'MR_tt2l':
                    ahistCharge_list = []
                    ahistCharge_total = None
                    for ifile in range(1,len(self.mc_file_list)):
                        print(self.mc_root_files[ifile].GetName())
                        label = self.mc_label_list[ifile]
                        ahistCharge = self.mc_root_files[ifile].Get(hist_name + '_1')
                        if nrebins > 0:
                            ahistCharge = ahistCharge.Rebin(nrebins,hist_name+'rebinned_charge'+str(ifile), xbins)
                
                        if underflow_bin:
                            underflow_Charge = ahistCharge.GetBinContent(0)
                            first_bin_content_Charge = ahistCharge.GetBinContent(1)
                            new_first_bin_content_Charge = first_bin_content_Charge + underflow_Charge
                            ahistCharge.SetBinContent(1, new_first_bin_content_Charge)
                            ahistCharge.SetBinContent(0, 0)
                        # Handling overflow
                        nb_entriesCharge = ahistCharge.GetEntries()
                        overflow_Charge = ahistCharge.GetBinContent(ahistCharge.GetNbinsX() + 1)
                        last_bin_content_Charge = ahistCharge.GetBinContent(ahistCharge.GetNbinsX())
                        new_last_bin_content_Charge = last_bin_content_Charge + overflow_Charge
                        ahistCharge.SetBinContent(ahistCharge.GetNbinsX(), new_last_bin_content_Charge)
                        ahistCharge.SetBinContent(ahistCharge.GetNbinsX() + 1, 0)
                        ahistCharge.SetEntries(nb_entriesCharge)
                        ahistCharge.Scale(self.sf_list[ifile])
                        print(ahistCharge.Integral())

                        ahistCharge_list.append(ahistCharge)
                        if ahistCharge_total == None:
                            ahistCharge_total = ahistCharge
                        else:
                            ahistCharge_total.Add(ahistCharge)

                    hist_group['Charge flip'] = ahistCharge_total
                    labellist.append('Charge flip')
                    mc_hist_sum.Add(ahistCharge_total)
                    ahistCharge_total.SetFillColorAlpha(self.ROOT.TColor.GetColor('#5b27a3'), self.fill_alpha)
                    ahistCharge_total.SetLineColor(self.ROOT.TColor.GetColor('#5b27a3'))
                    ahistCharge_total.SetFillStyle(self.pattern_list[self.mc_pattern_list[0]])

                    error = ctypes.c_double(0.)
                    histo_int = ahistCharge_total.IntegralAndError(0,ahistCharge_total.GetNbinsX() + 1,error)
                    print(histo_int)
                    print(error.value)

                    if mode == NORMALIZED:
                        ahistCharge_total.SetMarkerSize(5)

                # fake
                ahistFake_list = []
                ahistFake_total = None
                for ifile in range(1,len(self.mc_file_list)):
                    print(self.mc_root_files[ifile].GetName())
                    label = self.mc_label_list[ifile]
                    ahistFake = self.mc_root_files[ifile].Get(hist_name + '_2')
                    if nrebins > 0:
                        ahistFake = ahistFake.Rebin(nrebins,hist_name+'rebinned_fake'+str(ifile), xbins)
                
                    if underflow_bin:
                        underflow_Fake = ahistFake.GetBinContent(0)
                        first_bin_content_Fake = ahistFake.GetBinContent(1)
                        new_first_bin_content_Fake = first_bin_content_Fake + underflow_Fake
                        ahistFake.SetBinContent(1, new_first_bin_content_Fake)
                        ahistFake.SetBinContent(0, 0)
                    # Handling overflow
                    nb_entriesFake = ahistFake.GetEntries()
                    overflow_Fake = ahistFake.GetBinContent(ahistFake.GetNbinsX() + 1)
                    last_bin_content_Fake = ahistFake.GetBinContent(ahistFake.GetNbinsX())
                    new_last_bin_content_Fake = last_bin_content_Fake + overflow_Fake
                    ahistFake.SetBinContent(ahistFake.GetNbinsX(), new_last_bin_content_Fake)
                    ahistFake.SetBinContent(ahistFake.GetNbinsX() + 1, 0)
                    ahistFake.SetEntries(nb_entriesFake)
                    ahistFake.Scale(self.sf_list[ifile])
                    print(ahistFake.Integral())

                    ahistFake_list.append(ahistFake)
                    if ahistFake_total == None:
                        ahistFake_total = ahistFake
                    else:
                        ahistFake_total.Add(ahistFake)

                hist_group['Non prompt'] = ahistFake_total
                labellist.append('Non prompt')
                mc_hist_sum.Add(ahistFake_total)
                ahistFake_total.SetFillColorAlpha(self.ROOT.TColor.GetColor('#02e002'), self.fill_alpha)
                ahistFake_total.SetLineColor(self.ROOT.TColor.GetColor('#02e002'))
                ahistFake_total.SetFillStyle(self.pattern_list[self.mc_pattern_list[0]])

                error = ctypes.c_double(0.)
                histo_int = ahistFake_total.IntegralAndError(0,ahistFake_total.GetNbinsX() + 1,error)
                print(histo_int)
                print(error.value)

                if mode == NORMALIZED:
                    ahistFake_total.SetMarkerSize(5)

                hist_group['Others'] = ahistOther_total
                labellist.append('Others')
                mc_hist_sum.Add(ahistOther_total)
                ahistOther_total.SetFillColorAlpha(self.ROOT.kOrange+3, self.fill_alpha)
                ahistOther_total.SetLineColor(self.ROOT.kOrange+3)
                ahistOther_total.SetFillStyle(self.pattern_list[self.mc_pattern_list[0]])

                error = ctypes.c_double(0.)
                histo_int = ahistOther_total.IntegralAndError(0,ahistOther_total.GetNbinsX() + 1,error)
                print(histo_int)
                print(error.value)

                if mode == NORMALIZED:
                    ahistOther_total.SetMarkerSize(5)

            if Flip and Fake and (Region == 'SR' or Region == 'CR_ttX'):
                # Signal
                ahist = self.mc_root_files[0].Get(hist_name)
                if nrebins>0:
                    ahist = ahist.Rebin(nrebins,hist_name+'rebinned_mc'+str(0),xbins)
            
                if ahist == None:
                    print("histogram %s not found in %s"%(hist_name, self.mc_file_list[0]))
                    print("quitting")
                    sys.exit(-1)
                else:
                    if underflow_bin:
                        underflow = ahist.GetBinContent(0)
                        first_bin_content = ahist.GetBinContent(1)
                        new_first_bin_content = first_bin_content + underflow
                        ahist.SetBinContent(1, new_first_bin_content)
                        ahist.SetBinContent(0, 0)
                    # Handling overflow
                    overflow = ahist.GetBinContent(ahist.GetNbinsX() + 1)
                    last_bin_content = ahist.GetBinContent(ahist.GetNbinsX())
                    new_last_bin_content = last_bin_content + overflow
                    ahist.SetBinContent(ahist.GetNbinsX(), new_last_bin_content)
                    ahist.SetBinContent(ahist.GetNbinsX() + 1, 0)
                    ahist.Scale(self.sf_list[0])                      

                    label = self.mc_label_list[0]
                    if label not in hist_group:
                        hist_group[label] = ahist
                        labellist.append(label) # need to take care of the order
                    else:
                        hist_group[label].Add(ahist)

                    if signal_hist is None:
                        signal_hist = ahist
                    else:
                        signal_hist.Add(ahist)
                    ahist.SetFillColorAlpha(self.color_list[self.mc_color_list[0]], self.fill_alpha)
                    ahist.SetLineColor(self.color_list[self.mc_color_list[0]])
                    ahist.SetFillStyle(self.pattern_list[self.mc_pattern_list[0]])
                    signal_histlist.append(ahist)

                    error = ctypes.c_double(0.)
                    histo_int = ahist.IntegralAndError(0,ahist.GetNbinsX() + 1,error)
                    print(histo_int)
                    print(error.value)

                if mode == NORMALIZED:
                    ahist.SetMarkerSize(5)
                
                # Background other than flip and fake
                ahistOther_list = []
                ahistOther_total = None
                for ifile in range(1,len(self.mc_file_list)):
                    label = self.mc_label_list[ifile]
                    print(self.mc_root_files[ifile].GetName())
                    is_data_driven = 'DataDriven' in label
                    if(not is_data_driven and ((Region == 'SR' or Region == 'CR_ttX') and (label == 't#bar{t}X' or label == 'Multibosons')) or ((Region == 'MR_tt1l_SR' or Region == 'MR_tt1l_CR_ttX') and label == 't#bar{t}X')):
                        ahist = self.mc_root_files[ifile].Get(hist_name + '_0')
                        if nrebins>0:
                            ahist = ahist.Rebin(nrebins,hist_name+'rebinned_mc'+str(ifile),xbins)

                        if ahist == None:
                            print("histogram %s not found in %s"%(hist_name, self.mc_file_list[ifile]))
                            print("quitting")
                            sys.exit(-1)
                        else:
                            if underflow_bin:
                                underflow = ahist.GetBinContent(0)
                                first_bin_content = ahist.GetBinContent(1)
                                new_first_bin_content = first_bin_content + underflow
                                ahist.SetBinContent(1, new_first_bin_content)
                                ahist.SetBinContent(0, 0)
                            # Handling overflow
                            overflow = ahist.GetBinContent(ahist.GetNbinsX() + 1)
                            last_bin_content = ahist.GetBinContent(ahist.GetNbinsX())
                            new_last_bin_content = last_bin_content + overflow
                            ahist.SetBinContent(ahist.GetNbinsX(), new_last_bin_content)
                            ahist.SetBinContent(ahist.GetNbinsX() + 1, 0)
                            ahist.Scale(self.sf_list[ifile])
                            print(ahist.Integral())

                            # group by labels
                            label = self.mc_label_list[ifile]
                            if label not in hist_group:
                                hist_group[label] = ahist
                                labellist.append(label) # need to take care of the order
                            else:
                                hist_group[label].Add(ahist)

                            if mc_hist_sum == None:
                                mc_hist_sum = ahist.Clone("mc_hist_sum")
                            else:
                                mc_hist_sum.Add(ahist)
                            ahist.SetFillColorAlpha(self.color_list[self.mc_color_list[ifile]], self.fill_alpha)
                            ahist.SetLineColor(self.color_list[self.mc_color_list[ifile]])
                            ahist.SetFillStyle(self.pattern_list[self.mc_pattern_list[ifile]])

                            error = ctypes.c_double(0.)
                            histo_int = ahist.IntegralAndError(0,ahist.GetNbinsX() + 1,error)
                            print(histo_int)
                            print(error.value)

                            if mode == NORMALIZED:
                                ahist.SetMarkerSize(5)

                    # Other minor backgrounds
                    elif not is_data_driven:
                        ahistOther = self.mc_root_files[ifile].Get(hist_name + '_0')
                        if nrebins>0:
                            ahistOther = ahistOther.Rebin(nrebins,hist_name+'rebinned_other'+str(ifile),xbins)

                        if ahistOther == None:
                            print("histogram %s not found in %s"%(hist_name, self.mc_file_list[ifile]))
                            print("quitting")
                            sys.exit(-1)
                        else:
                            if underflow_bin:
                                underflow_Other = ahistOther.GetBinContent(0)
                                first_bin_content_Other = ahistOther.GetBinContent(1)
                                new_first_bin_content_Other = first_bin_content_Other + underflow_Other
                                ahistOther.SetBinContent(1, new_first_bin_content_Other)
                                ahistOther.SetBinContent(0, 0)
                            # Handling overflow
                            nb_entriesOther = ahistOther.GetEntries()
                            overflow_Other = ahistOther.GetBinContent(ahistOther.GetNbinsX() + 1)
                            last_bin_content_Other = ahistOther.GetBinContent(ahistOther.GetNbinsX())
                            new_last_bin_content_Other = last_bin_content_Other + overflow_Other
                            ahistOther.SetBinContent(ahistOther.GetNbinsX(), new_last_bin_content_Other)
                            ahistOther.SetBinContent(ahistOther.GetNbinsX() + 1, 0)
                            ahistOther.SetBinContent(ahistOther.GetNbinsX() + 1, 0)
                            ahistOther.SetEntries(nb_entriesOther)
                            ahistOther.Scale(self.sf_list[ifile])
                            print(ahistOther.Integral())

                            ahistOther_list.append(ahistOther)
                            if ahistOther_total == None:
                                ahistOther_total = ahistOther
                            else:
                                ahistOther_total.Add(ahistOther)

                # flip
                ahistCharge_list = []
                ahistCharge_total = None
                for ifile in range(1,len(self.mc_file_list)):
                    print(self.mc_root_files[ifile].GetName())
                    # print(self.sf_list[ifile])
                    label = self.mc_label_list[ifile]
                    is_data_driven = 'tt2l_{DataDriven}' in label
                    if is_data_driven:
                        if(Region == 'SR'):
                            ahistCharge = self.Flip_dD_h_copy_forFlipPlot_SR
                        elif(Region == 'CR_ttX'):
                            ahistCharge = self.Flip_dD_h_copy_forFlipPlot_CRttX
                    # elif not 'TT_2L' in self.mc_root_files[ifile].GetName() and not 'Fake_dataDriven' in self.mc_root_files[ifile].GetName():
                    #     ahistCharge = self.mc_root_files[ifile].Get(hist_name + '_1')
                    else:
                        continue
                    # print(ahistCharge.Integral())
                    if nrebins > 0:
                        ahistCharge = ahistCharge.Rebin(nrebins,hist_name+'rebinned_charge'+str(ifile), xbins)

                    if underflow_bin:
                        underflow_Charge = ahistCharge.GetBinContent(0)
                        first_bin_content_Charge = ahistCharge.GetBinContent(1)
                        new_first_bin_content_Charge = first_bin_content_Charge + underflow_Charge
                        ahistCharge.SetBinContent(1, new_first_bin_content_Charge)
                        ahistCharge.SetBinContent(0, 0)
                    # Handling overflow
                    nb_entriesCharge = ahistCharge.GetEntries()
                    overflow_Charge = ahistCharge.GetBinContent(ahistCharge.GetNbinsX() + 1)
                    last_bin_content_Charge = ahistCharge.GetBinContent(ahistCharge.GetNbinsX())
                    new_last_bin_content_Charge = last_bin_content_Charge + overflow_Charge
                    ahistCharge.SetBinContent(ahistCharge.GetNbinsX(), new_last_bin_content_Charge)
                    ahistCharge.SetBinContent(ahistCharge.GetNbinsX() + 1, 0)
                    ahistCharge.SetEntries(nb_entriesCharge)
                    ahistCharge.Scale(self.sf_list[ifile])
                    print(ahistCharge.Integral())

                    ahistCharge_list.append(ahistCharge)
                    if ahistCharge_total == None:
                        ahistCharge_total = ahistCharge
                    else:
                        ahistCharge_total.Add(ahistCharge)

                hist_group['Charge flip'] = ahistCharge_total
                # print(ahistCharge_total.Integral())
                labellist.append('Charge flip')
                mc_hist_sum.Add(ahistCharge_total)
                ahistCharge_total.SetFillColorAlpha(self.ROOT.TColor.GetColor('#5b27a3'), self.fill_alpha)
                ahistCharge_total.SetLineColor(self.ROOT.TColor.GetColor('#5b27a3'))
                ahistCharge_total.SetFillStyle(self.pattern_list[self.mc_pattern_list[0]])

                error = ctypes.c_double(0.)
                histo_int = ahistCharge_total.IntegralAndError(0,ahistCharge_total.GetNbinsX() + 1,error)
                print(histo_int)
                print(error.value)

                if mode == NORMALIZED:
                    ahistCharge_total.SetMarkerSize(5)

                # fake
                ahistFake_list = []
                ahistFake_total = None
                for ifile in range(1,len(self.mc_file_list)):
                    print(self.mc_root_files[ifile].GetName())
                    # print(self.sf_list[ifile])
                    label = self.mc_label_list[ifile]
                    is_data_driven = 'tt1l_{DataDriven}' in label
                    if is_data_driven:
                        if(Region == 'SR'):
                            ahistFake = self.Fake_dD_h_copy_forFakePlot_SR
                        elif(Region == 'CR_ttX'):
                            ahistFake = self.Fake_dD_h_copy_forFakePlot_CRttX
                    # elif not 'TT_SL' in self.mc_root_files[ifile].GetName() and not 'Flip_dataDriven' in self.mc_root_files[ifile].GetName():
                    #     ahistFake = self.mc_root_files[ifile].Get(hist_name + '_2')
                    else:
                        continue
                    # print(ahistFake.Integral())
                    if nrebins > 0:
                        ahistFake = ahistFake.Rebin(nrebins,hist_name+'rebinned_fake'+str(ifile), xbins)
                
                    if underflow_bin:
                        underflow_Fake = ahistFake.GetBinContent(0)
                        first_bin_content_Fake = ahistFake.GetBinContent(1)
                        new_first_bin_content_Fake = first_bin_content_Fake + underflow_Fake
                        ahistFake.SetBinContent(1, new_first_bin_content_Fake)
                        ahistFake.SetBinContent(0, 0)
                    # Handling overflow
                    nb_entriesFake = ahistFake.GetEntries()
                    overflow_Fake = ahistFake.GetBinContent(ahistFake.GetNbinsX() + 1)
                    last_bin_content_Fake = ahistFake.GetBinContent(ahistFake.GetNbinsX())
                    new_last_bin_content_Fake = last_bin_content_Fake + overflow_Fake
                    ahistFake.SetBinContent(ahistFake.GetNbinsX(), new_last_bin_content_Fake)
                    ahistFake.SetBinContent(ahistFake.GetNbinsX() + 1, 0)
                    ahistFake.SetEntries(nb_entriesFake)
                    ahistFake.Scale(self.sf_list[ifile])
                    print(ahistFake.Integral())

                    ahistFake_list.append(ahistFake)
                    if ahistFake_total == None:
                        ahistFake_total = ahistFake
                    else:
                        ahistFake_total.Add(ahistFake)

                hist_group['Non prompt'] = ahistFake_total
                # print(ahistFake_total.Integral())
                labellist.append('Non prompt')
                mc_hist_sum.Add(ahistFake_total)
                ahistFake_total.SetFillColorAlpha(self.ROOT.TColor.GetColor('#02e002'), self.fill_alpha)
                ahistFake_total.SetLineColor(self.ROOT.TColor.GetColor('#02e002'))
                ahistFake_total.SetFillStyle(self.pattern_list[self.mc_pattern_list[0]])

                error = ctypes.c_double(0.)
                histo_int = ahistFake_total.IntegralAndError(0,ahistFake_total.GetNbinsX() + 1,error)
                print(histo_int)
                print(error.value)

                if mode == NORMALIZED:
                    ahistFake_total.SetMarkerSize(5)

                hist_group['Others'] = ahistOther_total
                labellist.append('Others')
                mc_hist_sum.Add(ahistOther_total)
                ahistOther_total.SetFillColorAlpha(self.ROOT.kOrange+3, self.fill_alpha)
                ahistOther_total.SetLineColor(self.ROOT.kOrange+3)
                ahistOther_total.SetFillStyle(self.pattern_list[self.mc_pattern_list[0]])

                error = ctypes.c_double(0.)
                histo_int = ahistOther_total.IntegralAndError(0,ahistOther_total.GetNbinsX() + 1,error)
                print(histo_int)
                print(error.value)

                if mode == NORMALIZED:
                    ahistOther_total.SetMarkerSize(5)

            if Flip and Region == 'MR_tt2l':
                # tt2l only
                for ifile in range(1,len(self.mc_file_list)):
                    print(self.mc_root_files[ifile])
                    label = self.mc_label_list[ifile]
                    is_data_driven = 'DataDriven' in label
                    if label == 't#bar{t}2l' and not is_data_driven:
                        ahist = self.mc_root_files[ifile].Get(hist_name + '_0')
                        if nrebins>0:
                            ahist = ahist.Rebin(nrebins,hist_name+'rebinned_flip'+str(ifile),xbins)

                        if ahist == None:
                            print("histogram %s not found in %s"%(hist_name, self.mc_file_list[ifile]))
                            print("quitting")
                            sys.exit(-1)
                        else:
                            if underflow_bin:
                                underflow = ahist.GetBinContent(0)
                                first_bin_content = ahist.GetBinContent(1)
                                new_first_bin_content = first_bin_content + underflow
                                ahist.SetBinContent(1, new_first_bin_content)
                                ahist.SetBinContent(0, 0)
                            # Handling overflow
                            overflow = ahist.GetBinContent(ahist.GetNbinsX() + 1)
                            last_bin_content = ahist.GetBinContent(ahist.GetNbinsX())
                            new_last_bin_content = last_bin_content + overflow
                            ahist.SetBinContent(ahist.GetNbinsX(), new_last_bin_content)
                            ahist.SetBinContent(ahist.GetNbinsX() + 1, 0)
                            ahist.Scale(self.sf_list[ifile])         
                            print(ahist.Integral())             

                            # group by labels
                            label = self.mc_label_list[ifile]
                            if label not in hist_group:
                                hist_group[label] = ahist
                                labellist.append(label) # need to take care of the order
                            else:
                                hist_group[label].Add(ahist)

                            if mc_hist_sum == None:
                                mc_hist_sum = ahist.Clone("mc_hist_sum")
                            else:
                                mc_hist_sum.Add(ahist)
                            ahist.SetFillColorAlpha(self.color_list[self.mc_color_list[ifile]], self.fill_alpha)
                            ahist.SetLineColor(self.color_list[self.mc_color_list[ifile]])
                            ahist.SetFillStyle(self.pattern_list[self.mc_pattern_list[ifile]])

                            error = ctypes.c_double(0.)
                            histo_int = ahist.IntegralAndError(0,ahist.GetNbinsX() + 1,error)
                            print(histo_int)
                            print(error.value)

                            if mode == NORMALIZED:
                                ahist.SetMarkerSize(5)

            if Fake and (Region == 'MR_tt1l_SR' or Region == 'MR_tt1l_CR_ttX'):
                ahistFake_list = []
                ahistFake_total = None
                for ifile in range(1,len(self.mc_file_list)):
                    print(self.mc_root_files[ifile])
                    label = self.mc_label_list[ifile]
                    is_data_driven = 'DataDriven' in label
                    if not is_data_driven:
                        ahistFake = self.mc_root_files[ifile].Get(hist_name + '_2')
                        if nrebins>0:
                            ahistFake = ahistFake.Rebin(nrebins,hist_name+'rebinned_fake'+str(ifile),xbins)

                        if ahistFake == None:
                            print("histogram %s not found in %s"%(hist_name, self.mc_file_list[ifile]))
                            print("quitting")
                            sys.exit(-1)
                        else:
                            if underflow_bin:
                                underflow_Fake = ahistFake.GetBinContent(0)
                                first_bin_content_Fake = ahistFake.GetBinContent(1)
                                new_first_bin_content_Fake = first_bin_content_Fake + underflow_Fake
                                ahistFake.SetBinContent(1, new_first_bin_content_Fake)
                                ahistFake.SetBinContent(0, 0)
                            # Handling overflow
                            nb_EntriesFake = ahistFake.GetEntries()
                            overflow_Fake = ahistFake.GetBinContent(ahistFake.GetNbinsX() + 1)
                            last_bin_content_Fake = ahistFake.GetBinContent(ahistFake.GetNbinsX())
                            new_last_bin_content_Fake = last_bin_content_Fake + overflow_Fake
                            ahistFake.SetBinContent(ahistFake.GetNbinsX(), new_last_bin_content_Fake)
                            ahistFake.SetBinContent(ahistFake.GetNbinsX() + 1, 0)
                            ahistFake.SetEntries(nb_EntriesFake)
                            ahistFake.Scale(self.sf_list[ifile])
                            # print(nb_EntriesFake)
                            print(ahistFake.Integral())

                            ahistFake_list.append(ahistFake)
                            if ahistFake_total == None:
                                ahistFake_total = ahistFake
                            else:
                                ahistFake_total.Add(ahistFake)
                            # print(ahistFake_total.Integral())

                            # if mc_hist_sum == None:
                            #     mc_hist_sum = ahist.Clone("mc_hist_sum")
                            # else:
                            #     mc_hist_sum.Add(ahist)
                            # ahist.SetFillColorAlpha(self.color_list[self.mc_color_list[ifile]], self.fill_alpha)
                            # ahist.SetLineColor(self.color_list[self.mc_color_list[ifile]])
                            # ahist.SetFillStyle(self.pattern_list[self.mc_pattern_list[ifile]])

                            # if mode == NORMALIZED:
                            #     ahist.SetMarkerSize(5)

                hist_group['Non prompt'] = ahistFake_total
                labellist.append('Non prompt')
                if mc_hist_sum == None:
                    mc_hist_sum = ahistFake_total.Clone("mc_hist_sum")
                else:
                    mc_hist_sum.Add(ahistFake_total)
                ahistFake_total.SetFillColorAlpha(self.ROOT.TColor.GetColor('#02e002'), self.fill_alpha)
                ahistFake_total.SetLineColor(self.ROOT.TColor.GetColor('#02e002'))
                ahistFake_total.SetFillStyle(self.pattern_list[self.mc_pattern_list[0]])

                error = ctypes.c_double(0.)
                histo_int = ahistFake_total.IntegralAndError(0,ahistFake_total.GetNbinsX() + 1,error)
                print(histo_int)
                print(error.value)

                if mode == NORMALIZED:
                    ahistFake_total.SetMarkerSize(5)

     ########################## Now we create the stack histo and the legend ##########################

        # List to reorder the labels in the right way
        labels_in_order = []

        # Organize and Stack Monte Carlos Backgrounds
        normevts = dict()
        for label in labellist:
            if not "Signal" in label:
                normevts[label] = hist_group[label].Integral()

        sumMC = 0
        if not(Flip and Region == 'MR_tt2l') and not(Fake and (Region == 'MR_tt1l_SR' or Region == 'MR_tt1l_CR_ttX')):
            labels_in_order.append(signal)

        nbins = 0
        integer = 0

        # for label in reordered_labellist:
        for label in labellist:
            ahist = hist_group[label]
            print(label+" Integral: %3.2f"%ahist.Integral())
            print(label+" Entries: %f"%ahist.GetEntries())
            sumMC += ahist.Integral()

            if 'Signal' not in label:
                if mode == NORMALIZED:
                    ahistcopy = ahist.Clone()
                    normscale = ahistcopy.Integral()
                    print('normscale_label=',normscale)
                    ahistcopy.Scale(1.0/normscale)
                    hs.Add(ahistcopy)
                    labels_in_order.append(label)
                else:
                    hs.Add(ahist)
                    labels_in_order.append(label)
                    if Paper:
                        error = ctypes.c_double(0.)
                        histo_int = ahist.IntegralAndError(0,ahist.GetNbinsX() + 1,error)
                        print(histo_int)
                        print(error.value)
                        if integer == 0:
                            nbins = ahist.GetNbinsX()
                            h_total = ahist.Clone("h_total")
                        for i in range(3, nbins + 1):
                            content = ahist.GetBinContent(i)
                            error2 = ahist.GetBinError(i)*ahist.GetBinError(i)
                            if integer == 0:
                                h_total.SetBinContent(i, content)
                                h_total.SetBinError(i, error2)
                            else:
                                h_total.SetBinContent(i, h_total.GetBinContent(i) + content)
                                h_total.SetBinError(i, h_total.GetBinError(i) + error2)
                        if integer == 0:
                            integer = 1

        print(f'Total background Integral: {sumMC}')

        # print(f'signal Integral: {signal_hist.Integral()}')
        # print(f'signal Entries: {signal_hist.GetEntries()}')
        # print(f'S/B: {signal_hist.Integral()/sumMC}')

        if mode == NORMALIZED:
            ahistcopy_sig = signal_hist.Clone()
            normscale = ahistcopy_sig.Integral()
            print('normscale_label = ',normscale)
            ahistcopy_sig.Scale(1.0/normscale)
            hs.Add(ahistcopy_sig)
            # labels_in_order.append(signal)

        elif not(Flip and Region == 'MR_tt2l') and not(Fake and (Region == 'MR_tt1l_SR' or Region == 'MR_tt1l_CR_ttX')):
        # else:
            hs.Add(signal_hist)
            # labels_in_order.append(signal)
            if Paper :
                for i in range(3, signal_hist.GetNbinsX()+1):
                    content = signal_hist.GetBinContent(i)
                    error2 = signal_hist.GetBinError(i)*signal_hist.GetBinError(i)
                    h_total.SetBinContent(i, h_total.GetBinContent(i) + content)
                    h_total.SetBinError(i, h_total.GetBinError(i) + error2)

        if Paper :
            g_errors = ROOT.TGraphErrors(nbins)
            g_errors_ratio = ROOT.TGraphErrors(nbins)
            for i in range(3, nbins + 1):
                x = h_total.GetBinCenter(i)
                y = h_total.GetBinContent(i)
                ex = h_total.GetBinWidth(i) / 2
                ey = math.sqrt(h_total.GetBinError(i))
                g_errors.SetPoint(i - 1, x, y)
                g_errors.SetPointError(i - 1, ex, ey)
                y_ratio = 1
                if y == 0:
                    ey_ratio = 0
                else:
                    ey_ratio = ey/y
                g_errors_ratio.SetPoint(i - 1, x, y_ratio)
                g_errors_ratio.SetPointError(i - 1, ex, ey_ratio)

        # labels_in_order.reverse()

        # Put MC labels in the correct order
        for label in labels_in_order:
            ahist = hist_group[label]
            if 'Signal' in label:
                tl.AddEntry(ahist, 'm_{T\'} = '+mass+' GeV', "F")
            else:
                if 't#bar{t}2l' in label and Flip and Region == 'MR_tt2l':
                    tl.AddEntry(ahist, 'MC t#bar{t}2l', "F")
                elif 'Non prompt' in label and Fake and (Region == 'MR_tt1l_SR' or Region == 'MR_tt1l_CR_ttX'):
                    tl.AddEntry(ahist, 'MC Non prompt', "F")
                else:
                    tl.AddEntry(ahist, label, "F")

     ########################## DATA input ##########################
        final_data_hist = None
        # not considering the data in SR (for the moment)
        if(Region == 'CR_ttX' or Region == 'MR_tt2l' or Region == 'MR_tt1l_SR' or Region == 'MR_tt1l_CR_ttX') and self.data_file_list:
            for ifile in self.data_file_list:
                atfile = self.ROOT.TFile(ifile)
                if Flip and Region == 'MR_tt2l':
                    ahist = self.Flip_dD_h_copy_intt2lregion_forPlot
                elif Fake and Region == 'MR_tt1l_SR':
                    ahist = self.Fake_dD_h_copy_intt1lregion_forPlot_SR
                elif Fake and Region == 'MR_tt1l_CR_ttX':
                    ahist = self.Fake_dD_h_copy_intt1lregion_forPlot_CRttX
                else:
                    ahist = atfile.Get(hist_name)

                if underflow_bin:
                    underflow = ahist.GetBinContent(0)
                    first_bin_content = ahist.GetBinContent(1)
                    new_first_bin_content = first_bin_content + underflow
                    ahist.SetBinContent(1, new_first_bin_content)
                    ahist.SetBinContent(0, 0)

                # Handling overflow
                overflow = ahist.GetBinContent(ahist.GetNbinsX() + 1)
                last_bin_content = ahist.GetBinContent(ahist.GetNbinsX())
                new_last_bin_content = last_bin_content + overflow
                ahist.SetBinContent(ahist.GetNbinsX(), new_last_bin_content)
                ahist.SetBinContent(ahist.GetNbinsX() + 1, 0)

                if nrebins>0:
                    ahist = ahist.Rebin(nrebins,hist_name+'rebinned_data',xbins)
                # ahist.SetBinContent(ahist.GetNbinsX(), ahist.GetBinContent(ahist.GetNbinsX()) + ahist.GetBinContent(ahist.GetNbinsX()+1))
                if ahist is None:
                    print("histogram %s not found in %s"%(hist_name, self.data_file_list[ifile]))
                    sys.exit(-1)

                else:
                    if final_data_hist == None:
                        final_data_hist = ahist.Clone("finaldata")
                    else:
                        final_data_hist.Add(ahist)
                print("Data: %i"%final_data_hist.Integral())

            if mode == NORMALIZED:
                print("Normalizing Data to one")
                normscale = final_data_hist.Integral()
                final_data_hist.Scale(1.0/normscale)
            
            final_data_hist.SetLineColor(ROOT.kBlack)
        
            # Legend add entry
            if (not Flip and not Fake) or (Flip and Fake and Region == 'SR' or Region == 'CR_ttX'):
                tl.AddEntry(final_data_hist, "Data", "P")
            elif Flip and Region == 'MR_tt2l':
                tl.AddEntry(final_data_hist, "Data (background subtracted)", "P")
            elif Fake and (Region == 'MR_tt1l_SR' or Region == 'MR_tt1l_CR_ttX'):
                tl.AddEntry(final_data_hist, "Data (background subtracted)", "P")

     ########################## Create TPads and elements of Pad ##########################
        if not Paper:
            if(Region == 'CR_ttX' or Region == 'MR_tt2l' or Region == 'MR_tt1l_SR' or Region == 'MR_tt1l_CR_ttX') and self.data_file_list:
                c1_top = self.ROOT.TPad("c1_top", "top", 0.01, 0.33, 0.99, 0.99)
            else:
                c1_top = self.ROOT.TPad("c1_top", "top", 0.01, 0.01, 0.99, 0.99)
            c1_top.Draw()
            c1_top.cd()
            if Region == 'SR':
                c1_top.SetBottomMargin(0.13)
            c1_top.SetRightMargin(0.1)
        
            # log y scale
            if isLogy:
                c1_top.SetLogy(isLogy)

            if mode == STACKED:
                hs.Draw(option)
            else:
                hs.Draw("nostack " + option)
            hs.GetXaxis().SetTitle(xtitle)
            hs.GetXaxis().SetNdivisions(6, 5, 0)
            hs.GetXaxis().SetTitleSize(0.05)
            hs.GetXaxis().SetTitleOffset(1.2)
            hs.GetXaxis().SetLabelSize(0.04)
                
            # Define the Max for Yaxis
            sig_max = -1
            for sighist in signal_histlist:
                if sig_max<sighist.GetMaximum():
                    sig_max = sighist.GetMaximum()

            # Set vertical range
            max1 = hs.GetMaximum()
            max2 = -1
            max3 = sig_max
            if(Region == 'CR_ttX' or Region == 'MR_tt2l' or Region == 'MR_tt1l_SR' or Region == 'MR_tt1l_CR_ttX') and self.data_file_list:
                max2 = final_data_hist.GetMaximum()
            total_max = max(max(max1,max2),max3)
            if ymin != -1111:
                hs.SetMinimum(ymin)
            if ymax != -1111:
                hs.SetMaximum(ymax)
            elif isLogy:
                hs.SetMaximum(total_max**1.3)
            else:
                hs.SetMaximum(total_max*1.3) #Y axis range

            # To plot the signal on top of background
            for sighist in signal_histlist:
                sighist.SetLineWidth(3)
                sighist.Draw("same Hist")
       
            hs.GetYaxis().SetTitle(ytitle)
            hs.GetYaxis().SetNdivisions(6, 5, 0)
            hs.GetYaxis().SetMaxDigits(3)
            hs.GetYaxis().SetLabelSize(0.04)
            hs.GetYaxis().SetTitleSize(0.04)

         ########################## Define type of plot to draw ##########################
            if not isLogy:
                    self.ROOT.TGaxis.SetExponentOffset(-0.039,0.01,"y")
            
            pt = self.ROOT.TPaveText(0.15,0.78,0.25,0.89,"NDC NB")
            pt.SetTextSize(0.05)
            pt.SetFillColor(0)
            pt.SetTextAlign(21)
            if(Region == 'SR'):
                pt.AddText('SR')
            elif(Region == 'CR_ttX'):
                pt.AddText('CR ttX')
            elif(Region == 'MR_tt2l'):
                pt.AddText('MR tt2l')
            elif(Region == 'MR_tt1l_SR'):
                pt.AddText('MR tt1l')
                pt.AddText('(SR)')
            elif(Region == 'MR_tt1l_CR_ttX'):
                pt.AddText('MR tt1l')
                pt.AddText('(CR ttX)')
            pt.SetTextColor(ROOT.kBlack)
            pt.Draw()

            if(Region == 'CR_ttX' or Region == 'MR_tt2l' or Region == 'MR_tt1l_SR' or Region == 'MR_tt1l_CR_ttX') and self.data_file_list:
                CMS_lumi.extraText = "CMS Preliminary"
                c1_top.SetMargin(0.13, 0.04, 0.02, 0.09) # left, right, bottom, top
                final_data_hist.SetMarkerStyle(self.ROOT.kFullCircle)
                final_data_hist.Draw("same err P")
                hs.GetXaxis().SetLabelSize(0)

                # Ratio plot
                if subplot == "R":
                    CMS_lumi.lumiTextSize = 0.6
                    CMS_lumi.cmsTextSize = 0.6
                    CMS_lumi.extraOverCmsTextSize = 0.9

                    hs.GetYaxis().SetTitle(ytitle)
                    hs.GetYaxis().SetMaxDigits(3)
                    hs.GetYaxis().SetTitleSize(0.05)
                    hs.GetYaxis().SetLabelSize(0.05)
                    hs.GetYaxis().SetTitleOffset(1.1)

                    hstackhist = mc_hist_sum
                    ratiohist = final_data_hist.Clone("ratiohist")
                    ratiohist.Divide(hstackhist)
                    for n in range(final_data_hist.GetNbinsX()):
                        if final_data_hist.GetBinContent(n) > 0:
                            newerror = ratiohist.GetBinContent(n)/math.sqrt(final_data_hist.GetBinContent(n))
                        else:
                            newerror = 0
                        ratiohist.SetBinError(n,newerror)
                    # print('bin ' + str(n) + ' = ' + str(ratiohist.GetBinContent(n)))
                    # ratiohist.SetMinimum(0.7)
                    # ratiohist.SetMaximum(1.2)
                    ratiohist.SetMinimum(ratiohist.GetMinimum()*0.5)
                    ratiohist.SetMaximum(ratiohist.GetMaximum()*1.2)

                    tl.Draw()
                    c1_top.Modified()
                    #CMS_lumi.CMS_lumi(c1_top, 4, 11)
                    CMS_lumi.CMS_lumi(self.ROOT, c1_top, 4, 0)
                    c1_top.cd()
                    c1_top.Update()
                    c1_top.RedrawAxis()

                    self.c1.cd()
                    c1_bottom = self.ROOT.TPad("c1_bottom", "bottom", 0.01, 0.01, 0.99, 0.33)
                    c1_bottom.Draw()
                    c1_bottom.cd()
                    c1_bottom.SetMargin(0.13, 0.04, 0.4, 0.02) # left, right, bottom, top
                    c1_bottom.SetGridx(1)
                    c1_bottom.SetGridy(1)
                    ratiohist.Draw("err")

                    ratiohist.GetXaxis().SetTitle(xtitle)
                    ratiohist.GetXaxis().SetNdivisions(6,5,0)
                    ratiohist.GetXaxis().SetTitleSize(0.1)
                    ratiohist.GetXaxis().SetTitleOffset(1.5)
                    ratiohist.GetXaxis().SetLabelSize(0.10)

                    ratiohist.GetYaxis().SetTitle("Data/Exp")
                    ratiohist.GetYaxis().SetTitleSize(0.1)
                    ratiohist.GetYaxis().SetTitleOffset(0.5)
                    ratiohist.GetYaxis().SetNdivisions(6,5,0)
                    ratiohist.GetYaxis().SetLabelSize(0.08) #Set offset between axis and axis labels
                    ratiohist.GetYaxis().SetLabelOffset(0.007)

                    c1_bottom.Modified()
                    c1_bottom.RedrawAxis()

                else:
                    CMS_lumi.lumiTextSize = 0.4
                    CMS_lumi.cmsTextSize = 0.4
                    CMS_lumi.extraOverCmsTextSize = 0.9

                    c1_top.SetMargin(0.1, 0.1, 0.1, 0.1) # left, right, bottom, top

                    hs.GetXaxis().SetTitle(xtitle)
                    hs.GetXaxis().SetNdivisions(6,5,0)
                    hs.GetXaxis().SetTitleSize(0.04)
                    hs.GetXaxis().SetTitleOffset(1.2)
                    hs.GetXaxis().SetLabelSize(0.04)
                
                    hs.GetYaxis().SetTitle(ytitle)
                    hs.GetYaxis().SetNdivisions(6,5,0)
                    hs.GetYaxis().SetMaxDigits(3)
                    hs.GetYaxis().SetTitleSize(0.04)
                    hs.GetYaxis().SetLabelSize(0.04)
                    hs.GetYaxis().SetTitleOffset(1.8)

                    if mode == NORMALIZED:
                        print(f'maximum in normalized mode: {hs.GetMaximum()}')
                        hs.SetMaximum(0.1)

                    tl.Draw()
                    c1_top.Modified()
                    CMS_lumi.CMS_lumi(self.ROOT, c1_top, 4, 0)
                    c1_top.cd()
                    c1_top.Update()
                    c1_top.RedrawAxis()
        
            else:
                CMS_lumi.extraText = "CMS Preliminary"
                CMS_lumi.lumiTextSize = 0.4
                CMS_lumi.cmsTextSize = 0.4
                CMS_lumi.extraOverCmsTextSize = 0.9

                hstackhist = mc_hist_sum
                c1_top.SetMargin(0.1, 0.1, 0.1, 0.1) # left, right, bottom, top

                hs.GetXaxis().SetTitle(xtitle)
                hs.GetXaxis().SetNdivisions(6,5,0)
                hs.GetXaxis().SetTitleSize(0.04)
                hs.GetXaxis().SetTitleOffset(1)
                hs.GetXaxis().SetLabelSize(0.04)
            
                hs.GetYaxis().SetTitle(ytitle)
                hs.GetYaxis().SetNdivisions(6,5,0)
                hs.GetYaxis().SetMaxDigits(3)
                hs.GetYaxis().SetTitleSize(0.04)
                hs.GetYaxis().SetLabelSize(0.04)
                # hs.GetYaxis().SetTitleOffset(1.8)

                if mode == NORMALIZED:
                    print(f'maximum in normalized mode: {hs.GetMaximum()}')
                    hs.SetMaximum(1)

                tl.Draw()
                c1_top.Modified()
                CMS_lumi.CMS_lumi(self.ROOT, c1_top, 4, 0)
                c1_top.cd()
                c1_top.Update()
                c1_top.RedrawAxis()

     
     ########################## Modify the stack histo and the legend for paper format ##########################

        if Paper:
            r = 0.3
            epsilon = 0.1
            tm = gStyle.GetPadTopMargin()
            self.c1.UseCurrentStyle()
            
            if(Region == 'CR_ttX' or Region == 'MR_tt2l' or Region == 'MR_tt1l_SR' or Region == 'MR_tt1l_CR_ttX') and self.data_file_list:
                c1_top_paper = self.ROOT.TPad("c1_top_paper", "top", 0, r-epsilon, 1, 1)
            else:
                c1_top_paper = self.ROOT.TPad("c1_top_paper", "top", 0, 0, 1, 1)
            c1_top_paper.SetBottomMargin(epsilon)
            c1_top_paper.SetTopMargin(1.5*tm)
            c1_top_paper.SetRightMargin(0.04)
            self.c1.cd()
            c1_top_paper.Draw()
            c1_top_paper.cd()

            if (Region == 'SR'):
                tl_paper = self.ROOT.TLegend(0.625, 0.55, 0.85, 0.9)
                tl_paper.SetTextSize(0.04)
            elif (not Flip and not Fake) or (Flip and Fake and Region == 'CR_ttX'):
                tl_paper = self.ROOT.TLegend(0.625, 0.46, 0.85, 0.9)
                tl_paper.SetTextSize(0.05)
            elif (Flip and Region == 'MR_tt2l') or (Fake and (Region == 'MR_tt1l_SR' or Region == 'MR_tt1l_CR_ttX')):
                tl_paper = self.ROOT.TLegend(0.525, 0.46, 0.85, 0.9)
                tl_paper.SetTextSize(0.05)
            tl_paper.SetBorderSize(0)

            # not considering the data in SR (for the moment)
            if(Region == 'CR_ttX' or Region == 'MR_tt2l' or Region == 'MR_tt1l_SR' or Region == 'MR_tt1l_CR_ttX') and self.data_file_list:
                # Legend add entry
                if (not Flip and not Fake) or (Flip and Fake and Region == 'SR' or Region == 'CR_ttX'):
                    tl_paper.AddEntry(final_data_hist, "Data", "EP")
                elif Flip and Region == 'MR_tt2l':
                    tl_paper.AddEntry(final_data_hist, "#splitline{Data (background}{subtracted)}", "EP")
                elif Fake and (Region == 'MR_tt1l_SR' or Region == 'MR_tt1l_CR_ttX'):
                    tl_paper.AddEntry(final_data_hist, "#splitline{Data (background}{subtracted)}", "EP")
            
            # Put MC labels in the correct order
            for label in labels_in_order:
                ahist = hist_group[label]
                if 'Signal' in label:
                    tl_paper.AddEntry(ahist, 'm_{T\'} = '+mass+' GeV', "F")
                else:
                    if 't#bar{t}2l' in label and Flip and Region == 'MR_tt2l':
                        tl_paper.AddEntry(ahist, 'MC t#bar{t}2l', "F")
                    elif 'Non prompt' in label and Fake and (Region == 'MR_tt1l_SR' or Region == 'MR_tt1l_CR_ttX'):
                        tl_paper.AddEntry(ahist, 'MC Non prompt', "F")
                    else:
                        tl_paper.AddEntry(ahist, label, "F")

            g_errors.GetXaxis().SetLimits(300,1500)
            g_errors.SetMinimum(0)
            g_errors.GetYaxis().SetTitleOffset(0.95)
            g_errors.Draw("2AP")
            g_errors.SetLineColor(1)
            g_errors.SetLineWidth(1)
            g_errors.SetFillColor(1)
            g_errors.SetFillStyle(3005)
            g_errors.SetMarkerSize(0)
            g_errors.SetMarkerStyle(1)

            if mode == STACKED:
                hs.Draw(option + "SAME")
            else:
                hs.Draw("nostack SAME " + option)
            hs.GetHistogram().GetXaxis().SetTickLength(0)
            ROOT.gPad.SetTicks(1, 0)
                
            # Define the Max for Yaxis
            sig_max = -1
            for sighist in signal_histlist:
                if sig_max<sighist.GetMaximum():
                    sig_max = sighist.GetMaximum()

            # Set vertical range
            max1 = hs.GetMaximum()
            max2 = -1
            max3 = sig_max
            if(Region == 'CR_ttX' or Region == 'MR_tt2l' or Region == 'MR_tt1l_SR' or Region == 'MR_tt1l_CR_ttX') and self.data_file_list:
                max2 = final_data_hist.GetMaximum()
            total_max = max(max(max1,max2),max3)
            if ymin != -1111:
                g_errors.SetMinimum(ymin)
            if ymax != -1111:
                g_errors.SetMaximum(ymax)
            elif isLogy:
                g_errors.SetMaximum(total_max**1.3)
            else:
                g_errors.SetMaximum(total_max*1.3) #Y axis range

            # To plot the signal on top of background
            # for sighist in signal_histlist:
            #     sighist.SetLineWidth(3)
            #     sighist.Draw("SAME HIST")

            if not isLogy:
                self.ROOT.TGaxis.SetExponentOffset(-0.039,0.01,"y")

            pt_paper = self.ROOT.TPaveText(0.25,0.78,0.25,0.89, 'NDC NB')
            pt_paper.SetTextSize(0.05)
            pt_paper.SetFillColor(0)
            pt_paper.SetTextAlign(21)
            if(Region == 'SR'):
                pt_paper.AddText('SR')
            elif(Region == 'CR_ttX'):
                pt_paper.AddText('CR ttX')
            elif(Region == 'MR_tt2l'):
                pt_paper.AddText('MR tt2l')
            elif(Region == 'MR_tt1l_SR'):
                pt_paper.AddText('MR tt1l')
                pt_paper.AddText('(SR)')
            elif(Region == 'MR_tt1l_CR_ttX'):
                pt_paper.AddText('MR tt1l')
                pt_paper.AddText('(CR ttX)')
            pt_paper.SetTextColor(ROOT.kBlack)
            pt_paper.Draw("SAME")

            if(Region == 'CR_ttX' or Region == 'MR_tt2l' or Region == 'MR_tt1l_SR' or Region == 'MR_tt1l_CR_ttX') and self.data_file_list:
                final_data_hist.SetMarkerStyle(self.ROOT.kFullCircle)
                final_data_hist.Draw("SAME ERR P")
                g_errors.GetXaxis().SetLabelSize(0)
                g_errors.GetXaxis().SetTitleSize(0)

                if(Year=='2016'):
                    tdrstyle.cmsPrel(36300., 13.,simOnly=False,thisIsPrelim=True)
                elif(Year=='2017'):
                    tdrstyle.cmsPrel(41500., 13.,simOnly=False,thisIsPrelim=True)
                elif(Year=='2018'):
                    tdrstyle.cmsPrel(59800., 13.,simOnly=False,thisIsPrelim=True)
                elif(Year=='Run2'):
                    tdrstyle.cmsPrel(137600., 13.,simOnly=False,thisIsPrelim=True)

                # Ratio plot
                if subplot == "R":
                    g_errors.GetYaxis().SetTitle("Events")
                    g_errors.GetYaxis().SetMaxDigits(4)
                    g_errors.GetYaxis().SetTitleSize(0.06)
                    g_errors.GetYaxis().SetLabelSize(0.04)
                    g_errors.GetYaxis().SetTitleOffset(0.95)
                    g_errors.GetYaxis().CenterTitle()

                    tl_paper.Draw()
                    c1_top_paper.Modified()
                    c1_top_paper.cd()
                    c1_top_paper.Update()
                    c1_top_paper.RedrawAxis()

                    self.c1.cd()
                    c1_bottom_paper = self.ROOT.TPad("c1_bottom_paper", "bottom", 0, 0, 1, r*(1-epsilon))
                    c1_bottom_paper.SetTopMargin(0)
                    c1_bottom_paper.SetBottomMargin(0.4)
                    c1_bottom_paper.SetRightMargin(0.04)
                    c1_bottom_paper.SetFillStyle(0)
                    c1_bottom_paper.Draw()
                    c1_bottom_paper.cd()

                    hstackhist = mc_hist_sum
                    ratiohist_paper = ROOT.TH1D("ratiohist_paper", "ratiohist", 8, 300, 1500)
                    for n in range(2, final_data_hist.GetNbinsX()+1):
                        if final_data_hist.GetBinContent(n) > 0:
                            newratio = final_data_hist.GetBinContent(n)/hstackhist.GetBinContent(n)
                            newerror = newratio/math.sqrt(final_data_hist.GetBinContent(n))
                        else:
                            newratio = 0
                            newerror = 0
                        ratiohist_paper.SetBinContent(n-2,newratio)
                        ratiohist_paper.SetBinError(n-2,newerror)

                    g_errors_ratio.GetXaxis().SetLimits(300,1500)
                    g_errors_ratio.GetXaxis().SetBinLabel(1, "300")
                    g_errors_ratio.GetXaxis().SetBinLabel(13, "450")
                    g_errors_ratio.GetXaxis().SetBinLabel(25, "600")
                    g_errors_ratio.GetXaxis().SetBinLabel(38, "750")
                    g_errors_ratio.GetXaxis().SetBinLabel(50, "900")
                    g_errors_ratio.GetXaxis().SetBinLabel(63, "1050")
                    g_errors_ratio.GetXaxis().SetBinLabel(75, "1200")
                    g_errors_ratio.GetXaxis().SetBinLabel(88, "1350")
                    g_errors_ratio.GetXaxis().SetBinLabel(100, "1500")
                    g_errors_ratio.Draw("2A SAME")
                    ratiohist_paper.Draw("ERR SAME")

                    g_errors_ratio.SetLineColor(1)
                    g_errors_ratio.SetLineWidth(1)
                    g_errors_ratio.SetFillColor(1)
                    g_errors_ratio.SetFillStyle(3005)
                    g_errors_ratio.SetMarkerSize(0)
                    g_errors_ratio.SetMarkerStyle(1)
                    g_errors_ratio.SetMinimum(ratiohist_paper.GetMinimum()*0.5)
                    g_errors_ratio.SetMaximum(ratiohist_paper.GetMaximum()*1.2)
                    if(Region == 'MR_tt2l'):
                        g_errors_ratio.SetMinimum(0.8)
                        g_errors_ratio.SetMaximum(1.2)

                    # g_errors_ratio.GetXaxis().SetNdivisions(8, False)
                    g_errors_ratio.GetXaxis().SetLabelSize(0.13)
                    g_errors_ratio.GetXaxis().SetTickLength(0)
                    g_errors_ratio.GetXaxis().SetTitle(xtitle)
                    g_errors_ratio.GetXaxis().SetTitleSize(0.17)
                    g_errors_ratio.GetXaxis().SetLabelOffset(0.01)
                    g_errors_ratio.GetXaxis().SetTitleOffset(1.08)
                    g_errors_ratio.GetXaxis().CenterTitle()

                    g_errors_ratio.GetYaxis().SetTitle("Data/MC")
                    g_errors_ratio.GetYaxis().SetMaxDigits(4)
                    g_errors_ratio.GetYaxis().SetLabelSize(0.09)
                    g_errors_ratio.GetYaxis().SetTitleSize(0.13)
                    g_errors_ratio.GetYaxis().SetTitleOffset(0.4)
                    g_errors_ratio.GetYaxis().CenterTitle()
                    g_errors_ratio.SetMarkerStyle(self.ROOT.kFullCircle)
                    g_errors_ratio.GetXaxis().SetTickLength(0)
                    ROOT.gPad.SetTicks(1, 0)

                    line = ROOT.TLine(300, 1, 1500, 1)
                    line.SetLineColor(ROOT.kGray)
                    line.SetLineStyle(1)
                    line.Draw("SAME")

                    c1_bottom_paper.Modified()
                    c1_bottom_paper.RedrawAxis()
        
            else:
                if(Year=='2016'):
                    tdrstyle.cmsPrel(36300., 13.,simOnly=False,thisIsPrelim=True,textSize=0.05)
                elif(Year=='2017'):
                    tdrstyle.cmsPrel(41500., 13.,simOnly=False,thisIsPrelim=True,textSize=0.05)
                elif(Year=='2018'):
                    tdrstyle.cmsPrel(59800., 13.,simOnly=False,thisIsPrelim=True,textSize=0.05)
                elif(Year=='Run2'):
                    tdrstyle.cmsPrel(137600., 13.,simOnly=False,thisIsPrelim=True,textSize=0.05)
                
                g_errors.GetXaxis().SetNdivisions(8, False)
                g_errors.GetXaxis().SetLabelSize(0.035)
                g_errors.GetXaxis().SetTickLength(0)
                g_errors.GetXaxis().SetTitle(xtitle)
                g_errors.GetXaxis().SetTitleSize(0.045)
                g_errors.GetXaxis().SetTitleOffset(1)
                g_errors.GetXaxis().CenterTitle()
            
                g_errors.GetYaxis().SetTitle("Events")
                g_errors.GetYaxis().SetMaxDigits(4)
                g_errors.GetYaxis().SetTitleSize(0.05)
                g_errors.GetYaxis().SetLabelSize(0.04)
                g_errors.GetYaxis().SetTitleOffset(1.2)
                g_errors.GetYaxis().CenterTitle()

                if mode == NORMALIZED:
                    print(f'maximum in normalized mode: {g_errors.GetMaximum()}')
                    g_errors.SetMaximum(1)

                tl_paper.Draw()
                c1_top_paper.Modified()
                c1_top_paper.cd()
                c1_top_paper.Update()
                c1_top_paper.RedrawAxis()

         ########################## Save plot ##########################

        outFormat = '.pdf'
        self.c1.SetCanvasSize(1200, 1000)
        if Paper:
            self.c1.SetCanvasSize(800, 800)
            # self.c1.Range(-138.0111,-9.957644,1736.989,89.61879)

        frame = self.c1.GetFrame()
        if Region == 'SR':
            if not Paper:
                frame = c1_top.GetFrame()
            if Paper:
                frame = c1_top_paper.GetFrame()
            if mode == NORMALIZED:
                frame = self.c1.GetFrame()
        frame.Draw()
        self.c1.Update()
        self.c1.cd()

        if underflow_bin: CMS_lumi.outputFileName += '_underflow_'

        if subplot == "R":
            path += "plot_ratio_"+str(self.integer_lumi)
            if not os.path.isdir(path):
                os.mkdir(path)
        else:
            path += "plot_"+str(self.integer_lumi)
            if not os.path.isdir(path):
                os.mkdir(path)

        if Region == 'SR':
            if not Paper:
                if(isLogy):
                    c1_top.SaveAs(path+"/"+hist_name+"_logy" + CMS_lumi.outputFileName + outFormat)
                    # self.c1.Print(path + '/' + "plots" + outFormat)
                    imageList.append(path+"/"+hist_name+"_logy" + CMS_lumi.outputFileName + outFormat)
                else:
                    c1_top.SaveAs(path+"/"+hist_name+"_nology" + CMS_lumi.outputFileName + outFormat)
                    # self.c1.Print(path + '/' + "plots" + outFormat)
                    imageList.append(path+"/"+hist_name+"_nology" + CMS_lumi.outputFileName + outFormat)
            if Paper:
                if(isLogy):
                    c1_top_paper.SaveAs(path+"/"+hist_name+"_logy" + CMS_lumi.outputFileName + outFormat)
                    # self.c1.Print(path + '/' + "plots" + outFormat)
                    imageList.append(path+"/"+hist_name+"_logy" + CMS_lumi.outputFileName + outFormat)
                else:
                    c1_top_paper.SaveAs(path+"/"+hist_name+"_nology" + CMS_lumi.outputFileName + outFormat)
                    # self.c1.Print(path + '/' + "plots" + outFormat)
                    imageList.append(path+"/"+hist_name+"_nology" + CMS_lumi.outputFileName + outFormat)
        else:
            if(isLogy):
                self.c1.SaveAs(path+"/"+hist_name+"_logy" + CMS_lumi.outputFileName + outFormat)
                # self.c1.Print(path + '/' + "plots" + outFormat)
                imageList.append(path+"/"+hist_name+"_logy" + CMS_lumi.outputFileName + outFormat)
            else:
                self.c1.SaveAs(path+"/"+hist_name+"_nology" + CMS_lumi.outputFileName + outFormat)
                # self.c1.Print(path + '/' + "plots" + outFormat)
                imageList.append(path+"/"+hist_name+"_nology" + CMS_lumi.outputFileName + outFormat)
        self.c1.Clear()
        CMS_lumi.extraText = ""
        pass