import os
import sys
import ROOT
from ROOT import TFile, TCanvas, TPad, TH1D, TLegend, TPaveText
from ROOT import gStyle
import CMS_lumi
#import tdrstyle
#tdrStyle = tdrstyle.setTDRStyle(ROOT, tdrstyle)
## Set tdrStyle
#ROOT.gROOT.SetStyle("tdrStyle")

ROOT.gROOT.SetBatch(True)

def create_ratio_histogram(h_nominal, h_systematic):
    """Create a ratio histogram of systematic / nominal."""
    h_ratio = h_systematic.Clone(h_systematic.GetName() + "_ratio")
    h_ratio.Divide(h_nominal)
    return h_ratio

def calculate_ratio_range(h_ratio_up, h_ratio_down):
    """Calculate Y axis range for the ratio plot based on max deviation."""
    max_deviation = max(max(abs(h_ratio_up.GetBinContent(i) - 1) for i in range(1, h_ratio_up.GetNbinsX() + 1)),
                        max(abs(h_ratio_down.GetBinContent(i) - 1) for i in range(1, h_ratio_down.GetNbinsX() + 1)))
    upper = 1 + max_deviation*1.2
    lower = 1 - max_deviation*1.2
    return lower, upper

def add_integral_and_entries_box(h_nominal, h_up, h_down, systematic):
    """Add a text box with the integrals and entries of the histograms."""
    # Calculate the integral and number of entries for each histogram
    nominal_integral = h_nominal.Integral(-1,-1)
    #nominal_integral = h_nominal.Integral(-1,-1, -1, -1)
    nominal_entries = h_nominal.GetEntries()

    up_integral = h_up.Integral(-1,-1)
    #up_integral = h_up.Integral(-1,-1, -1, -1)
    up_entries = h_up.GetEntries()

    down_integral = h_down.Integral(-1,-1)
    #down_integral = h_down.Integral(-1,-1, -1, -1)
    down_entries = h_down.GetEntries()

    # Create a TPaveText box to display the integrals and entries
    pave_text = TPaveText(0.6, 0.4, 0.9, 0.7, "NDC")
    pave_text.SetBorderSize(0)
    pave_text.SetFillColor(0)
    pave_text.SetTextAlign(12)
    pave_text.SetTextFont(42)

    pave_text.AddText(f"Nominal (Entries/Integral): {nominal_entries:.0f} / {nominal_integral:.2f}")
    pave_text.AddText(f"{systematic} Up (Entries/Integral): {up_entries:.0f} / {up_integral:.2f}")
    pave_text.AddText(f"{systematic} Down (Entries/Integral): {down_entries:.0f} / {down_integral:.2f}")
    pave_text.SetTextSize(0.02)

    return pave_text

def plot_histograms(h_nominal, h_up, h_down, process, systematic, outputdir, variable, single):
    """Plot nominal, up, and down histograms and their ratios with modifications."""
    if not os.path.exists(outputdir):
        os.makedirs(outputdir)

    c = TCanvas(f"c_{process}_{systematic}", f"{process} {systematic}", 1200, 1000)
    gStyle.SetOptStat(0)

    pad1 = TPad("pad1", "pad1", 0.01, 0.33, 0.99, 0.99)
    #pad1.SetBottomMargin(0.0)
    pad1.SetMargin(0.13, 0.04, 0.02, 0.09) # left, right, bottom, top
    pad1.Draw()
    pad1.cd()

    h_nominal.SetLineColor(ROOT.kBlack)
    h_nominal.SetLineWidth(2)  # Increase line width for better visibility
    h_nominal.SetTitle(process)
    h_nominal.GetXaxis().SetLabelOffset(999)
    h_nominal.GetYaxis().SetTitle("Events")
    h_nominal.GetXaxis().SetTitleSize(0.04)  # Adjust axis label size
    h_nominal.GetXaxis().SetTitleOffset(1.2)  # Adjust axis label position
    h_nominal.GetYaxis().SetTitleSize(0.04)  # Adjust axis label size
    h_nominal.GetYaxis().SetTitleOffset(1.4)  # Adjust axis label position


    h_nominal.Draw("hist")
    h_up.SetLineColor(ROOT.kRed)
    h_up.SetLineWidth(2)  # Increase line width for better visibility
    h_up.Draw("histsame")
    h_down.SetLineColor(ROOT.kBlue)
    h_down.SetLineWidth(2)  # Increase line width for better visibility
    h_down.Draw("histsame")
    h_nominal.GetYaxis().SetRangeUser(0.0, 1.02*max(h_nominal.GetMaximum(), h_down.GetMaximum(), h_up.GetMaximum()))
    legend = TLegend(0.6, 0.7, 0.9, 0.9)
    legend.SetBorderSize(0)  # Remove legend border
    legend.AddEntry(h_nominal, "Nominal", "l")
    legend.AddEntry(h_up, f"{systematic} Up", "l")
    legend.AddEntry(h_down, f"{systematic} Down", "l")
    legend.Draw()
    # Add the integral and entries box to the plot
    pave_text = add_integral_and_entries_box(h_nominal, h_up, h_down, systematic)
    pave_text.Draw()


    pad1.Update()
    
    c.cd()
    pad2 = TPad("pad2", "pad2", 0.01, 0.01, 0.99, 0.33)
    #pad2.SetTopMargin(0)
    #pad2.SetBottomMargin(0.3)
    pad2.SetMargin(0.13, 0.04, 0.4, 0.02) # left, right, bottom, top
    pad2.Draw()
    pad2.cd()
    pad2.SetGridy()  # Add grid to the ratio plot

    h_ratio_up = create_ratio_histogram(h_nominal, h_up)
    h_ratio_down = create_ratio_histogram(h_nominal, h_down)
    lower, upper = calculate_ratio_range(h_ratio_up, h_ratio_down)

    h_ratio_up.SetLineWidth(2)
    h_ratio_up.SetTitle("")
    h_ratio_up.SetLineColor(ROOT.kRed)

    xaxis = h_ratio_up.GetXaxis()
    if single:
        if "0_0_0_0" in variable:
            xaxis.SetTitle(variable.split("_cut")[0]+" in Signal Region")
        elif "0_0_0_1" in variable:
            xaxis.SetTitle(variable.split("_cut")[0]+" in ttbar Control Region")
        elif "0_0_0_2" in variable:
            xaxis.SetTitle(variable.split("_cut")[0]+" in WJets Control Region")
    else:
        xaxis.SetTitle(variable)
    xaxis.SetNdivisions(6,5,0)
    xaxis.SetTitleSize(0.1)
    xaxis.SetTitleOffset(1.5)
    xaxis.SetLabelSize(0.10)
    
    yaxis = h_ratio_up.GetYaxis()
    yaxis.SetTitle("Syst/Nominal")
    yaxis.SetTitleSize(0.1)
    yaxis.SetTitleOffset(0.5)
    yaxis.SetNdivisions(6,5,0)
    yaxis.SetLabelSize(0.08) #Set offset between axis and axis labels
    yaxis.SetLabelOffset(0.007)

    #h_ratio_up.SetTitle("")
    #h_ratio_up.GetXaxis().SetTitle(variable)
    #h_ratio_up.GetYaxis().SetTitle("Ratio")
    #h_ratio_up.GetXaxis().SetTitleSize(0.04)  # Adjust axis label size
    #h_ratio_up.GetXaxis().SetTitleOffset(1.2)  # Adjust axis label position
    #h_ratio_up.GetYaxis().SetTitleSize(0.04)  # Adjust axis label size
    #h_ratio_up.GetYaxis().SetTitleOffset(1.4)  # Adjust axis label position
    h_ratio_up.GetYaxis().SetRangeUser(lower, upper)
    h_ratio_up.Draw("hist")

    h_ratio_down.SetLineColor(ROOT.kBlue)
    h_ratio_down.SetLineWidth(2)
    h_ratio_down.Draw("histsame")
    pad2.Update()
    if single:
        output_path = os.path.join(outputdir, f"{variable}_{systematic}.png")
    else:
        output_path = os.path.join(outputdir, f"{process}_{systematic}.png")
    c.SaveAs(output_path)

def process_systematics(file_name, variable, processes, systematics, outputdir, single):
    f = TFile.Open(file_name, "READ")
    f.ls()
    for process in processes:
        h_nominal = f.Get(f"{process}")
        #h_nominal = f.Get(f"{process}_cut_0_0_0_2")
        if not h_nominal:
            print(f"Nominal histogram for {process} not found.")
            continue

        for systematic in systematics:
            """
            if not single:
                var = variable
                cut = ''
                #h_up = f.Get(f"{process}{variable}_{systematic}Up")
                #h_down = f.Get(f"{process}{variable}_{systematic}Down")
            else:
                var = variable.split("_cut")[0]
                cut = "_cut"+variable.split("_cut")[1]
            print(var)
            print(cut)
            """
            h_up = f.Get(f"{process}_{systematic}Up")
            h_down = f.Get(f"{process}_{systematic}Down")
            #h_up = f.Get(f"{process}_{systematic}Up_cut_0_0_0_2")#phi_star_new_SR_BDT_output_syst_elec_recoDown_cut_0_0_0_2
            #h_down = f.Get(f"{process}_{systematic}Down_cut_0_0_0_2")
                
            if not h_up or not h_down:
                print(f"Systematic histograms for {process}_{systematic}Up or {process}_{systematic}Down  not found.")
                continue

            plot_histograms(h_nominal, h_up, h_down, process, systematic, outputdir, variable, single)

def main():
    year = sys.argv[5]
    #file_name = "/eos/lyoeos.in2p3.fr/grid/cms/store/user/apurohit/SingleTop_Analysis/results/{year}/2024-03-11/muChannel_With_MediumBTag_nbTasksall/normalized/Wboson_transversMass_rootfile_forCombine.root"
    #file_name = "/gridgroup/cms/apurohit/SingleTop_Analysis/Combine_Analysis/CMSSW_10_2_13/src/20{year}/7th_June_WithsmUnc/phi_star_new_SR_BDT_output_eft_rootfile_forCombine.root"
    #file_name = "/eos/lyoeos.in2p3.fr/grid/cms/store/user/apurohit/SingleTop_Analysis/results/{year}/2024-06-11/muChannel_SR_QCDDD_CosthetaStarY_New_nbTasksall/qcddd/normalized/cosTheta_starY_new_rootfile_forCombine.root"
    #file_name = "/gridgroup/cms/apurohit/SingleTop_Analysis/results/{year}/2024-06-28/muChannel_QCDDD_AllRegion_allVars_nbTasksall/qcddd/normalized/cosTheta_starY_new_rootfile_forCombine.root"
    file_name = sys.argv[1]
    #processes = ["signal", "qcd_mu", "drellyan", "ttbar", "wjets", "diboson"]
    #processes = ["sm", "vjets", "ttbar", "singletop", "diboson"]
    #processes = ["signal", "vjets", "ttbar", "singletop", "diboson"]
    #processes = ["phi_star_new_SR_BDT_output"]
    #processes = ["phi_star_new", "lep_pt", "cosTheta_starY_new", "Wboson_transversMass_fixedlep_pt_45", "MediumBJet_leading_pt"]
    #processes = ["sm"]
    #processes = ["Wboson_transversMass_fixedlep_pt_45_SR_BDT_output_bin_1_phi_star_new_bin_1", "SR_BDT_output"]
    #processes = ["SR_BDT_output", "MET_phi_corr",           "Wboson_phi",  "Wboson_rapidity",     "cosTheta_starZ_new", "cosTheta_starY_new", "phi_star_new", "lep_phi",  "specJet_eta",  "specJet_pt",  "top_mass",  "top_pt", "MediumBJet_leading_pt",  "Wboson_pt",   "cosTheta_starX_new",  "lep_eta", "lep_pt", "specJet_phi",  "top_eta", "top_phi"]
    #processes = [f"Wboson_transversMass_fixedlep_pt_45_bin_{i}_{j}" for i in range(1,4,1) for j in range(1,6,1)]
    #processes = [f"Wboson_transversMass_fixedlep_pt_45_SR_BDT_output_bin_{i}_phi_star_new_bin_{j}" for i in range(1,4,1) for j in range(1,6,1)]
    #processes = ["Wboson_transversMass_fixedlep_pt_45"]
    processes = ["sm", "sm_lin_quad_ctwi", "quad_ctwi", f"qcd_20{year}", "vjets", "ttbar", "singletop", "diboson"]

    #systematics = ["syst_muon_id", "syst_muon_iso", "syst_elec_id", "syst_elec_reco"]
    systematics = ["syst_elec_id", f"stat_muon_id_20{year}", "syst_muon_id", "syst_pu", f"stat_muon_iso_20{year}", f"stat_muon_hlt_20{year}", "syst_elec_reco", "syst_b_correlated", "syst_muon_hlt", "syst_muon_iso", f"stat_muon_reco_20{year}", f"syst_b_uncorrelated_20{year}", "syst_l_correlated", "syst_muon_reco", f"syst_l_uncorrelated_20{year}", "syst_prefiring", f"Regrouped_Absolute_20{year}", "Regrouped_Absolute", f"Regrouped_BBEC1_20{year}", "Regrouped_BBEC1", f"Regrouped_EC2_20{year}", "Regrouped_EC2", "Regrouped_FlavorQCD", f"Regrouped_HF_20{year}", "Regrouped_HF", f"Regrouped_RelativeSample_20{year}", "isr", "fsr", "syst_pdfas", "syst_qcdscale", "syst_toppt", f"syst_QCDEra20{year}", "syst_LO_NLO_Diff", "JER"]
    #outputdir = "/gridgroup/cms/apurohit/SingleTop_Analysis/Combine_Analysis/CMSSW_10_2_13/src/20{year}/{year}th_June_WithsmUnc/plots_syst/"
    outputdir = sys.argv[2]+sys.argv[3]
    if not os.path.exists(outputdir):
        os.makedirs(outputdir)
        print(outputdir+" is created")
    else:
        print(outputdir+" already exists.")
    variable = sys.argv[3]

    if 'single' in sys.argv[4]:
        processes = [""]

        process_systematics(file_name, variable, processes, systematics, outputdir, 1)
    else:
        process_systematics(file_name, variable, processes, systematics, outputdir, 0)

if __name__ == "__main__":
    main()