#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import ROOT
import os
import shutil
import stackhists
import CMS_lumi
import tdrstyle
import argparse
import subprocess

ROOT.TH1D.AddDirectory(False)

def get_prep_stack_hist(year, initial_tdrStyle, rlumi):
    """Prepare necessary variables with given year."""
    lumi_values = {
        "2016preVFP": "19.5 fb^{-1}",
        "2016postVFP": "16.8 fb^{-1}",
        "2016": "36.3 fb^{-1}",
        "2017": "41.5 fb^{-1}",
        "2018": "59.8 fb^{-1}",
        "Run2": "137.6 fb^{-1}"
    }

    lumi_fb = {
        "2016preVFP": 19.5,
        "2016postVFP": 16.8,
        "2016": 36.3,
        "2017": 41.5,
        "2018": 59.8,
        "Run2": 137.6
    }

    runs = []
    s = None

    if year in lumi_values:
        CMS_lumi.lumi_13TeV = lumi_values[year]
        s = stackhists.StackHists(ROOT, initial_tdrStyle, lumi_fb[year])
        runs = [year]

    if year != "Run2":
        print(f"Year {year}")
    else:
        print("Run2")

    return s, CMS_lumi.lumi_13TeV, rlumi, runs

def parse_arguments():
    """Parse command line arguments."""
    parser = argparse.ArgumentParser(description="Your script description here")
    parser.add_argument('--year', '-Y', type=str, required=True, help='Year parameter')
    parser.add_argument('--inputfiles', '-I', type=str, required=True, help='Input files parameter')
    parser.add_argument('--ratio', '-Ra', action='store_true', help='Enable ratio plot')
    parser.add_argument('--mass', '-M', type=str, help='Signal mass point value')
    parser.add_argument('--data', '-D', action='store_true', help='Data included')
    parser.add_argument('--flipdd', '-Fl', action='store_true', help='Data-driven estimation method for charge misidentified leptons')
    parser.add_argument('--fakedd', '-Fa', action='store_true', help='Data-driven estimation method for fake leptons')
    parser.add_argument('--paper', '-P', action='store_true', help='Paper format')

    return parser.parse_args()

def list_folder(inputfiles):
    listFolder=[]

    for dir in os.listdir(inputfiles):
        #Search in inputfiles/plots
        if dir.startswith('plots'):
            fullPath = os.path.join(inputfiles,dir)
            for file in os.listdir(fullPath):
                filepath=os.path.join(fullPath,file)
                if os.path.isdir(filepath):
                    listFolder.append(filepath)
                                    
    return listFolder

def main():
    args = parse_arguments()

    # Additional setup based on arguments
    initial_tdrStyle = ROOT.TStyle("tdrStyle", "Style for P-TDR")
    tdrstyle.setTDRStyle(ROOT, initial_tdrStyle)

    rlumi = {"2016preVFP": 1., "2016postVFP": 1., "2016": 1., "2017": 1., "2018": 1., "Run2": 1.}
    s, CMS_lumi.lumi_13TeV, rlumi, runs = get_prep_stack_hist(args.year, initial_tdrStyle, rlumi)

    year = args.year
    inputfiles = args.inputfiles
    ratio = args.ratio
    mass = args.mass
    data = args.data
    flipdd = args.flipdd
    fakedd = args.fakedd
    paper = args.paper

    # Set up CMS lumi and style options
    CMS_lumi.extraText = ''
    CMS_lumi.cmsText = ''
    s.setupStyle(alpha=1)

    # Define histogram color coding
    SignalColor = 0
    tt2lColor = 1
    tt1lColor = 2
    tt0lColor = 3
    SingletopColor = 4
    ttXColor = 5
    MultibosonsColor = 6
    WJetsColor = 7
    DYColor = 8
    DataColor = 999

    # Scale factors
    MC_SF = 1
    dataDriven_SF = 1

    s.sum_of_weights_hist_name = 'genweight_nocut;1' #TO BE CHANGED
    signal = 'Signal' + mass
    NameSignal = ["Signal600","Signal625","Signal650","Signal675","Signal700","Signal800","Signal900","Signal1000","Signal1100","Signal1200"]
    CrosssectionSignal = [176.4, 148.9, 121.3, 105.0, 88.6, 45.9, 25.1, 14.5, 8.67, 5.36]
    index = NameSignal.index(signal)
    crosssection = CrosssectionSignal[index]

    if flipdd:
        if year == "Run2":
            s.tt2lregion_data_file = inputfiles + 'Data_Run2.root'
        else:
            s.tt2lregion_data_file = inputfiles + 'Data_UL' + year + '.root'
        s.histName_forFlipFit_SR_mumu = 'Tprime_transverse_mass_First_cut_0_0_0_' + str(index) + '_0_1'
        s.histName_forFlipFit_CRttX_mumu = 'Tprime_transverse_mass_First_cut_0_0_0_' + str(index) + '_1_1'
        s.histName_forFlipDD_mumu = 'Tprime_transverse_mass_First_cut_0_0_1_' + str(index) + '_0'
        s.histName_forFlip_dD_SR_mumu = 'flip_evWeight_cut_0_0_0_' + str(index) + '_0_1'
        s.histName_forFlip_dD_SR_mumu_systshape_Up = 'flip_syst_mr_flipUp_cut_0_0_0_' + str(index) + '_0_1'
        s.histName_forFlip_dD_SR_mumu_systshape_Down = 'flip_syst_mr_flipDown_cut_0_0_0_' + str(index) + '_0_1'
        s.histName_forFlip_dD_SR_mumu_systmc_Up = 'flip_syst_tr_flipUp_cut_0_0_0_' + str(index) + '_0_1'
        s.histName_forFlip_dD_SR_mumu_systmc_Down = 'flip_syst_tr_flipDown_cut_0_0_0_' + str(index) + '_0_1'
        s.histName_forFlip_dD_CRttX_mumu = 'flip_evWeight_cut_0_0_0_' + str(index) + '_1_1'
        s.histName_forFlip_dD_CRttX_mumu_systshape_Up = 'flip_syst_mr_flipUp_cut_0_0_0_' + str(index) + '_1_1'
        s.histName_forFlip_dD_CRttX_mumu_systshape_Down = 'flip_syst_mr_flipDown_cut_0_0_0_' + str(index) + '_1_1'
        s.histName_forFlip_dD_CRttX_mumu_systmc_Up = 'flip_syst_tr_flipUp_cut_0_0_0_' + str(index) + '_1_1'
        s.histName_forFlip_dD_CRttX_mumu_systmc_Down = 'flip_syst_tr_flipDown_cut_0_0_0_' + str(index) + '_1_1'
        s.histName_forFlipFit_SR_emu = 'Tprime_transverse_mass_First_cut_1_0_0_' + str(index) + '_0_1'
        s.histName_forFlipFit_CRttX_emu = 'Tprime_transverse_mass_First_cut_1_0_0_' + str(index) + '_1_1'
        s.histName_forFlipDD_emu = 'Tprime_transverse_mass_First_cut_1_0_1_' + str(index) + '_0'
        s.histName_forFlip_dD_SR_emu = 'flip_evWeight_cut_1_0_0_' + str(index) + '_0_1'
        s.histName_forFlip_dD_SR_emu_systshape_Up = 'flip_syst_mr_flipUp_cut_1_0_0_' + str(index) + '_0_1'
        s.histName_forFlip_dD_SR_emu_systshape_Down = 'flip_syst_mr_flipDown_cut_1_0_0_' + str(index) + '_0_1'
        s.histName_forFlip_dD_SR_emu_systmc_Up = 'flip_syst_tr_flipUp_cut_1_0_0_' + str(index) + '_0_1'
        s.histName_forFlip_dD_SR_emu_systmc_Down = 'flip_syst_tr_flipDown_cut_1_0_0_' + str(index) + '_0_1'
        s.histName_forFlip_dD_CRttX_emu = 'flip_evWeight_cut_1_0_0_' + str(index) + '_1_1'
        s.histName_forFlip_dD_CRttX_emu_systshape_Up = 'flip_syst_mr_flipUp_cut_1_0_0_' + str(index) + '_1_1'
        s.histName_forFlip_dD_CRttX_emu_systshape_Down = 'flip_syst_mr_flipDown_cut_1_0_0_' + str(index) + '_1_1'
        s.histName_forFlip_dD_CRttX_emu_systmc_Up = 'flip_syst_tr_flipUp_cut_1_0_0_' + str(index) + '_1_1'
        s.histName_forFlip_dD_CRttX_emu_systmc_Down = 'flip_syst_tr_flipDown_cut_1_0_0_' + str(index) + '_1_1'
        s.histName_forFlipFit_SR_ee = 'Tprime_transverse_mass_First_cut_2_0_0_' + str(index) + '_0_1'
        s.histName_forFlipFit_CRttX_ee = 'Tprime_transverse_mass_First_cut_2_0_0_' + str(index) + '_1_1'
        s.histName_forFlipDD_ee = 'Tprime_transverse_mass_First_cut_2_0_1_' + str(index) + '_0'
        s.histName_forFlip_dD_SR_ee = 'flip_evWeight_cut_2_0_0_' + str(index) + '_0_1'
        s.histName_forFlip_dD_SR_ee_systshape_Up = 'flip_syst_mr_flipUp_cut_2_0_0_' + str(index) + '_0_1'
        s.histName_forFlip_dD_SR_ee_systshape_Down = 'flip_syst_mr_flipDown_cut_2_0_0_' + str(index) + '_0_1'
        s.histName_forFlip_dD_SR_ee_systmc_Up = 'flip_syst_tr_flipUp_cut_2_0_0_' + str(index) + '_0_1'
        s.histName_forFlip_dD_SR_ee_systmc_Down = 'flip_syst_tr_flipDown_cut_2_0_0_' + str(index) + '_0_1'
        s.histName_forFlip_dD_CRttX_ee = 'flip_evWeight_cut_2_0_0_' + str(index) + '_1_1'
        s.histName_forFlip_dD_CRttX_ee_systshape_Up = 'flip_syst_mr_flipUp_cut_2_0_0_' + str(index) + '_1_1'
        s.histName_forFlip_dD_CRttX_ee_systshape_Down = 'flip_syst_mr_flipDown_cut_2_0_0_' + str(index) + '_1_1'
        s.histName_forFlip_dD_CRttX_ee_systmc_Up = 'flip_syst_tr_flipUp_cut_2_0_0_' + str(index) + '_1_1'
        s.histName_forFlip_dD_CRttX_ee_systmc_Down = 'flip_syst_tr_flipDown_cut_2_0_0_' + str(index) + '_1_1'

    if fakedd:
        if year == "Run2":
            s.tt1lregion_data_file = inputfiles + 'Data_Run2.root'
        else:
            s.tt1lregion_data_file = inputfiles + 'Data_UL' + year + '.root'
        s.histName_forFakeFit_SR_mumu = 'Tprime_transverse_mass_First_cut_0_0_0_' + str(index) + '_0_2'
        s.histName_forFakeFit_CRttX_mumu = 'Tprime_transverse_mass_First_cut_0_0_0_' + str(index) + '_1_2'
        s.histName_forFakeDD_mumu_SR = 'Tprime_transverse_mass_First_cut_0_0_2_' + str(index) + '_0'
        s.histName_forFakeDD_mumu_CRttX = 'Tprime_transverse_mass_First_cut_0_0_2_' + str(index) + '_1'
        s.histName_forFake_dD_SR_mumu = 'fake_evWeight_cut_0_0_0_' + str(index) + '_0_2'
        s.histName_forFake_dD_SR_mumu_systshape_Up = 'fake_syst_mr_fakeUp_cut_0_0_0_' + str(index) + '_0_2'
        s.histName_forFake_dD_SR_mumu_systshape_Down = 'fake_syst_mr_fakeDown_cut_0_0_0_' + str(index) + '_0_2'
        s.histName_forFake_dD_SR_mumu_systmc_Up = 'fake_syst_tr_fakeUp_cut_0_0_0_' + str(index) + '_0_2'
        s.histName_forFake_dD_SR_mumu_systmc_Down = 'fake_syst_tr_fakeDown_cut_0_0_0_' + str(index) + '_0_2'
        s.histName_forFake_dD_CRttX_mumu = 'fake_evWeight_cut_0_0_0_' + str(index) + '_1_2'
        s.histName_forFake_dD_CRttX_mumu_systshape_Up = 'fake_syst_mr_fakeUp_cut_0_0_0_' + str(index) + '_1_2'
        s.histName_forFake_dD_CRttX_mumu_systshape_Down = 'fake_syst_mr_fakeDown_cut_0_0_0_' + str(index) + '_1_2'
        s.histName_forFake_dD_CRttX_mumu_systmc_Up = 'fake_syst_tr_fakeUp_cut_0_0_0_' + str(index) + '_1_2'
        s.histName_forFake_dD_CRttX_mumu_systmc_Down = 'fake_syst_tr_fakeDown_cut_0_0_0_' + str(index) + '_1_2'
        s.histName_forFakeFit_SR_emu = 'Tprime_transverse_mass_First_cut_1_0_0_' + str(index) + '_0_2'
        s.histName_forFakeFit_CRttX_emu = 'Tprime_transverse_mass_First_cut_1_0_0_' + str(index) + '_1_2'
        s.histName_forFakeDD_emu_SR = 'Tprime_transverse_mass_First_cut_1_0_2_' + str(index) + '_0'
        s.histName_forFakeDD_emu_CRttX = 'Tprime_transverse_mass_First_cut_1_0_2_' + str(index) + '_1'
        s.histName_forFake_dD_SR_emu = 'fake_evWeight_cut_1_0_0_' + str(index) + '_0_2'
        s.histName_forFake_dD_SR_emu_systshape_Up = 'fake_syst_mr_fakeUp_cut_1_0_0_' + str(index) + '_0_2'
        s.histName_forFake_dD_SR_emu_systshape_Down = 'fake_syst_mr_fakeDown_cut_1_0_0_' + str(index) + '_0_2'
        s.histName_forFake_dD_SR_emu_systmc_Up = 'fake_syst_tr_fakeUp_cut_1_0_0_' + str(index) + '_0_2'
        s.histName_forFake_dD_SR_emu_systmc_Down = 'fake_syst_tr_fakeDown_cut_1_0_0_' + str(index) + '_0_2'
        s.histName_forFake_dD_CRttX_emu = 'fake_evWeight_cut_1_0_0_' + str(index) + '_1_2'
        s.histName_forFake_dD_CRttX_emu_systshape_Up = 'fake_syst_mr_fakeUp_cut_1_0_0_' + str(index) + '_1_2'
        s.histName_forFake_dD_CRttX_emu_systshape_Down = 'fake_syst_mr_fakeDown_cut_1_0_0_' + str(index) + '_1_2'
        s.histName_forFake_dD_CRttX_emu_systmc_Up = 'fake_syst_tr_fakeUp_cut_1_0_0_' + str(index) + '_1_2'
        s.histName_forFake_dD_CRttX_emu_systmc_Down = 'fake_syst_tr_fakeDown_cut_1_0_0_' + str(index) + '_1_2'
        s.histName_forFakeFit_SR_ee = 'Tprime_transverse_mass_First_cut_2_0_0_' + str(index) + '_0_2'
        s.histName_forFakeFit_CRttX_ee = 'Tprime_transverse_mass_First_cut_2_0_0_' + str(index) + '_1_2'
        s.histName_forFakeDD_ee_SR = 'Tprime_transverse_mass_First_cut_2_0_2_' + str(index) + '_0'
        s.histName_forFakeDD_ee_CRttX = 'Tprime_transverse_mass_First_cut_2_0_2_' + str(index) + '_1'
        s.histName_forFake_dD_SR_ee = 'fake_evWeight_cut_2_0_0_' + str(index) + '_0_2'
        s.histName_forFake_dD_SR_ee_systshape_Up = 'fake_syst_mr_fakeUp_cut_2_0_0_' + str(index) + '_0_2'
        s.histName_forFake_dD_SR_ee_systshape_Down = 'fake_syst_mr_fakeDown_cut_2_0_0_' + str(index) + '_0_2'
        s.histName_forFake_dD_SR_ee_systmc_Up = 'fake_syst_tr_fakeUp_cut_2_0_0_' + str(index) + '_0_2'
        s.histName_forFake_dD_SR_ee_systmc_Down = 'fake_syst_tr_fakeDown_cut_2_0_0_' + str(index) + '_0_2'
        s.histName_forFake_dD_CRttX_ee = 'fake_evWeight_cut_2_0_0_' + str(index) + '_1_2'
        s.histName_forFake_dD_CRttX_ee_systshape_Up = 'fake_syst_mr_fakeUp_cut_2_0_0_' + str(index) + '_1_2'
        s.histName_forFake_dD_CRttX_ee_systshape_Down = 'fake_syst_mr_fakeDown_cut_2_0_0_' + str(index) + '_1_2'
        s.histName_forFake_dD_CRttX_ee_systmc_Up = 'fake_syst_tr_fakeUp_cut_2_0_0_' + str(index) + '_1_2'
        s.histName_forFake_dD_CRttX_ee_systmc_Down = 'fake_syst_tr_fakeDown_cut_2_0_0_' + str(index) + '_1_2'
        
    # Loop through runs and set up channels
    for run in runs:
        if run == "2016preVFP":
            print("Run 2016_preVFP")
        
        elif run == "2016postVFP":
            print("Run 2016_postVFP")

        elif run == "2016":
            print("Run 2016")
        
        elif run == "2017":
            print("Run 2017")

        elif run == "2018":
            print("Run 2018")

        elif run == "Run2":
            print("Run Run2")

        if run == "Run2":
            s.addChannel(inputfiles + signal + '_Run2.root', signal, SignalColor, isMC=True, xsec=crosssection, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'TT_2L_Run2.root', 't#bar{t}2l', tt2lColor, isMC=True, xsec=88.3419*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'TT_SL_Run2.root', 't#bar{t}1l', tt1lColor, isMC=True, xsec=365.4574*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'TT_Had_Run2.root', 't#bar{t}0l', tt0lColor, isMC=True, xsec=377.9607*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'tW_antitop_Run2.root', 'Singletop', SingletopColor, isMC=True, xsec=39.65*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'tW_top_Run2.root', 'Singletop', SingletopColor, isMC=True, xsec=39.65*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'tHq_Run2.root', 'Singletop', SingletopColor, isMC=True, xsec=0.07096*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'tHW_Run2.root', 'Singletop', SingletopColor, isMC=True, xsec=0.01561*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'tZq_Run2.root', 'Singletop', SingletopColor, isMC=True, xsec=0.07358*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'ttH_Run2.root', 't#bar{t}X', ttXColor, isMC=True, xsec=0.2112*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'ttW_Run2.root', 't#bar{t}X', ttXColor, isMC=True, xsec=0.235*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'ttZ_Run2.root', 't#bar{t}X', ttXColor, isMC=True, xsec=0.859*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'WW_Run2.root', 'Multibosons', MultibosonsColor, isMC=True, xsec=12.178*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'WW_DS_Run2.root', 'Multibosons', MultibosonsColor, isMC=True, xsec=0.2232*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'WH_plus_Run2.root', 'Multibosons', MultibosonsColor, isMC=True, xsec=0.0313*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'WH_minus_Run2.root', 'Multibosons', MultibosonsColor, isMC=True, xsec=0.0313*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'WZ_Run2.root', 'Multibosons', MultibosonsColor, isMC=True, xsec=4.4297*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'ZH_Run2.root', 'Multibosons', MultibosonsColor, isMC=True, xsec=0.1858*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'ZZ_2L2Nu_Run2.root', 'Multibosons', MultibosonsColor, isMC=True, xsec=0.5644*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'ZZ_2L2Q_Run2.root', 'Multibosons', MultibosonsColor, isMC=True, xsec=1.973*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'ZZ_4L_Run2.root', 'Multibosons', MultibosonsColor, isMC=True, xsec=1.256*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'WWW_Run2.root', 'Multibosons', MultibosonsColor, isMC=True, xsec=0.2086*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'WWZ_Run2.root', 'Multibosons', MultibosonsColor, isMC=True, xsec=0.1651*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'WZZ_Run2.root', 'Multibosons', MultibosonsColor, isMC=True, xsec=0.05565*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'ZZZ_Run2.root', 'Multibosons', MultibosonsColor, isMC=True, xsec=0.01476*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            # s.addChannel(inputfiles + 'WJets_Run2.root', 'WJets', WJetsColor, isMC=True, xsec=61334.9*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            # s.addChannel(inputfiles + 'DY_10_50_Run2.root', 'DY', DYColor, isMC=True, xsec=18610*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'DY_4_50_70_Run2.root', 'DY', DYColor, isMC=True, xsec=301.2*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'DY_4_50_100_Run2.root', 'DY', DYColor, isMC=True, xsec=224.2*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'DY_4_50_200_Run2.root', 'DY', DYColor, isMC=True, xsec=37.2*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'DY_4_50_400_Run2.root', 'DY', DYColor, isMC=True, xsec=3.581*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'DY_4_50_600_Run2.root', 'DY', DYColor, isMC=True, xsec=1.124*10**3, scale_factor=MC_SF, counter_histogram_root = '')

            if data:
                s.addChannel(inputfiles + 'Data_Run2.root', 'Data', DataColor, isMC=False)

            if flipdd:
                s.addChannel(inputfiles + 'Data_Run2_Flip_dataDriven.root', 'tt2l_{DataDriven}', tt2lColor, isMC=True, xsec=1, scale_factor=dataDriven_SF, counter_histogram_root = '')
                s.add_mctt2l_channel_forFit(inputfiles + 'TT_2L_Run2.root', xsec=88.3419*10**3)

            if fakedd:
                s.addChannel(inputfiles + 'Data_Run2_Fake_dataDriven.root', 'tt1l_{DataDriven}', tt1lColor, isMC=True, xsec=1, scale_factor=dataDriven_SF, counter_histogram_root = '')
                s.add_mctt1l_channel_forFit(inputfiles + 'TT_2L_Run2.root', xsec=88.3419*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'TT_SL_Run2.root', xsec=365.4574*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'TT_Had_Run2.root', xsec=377.9607*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'tW_antitop_Run2.root', xsec=39.65*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'tW_top_Run2.root', xsec=39.65*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'tHq_Run2.root', xsec=0.07096*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'tHW_Run2.root', xsec=0.01561*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'tZq_Run2.root', xsec=0.07358*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'ttH_Run2.root', xsec=0.2112*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'ttW_Run2.root', xsec=0.235*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'ttZ_Run2.root', xsec=0.859*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'WW_Run2.root', xsec=12.178*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'WW_DS_Run2.root', xsec=0.2232*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'WH_plus_Run2.root', xsec=0.0313*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'WH_minus_Run2.root', xsec=0.0313*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'WZ_Run2.root', xsec=4.4297*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'ZH_Run2.root', xsec=0.1858*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'ZZ_2L2Nu_Run2.root', xsec=0.5644*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'ZZ_2L2Q_Run2.root', xsec=1.973*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'ZZ_4L_Run2.root', xsec=1.256*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'WWW_Run2.root', xsec=0.2086*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'WWZ_Run2.root', xsec=0.1651*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'WZZ_Run2.root', xsec=0.05565*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'ZZZ_Run2.root', xsec=0.01476*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'DY_4_50_70_Run2.root', xsec=301.2*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'DY_4_50_100_Run2.root', xsec=224.2*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'DY_4_50_200_Run2.root', xsec=37.2*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'DY_4_50_400_Run2.root', xsec=3.581*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'DY_4_50_600_Run2.root', xsec=1.124*10**3)

        else:
            s.addChannel(inputfiles + signal + '_UL' + year + '.root', signal, SignalColor, isMC=True, xsec=crosssection, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'TT_2L_UL' + year + '.root', 't#bar{t}2l', tt2lColor, isMC=True, xsec=88.3419*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'TT_SL_UL' + year + '.root', 't#bar{t}1l', tt1lColor, isMC=True, xsec=365.4574*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'TT_Had_UL' + year + '.root', 't#bar{t}0l', tt0lColor, isMC=True, xsec=377.9607*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'tW_antitop_UL' + year + '.root', 'Singletop', SingletopColor, isMC=True, xsec=39.65*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'tW_top_UL' + year + '.root', 'Singletop', SingletopColor, isMC=True, xsec=39.65*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'tHq_UL' + year + '.root', 'Singletop', SingletopColor, isMC=True, xsec=0.07096*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'tHW_UL' + year + '.root', 'Singletop', SingletopColor, isMC=True, xsec=0.01561*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'tZq_UL' + year + '.root', 'Singletop', SingletopColor, isMC=True, xsec=0.07358*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'ttH_UL' + year + '.root', 't#bar{t}X', ttXColor, isMC=True, xsec=0.2112*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'ttW_UL' + year + '.root', 't#bar{t}X', ttXColor, isMC=True, xsec=0.235*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'ttZ_UL' + year + '.root', 't#bar{t}X', ttXColor, isMC=True, xsec=0.859*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'WW_UL' + year + '.root', 'Multibosons', MultibosonsColor, isMC=True, xsec=12.178*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'WW_DS_UL' + year + '.root', 'Multibosons', MultibosonsColor, isMC=True, xsec=0.2232*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'WH_plus_UL' + year + '.root', 'Multibosons', MultibosonsColor, isMC=True, xsec=0.0313*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'WH_minus_UL' + year + '.root', 'Multibosons', MultibosonsColor, isMC=True, xsec=0.0313*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'WZ_UL' + year + '.root', 'Multibosons', MultibosonsColor, isMC=True, xsec=4.4297*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'ZH_UL' + year + '.root', 'Multibosons', MultibosonsColor, isMC=True, xsec=0.1858*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'ZZ_2L2Nu_UL' + year + '.root', 'Multibosons', MultibosonsColor, isMC=True, xsec=0.5644*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'ZZ_2L2Q_UL' + year + '.root', 'Multibosons', MultibosonsColor, isMC=True, xsec=1.973*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'ZZ_4L_UL' + year + '.root', 'Multibosons', MultibosonsColor, isMC=True, xsec=1.256*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'WWW_UL' + year + '.root', 'Multibosons', MultibosonsColor, isMC=True, xsec=0.2086*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'WWZ_UL' + year + '.root', 'Multibosons', MultibosonsColor, isMC=True, xsec=0.1651*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'WZZ_UL' + year + '.root', 'Multibosons', MultibosonsColor, isMC=True, xsec=0.05565*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'ZZZ_UL' + year + '.root', 'Multibosons', MultibosonsColor, isMC=True, xsec=0.01476*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            # s.addChannel(inputfiles + 'WJets_UL' + year + '.root', 'WJets', WJetsColor, isMC=True, xsec=61334.9*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            # s.addChannel(inputfiles + 'DY_10_50_UL' + year + '.root', 'DY', DYColor, isMC=True, xsec=18610*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'DY_4_50_70_UL' + year + '.root', 'DY', DYColor, isMC=True, xsec=301.2*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'DY_4_50_100_UL' + year + '.root', 'DY', DYColor, isMC=True, xsec=224.2*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'DY_4_50_200_UL' + year + '.root', 'DY', DYColor, isMC=True, xsec=37.2*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'DY_4_50_400_UL' + year + '.root', 'DY', DYColor, isMC=True, xsec=3.581*10**3, scale_factor=MC_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'DY_4_50_600_UL' + year + '.root', 'DY', DYColor, isMC=True, xsec=1.124*10**3, scale_factor=MC_SF, counter_histogram_root = '')

            if data:
                s.addChannel(inputfiles + 'Data_UL' + year + '.root', 'Data', DataColor, isMC=False)

            if flipdd:
                s.addChannel(inputfiles + 'Data_UL' + year + '_Flip_dataDriven.root', 'tt2l_{DataDriven}', tt2lColor, isMC=True, xsec=1, scale_factor=dataDriven_SF, counter_histogram_root = '')
                s.add_mctt2l_channel_forFit(inputfiles + 'TT_2L_UL' + year + '.root', xsec=87.315*10**3)

            if fakedd:
                s.addChannel(inputfiles + 'Data_UL' + year + '_Fake_dataDriven.root', 'tt1l_{DataDriven}', tt1lColor, isMC=True, xsec=1, scale_factor=dataDriven_SF, counter_histogram_root = '')
                s.add_mctt1l_channel_forFit(inputfiles + 'TT_2L_UL' + year + '.root', xsec=88.3419*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'TT_SL_UL' + year + '.root', xsec=365.4574*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'TT_Had_UL' + year + '.root', xsec=377.9607*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'tW_antitop_UL' + year + '.root', xsec=39.65*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'tW_top_UL' + year + '.root', xsec=39.65*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'tHq_UL' + year + '.root', xsec=0.07096*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'tHW_UL' + year + '.root', xsec=0.01561*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'tZq_UL' + year + '.root', xsec=0.07358*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'ttH_UL' + year + '.root', xsec=0.2112*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'ttW_UL' + year + '.root', xsec=0.235*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'ttZ_UL' + year + '.root', xsec=0.859*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'WW_UL' + year + '.root', xsec=12.178*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'WW_DS_UL' + year + '.root', xsec=0.2232*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'WH_plus_UL' + year + '.root', xsec=0.0313*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'WH_minus_UL' + year + '.root', xsec=0.0313*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'WZ_UL' + year + '.root', xsec=4.4297*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'ZH_UL' + year + '.root', xsec=0.1858*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'ZZ_2L2Nu_UL' + year + '.root', xsec=0.5644*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'ZZ_2L2Q_UL' + year + '.root', xsec=1.973*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'ZZ_4L_UL' + year + '.root', xsec=1.256*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'WWW_UL' + year + '.root', xsec=0.2086*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'WWZ_UL' + year + '.root', xsec=0.1651*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'WZZ_UL' + year + '.root', xsec=0.05565*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'ZZZ_UL' + year + '.root', xsec=0.01476*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'DY_4_50_70_UL' + year + '.root', xsec=301.2*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'DY_4_50_100_UL' + year + '.root', xsec=224.2*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'DY_4_50_200_UL' + year + '.root', xsec=37.2*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'DY_4_50_400_UL' + year + '.root', xsec=3.581*10**3)
                s.add_mctt1l_channel_forFit(inputfiles + 'DY_4_50_600_UL' + year + '.root', xsec=1.124*10**3)

    from histogram_adder import (
    add_MC_histograms_to_stack,
    add_histograms_to_stack,
    )

    # if not data:
    #     add_MC_histograms_to_stack(s, index)
    add_histograms_to_stack(s, index)

    # Drawing histograms and final output
    imageList = []
    subplot = "R" if ratio else ""
    s.draw(subplot, inputfiles, signal, imageList, flipdd, fakedd, paper, year, mass)

    from PIL import Image

    if year == "Run2":
        if data:
            if fakedd and flipdd:
                pdf_path = inputfiles + 'plots/Data_Run2_' + mass + '_DataDriven.pdf'
            else:
                pdf_path = inputfiles + 'plots/Data_Run2_' + mass + '.pdf'
        else:
            pdf_path = inputfiles + 'plots/Run2_' + mass + '.pdf'
    else:
        if data:
            if fakedd and flipdd:
                pdf_path = inputfiles + 'plots/Data_UL' + year + '_' + mass + '_DataDriven.pdf'
            else:
                pdf_path = inputfiles + 'plots/Data_UL' + year + '_' + mass + '.pdf'
        else:
            pdf_path = inputfiles + 'plots/UL' + year + '_' + mass + '.pdf'
    if paper:
        pdf_path = pdf_path.replace('.pdf','_Paper.pdf')
    pdf_path = pdf_path.replace('.pdf','_pouet.pdf')

    cmd = ["gs", "-dBATCH", "-dNOPAUSE", "-q", "-sDEVICE=pdfwrite", f"-sOutputFile={pdf_path}"] + imageList
    subprocess.run(cmd)

    listFolder = list_folder(inputfiles)
    # print(listFolder)
    for folder in listFolder:
        print('Removing '+folder)
        shutil.rmtree(folder)

if __name__ == '__main__':
    main()