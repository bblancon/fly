import stackhists

def add_MC_histograms_to_stack(s, index):
    histograms = [

        {"name": 'genweight', "title": 'Genweight of Events', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        {"name": 'puweight', "title": 'PU', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        {"name": 'pugenweight', "title": 'PU * Genweight of Events', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        {"name": 'MotherFromLepton1', "title": 'Nature of mother of first lepton', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        {"name": 'MotherFromLepton2', "title": 'Nature of mother of second lepton', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        {"name": 'MotherFromLepton12', "title": 'Sum of nature of mothers of leptons', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        {"name": 'muon_sf_central', "title": 'Muon SF central', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        {"name": 'ele_sf_central', "title": 'Electron SF central', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        {"name": 'btag_sf_bcflav_central', "title": 'Btag SF central (bc flavour)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        {"name": 'btag_sf_lflav_central', "title": 'Btag SF central (light flavour)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        {"name": 'btag_total', "title": 'Btag SF central', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        {"name": 'evWeight_wobtagsf', "title": 'Event weight without Btag', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        {"name": 'evWeight', "title": 'Event weight', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},

    ]

    # for i in range(4):
    #     for hist in histograms:
    #         s.addHistogram(hist["name"]+'_cut_'+str(i)+'_0_0_'+str(index)+'_0', hist["title"], hist["y_label"], draw_mode=hist["draw_mode"], draw_option=hist["draw_option"], is_logy=hist.get("is_logy", False), underflow_bin=hist.get("underflow_bin", False), extratext=hist.get("extratext", ''))
    #         s.addHistogram(hist["name"]+'_cut_'+str(i)+'_0_0_'+str(index)+'_1', hist["title"], hist["y_label"], draw_mode=hist["draw_mode"], draw_option=hist["draw_option"], is_logy=hist.get("is_logy", False), underflow_bin=hist.get("underflow_bin", False), extratext=hist.get("extratext", ''))
    #         s.addHistogram(hist["name"]+'_cut_'+str(i)+'_0_1_'+str(index)+'_0', hist["title"], hist["y_label"], draw_mode=hist["draw_mode"], draw_option=hist["draw_option"], is_logy=hist.get("is_logy", False), underflow_bin=hist.get("underflow_bin", False), extratext=hist.get("extratext", ''))
    #         s.addHistogram(hist["name"]+'_cut_'+str(i)+'_0_2_'+str(index)+'_0', hist["title"], hist["y_label"], draw_mode=hist["draw_mode"], draw_option=hist["draw_option"], is_logy=hist.get("is_logy", False), underflow_bin=hist.get("underflow_bin", False), extratext=hist.get("extratext", ''))
    #         s.addHistogram(hist["name"]+'_cut_'+str(i)+'_0_0_'+str(index)+'_0', hist["title"], hist["y_label"], draw_mode=hist["draw_mode"], draw_option=hist["draw_option"], is_logy=hist.get("is_logy", True), underflow_bin=hist.get("underflow_bin", False), extratext=hist.get("extratext", ''))
    #         s.addHistogram(hist["name"]+'_cut_'+str(i)+'_0_0_'+str(index)+'_1', hist["title"], hist["y_label"], draw_mode=hist["draw_mode"], draw_option=hist["draw_option"], is_logy=hist.get("is_logy", True), underflow_bin=hist.get("underflow_bin", False), extratext=hist.get("extratext", ''))
    #         s.addHistogram(hist["name"]+'_cut_'+str(i)+'_0_1_'+str(index)+'_0', hist["title"], hist["y_label"], draw_mode=hist["draw_mode"], draw_option=hist["draw_option"], is_logy=hist.get("is_logy", True), underflow_bin=hist.get("underflow_bin", False), extratext=hist.get("extratext", ''))
    #         s.addHistogram(hist["name"]+'_cut_'+str(i)+'_0_2_'+str(index)+'_0', hist["title"], hist["y_label"], draw_mode=hist["draw_mode"], draw_option=hist["draw_option"], is_logy=hist.get("is_logy", True), underflow_bin=hist.get("underflow_bin", False), extratext=hist.get("extratext", ''))

    for hist in histograms:
        s.addHistogram(hist["name"]+'_cut_3_0_0_'+str(index)+'_0', hist["title"], hist["y_label"], draw_mode=hist["draw_mode"], draw_option=hist["draw_option"], is_logy=hist.get("is_logy", False), underflow_bin=hist.get("underflow_bin", False), extratext=hist.get("extratext", ''))
        s.addHistogram(hist["name"]+'_cut_3_0_0_'+str(index)+'_1', hist["title"], hist["y_label"], draw_mode=hist["draw_mode"], draw_option=hist["draw_option"], is_logy=hist.get("is_logy", False), underflow_bin=hist.get("underflow_bin", False), extratext=hist.get("extratext", ''))
        s.addHistogram(hist["name"]+'_cut_3_0_1_'+str(index)+'_0', hist["title"], hist["y_label"], draw_mode=hist["draw_mode"], draw_option=hist["draw_option"], is_logy=hist.get("is_logy", False), underflow_bin=hist.get("underflow_bin", False), extratext=hist.get("extratext", ''))
        s.addHistogram(hist["name"]+'_cut_3_0_2_'+str(index)+'_0', hist["title"], hist["y_label"], draw_mode=hist["draw_mode"], draw_option=hist["draw_option"], is_logy=hist.get("is_logy", False), underflow_bin=hist.get("underflow_bin", False), extratext=hist.get("extratext", ''))
        s.addHistogram(hist["name"]+'_cut_3_0_0_'+str(index)+'_0', hist["title"], hist["y_label"], draw_mode=hist["draw_mode"], draw_option=hist["draw_option"], is_logy=hist.get("is_logy", True), underflow_bin=hist.get("underflow_bin", False), extratext=hist.get("extratext", ''))
        s.addHistogram(hist["name"]+'_cut_3_0_0_'+str(index)+'_1', hist["title"], hist["y_label"], draw_mode=hist["draw_mode"], draw_option=hist["draw_option"], is_logy=hist.get("is_logy", True), underflow_bin=hist.get("underflow_bin", False), extratext=hist.get("extratext", ''))
        s.addHistogram(hist["name"]+'_cut_3_0_1_'+str(index)+'_0', hist["title"], hist["y_label"], draw_mode=hist["draw_mode"], draw_option=hist["draw_option"], is_logy=hist.get("is_logy", True), underflow_bin=hist.get("underflow_bin", False), extratext=hist.get("extratext", ''))
        s.addHistogram(hist["name"]+'_cut_3_0_2_'+str(index)+'_0', hist["title"], hist["y_label"], draw_mode=hist["draw_mode"], draw_option=hist["draw_option"], is_logy=hist.get("is_logy", True), underflow_bin=hist.get("underflow_bin", False), extratext=hist.get("extratext", ''))

def add_histograms_to_stack(s, index):
    histograms = [

        # # {"name": 'hnevents', "title": 'Number(Events)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'Number_Electrons', "title": 'Number(electrons)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # {"name": 'Pt_Electrons', "title": 'p_{T}(electrons) (GeV)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'Leading_Pt_Electrons', "title": 'p_{T}(e_{1}) (GeV)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'Subleading_Pt_Electrons', "title": 'p_{T}(e_{2}) (GeV)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # {"name": 'Eta_Electrons', "title": 'Eta(electrons)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'Leading_Eta_Electrons', "title": 'Eta(e_{1})', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'Subleading_Eta_Electrons', "title": 'Eta(e_{2})', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # {"name": 'Phi_Electrons', "title": 'Phi(electrons)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'Charge_Electrons', "title": 'Charge(electrons)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'miniPFRelIso_all_Electrons', "title": 'Mini relative isolation(electrons)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'Sip_Electrons', "title": 'SIP_{3D}(electrons)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'Number_Muons', "title": 'Number(muons)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # {"name": 'Pt_Muons', "title": 'p_{T}(muons) (GeV)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'Leading_Pt_Muons', "title": 'p_{T}(\mu_{1}) (GeV)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'Subleading_Pt_Muons', "title": 'p_{T}(\mu_{2}) (GeV)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # {"name": 'Eta_Muons', "title": 'Eta(muons)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'Leading_Eta_Muons', "title": 'Eta(\mu_{1})', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'Subleading_Eta_Muons', "title": 'Eta(\mu_{2})', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # {"name": 'Phi_Muons', "title": 'Phi(muons)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'Charge_Muons', "title": 'Charge(muons)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'miniPFRelIso_all_Muons', "title": 'Mini relative isolation(muons)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'Sip_Muons', "title": 'SIP_{3D}(muons)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'Number_Leptons', "title": 'Number(leptons)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'Pt_Leptons', "title": 'p_{T}(leptons) (GeV)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'Leading_Pt_Leptons', "title": 'p_{T}(l_{1}) (GeV)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'Subleading_Pt_Leptons', "title": 'p_{T}(l_{2}) (GeV)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # {"name": 'Sum_Pt_Two_Leptons', "title": 'p_{T}(l_{1}) + p_{T}(l_{2}) (GeV)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'Eta_Leptons', "title": 'Eta(leptons)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'Leading_Eta_Leptons', "title": 'Eta(l_{1})', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'Subleading_Eta_Leptons', "title": 'Eta(l_{2})', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'Phi_Leptons', "title": 'Phi(leptons)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'Charge_Leptons', "title": 'Charge(leptons)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'Deltaeta_Leptons', "title": '\Delta \eta (l_{1},l_{2})', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'Deltaphi_Leptons', "title": '\Delta \phi (l_{1},l_{2})', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # {"name": 'DeltaR_Leptons', "title": '\Delta R (l_{1},l_{2})', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'miniPFRelIso_all_Leptons', "title": 'Mini relative isolation(leptons)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'Sip_Leptons', "title": 'SIP_{3D}(leptons)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'Pt_Sum_Two_Leptons', "title": 'p_{T}(l_{1}+l_{2}) (GeV)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'Mass_Sum_Two_Leptons', "title": 'M(l_{1}+l_{2}) (GeV)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # {"name": 'Number_Jets', "title": 'Number(jets)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # {"name": 'Pt_Jets', "title": 'p_{T}(jets) (GeV)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'Leading_Pt_Jets', "title": 'p_{T}(j_{1}) (GeV)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'Subleading_Pt_Jets', "title": 'p_{T}(j_{2}) (GeV)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'Subsubleading_Pt_Jets', "title": 'p_{T}(j_{3}) (GeV)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'Ht_Jets', "title": 'Ht (GeV)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # {"name": 'Eta_Jets', "title": 'Eta(jets)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # {"name": 'Phi_Jets', "title": 'Phi(jets)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'Mass_Jets', "title": 'M(jets) (GeV)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'DeepB_Jets', "title": 'DeepCSV(jets)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'DeepFlavB_Jets', "title": 'DeepJet(jets)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # {"name": 'Number_bJets', "title": 'Number(bjets)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # {"name": 'Pt_bJets', "title": 'p_{T}(bjets) (GeV)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'Leading_Pt_bJets', "title": 'p_{T}(b_{1}) (GeV)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'Subleading_Pt_bJets', "title": 'p_{T}(b_{2}) (GeV)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'Subsubleading_Pt_bJets', "title": 'p_{T}(b_{3}) (GeV)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # {"name": 'Eta_bJets', "title": 'Eta(bjets)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # {"name": 'Phi_bJets', "title": 'Phi(bjets)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'Mass_bJets', "title": 'M(bjets) (GeV)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'DeepB_bJets', "title": 'DeepCSV(bjets)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'DeepFlavB_bJets', "title": 'DeepJet(bjets)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'Mass_Sum_Three_Jets', "title": 'M(3j) (GeV)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'Mass_Sum_Three_Jets_offset', "title": 'M(3j) - m_{top} (GeV)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # {"name": 'Mass_Sum_Three_Jets_min', "title": 'Min(|M(3j) - m_{top}|) (GeV)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'Pt_MET', "title": 'p_{T}(MET) (GeV)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'Phi_MET', "title": 'Phi(MET)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'St', "title": 'St_{lep} (GeV)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'St_with_MET', "title": 'St (GeV)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'Mass_LeptonFromtop_bJetFromtop', "title": 'M_{lb} (GeV)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        {"name": 'Tprime_transverse_mass_First', "title": 'M_{T,T\'} (GeV)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        # # {"name": 'Tprime_transverse_mass_Second', "title": 'M_{T,T\'} (GeV)', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'}

    ]

    # for i in range(4):
    #     for hist in histograms:
    #         s.addHistogram(hist["name"]+'_cut_'+str(i)+'_0_0_'+str(index)+'_0', hist["title"], hist["y_label"], draw_mode=hist["draw_mode"], draw_option=hist["draw_option"], is_logy=hist.get("is_logy", False), underflow_bin=hist.get("underflow_bin", False), extratext=hist.get("extratext", ''))
    #         s.addHistogram(hist["name"]+'_cut_'+str(i)+'_0_0_'+str(index)+'_1', hist["title"], hist["y_label"], draw_mode=hist["draw_mode"], draw_option=hist["draw_option"], is_logy=hist.get("is_logy", False), underflow_bin=hist.get("underflow_bin", False), extratext=hist.get("extratext", ''))
    #         s.addHistogram(hist["name"]+'_cut_'+str(i)+'_0_1_'+str(index)+'_0', hist["title"], hist["y_label"], draw_mode=hist["draw_mode"], draw_option=hist["draw_option"], is_logy=hist.get("is_logy", False), underflow_bin=hist.get("underflow_bin", False), extratext=hist.get("extratext", ''))
    #         s.addHistogram(hist["name"]+'_cut_'+str(i)+'_0_2_'+str(index)+'_0', hist["title"], hist["y_label"], draw_mode=hist["draw_mode"], draw_option=hist["draw_option"], is_logy=hist.get("is_logy", False), underflow_bin=hist.get("underflow_bin", False), extratext=hist.get("extratext", ''))
    #         s.addHistogram(hist["name"]+'_cut_'+str(i)+'_0_0_'+str(index)+'_0', hist["title"], hist["y_label"], draw_mode=hist["draw_mode"], draw_option=hist["draw_option"], is_logy=hist.get("is_logy", True), underflow_bin=hist.get("underflow_bin", False), extratext=hist.get("extratext", ''))
    #         s.addHistogram(hist["name"]+'_cut_'+str(i)+'_0_0_'+str(index)+'_1', hist["title"], hist["y_label"], draw_mode=hist["draw_mode"], draw_option=hist["draw_option"], is_logy=hist.get("is_logy", True), underflow_bin=hist.get("underflow_bin", False), extratext=hist.get("extratext", ''))
    #         s.addHistogram(hist["name"]+'_cut_'+str(i)+'_0_1_'+str(index)+'_0', hist["title"], hist["y_label"], draw_mode=hist["draw_mode"], draw_option=hist["draw_option"], is_logy=hist.get("is_logy", True), underflow_bin=hist.get("underflow_bin", False), extratext=hist.get("extratext", ''))
    #         s.addHistogram(hist["name"]+'_cut_'+str(i)+'_0_2_'+str(index)+'_0', hist["title"], hist["y_label"], draw_mode=hist["draw_mode"], draw_option=hist["draw_option"], is_logy=hist.get("is_logy", True), underflow_bin=hist.get("underflow_bin", False), extratext=hist.get("extratext", ''))

    for hist in histograms:
        # s.addHistogram(hist["name"]+'_cut_3', hist["title"], hist["y_label"], draw_mode=hist["draw_mode"], draw_option=hist["draw_option"], is_logy=hist.get("is_logy", False), underflow_bin=hist.get("underflow_bin", False), extratext=hist.get("extratext", ''))
        # s.addHistogram(hist["name"]+'_cut_3_0', hist["title"], hist["y_label"], draw_mode=hist["draw_mode"], draw_option=hist["draw_option"], is_logy=hist.get("is_logy", False), underflow_bin=hist.get("underflow_bin", False), extratext=hist.get("extratext", ''))
        # s.addHistogram(hist["name"]+'_cut_3_0_0', hist["title"], hist["y_label"], draw_mode=hist["draw_mode"], draw_option=hist["draw_option"], is_logy=hist.get("is_logy", False), underflow_bin=hist.get("underflow_bin", False), extratext=hist.get("extratext", ''))
        # s.addHistogram(hist["name"]+'_cut_3_0_0_0', hist["title"], hist["y_label"], draw_mode=hist["draw_mode"], draw_option=hist["draw_option"], is_logy=hist.get("is_logy", False), underflow_bin=hist.get("underflow_bin", False), extratext=hist.get("extratext", ''))
        # s.addHistogram(hist["name"]+'_cut_3_0_0_0_0', hist["title"], hist["y_label"], draw_mode=hist["draw_mode"], draw_option=hist["draw_option"], is_logy=hist.get("is_logy", False), underflow_bin=hist.get("underflow_bin", False), extratext=hist.get("extratext", ''))
        # s.addHistogram(hist["name"]+'_cut_3_0_0_0_0_0', hist["title"], hist["y_label"], draw_mode=hist["draw_mode"], draw_option=hist["draw_option"], is_logy=hist.get("is_logy", False), underflow_bin=hist.get("underflow_bin", False), extratext=hist.get("extratext", ''))
        # s.addHistogram(hist["name"]+'_cut_3_0_0', hist["title"], hist["y_label"], draw_mode=hist["draw_mode"], draw_option=hist["draw_option"], is_logy=hist.get("is_logy", False), underflow_bin=hist.get("underflow_bin", False), extratext=hist.get("extratext", ''))
        # s.addHistogram(hist["name"]+'_cut_3_0_0_4', hist["title"], hist["y_label"], draw_mode=hist["draw_mode"], draw_option=hist["draw_option"], is_logy=hist.get("is_logy", False), underflow_bin=hist.get("underflow_bin", False), extratext=hist.get("extratext", ''))
        # s.addHistogram(hist["name"]+'_cut_3_0_0_4_0', hist["title"], hist["y_label"], draw_mode=hist["draw_mode"], draw_option=hist["draw_option"], is_logy=hist.get("is_logy", False), underflow_bin=hist.get("underflow_bin", False), extratext=hist.get("extratext", ''))
        # s.addHistogram(hist["name"]+'_cut_3_0_0_4_0_0', hist["title"], hist["y_label"], draw_mode=hist["draw_mode"], draw_option=hist["draw_option"], is_logy=hist.get("is_logy", False), underflow_bin=hist.get("underflow_bin", False), extratext=hist.get("extratext", ''))
        s.addHistogram(hist["name"]+'_cut_3_0_0_'+str(index)+'_0', hist["title"], hist["y_label"], draw_mode=hist["draw_mode"], draw_option=hist["draw_option"], is_logy=hist.get("is_logy", False), underflow_bin=hist.get("underflow_bin", False), extratext=hist.get("extratext", ''))
        # s.addHistogram(hist["name"]+'_cut_3_0_0_'+str(index)+'_1', hist["title"], hist["y_label"], draw_mode=hist["draw_mode"], draw_option=hist["draw_option"], is_logy=hist.get("is_logy", False), underflow_bin=hist.get("underflow_bin", False), extratext=hist.get("extratext", ''))
        # s.addHistogram(hist["name"]+'_cut_3_0_1_'+str(index)+'_0', hist["title"], hist["y_label"], draw_mode=hist["draw_mode"], draw_option=hist["draw_option"], is_logy=hist.get("is_logy", False), underflow_bin=hist.get("underflow_bin", False), extratext=hist.get("extratext", ''))
        # s.addHistogram(hist["name"]+'_cut_3_0_2_'+str(index)+'_0', hist["title"], hist["y_label"], draw_mode=hist["draw_mode"], draw_option=hist["draw_option"], is_logy=hist.get("is_logy", False), underflow_bin=hist.get("underflow_bin", False), extratext=hist.get("extratext", ''))
        # s.addHistogram(hist["name"]+'_cut_3_0_2_'+str(index)+'_1', hist["title"], hist["y_label"], draw_mode=hist["draw_mode"], draw_option=hist["draw_option"], is_logy=hist.get("is_logy", False), underflow_bin=hist.get("underflow_bin", False), extratext=hist.get("extratext", ''))
        # s.addHistogram(hist["name"]+'_cut_3_0_0_'+str(index)+'_0', hist["title"], hist["y_label"], draw_mode=hist["draw_mode"], draw_option=hist["draw_option"], is_logy=hist.get("is_logy", True), underflow_bin=hist.get("underflow_bin", False), extratext=hist.get("extratext", ''))
        # s.addHistogram(hist["name"]+'_cut_3_0_0_'+str(index)+'_1', hist["title"], hist["y_label"], draw_mode=hist["draw_mode"], draw_option=hist["draw_option"], is_logy=hist.get("is_logy", True), underflow_bin=hist.get("underflow_bin", False), extratext=hist.get("extratext", ''))
        # s.addHistogram(hist["name"]+'_cut_3_0_1_'+str(index)+'_0', hist["title"], hist["y_label"], draw_mode=hist["draw_mode"], draw_option=hist["draw_option"], is_logy=hist.get("is_logy", True), underflow_bin=hist.get("underflow_bin", False), extratext=hist.get("extratext", ''))
        # s.addHistogram(hist["name"]+'_cut_3_0_2_'+str(index)+'_0', hist["title"], hist["y_label"], draw_mode=hist["draw_mode"], draw_option=hist["draw_option"], is_logy=hist.get("is_logy", True), underflow_bin=hist.get("underflow_bin", False), extratext=hist.get("extratext", ''))
        # s.addHistogram(hist["name"]+'_cut_3_0_2_'+str(index)+'_1', hist["title"], hist["y_label"], draw_mode=hist["draw_mode"], draw_option=hist["draw_option"], is_logy=hist.get("is_logy", True), underflow_bin=hist.get("underflow_bin", False), extratext=hist.get("extratext", ''))