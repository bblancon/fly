#!/usr/bin/env python3
import ROOT
import sys
import os

def list_filenames(root):
    listFiles=[]

    for file in os.listdir(root):
        filepath = os.path.join(root,file)
        if file.startswith('Data') and file.endswith('.root'):
            listFiles.append(filepath)
    return listFiles

sep='-'*15

if len( sys.argv )!=2:
    print("USAGE : % s < input file >"%(sys.argv[0]))
    sys.exit(1)

root = sys.argv[1]

list = list_filenames(root)

for file in list:
    while not os.path.exists(file):
        print('File not found')
        file = input('Enter file name: ')

    print("Processing", file)

    inFile = ROOT.TFile.Open(file)
    tree = inFile.Get("outputTree")
    outfile = ROOT.TFile.Open(file, "UPDATE")
    newtree = tree.CloneTree(0)

    EventID = []
    print("Processing ", tree.GetEntries(), " events")
    print(sep)
    print("Getting all the entries")
    print(sep)
    for i in range(tree.GetEntries()):
        try:
            tree.GetEntry(i)
        except:
            print("Error with entry", i, ". Quitting...")
            sys.exit(1)
        # For every entries, get the identification triplet [run,lumiblock,eventid], and associate it with the event number i in the file
        EventID.append([getattr(tree,"run"),getattr(tree,"luminosityBlock"), getattr(tree,"event"),i])
    print("Sorting")
    print(sep)
    # Sort event by run, then lumiblock, then eventid
    EventID.sort()
    PosToRemove = [0] * tree.GetEntries() # 0 and 1, if 1 then remove
    print("Finding the double events")
    print(sep)
    for i in range(len(EventID)-1):
        # Compare the triplet for event i and i+1 (and verify if the events number are not the same, don't really remember what's the use but it's just being too precocious)
        # Since they are sorted, only look to the neighbourhood
        if EventID[i][0] == EventID[i+1][0] and EventID[i][1] == EventID[i+1][1] and EventID[i][2] == EventID[i+1][2] and EventID[i][3] != EventID[i+1][3]:
            # If they are the same, only remove the second one
            PosToRemove[EventID[i+1][3]] = 1
        else :
            PosToRemove[EventID[i+1][3]] = 0
    print("Removing them")
    print(sep)
    nbDouble = 0.0
    for i in range(len(PosToRemove)):
        tree.GetEntry(i)
        if PosToRemove[i] == 0:
            newtree.Fill()
            #Write them into the new file, or nothing if it is possible to remove the oders directly
        else:
            nbDouble+=1.0

    print("Double counted : ", round(nbDouble,0))
    print("Percentage : ", round(nbDouble/tree.GetEntries()*100,2), "%")
    print("Writting new tree")
    print(sep)
    newtree.Write("outputTree", ROOT.TObject.kOverwrite)
    print("Done")

print(sep)
print(sep*2)
print(sep)