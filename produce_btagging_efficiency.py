import os
import sys
import ROOT
from array import array

def merge_with_previous_bin(hist_nume, hist_denom, i, j):
    previous_content, previous_denom, new_nume, new_denom = 0,0,0,0,
    current_content = hist_nume.GetBinContent(i + 1, j + 1)
    current_denom = hist_denom.GetBinContent(i + 1, j + 1)

    while i > 0:
        previous_content += hist_nume.GetBinContent(i, j + 1)
        previous_denom += hist_denom.GetBinContent(i, j + 1)
        new_nume = previous_content + current_content
        new_denom = previous_denom + current_denom
        print("previous_content, current_content, previous_denom, current_denom, new_nume, new_denom", i)
        print(previous_content, current_content, previous_denom, current_denom, new_nume, new_denom)
        if new_nume >= 0 and new_denom > 0 and new_nume < new_denom:
            return new_nume/new_denom
        i -= 1
    return 1

# Define the base directory where the dataset folders are located
date = '2025_02_06_00'
# year = '2016preVFP'
# year = '2016postVFP'
# year = '2017'
year = '2018'

Lumi = 59.8
if('2016preVFP' in year):
    Lumi = 19.5
elif('2016postVFP' in year):
    Lumi = 16.8
elif('2017' in year):
    Lumi = 41.5
Samples = [

    'Signal600',
    'Signal625',
    'Signal650',
    'Signal675',
    'Signal700',
    'Signal800',
    'Signal900',
    'Signal1000',
    'Signal1100',
    'Signal1200',
    'DY_10_50',
    'DY_4_50_70',
    'DY_4_50_100',
    'DY_4_50_200',
    'DY_4_50_400',
    'DY_4_50_600',
    'TT_2L',
    'TT_SL',
    'TT_Had',
    'ttW',
    'ttH',
    'ttZ',
    'tW_antitop',
    'tW_top',
    'tHq',
    'tHW',
    'tZq',
    'WW',
    'WW_DS',
    'WJets',
    'WH_plus',
    'WH_minus',
    'WZ',
    'ZH',
    'ZZ_2L2Nu',
    'ZZ_2L2Q',
    'ZZ_4L',
    'WWW',
    'WWZ',
    'WZZ',
    'ZZZ',
]

pt_bins = [0., 50., 100., 150., 200., 250., 300., 350., 400., 450., 500., 550., 600., 650., 700., 750., 800., 850., 900., 950., 1000.]
eta_bins = [0., 0.25, 0.50, 0.75, 1., 1.25, 1.5, 1.75, 2., 2.25, 2.5]

btagpass_bcflav_pt_hist = ROOT.TH2D("btagpass_bcflav_pt_hist","btagpass_bcflav_pt_hist", len(eta_bins) - 1, array('d', eta_bins), len(pt_bins) - 1, array('d', pt_bins))
all_bcflav_pt_hist = ROOT.TH2D("all_bcflav_pt_hist","all_bcflav_pt_hist", len(eta_bins) - 1, array('d', eta_bins), len(pt_bins) - 1, array('d', pt_bins))
btagpass_lflav_pt_hist = ROOT.TH2D("btagpass_lflav_pt_hist","btagpass_lflav_pt_hist", len(eta_bins) - 1, array('d', eta_bins), len(pt_bins) - 1, array('d', pt_bins))
all_lflav_pt_hist = ROOT.TH2D("all_lflav_pt_hist","all_lflav_pt_hist", len(eta_bins) - 1, array('d', eta_bins), len(pt_bins) - 1, array('d', pt_bins))
h_btagEff_bcflav = ROOT.TH2D('h_btagEff_bcflav', 'DeepJet Efficiency '+year+', bctag; Jet p_{T}; Jet |\eta|;', len(eta_bins) - 1, array('d', eta_bins), len(pt_bins) - 1, array('d', pt_bins))
h_btagEff_lflav = ROOT.TH2D('h_btagEff_lflav', 'DeepJet Efficiency '+year+', light; Jet p_{T}; Jet |\eta|;', len(eta_bins) - 1, array('d', eta_bins), len(pt_bins) - 1, array('d', pt_bins))

for sample in Samples:

    inputFile = ROOT.TFile.Open('analyzed/' + date + '/Merged/' + sample + '_UL' + year + '.root', 'READ')

    genhisto = inputFile.Get('genweight_nocut;1')
    sumofweights = genhisto.GetEntries()*genhisto.GetMean()
    if(sample == 'Signal600'):
        scale = 176.4*Lumi/sumofweights
    elif(sample == 'Signal625'):
        scale = 148.9*Lumi/sumofweights
    elif(sample == 'Signal650'):
        scale = 121.3*Lumi/sumofweights
    elif(sample == 'Signal675'):
        scale = 105.0*Lumi/sumofweights
    elif(sample == 'Signal700'):
        scale = 88.6*Lumi/sumofweights
    elif(sample == 'Signal800'):
        scale = 45.9*Lumi/sumofweights
    elif(sample == 'Signal900'):
        scale = 25.1*Lumi/sumofweights
    elif(sample == 'Signal1000'):
        scale = 14.5*Lumi/sumofweights
    elif(sample == 'Signal1100'):
        scale = 8.67*Lumi/sumofweights
    elif(sample == 'Signal1200'):
        scale = 5.36*Lumi/sumofweights
    elif(sample == 'DY_10_50'):
        scale = 18610000*Lumi/sumofweights
    elif(sample == 'DY_4_50_70'):
        scale = 301200*Lumi/sumofweights
    elif(sample == 'DY_4_50_100'):
        scale = 224200*Lumi/sumofweights
    elif(sample == 'DY_4_50_200'):
        scale = 37200*Lumi/sumofweights
    elif(sample == 'DY_4_50_400'):
        scale = 3581*Lumi/sumofweights
    elif(sample == 'DY_4_50_600'):
        scale = 1124*Lumi/sumofweights
    elif(sample == 'TT_2L'):
        scale = 88341.9*Lumi/sumofweights
    elif(sample == 'TT_SL'):
        scale = 365457.4*Lumi/sumofweights
    elif(sample == 'TT_Had'):
        scale = 377960.7*Lumi/sumofweights
    elif(sample == 'ttW'):
        scale = 235*Lumi/sumofweights
    elif(sample == 'ttH'):
        scale = 211.2*Lumi/sumofweights
    elif(sample == 'ttZ'):
        scale = 859*Lumi/sumofweights
    elif('tW' in sample):
        scale = 39650*Lumi/sumofweights
    elif(sample == 'tHq'):
        scale = 70.96*Lumi/sumofweights
    elif(sample == 'tHW'):
        scale = 15.61*Lumi/sumofweights
    elif(sample == 'tZq'):
        scale = 73.58*Lumi/sumofweights
    elif(sample == 'WW'):
        scale = 12178*Lumi/sumofweights
    elif(sample == 'WW_DS'):
        scale = 223.2*Lumi/sumofweights
    elif(sample == 'WJets'):
        scale = 61334900*Lumi/sumofweights
    elif('WH' in sample):
        scale = 31.3*Lumi/sumofweights
    elif(sample == 'WZ'):
        scale = 4429.7*Lumi/sumofweights
    elif(sample == 'ZH'):
        scale = 185.8*Lumi/sumofweights
    elif(sample == 'ZZ_2L2Nu'):
        scale = 564.4*Lumi/sumofweights
    elif(sample == 'ZZ_2L2Q'):
        scale = 1973*Lumi/sumofweights
    elif(sample == 'ZZ_4L'):
        scale = 1256*Lumi/sumofweights
    elif(sample == 'WWW'):
        scale = 208.6*Lumi/sumofweights
    elif(sample == 'WWZ'):
        scale = 165.1*Lumi/sumofweights
    elif(sample == 'WZZ'):
        scale = 55.65*Lumi/sumofweights
    elif(sample == 'ZZZ'):
        scale = 14.76*Lumi/sumofweights
    print(sample)
    print(sumofweights)
    print(scale)

    btagpass_bcflav_pt_hist_1 = inputFile.Get('Eta_Pt_bJets_BtagPass_bcFlav_cut_0_0_0_0')
    all_bcflav_pt_hist_1 = inputFile.Get('Eta_Pt_Jets_All_bcFlav_cut_0_0_0_0')
    btagpass_lflav_pt_hist_1 = inputFile.Get('Eta_Pt_bJets_BtagPass_lFlav_cut_0_0_0_0')
    all_lflav_pt_hist_1 = inputFile.Get('Eta_Pt_Jets_All_lFlav_cut_0_0_0_0')
    btagpass_bcflav_pt_hist_2 = inputFile.Get('Eta_Pt_bJets_BtagPass_bcFlav_cut_0_0_1_0')
    all_bcflav_pt_hist_2 = inputFile.Get('Eta_Pt_Jets_All_bcFlav_cut_0_0_1_0')
    btagpass_lflav_pt_hist_2 = inputFile.Get('Eta_Pt_bJets_BtagPass_lFlav_cut_0_0_1_0')
    all_lflav_pt_hist_2 = inputFile.Get('Eta_Pt_Jets_All_lFlav_cut_0_0_1_0')
    btagpass_bcflav_pt_hist_1.Scale(scale)
    all_bcflav_pt_hist_1.Scale(scale)
    btagpass_lflav_pt_hist_1.Scale(scale)
    all_lflav_pt_hist_1.Scale(scale)
    btagpass_bcflav_pt_hist_2.Scale(scale)
    all_bcflav_pt_hist_2.Scale(scale)
    btagpass_lflav_pt_hist_2.Scale(scale)
    all_lflav_pt_hist_2.Scale(scale)

    btagpass_bcflav_pt_hist.Add(btagpass_bcflav_pt_hist_1)
    all_bcflav_pt_hist.Add(all_bcflav_pt_hist_1)
    btagpass_lflav_pt_hist.Add(btagpass_lflav_pt_hist_1)
    all_lflav_pt_hist.Add(all_lflav_pt_hist_1)
    btagpass_bcflav_pt_hist.Add(btagpass_bcflav_pt_hist_2)
    all_bcflav_pt_hist.Add(all_bcflav_pt_hist_2)
    btagpass_lflav_pt_hist.Add(btagpass_lflav_pt_hist_2)
    all_lflav_pt_hist.Add(all_lflav_pt_hist_2)

for i in range(len(eta_bins) - 1):
    for j in range(len(pt_bins) - 1):
        numerator_bcflav = btagpass_bcflav_pt_hist.GetBinContent(i + 1, j + 1)
        denominator_bcflav = all_bcflav_pt_hist.GetBinContent(i + 1, j + 1)
        bcflav_efficiency = numerator_bcflav / denominator_bcflav if denominator_bcflav != 0 else 0
            
        numerator_lflav = btagpass_lflav_pt_hist.GetBinContent(i + 1, j + 1)
        denominator_lflav = all_lflav_pt_hist.GetBinContent(i + 1, j + 1)
        lflav_efficiency = numerator_lflav / denominator_lflav if denominator_lflav != 0 else 0

        bcflav_efficiency_round = round(bcflav_efficiency, 3)
        lflav_efficiency_round = round(lflav_efficiency, 3)
        h_btagEff_bcflav.SetBinContent(i + 1, j + 1, bcflav_efficiency_round)
        h_btagEff_lflav.SetBinContent(i + 1, j + 1, lflav_efficiency_round)

        if numerator_bcflav < 0 or denominator_bcflav < 0 or numerator_bcflav > denominator_bcflav:
            bcflav_efficiency = merge_with_previous_bin(btagpass_bcflav_pt_hist, all_bcflav_pt_hist, i, j)

        if numerator_lflav < 0 or denominator_lflav < 0 or numerator_lflav > denominator_lflav:
            lflav_efficiency = merge_with_previous_bin(btagpass_lflav_pt_hist, all_lflav_pt_hist, i, j)

        bcflav_efficiency_round = round(bcflav_efficiency, 3)
        lflav_efficiency_round = round(lflav_efficiency, 3)
        h_btagEff_bcflav.SetBinContent(i + 1, j + 1, bcflav_efficiency_round)
        h_btagEff_lflav.SetBinContent(i + 1, j + 1, lflav_efficiency_round)

output_file_path = 'data/BTV/' + year + '_UL/BtaggingEfficiency.root'
output_file = ROOT.TFile.Open(output_file_path, 'RECREATE')
h_btagEff_bcflav.SetStats(0)
h_btagEff_lflav.SetStats(0)
h_btagEff_bcflav.Write()
h_btagEff_lflav.Write()
output_file.Close()
inputFile.Close()

print('Process completed.')
