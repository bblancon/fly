#!/bin/bash
#
#SBATCH --partition=normal
# mail-type=BEGIN, END, FAIL, REQUEUE, ALL, STAGE_OUT, TIME_LIMIT_90
#SBATCH --mail-type=FAIL
#SBATCH --mail-user=b.blancon@ip2i.in2p3.fr

export X509_USER_PROXY=$HOME/x509up_u2459
export EOS_MGM_URL=root://lyoeos.in2p3.fr

inputFile=${1}
year=${3}
process=${4}
Combine=${5}
JEC=${6}
cutindex=${7}
Data=${8}

root='.root'

for line in $inputFile
do
    outputFile=$(echo $line | tr "/" "\n")
    for place in $outputFile
    do
        if [[ "$place" == *"$root"* ]]; then
            srun -N1 -n1 ./processHist.py "$line" ${2}$place jobconfiganalysis$year "$year" "$process" "$Combine" "$JEC" "$cutindex" "$Data"  > ${2}$(echo $place | awk '{ print substr( $0, 1, length($0)-5 ) }').out &
        fi
    done
done
wait